Madsonic
========

### About

Madsonic is a free, web-based media streamer and jukebox.

### Compatibility

Based on Java technology, Madsonic runs on most platforms, including Windows, Mac, Linux, OSX, and Unix variants.<br>
Madsonic is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License.<br>

More information, including installation instructions, is found at <https://madsonic.org/>

### Contact

Madsonic is developed and maintained by Martin Karel (support@madsonic.org).

If you have any questions, comments or suggestions for improvements,<br>
please visit the Madsonic Forum <https://support.madsonic.org/>
