/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap.ConnectTest;

import static org.junit.Assert.assertEquals;

import javax.naming.ldap.LdapName;

import org.junit.Test;
import org.madsonic.ldap.MicrosoftADLdapIntegrationTests;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;

public class MicrosoftFilterBasedLdapUserSearchTests extends MicrosoftADLdapIntegrationTests {

	@Test
	public void basicMicrosoftSearchSucceeds() throws Exception {

		FilterBasedLdapUserSearch locator = new FilterBasedLdapUserSearch("","(sAMAccountName={0})", getContextSource());
		locator.setSearchSubtree(true);
		locator.setSearchTimeLimit(0);
		locator.setDerefLinkFlag(true);

		DirContextOperations bob = locator.searchForUser("bob");
		
		assertEquals("bob", bob.getStringAttribute("sAMAccountName"));
		assertEquals(new LdapName("CN=bob,OU=users,OU=organisation"), bob.getDn());
	}
}	
