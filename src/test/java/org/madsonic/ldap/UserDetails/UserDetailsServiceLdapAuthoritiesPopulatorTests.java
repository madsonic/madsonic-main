/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap.UserDetails;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.authentication.UserDetailsServiceLdapAuthoritiesPopulator;

public class UserDetailsServiceLdapAuthoritiesPopulatorTests {

	@SuppressWarnings("unchecked")
	@Test
	public void delegationToUserDetailsServiceReturnsCorrectRoles() throws Exception {
		UserDetailsService uds = mock(UserDetailsService.class);
		UserDetails user = mock(UserDetails.class);
		when(uds.loadUserByUsername("bob")).thenReturn(user);
		
		@SuppressWarnings("rawtypes")
		List authorities = AuthorityUtils.createAuthorityList("ROLE_STREAM");
		when(user.getAuthorities()).thenReturn(authorities);

		UserDetailsServiceLdapAuthoritiesPopulator populator = new UserDetailsServiceLdapAuthoritiesPopulator(uds);
		Collection<? extends GrantedAuthority> auths = populator.getGrantedAuthorities(new DirContextAdapter(), "bob");

		assertEquals(1, auths.size());
		assertTrue(AuthorityUtils.authorityListToSet(auths).contains("ROLE_STREAM"));
	}
}
