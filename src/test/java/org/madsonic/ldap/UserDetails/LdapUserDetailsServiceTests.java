/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2017 (C) Martin Karel
 *  
 */
package org.madsonic.ldap.UserDetails;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Set;

import javax.naming.ldap.LdapName;

import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.authentication.NullLdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;
import org.springframework.security.ldap.userdetails.LdapUserDetailsService;

/**
 * Tests for {@link LdapUserDetailsService}
 *
 * @author Luke Taylor
 */
public class LdapUserDetailsServiceTests {

	@Test(expected = IllegalArgumentException.class)
	public void rejectsNullSearchObject() {
		new LdapUserDetailsService(null, new NullLdapAuthoritiesPopulator());
	}

	@Test(expected = IllegalArgumentException.class)
	public void rejectsNullAuthoritiesPopulator() {new LdapUserDetailsService(new MadsonicUserSearch(), null);
	}

	@Test
	public void correctAuthoritiesAreReturned() throws Exception {
		DirContextAdapter userData = new DirContextAdapter(new LdapName("uid=joe"));

		LdapUserDetailsService service = new LdapUserDetailsService(new MadsonicUserSearch(userData), new MockAuthoritiesPopulator());
		service.setUserDetailsMapper(new LdapUserDetailsMapper());

		UserDetails user = service.loadUserByUsername("doesntmatterwegetjoeanyway");
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		assertEquals(1, authorities.size());
		assertTrue(authorities.contains("ROLE_FROM_POPULATOR"));
	}

	@Test
	public void nullPopulatorConstructorReturnsEmptyAuthoritiesList() throws Exception {
		DirContextAdapter userData = new DirContextAdapter(new LdapName("uid=joe"));
		LdapUserDetailsService service = new LdapUserDetailsService(new MadsonicUserSearch(userData));
		UserDetails user = service.loadUserByUsername("doesntmatterwegetjoeanyway");
		assertEquals(0, user.getAuthorities().size());
	}

	class MockAuthoritiesPopulator implements LdapAuthoritiesPopulator {
		public Collection<GrantedAuthority> getGrantedAuthorities(
				DirContextOperations userCtx, String username) {
			return AuthorityUtils.createAuthorityList("ROLE_FROM_POPULATOR");
		}
	}
}
