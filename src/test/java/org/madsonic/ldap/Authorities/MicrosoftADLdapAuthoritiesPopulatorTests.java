/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap.Authorities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.naming.ldap.LdapName;

import org.junit.Before;
import org.junit.Test;
import org.madsonic.ldap.MadsonicLdapAuthoritiesPopulator;
import org.madsonic.ldap.MicrosoftADLdapIntegrationTests;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

//~ Test only with with MicrosoftAD
//~ ========================================================================================================

public class MicrosoftADLdapAuthoritiesPopulatorTests extends MicrosoftADLdapIntegrationTests {
	
    private MadsonicLdapAuthoritiesPopulator populator;
    
    private final String distinguishedName = "cn=test,ou=users,ou=organisation,dc=madsonic,dc=org";
    
    //~ Methods ========================================================================================================

    @Before
    public void setUp() throws Exception {
        populator = new MadsonicLdapAuthoritiesPopulator(getContextSource(),"");
        populator.setIgnorePartialResultException(true);
    }

    @Test
    public void groupSearchReturnsExpectedRoles() {
        populator.setRolePrefix("ROLE_");
        populator.setGroupRoleAttribute("cn");
        populator.setSearchSubtree(true);
        populator.setConvertToUpperCase(true);
        populator.setGroupSearchFilter("(&(objectClass=group)(member={0}))");

        DirContextAdapter ctx = new DirContextAdapter(distinguishedName);
        Set<String> authorities = AuthorityUtils.authorityListToSet(populator.getGrantedAuthorities(ctx, "test"));

        assertEquals("Should have 10 roles", 10, authorities.size());
        
        assertTrue(authorities.contains("ROLE_MADSONIC.MANAGER"));
        assertTrue(authorities.contains("ROLE_MADSONIC.JUKEBOX"));
        assertTrue(authorities.contains("ROLE_MADSONIC.ADMIN"));
        assertTrue(authorities.contains("ROLE_MADSONIC.STREAM"));
        assertTrue(authorities.contains("ROLE_MADSONIC.PLAY"));
    }

    @Test
    public void useOfUsernameParameterReturnsExpectedRoles() {
        populator.setGroupRoleAttribute("cn");
        populator.setConvertToUpperCase(true);
        populator.setGroupSearchFilter("(member={0})");

        DirContextAdapter ctx = new DirContextAdapter(distinguishedName);
        Set<String> authorities = AuthorityUtils.authorityListToSet(populator.getGrantedAuthorities(ctx, "madsonic.manager"));

        assertEquals("Should have 1 role", 1, authorities.size());
        assertTrue(authorities.contains("ROLE_MADSONIC.MANAGER"));
    }

    @Test
    public void subGroupRolesAreNotFoundByDefault() {
        populator.setGroupRoleAttribute("cn");
        populator.setConvertToUpperCase(true);

        DirContextAdapter ctx = new DirContextAdapter(distinguishedName);
        Set<String> authorities = AuthorityUtils.authorityListToSet(populator.getGrantedAuthorities(ctx, "madsonic.manager"));

        assertEquals("Should have 4 roles", 4, authorities.size());
        assertTrue(authorities.contains("ROLE_MADSONIC.MANAGER"));
        assertTrue(authorities.contains("ROLE_MADSONIC.ADMIN"));
        assertTrue(authorities.contains("ROLE_MADSONIC.STREAM"));
        assertTrue(authorities.contains("ROLE_MADSONIC.PLAY"));
    }

    @Test
    public void subGroupRolesAreFoundWhenSubtreeSearchIsEnabled() {
        populator.setGroupRoleAttribute("cn");
        populator.setConvertToUpperCase(true);
        populator.setGroupSearchFilter("(cn={0})");
        populator.setSearchSubtree(true);

        DirContextAdapter ctx = new DirContextAdapter(distinguishedName);
        Set<String> authorities = AuthorityUtils.authorityListToSet(populator.getGrantedAuthorities(ctx, "madsonic.manager"));

        assertEquals("Should have 4 roles", 4, authorities.size());
        assertTrue(authorities.contains("ROLE_MADSONIC.MANAGER"));
        assertTrue(authorities.contains("ROLE_MADSONIC.ADMIN"));
        assertTrue(authorities.contains("ROLE_MADSONIC.STREAM"));
        assertTrue(authorities.contains("ROLE_MADSONIC.PLAY"));
    }

    @Test
    public void extraRolesAreAdded() throws Exception {
        populator = new MadsonicLdapAuthoritiesPopulator(getContextSource(), null) {
            @Override
            protected Set<GrantedAuthority> getAdditionalRoles(DirContextOperations user, String username) {
                return new HashSet<GrantedAuthority>(AuthorityUtils.createAuthorityList("ROLE_EXTRA"));
            }
        };

        Collection<GrantedAuthority> authorities = populator.getGrantedAuthorities(
        		
                new DirContextAdapter(new LdapName("cn=notused")), "notused");
        assertEquals(1, authorities.size());
        assertTrue(AuthorityUtils.authorityListToSet(authorities).contains("ROLE_EXTRA"));
    }

}
