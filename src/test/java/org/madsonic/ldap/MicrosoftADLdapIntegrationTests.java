/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap;

import org.junit.BeforeClass;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;

public abstract class MicrosoftADLdapIntegrationTests {
	
	//~ Test only with with Microsoft AD 
	//~ ========================================================================================================
	
	private static DefaultSpringSecurityContextSource contextSource;

	@BeforeClass
	public static void createContextSource() throws Exception {
		contextSource = new DefaultSpringSecurityContextSource("ldap://localhost:389/dc=madsonic,dc=at");
		contextSource.setUserDn("cn=ldap,ou=users,ou=organisation,dc=madsonic,dc=at");
		contextSource.setPassword("bind");
		contextSource.setAnonymousReadOnly(false);		
		contextSource.afterPropertiesSet();
	}

	public BaseLdapPathContextSource getContextSource() {
		return contextSource;
	}

}