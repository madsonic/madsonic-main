/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap.BindAuthenticator;

import static org.junit.Assert.*;

import org.junit.*;
import org.madsonic.ldap.ApacheDSLdapIntegrationTests;
import org.madsonic.ldap.MadsonicBindAuthenticator;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;

public class BindAuthenticatorTests extends ApacheDSLdapIntegrationTests {

	// ~ Instance fields
	// ================================================================================================

	private MadsonicBindAuthenticator authenticator;
	private Authentication bob;

	// ~ Methods
	// ========================================================================================================

	@Before
	public void setUp() {
		authenticator = new MadsonicBindAuthenticator(getContextSource());
		authenticator.setMessageSource(new SpringSecurityMessageSource());
		bob = new UsernamePasswordAuthenticationToken("bob", "bob");
	}

	@Test(expected = BadCredentialsException.class)
	public void emptyPasswordIsRejected() {
		authenticator.authenticate(new UsernamePasswordAuthenticationToken("jen", ""));
	}

	@Test
	public void testAuthenticationWithCorrectPasswordSucceeds() {
		authenticator.setUserDnPatterns(new String[] { "uid={0},ou=users", "cn={0},ou=users" });

		DirContextOperations user = authenticator.authenticate(bob);
		assertEquals("bob", user.getStringAttribute("uid"));
		authenticator.authenticate(new UsernamePasswordAuthenticationToken("test", "test"));
	}

	@Test
	public void testAuthenticationWithInvalidUserNameFails() {
		authenticator.setUserDnPatterns(new String[] { "uid={0},ou=users" });

		try {
			authenticator.authenticate(new UsernamePasswordAuthenticationToken("nonexistentsuser", "password"));
			fail("Shouldn't be able to bind with invalid username");
		}
		catch (BadCredentialsException expected) {
		}
	}

	@Test
	public void testAuthenticationWithUserSearch() throws Exception {
		
		// DirContextAdapter ctx = new DirContextAdapter(new DistinguishedName("uid=bob,ou=users"));
		authenticator.setUserSearch(new FilterBasedLdapUserSearch("ou=users","(uid={0})", getContextSource()));
		authenticator.afterPropertiesSet();
		
		authenticator.authenticate(bob);// SEC-1444
		authenticator.setUserSearch(new FilterBasedLdapUserSearch("ou=users","(cn={0})", getContextSource()));
		authenticator.authenticate(new UsernamePasswordAuthenticationToken("test", "test"));
	}

	@Test
	public void testAuthenticationWithWrongPasswordFails() {
		authenticator.setUserDnPatterns(new String[] { "uid={0},ou=users" });

		try {
			authenticator.authenticate(new UsernamePasswordAuthenticationToken("bob","wrongpassword"));
			fail("Shouldn't be able to bind with wrong password");
		}
		catch (BadCredentialsException expected) {
		}
	}
}