/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.dao;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.madsonic.domain.AccessGroup;
import org.madsonic.domain.AccessToken;
import org.madsonic.domain.AccessRight;
import org.madsonic.domain.MusicFolder;

import org.madsonic.domain.Group;

public class GroupDaoTestCase extends DaoTestCaseBase {

	protected void setUp() throws Exception {

    	getJdbcTemplate().execute("delete from user_group_access");
		getJdbcTemplate().execute("insert into user_group_access (user_group_id, music_folder_id, enabled) (select distinct user_group.id as user_group_id, music_folder.id as music_folder_id, true as enabled from user_group, music_folder)");
		}

	
    public void testCreateMusicFolder() {
        MusicFolder musicFolder = new MusicFolder(new File("path"), "name", true, new Date(), 1, 1, 0, new Date(), 3600);
        musicFolderDao.createMusicFolder(musicFolder);
        MusicFolder newMusicFolder = musicFolderDao.getAllMusicFolders().get(0);
    }	
    
    public void testGetAllGroups(){
    	List<Group> x = groupDao.getAllGroups();
    }
    
    public void testCreateAcessGroup(){
    	//Create Group
    	AccessGroup AG = new AccessGroup();

    	//Create Admin Token
    	AccessToken AT_Admin = new AccessToken();
    	AT_Admin.setUserGroupId(1);
    	AT_Admin.addAccessRight(new AccessRight(0,true,true));
    	AT_Admin.addAccessRight(new AccessRight(1,true,true));
    	AT_Admin.addAccessRight(new AccessRight(2,true,true));
    	AT_Admin.addAccessRight(new AccessRight(3,true,false));
    	AT_Admin.addAccessRight(new AccessRight(4,true,false));
    	
    	AG.addAccessToken(AT_Admin);   	

    	//Create Guest Token
    	AccessToken AT_Guest = new AccessToken();
    	AT_Admin.setUserGroupId(2);
    	AT_Guest.addAccessRight(new AccessRight(0,true,true));
    	AT_Guest.addAccessRight(new AccessRight(1,false,true));
    	AT_Guest.addAccessRight(new AccessRight(2,false,true));
    	AT_Guest.addAccessRight(new AccessRight(3,false,false));
    	AT_Guest.addAccessRight(new AccessRight(4,false,false));

    	AG.addAccessToken(AT_Guest);   
    	
    	assertEquals(true,  AG.getAccessToken().get(0).getAccessRight().get(0).isEnabled());
    	assertEquals(true,  AG.getAccessToken().get(1).getAccessRight().get(0).isEnabled());
    	assertEquals(false, AG.getAccessToken().get(0).getAccessRight().get(4).isEnabled());
    	assertEquals(false, AG.getAccessToken().get(1).getAccessRight().get(4).isEnabled());
    }

    public void testCreateAcessToken(){
    	AccessToken AT = new AccessToken();
    	AT.addAccessRight(new AccessRight(0,false,false));
    	AT.addAccessRight(new AccessRight(1,false,false));
    	AT.addAccessRight(new AccessRight(2,false,false));
    	AT.addAccessRight(new AccessRight(3,true,true));
    	AT.addAccessRight(new AccessRight(4,true,true));
    	assertNotNull(AT);
    }       

    public void testCreateAcessRight(){
    	AccessRight AR = new AccessRight();
    	AR.setMusicfolder_id(0);
    	AR.setEnabled(true);
    	assertNotNull(AR);
    	}

}
