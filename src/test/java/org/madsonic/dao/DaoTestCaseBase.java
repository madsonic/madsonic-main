/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.dao;

import junit.framework.TestCase;

import org.madsonic.util.FileUtil;
import org.springframework.jdbc.core.JdbcTemplate;
 
import java.io.File;

/**
 * Superclass for all DAO test cases.
 * Creates and configures the DAO's, and resets the test database.
 *
 * @author Sindre Mehus, Martin Karel
 */
public abstract class DaoTestCaseBase extends TestCase {

    /** Do not re-create database if it is less than one hour old. */
    private static final long MAX_DB_AGE_MILLIS = 60L * 60 * 1000;

    static {
        deleteDatabase();
    }

    private DaoHelper daoHelper;
    
    protected PlayerDao playerDao;
    protected NodeDao nodeDao;
    protected InternetRadioDao internetRadioDao;
    protected RatingDao ratingDao;
    protected MusicFolderDao musicFolderDao;
    protected MusicFolderTasksDao musicFolderTasksDao;
    protected UserDao userDao;
    protected TranscodingDao transcodingDao;
    protected PodcastDao podcastDao;
    protected GroupDao groupDao;
    protected AccessRightDao accessRightDao;
    protected LastFMArtistDao lastFMArtistDao;
    protected LastFMArtistSimilarDao lastFMArtistSimilarDao;
    protected AudioConversionDao audioConversionDao;
    protected VideoConversionDao videoConversionDao;
	protected MediaFileDao mediaFileDao;
    protected AvatarDao avatarDao;

    protected DaoTestCaseBase() {
    	
        daoHelper = new HsqlDaoHelper();
        playerDao = new PlayerDao();
        nodeDao = new NodeDao();
        internetRadioDao = new InternetRadioDao();
        ratingDao = new RatingDao();
        musicFolderDao = new MusicFolderDao();
        musicFolderTasksDao = new MusicFolderTasksDao();
        userDao = new UserDao();
        transcodingDao = new TranscodingDao();
        podcastDao = new PodcastDao();
        groupDao = new GroupDao();
        accessRightDao = new AccessRightDao();
        lastFMArtistDao = new LastFMArtistDao();  
        lastFMArtistSimilarDao = new LastFMArtistSimilarDao();
        audioConversionDao = new AudioConversionDao();
        videoConversionDao = new VideoConversionDao();
    	mediaFileDao = new MediaFileDao();
        avatarDao = new AvatarDao();
        
        playerDao.setDaoHelper(daoHelper);
        nodeDao.setDaoHelper(daoHelper);
        internetRadioDao.setDaoHelper(daoHelper);
        ratingDao.setDaoHelper(daoHelper);
        musicFolderDao.setDaoHelper(daoHelper);
        musicFolderTasksDao.setDaoHelper(daoHelper);
        userDao.setDaoHelper(daoHelper);
        transcodingDao.setDaoHelper(daoHelper);
        podcastDao.setDaoHelper(daoHelper);
        groupDao.setDaoHelper(daoHelper);
        accessRightDao.setDaoHelper(daoHelper);
        lastFMArtistDao.setDaoHelper(daoHelper);
        lastFMArtistSimilarDao.setDaoHelper(daoHelper);
        audioConversionDao.setDaoHelper(daoHelper);
        videoConversionDao.setDaoHelper(daoHelper);
    	mediaFileDao.setDaoHelper(daoHelper);
    	avatarDao.setDaoHelper(daoHelper);

        }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        getJdbcTemplate().execute("shutdown");
    }

    protected JdbcTemplate getJdbcTemplate() {
        return daoHelper.getJdbcTemplate();
    }

    private static void deleteDatabase() {
        File madsonicHome = new File("/tmp/madsonic");
        File dbHome = new File(madsonicHome, "db");
        System.setProperty("madsonic.home", madsonicHome.getPath());

        long now = System.currentTimeMillis();
        if (now - dbHome.lastModified() > MAX_DB_AGE_MILLIS) {
            System.out.println("Resetting test database: " + dbHome);
            delete(dbHome);
        }
    }

    private static void delete(File file) {
        if (file.isDirectory()) {
            for (File child : FileUtil.listFiles(file)) {
                delete(child);
            }
        }
        file.delete();
    }
}
