/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.dao;

import org.madsonic.domain.Node;

/**
 * Unit test of {@link NodeDao}.
 *
 * @author Martin Karel 
 */
public class NodeDaoTestCase extends DaoTestCaseBase {

    protected void setUp() throws Exception {
        getJdbcTemplate().execute("delete from nodes");
    }

    public void testCreateNode() {
        Node node = new Node(null, "http://localhost:8080", "node-10.0.0.50-living-room", false, true);
        nodeDao.createNode(node);
        Node newNode = nodeDao.getAllNodes().get(0);
        assertNodeEquals(node, newNode);
    }

    public void testUpdateNode() {
        Node node = new Node(null, "url", "name", true, true);
        nodeDao.createNode(node);
        node = nodeDao.getAllNodes().get(0);
        node.setUrl("newUrl");
        node.setName("newName");
        node.setOnline(false);
        node.setEnabled(false);
        nodeDao.updateNode(node);

        Node newnode = nodeDao.getAllNodes().get(0);
        assertNodeNotEquals(node, newnode);
    }

    public void testDeleteNode() {
        assertEquals("Wrong number of nodes.", 0, nodeDao.getAllNodes().size());

        nodeDao.createNode(new Node(null, "Url", "Name", true, true));
        assertEquals("Wrong number of nodes.", 1, nodeDao.getAllNodes().size());

        nodeDao.createNode(new Node(null, "Url", "Name", true, true));
        assertEquals("Wrong number of nodes.", 2, nodeDao.getAllNodes().size());

        nodeDao.deleteNode(nodeDao.getAllNodes().get(0).getId());
        assertEquals("Wrong number of nodes.", 1, nodeDao.getAllNodes().size());

        nodeDao.deleteNode(nodeDao.getAllNodes().get(0).getId());
        assertEquals("Wrong number of nodes.", 0, nodeDao.getAllNodes().size());
    }

    private void assertNodeNotEquals(Node expected, Node actual) {
    	assertNotSame("Wrong stream url.", expected.getUrl(), actual.getUrl());
    	assertNotSame("Wrong name.", expected.getName(), actual.getName());
    	assertNotSame("Wrong online state.", expected.isOnline(), actual.isOnline());
    	assertNotSame("Wrong enabled state.", expected.isEnabled(), actual.isEnabled());
    }    
    
    private void assertNodeEquals(Node expected, Node actual) {
    	assertEquals("Wrong stream url.", expected.getUrl(), actual.getUrl());
    	assertEquals("Wrong name.", expected.getName(), actual.getName());
    	assertEquals("Wrong online state.", expected.isOnline(), actual.isOnline());
    	assertEquals("Wrong enabled state.", expected.isEnabled(), actual.isEnabled());
    }
}