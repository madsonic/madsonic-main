/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.domain;

/**
 * Unit test of {@link Version}.
 * @author Sindre Mehus, Martin Karel
 */

import junit.framework.*;

public class ServerVersionTestCase extends TestCase {

    /**
     * Tests that equals(), hashCode(), toString() and compareTo() works.
     */
    public void testVersion()  {
        
        doTestVersion("6.0.6800.abdfb4b", "6.0.6900.abdfb4b");
    }

    /**
     * Tests that equals(), hashCode(), toString() and compareTo() works.
     * @param v1 A lower version.
     * @param v2 A higher version.
     */
    private void doTestVersion(String v1, String v2) {
        ServerVersion ver1 = new ServerVersion(v1);
        ServerVersion ver2 = new ServerVersion(v2);

        assertEquals("Error in toString().", v1, ver1.toString());
        assertEquals("Error in toString().", v2, ver2.toString());

        assertEquals("Error in equals().", ver1, ver1);

        assertEquals("Error in compareTo().", 0, ver1.compareTo(ver1));
        assertEquals("Error in compareTo().", 0, ver2.compareTo(ver2));
        
        assertTrue("Error in compareTo().", ver1.compareTo(ver2) < 0);
        assertTrue("Error in compareTo().", ver2.compareTo(ver1) > 0);
    }
}