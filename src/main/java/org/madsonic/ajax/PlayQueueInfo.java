/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.ajax;

import java.util.List;

import org.madsonic.util.StringUtil;

/**
 * The playlist of a player.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PlayQueueInfo {

    private final List<Entry> entries;
    private final int index;
    private final int lockMode;
    private final boolean stopEnabled;
    private final boolean repeatEnabled;
    private final boolean followMeEnabled;
    private final boolean sendM3U;
    private final float jukeboxGain;
    private final boolean jukeboxMute;
    private int startPlayerAt = -1;
    private long startPlayerAtPosition; // millis

    public PlayQueueInfo(List<Entry> entries, int index, boolean stopEnabled, boolean repeatEnabled, boolean followMeEnabled, boolean sendM3U, 
	float jukeboxGain, boolean jukeboxMute, int lockMode) {
        this.entries = entries;
        this.index = index;
        this.lockMode = lockMode;
        this.stopEnabled = stopEnabled;
        this.repeatEnabled = repeatEnabled;
        this.followMeEnabled = followMeEnabled;
        this.sendM3U = sendM3U;
        this.jukeboxGain = jukeboxGain;
        this.jukeboxMute = jukeboxMute;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public String getDurationAsString() {
        int durationSeconds = 0;
        for (Entry entry : entries) {
            if (entry.getDuration() != null) {
                durationSeconds += entry.getDuration();
            }
        }
        return StringUtil.formatDuration(durationSeconds);
    }

    public int getIndex() {
        return index;
    }

    public boolean isStopEnabled() {
        return stopEnabled;
    }

    public boolean isSendM3U() {
        return sendM3U;
    }

    public boolean isRepeatEnabled() {
        return repeatEnabled;
    }

    public boolean isFollowMeEnabled() {
        return followMeEnabled;
    }    
    
    public float getJukeboxGain() {
        return jukeboxGain;
    }

    public boolean isJukeboxMute() {
        return jukeboxMute;
    }

    public int getStartPlayerAt() {
        return startPlayerAt;
    }

    public PlayQueueInfo setStartPlayerAt(int startPlayerAt) {
        this.startPlayerAt = startPlayerAt;
        return this;
    }

    public long getStartPlayerAtPosition() {
        return startPlayerAtPosition;
    }

    public PlayQueueInfo setStartPlayerAtPosition(long startPlayerAtPosition) {
        this.startPlayerAtPosition = startPlayerAtPosition;
        return this;
    }
    public int getLockMode() {
		return lockMode;
	}

	public static class Entry {
        private final int id;
        private final String hash;
		
        private final Integer discNumber;
        private final Integer trackNumber;
        private final String title;
        private final String artist;
        private final String composer;
        private final String album;
        private final String genre;
        private final String mood;
        private final Integer year;
        private final Double bpm;
        private final String bitRate;
        private final Integer duration;
        private final String durationAsString;
        private final String sourceFormat;
        private final String format;
        private final String contentType;
        private final String fileSize;
        private final boolean loved;
        private final boolean starred;
        private final Integer bookmarkPosition;
        private final String albumUrl;
        private final String streamUrl;
        private final String remoteStreamUrl;
        private final String coverArtUrl;
        private final String remoteCoverArtUrl;
        private final Integer rank;

        public Entry(int id, String hash, Integer discNumber, Integer trackNumber, String title, String artist, String composer, String album, String genre, String mood, Integer year,
                     Double bpm, String bitRate, Integer duration, String durationAsString, String sourceFormat, String format, String contentType, String fileSize,
                     boolean loved, boolean starred, int bookmarkPosition, String albumUrl, String streamUrl, String remoteStreamUrl, String coverArtUrl, String remoteCoverArtUrl, Integer rank) {
            this.id = id;
            this.hash = hash;
            this.discNumber = discNumber;
            this.trackNumber = trackNumber;
            this.title = title;
            this.artist = artist;
            this.composer = composer;
            this.album = album;
            this.genre = genre;
            this.mood = mood;
            this.year = year;
            this.bpm = bpm;
            this.bitRate = bitRate;
            this.duration = duration;
            this.durationAsString = durationAsString;
            this.sourceFormat = sourceFormat;
            this.format = format;
            this.contentType = contentType;
            this.fileSize = fileSize;
            this.loved = loved;
            this.starred = starred;
            this.bookmarkPosition = bookmarkPosition;
            this.albumUrl = albumUrl;
            this.streamUrl = streamUrl;
            this.remoteStreamUrl = remoteStreamUrl;
            this.coverArtUrl = coverArtUrl;
            this.remoteCoverArtUrl = remoteCoverArtUrl;
            this.rank = rank;
        }

        public int getId() {
            return id;
        }

        public String getHash() {
            return hash;
        }

        public Integer getDiscNumber() {
			return discNumber;
		}

		public Integer getTrackNumber() {
            return trackNumber;
        }

        public String getTitle() {
            return title;
        }

        public String getArtist() {
            return artist;
        }

        public String getAlbum() {
            return album;
        }

        public String getGenre() {
            return genre;
        }

        public String getMood() {
            return mood;
        }        
        
        public Integer getYear() {
            return year;
        }

        public String getBitRate() {
            return bitRate;
        }

        public String getDurationAsString() {
            return durationAsString;
        }

        public Integer getDuration() {
            return duration;
        }

        public String getSourceFormat() {
            return sourceFormat;
        }

        public String getFormat() {
            return format;
        }

        public String getContentType() {
            return contentType;
        }

        public String getFileSize() {
            return fileSize;
        }

        public boolean isStarred() {
            return starred;
        }

		public int getBookmarkPosition() {
			return bookmarkPosition;
		}
		
		public String getAlbumUrl() {
            return albumUrl;
        }

        public String getStreamUrl() {
            return streamUrl;
        }
		
        public String getRemoteStreamUrl() {
            return remoteStreamUrl;
        }

        public String getCoverArtUrl() {
            return coverArtUrl;
        }

        public String getRemoteCoverArtUrl() {
            return remoteCoverArtUrl;
        }
		
		public Integer getRank() {
			return rank;
		}

		public Double getBpm() {
			return bpm;
		}

		public String getComposer() {
			return composer;
		}

		public boolean isLoved() {
			return loved;
		}

    }

}