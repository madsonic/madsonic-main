/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.ajax;

/**
 * Contains info about top album.
 *
 * @author Martin Karel
 */
public class TopAlbum {

    private final int mediaFileId;
    private final String albumName;

    public TopAlbum(int mediaFileId, String albumName) {
        this.mediaFileId = mediaFileId;
        this.albumName = albumName;
    }

    public int getMediaFileId() {
        return mediaFileId;
    }

    public String getAlbumName() {
        return albumName;
    }
}