/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2017 (C) Martin Karel
 *  
 */
package org.madsonic.ajax;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.directwebremoting.WebContextFactory;
import org.madsonic.Logger;
import org.madsonic.domain.AlbumNotes;
import org.madsonic.domain.Artist;
import org.madsonic.domain.ArtistBio;
import org.madsonic.domain.AudioConversion;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.domain.VideoConversion;
import org.madsonic.service.LastFMService;
import org.madsonic.service.LastFmServiceBasic;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.NetworkService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.VideoConversionService;
import org.madsonic.service.AudioConversionService;
import org.madsonic.util.StringUtil;

/**
 * Provides miscellaneous AJAX-enabled services.
 * <p/>
 * This class is used by the DWR framework (http://getahead.ltd.uk/dwr/).
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MultiService {

	private static final Logger LOG = Logger.getLogger(MultiService.class);
    private final NetworkService networkService;
    private final MediaFileService mediaFileService;
	private final LastFMService lastFMService;
	private final LastFmServiceBasic lastFmServiceBasic;
	private final SettingsService settingsService;
	private final SecurityService securityService;
    private final AudioConversionService audioConversionService;
    private final VideoConversionService videoConversionService;
    
    public MultiService(NetworkService networkService, MediaFileService mediaFileService, LastFMService lastFMService, LastFmServiceBasic lastFmServiceBasic,
                        SecurityService securityService, SettingsService settingsService, AudioConversionService audioConversionService, VideoConversionService videoConversionService) {
        this.networkService = networkService;
        this.mediaFileService = mediaFileService;
        this.lastFMService = lastFMService;
        this.lastFmServiceBasic = lastFmServiceBasic;
        this.securityService = securityService;
        this.settingsService = settingsService;
        this.audioConversionService = audioConversionService;
        this.videoConversionService = videoConversionService;
    }

	/**
	 * Returns status for port forwarding and URL redirection.
	 */
	public NetworkStatus getNetworkStatus() {
		NetworkService.Status portForwardingStatus = networkService.getPortForwardingStatus();
		NetworkService.Status urlRedirectionStatus = networkService.getURLRedirecionStatus();
		return new NetworkStatus(portForwardingStatus.getText(),
								 portForwardingStatus.getDate(),
								 urlRedirectionStatus.getText(),
								 urlRedirectionStatus.getDate());
	}

//	public String getMediaFileInfo(int mediaFileId) {
//		MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
//		return new String(mediaFile == null ? null : mediaFile.getHash());
//	}
	
    public AlbumInfo getAlbumInfo(int mediaFileId, int maxSimilarArtists) {
        long t = System.nanoTime();
        MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
        AlbumNotes albumNotes = lastFmServiceBasic.getAlbumNotes(mediaFile);
        ArtistInfo artistInfo = getArtistInfo(mediaFileId, maxSimilarArtists);
        log("getAlbumInfo", t);
        return new AlbumInfo(artistInfo, albumNotes == null ? null : albumNotes.getNotes());
    }	
	
//	public ArtistInfo getArtistInfo(int mediaFileId) {
//	MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
//	List<SimilarArtist> similarArtists = new ArrayList<SimilarArtist>();
//	List<TopAlbum> topAlbums = new ArrayList<TopAlbum>();
//	List<TopTrack> topTracks = getTopTrackArtists(mediaFileId, 99);
//	ArtistBio artistBio = lastFmServiceBasic.getArtistBio(mediaFile);
//	return new ArtistInfo(similarArtists, topAlbums, topTracks ,artistBio);
//}

	public ArtistInfo getArtistInfo(int mediaFileId, int maxSimilarArtists) {
        long t = System.nanoTime();
		MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
		List<SimilarArtist> similarArtists = new ArrayList<SimilarArtist>(); 	// getSimilarArtists(mediaFileId, 10);
		List<TopAlbum> topAlbums = new ArrayList<TopAlbum>(); 					// getTopAlbumArtists(mediaFileId, 10);
		List<TopTrack> topTracks = new ArrayList<TopTrack>(); 					// getTopTrackArtists(mediaFileId, 10);
		ArtistBio artistBio = lastFmServiceBasic.getArtistBio(mediaFile);
        log("getArtistInfo", t);
		return new ArtistInfo(similarArtists, topAlbums, topTracks ,artistBio);
	}	

	public ArtistInfo getMainArtistInfo(int mediaFileId, int maxResults) {
        long t = System.nanoTime();
		MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
		List<SimilarArtist> similarArtists = getSimilarArtists(mediaFileId, maxResults);
		List<TopAlbum> topAlbums = new ArrayList<TopAlbum>();
		List<TopTrack> topTracks = new ArrayList<TopTrack>();
		ArtistBio artistBio = lastFmServiceBasic.getArtistBio(mediaFile);
        log("getFullArtistInfo", t);
		return new ArtistInfo(similarArtists, topAlbums, topTracks ,artistBio);
	}	
	
	public ArtistInfo getFullArtistInfo(int mediaFileId, int maxResults) {
        long t = System.nanoTime();
		MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
		List<SimilarArtist> similarArtists = getSimilarArtists(mediaFileId, maxResults);
		List<TopAlbum> topAlbums = getTopAlbumArtists(mediaFileId, maxResults);
		List<TopTrack> topTracks = getTopTrackArtists(mediaFileId, maxResults);
		ArtistBio artistBio = lastFmServiceBasic.getArtistBio(mediaFile);
        log("getFullArtistInfo", t);
		return new ArtistInfo(similarArtists, topAlbums, topTracks ,artistBio);
	}	
	
	public ArtistBio getArtistBio(int mediaFileId) {
		MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
		return lastFmServiceBasic.getArtistBio(mediaFile);
	}    

	public List<SimilarArtist> getSimilarArtists(int mediaFileId, int limit) {
		MediaFile artist = mediaFileService.getMediaFile(mediaFileId);
		List<MediaFile> similarArtists = lastFmServiceBasic.getSimilarArtists(artist, limit, false);
		SimilarArtist[] result = new SimilarArtist[similarArtists.size()];
		if (similarArtists.size() == 0) {
			return Arrays.asList(new SimilarArtist[0]);
		}
		for (int i = 0; i < result.length; i++) {
			MediaFile similarArtist = similarArtists.get(i);
			result[i] = new SimilarArtist(similarArtist.getId(), similarArtist.getName());
		}
		return Arrays.asList(result);
	}

	public List<TopAlbum> getTopAlbumArtists(int mediaFileId, int limit) {
		MediaFile artist = mediaFileService.getMediaFile(mediaFileId);
		List<MediaFile> topAlbums = lastFmServiceBasic.getTopAlbumArtists(artist, limit, false);
		TopAlbum[] result = new TopAlbum[topAlbums.size()];
		for (int i = 0; i < result.length; i++) {
			MediaFile similarAlbum = topAlbums.get(i);
			result[i] = new TopAlbum(similarAlbum.getId(), similarAlbum.getAlbumSetName());
		}
		return Arrays.asList(result);
	}    

	public TopTrackInfo getTopTrackInfo(int mediaFileId){
		Artist artistCandidate = null ; 
		MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
		String artist = lastFMService.getCorrection(mediaFile.getArtist());
		if (artist != null) artistCandidate = mediaFileService.getLowerArtist(artist);
		boolean isArtist = mediaFile.isSingleArtist() == true ? true : false;
		boolean artistFound = artistCandidate == null ? false : true;
		int topPlayedfound = artistCandidate == null ? 0 : artistCandidate.getTopPlayFound();
		int toplastFMfound = artistCandidate == null ? 0 : artistCandidate.getTopPlayCount();
		if (isArtist) {
			if (artistFound) {
				if (toplastFMfound == 0 && toplastFMfound != -1) {
					toplastFMfound = lastFMService.updateTopTrackArtist(artistCandidate.getName());
					if (toplastFMfound < 1) {
						toplastFMfound = -1;
					} else {
					 // toplastFMfound = lastFMService.getTopTrackArtist(artistCandidate.getName()).size();
						lastFMService.updateTopTrackEntries(artistCandidate.getName(), settingsService.getLastFMResultSize(), 0).size(); 
						topPlayedfound = mediaFileService.getTopTracks(artist, 0).size();
					}
					artistCandidate.setTopPlayFound(topPlayedfound);
					artistCandidate.setTopPlayCount(toplastFMfound);
					if (artistCandidate != null) mediaFileService.createOrUpdateArtist(artistCandidate);
				} else {
					topPlayedfound = mediaFileService.getTopTracks(artist, 0).size();
				}
			}
		}
		return new TopTrackInfo(topPlayedfound, toplastFMfound);
	}

	public List<TopTrack> getTopTrackArtists(int mediaFileId, int limit) {
		MediaFile artist = mediaFileService.getMediaFile(mediaFileId);
		LOG.debug("Starting getTopTrackArtists() id:" + mediaFileId + " artist:" + artist.getArtist() + " limit:" + limit);
		
		List<MediaFile> topTracks = lastFmServiceBasic.getTopTrackArtists(artist, limit, false);
		TopTrack[] result = new TopTrack[topTracks.size()];
		for (int i = 0; i < result.length; i++) {
			MediaFile similarTrack = topTracks.get(i);
			result[i] = new TopTrack(similarTrack.getId(), similarTrack.getTitle());
		}
		return Arrays.asList(result);
	}    

    public void setShowLeftBar(boolean show) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);
        userSettings.setShowLeftBar(show);
        userSettings.setChanged(new Date());
        settingsService.updateUserSettings(userSettings);
    }	
	
    public void setSelectedMusicFolder(int musicFolderId) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));

        // Note: UserSettings.setChanged() is intentionally not called. This would break browser caching
        // of the index frame.
        settings.setSelectedMusicFolderId(musicFolderId);
        settingsService.updateUserSettings(settings);
    }

    public void setSelectedGenre(String genre) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));

        // Note: UserSettings.setChanged() is intentionally not called. This would break browser caching
        // of the index frame.
        settings.setSelectedGenre(genre);
        settingsService.updateUserSettings(settings);
    }    
    
    public AudioConversionStatus getAudioConversionStatus(int mediaFileId) {
        AudioConversion conversion = audioConversionService.getAudioConversionForFile(mediaFileId);
        if (conversion == null) {
            return null;
        }
        AudioConversionStatus result = new AudioConversionStatus();
        result.setTargetFile(conversion.getTargetFile());
        result.setTargetFormat(conversion.getTargetFormat());        
        result.setLogFile(conversion.getLogFile());        
        result.setProgressSeconds(conversion.getProgressSeconds());
        result.setProgressString(StringUtil.formatDuration(conversion.getProgressSeconds()));

        switch (conversion.getStatus()) {
            case NEW:
                result.setStatusNew(true);
                break;
            case IN_PROGRESS:
                result.setStatusInProgress(true);
                break;
            case COMPLETED:
                result.setStatusCompleted(true);
                break;
            case ERROR:
                result.setStatusError(true);
                break;
            default:
                break;
        }
        return result;
    }

    public AudioConversionStatus startAudioConversion(int mediaFileId, Integer bitRate, String format) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);
        authorizeAudioConversion();
        
        File dir = new File(settingsService.getAudioConversionDirectory());
        dir.mkdirs();
        
		MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
        
        String targetFormat = format;
		String title = String.valueOf(mediaFileId);
		
		if (!mediaFile.getArtist().isEmpty()) {	title += " - " + mediaFile.getArtist();	}
		if (!mediaFile.getTitle().isEmpty())  {	title += " - " + mediaFile.getTitle(); }
		
		title += " - " + bitRate;
		
        String targetFile = new File(dir, title + "." + targetFormat).getPath();
        String logFile = new File(dir, title + ".log").getPath();
        

        AudioConversion conversion = new AudioConversion(null, mediaFileId, username, AudioConversion.Status.NEW,
        													targetFile, targetFormat, logFile, bitRate, null, new Date(), new Date(), null);
        audioConversionService.createAudioConversion(conversion);
        return getAudioConversionStatus(mediaFileId);
    }

    public AudioConversionStatus cancelAudioConversion(int mediaFileId) {
    	authorizeAudioConversion();
        AudioConversion conversion = audioConversionService.getAudioConversionForFile(mediaFileId);
        if (conversion != null) {
            audioConversionService.deleteAudioConversion(conversion);
        }
        return getAudioConversionStatus(mediaFileId);
    }

    
    public VideoConversionStatus getVideoConversionStatus(int mediaFileId) {
        VideoConversion conversion = videoConversionService.getVideoConversionForFile(mediaFileId);
        if (conversion == null) {
            return null;
        }
        VideoConversionStatus result = new VideoConversionStatus();
        result.setTargetFile(conversion.getTargetFile());
        result.setLogFile(conversion.getLogFile());
        result.setProgressSeconds(conversion.getProgressSeconds());
        result.setProgressString(StringUtil.formatDuration(conversion.getProgressSeconds()));

        switch (conversion.getStatus()) {
            case NEW:
                result.setStatusNew(true);
                break;
            case IN_PROGRESS:
                result.setStatusInProgress(true);
                break;
            case COMPLETED:
                result.setStatusCompleted(true);
                break;
            case ERROR:
                result.setStatusError(true);
                break;
            default:
                break;
        }
        return result;
    }

    public VideoConversionStatus startVideoConversion(int mediaFileId, Integer audioTrackId, Integer bitRate) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);
        authorizeVideoConversion();

        File dir = new File(settingsService.getVideoConversionDirectory());
        dir.mkdirs();

        String targetFile = new File(dir, mediaFileId + ".mp4").getPath();
        String logFile = new File(dir, mediaFileId + ".log").getPath();
        
        VideoConversion conversion = new VideoConversion(null, mediaFileId, audioTrackId, username, VideoConversion.Status.NEW,
                                                         targetFile, logFile, bitRate, null, new Date(), new Date(), null);
        videoConversionService.createVideoConversion(conversion);

        return getVideoConversionStatus(mediaFileId);
    }

    public VideoConversionStatus cancelVideoConversion(int mediaFileId) {
        authorizeVideoConversion();
        VideoConversion conversion = videoConversionService.getVideoConversionForFile(mediaFileId);
        if (conversion != null) {
            videoConversionService.deleteVideoConversion(conversion);
        }

        return null;
    }

    private void authorizeVideoConversion() {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        User user = securityService.getCurrentUser(request);
        if (!user.isVideoConversionRole()) {
            LOG.warn("User " + user.getUsername() + " is not allowed to convert videos.");
            throw new RuntimeException("User " + user.getUsername() + " is not allowed to convert videos.");
        }
    }

    private void authorizeAudioConversion() {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        User user = securityService.getCurrentUser(request);
        if (!user.isAudioConversionRole()) {
            LOG.warn("User " + user.getUsername() + " is not allowed to convert audio.");
            throw new RuntimeException("User " + user.getUsername() + " is not allowed to convert audio.");
        }
    }    
		
    private void log(String function, long startTimeNano) {
        long millis = (System.nanoTime() - startTimeNano) / 1000000L;

        // Log queries that take more than 3 seconds.
        if (millis > TimeUnit.SECONDS.toMillis(1L)) {
            LOG.debug("fn::" + function + " took: " + millis + " ms " );
        }
    }    
	
}