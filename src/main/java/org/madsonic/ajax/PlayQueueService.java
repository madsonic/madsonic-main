/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2017 (C) Martin Karel
 *  
 */
package org.madsonic.ajax;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.directwebremoting.WebContextFactory;
import org.madsonic.dao.BookmarkDao;
import org.madsonic.dao.MediaFileDao;
import org.madsonic.dao.PlayQueueDao;
import org.madsonic.domain.Bookmark;
import org.madsonic.domain.GenreSearchCriteria;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MoodsSearchCriteria;
import org.madsonic.domain.MultiSearchCriteria;
import org.madsonic.domain.PlayQueue;
import org.madsonic.domain.Player;
import org.madsonic.domain.Playlist;
import org.madsonic.domain.PodcastEpisode;
import org.madsonic.domain.PodcastStatus;
import org.madsonic.domain.SavedPlayQueue;
import org.madsonic.domain.UrlRedirectType;
import org.madsonic.domain.User;
import org.madsonic.service.AudioAdService;
import org.madsonic.service.JukeboxService;
import org.madsonic.service.LastFMService;
import org.madsonic.service.LastFmServiceBasic;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.NodeService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.PodcastService;
import org.madsonic.service.RatingService;
import org.madsonic.service.SearchService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.TranscodingService;
import org.madsonic.util.StringUtil;

import org.springframework.web.servlet.support.RequestContextUtils;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * Provides AJAX-enabled services for manipulating the play queue of a player.
 * This class is used by the DWR framework (http://getahead.ltd.uk/dwr/).
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PlayQueueService {

	private final PlayerService playerService;
	private final JukeboxService jukeboxService;
	private final NodeService nodeService;
	private final TranscodingService transcodingService;
	private final SettingsService settingsService;
	private final MediaFileService mediaFileService;
	private final SecurityService securityService;
	private final MediaFileDao mediaFileDao;
	private final BookmarkDao bookmarkDao;
	
	private final PodcastService podcastService;
	private final SearchService searchService;
	private final LastFMService lastFMService;   
	private final LastFmServiceBasic lastFmServiceBasic;
	
	@SuppressWarnings("unused")
	private final RatingService ratingService;
	private final AudioAdService audioAdService;

	private final org.madsonic.service.PlaylistService playlistService;
	private final PlayQueueDao playQueueDao;

    public PlayQueueService(PlayerService playerService, JukeboxService jukeboxService, NodeService nodeService, TranscodingService transcodingService,
                            SettingsService settingsService, MediaFileService mediaFileService, SecurityService securityService, MediaFileDao mediaFileDao,
                            BookmarkDao bookmarkDao, SearchService searchService, LastFMService lastFMService, LastFmServiceBasic lastFmServiceBasic, 
                             RatingService ratingService, AudioAdService audioAdService, PodcastService podcastService, org.madsonic.service.PlaylistService playlistService, PlayQueueDao playQueueDao) {
        this.playerService = playerService;
        this.jukeboxService = jukeboxService;
        this.nodeService = nodeService;
        this.transcodingService = transcodingService;
        this.settingsService = settingsService;
        this.mediaFileService = mediaFileService;
        this.securityService = securityService;
        this.mediaFileDao = mediaFileDao;
        this.bookmarkDao = bookmarkDao;
        this.searchService = searchService;
        this.lastFMService = lastFMService;
        this.lastFmServiceBasic = lastFmServiceBasic;
        this.ratingService = ratingService;
        this.audioAdService = audioAdService;
        this.podcastService = podcastService;
        this.playlistService = playlistService;
        this.playQueueDao = playQueueDao;
    }


	/**
	 * Returns the play queue for the player of the current user.
	 *
	 * @return The play queue.
	 */
	public PlayQueueInfo getPlayQueue() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		return convert(request, player, false);
	}

	public PlayQueueInfo start() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doStart(request, response);
	}

	public PlayQueueInfo doStart(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().setStatus(PlayQueue.Status.PLAYING);
		return convert(request, player, true);
	}

	public PlayQueueInfo stop() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doStop(request, response);
	}

	public PlayQueueInfo doStop(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().setStatus(PlayQueue.Status.STOPPED);
		return convert(request, player, true);
	}

	public PlayQueueInfo skip(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doSkip(request, response, index, 0);
	}

	public PlayQueueInfo doSkip(HttpServletRequest request, HttpServletResponse response, int index, int offset) throws Exception {
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().setIndex(index);
		boolean serverSidePlaylist = !player.isExternalWithPlaylist();
		return convert(request, player, serverSidePlaylist, offset);
	}

	public void savePlayQueue(int currentSongIndex, long positionMillis) {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

		Player player = getCurrentPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		PlayQueue playQueue = player.getPlayQueue();
		List<Integer> ids = MediaFile.toIdList(playQueue.getFiles());

		Integer currentId = currentSongIndex == -1 ? null : playQueue.getFile(currentSongIndex).getId();
		SavedPlayQueue savedPlayQueue = new SavedPlayQueue(null, username, ids, currentId, positionMillis, new Date(), "Madsonic");
		playQueueDao.savePlayQueue(savedPlayQueue);
	}

	public PlayQueueInfo loadPlayQueue() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		SavedPlayQueue savedPlayQueue = playQueueDao.getPlayQueue(username);
		if (savedPlayQueue == null) {
			return convert(request, player, false);
		}
		PlayQueue playQueue = player.getPlayQueue();
		playQueue.clear();
		for (Integer mediaFileId : savedPlayQueue.getMediaFileIds()) {
			MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
			if (mediaFile != null) {
				playQueue.addFiles(true, mediaFile);
			}
		}
		PlayQueueInfo result = convert(request, player, false);

		Integer currentId = savedPlayQueue.getCurrentMediaFileId();
		int currentIndex = -1;
		long positionMillis = savedPlayQueue.getPositionMillis() == null ? 0L : savedPlayQueue.getPositionMillis();
		if (currentId != null) {
			MediaFile current = mediaFileService.getMediaFile(currentId);
			currentIndex = playQueue.getFiles().indexOf(current);
			if (currentIndex != -1) {
				result.setStartPlayerAt(currentIndex);
				result.setStartPlayerAtPosition(positionMillis);
			}
		}

		boolean serverSidePlaylist = !player.isExternalWithPlaylist();
		if (serverSidePlaylist && currentIndex != -1) {
			doSkip(request, response, currentIndex, (int) (positionMillis / 1000L));
		}

		return result;
	}

    public PlayQueueInfo createBookmark(int index, long position) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		String username = securityService.getCurrentUsername(request);
		Player player = getCurrentPlayer(request, response);
		PlayQueue playQueue = player.getPlayQueue();
		Integer currentId = index == -1 ? null : playQueue.getFile(index).getId();
		Date now = new Date();
		
		//TODO: Build better comment
		String comment = ""; // playQueue.getFile(index).getArtist() + " - " + playQueue.getFile(index).getTitle();
		
		Bookmark bookmark = new Bookmark(0, currentId, position, username, comment, now, now);
		bookmarkDao.createOrUpdateBookmark(bookmark);
		return convert(request, player, true);
    }

    public void deleteBookmarkid(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);    	
        bookmarkDao.deleteBookmark(username, index);
    }
    
    public PlayQueueInfo deleteBookmark(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
        String username = securityService.getCurrentUsername(request);
        Player player = getCurrentPlayer(request, response);
        PlayQueue playQueue = player.getPlayQueue();
        bookmarkDao.deleteBookmark(username, playQueue.getFile(index).getId());
		return convert(request, player, true);
    }
	
	public PlayQueueInfo play(int id) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		MediaFile file = mediaFileService.getMediaFile(id);
		if (file.isFile()) {
			boolean queueFollowingSongs = false; 
			List<MediaFile> songs;
			if (queueFollowingSongs) {
				MediaFile dir = mediaFileService.getParentOf(file);
				songs = mediaFileService.getChildrenOf(dir, true, false, true);
				if (!songs.isEmpty()) {
					int index = songs.indexOf(file);
					songs = songs.subList(index, songs.size());
				}
			} else {
				songs = Arrays.asList(file);
			}
			return doPlay(request, player, songs).setStartPlayerAt(0);
		} else {
			List<MediaFile> songs = mediaFileService.getDescendantsOf(file, true);
			return doPlay(request, player, songs).setStartPlayerAt(0);
		}
	}

	public PlayQueueInfo playAll(int [] ids) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		List<MediaFile> files = new ArrayList<MediaFile>();
		for (int id : ids) {
			MediaFile file = mediaFileService.getMediaFile(id);
			files.addAll(mediaFileService.getDescendantsOf(file, true));
		}
		return doPlay(request, player, files).setStartPlayerAt(0);
	}	

	public PlayQueueInfo addAll(int [] ids) throws Exception {
		return addSelected(ids);
	}		

	public PlayQueueInfo playMore(int id) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		MediaFile file = mediaFileService.getMediaFile(id);
		if (file.isFile()) {
			boolean queueFollowingSongs = true; 
			List<MediaFile> songs;
			if (queueFollowingSongs) {
				MediaFile dir = mediaFileService.getParentOf(file);
				songs = mediaFileService.getChildrenOf(dir, true, false, true);
				if (!songs.isEmpty()) {
					int index = songs.indexOf(file);
					songs = songs.subList(index, songs.size());
				}
			} else {
				songs = Arrays.asList(file);
			}
			return doPlay(request, player, songs).setStartPlayerAt(0);
		} else {
			List<MediaFile> songs = mediaFileService.getDescendantsOf(file, true);
			return doPlay(request, player, songs).setStartPlayerAt(0);
		}
	}

	public PlayQueueInfo AddPlaylist(int id) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		List<MediaFile> playlistFiles = playlistService.getFilesInPlaylist(id);
		int[] files = new int[playlistFiles.size()];
		for (int i=0; i < files.length; i++)
		{
			files[i] = playlistFiles.get(i).getId();
		}
		return doAdd(request, response, files, null);
	}

	public PlayQueueInfo playPlaylist(int id, boolean random) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		List<MediaFile> files = playlistService.getFilesInPlaylist(id);
		Player player = getCurrentPlayer(request, response);
		if (player.isWeb()) {
			removeVideoFiles(files);
		}

		player.getPlayQueue().addFiles(false, files);
		player.getPlayQueue().setRandomSearchCriteria(null);
		if (random){
			player.getPlayQueue().shuffle();
		}
		return convert(request, player, true).setStartPlayerAt(0);
	}

	private PlayQueueInfo doPlay(HttpServletRequest request, Player player, List<MediaFile> files) throws Exception {
		if (player.isWeb()) {
			removeVideoFiles(files);
		}
        player.getPlayQueue().addFiles(false, addAds(files));
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true);
	}
	
    public PlayQueueInfo playPodcastChannel(int id) throws Exception {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

        List<PodcastEpisode> episodes = podcastService.getEpisodes(id);
        List<MediaFile> files = new ArrayList<MediaFile>();
        for (PodcastEpisode episode : episodes) {
            if (episode.getStatus() == PodcastStatus.COMPLETED) {
                MediaFile mediaFile = mediaFileService.getMediaFile(episode.getMediaFileId());
                if (mediaFile != null && mediaFile.isPresent()) {
                    files.add(mediaFile);
                }
            }
        }
        Player player = getCurrentPlayer(request, response);
        return doPlay(request, player, files).setStartPlayerAt(0);
    }

    public PlayQueueInfo addPodcastChannel(int id) throws Exception {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

        List<PodcastEpisode> episodes = podcastService.getEpisodes(id);
        List<MediaFile> files = new ArrayList<MediaFile>();
        for (PodcastEpisode episode : episodes) {
            if (episode.getStatus() == PodcastStatus.COMPLETED) {
                MediaFile mediaFile = mediaFileService.getMediaFile(episode.getMediaFileId());
                if (mediaFile != null && mediaFile.isPresent()) {
                    files.add(mediaFile);
                }
            }
        }
        Player player = getCurrentPlayer(request, response);
        player.getPlayQueue().addFiles(true, files);
		return convert(request, player, false);
    }    
    
    public PlayQueueInfo playPodcastEpisode(int id) throws Exception {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
        PodcastEpisode episode = podcastService.getEpisode(id, false);
        List<PodcastEpisode> allEpisodes = podcastService.getEpisodes(episode.getChannelId());
        List<MediaFile> files = new ArrayList<MediaFile>();

        boolean queueFollowingSongs = false; 

        for (PodcastEpisode ep : allEpisodes) {
            if (ep.getStatus() == PodcastStatus.COMPLETED) {
                MediaFile mediaFile = mediaFileService.getMediaFile(ep.getMediaFileId());
                if (mediaFile != null && mediaFile.isPresent() &&
                    (ep.getId().equals(episode.getId()) || queueFollowingSongs && !files.isEmpty())) {
                    files.add(mediaFile);
                }
            }
        }
        Player player = getCurrentPlayer(request, response);
        return doPlay(request, player, files).setStartPlayerAt(0);
    }

    public PlayQueueInfo playNewestPodcastEpisode(Integer index) throws Exception {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

        List<PodcastEpisode> episodes = podcastService.getNewestEpisodes(10);
        List<MediaFile> files = Lists.transform(episodes, new Function<PodcastEpisode, MediaFile>() {
            @Override
            public MediaFile apply(PodcastEpisode episode) {
                return mediaFileService.getMediaFile(episode.getMediaFileId());
            }
        });

        boolean queueFollowingSongs = false; 

        if (!files.isEmpty() && index != null) {
            if (queueFollowingSongs) {
                files = files.subList(index, files.size());
            } else {
                files = Arrays.asList(files.get(index));
            }
        }

        Player player = getCurrentPlayer(request, response);
        return doPlay(request, player, files).setStartPlayerAt(0);
    }
	
	public PlayQueueInfo playFollowMeRadio() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		int userGroupId = securityService.getCurrentUserGroupId(request);
		Player player = getCurrentPlayer(request, response);
		List<String> moods = new ArrayList<String>();
		List<String> genres = new ArrayList<String>();
		List<String> artists = new ArrayList<String>();
		List<String> albums = new ArrayList<String>();

		List<MediaFile> mediaFiles = player.getPlayQueue().getFiles();

		for (MediaFile mediaFile : mediaFiles) {
			if(mediaFile.getMood() != null && !moods.contains(mediaFile.getMood())) {
				moods.add(mediaFile.getMood());
			}
			if(mediaFile.getGenre() != null && !genres.contains(mediaFile.getGenre())) {
				genres.add(mediaFile.getGenre());
			}
			if(mediaFile.getArtist() != null && !artists.contains(mediaFile.getArtist())) {
				artists.add(mediaFile.getArtist());
			}
			if(mediaFile.getAlbumName() != null && !albums.contains(mediaFile.getAlbumName())) {
				albums.add(mediaFile.getAlbumName());
			}
		}
		String[] moodsArr = new String[moods.size()];
		String[] genreArr = new String[genres.size()];
		String[] artistsArr = new String[artists.size()];
		String[] albumsArr = new String[albums.size()];

		moodsArr = moods.toArray(moodsArr);
		genreArr = genres.toArray(genreArr);        
		artistsArr = artists.toArray(artistsArr);
		albumsArr = albums.toArray(albumsArr);        

		List<MediaFile> result = new ArrayList<MediaFile>();

		// RESULT: ALBUM
		MultiSearchCriteria criteria1 = new MultiSearchCriteria (
				settingsService.getPandoraResultAlbum(), artistsArr, albumsArr, null, null, null, null, null, userGroupId);
		result.addAll(searchService.getRandomSongs(criteria1)); 

		// RESULT: ARTIST
		MultiSearchCriteria criteria2 = new MultiSearchCriteria (
				settingsService.getPandoraResultArtist(), artistsArr, null, null, null, null, null, null, userGroupId);
		result.addAll(searchService.getRandomSongs(criteria2)); 

		// RESULT: GENRE				
		GenreSearchCriteria criteria3 = new GenreSearchCriteria (
				settingsService.getPandoraResultGenre(), null, genreArr, null, null, null, userGroupId);
		result.addAll(searchService.getRandomSongs(criteria3)); 

		// RESULT: MOODS
		MoodsSearchCriteria criteria4 = new MoodsSearchCriteria (
				settingsService.getPandoraResultMood(), null, moodsArr, null, null, null, userGroupId);
		result.addAll(searchService.getRandomSongs(criteria4)); 

		// RESULT: OTHER
		List<String> allArtists = new ArrayList<String>();
		List<String> similar = new ArrayList<String>();

		for (String _artist : artistsArr) {
			similar = lastFMService.getSimilarArtist(_artist) ;
			allArtists.addAll(similar);
			if (allArtists.size() > 10) {
				break;
			}
		} 
		String[] array = allArtists.toArray(new String[allArtists.size()]);

		//RESULT: RANDOM
		MultiSearchCriteria criteria5 = new MultiSearchCriteria (settingsService.getPandoraResultSimilar(), array, null, null, null, null, null, null, userGroupId);
		result.addAll(searchService.getRandomSongs(criteria5));

		//RESULT: ARTIST TOPTRACKS
		List<MediaFile> resultTopTrack = new ArrayList<MediaFile>();

		if ( mediaFileDao.getTopTracks(artistsArr[0], userGroupId).size() == 0) {
			resultTopTrack = mediaFileService.getTopTracks(20, artistsArr[0], userGroupId);
		}

		if (artistsArr.length > 1) {
			for (String _artist : artistsArr) {
				resultTopTrack = mediaFileDao.getTopTracks(_artist, userGroupId);
				if (resultTopTrack.size() > 0) {
					for(int i = 0; i < settingsService.getPandoraResultArtistTopTrack(); i++) {
						Random myRandomizer = new Random();
						MediaFile randomTopTrack = resultTopTrack.get(myRandomizer.nextInt(resultTopTrack.size()));
						result.add(randomTopTrack);
					}
				}
			}			
		}

		if (artistsArr.length == 1) {		
			resultTopTrack = mediaFileDao.getTopTracks(artistsArr[0], userGroupId);
			if (resultTopTrack.size() > 0) {
				for(int i = 0; i < settingsService.getPandoraResultArtistTopTrack(); i++) {
					Random myRandomizer = new Random();
					MediaFile randomTopTrack = resultTopTrack.get(myRandomizer.nextInt(resultTopTrack.size()));
					result.add(randomTopTrack);
				}
			}
		}

		MoodsSearchCriteria criteria6 = new MoodsSearchCriteria (settingsService.getPandoraResultSimilar(), null, moodsArr, null, null, null, userGroupId);
		result.addAll(searchService.getRandomSongs(criteria6));

		// Filter out duplicates
		HashSet<MediaFile> hs = new HashSet<MediaFile>();
		hs.addAll(result);
		result.clear();
		result.addAll(hs);
		player.getPlayQueue().addFiles(false, result);
		player.getPlayQueue().setRandomSearchCriteria(null);
		player.getPlayQueue().shuffle();
		return convert(request, player, true).setStartPlayerAt(0);
	}

	public PlayQueueInfo playRandomRadio(int count, String genre, String mood, String year, Integer musicFolderId) throws Exception {

		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		int userGroupId = securityService.getCurrentUserGroupId(request);
		String [] _genre = null;
		if (!StringUtils.equalsIgnoreCase("any", genre)) {
			_genre = new String[]{genre};
		} else {
			_genre = null;
		}
		String [] _mood = null;
		if (!StringUtils.equalsIgnoreCase("any", mood)) {
			_mood = new String[]{mood};
		} else {
			_mood = null;
		}
		Integer fromYear = null;
		Integer toYear = null;
		if (!StringUtils.equalsIgnoreCase("any", year)) {
			String[] tmp = StringUtils.split(year);
			fromYear = Integer.parseInt(tmp[0]);
			toYear = Integer.parseInt(tmp[1]);
		}
		if (musicFolderId == -1) {
			musicFolderId = null;
		}

		MultiSearchCriteria criteria = new MultiSearchCriteria (count, null, null, _genre, _mood, fromYear, toYear, musicFolderId, userGroupId);
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().addFiles(false, searchService.getRandomSongs(criteria));
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true).setStartPlayerAt(0);
	}


	public PlayQueueInfo playGenreRadio(String[] tags, int count, String year) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

		int userGroupId = securityService.getCurrentUserGroupId(request);  
		Player player = getCurrentPlayer(request, response);
		if (count > 501 || count == 0) { count = 10; }
		Integer fromYear = null;
		Integer toYear = null;
		if (year != null) {
			if (!StringUtils.equalsIgnoreCase("any", year)) {
				String[] tmp = StringUtils.split(year);
				fromYear = Integer.parseInt(tmp[0]);
				toYear = Integer.parseInt(tmp[1]);
			}
		}

		GenreSearchCriteria criteria = new GenreSearchCriteria (count, null, tags, fromYear, toYear, null, userGroupId);
		player.getPlayQueue().addFiles(false, searchService.getRandomSongs(criteria));
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true).setStartPlayerAt(0);
	}

	public PlayQueueInfo playMoodRadio(String[] tags, int count, String year) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

		int userGroupId = securityService.getCurrentUserGroupId(request);  

		Player player = getCurrentPlayer(request, response);
		if (count > 501 || count == 0) { count = 20; }

		Integer fromYear = null;
		Integer toYear = null;

		if (year != null) {
			if (!StringUtils.equalsIgnoreCase("any", year)) {
				String[] tmp = StringUtils.split(year);
				fromYear = Integer.parseInt(tmp[0]);
				toYear = Integer.parseInt(tmp[1]);
			}
		}
		MoodsSearchCriteria criteria = new MoodsSearchCriteria (count, null, tags, fromYear, toYear, null, userGroupId);
		player.getPlayQueue().addFiles(false, searchService.getRandomSongs(criteria));
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true).setStartPlayerAt(0);
	}

	public PlayQueueInfo playRandomGenre(int id, int count) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

		MediaFile file = mediaFileService.getMediaFile(id);
		List<MediaFile> randomFiles = getRandomChildren(file, count);
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().addFiles(false, randomFiles);
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true).setStartPlayerAt(0);
	}

	public PlayQueueInfo playTopTrack(String artist) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

		int userGroupId = securityService.getCurrentUserGroupId(request); 
		List<MediaFile> topTrackSongs = new ArrayList<MediaFile>();

		topTrackSongs = mediaFileService.getTopTracks(artist, userGroupId);
		if (topTrackSongs.size() < 1) {
			lastFMService.updateTopTrackArtist(artist);
			lastFMService.updateTopTrackEntries(artist, settingsService.getLastFMResultSize(), userGroupId); 
			topTrackSongs = mediaFileService.getTopTracks(artist, userGroupId);
		}

		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().addFiles(false, topTrackSongs);
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true).setStartPlayerAt(0);
	}

	public PlayQueueInfo addTopTrack(String artist) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

		int userGroupId = securityService.getCurrentUserGroupId(request); 
		List<MediaFile> topTrackSongs = new ArrayList<MediaFile>();
		topTrackSongs = mediaFileService.getTopTracks(artist, userGroupId); 
		if (topTrackSongs.size() < 1) {
			lastFMService.updateTopTrackArtist(artist);
			lastFMService.updateTopTrackEntries(artist, settingsService.getLastFMResultSize(), userGroupId); 
			topTrackSongs = mediaFileService.getTopTracks(artist, userGroupId);
		}
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().addFiles(true, topTrackSongs);
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true);
	}

	public void updateTopTrack(String artist) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		int userGroupId = securityService.getCurrentUserGroupId(request); 
		int toplastFMfound = lastFMService.updateTopTrackArtist(artist);
		int topPlayedfound = lastFMService.updateTopTrackEntries(artist, settingsService.getLastFMResultSize(), userGroupId).size();
		topPlayedfound = mediaFileService.getTopTracks(artist, userGroupId).size();
		lastFMService.updateTopTrackStats(artist, topPlayedfound, toplastFMfound );   	
	}

	public PlayQueueInfo playRandom(int id, int count) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();

		MediaFile file = mediaFileService.getMediaFile(id);
		List<MediaFile> randomFiles = getRandomChildren(file, count);
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().addFiles(false, randomFiles);
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true);
	}

	public PlayQueueInfo playSimilar(int id, int count) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		MediaFile artist = mediaFileService.getMediaFile(id);
		List<MediaFile> similarSongs = lastFmServiceBasic.getSimilarSongs(artist, count);
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().addFiles(false, similarSongs);
		return convert(request, player, true).setStartPlayerAt(0);
	}

	public PlayQueueInfo playRandomAll(int [] ids, int count) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		List<MediaFile> files = new ArrayList<MediaFile>();
		for (int id : ids) {
			MediaFile file = mediaFileService.getMediaFile(id);
			files.addAll(mediaFileService.getDescendantsOf(file, true));
		}
		List<MediaFile> randomFiles = getRandomChildren(files, count);
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().addFiles(false, randomFiles);
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, true).setStartPlayerAt(0);
	}	

    private List<MediaFile> addAds(List<MediaFile> files) {
        return audioAdService.addAudioAds(files);
    }

	public PlayQueueInfo add(int id) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doAdd(request, response, new int[]{id}, null);
	}

	public PlayQueueInfo addAt(int id, int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doAdd(request, response, new int[]{id}, index);
	}

	public PlayQueueInfo addSelected(int[] ids) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doAdd(request, response, ids, null);
	}

	public PlayQueueInfo addSelectedAt(int[] ids, int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doAdd(request, response, ids, index);
	}    

	public PlayQueueInfo doAdd(HttpServletRequest request, HttpServletResponse response, int[] ids, Integer index) throws Exception {
		Player player = getCurrentPlayer(request, response);
		List<MediaFile> files = new ArrayList<MediaFile>(ids.length);
		for (int id : ids) {
			MediaFile ancestor = mediaFileService.getMediaFile(id);
			files.addAll(mediaFileService.getDescendantsOf(ancestor, true));
		}
		if (player.isWeb()) {
            mediaFileService.removeVideoFiles(files);
		}
        files = addAds(files);
        
		if (index != null) {
			player.getPlayQueue().addFilesAt(files, index);
		} else {
			player.getPlayQueue().addFiles(true, files);
		}
		player.getPlayQueue().setRandomSearchCriteria(null);
		return convert(request, player, false);
	}

	public PlayQueueInfo doSet(HttpServletRequest request, HttpServletResponse response, int[] ids) throws Exception {
		Player player = getCurrentPlayer(request, response);
		PlayQueue playQueue = player.getPlayQueue();
		MediaFile currentFile = playQueue.getCurrentFile();
		PlayQueue.Status status = playQueue.getStatus();

		playQueue.clear();
		PlayQueueInfo result = doAdd(request, response, ids, null);

		int index = currentFile == null ? -1 : playQueue.getFiles().indexOf(currentFile);
		playQueue.setIndex(index);
		playQueue.setStatus(status);
		return result;
	}

	public void doControlNode(HttpServletRequest request, HttpServletResponse response, String url, String command) throws Exception {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet getRequest = new HttpGet(url + "/rest/node.view/" + command);
		getRequest.addHeader("accept", "application/xml");
		httpClient.execute(getRequest);
	}

	public void doConfigNode(HttpServletRequest request, HttpServletResponse response, String url) throws Exception {
		HttpClient httpClient = HttpClientBuilder.create().build();
		String command = "rest/node.view/setCredential";
		String username = User.USERNAME_NODE;
		String password = securityService.getSecureUserToken(User.USERNAME_NODE);
		String host = settingsService.getLocalIpAddress();
		int httpPort = settingsService.getPort();
		int httpsPort = settingsService.getHttpsPort();
		String context = settingsService.getUrlRedirectContextPath().equals("") ? "default" : settingsService.getUrlRedirectContextPath();
		boolean redirected = settingsService.isUrlRedirectionEnabled();
		UrlRedirectType redirectedType = settingsService.getUrlRedirectType();
		String customUrl = settingsService.getUrlRedirectCustomUrl();
		StringBuilder nodeRequest = new StringBuilder();
		if (httpsPort > 0) {
			nodeRequest.append("true");  
			nodeRequest.append("/");  
		} else {
			nodeRequest.append("false");  
			nodeRequest.append("/");  
		}
		if (redirected) {
			if (redirectedType.equals(UrlRedirectType.CUSTOM)) {
				nodeRequest.append(customUrl.replace("http://", "").replace("https://", ""));  
				if (context.equals("default")) {
					if (!customUrl.endsWith("/")) {
						 nodeRequest.append("/");
					}
					nodeRequest.append("default");  
				} 			
			} else {
				nodeRequest.append(settingsService.getUrlRedirectFrom() + ".madsonic.org/default");  
			}
		} else {
			nodeRequest.append(host);
			if (httpsPort > 0) {
				nodeRequest.append(":");  
				nodeRequest.append(httpsPort);  
			} else {
				nodeRequest.append(":");  
				nodeRequest.append(httpPort);  
			}
			if (context.equals("default")) {
				nodeRequest.append("/");  
				nodeRequest.append("default");  
			} else {
				nodeRequest.append("/");  
				nodeRequest.append(context);  				
			}			
		}
		HttpGet getRequest = new HttpGet(url + "/" + command + "/" + username + "/" + password + "/" + nodeRequest.toString());
		getRequest.addHeader("accept", "application/xml");
		httpClient.execute(getRequest);
	}
	
	public void doSendToNode(HttpServletRequest request, HttpServletResponse response, String url) throws Exception {
		Player player = getCurrentPlayer(request, response);
		PlayQueue playQueue = player.getPlayQueue();
		String strIds = MediaFile.toIdList(playQueue.getFiles()).toString();
		strIds = strIds.replace("[", "").replace("]", "").replace(" ", "");
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet getRequest = new HttpGet(url + "/rest/node.view/set/" + strIds);
		getRequest.addHeader("accept", "application/xml");
		httpClient.execute(getRequest);
	}
	
	public PlayQueueInfo clear() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doClear(request, response);
	}

	public PlayQueueInfo doClear(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().clear();
		boolean serverSidePlaylist = !player.isExternalWithPlaylist();
		return convert(request, player, serverSidePlaylist);
	}

	public PlayQueueInfo shuffle() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doShuffle(request, response);
	}

	public PlayQueueInfo doShuffle(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().shuffle();
		return convert(request, player, false);
	}

	public PlayQueueInfo remove(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		return doRemove(request, response, index);
	}

	public PlayQueueInfo toggleStar(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);

		MediaFile file = player.getPlayQueue().getFile(index);
		String username = securityService.getCurrentUsername(request);
		boolean starred = mediaFileDao.getMediaFileStarredDate(file.getId(), username) != null;
		if (starred) {
			mediaFileDao.unstarMediaFile(file.getId(), username);
		} else {
			mediaFileDao.starMediaFile(file.getId(), username);
		}
		return convert(request, player, false);
	}
	
	public PlayQueueInfo toggleLove(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);

		MediaFile file = player.getPlayQueue().getFile(index);
		String username = securityService.getCurrentUsername(request);
		
		boolean loved = mediaFileDao.getMediaFileLovedDate(file.getId(), username) != null;
		if (loved) {
			mediaFileDao.unloveMediaFile(file.getId(), username);
		} else {
			mediaFileDao.loveMediaFile(file.getId(), username);
		}
		return convert(request, player, false);
	}
	

	public PlayQueueInfo doRemove(HttpServletRequest request, HttpServletResponse response, int index) throws Exception {
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().removeFileAt(index);
		return convert(request, player, false);
	}

	public PlayQueueInfo removeMany(int[] indexes) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		for (int i = indexes.length - 1; i >= 0; i--) {
			player.getPlayQueue().removeFileAt(indexes[i]);
		}
		return convert(request, player, false);
	}

	public PlayQueueInfo rearrange(int[] indexes) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().rearrange(indexes);
		return convert(request, player, false);
	}
	
	public PlayQueueInfo up(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().moveUp(index);
		return convert(request, player, false); 
	}

	public PlayQueueInfo down(int index) throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().moveDown(index);
		return convert(request, player, false);
	}

	public PlayQueueInfo toggleRepeat() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().setRepeatEnabled(!player.getPlayQueue().isRepeatEnabled());
		return convert(request, player, false);
	} 

	public PlayQueueInfo toggleLockMode() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);

		int mode = player.getPlayQueue().getLockMode();
		switch(mode){ 
		case  0: mode = 1; break; 
		case  1: mode = 2; break; 
		case  2: mode = 0; break; 
		default: mode = 0; break; 
		} 	
		player.getPlayQueue().setLockMode(mode);
		return convert(request, player, false);
	} 	

	public PlayQueueInfo toggleFollowMe() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().setFollowMeEnabled(!player.getPlayQueue().isFollowMeEnabled());
		return convert(request, player, false);
	}	

	public PlayQueueInfo undo() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().undo();
		boolean serverSidePlaylist = !player.isExternalWithPlaylist();
		return convert(request, player, serverSidePlaylist);
	}

	public PlayQueueInfo sortByRank() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.RANK);
		return convert(request, player, false);
	}

	public PlayQueueInfo sortByTrack() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.TRACK);
		return convert(request, player, false);
	}

	public PlayQueueInfo sortByAlbum() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.ALBUM);
		return convert(request, player, false);
	}

	public PlayQueueInfo sortByArtist() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.ARTIST);
		return convert(request, player, false);
	}

	public PlayQueueInfo sortByGenre() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.GENRE);
		return convert(request, player, false);
	}

	public PlayQueueInfo sortByMood() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.MOOD);
		return convert(request, player, false);
	}

	public PlayQueueInfo sortByBpm() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.BPM);
		return convert(request, player, false);
	}

	public PlayQueueInfo sortByComposer() throws Exception {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		player.getPlayQueue().sort(PlayQueue.SortOrder.COMPOSER);
		return convert(request, player, false);
	}
	
    public void setJukeboxGain(float gain) {
        jukeboxService.setGain(gain);
    }

    public void setJukeboxMute(boolean mute) {
        jukeboxService.setMute(mute);
    }

	public String savePlaylist() {
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
		Player player = getCurrentPlayer(request, response);
		Locale locale = settingsService.getLocale();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);

		Date now = new Date();
		Playlist playlist = new Playlist();
		playlist.setUsername(securityService.getCurrentUsername(request));
		playlist.setCreated(now);
		playlist.setChanged(now);
		playlist.setPublic(false);
		playlist.setName(dateFormat.format(now));

		playlistService.createPlaylist(playlist);
		playlistService.setFilesInPlaylist(playlist.getId(), player.getPlayQueue().getFiles());
		return playlist.getName();
	}

	private List<MediaFile> getRandomChildren(MediaFile file, int count) throws IOException {
		List<MediaFile> children = mediaFileService.getDescendantsOf(file, false);
		removeVideoFiles(children);

		if (children.isEmpty()) {
			return children;
		}
		Collections.shuffle(children);
		return children.subList(0, Math.min(count, children.size()));
	}

	private List<MediaFile> getRandomChildren(List<MediaFile> children, int count) throws IOException {

		removeVideoFiles(children);

		if (children.isEmpty()) {
			return children;
		}
		Collections.shuffle(children);
		return children.subList(0, Math.min(count, children.size()));
	}	

	private void removeVideoFiles(List<MediaFile> files) {
		Iterator<MediaFile> iterator = files.iterator();
		while (iterator.hasNext()) {
			MediaFile file = iterator.next();
			if (file.isVideo()) {
				iterator.remove();
			}
		}
	}

	private PlayQueueInfo convert(HttpServletRequest request, Player player, boolean serverSidePlaylist) throws Exception {
		return convert(request, player, serverSidePlaylist, 0);
	}

	private PlayQueueInfo convert(HttpServletRequest request, Player player, boolean serverSidePlaylist, int offset) throws Exception {
		String url = request.getRequestURL().toString();

		if (serverSidePlaylist && player.isJukebox()) {
			jukeboxService.updateJukebox(player, offset);
		}
		if (serverSidePlaylist && player.isNode()) {
			nodeService.updateNodes(player, offset);
		}		
		
		boolean isCurrentPlayer = player.getIpAddress() != null && player.getIpAddress().equals(request.getRemoteAddr());

		boolean m3uSupported = player.isExternal() || player.isExternalWithPlaylist();
		serverSidePlaylist = player.isAutoControlEnabled() && m3uSupported && isCurrentPlayer && serverSidePlaylist;
		Locale locale = RequestContextUtils.getLocale(request);

		List<PlayQueueInfo.Entry> entries = new ArrayList<PlayQueueInfo.Entry>();
		PlayQueue playQueue = player.getPlayQueue();

		for (MediaFile file : playQueue.getFiles()) {
			MediaFile parent = mediaFileService.getMediaFile(file.getParentPath());
			String albumUrl = url.replaceFirst("/dwr/.*", "/main.view?id=" + parent.getId());
            String streamUrl = url.replaceFirst("/dwr/.*", "/stream?player=" + player.getId() + "&id=" + file.getId() + "&auth=" + file.getHash());
            String coverArtUrl = url.replaceFirst("/dwr/.*", "/coverArt.view?id=" + file.getId() + "&auth=" + file.getHash());

			// Rewrite URLs in case we're behind a proxy.
			if (settingsService.isRewriteUrlEnabled()) {
				String referer = request.getHeader("referer");
				albumUrl = StringUtil.rewriteUrl(albumUrl, referer);
				streamUrl = StringUtil.rewriteUrl(streamUrl, referer);
			}

            String remoteStreamUrl = settingsService.rewriteRemoteUrl(streamUrl);
            String remoteCoverArtUrl = settingsService.rewriteRemoteUrl(coverArtUrl);

			String format = formatFormat(player, file);
			String username = securityService.getCurrentUsername(request);
			boolean starred = mediaFileService.getMediaFileStarredDate(file.getId(), username) != null;
			boolean loved = mediaFileService.getMediaFileLovedDate(file.getId(), username) != null;
			
			int bookmarkPostion = mediaFileService.getMediaFileBookmarkPosition(file.getId(), username);
			
			entries.add(new PlayQueueInfo.Entry(file.getId(), file.getHash(), file.getDiscNumber(), file.getTrackNumber(), file.getTitle(), file.getArtist(),
					file.getComposer(), file.getAlbumName(), file.getGenre(), file.getMood(), file.getYear(), file.getBpm(), formatBitRate(file),
					file.getDurationSeconds(), file.getDurationString(), file.getFormat(), format, formatContentType(format),
					formatFileSize(file.getFileSize(), locale), loved, starred, bookmarkPostion, albumUrl, streamUrl, remoteStreamUrl, coverArtUrl, remoteCoverArtUrl, file.getRank() ));
		}

		boolean isStopEnabled = playQueue.getStatus() == PlayQueue.Status.PLAYING && !player.isExternalWithPlaylist();

		return new PlayQueueInfo(entries, playQueue.getIndex(), isStopEnabled, playQueue.isRepeatEnabled(), playQueue.isFollowMeEnabled(), serverSidePlaylist, jukeboxService.getGain(), jukeboxService.isMute(), playQueue.getLockMode());
	}

	private String formatFileSize(Long fileSize, Locale locale) {
		if (fileSize == null) {
			return null;
		}
		return StringUtil.formatBytes(fileSize, locale);
	}

	private String formatFormat(Player player, MediaFile file) {
		return transcodingService.getSuffix(player, file, null);
	}

	private String formatContentType(String format) {
		return StringUtil.getMimeType(format);
	}

	private String formatBitRate(MediaFile mediaFile) {
		if (mediaFile.getBitRate() == null) {
			return null;
		}
		if (mediaFile.isVariableBitRate()) {
			return mediaFile.getBitRate() + " Kbps vbr";
		}
		return mediaFile.getBitRate() + " Kbps";
	}

	private Player getCurrentPlayer(HttpServletRequest request, HttpServletResponse response) {
		return playerService.getPlayer(request, response);
	}
}