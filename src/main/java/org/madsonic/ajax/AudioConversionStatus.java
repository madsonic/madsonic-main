/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.ajax;

/**
 * @author Sindre Mehus
 * @version $Id$
 */
public class AudioConversionStatus {

    private boolean statusNew;
    private boolean statusInProgress;
    private boolean statusError;
    private boolean statusCompleted;
    private Integer progressSeconds;
    private String progressString;
    private String targetFile;
    private String targetFormat;    
    private String logFile;

    public boolean isStatusNew() {
        return statusNew;
    }

    public void setStatusNew(boolean statusNew) {
        this.statusNew = statusNew;
    }

    public boolean isStatusInProgress() {
        return statusInProgress;
    }

    public void setStatusInProgress(boolean statusInProgress) {
        this.statusInProgress = statusInProgress;
    }

    public boolean isStatusError() {
        return statusError;
    }

    public void setStatusError(boolean statusError) {
        this.statusError = statusError;
    }

    public boolean isStatusCompleted() {
        return statusCompleted;
    }

    public void setStatusCompleted(boolean statusCompleted) {
        this.statusCompleted = statusCompleted;
    }

    public void setProgressSeconds(Integer progressSeconds) {
        this.progressSeconds = progressSeconds;
    }

    public Integer getProgressSeconds() {
        return progressSeconds;
    }

    public void setProgressString(String progressString) {
        this.progressString = progressString;
    }

    public String getProgressString() {
        return progressString;
    }

    public String getTargetFile() {
        return targetFile;
    }

    public void setTargetFile(String targetFile) {
        this.targetFile = targetFile;
    }
    
    public String getTargetFormat() {
        return targetFormat;
    }

    public void setTargetFormat(String targetFormat) {
        this.targetFormat = targetFormat;
    }
    

    public String getLogFile() {
        return logFile;
    }

    public void setLogFile(String logFile) {
        this.logFile = logFile;
    }
}
