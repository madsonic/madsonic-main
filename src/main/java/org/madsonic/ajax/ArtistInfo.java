/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.ajax;

import java.util.List;

import org.madsonic.domain.ArtistBio;

/**
 * @author Sindre Mehus, Martin Karel
 */
public class ArtistInfo {

    private final List<SimilarArtist> similarArtists;
    private final List<TopAlbum> topAlbums;
    private final List<TopTrack> topTracks;    
    private final ArtistBio artistBio;

    public ArtistInfo(List<SimilarArtist> similarArtists, List<TopAlbum> topAlbums, List<TopTrack> topTracks, ArtistBio artistBio) {
        this.similarArtists = similarArtists;
        this.topAlbums = topAlbums;
        this.topTracks = topTracks;          
        this.artistBio = artistBio;
    }

    public List<SimilarArtist> getSimilarArtists() {
        return similarArtists;
    }

    public ArtistBio getArtistBio() {
        return artistBio;
    }

	public List<TopAlbum> getTopAlbums() {
		return topAlbums;
	}

	public List<TopTrack> getTopTracks() {
		return topTracks;
	}
}
