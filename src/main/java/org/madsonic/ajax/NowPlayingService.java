/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.ajax;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.madsonic.Logger;
import org.madsonic.domain.AvatarScheme;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.PlayStatus;
import org.madsonic.domain.Player;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.MediaScannerService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.StatusService;
import org.madsonic.service.metadata.MetaData;
import org.madsonic.service.metadata.MetaDataParser;
import org.madsonic.service.metadata.MetaDataParserFactory;
import org.madsonic.util.StringUtil;

/**
 * Provides AJAX-enabled services for retrieving the currently playing file and directory.
 * This class is used by the DWR framework (http://getahead.ltd.uk/dwr/).
 *
 * @author Sindre Mehus, Martin Karel
 */
public class NowPlayingService {

    private static final Logger LOG = Logger.getLogger(NowPlayingService.class);

    private final PlayerService playerService;
    private final StatusService statusService;
    private final SettingsService settingsService;
    private final SecurityService securityService;    
    private final MediaScannerService mediaScannerService;
    private MediaFileService mediaFileService;
    private MetaDataParserFactory metaDataParserFactory;

    public NowPlayingService(PlayerService playerService, StatusService statusService, SettingsService settingsService, 
    		SecurityService securityService,  MediaScannerService mediaScannerService, MediaFileService mediaFileService, 
    		MetaDataParserFactory metaDataParserFactory) {
        this.playerService = playerService;
        this.statusService = statusService;
        this.settingsService = settingsService;
        this.securityService = securityService;
        this.mediaScannerService = mediaScannerService;
        this.mediaFileService = mediaFileService;
        this.metaDataParserFactory = metaDataParserFactory;
    }

    /**
     * Returns details about what the current player is playing.
     *
     * @return Details about what the current player is playing, or <code>null</code> if not playing anything.
     */
    public NowPlayingInfo getNowPlayingForCurrentPlayer() throws Exception {
        WebContext webContext = WebContextFactory.get();
        Player player = playerService.getPlayer(webContext.getHttpServletRequest(), webContext.getHttpServletResponse());

        for (NowPlayingInfo info : getNowPlaying()) {
            if (player.getId().equals(info.getPlayerId())) {
                return info;
            }
        }
        return null;
    }

    /**
     * Returns details about what all users are currently playing.
     *
     * @return Details about what all users are currently playing.
     */
    public List<NowPlayingInfo> getNowPlaying() throws Exception {
        try {
            return convert(statusService.getPlayStatuses());
        } catch (NullPointerException ex) {
            return Collections.emptyList();
        } catch (Throwable x) {
            LOG.error("Unexpected error in getNowPlaying: " + x, x);
            return Collections.emptyList();
        }
    }

    public List<NowPlayingInfo> getNowListening() throws Exception {
        try {
    		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
            return convert(statusService.getListeningStatuses(securityService.getCurrentUsername(request)));
        } catch (NullPointerException ex) {
            return Collections.emptyList();
        } catch (Throwable x) {
            LOG.error("Unexpected error in getNowListening: " + x, x);
            return Collections.emptyList();
        }
    }    
    
    /**
     * Returns media folder scanning status.
     */
    public ScanInfo getScanningStatus() {
        return new ScanInfo(mediaScannerService.isScanning(), mediaScannerService.getScanCount());
    }

    private List<NowPlayingInfo> convert(List<PlayStatus> playStatuses) {
    	
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String url = request.getRequestURL().toString();
        
        List<NowPlayingInfo> result = new ArrayList<NowPlayingInfo>();
        for (PlayStatus status : playStatuses) {

            Player player = status.getPlayer();
            MediaFile mediaFile = status.getMediaFile();
            String username = player.getUsername();
            if (username == null) {
                continue;
            }
                UserSettings userSettings = settingsService.getUserSettings(username);
                if (!userSettings.isNowPlayingAllowed()) {
                    continue;
                }
                File coverArt = mediaFileService.getCoverArt(mediaFile);

                String artist = mediaFile.getArtist();
                String title = mediaFile.getTitle();
                
                String streamUrl = url.replaceFirst("/dwr/.*", "/stream?player=" + player.getId() + "&id=" + mediaFile.getId() + "&auth=" + mediaFile.getHash());
                String albumUrl = url.replaceFirst("/dwr/.*", "/main.view?id=" + mediaFile.getId());
                
                MetaDataParser parser = metaDataParserFactory.getParser(mediaFile.getFile());
                MetaData metaData = parser.getMetaData(mediaFile.getFile());

            	String lyricsUrl;
            	if (metaData.hasLyrics()) {
            		lyricsUrl = "lyrics.view?id=" + mediaFile.getId();
            	} else {
                    lyricsUrl = url.replaceFirst("/dwr/.*", "/lyrics.view?artistUtf8Hex=" + StringUtil.utf8HexEncode(artist) + "&songUtf8Hex=" + StringUtil.utf8HexEncode(title));
            	}
           	
                String coverArtUrl = url.replaceFirst("/dwr/.*", "/coverArt.view?size=50&id=" + mediaFile.getId() + "&auth=" + mediaFile.getHash());
                String coverArtZoomUrl = url.replaceFirst("/dwr/.*", "/coverArt.view?id=" + mediaFile.getId() + "&auth=" + mediaFile.getHash());
                String avatarUrl = null;
                
                if (userSettings.getAvatarScheme() == AvatarScheme.SYSTEM) {
                    avatarUrl = url.replaceFirst("/dwr/.*", "/avatar.view?id=" + userSettings.getSystemAvatarId());
                } else if (userSettings.getAvatarScheme() == AvatarScheme.CUSTOM && settingsService.getCustomAvatar(username) != null) {
                    avatarUrl = url.replaceFirst("/dwr/.*", "/avatar.view?usernameUtf8Hex=" + StringUtil.utf8HexEncode(username));
                }

                // Rewrite URLs in case we're behind a proxy.
                if (settingsService.isRewriteUrlEnabled()) {
                    String referer = request.getHeader("referer");
                    streamUrl = StringUtil.rewriteUrl(streamUrl, referer);
                    albumUrl = StringUtil.rewriteUrl(albumUrl, referer);
                    lyricsUrl = StringUtil.rewriteUrl(lyricsUrl, referer);
                    coverArtUrl = StringUtil.rewriteUrl(coverArtUrl, referer);
                    coverArtZoomUrl = StringUtil.rewriteUrl(coverArtZoomUrl, referer);
                    avatarUrl = StringUtil.rewriteUrl(avatarUrl, referer);
                }

            String tooltip = StringUtil.toHtml(artist) + " &ndash; " + StringUtil.toHtml(title);

            if (StringUtils.isNotBlank(player.getName())) {
                username += "@" + player.getName();
            }
            artist = StringUtil.toHtml(StringUtils.abbreviate(artist, 25));
            title = StringUtil.toHtml(StringUtils.abbreviate(title, 35));
            username = StringUtil.toHtml(StringUtils.abbreviate(username, 25));

            long minutesAgo = status.getMinutesAgo();
            
            int mediaId = mediaFile.getId();
            
            mediaFileService.populateStarredDate(mediaFile, player.getUsername());
            mediaFileService.populateLovedDate(mediaFile, player.getUsername());   
            mediaFileService.populateBookmarkPosition(mediaFile, player.getUsername());
            
            boolean starred = mediaFile.getStarredDate() != null ? true : false;
            boolean loved = mediaFile.getLovedDate() != null ? true : false;
            boolean bookmarked = mediaFile.getBookmarkPosition() > 0 ? true : false;
            
            String year = mediaFile.getYear() != null ? mediaFile.getYear().toString() : "null";
            
            if (minutesAgo < 120) {
                result.add(new NowPlayingInfo(player.getId(),username, artist, title, year, mediaFile.getDurationString(), tooltip, streamUrl, albumUrl, lyricsUrl, coverArtUrl, coverArtZoomUrl, avatarUrl, (int) minutesAgo, mediaId, starred, loved, bookmarked));
                }
            }

        return result;

    }
}
