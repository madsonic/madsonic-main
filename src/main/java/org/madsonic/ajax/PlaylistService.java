/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.ajax;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.dao.MediaFileDao;
import org.madsonic.service.SettingsService;
import org.directwebremoting.WebContextFactory;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.Player;
import org.madsonic.domain.Playlist;
import org.madsonic.i18n.MadsonicLocaleResolver;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;

/**
 * Provides AJAX-enabled services for manipulating playlists.
 * This class is used by the DWR framework (http://getahead.ltd.uk/dwr/).
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PlaylistService {

    private final MediaFileService mediaFileService;
    private final SecurityService securityService;
    private final org.madsonic.service.PlaylistService playlistService;
    private final MediaFileDao mediaFileDao;
    private final SettingsService settingsService;
    private final PlayerService playerService;
    private final MadsonicLocaleResolver localeResolver;

    public PlaylistService(MediaFileService mediaFileService, SecurityService securityService,
                           org.madsonic.service.PlaylistService playlistService, MediaFileDao mediaFileDao,
                           SettingsService settingsService, PlayerService playerService, MadsonicLocaleResolver localeResolver) {
        this.mediaFileService = mediaFileService;
        this.securityService = securityService;
        this.playlistService = playlistService;
        this.mediaFileDao = mediaFileDao;
        this.settingsService = settingsService;
        this.playerService = playerService;
        this.localeResolver = localeResolver;
    }
    public List<Playlist> getReadablePlaylists() {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);
        return playlistService.getReadablePlaylistsForUser(username, "name", "asc");
    }

    //TODO:CLEANUP --> name asc
    
    public List<Playlist> getWritablePlaylists() {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);
        return playlistService.getWritablePlaylistsForUser(username, "name", "asc");
    }

    public PlaylistInfo getPlaylist(int id) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();

        Playlist playlist = playlistService.getPlaylist(id);
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);

        String username = securityService.getCurrentUsername(request);
        mediaFileService.populateStarredDate(files, username);
        mediaFileService.populateLovedDate(files, username);
        
        return new PlaylistInfo(playlist, createEntries(files));
    }

    public List<Playlist> createNamedPlaylist(String playlistName, String playlistComment, boolean isPublic) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();

        Date now = new Date();
        Playlist playlist = new Playlist();
        playlist.setUsername(securityService.getCurrentUsername(request));
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(isPublic);
        playlist.setComment(playlistComment);
        playlist.setName(playlistName);
        if (isPublic){playlist.setShareLevel(1);}
        playlistService.createPlaylist(playlist);
        return getReadablePlaylists();
    }

    public int createPlaylistForMain(List<Integer> mediaFileIds, String PlaylistName) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        Locale locale = localeResolver.resolveLocale(request);
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);
        Date now = new Date();
        Playlist playlist = new Playlist();
        playlist.setUsername(securityService.getCurrentUsername(request));
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(false);
        if (PlaylistName == null) {
            playlist.setName(dateFormat.format(now));
        } else {
        	playlist.setName(PlaylistName);
        }
        playlistService.createPlaylist(playlist);
        appendToPlaylist(playlist.getId(), mediaFileIds);

        return playlist.getId();
    }
    
    public int createPlaylistForPlayQueue() {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        HttpServletResponse response = WebContextFactory.get().getHttpServletResponse();
        Player player = playerService.getPlayer(request, response);
        Locale locale = localeResolver.resolveLocale(request);
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);        
        Date now = new Date();
        Playlist playlist = new Playlist();
        playlist.setUsername(securityService.getCurrentUsername(request));
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(false);
        playlist.setShareLevel(0);      
        
        String artist = "";
        Set<String> artists = new HashSet<String>();
        for (MediaFile child : player.getPlayQueue().getFiles()) {
            if (child.getArtist() != null) {
                artists.add(child.getArtist().toLowerCase());
            }
        }
        if (artists.size() < 2) {
            if (player.getPlayQueue().getFile(0).getArtist() != null) {
            	artist = player.getPlayQueue().getFile(0).getArtist() + " - ";
                playlist.setName(artist + securityService.getCurrentUsername(request) + " - " + dateFormat.format(now));
            }
        } else {
            playlist.setName("Various Artists - " + securityService.getCurrentUsername(request) + " - " + dateFormat.format(now));
        }
        playlistService.createPlaylist(playlist);
        playlistService.setFilesInPlaylist(playlist.getId(), player.getPlayQueue().getFiles());

        return playlist.getId();
    }


    public Playlist createPlaylist() {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        Locale locale = localeResolver.resolveLocale(request);
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);

        Date now = new Date();
        Playlist playlist = new Playlist();
        playlist.setUsername(securityService.getCurrentUsername(request));
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(true);
        playlist.setShareLevel(1);
        playlist.setName(dateFormat.format(now));
        playlistService.createPlaylist(playlist);
        return playlist;
    }
    
    public List<Playlist> createEmptyPlaylist() {
    	return createEmptyPlaylist(null, false);
    }
    
    public List<Playlist> createEmptyPlaylist(String playlistComment, boolean isPublic) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        Locale locale = settingsService.getLocale();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);

        Date now = new Date();
        Playlist playlist = new Playlist();
        playlist.setUsername(securityService.getCurrentUsername(request));
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(isPublic);
        playlist.setComment(playlistComment);
        playlist.setName(dateFormat.format(now));

        if(isPublic){playlist.setShareLevel(1);}
        
        playlistService.createPlaylist(playlist);
        return getReadablePlaylists();
    }

    public void appendToPlaylist(int playlistId, List<Integer> mediaFileIds) {
        List<MediaFile> files = playlistService.getFilesInPlaylist(playlistId, true);
        for (Integer mediaFileId : mediaFileIds) {
            MediaFile file = mediaFileService.getMediaFile(mediaFileId);
            if (file != null) {
                files.add(file);
            }
        }
        playlistService.setFilesInPlaylist(playlistId, files);
    }

    public String savePlaylist(List<Integer> mediaFileIds) {
		
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		
        Locale locale = settingsService.getLocale();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);

        Date now = new Date();
        Playlist playlist = new Playlist();
        playlist.setUsername(securityService.getCurrentUsername(request));
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(false);
        playlist.setName(dateFormat.format(now));

        playlistService.createPlaylist(playlist);
        appendToPlaylist(playlist.getId(), mediaFileIds);
        return playlist.getName();
		
    }

    public String savePlaylist(List<Integer> mediaFileIds, String PlaylistName) {
		
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		
        Date now = new Date();
        Playlist playlist = new Playlist();
        playlist.setUsername(securityService.getCurrentUsername(request));
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(false);
        playlist.setName(PlaylistName);
        playlist.setShareLevel(0);

        playlistService.createPlaylist(playlist);
        appendToPlaylist(playlist.getId(), mediaFileIds);
        return playlist.getName();
    }    

    public String saveStarredPlaylist(List<Integer> mediaFileIds) {
		
		HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
		
        Date now = new Date();
        String username = securityService.getCurrentUsername(request);
        String playlistName = "Starred ";
        Playlist playlist = new Playlist();
        playlist.setUsername(username);
        playlist.setCreated(now);
        playlist.setChanged(now);
        playlist.setPublic(false);
        playlist.setName(playlistName + " - " + username + " - " + now);
        playlist.setShareLevel(0);

        playlistService.createPlaylist(playlist);
        appendToPlaylist(playlist.getId(), mediaFileIds);
        return playlist.getName();
    }    
   
    private List<PlaylistInfo.Entry> createEntries(List<MediaFile> files) {
        List<PlaylistInfo.Entry> result = new ArrayList<PlaylistInfo.Entry>();
        for (MediaFile file : files) {
            result.add(new PlaylistInfo.Entry(file.getId(), file.getRank(), file.getTitle(), file.getArtist(), file.getAlbumName(), file.getDurationString(), file.getStarredDate() != null, file.getLovedDate() != null, file.isPresent()));
        }

        return result;
    }

    public PlaylistInfo toggleStar(int id, int index, boolean forced) {
    	HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
    	String username = securityService.getCurrentUsername(request);
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);
    	MediaFile file = files.get(index);

    	boolean starred = mediaFileDao.getMediaFileStarredDate(file.getId(), username) != null;
    	if (starred) {
    		if (forced) {
    			mediaFileDao.unstarMediaFile(file.getId(), username);
    		}
    	} else {
    		mediaFileDao.starMediaFile(file.getId(), username);
    	}
    	return getPlaylist(id);
    }

    public PlaylistInfo toggleLove(int id, int index, boolean forced) {
    	HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
    	String username = securityService.getCurrentUsername(request);
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);
    	MediaFile file = files.get(index);

    	boolean loved = mediaFileDao.getMediaFileLovedDate(file.getId(), username) != null;
    	if (loved) {
    		if (forced) {
    			mediaFileDao.unloveMediaFile(file.getId(), username);
    		}
    	} else {
    		mediaFileDao.loveMediaFile(file.getId(), username);
    	}
    	return getPlaylist(id);
    }    
    
    public PlaylistInfo toggleAllStar(int id, boolean forced) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);

        for (MediaFile file : files) {
            boolean starred = mediaFileDao.getMediaFileStarredDate(file.getId(), username) != null;
            if (starred) {
            	if (!forced) {
                    mediaFileDao.unstarMediaFile(file.getId(), username);
            	}
            } else {
                mediaFileDao.starMediaFile(file.getId(), username);
            }
        }
        return getPlaylist(id);
    }
    
    public PlaylistInfo toggleAllLove(int id, boolean forced) {
        HttpServletRequest request = WebContextFactory.get().getHttpServletRequest();
        String username = securityService.getCurrentUsername(request);
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);

        for (MediaFile file : files) {
            boolean loved = mediaFileDao.getMediaFileLovedDate(file.getId(), username) != null;
            if (loved) {
            	if (!forced) {
                    mediaFileDao.unloveMediaFile(file.getId(), username);
            	}
            } else {
                mediaFileDao.loveMediaFile(file.getId(), username);
            }
        }
        return getPlaylist(id);
    }    
    
    public PlaylistInfo remove(int id, int index) {
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);
        files.remove(index);
        playlistService.setFilesInPlaylist(id, files);
        return getPlaylist(id);
    }

    public PlaylistInfo up(int id, int index) {
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);
        if (index > 0) {
            MediaFile file = files.remove(index);
            files.add(index - 1, file);
            playlistService.setFilesInPlaylist(id, files);
        }
        return getPlaylist(id);
    }

    public PlaylistInfo rearrange(int id, int[] indexes) {
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);
        MediaFile[] newFiles = new MediaFile[files.size()];
        for (int i = 0; i < indexes.length; i++) {
            newFiles[i] = files.get(indexes[i]);
        }
        playlistService.setFilesInPlaylist(id, Arrays.asList(newFiles));
        return getPlaylist(id);
    }

    public PlaylistInfo down(int id, int index) {
        List<MediaFile> files = playlistService.getFilesInPlaylist(id, true);
        if (index < files.size() - 1) {
            MediaFile file = files.remove(index);
            files.add(index + 1, file);
            playlistService.setFilesInPlaylist(id, files);
        }
        return getPlaylist(id);
    }

    public void deletePlaylist(int id) {
        playlistService.deletePlaylist(id);
    }

    public PlaylistInfo updatePlaylist(int id, String name, String comment, boolean isPublic) {
        Playlist playlist = playlistService.getPlaylist(id);
        playlist.setName(name);
        playlist.setComment(comment);
        playlist.setPublic(isPublic);
        if (isPublic){
        	playlist.setShareLevel(1);
        }   else {
        	playlist.setShareLevel(0);
        	}
        
        //playlist.setShareLevel(shareLevel);
        
        playlistService.updatePlaylist(playlist);
        return getPlaylist(id);
    }
}