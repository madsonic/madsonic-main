/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ajax;

/**
 * Details about what a user is currently listening to.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class NowPlayingInfo {

    private final String playerId;
    private final String username;
    private final String artist;
    private final String title;
    private final String year;
    private final String duration;
    private final String tooltip;
    private final String streamUrl;
    private final String albumUrl;
    private final String lyricsUrl;
    private final String coverArtUrl;
    private final String coverArtZoomUrl;
    private final String avatarUrl;
    private final int minutesAgo;
    private final int mediaId;
    private final boolean starred;
    private final boolean loved;
    private final boolean bookmarked;
    
    public NowPlayingInfo(String playerId, String user, String artist, String title, String year, String duration, String tooltip, String streamUrl, String albumUrl,
                          String lyricsUrl, String coverArtUrl, String coverArtZoomUrl, String avatarUrl, int minutesAgo, int mediaId, boolean starred, boolean loved, boolean bookmarked) {
        this.playerId = playerId;
        this.username = user;
        this.artist = artist;
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.tooltip = tooltip;
        this.streamUrl = streamUrl;
        this.albumUrl = albumUrl;
        this.lyricsUrl = lyricsUrl;
        this.coverArtUrl = coverArtUrl;
        this.coverArtZoomUrl = coverArtZoomUrl;
        this.avatarUrl = avatarUrl;
        this.minutesAgo = minutesAgo;
        this.mediaId = mediaId;
        this.starred = starred;
        this.loved = loved;
        this.bookmarked = bookmarked;
        
    }
    

    public String getPlayerId() {
        return playerId;
    }

    public String getUsername() {
        return username;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
		return year;
	}

	public String getDuration() {
		return duration;
	}

	public String getTooltip() {
        return tooltip;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public String getAlbumUrl() {
        return albumUrl;
    }

    public String getLyricsUrl() {
        return lyricsUrl;
    }

    public String getCoverArtUrl() {
        return coverArtUrl;
    }

    public String getCoverArtZoomUrl() {
        return coverArtZoomUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public int getMinutesAgo() {
        return minutesAgo;
    }

	public int getMediaId() {
		return mediaId;
	}

	public boolean isStarred() {
		return starred;
	}

	public boolean isBookmarked() {
		return bookmarked;
	}

	public boolean isLoved() {
		return loved;
	}
}
