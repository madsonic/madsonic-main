/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller.REST.Subsonic;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

import org.madsonic.Logger;
import org.madsonic.ajax.ChatService;
import org.madsonic.ajax.LyricsInfo;
import org.madsonic.ajax.LyricsService;
import org.madsonic.ajax.PlayQueueService;
import org.madsonic.command.UserSettingsCommand;
import org.madsonic.controller.ArtistsController;
import org.madsonic.controller.AvatarController;
import org.madsonic.controller.CaptionsController;
import org.madsonic.controller.CoverArtController;
import org.madsonic.controller.DownloadController;
import org.madsonic.controller.HLSController;
import org.madsonic.controller.StreamController;
import org.madsonic.controller.UserSettingsController;

import org.madsonic.dao.AlbumDao;
import org.madsonic.dao.ArtistDao;
import org.madsonic.dao.BookmarkDao;
import org.madsonic.dao.MediaFileDao;
import org.madsonic.dao.PlayQueueDao;
import org.madsonic.domain.Album;
import org.madsonic.domain.AlbumNotes;
import org.madsonic.domain.Artist;
import org.madsonic.domain.ArtistBio;
import org.madsonic.domain.Avatar;
import org.madsonic.domain.AvatarScheme;
import org.madsonic.domain.Bookmark;
import org.madsonic.domain.Genre;
import org.madsonic.domain.InternetRadio;
import org.madsonic.domain.LicenseInfo;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MusicFolder;
import org.madsonic.domain.MusicIndex;
import org.madsonic.domain.PlayQueue;
import org.madsonic.domain.PlayStatus;
import org.madsonic.domain.Player;
import org.madsonic.domain.PlayerTechnology;
import org.madsonic.domain.Playlist;
import org.madsonic.domain.PodcastChannel;
import org.madsonic.domain.PodcastEpisode;
import org.madsonic.domain.RandomSearchCriteria;
import org.madsonic.domain.SavedPlayQueue;
import org.madsonic.domain.SearchCriteria;
import org.madsonic.domain.SearchResult;
import org.madsonic.domain.Share;
import org.madsonic.domain.TranscodeScheme;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.domain.VideoConversion;
import org.subsonic.restapi.AlbumID3;
import org.subsonic.restapi.AlbumInfo;
import org.subsonic.restapi.AlbumList;
import org.subsonic.restapi.AlbumList2;
import org.subsonic.restapi.AlbumWithSongsID3;

import org.subsonic.restapi.ArtistID3;
import org.subsonic.restapi.ArtistInfo;
import org.subsonic.restapi.ArtistInfo2;
import org.subsonic.restapi.ArtistWithAlbumsID3;
import org.subsonic.restapi.ArtistsID3;
import org.subsonic.restapi.AudioTrack;
import org.subsonic.restapi.Bookmarks;
import org.subsonic.restapi.Captions;
import org.subsonic.restapi.ChatMessage;
import org.subsonic.restapi.ChatMessages;
import org.subsonic.restapi.Child;
import org.subsonic.restapi.Directory;
import org.subsonic.restapi.Genres;
import org.subsonic.restapi.Index;
import org.subsonic.restapi.IndexID3;
import org.subsonic.restapi.Indexes;
import org.subsonic.restapi.InternetRadioStation;
import org.subsonic.restapi.InternetRadioStations;
import org.subsonic.restapi.JukeboxPlaylist;
import org.subsonic.restapi.JukeboxStatus;
import org.subsonic.restapi.License;
import org.subsonic.restapi.Lyrics;
import org.subsonic.restapi.MediaType;
import org.subsonic.restapi.MusicFolders;
import org.subsonic.restapi.NewestPodcasts;
import org.subsonic.restapi.NowPlaying;
import org.subsonic.restapi.NowPlayingEntry;
import org.subsonic.restapi.PlaylistWithSongs;
import org.subsonic.restapi.Playlists;
import org.subsonic.restapi.PodcastStatus;
import org.subsonic.restapi.Podcasts;
import org.subsonic.restapi.Response;
import org.subsonic.restapi.SearchResult2;
import org.subsonic.restapi.SearchResult3;
import org.subsonic.restapi.Shares;
import org.subsonic.restapi.SimilarSongs;
import org.subsonic.restapi.SimilarSongs2;
import org.subsonic.restapi.Songs;
import org.subsonic.restapi.Starred;
import org.subsonic.restapi.Starred2;
import org.subsonic.restapi.TopSongs;
import org.subsonic.restapi.Users;
import org.subsonic.restapi.VideoInfo;
import org.subsonic.restapi.Videos;

import org.madsonic.service.AudioScrobblerService;
import org.madsonic.service.JukeboxService;
import org.madsonic.service.LastFmServiceBasic;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.MusicIndexService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.PlaylistService;
import org.madsonic.service.PodcastService;
import org.madsonic.service.RatingService;
import org.madsonic.service.SearchService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.ShareService;
import org.madsonic.service.StatusService;
import org.madsonic.service.TranscodingService;
import org.madsonic.service.VideoConversionService;
import org.madsonic.service.metadata.MetaData;
import org.madsonic.service.metadata.Track;
import org.madsonic.util.Pair;
import org.madsonic.util.StringUtil;
import org.madsonic.util.Util;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import static org.madsonic.security.SubsonicRESTRequestParameterProcessingFilter.decrypt;
import static org.springframework.web.bind.ServletRequestUtils.getBooleanParameter;
import static org.springframework.web.bind.ServletRequestUtils.getIntParameter;
import static org.springframework.web.bind.ServletRequestUtils.getIntParameters;
import static org.springframework.web.bind.ServletRequestUtils.getLongParameter;
import static org.springframework.web.bind.ServletRequestUtils.getLongParameters;
import static org.springframework.web.bind.ServletRequestUtils.getRequiredFloatParameter;
import static org.springframework.web.bind.ServletRequestUtils.getRequiredIntParameter;
import static org.springframework.web.bind.ServletRequestUtils.getRequiredIntParameters;
import static org.springframework.web.bind.ServletRequestUtils.getRequiredLongParameter;
import static org.springframework.web.bind.ServletRequestUtils.getRequiredStringParameter;
import static org.springframework.web.bind.ServletRequestUtils.getStringParameter;

/**
 * Multi-controller used for the REST API.
 * <p/>
 * For documentation, please refer to api.jsp.
 * <p/>
 * Note: Exceptions thrown from the methods are intercepted by RESTSubsonicFilter.
 *
 * @author Sindre Mehus
 */
public class RESTController extends MultiActionController {

	private static final Logger LOG = Logger.getLogger(RESTController.class);

	private SettingsService settingsService;
	private SecurityService securityService;
	private PlayerService playerService;
	private MediaFileService mediaFileService;
	private LastFmServiceBasic lastFmServiceBasic;
	private MusicIndexService musicIndexService;
	private TranscodingService transcodingService;
	private DownloadController downloadController;
	private CoverArtController coverArtController;
	private AvatarController avatarController;
	private UserSettingsController userSettingsController;
	private ArtistsController artistsController;
	private StatusService statusService;
	private StreamController streamController;
	private HLSController hlsController;
	private ShareService shareService;
	private PlaylistService playlistService;
	private ChatService chatService;
	private LyricsService lyricsService;
	private PlayQueueService playQueueService;
	private JukeboxService jukeboxService;
	private AudioScrobblerService audioScrobblerService;
	private PodcastService podcastService;
	private RatingService ratingService;
	private SearchService searchService;
	private VideoConversionService videoConversionService;
	private MediaFileDao mediaFileDao;
	private ArtistDao artistDao;
	private AlbumDao albumDao;
	private BookmarkDao bookmarkDao;
	private PlayQueueDao playQueueDao;
	private CaptionsController captionsController;

	private final Map<BookmarkKey, Bookmark> bookmarkCache = new ConcurrentHashMap<BookmarkKey, Bookmark>();
	
	private final org.madsonic.controller.REST.Madsonic.JAXBWriter madsonicJaxbWriter = new org.madsonic.controller.REST.Madsonic.JAXBWriter();
	private final org.madsonic.controller.REST.Subsonic.JAXBWriter subsonicJaxbWriter = new org.madsonic.controller.REST.Subsonic.JAXBWriter();

	public void init() {
		LOG.debug("supported SUBSONIC REST version: " + subsonicJaxbWriter.getRestProtocolVersion());
		refreshBookmarkCache();
	}

	private void refreshBookmarkCache() {
		bookmarkCache.clear();
		for (Bookmark bookmark : bookmarkDao.getBookmarks()) {
			bookmarkCache.put(BookmarkKey.forBookmark(bookmark), bookmark);
		}
	}

	public void ping(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Response res = createResponse();
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getLicense(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		License license = new License();

		LicenseInfo licenseInfo = settingsService.getLicenseInfo();
		license.setEmail(licenseInfo.getLicenseEmail());
		license.setValid(licenseInfo.isLicenseValid());
		license.setLicenseExpires(subsonicJaxbWriter.convertDate(licenseInfo.getLicenseExpires()));
		license.setTrialExpires(subsonicJaxbWriter.convertDate(licenseInfo.getTrialExpires()));

		Response res = createResponse();
		res.setLicense(license);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getApi(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		org.subsonic.restapi.ApiStatus apiStatus = new org.subsonic.restapi.ApiStatus();
		org.subsonic.restapi.ServerStatus serverStatusMadsonic = new org.subsonic.restapi.ServerStatus(); 
		org.subsonic.restapi.ServerStatus serverStatusSubsonic = new org.subsonic.restapi.ServerStatus(); 

		// set supported Madsonic REST version
		serverStatusMadsonic.setType(org.subsonic.restapi.Type.MADSONIC);
		serverStatusMadsonic.setVersion(madsonicJaxbWriter.getRestProtocolVersion());

		// set supported Subsonic REST version
		serverStatusSubsonic.setType(org.subsonic.restapi.Type.SUBSONIC);
		serverStatusSubsonic.setVersion(subsonicJaxbWriter.getRestProtocolVersion());
		apiStatus.getRest().add(serverStatusMadsonic);
		apiStatus.getRest().add(serverStatusSubsonic);

		// send statusRepsonse
		Response res = createStatusResponse();
		res.setApi(apiStatus);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}    

	public void getMusicFolders(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		MusicFolders musicFolders = new MusicFolders();
		int userGroupId = securityService.getCurrentUserGroupId(request);
		for (MusicFolder musicFolder : settingsService.getAllMusicFolders(false, false, userGroupId)) {
			org.subsonic.restapi.MusicFolder mf = new org.subsonic.restapi.MusicFolder();
			mf.setId(musicFolder.getId());
			mf.setName(musicFolder.getName());
			musicFolders.getMusicFolder().add(mf);
		}
		Response res = createResponse();
		res.setMusicFolders(musicFolders);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getIndexes(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Response res = createResponse();
		String username = securityService.getCurrentUser(request).getUsername();
		int userGroupId = securityService.getCurrentUserGroupId(request);

		long ifModifiedSince = getLongParameter(request, "ifModifiedSince", 0L);
		long lastModified = artistsController.getLastModified(request);

		if (lastModified <= ifModifiedSince) {
			subsonicJaxbWriter.writeResponse(request, response, res);
			return;
		}

		Indexes indexes = new Indexes();
		indexes.setLastModified(lastModified);
		indexes.setIgnoredArticles(settingsService.getIgnoredArticles());

		List<MusicFolder> musicFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder());
		Integer musicFolderId = getIntParameter(request, "musicFolderId");
		if (musicFolderId != null) {
			for (MusicFolder musicFolder : musicFolders) {
				if (musicFolderId.equals(musicFolder.getId())) {
					musicFolders = Collections.singletonList(musicFolder);
					break;
				}
			}
		}

		for (MediaFile shortcut : musicIndexService.getShortcutsX(musicFolders)) {
			indexes.getShortcut().add(createJaxbArtist(shortcut, username));
		}

		org.madsonic.domain.MusicFolderContent musicFolderContent = musicIndexService.getMusicFolderContentX(musicFolders, false);

		for (Map.Entry<MusicIndex, List<MusicIndex.SortableArtistWithMediaFiles>> entry : musicFolderContent.getIndexedArtists().entrySet()) {
			Index index = new Index();
			indexes.getIndex().add(index);
			index.setName(entry.getKey().getIndex());

			for (MusicIndex.SortableArtistWithMediaFiles artist : entry.getValue()) {
				for (MediaFile mediaFile : artist.getMediaFiles()) {
					if (mediaFile.isDirectory()) {
						Date starredDate = mediaFileDao.getMediaFileStarredDate(mediaFile.getId(), username);
						org.subsonic.restapi.Artist a = new org.subsonic.restapi.Artist();
						index.getArtist().add(a);
						a.setId(String.valueOf(mediaFile.getId()));
						a.setName(artist.getName());
						a.setStarred(subsonicJaxbWriter.convertDate(starredDate));

						if (mediaFile.isAlbum()) {
							a.setAverageRating(ratingService.getAverageRating(mediaFile));
							a.setUserRating(ratingService.getRatingForUser(username, mediaFile));
						}
					}
				}
			}
		}

		// Add children
		Player player = playerService.getPlayer(request, response);

		for (MediaFile singleSong : musicFolderContent.getSingleSongs()) {
			indexes.getChild().add(createJaxbChild(player, singleSong, username));
		}

		res.setIndexes(indexes);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getGenres(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Genres genres = new Genres();

		for (Genre genre : mediaFileDao.getGenres(false)) {
			org.subsonic.restapi.Genre g = new org.subsonic.restapi.Genre();
			genres.getGenre().add(g);
			g.setContent(genre.getName());
			g.setAlbumCount(genre.getAlbumCount());
			g.setSongCount(genre.getSongCount());
		}
		Response res = createResponse();
		res.setGenres(genres);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getSongsByGenre(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);


		String genre = getRequiredStringParameter(request, "genre");
		int offset = getIntParameter(request, "offset", 0);
		int count = getIntParameter(request, "count", 10);
		count = Math.max(0, Math.min(count, 500));

		Songs result = new Songs();

		for (MediaFile mediaFile : mediaFileDao.getSongsByGenre(genre, offset, count)) {
			result.getSong().add(createJaxbChild(player, mediaFile, username));
		}
		Response res = createResponse();
		res.setSongsByGenre(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getArtists(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);

		ArtistsID3 result = new ArtistsID3();
		result.setIgnoredArticles(settingsService.getIgnoredArticles());

		List<Artist> artists = artistDao.getAlphabetialArtists(0, Integer.MAX_VALUE);
		SortedMap<MusicIndex, List<MusicIndex.SortableArtistWithArtist>> indexedArtists = musicIndexService.getIndexedArtists(artists, 1); //TODO: fixed Default Index 
		for (Map.Entry<MusicIndex, List<MusicIndex.SortableArtistWithArtist>> entry : indexedArtists.entrySet()) {
			IndexID3 index = new IndexID3();
			result.getIndex().add(index);
			index.setName(entry.getKey().getIndex());
			for (MusicIndex.SortableArtistWithArtist sortableArtist : entry.getValue()) {
				index.getArtist().add(createJaxbArtist(new ArtistID3(), sortableArtist.getArtist(), username));
			}
		}

		Response res = createResponse();
		res.setArtists(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getSimilarSongs(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);

		int id = getRequiredIntParameter(request, "id");
		int count = getIntParameter(request, "count", 50);

		SimilarSongs result = new SimilarSongs();

		MediaFile mediaFile = mediaFileService.getMediaFile(id);
		if (mediaFile == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Media file not found.");
			return;
		}
		List<MediaFile> similarSongs = lastFmServiceBasic.getSimilarSongs(mediaFile, count);
		Player player = playerService.getPlayer(request, response);
		for (MediaFile similarSong : similarSongs) {
			result.getSong().add(createJaxbChild(player, similarSong, username));
		}

		Response res = createResponse();
		res.setSimilarSongs(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getSimilarSongs2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);

		int id = getRequiredIntParameter(request, "id");
		int count = getIntParameter(request, "count", 50);

		SimilarSongs2 result = new SimilarSongs2();

		Artist artist = artistDao.getArtist(id);
		if (artist == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Artist not found.");
			return;
		}

		List<MediaFile> similarSongs = lastFmServiceBasic.getSimilarSongs(artist, count);
		Player player = playerService.getPlayer(request, response);
		for (MediaFile similarSong : similarSongs) {
			result.getSong().add(createJaxbChild(player, similarSong, username));
		}

		Response res = createResponse();
		res.setSimilarSongs2(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getTopSongs(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);

		String artist = getRequiredStringParameter(request, "artist");
		int count = getIntParameter(request, "count", 50);

		TopSongs result = new TopSongs();

		List<MusicFolder> musicFolders = settingsService.getAllMusicFolders(); //TODO: group
		List<MediaFile> topSongs = lastFmServiceBasic.getTopSongs(artist, count, musicFolders);
		Player player = playerService.getPlayer(request, response);
		for (MediaFile topSong : topSongs) {
			result.getSong().add(createJaxbChild(player, topSong, username));
		}

		Response res = createResponse();
		res.setTopSongs(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getArtistInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);

		int id = getRequiredIntParameter(request, "id");
		int count = getIntParameter(request, "count", 20);
		boolean includeNotPresent = ServletRequestUtils.getBooleanParameter(request, "includeNotPresent", false);

		ArtistInfo result = new ArtistInfo();

		MediaFile mediaFile = mediaFileService.getMediaFile(id);
		if (mediaFile == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Media file not found.");
			return;
		}
		List<MediaFile> similarArtists = lastFmServiceBasic.getSimilarArtists(mediaFile, count, includeNotPresent);
		for (MediaFile similarArtist : similarArtists) {
			result.getSimilarArtist().add(createJaxbArtist(similarArtist, username));
		}
		ArtistBio artistBio = lastFmServiceBasic.getArtistBio(mediaFile, true);
		if (artistBio != null) {
			result.setBiography(artistBio.getBiography());
			result.setMusicBrainzId(artistBio.getMusicBrainzId());
			result.setLastFmUrl(artistBio.getLastFmUrl());
			result.setSmallImageUrl(artistBio.getSmallImageUrl());
			result.setMediumImageUrl(artistBio.getMediumImageUrl());
			result.setLargeImageUrl(artistBio.getLargeImageUrl());
		}

		Response res = createResponse();
		res.setArtistInfo(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getArtistInfo2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);

		int id = getRequiredIntParameter(request, "id");
		int count = getIntParameter(request, "count", 20);
		boolean includeNotPresent = ServletRequestUtils.getBooleanParameter(request, "includeNotPresent", false);

		ArtistInfo2 result = new ArtistInfo2();

		Artist artist = artistDao.getArtist(id);
		if (artist == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Artist not found.");
			return;
		}

		// List<MusicFolder> musicFolders = settingsService.getAllMusicFolders(); //TODO: group

		List<Artist> similarArtists = lastFmServiceBasic.getSimilarArtists(artist, count, includeNotPresent);
		for (Artist similarArtist : similarArtists) {
			result.getSimilarArtist().add(createJaxbArtist(new ArtistID3(), similarArtist, username));
		}
		ArtistBio artistBio = lastFmServiceBasic.getArtistBio(artist, true);
		if (artistBio != null) {
			result.setBiography(artistBio.getBiography());
			result.setMusicBrainzId(artistBio.getMusicBrainzId());
			result.setLastFmUrl(artistBio.getLastFmUrl());
			result.setSmallImageUrl(artistBio.getSmallImageUrl());
			result.setMediumImageUrl(artistBio.getMediumImageUrl());
			result.setLargeImageUrl(artistBio.getLargeImageUrl());
		}

		Response res = createResponse();
		res.setArtistInfo2(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getAlbumInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		int id = getRequiredIntParameter(request, "id");
		MediaFile mediaFile = mediaFileService.getMediaFile(id);
		if (mediaFile == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Media file not found.");
			return;
		}

		AlbumInfo result = new AlbumInfo();
		AlbumNotes albumNotes = lastFmServiceBasic.getAlbumNotes(mediaFile);
		if (albumNotes != null) {
			result.setNotes(albumNotes.getNotes());
			result.setMusicBrainzId(albumNotes.getMusicBrainzId());
			result.setLastFmUrl(albumNotes.getLastFmUrl());
			result.setSmallImageUrl(albumNotes.getSmallImageUrl());
			result.setMediumImageUrl(albumNotes.getMediumImageUrl());
			result.setLargeImageUrl(albumNotes.getLargeImageUrl());
		}

		Response res = createResponse();
		res.setAlbumInfo(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getAlbumInfo2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		int id = getRequiredIntParameter(request, "id");
		Album album = albumDao.getAlbum(id);
		if (album == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Album not found.");
			return;
		}

		AlbumInfo result = new AlbumInfo();
		AlbumNotes albumNotes = lastFmServiceBasic.getAlbumNotes(album);
		if (albumNotes != null) {
			result.setNotes(albumNotes.getNotes());
			result.setMusicBrainzId(albumNotes.getMusicBrainzId());
			result.setLastFmUrl(albumNotes.getLastFmUrl());
			result.setSmallImageUrl(albumNotes.getSmallImageUrl());
			result.setMediumImageUrl(albumNotes.getMediumImageUrl());
			result.setLargeImageUrl(albumNotes.getLargeImageUrl());
		}

		Response res = createResponse();
		res.setAlbumInfo(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	private <T extends ArtistID3> T createJaxbArtist(T jaxbArtist, Artist artist, String username) {
		jaxbArtist.setId(String.valueOf(artist.getId()));
		jaxbArtist.setName(artist.getName());
		jaxbArtist.setStarred(subsonicJaxbWriter.convertDate(mediaFileDao.getMediaFileStarredDate(artist.getId(), username)));
		jaxbArtist.setAlbumCount(artist.getAlbumCount());
		if (artist.getCoverArtPath() != null) {
			jaxbArtist.setCoverArt(CoverArtController.ARTIST_COVERART_PREFIX + artist.getId());
		}
		return jaxbArtist;
	}

	private org.subsonic.restapi.Artist createJaxbArtist(MediaFile artist, String username) {
		org.subsonic.restapi.Artist result = new org.subsonic.restapi.Artist();
		result.setId(String.valueOf(artist.getId()));
		result.setName(artist.getArtist());
		Date starred = mediaFileDao.getMediaFileStarredDate(artist.getId(), username);
		result.setStarred(subsonicJaxbWriter.convertDate(starred));
		return result;
	}

	public void getArtist(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		String username = securityService.getCurrentUsername(request);
		int id = getRequiredIntParameter(request, "id");
		Artist artist = artistDao.getArtist(id);
		if (artist == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Artist not found.");
			return;
		}

		// List<MusicFolder> musicFolders = settingsService.getAllMusicFolders(); //TODO: usergroup
		
		ArtistWithAlbumsID3 result = createJaxbArtist(new ArtistWithAlbumsID3(), artist, username);
		for (Album album : albumDao.getAlbumsForArtist(artist.getName())) {
			result.getAlbum().add(createJaxbAlbum(new AlbumID3(), album, username));
		}

		Response res = createResponse();
		res.setArtist(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	private <T extends AlbumID3> T createJaxbAlbum(T jaxbAlbum, Album album, String username) {
		jaxbAlbum.setId(String.valueOf(album.getId()));
		jaxbAlbum.setName(album.getName());
		if (album.getArtist() != null) {
			jaxbAlbum.setArtist(album.getArtist());
			Artist artist = artistDao.getArtist(album.getArtist());
			if (artist != null) {
				jaxbAlbum.setArtistId(String.valueOf(artist.getId()));
			}
		}
		if (album.getCoverArtPath() != null) {
			jaxbAlbum.setCoverArt(CoverArtController.ALBUM_COVERART_PREFIX + album.getId());
		}
		jaxbAlbum.setSongCount(album.getSongCount());
		jaxbAlbum.setDuration(album.getDurationSeconds());
		jaxbAlbum.setCreated(subsonicJaxbWriter.convertDate(album.getCreated()));
		jaxbAlbum.setStarred(subsonicJaxbWriter.convertDate(albumDao.getAlbumStarredDate(album.getId(), username)));
		jaxbAlbum.setPlayCount((long) album.getPlayCount());
		jaxbAlbum.setYear(album.getYear());
		jaxbAlbum.setGenre(album.getGenre());
		return jaxbAlbum;
	}

	private <T extends org.subsonic.restapi.Playlist> T createJaxbPlaylist(T jaxbPlaylist, Playlist playlist) {
		jaxbPlaylist.setId(String.valueOf(playlist.getId()));
		jaxbPlaylist.setName(playlist.getName());
		jaxbPlaylist.setComment(playlist.getComment());
		jaxbPlaylist.setOwner(playlist.getUsername());
		jaxbPlaylist.setPublic(playlist.isPublic());
		jaxbPlaylist.setSongCount(playlist.getFileCount());
		jaxbPlaylist.setDuration(playlist.getDurationSeconds());
		jaxbPlaylist.setCreated(subsonicJaxbWriter.convertDate(playlist.getCreated()));
		jaxbPlaylist.setChanged(subsonicJaxbWriter.convertDate(playlist.getChanged()));
		jaxbPlaylist.setCoverArt(CoverArtController.PLAYLIST_COVERART_PREFIX + playlist.getId());

		for (String username : playlistService.getPlaylistUsers(playlist.getId())) {
			jaxbPlaylist.getAllowedUser().add(username);
		}
		return jaxbPlaylist;
	}

	public void getAlbum(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);

		int id = getRequiredIntParameter(request, "id");
		Album album = albumDao.getAlbum(id);
		if (album == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Album not found.");
			return;
		}

		AlbumWithSongsID3 result = createJaxbAlbum(new AlbumWithSongsID3(), album, username);
		for (MediaFile mediaFile : mediaFileDao.getSongsForAlbum(album.getArtist(), album.getName())) {
			result.getSong().add(createJaxbChild(player, mediaFile, username));
		}

		Response res = createResponse();
		res.setAlbum(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getSong(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);

		int id = getRequiredIntParameter(request, "id");
		MediaFile song = mediaFileDao.getMediaFile(id);
		if (song == null || song.isDirectory()) {
			error(request, response, ErrorCode.NOT_FOUND, "Song not found.");
			return;
		}
		if (!securityService.isAccessAllowed(song, userGroupId)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Access denied");
			return;
		}

		Response res = createResponse();
		res.setSong(createJaxbChild(player, song, username));
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getMusicDirectory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);

		int id = getRequiredIntParameter(request, "id");
		MediaFile dir = mediaFileService.getMediaFile(id, userGroupId);
		if (dir == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Directory not found");
			return;
		}

		MediaFile parent = mediaFileService.getParentOf(dir);
		Directory directory = new Directory();
		directory.setId(String.valueOf(id));
		try {
			if (!mediaFileService.isRoot(parent)) {
				directory.setParent(String.valueOf(parent.getId()));
			}
		} catch (SecurityException x) {
			// Ignored.
		}
		directory.setName(dir.getName());
		directory.setStarred(subsonicJaxbWriter.convertDate(mediaFileDao.getMediaFileStarredDate(id, username)));

		if (dir.isAlbum()) {
			directory.setAverageRating(ratingService.getAverageRating(dir));
			directory.setUserRating(ratingService.getRatingForUser(username, dir));
			directory.setPlayCount((long) dir.getPlayCount());
		}

		for (MediaFile child : mediaFileService.getChildrenOf(dir, true, true, true)) {
			directory.getChild().add(createJaxbChild(player, child, username));
		}

		Response res = createResponse();
		res.setDirectory(directory);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	@Deprecated

	public void search(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);

		String any = request.getParameter("any");
		String artist = request.getParameter("artist");
		String album = request.getParameter("album");
		String title = request.getParameter("title");

		StringBuilder query = new StringBuilder();
		if (any != null) {
			query.append(any).append(" ");
		}
		if (artist != null) {
			query.append(artist).append(" ");
		}
		if (album != null) {
			query.append(album).append(" ");
		}
		if (title != null) {
			query.append(title);
		}

		SearchCriteria criteria = new SearchCriteria();
		criteria.setQuery(query.toString().trim());
		criteria.setCount(getIntParameter(request, "count", 20));
		criteria.setOffset(getIntParameter(request, "offset", 0));

		SearchResult result = searchService.search(criteria, SearchService.IndexType.SONG, 1);
		org.subsonic.restapi.SearchResult searchResult = new org.subsonic.restapi.SearchResult();
		searchResult.setOffset(result.getOffset());
		searchResult.setTotalHits(result.getTotalHits());

		for (MediaFile mediaFile : result.getMediaFiles()) {
			searchResult.getMatch().add(createJaxbChild(player, mediaFile, username));
		}
		Response res = createResponse();
		res.setSearchResult(searchResult);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void search2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);

		SearchResult2 searchResult = new SearchResult2();

		String query = request.getParameter("query");
		SearchCriteria criteria = new SearchCriteria();
		criteria.setQuery(StringUtils.trimToEmpty(query));
		criteria.setCount(getIntParameter(request, "artistCount", 20));
		criteria.setOffset(getIntParameter(request, "artistOffset", 0));
		SearchResult artists = searchService.search(criteria, SearchService.IndexType.ARTIST, userGroupId);
		for (MediaFile mediaFile : artists.getMediaFiles()) {
			searchResult.getArtist().add(createJaxbArtist(mediaFile, username));
		}

		criteria.setCount(getIntParameter(request, "albumCount", 20));
		criteria.setOffset(getIntParameter(request, "albumOffset", 0));
		SearchResult albums = searchService.search(criteria, SearchService.IndexType.ALBUM, userGroupId);
		for (MediaFile mediaFile : albums.getMediaFiles()) {
			searchResult.getAlbum().add(createJaxbChild(player, mediaFile, username));
		}

		criteria.setCount(getIntParameter(request, "songCount", 20));
		criteria.setOffset(getIntParameter(request, "songOffset", 0));
		SearchResult songs = searchService.search(criteria, SearchService.IndexType.SONG, userGroupId);
		for (MediaFile mediaFile : songs.getMediaFiles()) {
			searchResult.getSong().add(createJaxbChild(player, mediaFile, username));
		}

		Response res = createResponse();
		res.setSearchResult2(searchResult);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void search3(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);

		SearchResult3 searchResult = new SearchResult3();

		String query = request.getParameter("query");
		SearchCriteria criteria = new SearchCriteria();
		criteria.setQuery(StringUtils.trimToEmpty(query));
		criteria.setCount(getIntParameter(request, "artistCount", 20));
		criteria.setOffset(getIntParameter(request, "artistOffset", 0));
		SearchResult result = searchService.search(criteria, SearchService.IndexType.ARTIST_ID3, userGroupId);
		for (Artist artist : result.getArtists()) {
			searchResult.getArtist().add(createJaxbArtist(new ArtistID3(), artist, username));
		}

		criteria.setCount(getIntParameter(request, "albumCount", 20));
		criteria.setOffset(getIntParameter(request, "albumOffset", 0));
		result = searchService.search(criteria, SearchService.IndexType.ALBUM_ID3, userGroupId);
		for (Album album : result.getAlbums()) {
			searchResult.getAlbum().add(createJaxbAlbum(new AlbumID3(), album, username));
		}

		criteria.setCount(getIntParameter(request, "songCount", 20));
		criteria.setOffset(getIntParameter(request, "songOffset", 0));
		result = searchService.search(criteria, SearchService.IndexType.SONG, userGroupId);
		for (MediaFile song : result.getMediaFiles()) {
			searchResult.getSong().add(createJaxbChild(player, song, username));
		}

		Response res = createResponse();
		res.setSearchResult3(searchResult);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getPlaylists(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		User user = securityService.getCurrentUser(request);
		String authenticatedUsername = user.getUsername();
		String requestedUsername = request.getParameter("username");

		if (requestedUsername == null) {
			requestedUsername = authenticatedUsername;
		} else if (!user.isAdminRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, authenticatedUsername + " is not authorized to get playlists for " + requestedUsername);
			return;
		}

		Playlists result = new Playlists();

		for (Playlist playlist : playlistService.getReadablePlaylistsForUser(requestedUsername, null, null)) {
			result.getPlaylist().add(createJaxbPlaylist(new org.subsonic.restapi.Playlist(), playlist));
		}

		Response res = createResponse();
		res.setPlaylists(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getPlaylist(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);        

		int id = getRequiredIntParameter(request, "id");

		Playlist playlist = playlistService.getPlaylist(id);
		if (playlist == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Playlist not found: " + id);
			return;
		}
		if (!playlistService.isReadAllowed(playlist, username)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Permission denied for playlist " + id);
			return;
		}
		PlaylistWithSongs result = createJaxbPlaylist(new PlaylistWithSongs(), playlist);
		for (MediaFile mediaFile : playlistService.getFilesInPlaylist(id)) {
			if (securityService.isAccessAllowed(mediaFile, userGroupId)) {
				result.getEntry().add(createJaxbChild(player, mediaFile, username));
			}
		}

		Response res = createResponse();
		res.setPlaylist(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void jukeboxControl(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request, true);

		User user = securityService.getCurrentUser(request);
		if (!user.isJukeboxRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to use jukebox.");
			return;
		}

		boolean returnPlaylist = false;
		String action = getRequiredStringParameter(request, "action");
		if ("start".equals(action)) {
			playQueueService.doStart(request, response);
		} else if ("stop".equals(action)) {
			playQueueService.doStop(request, response);
		} else if ("skip".equals(action)) {
			int index = getRequiredIntParameter(request, "index");
			int offset = getIntParameter(request, "offset", 0);
			playQueueService.doSkip(request, response, index, offset);
		} else if ("add".equals(action)) {
			int[] ids = getIntParameters(request, "id");
			playQueueService.doAdd(request, response, ids, null);
		} else if ("set".equals(action)) {
			int[] ids = getIntParameters(request, "id");
			playQueueService.doSet(request, response, ids);
		} else if ("clear".equals(action)) {
			playQueueService.doClear(request, response);
		} else if ("remove".equals(action)) {
			int index = getRequiredIntParameter(request, "index");
			playQueueService.doRemove(request, response, index);
		} else if ("shuffle".equals(action)) {
			playQueueService.doShuffle(request, response);
		} else if ("setGain".equals(action)) {
			float gain = getRequiredFloatParameter(request, "gain");
			jukeboxService.setGain(gain);
		} else if ("get".equals(action)) {
			returnPlaylist = true;
		} else if ("status".equals(action)) {
			// No action necessary.
		} else {
			throw new Exception("Unknown jukebox action: '" + action + "'.");
		}

		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		Player jukeboxPlayer = jukeboxService.getPlayer();
		boolean controlsJukebox = jukeboxPlayer != null && jukeboxPlayer.getId().equals(player.getId());
		PlayQueue playQueue = player.getPlayQueue();


		int currentIndex = controlsJukebox && !playQueue.isEmpty() ? playQueue.getIndex() : -1;
		boolean playing = controlsJukebox && !playQueue.isEmpty() && playQueue.getStatus() == PlayQueue.Status.PLAYING;
		float gain = jukeboxService.getGain();
		int position = controlsJukebox && !playQueue.isEmpty() ? jukeboxService.getPosition() : 0;

		Response res = createResponse();
		if (returnPlaylist) {
			JukeboxPlaylist result = new JukeboxPlaylist();
			res.setJukeboxPlaylist(result);
			result.setCurrentIndex(currentIndex);
			result.setPlaying(playing);
			result.setGain(gain);
			result.setPosition(position);
			for (MediaFile mediaFile : playQueue.getFiles()) {
				result.getEntry().add(createJaxbChild(player, mediaFile, username));
			}
		} else {
			JukeboxStatus result = new JukeboxStatus();
			res.setJukeboxStatus(result);
			result.setCurrentIndex(currentIndex);
			result.setPlaying(playing);
			result.setGain(gain);
			result.setPosition(position);
		}

		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void createPlaylist(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request, true);
		String username = securityService.getCurrentUsername(request);

		Integer playlistId = getIntParameter(request, "playlistId");
		String name = request.getParameter("name");
		if (playlistId == null && name == null) {
			error(request, response, ErrorCode.MISSING_PARAMETER, "Playlist ID or name must be specified.");
			return;
		}

		Playlist playlist;
		if (playlistId != null) {
			playlist = playlistService.getPlaylist(playlistId);
			if (playlist == null) {
				error(request, response, ErrorCode.NOT_FOUND, "Playlist not found: " + playlistId);
				return;
			}
			if (!playlistService.isWriteAllowed(playlist, username)) {
				error(request, response, ErrorCode.NOT_AUTHORIZED, "Permission denied for playlist " + playlistId);
				return;
			}
		} else {
			playlist = new Playlist();
			playlist.setName(name);
			playlist.setCreated(new Date());
			playlist.setChanged(new Date());
			playlist.setPublic(false);
			playlist.setUsername(username);
			playlistService.createPlaylist(playlist);
		}

		List<MediaFile> songs = new ArrayList<MediaFile>();
		for (int id : getIntParameters(request, "songId")) {
			MediaFile song = mediaFileService.getMediaFile(id);
			if (song != null) {
				songs.add(song);
			}
		}
		playlistService.setFilesInPlaylist(playlist.getId(), songs);

		writeEmptyResponse(request, response);
	}

	public void updatePlaylist(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request, true);
		String username = securityService.getCurrentUsername(request);

		int id = getRequiredIntParameter(request, "playlistId");
		Playlist playlist = playlistService.getPlaylist(id);
		if (playlist == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Playlist not found: " + id);
			return;
		}
		if (!playlistService.isWriteAllowed(playlist, username)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Permission denied for playlist " + id);
			return;
		}

		String name = request.getParameter("name");
		if (name != null) {
			playlist.setName(name);
		}
		String comment = request.getParameter("comment");
		if (comment != null) {
			playlist.setComment(comment);
		}
		Boolean isPublic = getBooleanParameter(request, "public");
		if (isPublic != null) {
			playlist.setPublic(isPublic);
		}
		playlistService.updatePlaylist(playlist);

		// TODO: Add later
		//            for (String usernameToAdd : ServletRequestUtils.getStringParameters(request, "usernameToAdd")) {
		//                if (securityService.getUserByName(usernameToAdd) != null) {
		//                    playlistService.addPlaylistUser(id, usernameToAdd);
		//                }
		//            }
		//            for (String usernameToRemove : ServletRequestUtils.getStringParameters(request, "usernameToRemove")) {
		//                if (securityService.getUserByName(usernameToRemove) != null) {
		//                    playlistService.deletePlaylistUser(id, usernameToRemove);
		//                }
		//            }
		List<MediaFile> songs = playlistService.getFilesInPlaylist(id);
		boolean songsChanged = false;

		SortedSet<Integer> tmp = new TreeSet<Integer>();
		for (int songIndexToRemove : getIntParameters(request, "songIndexToRemove")) {
			tmp.add(songIndexToRemove);
		}
		List<Integer> songIndexesToRemove = new ArrayList<Integer>(tmp);
		Collections.reverse(songIndexesToRemove);
		for (Integer songIndexToRemove : songIndexesToRemove) {
			songs.remove(songIndexToRemove.intValue());
			songsChanged = true;
		}
		for (int songToAdd : getIntParameters(request, "songIdToAdd")) {
			MediaFile song = mediaFileService.getMediaFile(songToAdd);
			if (song != null) {
				songs.add(song);
				songsChanged = true;
			}
		}
		if (songsChanged) {
			playlistService.setFilesInPlaylist(id, songs);
		}

		writeEmptyResponse(request, response);
	}

	public void deletePlaylist(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request, true);
		String username = securityService.getCurrentUsername(request);

		int id = getRequiredIntParameter(request, "id");
		Playlist playlist = playlistService.getPlaylist(id);
		if (playlist == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Playlist not found: " + id);
			return;
		}
		if (!playlistService.isWriteAllowed(playlist, username)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Permission denied for playlist " + id);
			return;
		}
		playlistService.deletePlaylist(id);

		writeEmptyResponse(request, response);
	}

	public void getAlbumList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);        

		int size = getIntParameter(request, "size", 10);
		int offset = getIntParameter(request, "offset", 0);
		size = Math.max(0, Math.min(size, 500));
		String type = getRequiredStringParameter(request, "type");

		List<MediaFile> albums = Collections.emptyList();
		if ("highest".equals(type)) {
			albums = ratingService.getHighestRatedAlbums(offset, size, userGroupId);
		} else if ("frequent".equals(type)) {
			albums = mediaFileService.getMostFrequentlyPlayedAlbums(offset, size, userGroupId);
		} else if ("recent".equals(type)) {
			albums = mediaFileService.getMostRecentlyPlayedAlbums(offset, size, userGroupId);
		} else if ("newest".equals(type)) {
			albums = mediaFileService.getNewestAlbums(offset, size, userGroupId);
		} else if ("starred".equals(type)) {
			albums = mediaFileService.getStarredAlbums(offset, size, username);
		} else if ("alphabeticalByArtist".equals(type)) {
			albums = mediaFileService.getAlphabeticalAlbums(offset, size, true, userGroupId);
		} else if ("alphabeticalByName".equals(type)) {
			albums = mediaFileService.getAlphabeticalAlbums(offset, size, false, userGroupId);
		} else if ("byGenre".equals(type)) {
			albums = mediaFileService.getAlbumsByGenre(offset, size, getRequiredStringParameter(request, "genre"), userGroupId);
		} else if ("byYear".equals(type)) {
			albums = mediaFileService.getAlbumsByYear(offset, size, getRequiredIntParameter(request, "fromYear"), getRequiredIntParameter(request, "toYear"), userGroupId);
			getRequiredIntParameter(request, "toYear");
		} else if ("random".equals(type)) {
			albums = searchService.getRandomAlbums(null, size, userGroupId);
		} else {
			throw new Exception("Invalid list type: " + type);
		}

		AlbumList result = new AlbumList();
		for (MediaFile album : albums) {
			result.getAlbum().add(createJaxbChild(player, album, username));
		}

		Response res = createResponse();
		res.setAlbumList(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getAlbumList2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		int size = getIntParameter(request, "size", 10);
		int offset = getIntParameter(request, "offset", 0);
		size = Math.max(0, Math.min(size, 500));
		String type = getRequiredStringParameter(request, "type");
		String username = securityService.getCurrentUsername(request);
		
		//TODO: Integer musicFolderId = getIntParameter(request, "musicFolderId");

		List<Album> albums;
		if ("frequent".equals(type)) {
			albums = albumDao.getMostFrequentlyPlayedAlbums(offset, size);
		} else if ("recent".equals(type)) {
			albums = albumDao.getMostRecentlyPlayedAlbums(offset, size);
		} else if ("newest".equals(type)) {
			albums = albumDao.getNewestAlbums(offset, size);
		} else if ("alphabeticalByArtist".equals(type)) {
			albums = albumDao.getAlphabetialAlbums(offset, size, true);
		} else if ("alphabeticalByName".equals(type)) {
			albums = albumDao.getAlphabetialAlbums(offset, size, false);
		} else if ("byGenre".equals(type)) {
			albums = albumDao.getAlbumsByGenre(offset, size, getRequiredStringParameter(request, "genre"));
		} else if ("byYear".equals(type)) {
			albums = albumDao.getAlbumsByYear(offset, size, getRequiredIntParameter(request, "fromYear"),
					getRequiredIntParameter(request, "toYear"));
		} else if ("starred".equals(type)) {
			albums = albumDao.getStarredAlbums(offset, size, securityService.getCurrentUser(request).getUsername());
		} else if ("random".equals(type)) {
			albums = searchService.getRandomAlbumsId3(size);
		} else {
			throw new Exception("Invalid list type: " + type);
		}
		AlbumList2 result = new AlbumList2();
		for (Album album : albums) {
			result.getAlbum().add(createJaxbAlbum(new AlbumID3(), album, username));
		}
		Response res = createResponse();
		res.setAlbumList2(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}


	public void getRandomSongs(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);   

		Songs result = new Songs();
		int size = getIntParameter(request, "size", 20);
		size = Math.max(0, Math.min(size, 500));
		String genre = getStringParameter(request, "genre");
		Integer fromYear = getIntParameter(request, "fromYear");
		Integer toYear = getIntParameter(request, "toYear");
		Integer musicFolderId = getIntParameter(request, "musicFolderId");
		if (musicFolderId == null){
			List<MusicFolder> musicFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder());
			Random generator = new Random();
			int index = generator.nextInt (musicFolders.size());
			if ( index >-1 ) { musicFolderId = musicFolders.get(index).getId();	}
		}
		RandomSearchCriteria criteria = new RandomSearchCriteria(size, genre, fromYear, toYear, musicFolderId);
		List <MediaFile> resultList; 
		int loop = 0;

		do { 
			resultList = searchService.getRandomSongs(criteria);
			loop++;
			List<MusicFolder> musicFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder());
			Random generator = new Random();
			int index = generator.nextInt (musicFolders.size());
			if ( index >-1 ) { musicFolderId = musicFolders.get(index).getId();	}
			RandomSearchCriteria tmpCriteria = new RandomSearchCriteria(size, genre, fromYear, toYear, musicFolderId);
			resultList = searchService.getRandomSongs(tmpCriteria);
			if ( loop>10 ){ 
				//System.out.println("break out!"); 
				break; }
		} while (resultList.size()<1);

		for (MediaFile mediaFile : resultList) {
			result.getSong().add(createJaxbChild(player, mediaFile, username));
		}
		Response res = createResponse();
		res.setRandomSongs(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getVideos(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);

		int size = getIntParameter(request, "size", Integer.MAX_VALUE);
		int offset = getIntParameter(request, "offset", 0);

		Videos result = new Videos();
		for (MediaFile mediaFile : mediaFileDao.getVideos(size, offset)) {
			result.getVideo().add(createJaxbChild(player, mediaFile, username));
		}
		Response res = createResponse();
		res.setVideos(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getVideoInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {

		request = wrapRequest(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);

		int id = ServletRequestUtils.getRequiredIntParameter(request, "id");
		MediaFile video = this.mediaFileDao.getMediaFile(id);
		if ((video == null) || (!video.isVideo())) {
			error(request, response, ErrorCode.NOT_FOUND, "Video not found.");
			return;
		}
		if (!this.securityService.isAccessAllowed(video, userGroupId)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Access denied");
			return;
		}
		VideoInfo result = new VideoInfo();
		result.setId(String.valueOf(id));

		org.madsonic.domain.VideoConversion conversion = this.videoConversionService.getVideoConversionForFile(id);
		if ((conversion != null) && (conversion.getStatus() == VideoConversion.Status.COMPLETED)) {
			org.subsonic.restapi.VideoConversion restConversion = new org.subsonic.restapi.VideoConversion();
			restConversion.setId(String.valueOf(conversion.getId()));
			restConversion.setAudioTrackId(conversion.getAudioTrackId());
			restConversion.setBitRate(conversion.getBitRate());
			result.getConversion().add(restConversion);
		}
		File captionsFile = this.captionsController.findCaptionsVideo(video);
		if (captionsFile != null) {
			Captions captions = new Captions();
			captions.setId("0");
			captions.setName(captionsFile.getName());
			result.getCaptions().add(captions);
		}
		MetaData videoMetaData = this.videoConversionService.getVideoMetaData(video);
		if (videoMetaData != null) {
			for (Track track : videoMetaData.getAudioTracks()) {
				AudioTrack restTrack = new AudioTrack();
				restTrack.setId(String.valueOf(track.getId()));
				restTrack.setName(track.getLanguageName());
				restTrack.setLanguageCode(track.getLanguage());
				result.getAudioTrack().add(restTrack);
			}
		}
		Response res = createResponse();
		res.setVideoInfo(result);
		this.subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getNowPlaying(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		NowPlaying result = new NowPlaying();

		for (PlayStatus status : statusService.getPlayStatuses()) {

			Player player = status.getPlayer();
			MediaFile mediaFile = status.getMediaFile();
			String username = player.getUsername();
			if (username == null) {
				continue;
			}

			UserSettings userSettings = settingsService.getUserSettings(username);
			if (!userSettings.isNowPlayingAllowed()) {
				continue;
			}

			long minutesAgo = status.getMinutesAgo();
			if (minutesAgo < 120) {
				NowPlayingEntry entry = new NowPlayingEntry();
				entry.setUsername(username);
				entry.setPlayerId(Integer.parseInt(player.getId()));
				entry.setPlayerName(player.getName());
				entry.setMinutesAgo((int) minutesAgo);
				result.getEntry().add(createJaxbChild(entry, player, mediaFile, username));
			}
		}

		Response res = createResponse();
		res.setNowPlaying(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	private Child createJaxbChild(Player player, MediaFile mediaFile, String username) {
		return createJaxbChild(new Child(), player, mediaFile, username);
	}

	private <T extends Child> T createJaxbChild(T child, Player player, MediaFile mediaFile, String username) {
		MediaFile parent = mediaFileService.getParentOf(mediaFile);
		child.setId(String.valueOf(mediaFile.getId()));
		try {
			if (!mediaFileService.isRoot(parent)) {
				child.setParent(String.valueOf(parent.getId()));
			}
		} catch (SecurityException x) {
			// Ignored.
		}

		if (mediaFile.isAlbum() || mediaFile.isAlbumSet()) 
		{
			if (settingsService.isShowAlbumsYearApi()) {
				child.setTitle("[" + mediaFile.getYear() + "] " + mediaFile.getAlbumName()); 
				child.setAlbum(mediaFile.getAlbumSetName());
			}else
			{
				child.setTitle(mediaFile.getAlbumName());
				child.setAlbum(mediaFile.getAlbumSetName());
				child.setPath(mediaFile.getPath().substring(mediaFile.getPath().lastIndexOf('\\') + 1));
			}
		}
		else {
			child.setTitle(mediaFile.getName());
			child.setAlbum(mediaFile.getAlbumName());
		}

		child.setArtist(mediaFile.getArtist());
		child.setIsDir(mediaFile.isDirectory());
		child.setCoverArt(findCoverArt(mediaFile, parent));
		child.setYear(mediaFile.getYear());
		child.setGenre(mediaFile.getGenre());
		child.setCreated(subsonicJaxbWriter.convertDate(mediaFile.getCreated()));
		child.setStarred(subsonicJaxbWriter.convertDate(mediaFileDao.getMediaFileStarredDate(mediaFile.getId(), username)));
		child.setUserRating(ratingService.getRatingForUser(username, mediaFile));
		child.setAverageRating(ratingService.getAverageRating(mediaFile));
		child.setPlayCount((long) mediaFile.getPlayCount());          

		if (mediaFile.isSingleArtist() || mediaFile.isMultiArtist()) 
		{
			child.setTitle(mediaFile.getArtist());
			child.setArtist(mediaFile.getGenre());
		}        
		if (mediaFile.isFile()) {
			child.setDuration(mediaFile.getDurationSeconds());
			child.setBitRate(mediaFile.getBitRate());
			child.setTrack(mediaFile.getTrackNumber());
			child.setDiscNumber(mediaFile.getDiscNumber());
			child.setSize(mediaFile.getFileSize());
			String suffix = mediaFile.getFormat();
			child.setSuffix(suffix);
			child.setContentType(StringUtil.getMimeType(suffix));
			child.setPath(getRelativePath(mediaFile));

			Bookmark bookmark = bookmarkCache.get(new BookmarkKey(username, mediaFile.getId()));
			if (bookmark != null) {
				child.setBookmarkPosition(bookmark.getPositionMillis());
			}

			if (mediaFile.getAlbumArtist() != null && mediaFile.getAlbumName() != null) {
				Album album = albumDao.getAlbum(mediaFile.getAlbumArtist(), mediaFile.getAlbumName());
				if (album != null) {
					child.setAlbumId(String.valueOf(album.getId()));
				}
			}
			if (mediaFile.getArtist() != null) {
				Artist artist = artistDao.getArtist(mediaFile.getArtist());
				if (artist != null) {
					child.setArtistId(String.valueOf(artist.getId()));
				}
			}
			switch (mediaFile.getMediaType()) {
			case MUSIC:
				child.setType(MediaType.MUSIC);
				break;
			case PODCAST:
				child.setType(MediaType.PODCAST);
				break;
			case AUDIOBOOK:
				child.setType(MediaType.AUDIOBOOK);
				break;
			case VIDEO:
				child.setType(MediaType.VIDEO);
				child.setOriginalWidth(mediaFile.getWidth());
				child.setOriginalHeight(mediaFile.getHeight());
				child.setIsVideo(true);

				VideoConversion conversion = videoConversionService.getVideoConversionForFile(mediaFile.getId());
				if (conversion != null && conversion.getStatus() == VideoConversion.Status.COMPLETED) {
					child.setHasVideoConversion(true);
				}
				break;
			default:
				break;
			}

			if (transcodingService.isTranscodingRequired(mediaFile, player)) {
				String transcodedSuffix = transcodingService.getSuffix(player, mediaFile, null);
				child.setTranscodedSuffix(transcodedSuffix);
				child.setTranscodedContentType(StringUtil.getMimeType(transcodedSuffix));
			}
		}
		return child;
	}

	private String findCoverArt(MediaFile mediaFile, MediaFile parent) {
		MediaFile dir = mediaFile.isDirectory() ? mediaFile : parent;
		if (dir != null && dir.getCoverArtPath() != null) {
			return String.valueOf(dir.getId());
		}
		return null;
	}

	private String getRelativePath(MediaFile musicFile) {

		String filePath = musicFile.getPath();

		// Convert slashes.
		filePath = filePath.replace('\\', '/');

		String filePathLower = filePath.toLowerCase();

		List<MusicFolder> musicFolders = settingsService.getAllMusicFolders(false, true);
		for (MusicFolder musicFolder : musicFolders) {
			String folderPath = musicFolder.getPath().getPath();
			folderPath = folderPath.replace('\\', '/');
			String folderPathLower = folderPath.toLowerCase();
			if (!folderPathLower.endsWith("/")) {
				folderPathLower += "/";
			}

			if (filePathLower.startsWith(folderPathLower)) {
				String relativePath = filePath.substring(folderPath.length());
				return relativePath.startsWith("/") ? relativePath.substring(1) : relativePath;
			}
		}

		return null;
	}

	public ModelAndView download(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isDownloadRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to download files.");
			return null;
		}

		long ifModifiedSince = request.getDateHeader("If-Modified-Since");
		long lastModified = downloadController.getLastModified(request);

		if (ifModifiedSince != -1 && lastModified != -1 && lastModified <= ifModifiedSince) {
			response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
			return null;
		}

		if (lastModified != -1) {
			response.setDateHeader("Last-Modified", lastModified);
		}

		return downloadController.handleRequest(request, response);
	}

	public ModelAndView stream(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isStreamRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to play files.");
			return null;
		}

		streamController.handleRequest(request, response, false);
		return null;
	}

	public ModelAndView hls(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isStreamRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to play files.");
			return null;
		}
		int id = getRequiredIntParameter(request, "id");
		MediaFile video = mediaFileDao.getMediaFile(id);
		if (video == null || video.isDirectory()) {
			error(request, response, ErrorCode.NOT_FOUND, "Video not found.");
			return null;
		}
		//        if (!securityService.isFolderAccessAllowed(video, user.getUsername())) {
		//            error(request, response, ErrorCode.NOT_AUTHORIZED, "Access denied");
		//            return null;
		//        }
		hlsController.handleRequest(request, response);
		return null;
	}

	public ModelAndView getCaptions(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		int userGroupId = securityService.getCurrentUserGroupId(request);

		int id = ServletRequestUtils.getRequiredIntParameter(request, "id");
		MediaFile video = this.mediaFileDao.getMediaFile(id);
		if ((video == null) || (video.isDirectory())) {
			error(request, response, ErrorCode.NOT_FOUND, "Video not found.");
			return null;
		}
		if (!this.securityService.isAccessAllowed(video, userGroupId)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Access denied");
			return null;
		}
		this.captionsController.handleRequest(request, response, false);
		return null;
	}

	public void scrobble(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		Player player = playerService.getPlayer(request, response);

		boolean submission = getBooleanParameter(request, "submission", true);
		int[] ids = getRequiredIntParameters(request, "id");
		long[] times = getLongParameters(request, "time");
		if (times.length > 0 && times.length != ids.length) {
			error(request, response, ErrorCode.GENERIC, "Wrong number of timestamps: " + times.length);
			return;
		}

		for (int i = 0; i < ids.length; i++) {
			int id = ids[i];
			MediaFile file = mediaFileService.getMediaFile(id);
			if (file == null) {
				LOG.warn("File to scrobble not found: " + id);
				continue;
			}
			Date time = times.length == 0 ? null : new Date(times[i]);

			statusService.addRemotePlay(new PlayStatus(file, player, time == null ? new Date() : time));
			mediaFileService.incrementPlayCount(file);
			if (settingsService.getUserSettings(player.getUsername()).isLastFmEnabled()) {
				audioScrobblerService.register(file, player.getUsername(), submission, time);
			}
		}

		writeEmptyResponse(request, response);
	}

	public void star(HttpServletRequest request, HttpServletResponse response) throws Exception {
		starOrUnstar(request, response, true);
	}

	public void unstar(HttpServletRequest request, HttpServletResponse response) throws Exception {
		starOrUnstar(request, response, false);
	}

	private void starOrUnstar(HttpServletRequest request, HttpServletResponse response, boolean star) throws Exception {
		request = wrapRequest(request);

		String username = securityService.getCurrentUser(request).getUsername();
		for (int id : getIntParameters(request, "id")) {
			MediaFile mediaFile = mediaFileDao.getMediaFile(id);
			if (mediaFile == null) {
				error(request, response, ErrorCode.NOT_FOUND, "Media file not found: " + id);
				return;
			}
			if (star) {
				mediaFileDao.starMediaFile(id, username);
			} else {
				mediaFileDao.unstarMediaFile(id, username);
			}
		}
		for (int albumId : getIntParameters(request, "albumId")) {
			Album album = albumDao.getAlbum(albumId);
			if (album == null) {
				error(request, response, ErrorCode.NOT_FOUND, "Album not found: " + albumId);
				return;
			}
			if (star) {
				albumDao.starAlbum(albumId, username);
			} else {
				albumDao.unstarAlbum(albumId, username);
			}
		}
		for (int artistId : getIntParameters(request, "artistId")) {
			Artist artist = artistDao.getArtist(artistId);
			if (artist == null) {
				error(request, response, ErrorCode.NOT_FOUND, "Artist not found: " + artistId);
				return;
			}
			if (star) {
				artistDao.starArtist(artistId, username);
			} else {
				artistDao.unstarArtist(artistId, username);
			}
		}

		writeEmptyResponse(request, response);
	}

	public void getStarred(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		
		//TODO: Integer musicFolderId = getIntParameter(request, "musicFolderId");

		Starred result = new Starred();
		for (MediaFile artist : mediaFileDao.getStarredDirectories(0, Integer.MAX_VALUE, username)) {
			result.getArtist().add(createJaxbArtist(artist, username));
		}
		for (MediaFile album : mediaFileDao.getStarredAlbums(0, Integer.MAX_VALUE, username)) {
			result.getAlbum().add(createJaxbChild(player, album, username));
		}
		for (MediaFile song : mediaFileDao.getStarredFiles(0, Integer.MAX_VALUE, username)) {
			result.getSong().add(createJaxbChild(player, song, username));
		}
		Response res = createResponse();
		res.setStarred(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getStarred2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		
		//TODO: Integer musicFolderId = getIntParameter(request, "musicFolderId");

		Starred2 result = new Starred2();
		for (Artist artist : artistDao.getStarredArtists(0, Integer.MAX_VALUE, username)) {
			result.getArtist().add(createJaxbArtist(new ArtistID3(), artist, username));
		}
		for (Album album : albumDao.getStarredAlbums(0, Integer.MAX_VALUE, username)) {
			result.getAlbum().add(createJaxbAlbum(new AlbumID3(), album, username));
		}
		for (MediaFile song : mediaFileDao.getStarredFiles(0, Integer.MAX_VALUE, username)) {
			result.getSong().add(createJaxbChild(player, song, username));
		}
		Response res = createResponse();
		res.setStarred2(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getPodcasts(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		boolean includeEpisodes = getBooleanParameter(request, "includeEpisodes", true);
		Integer channelId = getIntParameter(request, "id");

		Podcasts result = new Podcasts();

		for (PodcastChannel channel : podcastService.getAllChannels()) {
			if (channelId == null || channelId.equals(channel.getId())) {

				org.subsonic.restapi.PodcastChannel c = new org.subsonic.restapi.PodcastChannel();
				result.getChannel().add(c);

				c.setId(String.valueOf(channel.getId()));
				c.setUrl(channel.getUrl());
				c.setStatus(PodcastStatus.valueOf(channel.getStatus().name()));
				c.setTitle(channel.getTitle());
				c.setDescription(channel.getDescription());
				c.setCoverArt(CoverArtController.PODCAST_COVERART_PREFIX + channel.getId());
				c.setOriginalImageUrl(channel.getImageUrl());
				c.setErrorMessage(channel.getErrorMessage());

				if (includeEpisodes) {
					List<PodcastEpisode> episodes = podcastService.getEpisodes(channel.getId());
					for (PodcastEpisode episode : episodes) {
						c.getEpisode().add(createJaxbPodcastEpisode(player, username, episode));
					}
				}
			}
		}
		Response res = createResponse();
		res.setPodcasts(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getNewestPodcasts(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);

		int count = getIntParameter(request, "count", 20);
		NewestPodcasts result = new NewestPodcasts();

		for (PodcastEpisode episode : podcastService.getNewestEpisodes(count)) {
			result.getEpisode().add(createJaxbPodcastEpisode(player, username, episode));
		}

		Response res = createResponse();
		res.setNewestPodcasts(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	private org.subsonic.restapi.PodcastEpisode createJaxbPodcastEpisode(Player player, String username, PodcastEpisode episode) {
		org.subsonic.restapi.PodcastEpisode e = new org.subsonic.restapi.PodcastEpisode();

		String path = episode.getPath();
		if (path != null) {
			MediaFile mediaFile = mediaFileService.getMediaFile(path);
			if (mediaFile == null) {
				LOG.warn("File for PodcastEpisode not found or not readable: " + path);
				return null;
			}
			e = createJaxbChild(new org.subsonic.restapi.PodcastEpisode(), player, mediaFile, username);
			e.setStreamId(String.valueOf(mediaFile.getId()));
		}

		e.setId(String.valueOf(episode.getId()));  // Overwrites the previous "id" attribute.
		e.setChannelId(String.valueOf(episode.getChannelId()));
		e.setStatus(PodcastStatus.valueOf(episode.getStatus().name()));
		e.setTitle(episode.getTitle());
		e.setDescription(episode.getDescription());
		e.setPublishDate(subsonicJaxbWriter.convertDate(episode.getPublishDate()));
		return e;
	}

	public void refreshPodcasts(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isPodcastRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to administrate podcasts.");
			return;
		}
		podcastService.refreshAllChannels(true);
		writeEmptyResponse(request, response);
	}

	public void createPodcastChannel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isPodcastRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to administrate podcasts.");
			return;
		}

		String url = getRequiredStringParameter(request, "url");
		podcastService.createChannel(url);
		writeEmptyResponse(request, response);
	}

	public void deletePodcastChannel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isPodcastRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to administrate podcasts.");
			return;
		}

		int id = getRequiredIntParameter(request, "id");
		podcastService.deleteChannel(id);
		writeEmptyResponse(request, response);
	}

	public void deletePodcastEpisode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isPodcastRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to administrate podcasts.");
			return;
		}

		int id = getRequiredIntParameter(request, "id");
		podcastService.deleteEpisode(id, true);
		writeEmptyResponse(request, response);
	}

	public void downloadPodcastEpisode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isPodcastRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to administrate podcasts.");
			return;
		}

		int id = getRequiredIntParameter(request, "id");
		PodcastEpisode episode = podcastService.getEpisode(id, true);
		if (episode == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Podcast episode " + id + " not found.");
			return;
		}

		podcastService.downloadEpisode(episode);
		writeEmptyResponse(request, response);
	}

	public void getInternetRadioStations(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		InternetRadioStations result = new InternetRadioStations();
		for (InternetRadio radio : settingsService.getAllInternetRadios()) {
			InternetRadioStation i = new InternetRadioStation();
			i.setId(String.valueOf(radio.getId()));
			i.setName(radio.getName());
			i.setStreamUrl(radio.getStreamUrl());
			i.setHomePageUrl(radio.getHomepageUrl());
			result.getInternetRadioStation().add(i);
		}
		Response res = createResponse();
		res.setInternetRadioStations(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void getBookmarks(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);

		Bookmarks result = new Bookmarks();
		for (Bookmark bookmark : bookmarkDao.getBookmarks(username)) {
			org.subsonic.restapi.Bookmark b = new org.subsonic.restapi.Bookmark();
			result.getBookmark().add(b);
			b.setPosition(bookmark.getPositionMillis());
			b.setUsername(bookmark.getUsername());
			b.setComment(bookmark.getComment());
			b.setCreated(subsonicJaxbWriter.convertDate(bookmark.getCreated()));
			b.setChanged(subsonicJaxbWriter.convertDate(bookmark.getChanged()));

			MediaFile mediaFile = mediaFileService.getMediaFile(bookmark.getMediaFileId());
			b.setEntry(createJaxbChild(player, mediaFile, username));
		}

		Response res = createResponse();
		res.setBookmarks(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void createBookmark(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);
		int mediaFileId = getRequiredIntParameter(request, "id");
		long position = getRequiredLongParameter(request, "position");
		String comment = request.getParameter("comment");
		Date now = new Date();

		Bookmark bookmark = new Bookmark(0, mediaFileId, position, username, comment, now, now);
		bookmarkDao.createOrUpdateBookmark(bookmark);
		refreshBookmarkCache();
		writeEmptyResponse(request, response);
	}

	public void deleteBookmark(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		String username = securityService.getCurrentUsername(request);
		int mediaFileId = getRequiredIntParameter(request, "id");
		bookmarkDao.deleteBookmark(username, mediaFileId);
		refreshBookmarkCache();

		writeEmptyResponse(request, response);
	}

	public void getPlayQueue(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);
		Player player = playerService.getPlayer(request, response);

		SavedPlayQueue playQueue = playQueueDao.getPlayQueue(username);
		if (playQueue == null) {
			writeEmptyResponse(request, response);
			return;
		}

		org.subsonic.restapi.PlayQueue restPlayQueue = new org.subsonic.restapi.PlayQueue();
		restPlayQueue.setUsername(playQueue.getUsername());
		restPlayQueue.setCurrent(playQueue.getCurrentMediaFileId());
		restPlayQueue.setPosition(playQueue.getPositionMillis());
		restPlayQueue.setChanged(subsonicJaxbWriter.convertDate(playQueue.getChanged()));
		restPlayQueue.setChangedBy(playQueue.getChangedBy());

		for (Integer mediaFileId : playQueue.getMediaFileIds()) {
			MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
			if (mediaFile != null) {
				restPlayQueue.getEntry().add(createJaxbChild(player, mediaFile, username));
			}
		}

		Response res = createResponse();
		res.setPlayQueue(restPlayQueue);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void savePlayQueue(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String username = securityService.getCurrentUsername(request);
		List<Integer> mediaFileIds = Util.toIntegerList(getIntParameters(request, "id"));
		Integer current = getIntParameter(request, "current");
		Long position = getLongParameter(request, "position");
		Date changed = new Date();
		String changedBy = getRequiredStringParameter(request, "c");

		if (!mediaFileIds.contains(current)) {
			error(request, response, ErrorCode.GENERIC, "Current track is not included in play queue");
			return;
		}

		SavedPlayQueue playQueue = new SavedPlayQueue(null, username, mediaFileIds, current, position, changed, changedBy);
		playQueueDao.savePlayQueue(playQueue);
		writeEmptyResponse(request, response);
	}

	public void getShares(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);
		User user = securityService.getCurrentUser(request);

		Shares result = new Shares();
		for (Share share : shareService.getSharesForUser(user)) {
			org.subsonic.restapi.Share s = createJaxbShare(share);
			result.getShare().add(s);

			for (MediaFile mediaFile : shareService.getSharedFiles(share.getId())) {
				s.getEntry().add(createJaxbChild(player, mediaFile, username));
			}
		}
		Response res = createResponse();
		res.setShares(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void createShare(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Player player = playerService.getPlayer(request, response);
		String username = securityService.getCurrentUsername(request);

		User user = securityService.getCurrentUser(request);
		if (!user.isShareRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to share media.");
			return;
		}

		if (!settingsService.isUrlRedirectionEnabled()) {
			error(request, response, ErrorCode.GENERIC, "Sharing is only supported for *.subsonic.org domain names.");
			return;
		}

		List<MediaFile> files = new ArrayList<MediaFile>();
		for (int id : getRequiredIntParameters(request, "id")) {
			files.add(mediaFileService.getMediaFile(id));
		}

		// TODO: Update api.jsp

		Share share = shareService.createShare(request, files);
		share.setDescription(request.getParameter("description"));
		long expires = getLongParameter(request, "expires", 0L);
		if (expires != 0) {
			share.setExpires(new Date(expires));
		}
		shareService.updateShare(share);

		Shares result = new Shares();
		org.subsonic.restapi.Share s = createJaxbShare(share);
		result.getShare().add(s);

		for (MediaFile mediaFile : shareService.getSharedFiles(share.getId())) {
			s.getEntry().add(createJaxbChild(player, mediaFile, username));
		}

		Response res = createResponse();
		res.setShares(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}

	public void deleteShare(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		int id = getRequiredIntParameter(request, "id");

		Share share = shareService.getShareById(id);
		if (share == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Shared media not found.");
			return;
		}
		if (!user.isAdminRole() && !share.getUsername().equals(user.getUsername())) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Not authorized to delete shared media.");
			return;
		}

		shareService.deleteShare(id);
		writeEmptyResponse(request, response);
	}

	public void updateShare(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		int id = getRequiredIntParameter(request, "id");

		Share share = shareService.getShareById(id);
		if (share == null) {
			error(request, response, ErrorCode.NOT_FOUND, "Shared media not found.");
			return;
		}
		if (!user.isAdminRole() && !share.getUsername().equals(user.getUsername())) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Not authorized to modify shared media.");
			return;
		}

		share.setDescription(request.getParameter("description"));
		String expiresString = request.getParameter("expires");
		if (expiresString != null) {
			long expires = Long.parseLong(expiresString);
			share.setExpires(expires == 0L ? null : new Date(expires));
		}
		shareService.updateShare(share);
		writeEmptyResponse(request, response);
	}

	private org.subsonic.restapi.Share createJaxbShare(Share share) {
		org.subsonic.restapi.Share result = new org.subsonic.restapi.Share();
		result.setId(String.valueOf(share.getId()));
		result.setUrl(shareService.getShareUrl(share));
		result.setUsername(share.getUsername());
		result.setCreated(subsonicJaxbWriter.convertDate(share.getCreated()));
		result.setVisitCount(share.getVisitCount());
		result.setDescription(share.getDescription());
		result.setExpires(subsonicJaxbWriter.convertDate(share.getExpires()));
		result.setLastVisited(subsonicJaxbWriter.convertDate(share.getLastVisited()));
		return result;
	}

	public ModelAndView getCoverArt(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		return coverArtController.handleRequest(request, response, false);
	}

	public ModelAndView getAvatar(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		return avatarController.handleRequest(request, response);
	}

	public void changePassword(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		String username = getRequiredStringParameter(request, "username");
		String password = decrypt(getRequiredStringParameter(request, "password"));

		User authUser = securityService.getCurrentUser(request);

		boolean allowed = authUser.isAdminRole()
				|| username.equals(authUser.getUsername()) && authUser.isSettingsRole();

		if (!allowed) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, authUser.getUsername() + " is not authorized to change password for " + username);
			return;
		}

		User user = securityService.getUserByName(username);
		user.setPassword(password);
		securityService.updateUser(user);

		writeEmptyResponse(request, response);
	}


	public void getUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		String username = getRequiredStringParameter(request, "username");

		User currentUser = securityService.getCurrentUser(request);
		if (!username.equals(currentUser.getUsername()) && !currentUser.isAdminRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, currentUser.getUsername() + " is not authorized to get details for other users.");
			return;
		}

		User requestedUser = securityService.getUserByName(username);
		if (requestedUser == null) {
			error(request, response, ErrorCode.NOT_FOUND, "No such user: " + username);
			return;
		}

		Response res = createResponse();
		res.setUser(createJaxbUser(requestedUser));
		subsonicJaxbWriter.writeResponse(request, response, res);
	}


	public void getUsers(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);

		User currentUser = securityService.getCurrentUser(request);
		if (!currentUser.isAdminRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, currentUser.getUsername() + " is not authorized to get details for other users.");
			return;
		}

		Users result = new Users();
		for (User user : securityService.getAllUsers()) {
			result.getUser().add(createJaxbUser(user));
		}

		Response res = createResponse();
		res.setUsers(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}


	private org.subsonic.restapi.User createJaxbUser(User user) {
		UserSettings userSettings = settingsService.getUserSettings(user.getUsername());

		org.subsonic.restapi.User result = new org.subsonic.restapi.User();
		result.setUsername(user.getUsername());
		result.setEmail(user.getEmail());
		result.setScrobblingEnabled(userSettings.isLastFmEnabled());
		result.setAdminRole(user.isAdminRole());
		result.setSettingsRole(user.isSettingsRole());
		result.setDownloadRole(user.isDownloadRole());
		result.setUploadRole(user.isUploadRole());
		result.setPlaylistRole(true);  // Since 1.8.0
		result.setCoverArtRole(user.isCoverArtRole());
		result.setCommentRole(user.isCommentRole());
		result.setPodcastRole(user.isPodcastRole());
		result.setStreamRole(user.isStreamRole());
		result.setJukeboxRole(user.isJukeboxRole());
		result.setShareRole(user.isShareRole());
		result.setVideoConversionRole(user.isVideoConversionRole());
		result.setAvatarLastChanged(this.subsonicJaxbWriter.convertDate(getAvatarLastChanged(userSettings)));
		TranscodeScheme transcodeScheme = userSettings.getTranscodeScheme();
		if (transcodeScheme != null && transcodeScheme != TranscodeScheme.OFF) {
			result.setMaxBitRate(transcodeScheme.getMaxBitRate());
		}
		//        List<MusicFolder> musicFolders = settingsService.getMusicFoldersForUser(user.getUsername());
		//        for (MusicFolder musicFolder : musicFolders) {
		//            result.getFolder().add(musicFolder.getId());
		//        }
		return result;
	}

	private Date getAvatarLastChanged(UserSettings userSettings) {
		Avatar avatar = null;
		if (userSettings.getAvatarScheme() == AvatarScheme.CUSTOM) {
			avatar = this.settingsService.getCustomAvatar(userSettings.getUsername());
		} else if (userSettings.getAvatarScheme() == AvatarScheme.SYSTEM) {
			avatar = this.settingsService.getSystemAvatar(userSettings.getSystemAvatarId().intValue());
		}
		if (avatar == null) {
			return userSettings.getChanged();
		}
		return new Date(Math.max(avatar.getCreatedDate().getTime(), userSettings.getChanged().getTime()));
	}    

	public void createUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isAdminRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to create new users.");
			return;
		}

		UserSettingsCommand command = new UserSettingsCommand();
		command.setUsername(getRequiredStringParameter(request, "username"));
		command.setPassword(decrypt(getRequiredStringParameter(request, "password")));
		command.setEmail(getRequiredStringParameter(request, "email"));
		command.setLdapAuthenticated(getBooleanParameter(request, "ldapAuthenticated", false));
		command.setAdminRole(getBooleanParameter(request, "adminRole", false));
		command.setCommentRole(getBooleanParameter(request, "commentRole", false));
		command.setCoverArtRole(getBooleanParameter(request, "coverArtRole", false));
		command.setDownloadRole(getBooleanParameter(request, "downloadRole", false));
		command.setStreamRole(getBooleanParameter(request, "streamRole", true));
		command.setUploadRole(getBooleanParameter(request, "uploadRole", false));
		command.setJukeboxRole(getBooleanParameter(request, "jukeboxRole", false));
		command.setPodcastRole(getBooleanParameter(request, "podcastRole", false));
		command.setSettingsRole(getBooleanParameter(request, "settingsRole", true));
		command.setShareRole(getBooleanParameter(request, "shareRole", false));
		command.setVideoConversionRole(getBooleanParameter(request, "videoConversionRole", false));
		command.setTranscodeSchemeName(TranscodeScheme.OFF.name());

		int[] folderIds = ServletRequestUtils.getIntParameters(request, "musicFolderId");
		if (folderIds.length == 0) {
			folderIds = Util.toIntArray(MusicFolder.toIdList(settingsService.getAllMusicFolders()));
		}
		//        command.setAllowedMusicFolderIds(folderIds);

		userSettingsController.createUser(command);
		writeEmptyResponse(request, response);
	}


	public void updateUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isAdminRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to update users.");
			return;
		}

		String requestedUsername = getRequiredStringParameter(request, "username");
		User requestedUser = securityService.getUserByName(requestedUsername);
		
		if (requestedUser == null) {
			error(request, response, ErrorCode.NOT_FOUND, "No such user: " + requestedUsername);
			return;
		} else if (User.USERNAME_ADMIN.equals(requestedUsername)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Not allowed to change admin user");
			return;
		}

		UserSettingsCommand command = new UserSettingsCommand();
		command.setUsername(requestedUsername);
		command.setEmail(getStringParameter(request, "email", requestedUser.getEmail()));
		command.setLdapAuthenticated(getBooleanParameter(request, "ldapAuthenticated", requestedUser.isLdapAuthenticated()));
		command.setAdminRole(getBooleanParameter(request, "adminRole", requestedUser.isAdminRole()));
		command.setCommentRole(getBooleanParameter(request, "commentRole", requestedUser.isCommentRole()));
		command.setCoverArtRole(getBooleanParameter(request, "coverArtRole", requestedUser.isCoverArtRole()));
		command.setDownloadRole(getBooleanParameter(request, "downloadRole", requestedUser.isDownloadRole()));
		command.setStreamRole(getBooleanParameter(request, "streamRole", requestedUser.isDownloadRole()));
		command.setUploadRole(getBooleanParameter(request, "uploadRole", requestedUser.isUploadRole()));
		command.setJukeboxRole(getBooleanParameter(request, "jukeboxRole", requestedUser.isJukeboxRole()));
		command.setPodcastRole(getBooleanParameter(request, "podcastRole", requestedUser.isPodcastRole()));
		command.setSettingsRole(getBooleanParameter(request, "settingsRole", requestedUser.isSettingsRole()));
		command.setShareRole(getBooleanParameter(request, "shareRole", requestedUser.isShareRole()));
		command.setVideoConversionRole(getBooleanParameter(request, "videoConversionRole", requestedUser.isVideoConversionRole()));
		command.setAudioConversionRole(getBooleanParameter(request, "audioConversionRole", requestedUser.isAudioConversionRole()));

		// int maxBitRate = getIntParameter(request, "maxBitRate", s.getTranscodeScheme().getMaxBitRate());
		// command.setTranscodeSchemeName(TranscodeScheme.fromMaxBitRate(maxBitRate).name());

		if (hasParameter(request, "password")) {
			command.setPassword(decrypt(getRequiredStringParameter(request, "password")));
			command.setPasswordChange(true);
		}

		// int[] folderIds = ServletRequestUtils.getIntParameters(request, "musicFolderId");
		// if (folderIds.length == 0) {
		// folderIds = Util.toIntArray(MusicFolder.toIdList(settingsService.getMusicFoldersForUser(username)));
		// }
		// command.setAllowedMusicFolderIds(folderIds);

		userSettingsController.updateUser(command);
		writeEmptyResponse(request, response);
	}


	private boolean hasParameter(HttpServletRequest request, String name) {
		return request.getParameter(name) != null;
	}


	public void deleteUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		User user = securityService.getCurrentUser(request);
		if (!user.isAdminRole()) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, user.getUsername() + " is not authorized to delete users.");
			return;
		}

		String username = getRequiredStringParameter(request, "username");
		if (User.USERNAME_ADMIN.equals(username)) {
			error(request, response, ErrorCode.NOT_AUTHORIZED, "Not allowed to delete admin user");
			return;
		}

		securityService.deleteUser(username);

		writeEmptyResponse(request, response);
	}


	public void getChatMessages(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		long since = getLongParameter(request, "since", 0L);

		ChatMessages result = new ChatMessages();
		for (ChatService.Message message : chatService.getMessages(0L).getMessages()) {
			long time = message.getDate().getTime();
			if (time > since) {
				ChatMessage c = new ChatMessage();
				result.getChatMessage().add(c);
				c.setUsername(message.getUsername());
				c.setTime(time);
				c.setMessage(message.getContent());
			}
		}
		Response res = createResponse();
		res.setChatMessages(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}


	public void addChatMessage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		chatService.doAddMessage(getRequiredStringParameter(request, "message"), request);
		writeEmptyResponse(request, response);
	}


	public void getLyrics(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		String artist = request.getParameter("artist");
		String title = request.getParameter("title");
		LyricsInfo lyrics = lyricsService.getLyrics(artist, title);

		Lyrics result = new Lyrics();
		result.setArtist(lyrics.getArtist());
		result.setTitle(lyrics.getTitle());
		result.setContent(lyrics.getLyrics());

		Response res = createResponse();
		res.setLyrics(result);
		subsonicJaxbWriter.writeResponse(request, response, res);
	}


	public void setRating(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request = wrapRequest(request);
		Integer rating = getRequiredIntParameter(request, "rating");
		if (rating == 0) {
			rating = null;
		}

		int id = getRequiredIntParameter(request, "id");
		MediaFile mediaFile = mediaFileService.getMediaFile(id);
		if (mediaFile == null) {
			error(request, response, ErrorCode.NOT_FOUND, "File not found: " + id);
			return;
		}

		String username = securityService.getCurrentUsername(request);
		ratingService.setRatingForUser(username, mediaFile, rating);

		writeEmptyResponse(request, response);
	}


	private HttpServletRequest wrapRequest(HttpServletRequest request) {
		return wrapRequest(request, false);
	}


	private HttpServletRequest wrapRequest(final HttpServletRequest request, boolean jukebox) {
		final String playerId = createPlayerIfNecessary(request, jukebox);
		return new HttpServletRequestWrapper(request) {
			@Override
			public String getParameter(String name) {
				// Returns the correct player to be used in PlayerService.getPlayer()
				if ("player".equals(name)) {
					return playerId;
				}

				// Support old style ID parameters.
				if ("id".equals(name)) {
					return mapId(request.getParameter("id"));
				}

				return super.getParameter(name);
			}
		};
	}


	private String mapId(String id) {
		if (id == null || id.startsWith(CoverArtController.ALBUM_COVERART_PREFIX) ||
				id.startsWith(CoverArtController.ARTIST_COVERART_PREFIX) || StringUtils.isNumeric(id)) {
			return id;
		}

		try {
			String path = StringUtil.utf8HexDecode(id);
			MediaFile mediaFile = mediaFileService.getMediaFile(path);
			return String.valueOf(mediaFile.getId());
		} catch (Exception x) {
			return id;
		}
	}


	private Response createResponse() {
		return subsonicJaxbWriter.createResponse(true);
	}


	private Response createStatusResponse() {
		return subsonicJaxbWriter.createStatusResponse(true);
	}


	private void writeEmptyResponse(HttpServletRequest request, HttpServletResponse response) throws Exception {
		subsonicJaxbWriter.writeResponse(request, response, createResponse());
	}


	public void error(HttpServletRequest request, HttpServletResponse response, ErrorCode code, String message) throws Exception {
		subsonicJaxbWriter.writeErrorResponse(request, response, code, message);
	}


	private String createPlayerIfNecessary(HttpServletRequest request, boolean jukebox) {
		String username = request.getRemoteUser();
		String clientId = request.getParameter("c");
		if (jukebox) {
			clientId += "-jukebox";
		}

		List<Player> players = playerService.getPlayersForUserAndClientId(username, clientId);

		// If not found, create it.
		if (players.isEmpty()) {
			Player player = new Player();
			player.setIpAddress(request.getRemoteAddr());
			player.setUsername(username);
			player.setClientId(clientId);
			player.setName(clientId);
			player.setTechnology(jukebox ? PlayerTechnology.JUKEBOX : PlayerTechnology.EXTERNAL_WITH_PLAYLIST);
			playerService.createPlayer(player);
			players = playerService.getPlayersForUserAndClientId(username, clientId);
		}

		// Return the player ID.
		return !players.isEmpty() ? players.get(0).getId() : null;
	}


	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}


	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}


	public void setPlayerService(PlayerService playerService) {
		this.playerService = playerService;
	}


	public void setTranscodingService(TranscodingService transcodingService) {
		this.transcodingService = transcodingService;
	}


	public void setDownloadController(DownloadController downloadController) {
		this.downloadController = downloadController;
	}


	public void setCoverArtController(CoverArtController coverArtController) {
		this.coverArtController = coverArtController;
	}


	public void setUserSettingsController(UserSettingsController userSettingsController) {
		this.userSettingsController = userSettingsController;
	}


	public void setArtistsController(ArtistsController artistsController) {
		this.artistsController = artistsController;
	}


	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}


	public void setPlaylistService(PlaylistService playlistService) {
		this.playlistService = playlistService;
	}


	public void setStreamController(StreamController streamController) {
		this.streamController = streamController;
	}


	public void setHlsController(HLSController hlsController) {
		this.hlsController = hlsController;
	}


	public void setChatService(ChatService chatService) {
		this.chatService = chatService;
	}


	public void setLyricsService(LyricsService lyricsService) {
		this.lyricsService = lyricsService;
	}


	public void setPlayQueueService(PlayQueueService playQueueService) {
		this.playQueueService = playQueueService;
	}


	public void setJukeboxService(JukeboxService jukeboxService) {
		this.jukeboxService = jukeboxService;
	}


	public void setAudioScrobblerService(AudioScrobblerService audioScrobblerService) {
		this.audioScrobblerService = audioScrobblerService;
	}


	public void setPodcastService(PodcastService podcastService) {
		this.podcastService = podcastService;
	}


	public void setRatingService(RatingService ratingService) {
		this.ratingService = ratingService;
	}


	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}


	public void setShareService(ShareService shareService) {
		this.shareService = shareService;
	}


	public void setMediaFileService(MediaFileService mediaFileService) {
		this.mediaFileService = mediaFileService;
	}


	public void setAvatarController(AvatarController avatarController) {
		this.avatarController = avatarController;
	}


	public void setArtistDao(ArtistDao artistDao) {
		this.artistDao = artistDao;
	}


	public void setAlbumDao(AlbumDao albumDao) {
		this.albumDao = albumDao;
	}


	public void setMediaFileDao(MediaFileDao mediaFileDao) {
		this.mediaFileDao = mediaFileDao;
	}


	public void setMusicIndexService(MusicIndexService musicIndexService) {
		this.musicIndexService = musicIndexService;
	}


	public void setBookmarkDao(BookmarkDao bookmarkDao) {
		this.bookmarkDao = bookmarkDao;
	}


	public void setLastFmServiceBasic(LastFmServiceBasic lastFmServiceBasic) {
		this.lastFmServiceBasic = lastFmServiceBasic;
	}


	public void setPlayQueueDao(PlayQueueDao playQueueDao) {
		this.playQueueDao = playQueueDao;
	}


	public void setCaptionsController(CaptionsController captionsController) {
		this.captionsController = captionsController;
	}    

	public void setVideoConversionService(VideoConversionService videoConversionService) {
		this.videoConversionService = videoConversionService;
	}

	public static enum ErrorCode {

		GENERIC(0, "A generic error."),
		MISSING_PARAMETER(10, "Required parameter is missing."),
		PROTOCOL_MISMATCH_CLIENT_TOO_OLD(20, "Incompatible REST protocol version. Client must upgrade."),
		PROTOCOL_MISMATCH_SERVER_TOO_OLD(30, "Incompatible REST protocol version. Server must upgrade."),
		NOT_AUTHENTICATED(40, "Wrong username or password."),
		NOT_AUTHORIZED(50, "User is not authorized for the given operation."),
		NOT_LICENSED(60, "The trial period for the Madsonic server is over. Please upgrade to Madsonic Premium. Visit http://www.madsonic.org for details."),
		NOT_FOUND(70, "Requested data was not found.");

		private final int code;
		private final String message;

		ErrorCode(int code, String message) {
			this.code = code;
			this.message = message;
		}

		public int getCode() {
			return code;
		}

		public String getMessage() {
			return message;
		}
	}

	@SuppressWarnings("serial")
	private static class BookmarkKey extends Pair<String, Integer> {
		private BookmarkKey(String username, int mediaFileId) {
			super(username, mediaFileId);
		}

		static BookmarkKey forBookmark(Bookmark b) {
			return new BookmarkKey(b.getUsername(), b.getMediaFileId());
		}
	}
}
