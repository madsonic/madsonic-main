/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;
import org.madsonic.Logger;
import org.madsonic.domain.InternetRadio;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MusicFolder;
import org.madsonic.domain.MusicFolderContentMadsonic;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.MediaScannerService;
import org.madsonic.service.MusicIndexService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.madsonic.util.FileUtil;

/**
 * Controller for the browse artists page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class ArtistsController extends ParameterizableViewController {

    private static final Logger LOG = Logger.getLogger(ArtistsController.class);
	
    // Update this time if you want to force a refresh in clients.
    private static final Calendar LAST_COMPATIBILITY_TIME = Calendar.getInstance();

    static {
        LAST_COMPATIBILITY_TIME.set(2016, Calendar.JANUARY, 1, 0, 0, 0);
        LAST_COMPATIBILITY_TIME.set(Calendar.MILLISECOND, 0);
    }

    private MediaFileService mediaFileService;
    private MediaScannerService mediaScannerService;
    private SettingsService settingsService;
    private SecurityService securityService;
    private MusicIndexService musicIndexService;
    private PlayerService playerService;
    
    private final int DEFAULT_GROUP_ALL = SettingsService.DEFAULT_GROUP_ALL;
	private final int DEFAULT_GROUP_NONE = SettingsService.DEFAULT_GROUP_NONE;
    private final int DEFAULT_GROUP_MUSIC = SettingsService.DEFAULT_GROUP_MUSIC;
    private final int DEFAULT_GROUP_VIDEO = SettingsService.DEFAULT_GROUP_VIDEO;
    private final int DEFAULT_GROUP_MOVIES = SettingsService.DEFAULT_GROUP_MOVIES;
    private final int DEFAULT_GROUP_SERIES = SettingsService.DEFAULT_GROUP_SERIES;
    private final int DEFAULT_GROUP_IMAGES = SettingsService.DEFAULT_GROUP_IMAGES;    
    private final int DEFAULT_GROUP_TV = SettingsService.DEFAULT_GROUP_TV; 

    /**
     * Note: This class intentionally does not implement org.springframework.web.servlet.mvc.LastModified
     * as we don't need browser-side caching of artists.jsp.  This method is only used by RESTController.
     */
    public long getLastModified(HttpServletRequest request) {
        saveSelectedMusicFolder(request);

        if (mediaScannerService.isScanning()) {
            return -1L;
        }

        long lastModified = LAST_COMPATIBILITY_TIME.getTimeInMillis();
        String username = securityService.getCurrentUsername(request);

        // When was settings last changed?
        lastModified = Math.max(lastModified, settingsService.getSettingsChanged());

        // When was music folder(s) on disk last changed?
        List<MusicFolder> allMusicFolders = settingsService.getAllMusicFolders();
        MusicFolder selectedMusicFolder = getSelectedMusicFolder(request);
        if (selectedMusicFolder != null) {
            File file = selectedMusicFolder.getPath();
            lastModified = Math.max(lastModified, FileUtil.lastModified(file));
        } else {
            for (MusicFolder musicFolder : allMusicFolders) {
                File file = musicFolder.getPath();
                lastModified = Math.max(lastModified, FileUtil.lastModified(file));
            }
        }

        // When was music folder table last changed?
        for (MusicFolder musicFolder : allMusicFolders) {
            lastModified = Math.max(lastModified, musicFolder.getChanged().getTime());
        }

        // When was internet radio table last changed?
        for (InternetRadio internetRadio : settingsService.getAllInternetRadios()) {
            lastModified = Math.max(lastModified, internetRadio.getChanged().getTime());
        }

        // When was user settings last changed?
        UserSettings userSettings = settingsService.getUserSettings(username);
        lastModified = Math.max(lastModified, userSettings.getChanged().getTime());

        return lastModified;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        boolean refresh = ServletRequestUtils.getBooleanParameter(request, "refresh", false);
        if (refresh) {
         //   settingsService.clearMusicFolderCache();
        }

        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);
        int userGroupId = securityService.getCurrentUserGroupId(request);
        
        List<MusicFolder> allMediaFolders = settingsService.getMusicFoldersForGroup(userGroupId);

        map.put("player", playerService.getPlayer(request, response));
        map.put("scanning", mediaScannerService.isScanning());
        
        int selectedMusicFolderId = getSelectedMusicFolderId(request);
        String selectedMusicFolderName;
        
        List<MusicFolder> allFolders = new ArrayList<MusicFolder>();
        List<MusicFolder> groupedFolder = new ArrayList<MusicFolder>();
        
        List<MusicFolder> allOtherFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_NONE);
        List<MusicFolder> allMusicFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_MUSIC);
        List<MusicFolder> allVideoFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_VIDEO);
        List<MusicFolder> allMoviesFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_MOVIES);
        List<MusicFolder> allSeriesFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_SERIES);
        List<MusicFolder> allImagesFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_IMAGES);
        List<MusicFolder> allTVFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_TV);
        
        allFolders.addAll(allOtherFolders);
        allFolders.addAll(allMusicFolders);
        allFolders.addAll(allVideoFolders);
        allFolders.addAll(allMoviesFolders);
        allFolders.addAll(allSeriesFolders);
        allFolders.addAll(allImagesFolders);
        allFolders.addAll(allTVFolders);
        
        if ( allMusicFolders.size() > 0)  { map.put("MusicFolderEnabled", true); }
        if ( allVideoFolders.size() > 0)  { map.put("VideoFolderEnabled", true); }
        if ( allMoviesFolders.size() > 0) { map.put("MoviesFolderEnabled", true); }
        if ( allSeriesFolders.size() > 0) { map.put("SeriesFolderEnabled", true); }        
        if ( allImagesFolders.size() > 0) { map.put("ImagesFolderEnabled", true); }        
        if ( allTVFolders.size() > 0)     { map.put("TVFolderEnabled", true); }
        
        switch(selectedMusicFolderId){ 
        case -1: selectedMusicFolderName  = "allFolders"; groupedFolder = allMediaFolders; break; 
        case -2: selectedMusicFolderName = "allMusicFolders"; groupedFolder = allMusicFolders; break; 
        case -3: selectedMusicFolderName = "allVideoFolders"; groupedFolder = allVideoFolders; break; 
        case -4: selectedMusicFolderName = "allMoviesFolders"; groupedFolder = allMoviesFolders; break; 
        case -5: selectedMusicFolderName = "allSeriesFolders"; groupedFolder = allSeriesFolders; break; 
        case -6: selectedMusicFolderName = "allImagesFolders"; groupedFolder = allImagesFolders; break; 
        case -7: selectedMusicFolderName = "allTVFolders"; groupedFolder = allTVFolders; break; 
        default: selectedMusicFolderName  = ""; break; 
	    } 

        MusicFolder selectedMusicFolder = getSelectedMusicFolder(request);
        List<MusicFolder> musicFoldersToUse = selectedMusicFolder == null ? groupedFolder : Arrays.asList(selectedMusicFolder);
        
        List <String> _allGenres = mediaFileService.getArtistGenresforFolder(musicFoldersToUse, userGroupId);
		map.put("allGenres", _allGenres);
        String selectedGenre = getSelectedGenre(request);
		boolean genreFound = false;
		for (String s : _allGenres) {
			if (s.equals(selectedGenre)) { genreFound = true; }
		}
		if (genreFound == false) {
	        selectedGenre = null;
		}
		MusicFolderContentMadsonic musicFolderContent = musicIndexService.getMusicFolderContent(musicFoldersToUse, selectedGenre, "0" , refresh);
        List<MediaFile> singleSongs = musicFolderContent.getSingleSongs();
        mediaFileService.populateStarredDate(singleSongs, username);
        
        map.put("selectedMusicFolderId", selectedMusicFolderId);
        map.put("selectedMusicFolderName", selectedMusicFolderName);        
        map.put("selectedMusicFolder", selectedMusicFolder);
        map.put("selectedGenre", selectedGenre);
        map.put("musicFolders", allOtherFolders);
		
        map.put("shortcuts", musicIndexService.getShortcutsX(musicFoldersToUse));
        map.put("partyMode", userSettings.isPartyModeEnabled());
        map.put("organizeByFolderStructure", settingsService.isOrganizeByFolderStructure());
        map.put("visibility", userSettings.getMainVisibility());
        map.put("indexedArtists", musicFolderContent.getIndexedArtists());
        map.put("singleSongs", singleSongs);
        map.put("indexes", musicFolderContent.getIndexedArtists().keySet());
        map.put("user", securityService.getCurrentUser(request));

		map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 	
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));

        map.put("indexListSize", settingsService.getIndexListSize());
        
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }

    private String getSelectedGenre(HttpServletRequest request) {
        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
        return settings.getSelectedGenre();
    }    

    private int getSelectedMusicFolderId(HttpServletRequest request) {
        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
        return settings.getSelectedMusicFolderId();
    }    
    
    private MusicFolder getSelectedMusicFolder(HttpServletRequest request) {
        Integer musicFolderId = null;
		try {
			musicFolderId = ServletRequestUtils.getIntParameter(request, "musicFolderId");
		} catch (ServletRequestBindingException e) {
			// ignore
		}
        if (musicFolderId != null) {
			return settingsService.getMusicFolderById(musicFolderId);
		} else {
	        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
	        return settingsService.getMusicFolderById(settings.getSelectedMusicFolderId());
		}
    }    
    
    private boolean saveSelectedMusicFolder(HttpServletRequest request) {
        if (request.getParameter("musicFolderId") == null) {
            return false;
        }
        int musicFolderId = Integer.parseInt(request.getParameter("musicFolderId"));

        // Note: UserSettings.setChanged() is intentionally not called. This would break browser caching of the left frame.
        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
        settings.setSelectedMusicFolderId(musicFolderId);
        settingsService.updateUserSettings(settings);

        return true;
    }

    public void setMediaScannerService(MediaScannerService mediaScannerService) {
        this.mediaScannerService = mediaScannerService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setMusicIndexService(MusicIndexService musicIndexService) {
        this.musicIndexService = musicIndexService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }
}
