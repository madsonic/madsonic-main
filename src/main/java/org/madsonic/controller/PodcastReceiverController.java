/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

import org.madsonic.domain.PodcastChannel;
import org.madsonic.domain.PodcastEpisode;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.PodcastService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.madsonic.util.StringUtil;

/**
 * Controller for the "Podcast receiver" page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PodcastReceiverController extends ParameterizableViewController {

	private PodcastService podcastService;
	private SecurityService securityService;
	private SettingsService settingsService;

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		ModelAndView result = super.handleRequestInternal(request, response);

		Map<PodcastChannel, List<PodcastEpisode>> channels = new LinkedHashMap<PodcastChannel, List<PodcastEpisode>>();
		
		for (PodcastChannel channel : podcastService.getAllChannels()) {
			channels.put(channel, podcastService.getEpisodes(channel.getId()));
		}

		User user = securityService.getCurrentUser(request);
		UserSettings userSettings = settingsService.getUserSettings(user.getUsername());
		
		map.put("user", user);
		map.put("channels", channels);
		map.put("expandedChannels", StringUtil.parseInts(request.getParameter("expandedChannels")));
		map.put("buttonVisibility", userSettings.getButtonVisibility()); 
		map.put("licenseInfo", settingsService.getLicenseInfo());
		map.put("partyMode", userSettings.isPartyModeEnabled());
		
		map.put("customScrollbar", userSettings.isCustomScrollbarEnabled());
		map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
		
		result.addObject("model", map);
		return result;
	}

	public void setPodcastService(PodcastService podcastService) {
		this.podcastService = podcastService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}
}