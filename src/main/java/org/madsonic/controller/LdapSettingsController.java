/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.madsonic.command.LdapSettingsCommand;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.domain.LdapGroupSyncType;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.springframework.web.servlet.mvc.SimpleFormController;

/**
 * Controller for the page used to administrate ldap settings.
 *
 * @author Martin Karel
 */
@SuppressWarnings("deprecation")
public class LdapSettingsController extends SimpleFormController {

	private SettingsService settingsService; 
	private SecurityService securityService;    

	@SuppressWarnings("rawtypes")
	@Override
	protected Map referenceData(HttpServletRequest request) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
      
		User user = securityService.getCurrentUser(request);
		UserSettings userSettings = settingsService.getUserSettings(user.getUsername());
		
	    model.put("customScrollbar", userSettings.isCustomScrollbarEnabled());    
	    model.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
  
        return model;
	}      

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		LdapSettingsCommand command = new LdapSettingsCommand();
		command.setLdapEnabled(settingsService.isLdapEnabled());
		command.setLdapUrl(settingsService.getLdapUrl());
		
		command.setLdapSearchFilter(settingsService.getLdapSearchFilter());
		command.setLdapGroupSearchBase(settingsService.getLdapGroupSearchBase());
		command.setLdapGroupFilter(settingsService.getLdapGroupFilter());
		command.setLdapGroupAttrib(settingsService.getLdapGroupAttrib());
		command.setLdapManagerDn(settingsService.getLdapManagerDn());
		command.setLdapAutoShadowing(settingsService.isLdapAutoShadowing());
		command.setLdapAutoMapping(settingsService.isLdapAutoMapping());		
		command.setLdapGroupSyncType(settingsService.getLdapGroupSyncType().name());
		command.setLdapGroupAutoSync(settingsService.isLdapGroupAutoSync());
		command.setBrand(settingsService.getBrand());
		command.setLicenseInfo(settingsService.getLicenseInfo());
		return command;
	}

	@Override
	protected void doSubmitAction(Object comm) throws Exception {
		LdapSettingsCommand command = (LdapSettingsCommand) comm;
		settingsService.setLdapEnabled(command.isLdapEnabled());
		settingsService.setLdapUrl(command.getLdapUrl());
		settingsService.setLdapSearchFilter(command.getLdapSearchFilter());
		settingsService.setLdapGroupSearchBase(command.getLdapGroupSearchBase());
		settingsService.setLdapGroupFilter(command.getLdapGroupFilter());
		settingsService.setLdapGroupAttrib(command.getLdapGroupAttrib());
		settingsService.setLdapManagerDn(command.getLdapManagerDn());
		settingsService.setLdapAutoShadowing(command.isLdapAutoShadowing());
		settingsService.setLdapAutoMapping(command.isLdapAutoMapping());
        settingsService.setLdapGroupSyncType(LdapGroupSyncType.valueOf(command.getLdapGroupSyncType()));
        settingsService.setLdapGroupAutoSync(command.isLdapGroupAutoSync());
        
		command.setToast(true);

		if (StringUtils.isNotEmpty(command.getLdapManagerPassword())) {
			settingsService.setLdapManagerPassword(command.getLdapManagerPassword());
		}
		settingsService.save();
	}

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}    

}
