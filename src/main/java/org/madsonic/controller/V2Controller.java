/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import org.madsonic.Logger;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.domain.UserSettings;

import org.madsonic.service.PlaylistService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

/**
 * V2UI
 *
 * @author Martin Karel
 */
public class V2Controller extends MultiActionController {

    private static final Logger LOG = Logger.getLogger(V2Controller.class);

    private SecurityService securityService;
    private SettingsService settingsService;
    private PlaylistService playlistService;

    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
    	
        UserSettings userSettings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("pageTitle", settingsService.getPageTitle());
        map.put("brand", settingsService.getBrand());
        
        return new ModelAndView("index", "model", map);
    }
   
    public ModelAndView index2(HttpServletRequest request, HttpServletResponse response) {
    	
        return new ModelAndView("index2");
    }
    
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setPlaylistService(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }
}