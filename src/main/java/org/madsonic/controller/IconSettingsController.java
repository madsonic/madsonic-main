/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.command.IconCommand;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

/**
 * Controller for the icon top page.
 *
 * @author Martin Karel
 */
@SuppressWarnings("deprecation")
public class IconSettingsController extends SimpleFormController {

    private SettingsService settingsService;
    private SecurityService securityService;    
    
    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {
    Map<String, Object> model = new HashMap<String, Object>();
    
    User user = securityService.getCurrentUser(request);
    UserSettings userSettings = settingsService.getUserSettings(user.getUsername());

    model.put("customScrollbar", userSettings.isCustomScrollbarEnabled());    
    model.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
    
    return model;
    }    

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
    	
        IconCommand command = new IconCommand();
        
        command.setToast(false);
      	command.setReloadNeeded(false);
      
        command.setShowIconHome(settingsService.showIconHome());
        command.setShowIconIndex(settingsService.showIconIndex());
        command.setShowIconArtist(settingsService.showIconArtist());
        command.setShowIconPlaying(settingsService.showIconPlaying());
        command.setShowIconCover(settingsService.showIconCover());
        command.setShowIconBookmark(settingsService.showIconBookmark());
        command.setShowIconInternetRadio(settingsService.showIconInternetRadio());        
        command.setShowIconStarred(settingsService.showIconStarred());
        command.setShowIconLoved(settingsService.showIconLoved());
        command.setShowIconRadio(settingsService.showIconRadio());
        command.setShowIconPodcast(settingsService.showIconPodcast());
        command.setShowIconPodcastManage(settingsService.showIconPodcastManage());
        command.setShowIconSettings(settingsService.showIconSettings());
        command.setShowIconStatus(settingsService.showIconStatus());
        command.setShowIconSocial(settingsService.showIconSocial());
        command.setShowIconHistory(settingsService.showIconHistory());
        command.setShowIconStatistics(settingsService.showIconStatistics());
        command.setShowIconPlaylists(settingsService.showIconPlaylists());
        command.setShowIconPlaylistManage(settingsService.showIconPlaylistManage());
        command.setShowIconPlaylistEditor(settingsService.showIconPlaylistEditor());
        command.setShowIconMore(settingsService.showIconMore());
        command.setShowIconRandom(settingsService.showIconRandom());
        command.setShowIconAbout(settingsService.showIconAbout());
        command.setShowIconGenre(settingsService.showIconGenre());
        command.setShowIconMoods(settingsService.showIconMoods());
        command.setShowIconAdmins(settingsService.showIconAdmins());
        
        return command;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object com, BindException errors)
		throws Exception {
        IconCommand command = (IconCommand) com;
        
        command.setToast(true);
      	command.setReloadNeeded(true);
        
        settingsService.setshowIconHome(command.isShowIconHome());
        settingsService.setshowIconIndex(command.isShowIconIndex());        
        settingsService.setshowIconArtist(command.isShowIconArtist());
        settingsService.setshowIconPlaying(command.isShowIconPlaying());
        settingsService.setshowIconCover(command.isShowIconCover());        
        settingsService.setshowIconStarred(command.isShowIconStarred());
        settingsService.setshowIconLoved(command.isShowIconLoved());
        settingsService.setshowIconBookmark(command.isShowIconBookmark());
        settingsService.setshowIconInternetRadio(command.isShowIconInternetRadio());
        settingsService.setshowIconRadio(command.isShowIconRadio());
        settingsService.setshowIconPodcast(command.isShowIconPodcast());
        settingsService.setshowIconPodcastManage(command.isShowIconPodcastManage());
        settingsService.setshowIconSettings(command.isShowIconSettings());
        settingsService.setshowIconStatus(command.isShowIconStatus());
        settingsService.setshowIconSocial(command.isShowIconSocial());
        settingsService.setshowIconHistory(command.isShowIconHistory());
        settingsService.setshowIconStatistics(command.isShowIconStatistics());
        settingsService.setshowIconPlaylists(command.isShowIconPlaylists());
        settingsService.setshowIconPlaylistEditor(command.isShowIconPlaylistEditor());
        settingsService.setshowIconPlaylistManage(command.isShowIconPlaylistManage());
        settingsService.setshowIconMore(command.isShowIconMore()); 
        settingsService.setshowIconRandom(command.isShowIconRandom()); 
        settingsService.setshowIconGenre(command.isShowIconGenre());
        settingsService.setshowIconMoods(command.isShowIconMoods());
        settingsService.setshowIconAbout(command.isShowIconAbout());        
        settingsService.setshowIconAdmins(command.isShowIconAdmins());        
        settingsService.save();

        return new ModelAndView(getSuccessView(), errors.getModel());
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }
    
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }     
}