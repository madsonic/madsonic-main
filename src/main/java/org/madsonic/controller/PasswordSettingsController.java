/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import org.springframework.web.servlet.mvc.*;
import org.madsonic.service.*;
import org.madsonic.command.*;
import org.madsonic.domain.*;

import javax.servlet.http.*;

/**
 * Controller for the page used to change password.
 *
 * @author Sindre Mehus, Martin Karel
 */
@SuppressWarnings("deprecation")
public class PasswordSettingsController extends SimpleFormController {

    private SecurityService securityService;

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        PasswordSettingsCommand command = new PasswordSettingsCommand();
        User user = securityService.getCurrentUser(request);
        command.setUsername(user.getUsername());
        command.setLdapAuthenticated(user.isLdapAuthenticated());
        return command;
    }

    protected void doSubmitAction(Object comm) throws Exception {
        PasswordSettingsCommand command = (PasswordSettingsCommand) comm;
        
        User user = securityService.getUserByName(command.getUsername());
        user.setPassword(command.getPassword());
        securityService.setSecurePassword(user);
        securityService.updateUser(user);

        command.setPassword(null);
        command.setConfirmPassword(null);
        command.setToast(true);
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
