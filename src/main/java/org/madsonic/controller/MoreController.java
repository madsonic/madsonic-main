/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;

/**
 * Controller for the "more" page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MoreController extends ParameterizableViewController {

    private SecurityService securityService;
    
    private PlayerService playerService;
    private SettingsService settingsService;
    private MediaFileService mediaFileService;
	
	private static final File MADSONIC_PATH_WINDOWS = new File("\\");
    private static final File MADSONIC_PATH_OTHER = new File("/");

    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

		String uploadDirectory = null;
        File madsonicUpload = SettingsService.getMadsonicUpload();

		boolean isWindows = System.getProperty("os.name", "Windows").toLowerCase().startsWith("windows");
        File Uploadpath = isWindows ? MADSONIC_PATH_WINDOWS : MADSONIC_PATH_OTHER;

        uploadDirectory = new File(madsonicUpload.getPath()).getPath() + Uploadpath;

        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        
        UserSettings userSettings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
        map.put("user", securityService.getCurrentUser(request));
        
        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 		
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        
        map.put("uploadDirectory", uploadDirectory);
        
		if (request.getRequestURI().toLowerCase().contains("/upload.view".toLowerCase())) {
            result.setViewName("upload");
		}
        return result;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }
}