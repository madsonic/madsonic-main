/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.domain.CoverArtScheme;
import org.madsonic.domain.Player;
import org.madsonic.domain.PodcastChannel;
import org.madsonic.domain.PodcastEpisode;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.PlayerService;
import org.madsonic.service.PodcastService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

/**
 * Controller for the "Podcast channels" page.
 *
 * @author Sindre Mehus
 */
public class PodcastChannelsController extends ParameterizableViewController {

    private PodcastService podcastService;
    private SecurityService securityService;
    private SettingsService settingsService;
    private PlayerService playerService;	

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>();
        ModelAndView result = super.handleRequestInternal(request, response);

        Map<PodcastChannel, List<PodcastEpisode>> channels = new LinkedHashMap<PodcastChannel, List<PodcastEpisode>>();
        Map<Integer, PodcastChannel> channelMap = new HashMap<Integer, PodcastChannel>();
        for (PodcastChannel channel : podcastService.getAllChannels()) {
            channels.put(channel, podcastService.getEpisodes(channel.getId()));
            channelMap.put(channel.getId(), channel);
        }
        User user = securityService.getCurrentUser(request);
        UserSettings userSettings = settingsService.getUserSettings(user.getUsername());
        map.put("user", user);
        
        Player player = playerService.getPlayer(request, response);
        CoverArtScheme scheme = player.getCoverArtScheme();
        
        if (scheme != CoverArtScheme.OFF) {
		           map.put("coverArtSize", scheme.getSize());
          } else { map.put("coverArtSize", CoverArtScheme.LARGE.getSize());
        }
        map.put("coverArtHQ", settingsService.isCoverArtHQ());
        
        map.put("channels", channels);
        map.put("channelMap", channelMap);
        
        map.put("newestEpisodes", podcastService.getNewestEpisodes(30));
        map.put("licenseInfo", settingsService.getLicenseInfo());
        
		String viewAs = ServletRequestUtils.getStringParameter(request, "viewAs", userSettings.getViewAs());
		if (viewAs != userSettings.getViewAs()) {
			userSettings.setViewAs(viewAs);
			userSettings.setChanged(new Date());
			settingsService.updateUserSettings(userSettings);
		}
		map.put("viewAs", viewAs);
        
        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled());
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        
        result.addObject("model", map);
        return result;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }
    
    public void setPodcastService(PodcastService podcastService) {
        this.podcastService = podcastService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }
}
