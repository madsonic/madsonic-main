/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.domain.CoverArtScheme;
import org.madsonic.domain.Player;
import org.madsonic.domain.Playlist;
import org.madsonic.domain.SortDirection;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.PlayerService;
import org.madsonic.service.PlaylistService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

/**
 * Controller for the playlists page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PlaylistsController extends ParameterizableViewController {

	private SettingsService settingsService;	
	private SecurityService securityService;
	private PlaylistService playlistService;
	private PlayerService playerService;	

	static String SORT_DESC = SortDirection.DESC.getValue();
	static String SORT_ASC = SortDirection.ASC.getValue();

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("listType", request.getParameter("listType"));
		
		User user = securityService.getCurrentUser(request);
		String username = securityService.getCurrentUsername(request);
		UserSettings userSettings = settingsService.getUserSettings(username);
		map.put("user", user);
		
		String direction = request.getParameter("direction");
		if (direction == null) { direction = SORT_DESC;	}
		List<Playlist> playlists = playlistService.getReadablePlaylistsForUser(user.getUsername(), request.getParameter("listType"), direction);
		map.put("direction", switchDirection(direction));
		map.put("playlists", playlists);
		
		String viewAs = ServletRequestUtils.getStringParameter(request, "viewAs", userSettings.getViewAs());
		if (viewAs != userSettings.getViewAs()) {
			userSettings.setViewAs(viewAs);
			userSettings.setChanged(new Date());
			settingsService.updateUserSettings(userSettings);
		}
		map.put("viewAs", viewAs);
		
		Player player = playerService.getPlayer(request, response);
		CoverArtScheme scheme = player.getCoverArtScheme();
		map.put("coverArtSize", scheme != CoverArtScheme.OFF ? scheme.getSize() : CoverArtScheme.LARGE.getSize());
		map.put("coverArtHQ", settingsService.isCoverArtHQ());        
		map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));		
		map.put("customScrollbar", userSettings.isCustomScrollbarEnabled());

		ModelAndView result = super.handleRequestInternal(request, response);
		result.addObject("model", map);
		return result;
	}

	private String switchDirection(String direction) {
		if (direction.contains(SORT_DESC)) {
			direction = SORT_ASC;
		} else {
			if (direction.contains(SORT_ASC)){
				direction = SORT_DESC;
			}
		}	
		return direction;
	}

	public void setPlayerService(PlayerService playerService) {
		this.playerService = playerService;
	}

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}	

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public void setPlaylistService(PlaylistService playlistService) {
		this.playlistService = playlistService;
	}
}
