/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import org.madsonic.Logger;
import org.madsonic.dao.ShareDao;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.Player;
import org.madsonic.domain.Share;
import org.madsonic.domain.VideoConversion;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.TranscodingService;
import org.madsonic.service.VideoConversionService;
import org.madsonic.util.StringUtil;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller for the page used to play shared music (Twitter, Facebook etc).
 *
 * @author Sindre Mehus, Martin Karel
 */
public class ExternalPlayerController extends ParameterizableViewController {

    private static final Logger LOG = Logger.getLogger(ExternalPlayerController.class);
    private static final int GUEST_GROUP_ID = 1;
    
    private SettingsService settingsService;
    private SecurityService securityService;
    private PlayerService playerService;
    private ShareDao shareDao;
    private MediaFileService mediaFileService;
    private TranscodingService transcodingService;
    private VideoConversionService videoConversionService;
    private CaptionsController captionsController;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>();

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || !pathInfo.startsWith("/")) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }

        Share share = shareDao.getShareByName(pathInfo.substring(1));
        if (share != null && share.getExpires() != null && share.getExpires().before(new Date())) {
            share = null;
        }
        if (share != null) {
            share.setLastVisited(new Date());
            share.setVisitCount(share.getVisitCount() + 1);
            shareDao.updateShare(share);
        }

        Player player = playerService.getGuestPlayer(request);
        
        map.put("share", share);
        map.put("entries", getEntries(share, player));
        map.put("redirection", settingsService.isAppRedirectEnabled());
        map.put("redirectUrl", settingsService.getUrlRedirectUrl());
        map.put("player", player.getId());
        map.put("maxBitRate", securityService.getUserGroupVideoDefault(GUEST_GROUP_ID));
        
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }

    private List<Entry> getEntries(Share share, Player player) throws IOException {
        List<Entry> result = new ArrayList<Entry>();
    	if (share == null) {
    		return result;
    	}
        for (String path : shareDao.getSharedFiles(share.getId())) {
            try {
                MediaFile file = mediaFileService.getMediaFile(path);
                if (file.getFile().exists()) {
                    if (file.isDirectory()) {
                        for (MediaFile child : mediaFileService.getChildrenOf(file, true, false, true)) {
                            result.add(createEntry(child, player));
                        }
	                    } else {
                    	
                    	 if (file.isVideo()) {
                    		 if (file.getDurationSeconds() == null) {
                    			 file.setDurationSeconds(0);
                    			 
                    			 if (file.getData() != null) {
                    				 file.setData(StringUtil.getYoutubeVideoUrl() + file.getData());
                    			 }
                    		 }
                    	 }
                        result.add(createEntry(file, player));
                    }
                }
            } catch (Exception x) {
                LOG.warn("Couldn't read file " + path);
            }
        }
        return result;
    }
    
    private Entry createEntry(MediaFile file, Player player) {
    	
      boolean converted = isConverted(file);
      boolean streamable = (converted) || (this.videoConversionService.isStreamable(file));
      
      String contentType;
      
      if (file.isVideo()) {
        contentType = streamable ? "video/mp4" : "application/x-mpegurl";
      } else {
        contentType = StringUtil.getMimeType(this.transcodingService.getSuffix(player, file, null));
      }
      
      boolean hasCaptions = (file.isVideo()) && (this.captionsController.findCaptionsVideo(file) != null);
      return new Entry(file, contentType, converted, streamable, hasCaptions);
    }
    
    private boolean isConverted(MediaFile file) {
      VideoConversion conversion = this.videoConversionService.getVideoConversionForFile(file.getId());
      return (conversion != null) && (conversion.getStatus() == VideoConversion.Status.COMPLETED);
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public void setShareDao(ShareDao shareDao) {
        this.shareDao = shareDao;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }
    public void setTranscodingService(TranscodingService transcodingService) {
        this.transcodingService = transcodingService;
    }

    public void setCaptionsController(CaptionsController captionsController) {
        this.captionsController = captionsController;
    }
    public void setVideoConversionService(VideoConversionService videoConversionService) {
        this.videoConversionService = videoConversionService;
    }

    public static class Entry {
    	  private final MediaFile file;
    	  private final String contentType;
    	  private final boolean converted;
    	  private final boolean streamable;
    	  private final boolean captions;

        public Entry(MediaFile file, String contentType, boolean converted, boolean streamable, boolean captions) {
          this.file = file;
          this.contentType = contentType;
          this.converted = converted;
          this.streamable = streamable;
          this.captions = captions;
        }
        
        public MediaFile getFile()
        {
          return this.file;
        }
        
        public String getContentType()
        {
          return this.contentType;
        }
        
        public boolean isConverted()
        {
          return this.converted;
        }
        
        public boolean isStreamable()
        {
          return this.streamable;
        }
        
        public boolean isCaptions()
        {
          return this.captions;
        }
    }
}