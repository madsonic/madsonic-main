/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2017 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.service.PodcastService;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller for the "Podcast receiver" page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PodcastReceiverRefreshController extends AbstractController {

	private PodcastService podcastService;

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Integer channelId = ServletRequestUtils.getIntParameter(request, "channelId");

		if (channelId != null) {
			podcastService.refreshChannel(channelId, true);
			return new ModelAndView(new RedirectView("podcastChannel.view?id=" + channelId));
		} else {
			podcastService.refreshAllChannels(true);
			return new ModelAndView(new RedirectView("podcastChannels.view"));
		}
	}

	public void setPodcastService(PodcastService podcastService) {
		this.podcastService = podcastService;
	}
}
