/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.madsonic.command.AdvancedSettingsCommand;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.springframework.web.servlet.mvc.SimpleFormController;

/**
 * Controller for the page used to administrate advanced settings.
 *
 * @author Sindre Mehus, Martin Karel
 */
@SuppressWarnings("deprecation")
public class AdvancedSettingsController extends SimpleFormController {

	private SettingsService settingsService; 
	private SecurityService securityService;    

	@SuppressWarnings("rawtypes")
	@Override
	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		User user = securityService.getCurrentUser(request);
		UserSettings userSettings = settingsService.getUserSettings(user.getUsername());
		
        model.put("customScrollbar", userSettings.isCustomScrollbarEnabled());    
        model.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
		return model;
	}      

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		AdvancedSettingsCommand command = new AdvancedSettingsCommand();
		
		command.setDownloadLimit(String.valueOf(settingsService.getDownloadBitrateLimit()));
		command.setUploadLimit(String.valueOf(settingsService.getUploadBitrateLimit()));
		command.setBrand(settingsService.getBrand());
		
        command.setPageTitle(settingsService.getPageTitle());        
        command.setWelcomeTitle(settingsService.getWelcomeTitle());
        command.setWelcomeSubtitle(settingsService.getWelcomeSubtitle());
        command.setWelcomeMessage(settingsService.getWelcomeMessage());
        command.setAdminMessage(settingsService.getAdminMessage());
        command.setLoginMessage(settingsService.getLoginMessage());
		
		return command;
	}

	@Override
	protected void doSubmitAction(Object comm) throws Exception {
		AdvancedSettingsCommand command = (AdvancedSettingsCommand) comm;
		command.setToast(true);
		command.setReloadNeeded(false);
		
        settingsService.setWelcomeTitle(command.getWelcomeTitle());
        settingsService.setPageTitle(command.getPageTitle());        
        settingsService.setWelcomeSubtitle(command.getWelcomeSubtitle());
        settingsService.setWelcomeMessage(command.getWelcomeMessage());
        settingsService.setAdminMessage(command.getAdminMessage());
        settingsService.setLoginMessage(command.getLoginMessage());		
        settingsService.save();
        
		try {
			settingsService.setDownloadBitrateLimit(Long.parseLong(command.getDownloadLimit()));
		} catch (NumberFormatException x) { /* Intentionally ignored. */ }
		try {
			settingsService.setUploadBitrateLimit(Long.parseLong(command.getUploadLimit()));
		} catch (NumberFormatException x) { /* Intentionally ignored. */ }
		settingsService.save();
	}

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}    

}
