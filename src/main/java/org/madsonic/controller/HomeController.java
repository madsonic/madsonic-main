/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import org.madsonic.Logger;
import org.madsonic.domain.CoverArtScheme;
import org.madsonic.domain.Genre;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MusicFolder;
import org.madsonic.domain.Player;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.HotService;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.MediaScannerService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.RatingService;
import org.madsonic.service.SearchService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;
import org.springframework.web.servlet.view.RedirectView;

import static org.springframework.web.bind.ServletRequestUtils.*;
/**
 * Controller for the home page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class HomeController extends ParameterizableViewController {

    private static final Logger LOG = Logger.getLogger(HomeController.class);

    private static final int DEFAULT_LIST_SIZE    =  50;
	private static final int DEFAULT_LIST_OFFSET  =   0;
    private static final int DEFAULT_TIPLIST_SIZE =  50;
    private static final int DEFAULT_TOPLIST_SIZE = 100;
    
    private static final String DEFAULT_LIST_TYPE = "random";
	
    private final int DEFAULT_GROUP_ALL = SettingsService.DEFAULT_GROUP_ALL;
	private final int DEFAULT_GROUP_NONE = SettingsService.DEFAULT_GROUP_NONE;
    private final int DEFAULT_GROUP_MUSIC = SettingsService.DEFAULT_GROUP_MUSIC;
    private final int DEFAULT_GROUP_VIDEO = SettingsService.DEFAULT_GROUP_VIDEO;
    private final int DEFAULT_GROUP_MOVIES = SettingsService.DEFAULT_GROUP_MOVIES;
    private final int DEFAULT_GROUP_SERIES = SettingsService.DEFAULT_GROUP_SERIES;
    private final int DEFAULT_GROUP_IMAGES = SettingsService.DEFAULT_GROUP_IMAGES;    
    private final int DEFAULT_GROUP_TV = SettingsService.DEFAULT_GROUP_TV;     
    
    private SettingsService settingsService;
    private MediaScannerService mediaScannerService;
    private RatingService ratingService;
    private SecurityService securityService;
    private MediaFileService mediaFileService;
    private SearchService searchService;
    private HotService hotService;
    private PlayerService playerService;
    
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
        Map<String, Object> map = new HashMap<String, Object>();

        User user = securityService.getCurrentUser(request);
        if (user.isAdminRole() && settingsService.isGettingStartedEnabled()) {
            return new ModelAndView(new RedirectView("gettingStarted.view"));
        }
        UserSettings userSettings = settingsService.getUserSettings(user.getUsername());
        Player player = playerService.getPlayer(request, response);
        
        int userGroupId = securityService.getCurrentUserGroupId(request);
        int selectedMusicFolderId = getSelectedMusicFolderId(request);
        
        String selectedMusicFolderName;
        
        List<MusicFolder> allFolders = new ArrayList<MusicFolder>();
        List<MusicFolder> groupedFolder = new ArrayList<MusicFolder>();
        
        List<MusicFolder> allOtherFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_NONE);
        List<MusicFolder> allMusicFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_MUSIC);
        List<MusicFolder> allVideoFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_VIDEO);
        List<MusicFolder> allMoviesFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_MOVIES);
        List<MusicFolder> allSeriesFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_SERIES);
        List<MusicFolder> allImagesFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_IMAGES);
        List<MusicFolder> allTVFolders = settingsService.getAllMusicFolders(userGroupId, settingsService.isSortMediaFileFolder(), DEFAULT_GROUP_TV);
        
        allFolders.addAll(allOtherFolders);
        allFolders.addAll(allMusicFolders);
        allFolders.addAll(allVideoFolders);
        allFolders.addAll(allMoviesFolders);
        allFolders.addAll(allSeriesFolders);
        allFolders.addAll(allImagesFolders);
        allFolders.addAll(allTVFolders);
        
        if ( allMusicFolders.size() > 0)  { map.put("MusicFolderEnabled", true); }
        if ( allVideoFolders.size() > 0)  { map.put("VideoFolderEnabled", true); }
        if ( allMoviesFolders.size() > 0) { map.put("MoviesFolderEnabled", true); }
        if ( allSeriesFolders.size() > 0) { map.put("SeriesFolderEnabled", true); }        
        if ( allImagesFolders.size() > 0) { map.put("ImagesFolderEnabled", true); }        
        if ( allTVFolders.size() > 0)     { map.put("TVFolderEnabled", true); }
        
        switch(selectedMusicFolderId){ 
        case -1: selectedMusicFolderName  = "allFolders"; groupedFolder = allFolders; break; 
        case -2: selectedMusicFolderName = "allMusicFolders"; groupedFolder = allMusicFolders; break; 
        case -3: selectedMusicFolderName = "allVideoFolders"; groupedFolder = allVideoFolders; break; 
        case -4: selectedMusicFolderName = "allMoviesFolders"; groupedFolder = allMoviesFolders; break; 
        case -5: selectedMusicFolderName = "allSeriesFolders"; groupedFolder = allSeriesFolders; break; 
        case -6: selectedMusicFolderName = "allImagesFolders"; groupedFolder = allImagesFolders; break; 
        case -7: selectedMusicFolderName = "allTVFolders"; groupedFolder = allTVFolders; break; 
        default: selectedMusicFolderName  = ""; break; 
	    } 
        MusicFolder selectedMusicFolder = getSelectedMusicFolder(request);
        List<MusicFolder> musicFoldersToUse = selectedMusicFolder == null ? groupedFolder : Arrays.asList(selectedMusicFolder);        
        
        map.put("selectedMusicFolderId", selectedMusicFolderId);
        map.put("selectedMusicFolderName", selectedMusicFolderName);
        map.put("selectedMusicFolder", selectedMusicFolder);        
        map.put("musicFolders", allOtherFolders);
        
        String listType = DEFAULT_LIST_TYPE;
        int listSize = DEFAULT_LIST_SIZE;
        
        CoverArtScheme scheme = player.getCoverArtScheme();
        if (scheme != CoverArtScheme.OFF) {
            map.put("coverArtSize", scheme.getSize());
        } else {
            map.put("coverArtSize", CoverArtScheme.LARGE.getSize());
            map.put("coverArtThumbs", false); 
        }
        if (scheme == CoverArtScheme.THUMBS || scheme == CoverArtScheme.MINI ) {
        	listSize = scheme == CoverArtScheme.MINI ? DEFAULT_LIST_SIZE * 3 : DEFAULT_LIST_SIZE * 2;        	
	        map.put("coverArtThumbs", true);        
        }        
        map.put("coverArtHQ",settingsService.isCoverArtHQ());
        
        int listOffset = getIntParameter(request, "listOffset", DEFAULT_LIST_OFFSET);
        if (request.getParameter("listType") != null) {
            listType = String.valueOf(request.getParameter("listType"));
        }
        List<Album> albums = Collections.emptyList();
        if ("highest".equals(listType)) {
            albums = getHighestRated(listOffset, listSize, userGroupId);
        } else if ("frequent".equals(listType)) {
            albums = getMostFrequent(musicFoldersToUse, listOffset, listSize);
        } else if ("recent".equals(listType)) {
            albums = getMostRecent(musicFoldersToUse, listOffset, listSize);
        } else if ("newest".equals(listType)) {
            albums = getNewest(musicFoldersToUse, listOffset, listSize, userGroupId);
        } else if ("tip".equals(listType)) {
            albums = getRandomHot(DEFAULT_TIPLIST_SIZE, userGroupId);
		} else if ("hot".equals(listType)) {
            albums = getHot(selectedMusicFolder, listOffset, listSize, userGroupId);
        } else if ("new".equals(listType)) {
            albums = getNewest(musicFoldersToUse, listOffset, listSize, userGroupId);
        } else if ("top".equals(listType)) {
            albums = getHighestRated(listOffset, DEFAULT_TOPLIST_SIZE, userGroupId);
		} else if ("random".equals(listType)) {
            albums = getRandom(musicFoldersToUse, listSize, userGroupId);
        } else if ("starredArtist".equals(listType)) {
            albums = getStarredArtist(musicFoldersToUse, listOffset, listSize, user.getUsername());
        } else if ("allArtist".equals(listType)) {
            albums = getArtist(musicFoldersToUse, listOffset, listSize, user.getUsername(),userGroupId );
        } else if ("starred".equals(listType)) {
            albums = getStarred(musicFoldersToUse, listOffset, listSize, user.getUsername());
        } else if ("alphabetical".equals(listType)) {
            albums = getAlphabetical(musicFoldersToUse, listOffset, listSize, true);
          
        } else if ("decade".equals(listType)) {
            List<Integer> decades = createDecades();
            map.put("decades", decades);
            int decade = getIntParameter(request, "decade", decades.get(0));
            map.put("decade", decade);
            albums = getByYear(listOffset, DEFAULT_LIST_SIZE, decade, decade + 9, userGroupId);
            
        } else if ("genre".equals(listType)) {
            List<Genre> genres = mediaFileService.getGenres(true);
            map.put("genres", genres);
            if (!genres.isEmpty()) {
                String genre = getStringParameter(request, "genre", genres.get(0).getName());
                map.put("genre", genre);
                albums = getByGenre(listOffset, listSize, genre, userGroupId); 
            }
        } 
        map.put("albums", albums);
        map.put("welcomeTitle", settingsService.getWelcomeTitle());
        map.put("welcomeSubtitle", settingsService.getWelcomeSubtitle());
        map.put("welcomeMessage", settingsService.getWelcomeMessage());
        map.put("isIndexBeingCreated", mediaScannerService.isScanning());
        
        map.put("listType", listType);
        
        if ("new".equals(listType)) {
            map.put("listSize", DEFAULT_TOPLIST_SIZE);
        }
        else if ("top".equals(listType)) {
            map.put("listSize", DEFAULT_TOPLIST_SIZE);
        }        
        else if ("tip".equals(listType)) {
            map.put("listSize", DEFAULT_TIPLIST_SIZE);
    	}
        else {
            map.put("listSize", listSize);
        }
        
        map.put("listOffset", listOffset);
		map.put("player", player);
		
        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled());
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        map.put("showTopTrack",  user.isLastFMRole());
        map.put("showPlayer",  user.isStreamRole());
        
        String header = "";
        if (settingsService.showHomeRandom()) {header = "random";}
        if (settingsService.showHomeNewAdded()){header += " newest";}
        if (settingsService.showHomeHotRated()){header += " hot";}
        if (settingsService.showHomeName()){header+= " alphabetical";}
        if (settingsService.showHomeAllArtist()){header += " allArtist";}
        if (settingsService.showHomeStarredArtist()){header += " starredArtist";}
        if (settingsService.showHomeStarredAlbum()){header += " starred";}
        if (settingsService.showHomeAlbumTip()){header += " tip";}
        if (settingsService.showHomeTopRated()){header += " highest";}
        if (settingsService.showHomeMostPlayed()){header += " frequent";}
        if (settingsService.showHomeLastPlayed()){header += " recent";}
        if (settingsService.showHomeDecade()){header += " decade";}
        if (settingsService.showHomeGenre()){header += " genre";}
        if (settingsService.showHomeTop100()){header += " top";}
        if (settingsService.showHomeNew100()){header += " new";}    
        map.put("homeHeader", header);
        map.put("showHomePagerTop", settingsService.showHomePagerTop());  
        map.put("showHomePagerBottom", settingsService.showHomePagerBottom());  
        
        
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }

    private int getSelectedMusicFolderId(HttpServletRequest request) {
        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
        return settings.getSelectedMusicFolderId();
    }
    
    private MusicFolder getSelectedMusicFolder(HttpServletRequest request) {
        UserSettings settings = settingsService.getUserSettings(securityService.getCurrentUsername(request));
        int musicFolderId = settings.getSelectedMusicFolderId();
        return settingsService.getMusicFolderById(musicFolderId);
    }      
    
    private List<Album> getHighestRated(int offset, int count, int user_group_id) {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile mediaFile : ratingService.getHighestRatedAlbums(offset, count, user_group_id)) {
            Album album = createAlbum(mediaFile);
            if (album != null) {
                album.setRating((int) Math.round(ratingService.getAverageRating(mediaFile) * 10.0D));
                // set parent id for url
                try {
	                MediaFile parent = mediaFileService.getParentOf(mediaFile);
	                album.setParentId(parent.getId());
                } catch (Exception x) {
                }                
                result.add(album);
            }
        }
        return result;
    }

    private List<Album> getRandomHot(int count, int user_group_id) {
	List<Album> result = new ArrayList<Album>();
	
		int randomOffset = 0;
		for ( int loop = 1; loop <= count; loop ++ ) {
			Random random = new Random(System.currentTimeMillis());
			randomOffset = random.nextInt(hotService.getCountHotFlag() == 0 ? 1 : hotService.getCountHotFlag());			
		        for (MediaFile mediaFile : hotService.getRandomHotRated(randomOffset, 1, user_group_id)) {
		            Album album = createAlbum(mediaFile);
		            if (album != null) {
				        try { 
		                      // set parent id for url
		                      MediaFile parent = mediaFileService.getParentOf(mediaFile);
		                      album.setParentId(parent.getId());
	
				        	  album.setPlayCount(mediaFile.getPlayCount());
		                	  
		                      Date created = mediaFile.getCreated();
		                      if (created == null) {
		                          created = mediaFile.getChanged();
		                      }
		                      album.setCreated(created);
				        	  album.setRating((int) Math.round(ratingService.getAverageRating(mediaFile) * 10.0D));

				            } catch (Exception x) {}
				        
				        boolean albumFound = false;
				        for (Album albumEntry : result){
				        	
				        	if (albumEntry.albumTitle == album.albumTitle){
				        		albumFound = true;
				        		break;
				        	}
				        }
				        if (albumFound == false){
							result.add(album);
				        }
				        if (result.size() > 4 ){
				        	return result;
				        }
					 }
		        }
		}
		
		if (result.size() < 1) {
			result = Collections.emptyList();
		}	

		return result;
	}
	
	List<Album> getHot(MusicFolder musicFolder, int offset, int count, int user_group_id) {
	List<Album> result = new ArrayList<Album>();
        for (MediaFile mediaFile : hotService.getHotRated(musicFolder, offset, count, user_group_id)) {
            Album album = createAlbum(mediaFile);
		if (album != null) {
			result.add(album);
		}
	}
	return result;
}
   private List<Album> getMostFrequent(List<MusicFolder> musicFolders, int offset, int count) {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile mediaFile : mediaFileService.getMostFrequentlyPlayedAlbums(musicFolders, offset, count)) {
            Album album = createAlbum(mediaFile);
            if (album != null) {
                album.setPlayCount(mediaFile.getPlayCount());
                // set parent id for url
                try {
	                MediaFile parent = mediaFileService.getParentOf(mediaFile);
	                album.setParentId(parent.getId());
                } catch (Exception x) {
                }
                result.add(album);
            }
        }
        return result;
    }

    private List<Album> getMostRecent(List<MusicFolder> musicFolders, int offset, int count) {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile mediaFile : mediaFileService.getMostRecentlyPlayedAlbums(musicFolders, offset, count)) {
            Album album = createAlbum(mediaFile);
            if (album != null) {
                album.setLastPlayed(mediaFile.getLastPlayed());
                // set parent id for url
                MediaFile parent = mediaFileService.getParentOf(mediaFile);
                album.setParentId(parent.getId());
                
                result.add(album);
            }
        }
        return result;
    }

    private List<Album> getNewest(List<MusicFolder> musicFolders, int offset, int count, int userGroupId) throws IOException {
        List<Album> result = new ArrayList<Album>();
		for (MediaFile file : mediaFileService.getNewestAlbums(musicFolders, offset, count, userGroupId)) {
            Album album = createAlbum(file);
            if (album != null) {
                Date created = file.getCreated();
                if (created == null) {
                    created = file.getChanged();
                }
                album.setCreated(created);
                // set parent id for url
                try {
                    MediaFile parent = mediaFileService.getParentOf(file);
                    album.setParentId(parent.getId());
                } catch (Exception x) {
                    LOG.warn("## Failed to get ParentId for list entry " + file.getPath(), x);
                }
                result.add(album);
            }
        }
        return result;
    }

    private List<Album> getStarred(List<MusicFolder> musicFolders, int offset, int count, String username) throws IOException {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile file : mediaFileService.getStarredAlbums(musicFolders, offset, count, username)) {
            Album album = createAlbum(file);
            if (album != null) {
                // set parent id for url
            	try {
                MediaFile parent = mediaFileService.getParentOf(file);
                album.setParentId(parent.getId());            	
                } catch (Exception x) {
                }
                result.add(album);
            }
        }
        return result;
    }

    public List<Album> getArtist(List<MusicFolder> musicFolders, int offset, int count, String username, int user_group_id) throws IOException {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile file : mediaFileService.getArtists(musicFolders, offset, count, username, user_group_id)) {
            Album album = createArtistAlbum(file);
            if (album != null) {
                // set parent id for url
            	try {
            		MediaFile parent = mediaFileService.getParentOf(file);
            		album.setParentId(file.getId());            	
                } catch (Exception x) {
            	}
                result.add(album);
            }
        }
        return result;
    }    
    
    private List<Album> getStarredArtist(List<MusicFolder> musicFolders, int offset, int count, String username) throws IOException {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile file : mediaFileService.getStarredArtists(musicFolders, offset, count, username)) {
            Album album = createArtistAlbum(file);
            if (album != null) {
                // set parent id for url
            	try {
	            	MediaFile parent = mediaFileService.getParentOf(file);
	                album.setParentId(file.getId());     
                } catch (Exception x) {
                }
                result.add(album);
            }
        }
        return result;
    }
    
    private List<Album> getRandom(List<MusicFolder> musicFolders, int count, int user_group_id) throws IOException {
        List<Album> result = new ArrayList<Album>();
        
        for (MediaFile file : searchService.getRandomAlbums(count, musicFolders)) {
            Album album = createAlbum(file);
            if (album != null) {
                // set parent id for url
            	try {
                MediaFile parent = mediaFileService.getParentOf(file);
                album.setParentId(parent.getId());
                } catch (Exception x) {
                }
                result.add(album);
            }
        }
        return result;
    }

    private  List<Album> getByYear(int offset, int count, int fromYear, int toYear, int user_group_id) {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile file : mediaFileService.getAlbumsByYear(offset, count, fromYear, toYear, user_group_id)) {
            Album album = createAlbum(file);
            if (album != null) {
            	try {
                    MediaFile parent = mediaFileService.getParentOf(file);
                    album.setParentId(parent.getId());
		            album.setYear(file.getYear());
                    } catch (Exception x) {
                    }
                    result.add(album);
            }
        }
        return result;
    }

    private List<Integer> createDecades() {
        List<Integer> result = new ArrayList<Integer>();
        int decade = Calendar.getInstance().get(Calendar.YEAR) / 10;
        for (int i = 0; i < 10; i++) {
            result.add((decade - i) * 10);
        }
        return result;
    }

    private List<Album> getByGenre(int offset, int count, String genre, int user_group_id) {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile file : mediaFileService.getAlbumsByGenre(offset, count, genre, user_group_id)) {
            Album album = createAlbum(file);
            if (album != null) {
            	try {
                    MediaFile parent = mediaFileService.getParentOf(file);
                    album.setParentId(parent.getId());
                    } catch (Exception x) {
                    }
                    result.add(album);            	
            }
        }
        return result;
	}	

    private List<Album> getAlphabetical(List<MusicFolder> musicFolders, int offset, int count, boolean byArtist) throws IOException {
        List<Album> result = new ArrayList<Album>();
        for (MediaFile file : mediaFileService.getAlphabeticalAlbums(musicFolders, offset, count, byArtist)) {
            Album album = createAlbum(file);
            if (album != null) {
            	try {
                    MediaFile parent = mediaFileService.getParentOf(file);
                    album.setParentId(parent.getId());
                    } catch (Exception x) {
                    }
                    result.add(album);            	
            }
        }
        return result;
    }

    private Album createAlbum(MediaFile file) {
        Album album = new Album();
        album.setId(file.getId());
        album.setHash(file.getHash());
        album.setPath(file.getPath());
        try {
            resolveArtistAndAlbumTitle(album, file);
            resolveCoverArt(album, file);
        } catch (Exception x) {
            LOG.warn("Failed to create albumTitle list entry for " + file.getPath(), x);
            return null;
        }
        return album;
    }
    
    private Album createArtistAlbum(MediaFile file) {
        Album album = new Album();
        album.setId(file.getId());
        album.setPath(file.getPath());
        album.setHash(file.getHash());
        try {
            album.setArtist(file.getArtist());
            album.setAlbumSetName(file.getGenre());
            album.setArtistFlag(file.isSingleArtist());
            
            resolveCoverArt(album, file);
        } catch (Exception x) {
            LOG.warn("Failed to create albumTitle list entry for " + file.getPath(), x);
            return null;
        }
        return album;
    }

    private void resolveArtistAndAlbumTitle(Album album, MediaFile file) throws IOException {
        album.setArtist(file.getArtist());
        album.setAlbumTitle(file.getAlbumName());
        album.setAlbumSetName(file.getAlbumSetName());
        album.setAlbumYear(file.getYear());
    }

    private void resolveCoverArt(Album album, MediaFile file) {
        album.setCoverArtPath(file.getCoverArtPath());
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setMediaScannerService(MediaScannerService mediaScannerService) {
        this.mediaScannerService = mediaScannerService;
    }

    public void setRatingService(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    public void setHotService(HotService hotService) {
        this.hotService = hotService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }
    
    /**
     * Contains info for a single album.
     */
    public static class Album {
        private int id;
        private String hash;
        private String path;
        private String coverArtPath;
        private String artist;
        private String albumTitle;
        private String albumSetName;
        private Integer albumYear;
        private Date created;
        private Date lastPlayed;
        private Integer playCount;
        private Integer rating;
        private int parentId;
        private boolean artistFlag;
        private Integer year;
        
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getCoverArtPath() {
            return coverArtPath;
        }

        public void setCoverArtPath(String coverArtPath) {
            this.coverArtPath = coverArtPath;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

        public String getAlbumTitle() {
            return albumTitle;
        }
		
        public String getAlbumSetName() {
            return albumSetName;
        }
		
        public void setAlbumTitle(String albumTitle) {
            this.albumTitle = albumTitle;
        }

        public void setAlbumSetName(String albumSetName) {
            this.albumSetName = albumSetName;
        }
		
		
        public Integer getAlbumYear() {
            return albumYear;
        }

        public void setAlbumYear(Integer albumYear) {
            this.albumYear = albumYear;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public Date getLastPlayed() {
            return lastPlayed;
        }

        public void setLastPlayed(Date lastPlayed) {
            this.lastPlayed = lastPlayed;
        }

        public Integer getPlayCount() {
            return playCount;
        }

        public void setPlayCount(Integer playCount) {
            this.playCount = playCount;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

		public Integer getParentId() {
			return parentId;
		}

		public void setParentId(Integer parentId) {
			this.parentId = parentId;
		}

		public boolean isArtistFlag() {
			return artistFlag;
		}

		public void setArtistFlag(boolean artistFlag) {
			this.artistFlag = artistFlag;
		}

		public Integer getYear() {
			return year;
		}

		public void setYear(Integer year) {
			this.year = year;
		}
    }
}
