/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import org.madsonic.command.PremiumSettingsCommand;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;

/**
 * Controller for the Madsonic Premium page.
 *
 * @author Sindre Mehus, Martin Karel
 */
@SuppressWarnings("deprecation")
public class PremiumSettingsController extends SimpleFormController {

    private SettingsService settingsService;
    private SecurityService securityService;

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        PremiumSettingsCommand command = new PremiumSettingsCommand();
        command.setPath(request.getParameter("path"));
        command.setForceChange(request.getParameter("change") != null);
        command.setLicenseInfo(settingsService.getLicenseInfo());
        command.setBrand(settingsService.getBrand());
        command.setUser(securityService.getCurrentUser(request));
        return command;
    }
    
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        ModelAndView result = super.handleRequestInternal(request, response);
        User activeUser = securityService.getCurrentUser(request);
        UserSettings activeUserSettings = settingsService.getUserSettings(activeUser.getUsername());

        map.put("customScrollbar", activeUserSettings.isCustomScrollbarEnabled());     
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(activeUserSettings.getThemeId() == null ? settingsService.getThemeId() : activeUserSettings.getThemeId()));
        
        result.addObject("model", map);
        return result;
    }    

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object com, BindException errors)
            throws Exception {
        PremiumSettingsCommand command = (PremiumSettingsCommand) com;
        Date now = new Date();

        settingsService.setLicenseCode(command.getLicenseCode());
        settingsService.setLicenseEmail(command.getLicenseInfo().getLicenseEmail());
        settingsService.setLicenseDate(now);
        settingsService.save();
        settingsService.scheduleLicenseValidation();

        // Reflect changes in view. The validator will validate the license asynchronously.
        command.setLicenseInfo(settingsService.getLicenseInfo());
        
        return new ModelAndView(getSuccessView(), errors.getModel());
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}