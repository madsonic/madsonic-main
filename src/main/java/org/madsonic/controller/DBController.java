/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import org.madsonic.Logger;
import org.madsonic.dao.DaoHelper;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.FileUtil;
import org.madsonic.util.Util;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller for the DB admin page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class DBController extends ParameterizableViewController {

    private DaoHelper daoHelper;
    private SecurityService securityService;
    
    private static final Logger LOG = Logger.getLogger(DBController.class);

    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
        Map<String, Object> map = new HashMap<String, Object>();
        String query = request.getParameter("query");
        boolean export = "export".equals(request.getParameter("option"));
        boolean download = "download".equals(request.getParameter("option"));
        String username = securityService.getCurrentUsername(request);
        
        if (query != null) {
            map.put("query", query);
            map.put("export", export);
            map.put("download", download);
            try {
            	List<Map<String, Object>> result = daoHelper.getJdbcTemplate().query(query, new ColumnMapRowMapper());
                map.put("result", result);
            	String fileName = SettingsService.getMadsonicHome() + "/madsonic-export.csv";
            	
            	if ( export || download ) {
            		
                	File file = new File(fileName);
                	map.put("exportPath", file.getAbsolutePath());
                    PrintWriter writer = new PrintWriter(new File(fileName));
                    
                    StringBuilder sb = new StringBuilder();
                    
                	// header
                    sb.append("sep=,\n");
                	boolean first = true;
                	for (String keyName : result.get(0).keySet()) {
                        if (!first) {
                            sb.append(",");
                        }
                		sb.append(keyName);
                		first = false;
                	}
            		sb.append("\n");

            		// data
                    for(Map<String, Object> obj : result){
	                	first = true;
	                	for (String keyName : obj.keySet()) {
	                        if (!first) {
	                            sb.append(",");
	                        }
	                		sb.append(obj.get(keyName));
	                		first = false;
	                	}
	            		sb.append("\n");
                    }
                    writer.println(sb.toString());
                    writer.close();
                    
                    if (download) {
                    	downloadFile(response, new File(fileName), username);
                    	return null;
                    }
                }
            } catch (DataAccessException x) {
                map.put("error", ExceptionUtils.getRootCause(x).getMessage());
            }
        }
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }

    private void downloadFile(HttpServletResponse response, File file, String username) throws IOException {
    	
        LOG.info("Starting to download '" + FileUtil.getShortPath(file));
        response.setContentType("application/x-download");
        response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + encodeAsRFC5987(file.getName()));
        Util.setContentLength(response, file.length());
        copyFileToStream(file, response.getOutputStream());
        LOG.info(username + " successfully Downloaded '" + FileUtil.getShortPath(file));
    }    
    
    private String encodeAsRFC5987(String string) throws UnsupportedEncodingException {
        byte[] stringAsByteArray = string.getBytes("UTF-8");
        char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        byte[] attrChar = {'!', '#', '$', '&', '+', '-', '.', '^', '_', '`', '|', '~', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        StringBuilder sb = new StringBuilder();
        for (byte b : stringAsByteArray) {
            if (Arrays.binarySearch(attrChar, b) >= 0) {
                sb.append((char) b);
            } else {
                sb.append('%');
                sb.append(digits[0x0f & (b >>> 4)]);
                sb.append(digits[b & 0x0f]);
            }
        }
        return sb.toString();
    }
    
    private void copyFileToStream(File file, OutputStream out) throws IOException {
        LOG.info("Downloading '" + FileUtil.getShortPath(file));
        final int bufferSize = 16 * 1024; // 16 Kbit
        InputStream in = new BufferedInputStream(new FileInputStream(file), bufferSize);
        try {
            byte[] buf = new byte[bufferSize];
            long bitrateLimit = 0;
            while (true) {
                long before = System.currentTimeMillis();
                int n = in.read(buf);
                if (n == -1) {
                    break;
                }
                out.write(buf, 0, n);

                long after = System.currentTimeMillis();

                // Sleep for a while to throttle bitrate.
                if (bitrateLimit != 0) {
                    long sleepTime = 8L * 1000 * bufferSize / bitrateLimit - (after - before);
                    if (sleepTime > 0L) {
                        try {
                            Thread.sleep(sleepTime);
                        } catch (Exception x) {
                            LOG.warn("Failed to sleep.", x);
                        }
                    }
                }
            }
        } finally {
            out.flush();
            IOUtils.closeQuietly(in);
        }
    }    
    
    public void setDaoHelper(DaoHelper daoHelper) {
        this.daoHelper = daoHelper;
    }
    
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }    
}