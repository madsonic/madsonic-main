/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.Logger;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

/**
 * Controller for the bookmark page.
 *
 * @author Martin Karel
 */
public class LovedTrackController extends ParameterizableViewController {

    @SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(LovedTrackController.class);

    private MediaFileService mediaFileService;
    private SettingsService settingsService;
    private SecurityService securityService;
    private PlayerService playerService;
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);
        
        List<MediaFile> loved = getLovedFromUser(username);
        mediaFileService.populateLovedDate(loved, username);
        mediaFileService.populateStarredDate(loved, username);
        
        map.put("user", securityService.getCurrentUser(request));
        map.put("player", playerService.getPlayer(request, response));
        map.put("loved", loved);

        map.put("buttonVisibility", userSettings.getButtonVisibility());  
		map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 	
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }
    
    private List<MediaFile> getLovedFromUser(String username) {
        List<MediaFile> userLoved = new ArrayList<MediaFile>();
    	for (MediaFile mediafile : mediaFileService.getLovedFiles(username)) {
    		userLoved.add(mediafile);
    	}
		return userLoved;
    }
    
    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }
    
    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

}
