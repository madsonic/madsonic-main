/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.madsonic.command.AdminSettingsCommand;
import org.madsonic.dao.MusicFolderStatisticsDao;
import org.madsonic.domain.MusicFolderStatistics;
import org.madsonic.domain.StatusEntry;
import org.madsonic.domain.Theme;
import org.madsonic.domain.TransferStatus;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.MediaScannerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.StatusService;
import org.madsonic.util.CustomTheme;
import org.madsonic.util.StringUtil;
import org.madsonic.util.Util;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller for the page used to administrate the server.
 *
 * @author Martin Karel
 */
@SuppressWarnings("deprecation")
public class AdminSettingsController extends SimpleFormController {

    private SettingsService settingsService;
    private SecurityService securityService;
    private MediaScannerService mediaScannerService;   
    private MusicFolderStatisticsDao musicFolderStatisticsDao;    
    private StatusService statusService;
    
    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {
	Map<String, Object> model = new HashMap<String, Object>();
	
    long totalMemory = Runtime.getRuntime().totalMemory();
    long freeMemory = Runtime.getRuntime().freeMemory();
    long usedMemory = totalMemory - freeMemory;
    
    model.put("statMemory", usedMemory * 100 / totalMemory);
    model.put("adminMessage", settingsService.getAdminMessage());
    
    List<MusicFolderStatistics> statList = musicFolderStatisticsDao.getAllFolderStatistics();

    long audioSize = 0;
    long videoSize = 0;
    long podcastSize = 0;
    long audiobookSize = 0;
    long libSize = 1;
    
    for (MusicFolderStatistics _statList : statList) {
    	audioSize += _statList.getSongSize();
    	videoSize += _statList.getVideoSize();
    	podcastSize += _statList.getPodcastSize();
    	audiobookSize += _statList.getAudiobookSize();    	
    }
    libSize = audioSize + videoSize + podcastSize + audiobookSize;
    
    StatusEntry statAudio = StringUtil.convertToStringRepresentation(audioSize);
    model.put("statAudio", statAudio.getValueInt());     
    model.put("statAudioCap", statAudio.getUnit());   
    model.put("statAudioPrecent", audioSize != 0 ? (int)(audioSize * 100 / libSize) : 0); 
        
    StatusEntry statVideo = StringUtil.convertToStringRepresentation(videoSize);
    model.put("statVideo", statVideo.getValueInt());     
    model.put("statVideoCap", statVideo.getUnit());   
    model.put("statVideoPrecent", videoSize != 0 ? (int)(videoSize * 100 / libSize) : 0); 

    StatusEntry statPodcast = StringUtil.convertToStringRepresentation(podcastSize);
    model.put("statPodcast", statPodcast.getValueInt());     
    model.put("statPodcastCap", statPodcast.getUnit());   
    model.put("statPodcastPrecent", podcastSize != 0 ? (int)(podcastSize * 100 / libSize) : 0); 
    
    StatusEntry statAudiobook = StringUtil.convertToStringRepresentation(audiobookSize);
    model.put("statAudiobook", statAudiobook.getValueInt());     
    model.put("statAudiobookCap", statAudiobook.getUnit());   
    model.put("statAudiobookPrecent", audiobookSize != 0 ? (int)(audiobookSize * 100 / libSize) : 0); 
    
    List<TransferStatus> statuses = statusService.getAllStreamStatuses();
    model.put("statPlayer", statuses.size());    
    model.put("statPlayerCap", "");    
    model.put("statPlayerPrecent", statuses.size() != 0 ? (int)((0.1 * 100.0f) / statuses.size()) : 0); 

    File HSQLDB = new File(SettingsService.getMadsonicHome() + Util.getDefaultSlash() + "db");
    long sizeHSQLDB = HSQLDB.exists() ? FileUtils.sizeOfDirectory(HSQLDB) : 0 ;
    StatusEntry entry = StringUtil.convertToStringRepresentation(sizeHSQLDB);
    
    model.put("statDB", entry.getValueInt());     
    model.put("statDBCap", entry.getUnit());   
    model.put("statDBPrecent", entry.getValueInt() != 0 ? (int)((0.2 * 100.0f) / entry.getValueInt()) : 0); 
    
    int statUser = securityService.getAllUsers().size();
    model.put("statUser", statUser);     
    model.put("statUserCap", "");   
    model.put("statUserPrecent", statUser != 0 ? (int)((0.2 * 100.0f) / statUser) : 0);
    
    String username = securityService.getCurrentUsername(request);
    UserSettings userSettings = settingsService.getUserSettings(username);    
    model.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 
    model.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
    
    return model;
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
    	
        if (request.getParameter("scanNow") != null) {
            mediaScannerService.backupAllPlaylists();
            mediaScannerService.scanFull();
        }
        AdminSettingsCommand command = new AdminSettingsCommand();
        
        command.setDebugOutput(settingsService.isDebugOutput());
        command.setShowCreateLink(settingsService.isShowCreateLink());
        command.setShowMediaType(settingsService.isShowMediaType());
        
        command.setLogfileLevel(settingsService.getLogLevel());
        command.setGettingStartedEnabled(settingsService.isGettingStartedEnabled());
        command.setPageTitle(settingsService.getPageTitle());        
        command.setWelcomeTitle(settingsService.getWelcomeTitle());
        command.setWelcomeSubtitle(settingsService.getWelcomeSubtitle());
        command.setListType(settingsService.getListType());
        command.setNewAdded(settingsService.getNewaddedTimespan());
        command.setLeftframeSize(settingsService.getLeftframeSize());
        command.setPlayQueueSize(settingsService.getPlayqueueSize());
        command.setIndexListSize(settingsService.getIndexListSize());
        
        command.setScanMode(settingsService.getScanMode());
        command.setCoverArtHQ(settingsService.isCoverArtHQ());
        command.setCustomLogo(settingsService.getCustomLogo());
        command.setPlaylistExportMode(settingsService.getPlaylistExportMode());
        command.setSnowEnabled(settingsService.isSnowEnabled());
        command.setAppRedirect(settingsService.isAppRedirectEnabled());
        
        command.setReloadNeeded(request.getParameter("reload") != null || request.getParameter("scanNow") != null);
        
        Theme[] themes = settingsService.getAvailableThemes();
        command.setThemes(themes);
        String currentThemeId = settingsService.getThemeId();
        for (int i = 0; i < themes.length; i++) {
            if (currentThemeId.equals(themes[i].getId())) {
                command.setThemeIndex(String.valueOf(i));
                break;
            }
        }

        Locale currentLocale = settingsService.getLocale();
        Locale[] locales = settingsService.getAvailableLocales();
        String[] localeStrings = new String[locales.length];
        for (int i = 0; i < locales.length; i++) {
            localeStrings[i] = locales[i].getDisplayName(locales[i]);

            if (currentLocale.equals(locales[i])) {
                command.setLocaleIndex(String.valueOf(i));
            }
        }
        command.setLocales(localeStrings);
        return command;
    }
    	
    protected ModelAndView onSubmit(Object comm) throws Exception {    	
        AdminSettingsCommand command = (AdminSettingsCommand) comm;

        int themeIndex = Integer.parseInt(command.getThemeIndex());
        Theme theme = settingsService.getAvailableThemes()[themeIndex];

        int localeIndex = Integer.parseInt(command.getLocaleIndex());
        Locale locale = settingsService.getAvailableLocales()[localeIndex];
        
        command.setReloadNeeded(!settingsService.getThemeId().equals(theme.getId()) ||
                                !settingsService.getLocale().equals(locale));

        command.setFullReloadNeeded(!(settingsService.getLeftframeSize()==(command.getLeftframeSize())) ||
        						    !(settingsService.getPlayqueueSize()==(command.getPlayQueueSize())) );

        settingsService.setDebugOutput(command.isDebugOutput());        
        settingsService.setShowCreateLink(command.isShowCreateLink());        
        settingsService.setShowMediaType(command.isShowMediaType());        
        
        settingsService.setLogfileLevel(command.getLogfileLevel());
        settingsService.setGettingStartedEnabled(command.isGettingStartedEnabled());
        settingsService.setWelcomeTitle(command.getWelcomeTitle());
        settingsService.setPageTitle(command.getPageTitle());        
        settingsService.setWelcomeSubtitle(command.getWelcomeSubtitle());
        settingsService.setThemeId(theme.getId());
        settingsService.setLocale(locale);
        settingsService.setListType(command.getListType());
        settingsService.setNewaddedTimespan(command.getNewAdded());
        settingsService.setLeftframeSize(command.getLeftframeSize());
        settingsService.setPlayqueueSize(command.getPlayQueueSize());
        settingsService.setIndexListSize(command.getIndexListSize());
        
        settingsService.setScanMode(command.getScanMode());
        settingsService.setCoverArtHQ(command.isCoverArtHQ());
        settingsService.setCustomLogo(command.isCustomLogo());
        settingsService.setSnowEnabled(command.isSnowEnabled());
        settingsService.setPlaylistEportMode(command.getPlaylistExportMode());
        settingsService.setAppRedirectEnabled(command.isAppRedirect());
        
        settingsService.save();
        
        return new ModelAndView(new RedirectView(getSuccessView() + ".view?reload"));
    }
    
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }     
    
    public void setMusicFolderStatisticsDao(MusicFolderStatisticsDao musicFolderStatisticsDao) {
        this.musicFolderStatisticsDao = musicFolderStatisticsDao;
    }
    
    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }
   
    public void setMediaScannerService(MediaScannerService mediaScannerService) {
        this.mediaScannerService = mediaScannerService;
    }    
    
    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

}