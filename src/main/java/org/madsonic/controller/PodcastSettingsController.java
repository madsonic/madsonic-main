/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.madsonic.service.PodcastService;
import org.madsonic.service.SecurityService;
import org.madsonic.command.PodcastSettingsCommand;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller for the page used to administrate the Podcast receiver.
 *
 * @author Sindre Mehus, Martin Karel
 */
@SuppressWarnings("deprecation")
public class PodcastSettingsController extends SimpleFormController {

	private SettingsService settingsService;
	private PodcastService podcastService;
	private SecurityService securityService;

	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		ModelAndView result = super.handleRequestInternal(request, response);

		User activeUser = securityService.getCurrentUser(request);
		UserSettings activeUserSettings = settingsService.getUserSettings(activeUser.getUsername());

		map.put("customScrollbar", activeUserSettings.isCustomScrollbarEnabled());     
		map.put("customScrollbarTheme", CustomTheme.setCustomTheme(activeUserSettings.getThemeId() == null ? settingsService.getThemeId() : activeUserSettings.getThemeId()));

		result.addObject("model", map);
		return result;
	}     

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		PodcastSettingsCommand command = new PodcastSettingsCommand();

		command.setInterval(String.valueOf(settingsService.getPodcastUpdateInterval()));
		command.setEpisodeRetentionCount(String.valueOf(settingsService.getPodcastEpisodeRetentionCount()));
		command.setEpisodeDownloadCount(String.valueOf(settingsService.getPodcastEpisodeDownloadCount()));
		command.setEpisodeDownloadLimit(String.valueOf(settingsService.getPodcastEpisodeDownloadLimit()));
		command.setFolder(settingsService.getPodcastFolder());
		return command;
	}

	protected void doSubmitAction(Object comm) throws Exception {
		PodcastSettingsCommand command = (PodcastSettingsCommand) comm;
		command.setToast(true);

		settingsService.setPodcastUpdateInterval(Integer.parseInt(command.getInterval()));
		settingsService.setPodcastEpisodeRetentionCount(Integer.parseInt(command.getEpisodeRetentionCount()));
		settingsService.setPodcastEpisodeDownloadCount(Integer.parseInt(command.getEpisodeDownloadCount()));
		settingsService.setPodcastFolder(command.getFolder());
		settingsService.setPodcastEpisodeDownloadLimit(Integer.parseInt(command.getEpisodeDownloadLimit()));
		settingsService.save();

		podcastService.schedule();
	}

	public void setPodcastService(PodcastService podcastService) {
		this.podcastService = podcastService;
	}

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}    
}