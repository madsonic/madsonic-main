/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import static org.madsonic.domain.MediaFile.MediaType.VIDEO;

import org.apache.commons.lang.StringUtils;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.Player;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.TranscodingService;
import org.madsonic.util.Pair;
import org.madsonic.util.StringUtil;

import java.awt.Dimension;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * Controller which produces HLS (Http Live Streaming) playlists.
 *
 * @author Scott Ware
 */
public class DashController implements Controller {

    private static final Pattern BITRATE_PATTERN = Pattern.compile("(\\d+)(@(\\d+)x(\\d+))?");
    
    private static final int SEGMENT_DURATION = 10;
    
    private PlayerService playerService;
    private MediaFileService mediaFileService;
    
    private TranscodingService transcodingService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        // Parameters for DASH playlist generation.
        Player player = playerService.getPlayer(request, response);
        
        MediaFile mediaFile;
        
        String format = "webm";
        
        String videoCodec = "vp8";
        String audioCodec;
        
        Integer maxAudioChannels;
        Integer maxSampleRate;
        
        Integer videoQuality;
        
        List<List<String>> qualities = null;
        
        TranscodingService.Parameters dashParameters = null;
                    
        int id = ServletRequestUtils.getIntParameter(request, "id");
        mediaFile = mediaFileService.getMediaFile(id);
        if(mediaFile == null)
        {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Unable to retrieve Media Element.");
            return null;
        }
            
        // Check this is a video.
        if(mediaFile.getMediaType() == null)
        {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Can't determine media type.");
            return null;
        }
        else if(mediaFile.getMediaType() != VIDEO)
        {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Media Element is not supported.");
            return null;
        }
        
        Integer duration = mediaFile.getDurationSeconds();
        if (duration == null || duration == 0)
        {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unknown duration for Media Element: " + id);
            return null;
        }

        // Max audio channels (Default to 2).
        maxAudioChannels = ServletRequestUtils.getIntParameter(request, "maxAudioChannels", 2);

        // Max audio sample rate (Default to 48000).
        maxSampleRate = ServletRequestUtils.getIntParameter(request, "maxSampleRate", 48000);

        // Retrieve desired audio codec.
        audioCodec = request.getParameter("audioCodec");
        if(audioCodec == null)
        {
            audioCodec = "vorbis";
        }

        // Get applicable qualities for this MediaFile
        qualities = getApplicableQualitySettings(mediaFile);
            
        if(qualities.isEmpty())
        {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot determine applicable quality settings for Media Element: " + id);
            return null;
        }
                
        // Build list of parameters
//        dashParameters = new TranscodingService.Parameters(mediaFile, format, videoCodec, audioCodec, maxAudioChannels, maxSampleRate, duration);
        
        // Build response.
        response.setContentType("application/dash+xml");
        response.setCharacterEncoding(StringUtil.ENCODING_UTF8);
        List<Pair<Integer, Dimension>> bitRates = parseBitRates(request);
        
        PrintWriter writer = response.getWriter();
        	
        if(!generatePlaylist(request, id, player, bitRates.size() == 1 ? bitRates.get(0) : null, duration, writer))
        {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Failed to generate MPEG-Dash .mpd for Media Element: " + id);
            return null;
        }

        return null;
    }

    private List<Pair<Integer, Dimension>> parseBitRates(HttpServletRequest request) throws IllegalArgumentException {
        List<Pair<Integer, Dimension>> result = new ArrayList<Pair<Integer, Dimension>>();
        String[] bitRates = request.getParameterValues("bitRate");
        if (bitRates != null) {
            for (String bitRate : bitRates) {
                result.add(parseBitRate(bitRate));
            }
        }
        return result;
    }    

    /**
     * Parses a string containing the bitrate and an optional width/height, e.g., 1200@640x480
     */
    protected Pair<Integer, Dimension> parseBitRate(String bitRate) throws IllegalArgumentException {

        Matcher matcher = BITRATE_PATTERN.matcher(bitRate);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid bitrate specification: " + bitRate);
        }
        int kbps = Integer.parseInt(matcher.group(1));
        if (matcher.group(3) == null) {
            return new Pair<Integer, Dimension>(kbps, null);
        } else {
            int width = Integer.parseInt(matcher.group(3));
            int height = Integer.parseInt(matcher.group(4));
            return new Pair<Integer, Dimension>(kbps, new Dimension(width, height));
        }
    }    
    
    private boolean generatePlaylist(HttpServletRequest request, int id, Player player, Pair<Integer, Dimension> bitRate, int duration, PrintWriter writer)
    {
       // try {
            
            // XMLBuilder builder = XMLBuilder.createXMLBuilder();
            // builder.preamble(StringUtil.ENCODING_UTF8);
            
            // builder.add("MPD", false,
                    // new XMLBuilder.Attribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                    // new XMLBuilder.Attribute("xmnls", "urn:mpeg:DASH:schema:MPD:2011"),
                    // new XMLBuilder.Attribute("xsi:schemaLocation", "urn:mpeg:DASH:schema:MPD:2011"),
                    // new XMLBuilder.Attribute("profiles", "urn:mpeg:dash:profile:isoff-main:2011"),
                    // new XMLBuilder.Attribute("type", "static"),
                    // new XMLBuilder.Attribute("mediaPresentationDuration", "PT" + duration + "S"),
                    // new XMLBuilder.Attribute("minBufferTime", "PT10S"));
            
            // builder.add("Period", false,
                    // new XMLBuilder.Attribute("start", "PT0S"),
                    // new XMLBuilder.Attribute("duration", "PT" + duration + "S"));
            
            // builder.add("AdaptationSet", false,
                    // new XMLBuilder.Attribute("bitstreamSwitching", "true"));
               
            // builder.add("Representation", false, 
                        // new XMLBuilder.Attribute("id", 0),
                        // new XMLBuilder.Attribute("codecs", "vp8"),
                        // new XMLBuilder.Attribute("mimeType", "video/webm"),
                        // new XMLBuilder.Attribute("width", 720),
                        // new XMLBuilder.Attribute("height", 306),
                        // new XMLBuilder.Attribute("startWithSAP", "1"),
                        // new XMLBuilder.Attribute("bandwidth", 2295403)); // 1000k
                
                // builder.add("SegmentList", false,
                        // new XMLBuilder.Attribute("duration", SEGMENT_DURATION));

                // int offset = 0;
                
             ////   Get Video Segments
                // for (int i = 0; i < ((duration / SEGMENT_DURATION) + 1); i++)
                // {
                	// offset = (i * SEGMENT_DURATION);
                    
			////		Add segment
                    // builder.add("SegmentURL", true,
                            // new XMLBuilder.Attribute("media", createStreamUrl(request, player, id, offset, SEGMENT_DURATION, bitRate)));
                // }
                
                // builder.end();
                // builder.end();
                
           
            // builder.endAll();
            
            // writer.print(builder);
            // writer.flush();
            
//        } catch (IOException ex) {
//            return false;
//        }
        
        return true;
    }

    private String getContextPath(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        if (StringUtils.isEmpty(contextPath)) {
            contextPath = "/";
        } else {
            contextPath += "/";
        }
        return contextPath;
    }    
    
    private String createStreamUrl(HttpServletRequest request, Player player, int id, int offset, int duration, Pair<Integer, Dimension> bitRate) {
        StringBuilder builder = new StringBuilder();
        builder.append(getContextPath(request)).append("stream/stream.webm?id=").append(id).append("&dash=true&timeOffset=").append(offset)
                .append("&player=").append(player.getId()).append("&duration=").append(duration);
        if (bitRate != null) {
            builder.append("&maxBitRate=").append(bitRate.getFirst());
            Dimension dimension = bitRate.getSecond();
            if (dimension != null) {
                builder.append("&size=").append(dimension.width).append("x").append(dimension.height);
            }
        }
        return builder.toString();
    }
    
    public List<List<String>> getApplicableQualitySettings(MediaFile element)
    {
        List<List<String>> qualities = new ArrayList<>();
                        
        Dimension resolution = new Dimension(element.getWidth(), element.getHeight());
        
        if(resolution.getWidth() >= 426 || resolution.getHeight() >= 240)
        {   
            qualities.add(Arrays.asList("240", "400000", "426", "240", "240p"));
        }
        
        if(resolution.getWidth() >= 640 || resolution.getHeight() >= 360)
        {
            qualities.add(Arrays.asList("360", "750000", "640", "360", "360p"));
        }
        
        if(resolution.getWidth() >= 854 || resolution.getHeight() >= 480)
        {
            qualities.add(Arrays.asList("480", "1000000", "854", "480", "480p"));
        }
        
        if(resolution.getWidth() >= 1280 || resolution.getHeight() >= 720)
        {
            qualities.add(Arrays.asList("720", "2500000", "1280", "720", "720p"));
        }
        
        if(resolution.getWidth() >= 1920 || resolution.getHeight() >= 1080)
        {
            qualities.add(Arrays.asList("1080", "4000000", "1920", "1080", "1080p"));
        }
        
        return qualities;
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }
    
    public void setTranscodingService(TranscodingService transcodingService) {
        this.transcodingService = transcodingService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }
}
