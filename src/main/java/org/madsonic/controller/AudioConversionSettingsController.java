/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

import org.madsonic.domain.AudioConversion;
import org.madsonic.domain.MediaFile;
import org.madsonic.service.AudioConversionService;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.SettingsService;

/**
 * Controller for the page used to administrate the audio conversions.
 *
 * @author Sindre Mehus
 */
public class AudioConversionSettingsController extends ParameterizableViewController {

    private AudioConversionService audioConversionService;
    private SettingsService settingsService;
    private MediaFileService mediaFileService;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>();

        if (isFormSubmission(request)) {
            handleParameters(request);
            map.put("toast", true);
        }

        ModelAndView result = super.handleRequestInternal(request, response);

        map.put("conversionInfos", getAudioConversionInfo());
        map.put("directory", settingsService.getAudioConversionDirectory());
        map.put("diskLimit", settingsService.getAudioConversionDiskLimit());
        map.put("bytesUsed", getDiskUsage());
        map.put("licenseInfo", settingsService.getLicenseInfo());

        result.addObject("model", map);
        return result;
    }

    private long getDiskUsage() {
        File dir = new File(settingsService.getAudioConversionDirectory());
        if (dir.canRead() && dir.isDirectory()) {
            return FileUtils.sizeOfDirectory(dir);
        }
        return 0;
    }

    private List<AudioConversionInfo> getAudioConversionInfo() {
        List<AudioConversionInfo> result = new ArrayList<AudioConversionInfo>();
        for (AudioConversion conversion : audioConversionService.getAllAudioConversions()) {
            File file = new File(conversion.getTargetFile());
            Long size = null;
            if (file.exists()) {
                size = file.length();
            }
            result.add(new AudioConversionInfo(conversion, mediaFileService.getMediaFile(conversion.getMediaFileId()), size));
        }
        return result;
    }

    /**
     * Determine if the given request represents a form submission.
     *
     * @param request current HTTP request
     * @return if the request represents a form submission
     */
    private boolean isFormSubmission(HttpServletRequest request) {
        return "POST".equals(request.getMethod());
    }

    private void handleParameters(HttpServletRequest request) throws ServletRequestBindingException {
        for (AudioConversion conversion : audioConversionService.getAllAudioConversions()) {
            boolean delete = getParameter(request, "delete", conversion.getId()) != null;
            if (delete) {
                audioConversionService.deleteAudioConversion(conversion);
            }
        }

        String directory = StringUtils.trimToNull(request.getParameter("directory"));
        if (directory != null) {
            settingsService.setAudioConversionDirectory(directory);
        }
        int limit = ServletRequestUtils.getRequiredIntParameter(request, "diskLimit");
        settingsService.setAudioConversionDiskLimit(limit);
        settingsService.save();
    }

    private String getParameter(HttpServletRequest request, String name, int id) {
        return StringUtils.trimToNull(request.getParameter(name + "[" + id + "]"));
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }

    public void setAudioConversionService(AudioConversionService audioConversionService) {
        this.audioConversionService = audioConversionService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public static class AudioConversionInfo {
        private final AudioConversion conversion;
        private final MediaFile audio;
        private final Long size;

        public AudioConversionInfo(AudioConversion conversion, MediaFile audio, Long size) {
            this.conversion = conversion;
            this.audio = audio;
            this.size = size;
        }

        public AudioConversion getConversion() {
            return conversion;
        }

        public MediaFile getVideo() {
            return audio;
        }

        public Long getSize() {
            return size;
        }
    }
}
