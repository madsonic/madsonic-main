/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;
import org.apache.commons.lang.StringUtils;

import org.madsonic.domain.Node;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.NodeService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.network.DiscoveryService;
import org.madsonic.util.CustomTheme;

/**
 * Controller for the "Node" devices.
 *
 * @author Martin Karel
 */
public class NodeSettingsController extends ParameterizableViewController {

	private NodeService nodeService;
	private SecurityService securityService;
	private SettingsService settingsService;

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		if (isFormSubmission(request)) {
			String error = handleParameters(request);
			map.put("error", error);
			if (error == null) {
				map.put("reload", true);
			}
		}
		
		
		ModelAndView result = super.handleRequestInternal(request, response);

		map.put("nodesServiceEnabled", settingsService.isNodesEnabled());
		map.put("nodes", nodeService.getAllNodes());
		map.put("user", securityService.getCurrentUser(request));
		map.put("licenseInfo", settingsService.getLicenseInfo());
		
        User user = securityService.getCurrentUser(request);
        UserSettings userSettings = settingsService.getUserSettings(user.getUsername());

        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled());    
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        		
		result.addObject("model", map);
		return result;
	}

	/**
	 * Determine if the given request represents a form submission.
	 *
	 * @param request current HTTP request
	 * @return if the request represents a form submission
	 */
	private boolean isFormSubmission(HttpServletRequest request) {
		return "POST".equals(request.getMethod());
	}

	private String handleParameters(HttpServletRequest request) {
		boolean nodesEnabled = ServletRequestUtils.getBooleanParameter(request, "nodesServiceEnabled", false);
		if (nodesEnabled != settingsService.isNodesEnabled()) {
			DiscoveryService discoveryService = DiscoveryService.getInstance();
			if (!nodesEnabled) {
				if (discoveryService.isRunning())
					discoveryService.stopTask();
			} else {
				if (!discoveryService.isRunning())
					discoveryService.startTask();
			}
			settingsService.setNodesEnabled(nodesEnabled);
			settingsService.save();
		}

		List<Node> nodes = nodeService.getAllNodes();
		for (Node node : nodes) {
			Integer id = node.getId();
			String url = getParameter(request, "url", id);
			String name = getParameter(request, "name", id);
			boolean enabled = getParameter(request, "enabled", id) != null;
			boolean delete = getParameter(request, "delete", id) != null;

			if (delete) {
				nodeService.deleteNode(id);
			} else {
				if (name == null) {
					return "nodesettings.noname";
				}
				if (url == null) {
					return "nodesettings.nourl";
				}
				nodeService.updateNode(new Node(id, url, name, node.isOnline(), enabled));
			}
		}

		String url = StringUtils.trimToNull(request.getParameter("url"));
		String name = StringUtils.trimToNull(request.getParameter("name"));
		boolean online = StringUtils.trimToNull(request.getParameter("online")) != null;
		boolean enabled = StringUtils.trimToNull(request.getParameter("enabled")) != null;

		if (name != null && url != null) {
			nodeService.createNode(new Node(null, url, name, online, enabled));
		}
		return null;
	}

	private String getParameter(HttpServletRequest request, String name, Integer id) {
		return StringUtils.trimToNull(request.getParameter(name + "[" + id + "]"));
	}

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

}