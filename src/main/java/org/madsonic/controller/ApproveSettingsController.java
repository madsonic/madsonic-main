/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller for the page used to administered the accounts for approval.
 *
 * @author Martin Karel
 */
public class ApproveSettingsController extends ParameterizableViewController {

    private SettingsService settingsService;
    private SecurityService securityService;
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>();

        if (isFormSubmission(request)) {
            String error = handleParameters(request);
            map.put("error", error);
            if (error == null) {
                map.put("reload", true);
            }
        }
        ModelAndView result = super.handleRequestInternal(request, response);
        
        map.put("users", securityService.getApproveUsers());
        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);
        
        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        
        result.addObject("model", map);
        return result;
    }

    /**
     * Determine if the given request represents a form submission.
     *
     * @param request current HTTP request
     * @return if the request represents a form submission
     */
    private boolean isFormSubmission(HttpServletRequest request) {
        return "POST".equals(request.getMethod());
    }

    private String handleParameters(HttpServletRequest request) {
        List<User> users = securityService.getApproveUsers();
        for (User user : users) {
        	
        	String username = user.getUsername();
        	boolean apporved = getParameter(request, "approved", username) != null;
            boolean delete = getParameter(request, "delete", username) != null;            
            
        	if (user.getUsername().equals("admin")) {
        		continue;
        	}
            
            if (delete) {
            	securityService.deleteUser(username);
            } else {
            	
            	if (user.isApproved() != apporved) {
                	user.setApproved(apporved);
                	user.setLocked(!apporved);
                    securityService.updateUser(user);
            	}
        	}
        }
        return null;
    }

    private String getParameter(HttpServletRequest request, String name, String username) {
        return StringUtils.trimToNull(request.getParameter(name + "[" + username + "]"));
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }    
    
}
