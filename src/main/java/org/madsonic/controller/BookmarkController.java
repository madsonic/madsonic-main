/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.Logger;
import org.madsonic.dao.BookmarkDao;
import org.madsonic.domain.Bookmark;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

/**
 * Controller for the bookmark page.
 *
 * @author Martin Karel
 */
public class BookmarkController extends ParameterizableViewController {

    @SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(BookmarkController.class);

    private MediaFileService mediaFileService;
    private SettingsService settingsService;
    private SecurityService securityService;
    private PlayerService playerService;
	private BookmarkDao bookmarkDao;
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);
        
        List<MediaFile> bookmark = getBookmarkFromUser(username);
        mediaFileService.populateStarredDate(bookmark, username);

        map.put("user", securityService.getCurrentUser(request));
        map.put("player", playerService.getPlayer(request, response));
        map.put("bookmark", bookmark);

        map.put("buttonVisibility", userSettings.getButtonVisibility());  
		map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 	
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }
    
    private List<MediaFile> getBookmarkFromUser(String username) {
        List<MediaFile> userBookmark = new ArrayList<MediaFile>();
    	for (Bookmark bookmark : bookmarkDao.getBookmarks(username)) {
    		MediaFile mediafile = mediaFileService.getMediaFile(bookmark.getMediaFileId());
    		userBookmark.add(mediafile);
    	}
		return userBookmark;
    }
    
    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }
    
    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

	public void setBookmarkDao(BookmarkDao bookmarkDao) {
		this.bookmarkDao = bookmarkDao;
	}    
    
}
