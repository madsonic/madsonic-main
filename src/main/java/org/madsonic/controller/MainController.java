/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import org.madsonic.dao.ArtistDao;
import org.madsonic.domain.CoverArtScheme;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MediaFileComparator;
import org.madsonic.domain.Player;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.AdService;
import org.madsonic.service.HotService;
import org.madsonic.service.LastFMService;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.RatingService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
 
import org.apache.commons.lang.StringUtils;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.*;

/**
 * Controller for the main page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MainController extends AbstractController {

    private SecurityService securityService;
    private PlayerService playerService;
    private SettingsService settingsService;
    private RatingService ratingService;
    private MediaFileService mediaFileService;
	private AdService adService;
    private HotService hotService;
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Player player = playerService.getPlayer(request, response);
        int userGroupId = securityService.getCurrentUserGroupId(request);        
        List<MediaFile> mediaFiles = null;
        List<Integer> ids = new ArrayList<Integer>();
        
        try {
            mediaFiles = getMediaFiles(request, userGroupId);
            for (MediaFile mediaFile: mediaFiles) {
            	ids.add(mediaFile.getId());
            }
	    } catch (SecurityException x) { // Ignored SecurityException
	    }
        
        String folderRequest = "";
        String podcastFolder = "";        
        MediaFile requestedDir = new MediaFile();
        
        try {
            // Redirect if root is podcast folder
        	requestedDir = mediaFiles.get(0);
            podcastFolder =  settingsService.getPodcastFolder();
            folderRequest = requestedDir.getPath(); 
        } catch (Throwable th) { // Ignored Throwable
	    }
        
        if (mediaFiles == null) {
            return new ModelAndView(new RedirectView("accessDenied.view"));
        }
        if (mediaFiles.isEmpty()) {
            return new ModelAndView(new RedirectView("notFound.view"));
        }
        if (folderRequest.equalsIgnoreCase(podcastFolder)) {
            return new ModelAndView(new RedirectView("podcastReceiver.view?"));
        }   

        MediaFile dir = mediaFiles.get(0);
        if (dir.isFile()) {
            dir = mediaFileService.getParentOf(dir);
            ids = new ArrayList<Integer>();
            ids.add(dir.getId());
        }

        // Redirect if root directory.
        if (mediaFileService.isRoot(dir)) {
            return new ModelAndView(new RedirectView("home.view?"));
        }
        
        List<MediaFile> children = mediaFiles.size() == 1 ? mediaFileService.getChildrenOf(dir, true, true, true) : getMultiFolderChildren(mediaFiles);
        List<MediaFile> files = new ArrayList<MediaFile>();
        List<MediaFile> subDirs = new ArrayList<MediaFile>();
        for (MediaFile child : children) {
            if (child.isFile()) {
                files.add(child);
            } else {
                subDirs.add(child);
            }
        }

        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);

        mediaFileService.populateStarredDate(dir, username);
        mediaFileService.populateStarredDate(children, username);
        mediaFileService.populateLovedDate(children, username);
        mediaFileService.populateBookmarkPosition(children, username);
        
        map.put("ids", ids);
        map.put("dir", dir);
        map.put("files", files);
        map.put("artist", guessArtist(children));
        map.put("album", guessAlbum(children));        
        map.put("subDirs", subDirs);
        map.put("children", children);
        map.put("ancestors", mediaFileService.getAncestorsOf(dir));
        map.put("musicFolder", settingsService.getMusicFolderByPath(dir.getFolder()));
        map.put("player", player);
        
        map.put("sieblingCoverArtScheme", CoverArtScheme.SMALL);
        map.put("user", securityService.getCurrentUser(request));
        map.put("multipleArtists", isMultipleArtists(children));
        map.put("visibility", userSettings.getMainVisibility());
        map.put("buttonVisibility", userSettings.getButtonVisibility());        
        map.put("showAlbumYear", settingsService.isShowAlbumsYear());
        map.put("updateNowPlaying", request.getParameter("updateNowPlaying") != null);
        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
    
        map.put("partyMode", userSettings.isPartyModeEnabled());
        map.put("brand", settingsService.getBrand());
		map.put("showCreateLink", settingsService.isShowCreateLink());
		map.put("showMediaType", settingsService.isShowMediaType());
		map.put("showQuickEdit", settingsService.isShowQuickEdit());
		map.put("showArtistInfo", userSettings.isShowArtistInfoEnabled());
		map.put("lastFMTopTrackSearch", settingsService.getLastFMTopTrackSearch());
		
        map.put("showAd", !settingsService.isLicenseValid() && adService.showAd());
        
		String viewAs = ServletRequestUtils.getStringParameter(request, "viewAs", userSettings.getViewAs());
        if (viewAs != userSettings.getViewAs()) {
	            userSettings.setViewAs(viewAs);
	            userSettings.setChanged(new Date());
	            settingsService.updateUserSettings(userSettings);
	        }
        map.put("viewAs", viewAs);
		
        try {
            MediaFile parent = mediaFileService.getParentOf(dir);
            map.put("parent", parent);
            map.put("navigateUpAllowed", !mediaFileService.isRoot(parent));
        } catch (SecurityException x) { // Happens if Podcast directory is outside music folder.
        }

        Integer userRating = ratingService.getRatingForUser(username, dir);
        Double averageRating = ratingService.getAverageRating(dir);

        if (userRating == null) {
            userRating = 0;
        }
        if (averageRating == null) {
            averageRating = 0.0D;
        }
        map.put("userRating", 10 * userRating);
        map.put("averageRating", Math.round(10.0D * averageRating));

        Integer hotRating = hotService.getAlbumHotFlag(username, dir);
    	map.put("hotRating", hotRating > 0 ? 1 : 0);
        map.put("starred", mediaFileService.getMediaFileStarredDate(dir.getId(), username) != null);

        CoverArtScheme scheme = player.getCoverArtScheme();
        
        if (scheme != CoverArtScheme.OFF) {
        	
            List<MediaFile> coverArts = getCoverArts(dir, children);
            int size = coverArts.size() > 1 ? scheme.getSize() : scheme.getSize() * 2 -20;
            
            map.put("coverArts", coverArts);
            map.put("coverArtSize", size);
            map.put("coverArtHQ", settingsService.isCoverArtHQ());
            
            if (dir.getCoverArtPath() == null && dir.isSingleArtist()) {
                map.put("coverArtSize", scheme.getSize());
                if (settingsService.isShowGenericArtistArt()){
                    map.put("showGenericArtistArt", true);
                } else {
                	map.put("showGenericArtistArt", false);
                }
            }            
            if (coverArts.isEmpty() && dir.isAlbum()) {
                map.put("showGenericCoverArt", true);
            }
            if (coverArts.isEmpty() && dir.isDirectory()) {
                map.put("showGenericCoverArt", true);
            }
            if (dir.isAlbum() || dir.isAlbumSet()) {
                map.put("sieblingAlbums", getSieblingAlbums(dir));
            }
        }
        
        ModelAndView result = null;
        String type = ServletRequestUtils.getStringParameter(request, "type"); 
        if (type != null && type.equalsIgnoreCase("default") || isUrlOnly(children)) {
            result = new ModelAndView("main");  	
        } else {
            result = new ModelAndView(isVideoOnly(children) ? "videoMain" : "main");
        }
        
        result.addObject("model", map);
        return result;
    }

//	private int isImageOnly(List<MediaFile> children) {
//		int imgFound = 0;
//        for (MediaFile child : children) {
//            if (child.isImage()) {
//                imgFound++;
//            }
//        }
//        return imgFound;
//    }

	private boolean isUrlOnly(List<MediaFile> children) {
        boolean urlFound = false;
        for (MediaFile child : children) {
            if (child.isAudio()) {
                return false;
            }
            if (child.isUrl()) {
                urlFound = true;
            }
        }
        return urlFound;
    }
	
    private boolean isVideoOnly(List<MediaFile> children) {
        boolean videoFound = false;
        for (MediaFile child : children) {
            if (child.isAudio()) {
                return false;
            }
            if (child.isVideo() || child.isTv()) {
                videoFound = true;
            }
        }
        return videoFound;
    }

    private List<MediaFile> getMediaFiles(HttpServletRequest request, int user_group_id) {
    	
        List<MediaFile> mediaFiles = new ArrayList<MediaFile>();
        for (String path : ServletRequestUtils.getStringParameters(request, "path")) {
            MediaFile mediaFile = mediaFileService.getMediaFile(path);
            if (mediaFile != null) {
                mediaFiles.add(mediaFile);
            }
        }
        for (int id : ServletRequestUtils.getIntParameters(request, "id")) {
            MediaFile mediaFile = mediaFileService.getMediaFile(id, user_group_id);
            if (mediaFile != null) {
                mediaFiles.add(mediaFile);
            }
        }
        return mediaFiles;
    }

    private String guessArtist(List<MediaFile> files) {
        for (MediaFile file : files) {
            if (file.isFile() || file.isAlbum() || file.isAlbumSet()) {
	            if (file.getArtist() != null) {
	                return file.getArtist();
	            }
            }
        }
        return null;
    }

    private String guessAlbum(List<MediaFile> files) {
        for (MediaFile file : files) {
            if (file.isFile() && file.getArtist() != null) {
                return file.getAlbumName();
            }
        }
        return null;
    }

    private List<MediaFile> getCoverArts(MediaFile dir, List<MediaFile> children) throws IOException {
    	int limit = settingsService.getCoverArtLimit();
    	if (limit == 0) {
    		limit = Integer.MAX_VALUE;
    	}
    	List<MediaFile> coverArts = new ArrayList<MediaFile>();

    	// ### Get ARTIST MAIN FOLDER
    	if (dir.getCoverArtPath() != null) {
    		coverArts.add(dir);
    	} 
    	// ### CHILDREN ####   
    	List<MediaFile> storedChildren = mediaFileService.getChildrenOf(children, true, true, true);
    	for (MediaFile child : storedChildren) {
    		if (child.getCoverArtPath() != null) {
    			coverArts.add(child);
    		} else {
    			// Subfolder detection	
    			List<MediaFile> substoredChildren = mediaFileService.getChildrenOf(child, true, true, true);
    			for (MediaFile subchild : substoredChildren) {
    				if (subchild.getCoverArtPath() != null) {
    					coverArts.add(subchild);
    					break;
    				}
    			}
    		}
    		if (coverArts.size() > limit) {
    			break;
    		}
    	}
    	return coverArts;
    }

    private List<MediaFile> getMultiFolderChildren(List<MediaFile> mediaFiles) throws IOException {
        SortedSet<MediaFile> result = new TreeSet<MediaFile>(new MediaFileComparator(settingsService.isSortAlbumsByFolder()));
        for (MediaFile mediaFile : mediaFiles) {
            if (mediaFile.isFile()) {
                mediaFile = mediaFileService.getParentOf(mediaFile);
            }
            result.addAll(mediaFileService.getChildrenOf(mediaFile, true, true, true));
        }
        return new ArrayList<MediaFile>(result);
    }

    private boolean isMultipleArtists(List<MediaFile> children) {
        // Collect unique artist names.
        Set<String> artists = new HashSet<String>();
        for (MediaFile child : children) {
            if (child.getArtist() != null) {
                artists.add(child.getArtist().toLowerCase());
            }
        }

        // If zero or one artist, it is definitely not multiple artists.
        if (artists.size() < 2) {
            return false;
        }

        // Fuzzily compare artist names, allowing for some differences in spelling, whitespace etc.
        List<String> artistList = new ArrayList<String>(artists);
        for (String artist : artistList) {
            if (StringUtils.getLevenshteinDistance(artist, artistList.get(0)) > 3) {
                return true;
            }
        }
        return false;
    }

//    private void setSieblingAlbums(MediaFile dir, Map<String, Object> map) throws IOException {
//    	
//        MediaFile parent = null;
//        if (!mediaFileService.isRoot(dir)) {
//            parent = mediaFileService.getParentOf(dir);
//    	}
//
//        if (dir.isAlbum() && !mediaFileService.isRoot(parent) && (!"podcast".equalsIgnoreCase(parent.toString()))  ) {  
//            List<MediaFile> sieblings = mediaFileService.getChildrenOf(parent, false, true, true);
//            sieblings.remove(dir);
//
//            int limit = settingsService.getCoverArtLimit();
//            if (limit == 0) {
//                limit = Integer.MAX_VALUE;
//            }
//            if (sieblings.size() > limit) {
//                sieblings = sieblings.subList(0, limit);
//            }
//            map.put("sieblingAlbums", sieblings);
//        }
//    }

    private List<MediaFile> getSieblingAlbums(MediaFile dir) {
        List<MediaFile> result = new ArrayList<MediaFile>();

        MediaFile parent = mediaFileService.getParentOf(dir);
        if (!mediaFileService.isRoot(parent)) {
            List<MediaFile> sieblings = mediaFileService.getChildrenOf(parent, false, true, true);
            for (MediaFile siebling : sieblings) {
                if (siebling.isAlbum() && !siebling.equals(dir)) {
                    result.add(siebling);
                }
            }
        }
        return result;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setRatingService(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    public void setAdService(AdService adService) {
        this.adService = adService;
    }

    public void setHotService(HotService hotService) {
        this.hotService = hotService;
    }
    
    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }
    
    public void setArtistDao(ArtistDao artistDao) {
    }      

    public void setLastFMService(LastFMService lastFMService) {
    }      
}
