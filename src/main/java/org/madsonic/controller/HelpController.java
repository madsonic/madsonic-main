/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.commons.lang.StringUtils;
import org.madsonic.Logger;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.TranscodingService;
import org.madsonic.service.VersionService;
import org.madsonic.util.CustomTheme;
import org.madsonic.util.OperatingSystem;
import org.madsonic.util.StringUtil;
import org.madsonic.util.Util;
import org.madsonic.util.VersionChecker;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

/**
 * Controller for the help page.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class HelpController extends ParameterizableViewController {

    private VersionService versionService;
    private SettingsService settingsService;
    private SecurityService securityService;
    private TranscodingService transcodingService;    
    
    private final org.madsonic.controller.REST.Madsonic.JAXBWriter madsonicJaxbWriter = new org.madsonic.controller.REST.Madsonic.JAXBWriter();
    private final org.madsonic.controller.REST.Subsonic.JAXBWriter subsonicJaxbWriter = new org.madsonic.controller.REST.Subsonic.JAXBWriter();
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        	map.put("localIp", settingsService.getLocalIpAddress());
            map.put("licenseInfo", settingsService.getLicenseInfo());       
            
        
        if (versionService.isNewFinalVersionAvailable()) {
            map.put("newVersionAvailable", true);
            map.put("latestVersion", versionService.getLatestFinalVersion());
            
        } else if (versionService.isNewBetaVersionAvailable()) {
            map.put("newVersionAvailable", true);
//          map.put("latestVersion", versionService.getLatestBetaVersion());
            map.put("latestVersion", versionService.getLatestAlphaVersion());
            
        }

        File HSQLDB = new File(SettingsService.getMadsonicHome() + Util.getDefaultSlash() + "db");
        long sizeHSQLDB = HSQLDB.exists() ? FileUtils.sizeOfDirectory(HSQLDB) / 1024 / 1024 : 0 ;
        
        File lastFMCache = new File(SettingsService.getMadsonicHome() + Util.getDefaultSlash() + "lastfmcache");
        long sizeLastFMCache = lastFMCache.exists() ? FileUtils.sizeOfDirectory(lastFMCache) / 1024 / 1024 : 0 ;
        
        File luceneCache = new File(SettingsService.getMadsonicHome() + Util.getDefaultSlash() + "lucene30");
        long sizeLuceneCache = luceneCache.exists() ? FileUtils.sizeOfDirectory(luceneCache) / 1024 / 1024 : 0 ;

        File thumbsCache = new File(SettingsService.getMadsonicHome() + Util.getDefaultSlash() + "thumbs");
        long sizeThumbsCache = thumbsCache.exists() ? FileUtils.sizeOfDirectory(thumbsCache) / 1024 / 1024 : 0 ;
        
        String cacheInfo;
        
        cacheInfo = "Size: Database: " + sizeHSQLDB + 
	        		"MB, Thumbs-Cache: " + sizeThumbsCache + 
	        		"MB, LastFM-Cache: " + sizeLastFMCache + 
	        		"MB, Lucene-Cache: " + sizeLuceneCache + "MB";
        
    	// Check transcoder
    	String ffmpegVersion = "WARNING no transcoder found!";
        String commandLine = settingsService.getDownsamplingCommand();
        boolean transcoderFound = isTranscodingStepInstalled(commandLine);
        File transcoderFolder = transcodingService.getTranscodeDirectory();
        String transcoderPath = transcoderFolder + Util.getDefaultSlash() + StringUtil.split(commandLine)[0] + (OperatingSystem.isWindows() == true ? ".exe" : "");
    	
        if (transcoderFound) {
        	ProcessBuilder processBuilder = new ProcessBuilder(transcoderFolder + Util.getDefaultSlash() + StringUtil.split(commandLine)[0], "-version" );    
        	try {
        		int readInt;
        		Process process = processBuilder.start();
        		InputStream instream = process.getInputStream();
        		StringBuffer commandResult = new StringBuffer();
        		while ((readInt = instream.read()) != -1) commandResult.append((char)readInt);
        		BufferedReader b = new BufferedReader(new StringReader(commandResult.toString()));
        		for (String line = b.readLine(); line != null; line = b.readLine()) {
        			if (line.contains("ffmpeg version")) {
        				ffmpegVersion = line;
        				File file = new File(transcoderPath);
        				SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        				String ffmpegFileDate = sdf.format(file.lastModified());
        				String searchString = "Copyright";
        				if (ffmpegVersion.contains(searchString)) {
            				ffmpegVersion = ffmpegVersion.replace(searchString, " (" +ffmpegFileDate + ") <br>" + searchString);
        				} else {
            				ffmpegVersion = ffmpegVersion + " (" +ffmpegFileDate + ")";
        				}
        				break;
        			}
        		}
        	} catch (IOException e) {
        	} catch (Exception e) {
        	}
        }
        long totalMemory = Runtime.getRuntime().totalMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();

        String serverInfo = request.getSession().getServletContext().getServerInfo() +
                            ", java " + System.getProperty("java.version") +
                            ", " + OperatingSystem.osVersion(); 

        map.put("licenseInfo", settingsService.getLicenseInfo());
        map.put("brand", settingsService.getBrand());
        map.put("localVersion", versionService.getLocalVersion());
        map.put("buildTime", versionService.getLocalBuildTime());
        map.put("buildDate", versionService.getLocalBuildDate());
        map.put("buildNumber", versionService.getLocalBuildNumber());
        map.put("serverInfo", serverInfo);
        map.put("cacheInfo", cacheInfo);        
        map.put("springInfo", VersionChecker.getSpringFrameworkVersion());
        map.put("databaseInfo", VersionChecker.getSpringSecurityVersion());
        map.put("transcoderPath", transcoderPath);
        map.put("transcoderFound", !ffmpegVersion.contains("WARNING"));
        map.put("ffmpegInfo", ffmpegVersion);
        map.put("madsonicRestInfo", madsonicJaxbWriter.getRestProtocolVersion());   
        map.put("subsonicRestInfo", subsonicJaxbWriter.getRestProtocolVersion());   
        map.put("usedMemory", totalMemory - freeMemory);
        map.put("totalMemory", totalMemory);
        map.put("logEntries", Logger.getLatestLogEntries(settingsService.isLogfileReverse()));
        map.put("logFile", Logger.getLogFile());
        map.put("user", securityService.getCurrentUser(request));
        
        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);
        
        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
       
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }

    private boolean isTranscodingStepInstalled(String step) {
        if (StringUtils.isEmpty(step)) {
            return true;
        }
        String executable = StringUtil.split(step)[0];
        PrefixFileFilter filter = new PrefixFileFilter(executable);
        String[] matches = transcodingService.getTranscodeDirectory().list(filter);
        return matches != null && matches.length > 0;
    }
    
    public void setVersionService(VersionService versionService) {
        this.versionService = versionService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }
    
    public void setTranscodingService(TranscodingService transcodingService) {
        this.transcodingService = transcodingService;
    }
    
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
    
}
