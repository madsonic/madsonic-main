/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * Controller for the ultimate followMe box.
 *
 * @author Martin Karel
 */
public class FollowMeSettingsController extends ParameterizableViewController {

    private SettingsService settingsService;
    private SecurityService securityService;    

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> map = new HashMap<String, Object>();

        if (isFormSubmission(request)) {
            String error = handleParameters(request);
            map.put("error", error);
            if (error == null) {
                map.put("reload", true);
            }
        }
        ModelAndView result = super.handleRequestInternal(request, response);
        
        map.put("similarAlbumtitle", settingsService.getPandoraResultAlbum());
        map.put("similarArtists", settingsService.getPandoraResultArtist());
        map.put("similarArtistsTopTrack", settingsService.getPandoraResultArtistTopTrack());
        map.put("similarGenre", settingsService.getPandoraResultGenre());
        map.put("similarMood", settingsService.getPandoraResultMood());
        map.put("similarOther", settingsService.getPandoraResultSimilar());

        User activeUser = securityService.getCurrentUser(request);
        UserSettings activeUserSettings = settingsService.getUserSettings(activeUser.getUsername());

        map.put("customScrollbar", activeUserSettings.isCustomScrollbarEnabled());     
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(activeUserSettings.getThemeId() == null ? settingsService.getThemeId() : activeUserSettings.getThemeId()));
        
        result.addObject("model", map);
        return result;
    }

    /**
     * Determine if the given request represents a form submission.
     *
     * @param request current HTTP request
     * @return if the request represents a form submission
     */
    private boolean isFormSubmission(HttpServletRequest request) {
        return "POST".equals(request.getMethod());
    }

    private String handleParameters(HttpServletRequest request) {

        String similarAblumtitle = StringUtils.trimToNull(request.getParameter("similarAlbumtitle"));
        String similarArtists = StringUtils.trimToNull(request.getParameter("similarArtists"));
        String similarArtistsTopTrack = StringUtils.trimToNull(request.getParameter("similarArtistsTopTrack"));
        String similarGenre = StringUtils.trimToNull(request.getParameter("similarGenre"));
        String similarMood = StringUtils.trimToNull(request.getParameter("similarMood"));
        String similarOther = StringUtils.trimToNull(request.getParameter("similarOther"));
        
        settingsService.setPandoraResultAlbum(Integer.valueOf(similarAblumtitle));
        settingsService.setPandoraResultArtist(Integer.valueOf(similarArtists));
        settingsService.setPandoraResultArtistTopTrack(Integer.valueOf(similarArtistsTopTrack));
        settingsService.setPandoraResultGenre(Integer.valueOf(similarGenre));
        settingsService.setPandoraResultMood(Integer.valueOf(similarMood));
        settingsService.setPandoraResultSimilar(Integer.valueOf(similarOther));
        
        settingsService.save();
        
        return null;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }
    
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }    
}
