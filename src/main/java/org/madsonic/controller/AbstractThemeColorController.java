/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import org.springframework.web.servlet.support.*;
import org.springframework.web.servlet.mvc.*;
import org.springframework.ui.context.*;

import javax.servlet.http.*;
import java.awt.*;
import java.util.*;

/**
 * Abstract super class for controllers which generate covers.
 *
 * @author Martin Karel
 */
public abstract class AbstractThemeColorController implements Controller {

    protected Color getDarkColor(HttpServletRequest request) {
        return getColor("darkColor", request);
    }
    
    protected Color getDefaultColor(HttpServletRequest request) {
        return getColor("defaultColor", request);
    }
    
    protected Color getLightColor(HttpServletRequest request) {
        return getColor("lightColor", request);
    }
    
    protected Color getAccentColor(HttpServletRequest request) {
        return getColor("accentColor", request);
    }

    private Color getColor(String code, HttpServletRequest request) {
        Theme theme = RequestContextUtils.getTheme(request);
        Locale locale = RequestContextUtils.getLocale(request);
        String colorHex = theme.getMessageSource().getMessage(code, new Object[0], locale);
        return new Color(Integer.parseInt(colorHex, 16));
    }
}
