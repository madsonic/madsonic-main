/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.madsonic.command.NetworkSettingsCommand;
import org.madsonic.domain.UrlRedirectType;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.NetworkService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.util.CustomTheme;
/**
 * Controller for the page used to change the network settings.
 *
 * @author Sindre Mehus, Martin Karel
 */
@SuppressWarnings("deprecation")
public class NetworkSettingsController extends SimpleFormController {

	private SettingsService settingsService;
    private SecurityService securityService;	
    private NetworkService networkService;

    private static final long TRIAL_DAYS = SettingsService.TRIAL_DAYS;
    
    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {
    Map<String, Object> model = new HashMap<String, Object>();
    
    User user = securityService.getCurrentUser(request);
    UserSettings userSettings = settingsService.getUserSettings(user.getUsername());
    
    model.put("user", user);
    
    model.put("customScrollbar", userSettings.isCustomScrollbarEnabled());    
    model.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));

    return model;
    }     
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
    	
        NetworkSettingsCommand command = new NetworkSettingsCommand();
        command.setPortForwardingEnabled(settingsService.isPortForwardingEnabled());
        command.setUrlRedirectionEnabled(settingsService.isUrlRedirectionEnabled());
        command.setUrlRedirectType(settingsService.getUrlRedirectType().name());
        command.setUrlRedirectFrom(settingsService.getUrlRedirectFrom());
        command.setUrlRedirectCustomUrl(settingsService.getUrlRedirectCustomUrl());
        command.setPort(settingsService.getPort());
        command.setLicenseInfo(settingsService.getLicenseInfo());

        return command;
    }

    protected void doSubmitAction(Object cmd) throws Exception {
        NetworkSettingsCommand command = (NetworkSettingsCommand) cmd;
        command.setToast(true);

        settingsService.setPortForwardingEnabled(command.isPortForwardingEnabled());
        settingsService.setUrlRedirectionEnabled(command.isUrlRedirectionEnabled());
        settingsService.setUrlRedirectType(UrlRedirectType.valueOf(command.getUrlRedirectType()));
        settingsService.setUrlRedirectFrom(StringUtils.lowerCase(command.getUrlRedirectFrom()));
        settingsService.setUrlRedirectCustomUrl(StringUtils.trimToEmpty(command.getUrlRedirectCustomUrl()));

        if (!settingsService.isLicenseValid() && settingsService.getTrialExpires() == null) {
            Date expiryDate = new Date(System.currentTimeMillis() + TRIAL_DAYS * 24L * 3600L * 1000L);
            settingsService.setTrialExpires(expiryDate);
        }
        
        if (settingsService.getServerId() == null) {
            Random rand = new Random(System.currentTimeMillis());
            settingsService.setServerId(String.valueOf(Math.abs(rand.nextLong())));
        }

        settingsService.save();
        networkService.initPortForwarding(0);
        networkService.initUrlRedirection(true);
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }    
    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }
}