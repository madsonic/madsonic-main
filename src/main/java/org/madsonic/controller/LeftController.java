/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.ParameterizableViewController;
import org.springframework.web.servlet.support.RequestContextUtils;

import org.madsonic.domain.MediaLibraryStatistics;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.MediaScannerService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.VersionService;
import org.madsonic.util.CustomTheme;
import org.madsonic.util.StringUtil;

/**
 * Controller for the left index frame.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class LeftController extends ParameterizableViewController {

    private SettingsService settingsService;
    private SecurityService securityService;
    
//    private MediaFileService mediaFileService;
    
    private VersionService versionService;    
    private MediaScannerService mediaScannerService;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
        Map<String, Object> map = new HashMap<String, Object>();

        String username = securityService.getCurrentUsername(request);
        UserSettings userSettings = settingsService.getUserSettings(username);
        MediaLibraryStatistics statistics = mediaScannerService.getStatistics();
        
        Locale locale = RequestContextUtils.getLocale(request);        
        
        map.put("showLeftBarShrinked", userSettings.isShowLeftBarShrinked());
        map.put("showLeftBar", userSettings.isShowLeftBar());
        map.put("showLeftPanel", userSettings.isShowLeftPanel());
        map.put("showRightPanel", userSettings.isShowRightPanel());
		map.put("showSidePanel", userSettings.isShowSidePanel());
		
        map.put("customScrollbar", userSettings.isCustomScrollbarEnabled()); 
        map.put("customScrollbarTheme", CustomTheme.setCustomTheme(userSettings.getThemeId() == null ? settingsService.getThemeId() : userSettings.getThemeId()));
        map.put("customlogo", settingsService.getCustomLogo());
        map.put("snowEnabled", settingsService.isSnowEnabled());
        
        map.put("brand", settingsService.getBrand());
        
        User user = securityService.getCurrentUser(request);
        map.put("user", user);
        map.put("listType", userSettings.getListType());
        map.put("licensed", settingsService.isLicenseValid());
        map.put("licenseInfo", settingsService.getLicenseInfo());        
        
        if (userSettings.isFinalVersionNotificationEnabled() && versionService.isNewFinalVersionAvailable()) {
            map.put("newVersionAvailable", true);
            map.put("latestVersion", versionService.getLatestFinalVersion());

        } else if (userSettings.isBetaVersionNotificationEnabled() && versionService.isNewBetaVersionAvailable()) {
            map.put("newVersionAvailable", true);
            map.put("latestVersion", versionService.getLatestAlphaVersion());
        }

        map.put("NotificationEnabled", userSettings.isBetaVersionNotificationEnabled() || userSettings.isFinalVersionNotificationEnabled()); 

        if (securityService.getCurrentUser(request).isAdminRole()){
            map.put("NotificationEnabled", true);
        }        
        
        map.put("showIconHome", settingsService.showIconHome());
        map.put("showIconIndex", settingsService.showIconIndex());
        map.put("showIconArtist", settingsService.showIconArtist());
        map.put("showIconPlaying", settingsService.showIconPlaying());
        map.put("showIconCover", settingsService.showIconCover());
        map.put("showIconStarred", settingsService.showIconStarred());
        map.put("showIconLoved", settingsService.showIconLoved());
        map.put("showIconBookmark", settingsService.showIconBookmark());
        map.put("showIconRadio", settingsService.showIconRadio());
		try {
			if (userSettings.isLastFmEnabled() &&
			    userSettings.getLastFmUsername().length() > 0) {
				map.put("showIconLastFM", true);
			}
		} catch (Throwable ex) {
		} 
        map.put("showIconPodcast", settingsService.showIconPodcast());
        map.put("showIconPodcastManage", settingsService.showIconPodcastManage());        
        map.put("showIconSettings", settingsService.showIconSettings());
        map.put("showIconStatus", settingsService.showIconStatus());
        map.put("showIconSocial", settingsService.showIconSocial());
        map.put("showIconHistory", settingsService.showIconHistory());
        map.put("showIconStatistics", settingsService.showIconStatistics());
        map.put("showIconPlaylists", settingsService.showIconPlaylists());
        map.put("showIconPlaylistEditor", settingsService.showIconPlaylistEditor());
        map.put("showIconPlaylistManage", settingsService.showIconPlaylistManage());
        map.put("showIconMore", settingsService.showIconMore());
        map.put("showIconRandom", settingsService.showIconRandom());
        map.put("showIconGenre", settingsService.showIconGenre());
        map.put("showIconMoods", settingsService.showIconMoods());
        map.put("showIconAbout", settingsService.showIconAbout());        
        map.put("showIconInternetRadio", settingsService.showIconInternetRadio());        
        
        if (securityService.getCurrentUser(request).isAdminRole()){
	        if (settingsService.showIconAdmins()){
	        	// to default
	        } else {
	            map.put("showIconHome", true);
	            map.put("showIconIndex", true);
	            map.put("showIconArtist", true);
	            map.put("showIconPlaying", true);
	            map.put("showIconCover", true);
	            map.put("showIconBookmark", false);
	            map.put("showIconStarred", true);
	            map.put("showIconLoved", true);
	            map.put("showIconGenre", true);
	            map.put("showIconRadio", true);
	            map.put("showIconPodcast", true);
	            map.put("showIconPodcastManage", true);
	            map.put("showIconSettings", true);
	            map.put("showIconStatus", true);
	            map.put("showIconSocial", true);
	            map.put("showIconStatistics", true);
	            map.put("showIconPlaylists", true);
	            map.put("showIconPlaylistEditor", false);
	            map.put("showIconPlaylistManage", false);
	            map.put("showIconMore", true);
	            map.put("showIconRandom", true);
	            map.put("showIconMoods", false);
	            map.put("showIconAbout", false);
	            map.put("showIconInternetRadio", false);
	        }
        }
        
        if (statistics != null) {
            map.put("statistics", statistics);
            long bytes = statistics.getTotalLengthInBytes();
            long hours = statistics.getTotalDurationInSeconds() / 3600L;
            map.put("hours", hours);
            map.put("bytes", StringUtil.formatBytes(bytes, locale));
        }        
        
        ModelAndView result = super.handleRequestInternal(request, response);
        result.addObject("model", map);
        return result;
    }

    public void setMediaScannerService(MediaScannerService mediaScannerService) {
        this.mediaScannerService = mediaScannerService;
    }

    public void setVersionService(VersionService versionService) {
        this.versionService = versionService;
    }
    
    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
