/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.controller;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.madsonic.Logger;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * Controller which produces images for the Sonos Controller.
 *
 * @author Martin Karel
 */
public class SonosArtController implements Controller {

	private static final Logger LOG = Logger.getLogger(SonosArtController.class);	

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Integer size = ServletRequestUtils.getIntParameter(request, "size");
		String cover = request.getParameter("q");

		if (cover != null) {
			InputStream in = null;
			BufferedImage sonosImage = null;
			try {
				in = getClass().getResourceAsStream("sonos/" + cover + ".png");
				sonosImage = ImageIO.read(in);

				if (size != null && size > 0) {
					sonosImage = scale(sonosImage, size, size);
				}
			} catch (IllegalArgumentException iae) { LOG.warn("sonosArt error: " + cover + " not found.");
			} catch (Exception ex) { LOG.warn("sonosArt error: " + ex.getMessage()); } 

			try {
				ImageIO.write(sonosImage, "png", response.getOutputStream());
			} finally {
				IOUtils.closeQuietly(in);
			}
		}
		return null;
	}

	public static BufferedImage scale(BufferedImage image, int width, int height) {
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage thumb = image;

		// For optimal results, use step by step bilinear resampling - halfing the size at each step.
		do {
			w /= 2;
			h /= 2;
			if (w < width) {
				w = width;
			}
			if (h < height) {
				h = height;
			}

			BufferedImage temp = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = temp.createGraphics();
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2.drawImage(thumb, 0, 0, temp.getWidth(), temp.getHeight(), null);
			g2.dispose();

			thumb = temp;
		} while (w != width);

		return thumb;
	}	
}
