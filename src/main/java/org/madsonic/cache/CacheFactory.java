/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2012 (C) Martin Karel
 *  
 */
package org.madsonic.cache;

import java.io.File;

import org.springframework.beans.factory.InitializingBean;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;

import org.madsonic.Logger;
import org.madsonic.service.SettingsService;

/**
 * Initializes Ehcache 
 *
 */
public class CacheFactory implements InitializingBean {

    @SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(CacheFactory.class);
    private CacheManager cacheManager;

    public void afterPropertiesSet() throws Exception {
        
        Configuration configuration = ConfigurationFactory.parseConfiguration();
        File cacheDir = new File(SettingsService.getMadsonicHome(), "cache");
        configuration.getDiskStoreConfiguration().setPath(cacheDir.getPath());
        cacheManager = CacheManager.create(configuration);
    }

    public Ehcache getCache(String name) {
        return cacheManager.getCache(name);
    }

}
