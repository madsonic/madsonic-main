/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.madsonic.Logger;

import static org.madsonic.controller.REST.Subsonic.RESTController.ErrorCode.GENERIC;
import static org.madsonic.controller.REST.Subsonic.RESTController.ErrorCode.MISSING_PARAMETER;

import org.madsonic.controller.REST.Subsonic.JAXBWriter;
import org.madsonic.controller.REST.Subsonic.RESTController;

import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.util.NestedServletException;

/**
 * Intercepts exceptions thrown by the subsonic RESTController.
 *
 * Also adds the CORS response header (http://enable-cors.org)
 *
 * @author Sindre Mehus, Martin Karel
 * @version $
 */
public class RESTSubsonicFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(RESTSubsonicFilter.class);

    private final JAXBWriter jaxbWriter = new JAXBWriter();

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletResponse response = (HttpServletResponse) res;
            response.setHeader("Access-Control-Allow-Origin", "*");
            chain.doFilter(req, res);
        } catch (Throwable x) {
            handleException(x, (HttpServletRequest) req, (HttpServletResponse) res);
        }
    }

    private void handleException(Throwable x, HttpServletRequest request, HttpServletResponse response) {
        if (x instanceof NestedServletException && x.getCause() != null) {
            x = x.getCause();
        }

        RESTController.ErrorCode code = (x instanceof ServletRequestBindingException) ? MISSING_PARAMETER : GENERIC;
        String msg = getErrorMessage(x);
        
        if (msg.contentEquals("EofException")) {
         // LOG.warn("Error in Subsonic REST API layer: broken Stream");
        }
        else
        {
            LOG.warn("Error in Subsonic REST API layer: " + msg, x);

        try {
            jaxbWriter.writeErrorResponse(request, response, code, msg);
        } catch (Exception e) {
            LOG.error("Failed to write error response.", e);
        }
		}
    }

    private String getErrorMessage(Throwable x) {
        if (x.getMessage() != null) {
            return x.getMessage();
        }
        return x.getClass().getSimpleName();
    }

    public void init(FilterConfig filterConfig) {
    }

    public void destroy() {
    }
}
