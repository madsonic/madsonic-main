/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2017 (C) Martin Karel
 *  
 */
package org.madsonic.util;

/**
 * @author Martin Karel
 *
 */
public class CustomTheme {

	public static String setCustomTheme(String themeId) {
		
		switch (themeId) {
		case "default": return "dark-2"; 
		case "madsonic": return "dark-2"; 		
		case "sigelmedia": return "rounded";
		case "madsonic_sunny": return "dark-2";
		case "madsonic_dark" : return "light-2";
		case "madsonic_black" : return "light-2";
		case "madsonic_4klight" : return "dark-2";		
		case "madsonic_4kdark" : return "rounded";		
		case "madsonic_4kblack" : return "light-thin";
		case "madsonic_4kpetrol" : return "light-thin";
		default: return "light-2";
		}
	}	
}