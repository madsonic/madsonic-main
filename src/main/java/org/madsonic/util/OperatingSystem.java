/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2009-2022 (C) Martin Karel
 *  
 */
 
package org.madsonic.util;

import java.util.Properties;

public enum OperatingSystem {
  WINDOWS,
  LINUX,
  OSX,
  UNKNOWN;

  private static OperatingSystem initOS() {
    String osName = System.getProperty("os.name");

    if(osName.startsWith("Windows "))
    	return WINDOWS;
    
    if(osName.startsWith("Linux"))
    	return LINUX;
    
    if(osName.startsWith("Mac OS X"))
		return OSX;
    
    return UNKNOWN;
  }

  public static final OperatingSystem userOS = initOS();

  /**
   * @return user-displayable operating system version
   */
  public static String osVersion() {
    Properties p = System.getProperties();
    String osName = p.getProperty("os.name");
    String osVersion = p.getProperty("os.version");

    if((osVersion != null) && (osVersion.length() != 0))
      osName += " " + osVersion;

    switch(userOS) {
    case OSX:
        if(osVersion.startsWith("10.2"))
            osName += " Jaguar";
          else if(osVersion.startsWith("10.3"))
            osName += " Panther";
          else if(osVersion.startsWith("10.4"))
            osName += " Tiger";
          else if(osVersion.startsWith("10.5"))
            osName += " Leopard";
          else if(osVersion.startsWith("10.6"))
            osName += " Snow Leopard";
          else if(osVersion.startsWith("10.7"))
              osName += " Lion";
          else if(osVersion.startsWith("10.8"))
              osName += " Mountain Lion";
          else if(osVersion.startsWith("10.9"))
              osName += " Mavericks";
          else if(osVersion.startsWith("10.10"))
              osName += " Yosemite";
          else if(osVersion.startsWith("10.11"))
              osName += " El Capitan";
          else if(osVersion.startsWith("10.12"))
              osName += " Sierra";        
          break;
    case WINDOWS:
      osName += " " + p.getProperty("sun.os.patch.level");
      break;
	case LINUX:
		break;
	case UNKNOWN:
		break;
	default:
		break;
    }

    osName += " (" + p.getProperty("os.arch") + ")";
    return osName;
  }
  
  public static boolean isWindows() {
	  return userOS == WINDOWS ? true : false;
  }

  public static boolean isLinux() {
	  return userOS == LINUX ? true : false;
  }

  public static boolean isOSX() {
	  return userOS == OSX ? true : false;
  }
  
  public static String javaVersion() {
    return "Java " + System.getProperties().getProperty("java.version");
  }
  
}