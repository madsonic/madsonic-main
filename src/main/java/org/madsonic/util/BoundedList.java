/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.util;

import java.util.*;

/**
 * Simple implementation of a bounded list. If the maximum size is reached, adding a new element will
 * remove the first element in the list.
 *
 * @author Sindre Mehus, Martin Karel
 * @version $Id$
 */
@SuppressWarnings("serial")
public class BoundedList<E> extends LinkedList<E> {
    private int maxSize;

    /**
     * Creates a new bounded list with the given maximum size.
     * @param maxSize The maximum number of elements the list may hold.
     */
    public BoundedList(int maxSize) {
        this.maxSize = maxSize;
    }

    /**
     * Adds an element to the tail of the list. If the list is full, the first element is removed.
     * @param e The element to add.
     * @return Always <code>true</code>.
     */
    public boolean add(E e) {
        if (isFull()) {
            removeFirst();
        }
        return super.add(e);
    }

    /**
     * Adds an element to the head of list. If the list is full, the last element is removed.
     * @param e The element to add.
     */
    public void addFirst(E e) {
        if (isFull()) {
            removeLast();
        }
        super.addFirst(e);
    }

    /**
     * Returns whether the list if full.
     * @return Whether the list is full.
     */
    private boolean isFull() {
        return size() == maxSize;
    }
}
