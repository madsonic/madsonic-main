/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2019 (C) Martin Karel
 *
 */
package org.madsonic.util;

import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * @author Martin Karel
 *
 */
public class HttpUtil {
	
	public HttpUtil() {		
	}

	public static String httpGet(String url, int connectTimeoutSeconds, int socketTimeoutSeconds) throws IOException {
		return (String) httpGet(url, connectTimeoutSeconds, socketTimeoutSeconds, new BasicResponseHandler());
	}

	public static <T> T httpGet(String url, int connectTimeoutSeconds, int socketTimeoutSeconds, ResponseHandler<T> responseHandler) throws IOException {
		return (T) httpRequest(new HttpGet(url), connectTimeoutSeconds, socketTimeoutSeconds, responseHandler);
	}

	public static String httpRequest(HttpRequestBase request, int connectTimeoutSeconds, int socketTimeoutSeconds) throws IOException {
		return (String) httpRequest(request, connectTimeoutSeconds, socketTimeoutSeconds, new BasicResponseHandler());
	}

	public static <T> T httpRequest(HttpRequestBase request, int connectTimeoutSeconds, int socketTimeoutSeconds, ResponseHandler<T> responseHandler) throws IOException {
		CloseableHttpClient client = null;
		try {
			client = client(connectTimeoutSeconds, socketTimeoutSeconds);
			return (T)client.execute(request, responseHandler);
		} finally {
			IOUtils.closeQuietly(client);
		}
	}

	public static CloseableHttpClient client(int connectTimeoutSeconds, int socketTimeoutSeconds) {
		RequestConfig requestConfig;		
		requestConfig = RequestConfig.custom()
						.setConnectTimeout(connectTimeoutSeconds * 1000)
						.setConnectionRequestTimeout(connectTimeoutSeconds * 1000)
						.setSocketTimeout(socketTimeoutSeconds * 1000)
						.build();
		return HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
	}
}