/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.security;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

/**
 * @author Sindre Mehus, Martin Karel
 */
@SuppressWarnings("rawtypes")
public class MadsonicApplicationEventListener implements ApplicationListener {

    private LoginFailureLogger loginFailureLogger;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof AbstractAuthenticationFailureEvent) {
            if (event.getSource() instanceof AbstractAuthenticationToken) {
                AbstractAuthenticationToken token = (AbstractAuthenticationToken) event.getSource();
                Object details = token.getDetails();
                if (details instanceof WebAuthenticationDetails) {
                    loginFailureLogger.log(((WebAuthenticationDetails) details).getRemoteAddress(), String.valueOf(token.getPrincipal()));
                }
            }
        }

    }

    public void setLoginFailureLogger(LoginFailureLogger loginFailureLogger) {
        this.loginFailureLogger = loginFailureLogger;
    }
}
