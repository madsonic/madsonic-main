/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.command;

import org.madsonic.controller.AdvancedSettingsController;
import org.madsonic.domain.LicenseInfo;

/**
 * Command used in {@link AdvancedSettingsController}.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class LdapSettingsCommand {

    private boolean ldapEnabled;
    
    private String ldapUrl;
    private String ldapSearchFilter;
    private String ldapGroupSearchBase;
    private String ldapGroupFilter;
    private String ldapGroupAttrib;
    private String ldapManagerDn;
    private String ldapManagerPassword;
    private boolean ldapAutoShadowing;
    private boolean ldapAutoMapping;
    private String ldapGroupSyncType;
    private boolean ldapGroupAutoSync;
    
    private String brand;
    private boolean toast;
    
    private LicenseInfo licenseInfo;

    public boolean isLdapEnabled() {
        return ldapEnabled;
    }

    public void setLdapEnabled(boolean ldapEnabled) {
        this.ldapEnabled = ldapEnabled;
    }

    public String getLdapUrl() {
        return ldapUrl;
    }

    public void setLdapUrl(String ldapUrl) {
        this.ldapUrl = ldapUrl;
    }

    public String getLdapSearchFilter() {
        return ldapSearchFilter;
    }

    public void setLdapSearchFilter(String ldapSearchFilter) {
        this.ldapSearchFilter = ldapSearchFilter;
    }

    public String getLdapManagerDn() {
        return ldapManagerDn;
    }

    public void setLdapManagerDn(String ldapManagerDn) {
        this.ldapManagerDn = ldapManagerDn;
    }

    public String getLdapManagerPassword() {
        return ldapManagerPassword;
    }

    public void setLdapManagerPassword(String ldapManagerPassword) {
        this.ldapManagerPassword = ldapManagerPassword;
    }

    public boolean isLdapAutoShadowing() {
        return ldapAutoShadowing;
    }

    public void setLdapAutoShadowing(boolean ldapAutoShadowing) {
        this.ldapAutoShadowing = ldapAutoShadowing;
    }

    public boolean isToast() {
        return toast;
    }

    public void setToast(boolean toast) {
        this.toast = toast;
    }

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getLdapGroupFilter() {
		return ldapGroupFilter;
	}

	public void setLdapGroupFilter(String ldapGroupFilter) {
		this.ldapGroupFilter = ldapGroupFilter;
	}

	public String getLdapGroupAttrib() {
		return ldapGroupAttrib;
	}

	public void setLdapGroupAttrib(String ldapGroupAttrib) {
		this.ldapGroupAttrib = ldapGroupAttrib;
	}

	/**
	 * @return the ldapAutoMapping
	 */
	public boolean isLdapAutoMapping() {
		return ldapAutoMapping;
	}

	/**
	 * @param ldapAutoMapping the ldapAutoMapping to set
	 */
	public void setLdapAutoMapping(boolean ldapAutoMapping) {
		this.ldapAutoMapping = ldapAutoMapping;
	}

	/**
	 * @return the ldapGroupSearchBase
	 */
	public String getLdapGroupSearchBase() {
		return ldapGroupSearchBase;
	}

	/**
	 * @param ldapGroupSearchBase the ldapGroupSearchBase to set
	 */
	public void setLdapGroupSearchBase(String ldapGroupSearchBase) {
		this.ldapGroupSearchBase = ldapGroupSearchBase;
	}
	/**
	 * 
	 * @return ldapGroupSyncType 
	 */
	public String getLdapGroupSyncType() {
		return ldapGroupSyncType;
	}
	/**
	 * 
	 * @param ldapGroupSyncType 
	 */
	public void setLdapGroupSyncType(String ldapGroupSyncType) {
		this.ldapGroupSyncType = ldapGroupSyncType;
	}

	public LicenseInfo getLicenseInfo() {
		return licenseInfo;
	}

	public void setLicenseInfo(LicenseInfo licenseInfo) {
		this.licenseInfo = licenseInfo;
	}

	public boolean isLdapGroupAutoSync() {
		return ldapGroupAutoSync;
	}

	public void setLdapGroupAutoSync(boolean ldapGroupAutoSync) {
		this.ldapGroupAutoSync = ldapGroupAutoSync;
	}
}
