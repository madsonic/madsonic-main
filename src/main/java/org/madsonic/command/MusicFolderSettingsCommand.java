/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.command;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.madsonic.controller.MusicFolderSettingsController;
import org.madsonic.domain.MusicFolder;
import org.apache.commons.lang.StringUtils;

/**
 * Command used in {@link MusicFolderSettingsController}.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MusicFolderSettingsCommand {

    private String interval;
    private String hour;
    private boolean scanning;
    private boolean fastCache;
    private boolean organizeByFolderStructure;
    private boolean organizeByGenreMap;
    private List<MusicFolderInfo> musicFolders;
    private MusicFolderInfo newMusicFolder;
    private boolean reload;

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public boolean isScanning() {
        return scanning;
    }

    public void setScanning(boolean scanning) {
        this.scanning = scanning;
    }

    public boolean isFastCache() {
        return fastCache;
    }

    public List<MusicFolderInfo> getMusicFolders() {
        return musicFolders;
    }

    public void setMusicFolders(List<MusicFolderInfo> musicFolders) {
        this.musicFolders = musicFolders;
    }

    public void setFastCache(boolean fastCache) {
        this.fastCache = fastCache;
    }

    public MusicFolderInfo getNewMusicFolder() {
        return newMusicFolder;
    }

    public void setNewMusicFolder(MusicFolderInfo newMusicFolder) {
        this.newMusicFolder = newMusicFolder;
    }

    public void setReload(boolean reload) {
        this.reload = reload;
    }

    public boolean isReload() {
        return reload;
    }

    public boolean isOrganizeByFolderStructure() {
        return organizeByFolderStructure;
    }

    public void setOrganizeByFolderStructure(boolean organizeByFolderStructure) {
        this.organizeByFolderStructure = organizeByFolderStructure;
    }

    public boolean isOrganizeByGenreMap() {
		return organizeByGenreMap;
	}

	public void setOrganizeByGenreMap(boolean organizeByGenreMap) {
		this.organizeByGenreMap = organizeByGenreMap;
	}

	public static class MusicFolderInfo {

        private Integer id;
        private String path;
        private String name;
        private boolean enabled;
        private boolean delete;
        private boolean existing;
        private Integer index;
        private Integer type;
        private Integer group;
        private Date timer;
        private Integer interval;
        private String status;
        private String result;
        private String comment;
        private Integer found;
        private Date lastrun;
        

        public MusicFolderInfo(MusicFolder musicFolder) {
            id = musicFolder.getId();
            path = musicFolder.getPath().getPath();
            name = musicFolder.getName();
            enabled = musicFolder.isEnabled();
            existing = musicFolder.getPath().exists() && musicFolder.getPath().isDirectory();
            index = musicFolder.getIndex();
            type =  musicFolder.getType();
            group = musicFolder.getGroup();
            timer = musicFolder.getTimer();
            interval = musicFolder.getInterval();
            status = musicFolder.getStatus();
            result = musicFolder.getResult();
            comment = musicFolder.getComment();
            found = musicFolder.getFound();
            lastrun = musicFolder.getLastrun();
        }

        public MusicFolderInfo() {
            enabled = true;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public boolean isDelete() {
            return delete;
        }

        public void setDelete(boolean delete) {
            this.delete = delete;
        }

        public MusicFolder toMusicFolder() {
            String path = StringUtils.trimToNull(this.path);
            if (path == null) {
                return null;
            }
            File file = new File(path);
            String name = StringUtils.trimToNull(this.name);
            if (name == null) {
                name = file.getName();
            }
            return new MusicFolder(id, new File(path), name, enabled, new Date(), index, type, group, timer, interval, status, result, comment, found, lastrun);
        }

        public boolean isExisting() {
            return existing;
        }

		public Integer getIndex() {
			return index;
		}

		public void setIndex(Integer index) {
			this.index = index;
		}
		
		public Integer getType() {
			return type;
		}

		public void setType(Integer type) {
			this.type = type;
		}

		public Integer getGroup() {
			return group;
		}

		public void setGroup(Integer group) {
			this.group = group;
		}			
		
		public void setTimer(Date timer) {
			this.timer = timer;
		}
		
		public Date getTimer() {
			return timer;
		}
		public void setInterval(Integer interval) {
			this.interval = interval;
		}
		
		
		public Integer getInterval() {
			return interval;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getResult() {
			return result;
		}

		public void setResult(String result) {
			this.result = result;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}
		
		public String getComment() {
			return comment;
		}

		public Integer getFound() {
			return found;
		}

		public void setFound(Integer found) {
			this.found = found;
		}

		public Date getLastrun() {
			return lastrun;
		}

		public void setLastrun(Date lastrun) {
			this.lastrun = lastrun;
		}
		
    }
}