/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.command;

import org.madsonic.controller.PersonalSettingsController;
import org.madsonic.domain.Avatar;
import org.madsonic.domain.Theme;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;

import java.util.List;

/**
 * Command used in {@link PersonalSettingsController}.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PersonalSettingsCommand {
	
    private User  user;
    
    private String localeIndex;
    private String[] locales;
    
    private String themeIndex;
    private Theme[] themes;
    
    private String profile;
    
    private int avatarId;
    private List<Avatar> avatars;
    private Avatar customAvatar;
    
    private UserSettings.Visibility mainVisibility;
    private UserSettings.Visibility playlistVisibility;
    private UserSettings.ButtonVisibility buttonVisibility;
    
    private boolean showLeftBarShrinked;
    private boolean showLeftPanelEnabled;
    private boolean showSidePanelEnabled;
    private boolean showRightPanelEnabled;
    
    private boolean partyModeEnabled;
    private boolean artistInfoEnabled;
    private boolean songNotificationEnabled;

    private boolean nowPlayingAllowed;
    private boolean showNowPlayingEnabled;
    private boolean showChatEnabled;
    
    private boolean lastFmEnabled;
    private String lastFmUsername;
    private String lastFmPassword;
    
    private boolean isReloadNeeded;
    
    private String listType;
    
    private boolean playQueueResizeEnabled;
    
	private boolean customScrollbarEnabled;
	private boolean customAccordionEnabled;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLocaleIndex() {
        return localeIndex;
    }

    public void setLocaleIndex(String localeIndex) {
        this.localeIndex = localeIndex;
    }

    public String[] getLocales() {
        return locales;
    }

    public void setLocales(String[] locales) {
        this.locales = locales;
    }

    public String getThemeIndex() {
        return themeIndex;
    }

    public void setThemeIndex(String themeIndex) {
        this.themeIndex = themeIndex;
    }

    public Theme[] getThemes() {
        return themes;
    }

    public void setThemes(Theme[] themes) {
        this.themes = themes;
    }

    public int getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    public List<Avatar> getAvatars() {
        return avatars;
    }

    public void setAvatars(List<Avatar> avatars) {
        this.avatars = avatars;
    }

    public Avatar getCustomAvatar() {
        return customAvatar;
    }

    public void setCustomAvatar(Avatar customAvatar) {
        this.customAvatar = customAvatar;
    }

    public UserSettings.Visibility getMainVisibility() {
        return mainVisibility;
    }

    public void setMainVisibility(UserSettings.Visibility mainVisibility) {
        this.mainVisibility = mainVisibility;
    }

    public UserSettings.Visibility getPlaylistVisibility() {
        return playlistVisibility;
    }

    public void setPlaylistVisibility(UserSettings.Visibility playlistVisibility) {
        this.playlistVisibility = playlistVisibility;
    }

    public UserSettings.ButtonVisibility getButtonVisibility() {
		return buttonVisibility;
	}

	public void setButtonVisibility(UserSettings.ButtonVisibility buttonVisibility) {
		this.buttonVisibility = buttonVisibility;
	}

	public boolean isPartyModeEnabled() {
        return partyModeEnabled;
    }

    public void setPartyModeEnabled(boolean partyModeEnabled) {
        this.partyModeEnabled = partyModeEnabled;
    }

    public boolean isShowNowPlayingEnabled() {
        return showNowPlayingEnabled;
    }

    public void setShowNowPlayingEnabled(boolean showNowPlayingEnabled) {
        this.showNowPlayingEnabled = showNowPlayingEnabled;
    }

    public boolean isShowChatEnabled() {
        return showChatEnabled;
    }

    public void setShowChatEnabled(boolean showChatEnabled) {
        this.showChatEnabled = showChatEnabled;
    }

    public boolean isNowPlayingAllowed() {
        return nowPlayingAllowed;
    }

    public void setNowPlayingAllowed(boolean nowPlayingAllowed) {
        this.nowPlayingAllowed = nowPlayingAllowed;
    }

    public void setSongNotificationEnabled(boolean songNotificationEnabled) {
        this.songNotificationEnabled = songNotificationEnabled;
    }

    public boolean isSongNotificationEnabled() {
        return songNotificationEnabled;
    }

    public boolean isLastFmEnabled() {
        return lastFmEnabled;
    }

    public void setLastFmEnabled(boolean lastFmEnabled) {
        this.lastFmEnabled = lastFmEnabled;
    }

    public String getLastFmUsername() {
        return lastFmUsername;
    }

    public void setLastFmUsername(String lastFmUsername) {
        this.lastFmUsername = lastFmUsername;
    }

    public String getLastFmPassword() {
        return lastFmPassword;
    }

    public void setLastFmPassword(String lastFmPassword) {
        this.lastFmPassword = lastFmPassword;
    }

    public boolean isReloadNeeded() {
        return isReloadNeeded;
    }

    public void setReloadNeeded(boolean reloadNeeded) {
        isReloadNeeded = reloadNeeded;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public void setPlayQueueResize(boolean PlayQueueResize) {
        this.playQueueResizeEnabled = PlayQueueResize;
    }

	public boolean getPlayQueueResize() {
        return playQueueResizeEnabled;
	}
	
	 public void setPlayQueueResizeEnabled(boolean playQueueResizeEnabled) {
		this.playQueueResizeEnabled = playQueueResizeEnabled;
	 }

	 public boolean getPlayQueueResizeEnabled() {
        return playQueueResizeEnabled;
	 }

	/**
	 * @return the customScrollbarEnabled
	 */
	public boolean isCustomScrollbarEnabled() {
		return customScrollbarEnabled;
	}

	/**
	 * @param customScrollbarEnabled the customScrollbarEnabled to set
	 */
	public void setCustomScrollbarEnabled(boolean customScrollbarEnabled) {
		this.customScrollbarEnabled = customScrollbarEnabled;
	}

	public boolean isCustomAccordionEnabled() {
		return customAccordionEnabled;
	}

	public void setCustomAccordionEnabled(boolean customAccordionEnabled) {
		this.customAccordionEnabled = customAccordionEnabled;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	/**
	 * @return the showSidePanelEnabled
	 */
	public boolean isShowSidePanelEnabled() {
		return showSidePanelEnabled;
	}

	/**
	 * @param showSidePanelEnabled the showSidePanelEnabled to set
	 */
	public void setShowSidePanelEnabled(boolean showSidePanelEnabled) {
		this.showSidePanelEnabled = showSidePanelEnabled;
	}
	
	/**
	 * @return the showRightPanelEnabled
	 */
	public boolean isShowRightPanelEnabled() {
		return showRightPanelEnabled;
	}

	/**
	 * @param showSidePanelEnabled the showRightPanelEnabled to set
	 */
	public void setShowRightPanelEnabled(boolean showRightPanelEnabled) {
		this.showRightPanelEnabled = showRightPanelEnabled;
	}	
	
	/**
	 * @return the artistInfoEnabled
	 */
	public boolean isArtistInfoEnabled() {
		return artistInfoEnabled;
	}

	/**
	 * @param artistInfoEnabled the artistInfoEnabled to set
	 */
	public void setArtistInfoEnabled(boolean artistInfoEnabled) {
		this.artistInfoEnabled = artistInfoEnabled;
	}

	public boolean isShowLeftPanelEnabled() {
		return showLeftPanelEnabled;
	}

	public void setShowLeftPanelEnabled(boolean showLeftPanelEnabled) {
		this.showLeftPanelEnabled = showLeftPanelEnabled;
	}

	public boolean isShowLeftBarShrinked() {
		return showLeftBarShrinked;
	}

	public void setShowLeftBarShrinked(boolean showLeftBarShrinked) {
		this.showLeftBarShrinked = showLeftBarShrinked;
	}
	
}
