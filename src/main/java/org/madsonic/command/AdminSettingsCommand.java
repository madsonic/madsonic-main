/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2012 (C) Martin Karel
 *  
 */
package org.madsonic.command;

import org.madsonic.controller.AdminSettingsController;
import org.madsonic.domain.Theme;

/**
 * Command used in {@link AdminSettingsController}.
 *
 * @author Martin Karel
 */
public class AdminSettingsCommand {

    private String logfileLevel;
    private String localeIndex;    
    private String[] locales;
    private Theme[] themes;
    private String themeIndex;
	private String listType = "random";
	private String newAdded;
	
	private boolean snowEnabled;
	private boolean debugOutput;
	private boolean showCreateLink;
	private boolean showMediaType;
    private boolean isReloadNeeded;
    private boolean isFullReloadNeeded;
    private boolean toast;
    
    private int indexListSize;
    private int leftframeSize;
    private int playQueueSize;
    private boolean gettingStartedEnabled;
	
	private String pageTitle;
    private String welcomeTitle;
    private String welcomeSubtitle;
    
    private String scanMode;
    private boolean coverArtHQ;
    private boolean customLogo;
    private String playlistExportMode;
    private boolean appRedirect;
    
	public String getLogfileLevel() {
		return logfileLevel;
	}

	public void setLogfileLevel(String logfileLevel) {
		this.logfileLevel = logfileLevel;
	}

	public String[] getLocales() {
		return locales;
	}

	public void setLocales(String[] locales) {
		this.locales = locales;
	}

	public String getThemeIndex() {
		return themeIndex;
	}

	public void setThemeIndex(String themeIndex) {
		this.themeIndex = themeIndex;
	}

	public Theme[] getThemes() {
		return themes;
	}

	public void setThemes(Theme[] themes) {
		this.themes = themes;
	}

	public String getListType() {
		return listType;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public String getNewAdded() {
		return newAdded;
	}

	public void setNewAdded(String newAdded) {
		this.newAdded = newAdded;
	}

	public int getLeftframeSize() {
		return leftframeSize;
	}

	public void setLeftframeSize(int leftframeSize) {
		this.leftframeSize = leftframeSize;
	}

	public int getPlayQueueSize() {
		return playQueueSize;
	}

	public void setPlayQueueSize(int playQueueSize) {
		this.playQueueSize = playQueueSize;
	}

	public boolean isGettingStartedEnabled() {
		return gettingStartedEnabled;
	}

	public void setGettingStartedEnabled(boolean gettingStartedEnabled) {
		this.gettingStartedEnabled = gettingStartedEnabled;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getWelcomeTitle() {
		return welcomeTitle;
	}

	public void setWelcomeTitle(String welcomeTitle) {
		this.welcomeTitle = welcomeTitle;
	}

	public String getWelcomeSubtitle() {
		return welcomeSubtitle;
	}

	public void setWelcomeSubtitle(String welcomeSubtitle) {
		this.welcomeSubtitle = welcomeSubtitle;
	}

	public String getLocaleIndex() {
		return localeIndex;
	}

	public void setLocaleIndex(String localeIndex) {
		this.localeIndex = localeIndex;
	}

	public boolean isReloadNeeded() {
		return isReloadNeeded;
	}

	public void setReloadNeeded(boolean isReloadNeeded) {
		this.isReloadNeeded = isReloadNeeded;
	}

	public boolean isFullReloadNeeded() {
		return isFullReloadNeeded;
	}

	public void setFullReloadNeeded(boolean isFullReloadNeeded) {
		this.isFullReloadNeeded = isFullReloadNeeded;
	}

	public boolean isToast() {
		return toast;
	}

	public void setToast(boolean toast) {
		this.toast = toast;
	}

	public boolean isSnowEnabled() {
		return snowEnabled;
	}

	public void setSnowEnabled(boolean snowEnabled) {
		this.snowEnabled = snowEnabled;
	}

	public boolean isDebugOutput() {
		return debugOutput;
	}

	public void setDebugOutput(boolean debugOutput) {
		this.debugOutput = debugOutput;
	}

	public int getIndexListSize() {
		return indexListSize;
	}

	public void setIndexListSize(int indexListSize) {
		this.indexListSize = indexListSize;
	}

	public boolean isShowCreateLink() {
		return showCreateLink;
	}

	public void setShowCreateLink(boolean showCreateLink) {
		this.showCreateLink = showCreateLink;
	}

	public boolean isShowMediaType() {
		return showMediaType;
	}

	public void setShowMediaType(boolean showMediaType) {
		this.showMediaType = showMediaType;
	}

	public String getScanMode() {
		return scanMode;
	}

	public void setScanMode(String scanMode) {
		this.scanMode = scanMode;
	}

	public boolean isCoverArtHQ() {
		return coverArtHQ;
	}

	public void setCoverArtHQ(boolean coverArtHQ) {
		this.coverArtHQ = coverArtHQ;
	}

	public boolean isCustomLogo() {
		return customLogo;
	}

	public void setCustomLogo(boolean customLogo) {
		this.customLogo = customLogo;
	}

	public String getPlaylistExportMode() {
		return playlistExportMode;
	}

	public void setPlaylistExportMode(String playlistExportMode) {
		this.playlistExportMode = playlistExportMode;
	}

	public boolean isAppRedirect() {
		return appRedirect;
	}

	public void setAppRedirect(boolean appRedirect) {
		this.appRedirect = appRedirect;
	}

    
}