/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.command;

import java.util.List;

import org.madsonic.command.MusicFolderSettingsCommand.MusicFolderInfo;
import org.madsonic.controller.MusicFolderTasksSettingsController;

/**
 * Command used in {@link MusicFolderTasksSettingsController}.
 *
 * @author Martin Karel
 */
public class MusicFolderTasksSettingsCommand {
 
    private List<MusicFolderInfo> musicFolders;
    private MusicFolderInfo newMusicFolder;
    
    private boolean reload;

    public List<MusicFolderInfo> getMusicFolders() {
        return musicFolders;
    }

    public void setMusicFolders(List<MusicFolderInfo> musicFolders) {
        this.musicFolders = musicFolders;
    }

    public MusicFolderInfo getNewMusicFolder() {
        return newMusicFolder;
    }
    public void setReload(boolean reload) {
        this.reload = reload;
    }

    public boolean isReload() {
        return reload;
    }

}