/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.domain;

/**
 * @author Martin Karel
 */
public enum TrackListType {

    RANDOM      ("random", "Random"),
    GENRE       ("genre", "By Genre"),
    DECADE      ("decade", "By Decade"),
    NEWEST      ("newest", "Recently Added"),
    RECENT      ("recent", "Recently Played"),
    FREQUENT    ("frequent", "Most Played");

    private final String id;
    private final String description;

    TrackListType(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public static TrackListType fromId(String id) {
        for (TrackListType trackListType : values()) {
            if (trackListType.id.equals(id)) {
                return trackListType;
            }
        }
        return null;
    }
}
