/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.domain;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

/**
 * Represents a top level directory in which music or other media is stored.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MusicFolder implements Serializable {

    private Integer id;
    private File path;
    private String name;
    private boolean enabled;
    private Date changed;
    private Integer index;
    private Integer type;
    private Integer group;
    private Date timer;
    private Integer interval;  
    private String status;
    private String result;
    private String comment;
	private Integer found;
	private Date lastrun;
    
    /**
     * Creates a new music folder.
     *
     * @param id      The system-generated ID.
     * @param path    The path of the music folder.
     * @param name    The user-defined name.
     * @param enabled Whether the folder is enabled.
     * @param changed When the corresponding database entry was last changed.
     * @param index   Which index to use.
     * @param type    Which type to use.
     * @param group   Which group to use.
     * @param status   Which status to use.
     * @param result   Which result to use.
     * @param comment   Which comment to use.
     */
    public MusicFolder(Integer id, 
    				File path, 
    				String name, 
    				boolean enabled, 
    				Date changed, 
    				Integer index, 
    				Integer type, 
    				Integer group, 
    				Date timer, 
    				Integer interval, 
    				String status, 
    				String result, 
    				String comment,
    				Integer found,
    				Date lastrun) {
        this.id = id;
        this.path = path;
        this.name = name;
        this.enabled = enabled;
        this.changed = changed;
        this.index = index;
        this.type = type;
        this.group = group;
        this.timer = timer;
        this.interval = interval;
        this.status = status;
        this.result = result;
        this.comment = comment;
        this.found = found;
        this.lastrun = lastrun;
    }

    public MusicFolder(File path, String name, boolean enabled, Date changed, Integer index, Integer type, Integer group, Date timer, Integer interval, String status, String result, String comment, Integer found, Date lastrun) {
        this(null, path, name, enabled, changed, index, type, group, timer, interval, status, result, comment, found, lastrun);
    }
    
    public MusicFolder(File path, String name, boolean enabled, Date changed, Integer index, Integer type, Integer group, Date timer, Integer interval) {
        this(null, path, name, enabled, changed, index, type, group, timer, interval, null, null, null, null, null);
    }

    
    /**
     * Returns the system-generated ID.
     *
     * @return The system-generated ID.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Returns the path of the music folder.
     *
     * @return The path of the music folder.
     */
    public File getPath() {
        return path;
    }

    /**
     * Sets the path of the music folder.
     *
     * @param path The path of the music folder.
     */
    public void setPath(File path) {
        this.path = path;
    }

    /**
     * Returns the user-defined name.
     *
     * @return The user-defined name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the user-defined name.
     *
     * @param name The user-defined name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns whether the folder is enabled.
     *
     * @return Whether the folder is enabled.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets whether the folder is enabled.
     *
     * @param enabled Whether the folder is enabled.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Returns when the corresponding database entry was last changed.
     *
     * @return When the corresponding database entry was last changed.
     */
    public Date getChanged() {
        return changed;
    }

    /**
     * Sets when the corresponding database entry was last changed.
     *
     * @param changed When the corresponding database entry was last changed.
     */
    public void setChanged(Date changed) {
        this.changed = changed;
    }

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return the group
	 */
	public Integer getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(Integer group) {
		this.group = group;
	}
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return Objects.equal(id, ((MusicFolder) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


    public static List<Integer> toIdList(List<MusicFolder> from) {
        return Lists.transform(from, toId());
    }

    public static List<String> toPathList(List<MusicFolder> from) {
        return Lists.transform(from, toPath());
    }

    public static Function<MusicFolder, Integer> toId() {
        return new Function<MusicFolder, Integer>() {
            @Override
            public Integer apply(MusicFolder from) {
                return from.getId();
            }
        };
    }

    public static Function<MusicFolder, String> toPath() {
        return new Function<MusicFolder, String>() {
            @Override
            public String apply(MusicFolder from) {
                return from.getPath().getPath();
            }
        };
    }

	public Date getTimer() {
		return timer;
	}

	public void setTimer(Date timer) {
		this.timer = timer;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the lastrun
	 */
	public Date getLastrun() {
		return lastrun;
	}

	/**
	 * @param lastrun the lastrun to set
	 */
	public void setLastrun(Date lastrun) {
		this.lastrun = lastrun;
	}

	/**
	 * @return the found
	 */
	public Integer getFound() {
		return found;
	}

	/**
	 * @param found the found to set
	 */
	public void setFound(Integer found) {
		this.found = found;
	}
}