/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.domain;

/**
 * @author Martin Karel
 */
public class URLParamEncoder {

	/**
	 * @param input
	 * @return encoded URL
	 */
    public static String encode(String input) {
        StringBuilder resultStr = new StringBuilder();
        for (char ch : input.toCharArray()) {
            if (isUnsafe(ch)) {
                resultStr.append('%');
                resultStr.append(toHex(ch / 16));
                resultStr.append(toHex(ch % 16));
            } else {
                resultStr.append(ch);
            }
        }
        String result = resultStr.toString();
        result = result.replace('\\', '/');
        result = result.replace("%F6", "%C3%B6"); // �
        result = result.replace("%E4", "%C3%A4"); // �
        result = result.replace("%FC", "%C3%BC"); // �
        result = result.replace("%D6", "%C3%96"); // �
        result = result.replace("%C4", "%C3%84"); // �
        result = result.replace("%DC", "%C3%9C"); // �
        result = result.replace("%F8", "%C3%B8"); // �
        
		//TODO: add more rules
        return result;
    }

    private static char toHex(int ch) {
        return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
    }

    private static boolean isUnsafe(char ch) {
        if (ch > 128 || ch < 0)
            return true;
        return " %$&+,;=?@<>()[]#".indexOf(ch) >= 0;
    }

}
