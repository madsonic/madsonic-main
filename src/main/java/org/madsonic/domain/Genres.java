/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a list of genres.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class Genres {

    private final Map<String, Genre> genres = new HashMap<String, Genre>();

    public void incrementAlbumCount(String genreName) {
        Genre genre = getOrCreateGenre(genreName);
        genre.incrementAlbumCount();
    }

    public void incrementArtistCount(String genreName) {
        Genre genre = getOrCreateGenre(genreName);
        genre.incrementArtistCount();
    }    
    
    public void incrementSongCount(String genreName) {
        Genre genre = getOrCreateGenre(genreName);
        genre.incrementSongCount();
    }

    private Genre getOrCreateGenre(String genreName) {
        Genre genre = genres.get(genreName);
        if (genre == null) {
            genre = new Genre(genreName);
            genres.put(genreName, genre);
        }
        return genre;
    }

    public List<Genre> getGenres() {
        return new ArrayList<Genre>(genres.values());
    }
}