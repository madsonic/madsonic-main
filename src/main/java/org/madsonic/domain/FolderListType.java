/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2017 (C) Martin Karel
 *  
 */
package org.madsonic.domain;

	public enum FolderListType {
	    ALL         (-1),
	    MUSIC       (-2),
	    VIDEOS      (-3),
	    MOVIES      (-4),
	    SERIES      (-5),
	    IMAGES      (-6);

	    private final int id;

	    FolderListType(int id) {
	        this.id = id;
	    }

	    public int getId() {
	        return id;
	    }
	}