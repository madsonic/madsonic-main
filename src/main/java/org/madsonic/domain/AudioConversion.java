/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.domain;

import java.util.Date;

/**
 * @author Sindre Mehus
 * @version $Id$
 */
public class AudioConversion {

    private Integer id;
    private int mediaFileId;
    private String username;
    private Status status;
    private String targetFile;
    private String targetFormat;
    private String logFile;
    private Integer bitRate;
    private final Integer progressSeconds;
    private Date created;
    private Date changed;
    private Date started;

    public AudioConversion(Integer id, int mediaFileId, String username, Status status,
                           String targetFile, String targetFormat, String logFile, Integer bitRate, Integer progressSeconds,
                           Date created, Date changed, Date started) {
        this.id = id;
        this.mediaFileId = mediaFileId;
        this.username = username;
        this.status = status;
        this.targetFile = targetFile;
        this.targetFormat = targetFormat;        
        this.logFile = logFile;
        this.bitRate = bitRate;
        this.progressSeconds = progressSeconds;
        this.created = created;
        this.changed = changed;
        this.started = started;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getMediaFileId() {
        return mediaFileId;
    }

    public void setMediaFileId(int mediaFileId) {
        this.mediaFileId = mediaFileId;
    }
    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Status getStatus() {
        return status;
    }

    public String getTargetFile() {
        return targetFile;
    }

	public String getTargetFormat() {
		return targetFormat;
	}

	public void setTargetFormat(String targetFormat) {
		this.targetFormat = targetFormat;
	}    
    
    public String getLogFile() {
        return logFile;
    }

    public Integer getBitRate() {
        return bitRate;
    }
		
    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getProgressSeconds() {
        return progressSeconds;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

	public enum Status {
        NEW,
        IN_PROGRESS,
        ERROR,
        COMPLETED
    }
}
