package org.madsonic.domain;

	public class StatusEntry {

	    private long valueLong;
	    private int valueInt;
	    
	    private String unit;
	    private String entry;
	    

		public String getUnit() {
			return unit;
		}
		public void setUnit(String unit) {
			this.unit = unit;
		}
		public String getEntry() {
			return entry;
		}
		public void setEntry(String entry) {
			this.entry = entry;
		}
		public int getValueInt() {
			return valueInt;
		}
		public void setValueInt(int valueInt) {
			this.valueInt = valueInt;
		}
		public long getValueLong() {
			return valueLong;
		}
		public void setValueLong(long valueLong) {
			this.valueLong = valueLong;
		}
	
}
