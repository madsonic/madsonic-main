/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.domain;

/**
 * Represents the version number of Madsonic.
 *
 */
public class ServerVersion implements Comparable<ServerVersion> {
	
    private int major;
    private int minor;
    private int build;
    private String revison;
    
    /**
     * Creates a new version instance by parsing the given string.
     * @param version A string of the format "6.0.6910.d6bde3c".
     */
    public ServerVersion(String version) {
    	
        String[] s = version.split("\\.");
        major = Integer.valueOf(s[0]);
        minor = Integer.valueOf(s[1]);
        
        if (s[2] != null ) {
            build = Integer.valueOf(s[2]); 
        }
        
        if (s.length > 3) {
            if (s[3] != null) {
            	revison = s[3];
            }
        }
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

	public int getBuild() {
		return build;
	}

	public String getRevision() {
		return revison;
	}
	
	/**
     * Return whether this object is equal to another.
     * @param o Object to compare to.
     * @return Whether this object is equals to another.
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final ServerVersion version = (ServerVersion) o;
        if (minor != version.minor) return false;
        if (major != version.major) return false;
        if (build != version.build) return false;
        return minor == version.minor;
    }

    /**
     * Returns a hash code for this object.
     * @return A hash code for this object.
     */
    public int hashCode() {
        int result;
        result = major;
        result = 29 * result + minor;
        result = 29 * result + build;        
        return result;
    }

    /**
     * Returns a string representation of the form "6.0.6910" or "6.0.6910.d6bde3c".
     * @return A string representation of the form "6.0.6910" or "6.0.6910.d6bde3c".
     */
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append(major).append('.').append(minor).append('.').append(build);
        if (revison != null && !revison.contains("git") ) {
        	buf.append('.').append(revison);
      	}
        return buf.toString();
    }

    /**
     * Compares this object with the specified object for order.
     * @param version The object to compare to.
     * @return A negative integer, zero, or a positive integer as this object is less than, equal to, or
     * greater than the specified object.
     */
    public int compareTo(ServerVersion version) {
        if (major < version.major) {
            return -1;
        } else if (major > version.major) {
            return 1;
        }

        if (minor < version.minor) {
            return -1;
        } else if (minor > version.minor) {
            return 1;
        }

        if (build < version.build) {
            return -1;
        } else if (build > version.build) {
            return 1;
        }
        
        return 0;
    }
}
