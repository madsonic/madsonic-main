/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.domain;

/**
 * Enumeration of cover art schemes. Each value contains a size, which indicates how big the
 * scaled covert art images should be.
 *
 * @author Sindre Mehus, Martin Karel
 * @version $Revision: 1.3 $ $Date: 2005/06/15 18:10:40 $
 */
public enum CoverArtScheme {

    OFF     ( 0, 0),
    MINI   ( 80, 0),
    SMALL  (100, 12),
    MEDIUM (118, 16),
    LARGE  (160, 20),
    XLARGE (200, 30),
    POSTER (256, 40),
    THUMBS (140, 0);
    
    private int size;
    private final int captionLength;

    CoverArtScheme(int size, int captionLength) {
    	
        this.size = size;
        this.captionLength = captionLength;
    }

    /**
     * Returns the covert art size for this scheme.
     *
     * @return the covert art size for this scheme.
     */
    public int getSize() {
        return size;
    }

    public int getCaptionLength() {
        return captionLength;
    }
}
