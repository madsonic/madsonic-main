/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2017 (C) Martin Karel
 *  
 */
package org.madsonic.domain;

import java.util.List;

import org.madsonic.util.Pair;

/**
 * result for playlist parsing.
 * 
 * @author Martin Karel
 *
 */
public class PlaylistResult {

	private Pair<List<MediaFile>, List<String>> mediaFiles;
	
	private String username;
	private boolean isPublic;
	
	public Pair<List<MediaFile>, List<String>> getMediaFiles() {
		return mediaFiles;
	}
	public void setMediaFiles(Pair<List<MediaFile>, List<String>> mediaFiles) {
		this.mediaFiles = mediaFiles;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

}
