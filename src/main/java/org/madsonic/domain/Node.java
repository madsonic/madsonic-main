/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.domain;

public class Node {

	    private Integer id;
	    private String url;
	    private String name;
	    private boolean isOnline;
	    private boolean isEnabled;
	    
	    public Node (Integer id, String url, String name, boolean isOnline, boolean isEnabled) {
			this.id = id;
			this.url = url;
			this.name = name;
			this.isOnline = isOnline;
			this.isEnabled = isEnabled;
	    }

		/**
		 * @return the id
		 */
		public Integer getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(Integer id) {
			this.id = id;
		}

		/**
		 * @return the url
		 */
		public String getUrl() {
			return url;
		}

		/**
		 * @param url the url to set
		 */
		public void setUrl(String url) {
			this.url = url;
		}

		/**
		 * @return the isEnabled
		 */
		public boolean isEnabled() {
			return isEnabled;
		}

		/**
		 * @param isEnabled the isEnabled to set
		 */
		public void setEnabled(boolean isEnabled) {
			this.isEnabled = isEnabled;
		}

		/**
		 * @return the isOnline
		 */
		public boolean isOnline() {
			return isOnline;
		}

		/**
		 * @param isOnline the isOnline to set
		 */
		public void setOnline(boolean isOnline) {
			this.isOnline = isOnline;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
}
