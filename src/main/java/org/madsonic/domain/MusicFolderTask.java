/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.domain;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class MusicFolderTask implements Serializable {

	private Integer id;
	private Integer music_folder_id;
	private Integer found;
    private Date scan;
    private String status;
    private String result;
    private String comment;
    private boolean enabled;
    
    
    public MusicFolderTask (Integer id, Integer music_folder_id, Integer found, Date scan, String status, String result, String comment, boolean enabled) {
    	this.id = id;
    	this.music_folder_id = music_folder_id;
    	this.found = found;
    	this.scan = scan;
    	this.status = status;
    	this.result = result;
    	this.comment = comment;
    	this.enabled = enabled;
    }
    
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMusic_folder_id() {
		return music_folder_id;
	}

	public void setMusic_folder_id(Integer music_folder_id) {
		this.music_folder_id = music_folder_id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getFound() {
		return found;
	}

	public void setFound(Integer found) {
		this.found = found;
	}

	public Date getScan() {
		return scan;
	}

	public void setScan(Date scan) {
		this.scan = scan;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
}
