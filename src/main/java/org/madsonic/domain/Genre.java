/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.domain;

/**
 * Represents a musical genre.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class Genre {

    private final String name;
    private int songCount;
    private int albumCount;
    private int artistCount;

    public Genre(String name) {
        this.name = name;
    }

    public Genre(String name, int songCount, int albumCount, int artistCount) {
        this.name = name;
        this.songCount = songCount;
        this.albumCount = albumCount;
        this.artistCount = artistCount;
    }

    public String getName() {
        return name;
    }

    public int getSongCount() {
        return songCount;
    }

    public int getAlbumCount() {
        return albumCount;
    }

    public int getArtistCount() {
        return artistCount;
    }

    public void incrementAlbumCount() {
        albumCount++;
    }

    public void incrementSongCount() {
        songCount++;
    }
    
    public void incrementArtistCount() {
    	artistCount++;
}    
}