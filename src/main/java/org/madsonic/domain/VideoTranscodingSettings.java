/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.domain;

/**
 * Parameters used when transcoding videos.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class VideoTranscodingSettings {

    private final int width;
    private final int height;
    private final int timeOffset;
    private final int duration;
    
    private final boolean hls;
    private final boolean dash;
    private final Integer audioTrack;    

    public VideoTranscodingSettings(int width, int height, int timeOffset, int duration, boolean hls, boolean dash, Integer audioTrack) {
        this.width = width;
        this.height = height;
        this.timeOffset = timeOffset;
        this.duration = duration;
        
        this.hls = hls;
        this.dash = dash;
        this.audioTrack = audioTrack;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getTimeOffset() {
        return timeOffset;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isHls() {
        return hls;
    }

	public boolean isDash() {
		return dash;
	}

	public Integer getAudioTrack() {
		return audioTrack;
	}
    
    
}
