/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.core.support.SimpleDirContextAuthenticationStrategy;
import org.springframework.security.ldap.LdapUtils;
import org.springframework.util.Assert;

public class MadsonicContextSource extends LdapContextSource {
	
    protected final Log logger = LogFactory.getLog(getClass());

    private String rootDn;

    public MadsonicContextSource(String providerUrl) {
        Assert.hasLength(providerUrl, "An LDAP connection URL must be supplied.");

        StringTokenizer st = new StringTokenizer(providerUrl);
        ArrayList<String> urls = new ArrayList<String>();

        // Work out rootDn from the first URL and check that the other URLs (if any) match
        while (st.hasMoreTokens()) {
            String url = st.nextToken();
            String urlRootDn = LdapUtils.parseRootDnFromUrl(url);
            urls.add(url.substring(0, url.lastIndexOf(urlRootDn)));
            logger.info(" URL '" + url + "', root DN is '" + urlRootDn + "'");
            if (rootDn == null) {
                rootDn = urlRootDn;
            } else if (!rootDn.equals(urlRootDn)) {
                throw new IllegalArgumentException("Root DNs must be the same when using multiple URLs");
            }
        }

        setUrls(urls.toArray(new String[urls.size()]));
        setBase(rootDn);
        setPooled(true);
        setAuthenticationStrategy(new SimpleDirContextAuthenticationStrategy() {
            @Override
            @SuppressWarnings({ "rawtypes", "deprecation", "unchecked" })
            public void setupEnvironment(Hashtable env, String dn, String password) {
                super.setupEnvironment(env, dn, password);
                // Remove the pooling flag unless we are authenticating as the 'manager' user.
                if (!userDn.equals(dn) && env.containsKey(SUN_LDAP_POOLING_FLAG)) {
                    logger.debug("Removing pooling flag for user " + dn);
                    env.remove(SUN_LDAP_POOLING_FLAG);
                }
            }
        });
    }

    public MadsonicContextSource(List<String> urls, String baseDn) {
        this(buildProviderUrl(urls, baseDn));
    }

    private static String buildProviderUrl(List<String> urls, String baseDn) {
        Assert.notNull(baseDn, "The Base DN for the LDAP server must not be null.");
        Assert.notEmpty(urls, "At least one LDAP server URL must be provided.");

        String trimmedBaseDn = baseDn.trim();
        StringBuilder providerUrl = new StringBuilder();

        for (String serverUrl : urls) {
            String trimmedUrl = serverUrl.trim();
            if ("".equals(trimmedUrl)) {
                continue;
            }

            providerUrl.append(trimmedUrl);
            if (! trimmedUrl.endsWith("/")) {
                providerUrl.append("/");
            }
            providerUrl.append(trimmedBaseDn);
            providerUrl.append(" ");
        }

        return providerUrl.toString();
    }
}
