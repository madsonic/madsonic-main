/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.directory.SearchControls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.SpringSecurityLdapTemplate;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.util.Assert;

public class MadsonicLdapAuthoritiesPopulator implements LdapAuthoritiesPopulator {
    
    // ~ Static fields/initializers
    // =====================================================================================

    private static final Log logger = LogFactory.getLog(MadsonicLdapAuthoritiesPopulator.class);

    // ~ Instance fields
    // ================================================================================================

    /**
     * A default role which will be assigned to all authenticated users if set
     */
    private GrantedAuthority defaultRole;

    /**
     * Template that will be used for searching
     */
    private final SpringSecurityLdapTemplate ldapTemplate;

    /**
     * Controls used to determine whether group searches should be performed over the full
     * sub-tree from the base DN. Modified by searchSubTree property
     */
    private final SearchControls searchControls = new SearchControls();

    /**
     * The ID of the attribute which contains the role name for a group
     */
    private String groupRoleAttribute = "cn";

    /**
     * The base DN from which the search for group membership should be performed
     */
    private String groupSearchBase;

    /**
     * The pattern to be used for the user search. {0} is the user's DN
     */
    private String groupSearchFilter = "(member={0})";
    /**
     * The role prefix that will be prepended to each role name
     */
    private String rolePrefix = "ROLE_";
    /**
     * Should we convert the role name to uppercase
     */
    private boolean convertToUpperCase = true;

    // ~ Constructors
    // ===================================================================================================

    /**
     * Constructor for group search scenarios. <tt>userRoleAttributes</tt> may still be
     * set as a property.
     *
     * @param contextSource supplies the contexts used to search for user roles.
     * @param groupSearchBase if this is an empty string the search will be performed from
     * the root DN of the context factory. If null, no search will be performed.
     */
    public MadsonicLdapAuthoritiesPopulator(ContextSource contextSource, String groupSearchBase) {
        Assert.notNull(contextSource, "contextSource must not be null");
        ldapTemplate = new SpringSecurityLdapTemplate(contextSource);
        getLdapTemplate().setSearchControls(searchControls);
        this.groupSearchBase = groupSearchBase; // should  ou=users

        if (groupSearchBase == null) {
            logger.info("groupSearchBase is null. No group search will be performed.");
        }
        else if (groupSearchBase.length() == 0) {
            logger.info("groupSearchBase is empty. Searches will be performed from the context source base");
        }
    }

    // ~ Methods
    // ========================================================================================================

    /**
     * This method should be overridden if required to obtain any additional roles for the
     * given user (on top of those obtained from the standard search implemented by this
     * class).
     *
     * @param user the context representing the user who's roles are required
     * @return the extra roles which will be merged with those returned by the group
     * search
     */

    protected Set<GrantedAuthority> getAdditionalRoles(DirContextOperations user,
            String username) {
        return null;
    }

    /**
     * Obtains the authorities for the user who's directory entry is represented by the
     * supplied LdapUserDetails object.
     *
     * @param user the user who's authorities are required
     * @return the set of roles granted to the user.
     */
    public final Collection<GrantedAuthority> getGrantedAuthorities(
            DirContextOperations user, String username) {
        String userDn = user.getNameInNamespace();

        if (logger.isDebugEnabled()) {
            logger.debug("Getting authorities for user " + userDn);
        }

        Set<GrantedAuthority> roles = getGroupMembershipRoles(userDn, username);
        Set<GrantedAuthority> extraRoles = getAdditionalRoles(user, username);

        if (extraRoles != null) {
            roles.addAll(extraRoles);
        }

        if (defaultRole != null) {
            roles.add(defaultRole);
        }

        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(roles.size());
        result.addAll(roles);

        return result;
    }

    public Set<GrantedAuthority> getGroupMembershipRoles(String userDn, String username) {
        if (getGroupSearchBase() == null) {
            return new HashSet<GrantedAuthority>();
        }

        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

        if (logger.isDebugEnabled()) {
            logger.debug("Searching for roles for user '" + username + "', DN = " + "'"
                    + userDn + "', with filter " + groupSearchFilter
                    + " in search base '" + getGroupSearchBase() + "'");
        }

        Set<String> userRoles = getLdapTemplate().searchForSingleAttributeValues(
                getGroupSearchBase(), groupSearchFilter,
                new String[] { userDn, username }, groupRoleAttribute);

        if (logger.isDebugEnabled()) {
            logger.debug("Roles from search: " + userRoles);
        }

        for (String role : userRoles) {

            if (convertToUpperCase) {
                role = role.toUpperCase();
            }

            authorities.add(new SimpleGrantedAuthority(rolePrefix + role));
        }

        return authorities;
    }

    protected ContextSource getContextSource() {
        return getLdapTemplate().getContextSource();
    }

    protected String getGroupSearchBase() {
        return groupSearchBase;
    }

    /**
     * Convert the role to uppercase
     */
    public void setConvertToUpperCase(boolean convertToUpperCase) {
        this.convertToUpperCase = convertToUpperCase;
    }

    /**
     * The default role which will be assigned to all users.
     *
     * @param defaultRole the role name, including any desired prefix.
     */
    public void setDefaultRole(String defaultRole) {
        Assert.notNull(defaultRole, "The defaultRole property cannot be set to null");
        this.defaultRole = new SimpleGrantedAuthority(defaultRole);
    }

    public void setGroupRoleAttribute(String groupRoleAttribute) {
        Assert.notNull(groupRoleAttribute, "groupRoleAttribute must not be null");
        this.groupRoleAttribute = groupRoleAttribute;
    }

    public void setGroupSearchFilter(String groupSearchFilter) {
        Assert.notNull(groupSearchFilter, "groupSearchFilter must not be null");
        this.groupSearchFilter = groupSearchFilter;
    }

    /**
     * Sets the prefix which will be prepended to the values loaded from the directory.
     * Defaults to "ROLE_" for compatibility with <tt>RoleVoter/tt>.
     */
    public void setRolePrefix(String rolePrefix) {
        Assert.notNull(rolePrefix, "rolePrefix must not be null");
        this.rolePrefix = rolePrefix;
    }

    /**
     * If set to true, a subtree scope search will be performed. If false a single-level
     * search is used.
     *
     * @param searchSubtree set to true to enable searching of the entire tree below the
     * <tt>groupSearchBase</tt>.
     */
    public void setSearchSubtree(boolean searchSubtree) {
        int searchScope = searchSubtree ? SearchControls.SUBTREE_SCOPE
                : SearchControls.ONELEVEL_SCOPE;
        searchControls.setSearchScope(searchScope);
    }

    /**
     * Sets the corresponding property on the underlying template, avoiding specific
     * issues with Active Directory.
     *
     * @see LdapTemplate#setIgnoreNameNotFoundException(boolean)
     */
    public void setIgnorePartialResultException(boolean ignore) {
        getLdapTemplate().setIgnorePartialResultException(ignore);
    }

    /**
     * Returns the current LDAP template. Method available so that classes extending this
     * can override the template used
     * @return the LDAP template
     */
    protected SpringSecurityLdapTemplate getLdapTemplate() {
        return ldapTemplate;
    }

    /**
     * Returns the attribute name of the LDAP attribute that will be mapped to the role
     * name Method available so that classes extending this can override
     * @return the attribute name used for role mapping
     */
    protected final String getGroupRoleAttribute() {
        return groupRoleAttribute;
    }

    /**
     * Returns the search filter configured for this populator Method available so that
     * classes extending this can override
     * @return the search filter
     */
    protected final String getGroupSearchFilter() {
        return groupSearchFilter;
    }

    /**
     * Returns the role prefix used by this populator Method available so that classes
     * extending this can override
     * @return the role prefix
     */
    protected final String getRolePrefix() {
        return rolePrefix;
    }

    /**
     * Returns true if role names are converted to uppercase Method available so that
     * classes extending this can override
     * @return true if role names are converted to uppercase.
     */
    protected final boolean isConvertToUpperCase() {
        return convertToUpperCase;
    }

    /**
     * Returns the default role Method available so that classes extending this can
     * override
     * @return the default role used
     */
    @SuppressWarnings("unused")
    private GrantedAuthority getDefaultRole() {
        return defaultRole;
    }

    /**
     * Returns the search controls Method available so that classes extending this can
     * override the search controls used
     * @return the search controls
     */
    @SuppressWarnings("unused")
    private SearchControls getSearchControls() {
        return searchControls;
    }
}
