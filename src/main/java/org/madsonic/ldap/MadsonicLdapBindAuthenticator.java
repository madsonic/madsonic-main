/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap;

import org.apache.commons.lang.StringUtils;

import org.madsonic.Logger;
import org.madsonic.domain.User;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.SpringSecurityMessageSource;

import org.springframework.security.ldap.authentication.LdapAuthenticator;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;

public class MadsonicLdapBindAuthenticator implements LdapAuthenticator {

	private static final Logger LOG = Logger.getLogger(MadsonicLdapBindAuthenticator.class);
	private SecurityService securityService;
	private SettingsService settingsService;
	private long authenticatorTimestamp;
	private MadsonicBindAuthenticator delegateAuthenticator;

	public DirContextOperations authenticate(Authentication authentication) {

		String username = authentication.getName();

		// LDAP authentication must be enabled on the system.
		if (!settingsService.isLdapEnabled()) {
			throw new BadCredentialsException("LDAP authentication disabled.");
		} 
		// User must be defined in Madsonic, unless auto-shadowing is enabled.
		User user = securityService.getUserByName(username);
		
		if (user == null && !settingsService.isLdapAutoShadowing()) {
			throw new BadCredentialsException("user does not exist.");
		}
		// LDAP authentication must be enabled for the given user.
		if (user != null && !user.isLdapAuthenticated()) {
			throw new BadCredentialsException("LDAP authentication disabled for user.");
		}
		try {
			createDelegate();
			
			LOG.debug("authentication request: " + username);
			DirContextOperations contextOperations = delegateAuthenticator.authenticate(authentication);
			
			if (contextOperations != null) {
				LOG.debug("user '" + username + "' successfully authenticated in LDAP. DN: " + contextOperations.getDn());

				if (user == null) {
					
					// set dm as comment
					User newUser = new User(username, "", null, true, 0L, 0L, 0L, 1, false, "#ldap user: " + contextOperations.getDn(), true);

					// set default group for user
					newUser.setGroupId(securityService.GetIdfromGroupName("LDAP"));
					
					// clone user
					securityService.cloneUser(newUser);
					LOG.info("cloned from default user '" + username + "' for DN " + contextOperations.getDn());

					// create shadow ldap account
					securityService.createLdapAccount(newUser);
					
					// generate token
					newUser.setPassword(authentication.getPrincipal().toString());
					securityService.setSecureUserToken(newUser);
					LOG.debug("set token for " + username);
				}
			}
			return contextOperations;
		} catch (RuntimeException x) {
			LOG.error("Failed to authenticate user '" + username + "' in LDAP.", x);
			throw x;
		}
	}

	private synchronized void createDelegate() {
		
		// Only create it if necessary.
		if (delegateAuthenticator == null || authenticatorTimestamp < settingsService.getSettingsChanged()) {
			
			MadsonicContextSource contextSource = new MadsonicContextSource(settingsService.getLdapUrl());
			String managerDn = settingsService.getLdapManagerDn();
			String managerPassword = settingsService.getLdapManagerPassword();
			if (StringUtils.isNotEmpty(managerDn) && StringUtils.isNotEmpty(managerPassword)) {
				contextSource.setUserDn(managerDn);
				contextSource.setPassword(managerPassword);
			}
			contextSource.setReferral("follow");
			contextSource.setAnonymousReadOnly(false);
			contextSource.afterPropertiesSet();
			
			FilterBasedLdapUserSearch userSearch = new FilterBasedLdapUserSearch("", settingsService.getLdapSearchFilter(), contextSource);
			userSearch.setSearchSubtree(true);
			userSearch.setDerefLinkFlag(true);

			delegateAuthenticator = new MadsonicBindAuthenticator(contextSource);
			delegateAuthenticator.setMessageSource(new SpringSecurityMessageSource());
			delegateAuthenticator.setUserSearch(userSearch);
			authenticatorTimestamp = settingsService.getSettingsChanged();
		}
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}
}