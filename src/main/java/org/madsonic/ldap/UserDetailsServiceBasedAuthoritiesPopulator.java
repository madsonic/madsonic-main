/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.ldap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import org.apache.commons.lang.StringUtils;
import org.madsonic.Logger;
import org.madsonic.domain.LdapGroupSyncType;
import org.madsonic.domain.User;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

/**
 * @author Martin Karel
 */
public class UserDetailsServiceBasedAuthoritiesPopulator implements LdapAuthoritiesPopulator {

    private SettingsService settingsService;
    private SecurityService securityService;
    private UserDetailsService userDetailsService;
    private MadsonicLdapAuthoritiesPopulator populator;
    
    private static final Logger LOG = Logger.getLogger(UserDetailsServiceBasedAuthoritiesPopulator.class);

    GrantedAuthority dbAdminRole = new SimpleGrantedAuthority("ROLE_ADMIN");
    GrantedAuthority dbSettingsRole = new SimpleGrantedAuthority("ROLE_SETTINGS");        
    GrantedAuthority dbStreamRole = new SimpleGrantedAuthority("ROLE_STREAM");        
    GrantedAuthority dbCommentRole = new SimpleGrantedAuthority("ROLE_COMMENT");
    GrantedAuthority dbCoverartRole = new SimpleGrantedAuthority("ROLE_COVERART");
    GrantedAuthority dbDownloadRole = new SimpleGrantedAuthority("ROLE_DOWNLOAD");
    GrantedAuthority dbJukeboxRole = new SimpleGrantedAuthority("ROLE_JUKEBOX");
    GrantedAuthority dbLastfmRole = new SimpleGrantedAuthority("ROLE_LASTFM");
    GrantedAuthority dbPodcastRole = new SimpleGrantedAuthority("ROLE_PODCAST");
    GrantedAuthority dbSearchRole = new SimpleGrantedAuthority("ROLE_SEARCH");
    GrantedAuthority dbShareRole = new SimpleGrantedAuthority("ROLE_SHARE");        
    GrantedAuthority dbUploadRole = new SimpleGrantedAuthority("ROLE_UPLOAD");        
    GrantedAuthority dbImageRole = new SimpleGrantedAuthority("ROLE_IMAGE");        
    GrantedAuthority dbVideoRole = new SimpleGrantedAuthority("ROLE_VIDEO");       
    GrantedAuthority dbAudioConversionRole = new SimpleGrantedAuthority("ROLE_AUDIO_CONVERSION");        
    GrantedAuthority dbVideoConversionRole = new SimpleGrantedAuthority("ROLE_VIDEO_CONVERSION"); 

    String ldapAdminRole = "ROLE_MADSONIC.ADMIN";
    String ldapSettingsRole = "ROLE_MADSONIC.CONFIG";
    String ldapSreamRole = "ROLE_MADSONIC.STREAM";        
    String ldapJukeboxRole = "ROLE_MADSONIC.JUKEBOX";        
    String ldapUploadRole = "ROLE_MADSONIC.UPLOAD";        
    String ldapSearchRole = "ROLE_MADSONIC.SEARCH";        
    String ldapCoverartRole = "ROLE_MADSONIC.COVER";        
    String ldapDownloadRole = "ROLE_MADSONIC.DOWNLOAD";        
    String ldapPodcastRole = "ROLE_MADSONIC.PODCAST";        
    String ldapCommentRole = "ROLE_MADSONIC.COMMENT";        
    String ldapLastfmRole = "ROLE_MADSONIC.LASTFM";        
    String ldapShareRole = "ROLE_MADSONIC.SHARE";        
    String ldapImageRole = "ROLE_MADSONIC.IMAGE";        
    String ldapVideoRole = "ROLE_MADSONIC.VIDEO";        
    String ldapAudioConversionRoleRole = "ROLE_MADSONIC.AUDIOCONVERSION";        
    String ldapVideoConversionRoleRole = "ROLE_MADSONIC.VIDEOCONVERSION"; 
    
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations userData, String username) {

		MadsonicContextSource contextSource = new MadsonicContextSource(settingsService.getLdapUrl());
		String managerDn = settingsService.getLdapManagerDn();
		String managerPassword = settingsService.getLdapManagerPassword();
		if (StringUtils.isNotEmpty(managerDn) && StringUtils.isNotEmpty(managerPassword)) {
			contextSource.setUserDn(managerDn);
			contextSource.setPassword(managerPassword);
		}
		contextSource.setReferral("follow");
		contextSource.setAnonymousReadOnly(false);
		contextSource.afterPropertiesSet();
	
        populator = new MadsonicLdapAuthoritiesPopulator(contextSource, settingsService.getLdapGroupSearchBase());
        populator.setIgnorePartialResultException(true);
        populator.setRolePrefix("ROLE_");
        populator.setGroupRoleAttribute(settingsService.getLdapGroupAttrib());
        populator.setGroupSearchFilter(settingsService.getLdapGroupFilter());
        populator.setSearchSubtree(true);
        populator.setSearchSubtree(false);
        populator.setConvertToUpperCase(true);
        
        DirContextAdapter ctx = null;
		try {
			ctx = new DirContextAdapter(new LdapName(userData.getDn().toString() + "," + contextSource.getBaseLdapPathAsString()));
		} catch (InvalidNameException ex) {
			LOG.warn("faild get context", ex);
		}
	    Set<String> ldapAuthorities = AuthorityUtils.authorityListToSet(populator.getGrantedAuthorities(ctx, username));
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        
        Collection<? extends GrantedAuthority> dbAuthorities = userDetails.getAuthorities();
        
	    LdapGroupSyncType ldapGroupSyncType = settingsService.getLdapGroupSyncType();
        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();        	
	    
	    if (settingsService.isLdapAutoMapping()) {
		    switch (ldapGroupSyncType) {
			    case MERGE:
		    		LOG.debug("DB: " + dbAuthorities.toString());
			    	LOG.debug("LDAP: " + ldapAuthorities.toString());
			    	result = checkAuth(ldapAuthorities, dbAuthorities, true);
			        LOG.debug("retrieved roles: " + result.toString());
			    	break;
				case REPLACE:
		    		LOG.debug("DB: " + dbAuthorities.toString());
			    	LOG.debug("LDAP: " + ldapAuthorities.toString());
		    		result = checkAuth(ldapAuthorities, new ArrayList<GrantedAuthority>(), false);
			        LOG.debug("retrieved roles: " + result.toString());					
					break;
			}        	
		    boolean sync = settingsService.isLdapGroupAutoSync();
		    User user = securityService.getUserByName(username);
		    
		    if (user != null && sync) {
		    	user.setAdminRole(result.contains(dbAdminRole) ? true : false);
		    	user.setSettingsRole(result.contains(dbSettingsRole) ? true : false);
		    	user.setStreamRole(result.contains(dbStreamRole) ? true : false);
		    	user.setCommentRole(result.contains(dbCommentRole) ? true : false);
		    	user.setCoverArtRole(result.contains(dbCoverartRole) ? true : false);
		    	user.setDownloadRole(result.contains(dbDownloadRole) ? true : false);
		    	user.setJukeboxRole(result.contains(dbJukeboxRole) ? true : false);
		    	user.setLastFMRole(result.contains(dbLastfmRole) ? true : false);
		    	user.setPodcastRole(result.contains(dbPodcastRole) ? true : false);
		    	user.setSearchRole(result.contains(dbSearchRole) ? true : false);
		    	user.setShareRole(result.contains(dbShareRole) ? true : false);
		    	user.setUploadRole(result.contains(dbUploadRole) ? true : false);
		    	user.setImageRole(result.contains(dbImageRole) ? true : false);
		    	user.setVideoRole(result.contains(dbVideoRole) ? true : false);
		    	user.setAudioConversionRole(result.contains(dbAudioConversionRole) ? true : false);
		    	user.setVideoConversionRole(result.contains(dbVideoConversionRole) ? true : false);
			    securityService.updateUser(user);
			    LOG.debug("Successfully synced roles to DB.");
		    }
			return result;
        }
        LOG.debug("retrieved roles from Madsonic DB: " + userDetails.getAuthorities().toString());
        return userDetails.getAuthorities();
	}
	
	private List<GrantedAuthority> checkAuth(Set<String> ldapAuthorities, Collection<? extends GrantedAuthority> dbAuthorities, boolean merge) {
		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();		
		
    	if (dbAuthorities.contains(dbAdminRole)) { result.add(dbAdminRole); }        
    	if (dbAuthorities.contains(dbSettingsRole)) { result.add(dbSettingsRole); }        
    	if (dbAuthorities.contains(dbStreamRole)) { result.add(dbStreamRole); }
    	if (dbAuthorities.contains(dbCommentRole)) { result.add(dbStreamRole); }
    	if (dbAuthorities.contains(dbCoverartRole)) { result.add(dbStreamRole); }
    	if (dbAuthorities.contains(dbDownloadRole)) { result.add(dbDownloadRole); }
    	if (dbAuthorities.contains(dbJukeboxRole)) { result.add(dbJukeboxRole); }
    	if (dbAuthorities.contains(dbLastfmRole)) { result.add(dbLastfmRole); }
    	if (dbAuthorities.contains(dbPodcastRole)) { result.add(dbPodcastRole); }
    	if (dbAuthorities.contains(dbSearchRole)) { result.add(dbSearchRole); }
    	if (dbAuthorities.contains(dbShareRole)) { result.add(dbShareRole); }
    	if (dbAuthorities.contains(dbUploadRole)) { result.add(dbUploadRole); }
    	if (dbAuthorities.contains(dbImageRole)) { result.add(dbImageRole); }
    	if (dbAuthorities.contains(dbVideoRole)) { result.add(dbVideoRole); }
    	if (dbAuthorities.contains(dbAudioConversionRole)) { result.add(dbAudioConversionRole); }
    	if (dbAuthorities.contains(dbVideoConversionRole)) { result.add(dbVideoConversionRole); }
    	
        if (ldapAuthorities.contains(ldapAdminRole)) { addAuthority(result, dbAdminRole); }
        if (ldapAuthorities.contains(ldapSettingsRole)) { addAuthority(result, dbSettingsRole); }
        if (ldapAuthorities.contains(ldapSreamRole)) { addAuthority(result, dbStreamRole); }
        if (ldapAuthorities.contains(ldapJukeboxRole)) { addAuthority(result, dbJukeboxRole); }
        if (ldapAuthorities.contains(ldapUploadRole)) { addAuthority(result, dbUploadRole); }
        if (ldapAuthorities.contains(ldapSearchRole)) { addAuthority(result, dbSearchRole); }
        if (ldapAuthorities.contains(ldapCoverartRole)) { addAuthority(result, dbCoverartRole); }
        if (ldapAuthorities.contains(ldapDownloadRole)) { addAuthority(result, dbDownloadRole); }
        if (ldapAuthorities.contains(ldapPodcastRole)) { addAuthority(result, dbPodcastRole); }
        if (ldapAuthorities.contains(ldapCommentRole)) { addAuthority(result, dbCommentRole); }
        if (ldapAuthorities.contains(ldapLastfmRole) ) { addAuthority(result, dbLastfmRole); }
        if (ldapAuthorities.contains(ldapShareRole)) { addAuthority(result, dbShareRole); }
        if (ldapAuthorities.contains(ldapImageRole)) { addAuthority(result, dbImageRole); }
        if (ldapAuthorities.contains(ldapVideoRole)) { addAuthority(result, dbVideoRole); }
        if (ldapAuthorities.contains(ldapAudioConversionRoleRole)) { addAuthority(result, dbAudioConversionRole); }
        if (ldapAuthorities.contains(ldapVideoConversionRoleRole)) { addAuthority(result, dbVideoConversionRole); }
        return result;
	}
	
	private  List<GrantedAuthority> addAuthority(List<GrantedAuthority> result, GrantedAuthority role) {
		if (!result.contains(role)) {
			result.add(role);
		}
		return result;
	}
	
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

	public void setSettingsService(SettingsService settingsService) {
		this.settingsService = settingsService;
	}
	
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}	
}