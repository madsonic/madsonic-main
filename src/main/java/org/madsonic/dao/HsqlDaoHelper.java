/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import java.io.File;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.madsonic.Logger;
import org.madsonic.dao.schema.Schema;
import org.madsonic.dao.schema.hsql.HSqlSchema01;
import org.madsonic.dao.schema.hsql.HSqlSchema02;
import org.madsonic.dao.schema.hsql.SchemaInfo;
import org.madsonic.dao.schema.hsql.SchemaLastFM;
import org.madsonic.service.SettingsService;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;

/**
 * DAO helper class which creates the data source, and updates the database schema.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class HsqlDaoHelper implements DaoHelper {

    private static final Logger LOG = Logger.getLogger(HsqlDaoHelper.class);
    
    private Schema[] schemas = {new HSqlSchema01(), new HSqlSchema02(), new SchemaLastFM(), new SchemaInfo() };

    private DataSource dataSource;
    private static boolean shutdownHookAdded;

    public HsqlDaoHelper() {
        dataSource = createDataSource();
        checkDatabase();
		try {
			LOG.info("Checking HSQLDB version: " + dataSource.getConnection().getMetaData().getDatabaseProductVersion() );
		} catch (SQLException e) {
		}
        addShutdownHook();
    }

    public String getVersion() {
		try {
			return dataSource.getConnection().getMetaData().getDatabaseProductVersion();
		} catch (Exception e) {
			return null;
		}
    }
    
    private void addShutdownHook() {
        if (shutdownHookAdded) {
            return;
        }
        shutdownHookAdded = true;
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
            	
            	LOG.warn("Shutting down HSQLDB database...");
                System.err.println("Shutting down HSQLDB database...");
                
                //shutdown hsqldb
                getJdbcTemplate().execute("shutdown");
                
            	LOG.info("Shutting down HSQLDB database - Done!");
                System.err.println("Shutting down HSQLDB database - Done!");
                
                //shutdown log4j2
                if( LogManager.getContext() instanceof LoggerContext ) {
                    LogManager.getLogger().info("Shutting down log4j2");
                    Configurator.shutdown((LoggerContext)LogManager.getContext());
                } else{
                    LogManager.getLogger().warn("Unable to shutdown log4j2");
                }
        }});
    }

    /**
     * Returns a JDBC template for performing database operations.
     *
     * @return A JDBC template.
     */
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    private DataSource createDataSource() {
        File madsonicHome = SettingsService.getMadsonicHome();
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.hsqldb.jdbcDriver");
        ds.setUrl("jdbc:hsqldb:file:" + madsonicHome.getPath() + "/db/madsonic;sql.enforce_size=false");
        ds.setUsername("sa");
        ds.setPassword("");
        return ds;
    }

    private void checkDatabase() {
        LOG.warn("Checking HSQLDB database schema ...");
        try {
            for (Schema schema : schemas) {
                schema.execute(getJdbcTemplate());
            }
            LOG.info("Done checking HSQLDB database schema.");
        } catch (Exception x) {
            LOG.error("Failed to initialize HSQLDB database.", x);
        }
    }
}
