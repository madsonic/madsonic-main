/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.madsonic.Logger;
import org.madsonic.domain.MusicFolderTask;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

/**
 * Provides database services for music folders scheduler tasks.
 *
 * @author Martin Karel
 */
public class MusicFolderTasksDao extends AbstractDao {

    private static final Logger LOG = Logger.getLogger(MusicFolderTasksDao.class);
    private static final String COLUMNS = "id, music_folder_id, found, scan, status, result, comment, enabled";
    private static final int[] DATATYPES = {Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.TIMESTAMP, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.BOOLEAN};
    
    private final MusicFolderTaskRowMapper rowMapper = new MusicFolderTaskRowMapper();

    public void createTask(MusicFolderTask musicFolderTask) {
        String sql = "insert into music_folder_tasks (" + COLUMNS + ") values (?, ?, ?, ?, ?, ?, ?, ?)";
        secureupdate(sql, DATATYPES, musicFolderTask.getId(), musicFolderTask.getMusic_folder_id(), musicFolderTask.getFound(), musicFolderTask.getScan(),
        		musicFolderTask.getStatus(), musicFolderTask.getResult(), musicFolderTask.getComment(), musicFolderTask.isEnabled());
        
        LOG.info("Created task for folder " + musicFolderTask.getMusic_folder_id());
    }

    public MusicFolderTask getTask(MusicFolderTask musicFolderTasks) {
        String sql = "select " + COLUMNS + " from music_folder_tasks where music_folder_id = ? and enabled";
        return queryOne(sql, rowMapper, musicFolderTasks.getId());
    }	
    
	public List<MusicFolderTask> getAllFolderTasks(int musicFolderId) {
		String sql ="select " + COLUMNS + " from music_folder_tasks " +
				"where id in (select music_folder_id from user_group_access where user_group_id = ? and enabled = 'true')";
        return query(sql, rowMapper, musicFolderId);
	}

	public List<MusicFolderTask> getQueuedTasks() {
		String sql ="select " + COLUMNS + " from music_folder_tasks where status = 'queued' and enabled = 'true')";
        return query(sql, rowMapper);
	}
	
	public MusicFolderTask getTask(int id) {
		String sql ="select " + COLUMNS + " from music_folder_tasks where music_folder_id = ?";
        return queryOne(sql, rowMapper, id);
	}
    
    public void deleteTask(Integer id) {
        String sql = "delete from music_folder_tasks where music_folder_id=?";
        update(sql, id);
        LOG.info("Deleted Task with ID " + id);
    }
    
    public void updateTask(MusicFolderTask musicFolderTask) {
        String sql = "update music_folder_tasks set found=?, scan=?, status=?, result=?, comment=?, enabled=? where music_folder_id=?";
        update(sql, musicFolderTask.getFound(), musicFolderTask.getScan(), musicFolderTask.getStatus(), 
        		    musicFolderTask.getResult(), musicFolderTask.getComment(), musicFolderTask.isEnabled(), musicFolderTask.getMusic_folder_id() );
    }

    private static class MusicFolderTaskRowMapper implements ParameterizedRowMapper<MusicFolderTask> {
        public MusicFolderTask mapRow(ResultSet rs, int rowNum) throws SQLException {
             return new MusicFolderTask(
            		rs.getInt(1),
            		rs.getInt(2), 
            		rs.getInt(3), 
            		rs.getTimestamp(4),
            		rs.getString(5), 
            		rs.getString(6), 
            		rs.getString(7), 
            		rs.getBoolean(8));
        }
    }

}
