/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import org.madsonic.Logger;
import org.madsonic.domain.Node;

/**
 * Provides database services for node devices.
 *
 * @author Martin Karel
 */
public class NodeDao extends AbstractDao {

    private static final Logger LOG = Logger.getLogger(NodeDao.class);
    private static final String COLUMNS = "id, url, name, online, enabled";
    private final NodeRowMapper rowMapper = new NodeRowMapper();

    /**
     * Returns all node devices.
     *
     * @return Possibly empty list of all node devices.
     */
    public List<Node> getAllNodes() {
        String sql = "select " + COLUMNS + " from nodes";
        return query(sql, rowMapper);
    }

    
	public Node getNode(String url) {
        String sql = "select " + COLUMNS + " from nodes where url=?";
        return queryOne(sql,rowMapper, url);
    }
    
    /**
     * Creates a new node device.
     *
     * @param node The node device to create.
     */
    public void createNode(Node node) {
        String sql = "insert into nodes (" + COLUMNS + ") values (null, ?, ?, ?, ?)";
        update(sql, node.getUrl(), node.getName(), node.isOnline(), node.isEnabled());
        LOG.info("Created node device " + node.getUrl());
    }

    /**
     * Deletes the node device with the given ID.
     *
     * @param id The node device ID.
     */
    public void deleteNode(Integer id) {
        String sql = "delete from nodes where id=?";
        update(sql, id);
        LOG.info("Deleted node device with ID " + id);
    }

    /**
     * Updates the given node device.
     *
     * @param node The node device to update.
     */
    public void updateNode(Node node) {
        String sql = "update nodes set url=?, name=?, online=?, enabled=? where url=?";
        update(sql, node.getUrl(), node.getName(), node.isOnline(), node.isEnabled(), node.getUrl());
    }

    private static class NodeRowMapper implements ParameterizedRowMapper<Node> {
        public Node mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Node(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getBoolean(4), rs.getBoolean(5));
        }
    }

}
