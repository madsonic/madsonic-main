/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2009-2015 (C) Sindre Mehus, Martin Karel
 */
package org.madsonic.dao.schema.postgresql;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.base.Splitter;

import org.madsonic.Logger;
import org.madsonic.dao.schema.Schema;
import org.madsonic.util.StringUtil;

/**
 * Used for creating and evolving the database schema.
 * This class implementes the database schema for Postgres.
 *
 * @author Martin Karel
 */
public class PostgreSQLSchema01 extends Schema {
    private static final Logger LOG = Logger.getLogger(PostgreSQLSchema01.class);

    public void execute(JdbcTemplate template) throws Exception {
    	
        if (!tableExists(template, "version")) {
            LOG.info("Database table 'version' not found. Creating it.");
            template.execute("create table version (version int not null)");
            template.execute("insert into version values (200)");
            
            LOG.info("Database table 'version' was created successfully.");

            InputStream in = getClass().getResourceAsStream("postgresql_schema_01.sql");
            String statements = IOUtils.toString(in, StringUtil.ENCODING_UTF8);
            for (String statement : Splitter.on(";").omitEmptyStrings().trimResults().split(statements)) {
                template.execute(statement);
                LOG.info(statement);
            }
        }
    }
}