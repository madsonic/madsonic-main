/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.dao.schema.hsql;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.madsonic.Logger;
import org.madsonic.dao.schema.Schema;
import org.madsonic.domain.TranscodeScheme;
import org.madsonic.util.Util;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Used for creating and evolving the database schema.
 * This class implementes the database schema for HyperSQL.
 *
 * @author Martin Karel
 */
public class HSqlSchema01 extends Schema {
	
    private static final Logger LOG = Logger.getLogger(HSqlSchema01.class);

	private static final String[] AVATARS = { "000", 
		"001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", 
		"012", "013", "014", "015", "016", "017", "018", "019", "020", "021", "022", 
		"023", "024", "025", "026", "027", "028", "029", "030", "031", "032", "033",
		"034", "035", "036", "037", "038", "039", "041", "041", "042", "043", "044",
		"045", "046", "047", "048", "049", "050", "051", "052", "053", "054", "055",
		"056", "057", "058", "059", "060", "061", "062", "063", "064", "065", "066",
		"067", "068", "069", "070", "071", "072", "073", "074", "075", "076", "077",
		"078", "079", "080", "081", "082", "083", "084", "085", "086", "087", "088",
		"089", "090", "091", "092", "093", "094", "095", "096", "097", "098", "099",
		"100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110",
		"111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121",
		"122", "123", "124", "125", "126", "127", "128", "129", "130", "131", "132",
		"133", "134", "135", "136", "137", "138", "139", "140", "141", "142", "143",
		"144", "145", "146", "147", "148", "149", "150", "151", "152", "153", "154",
		"155", "156", "201", "202", "203", "204", "205", "206", "207", "208", "209",
		"210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220",
		"221", "222", "223", "229", "230", "231", "232", "233", "234", "235", "236",
		"237", "238", "239", "240", "241", "242", "243", "244", "245", "246", "247",
		"248", "249", "250", "251", "252", "253", "254", "255", "256", "258", "259",
		"260", "261", "262", "263", "264", "265", "266", "267", "268", "300", "301",
		"302", "303", "304", "305", "306", "307", "308", "309", "310", "311", "312",
		"313", "314", "315", "316", "317", "318", "319", "320", "321", "322", "323",
		"324", "325", "326", "327", "328", "329", "330", "331", "332", "333", "334",
		"335", "336", "337", "338", "339", "340", "341", "342", "343"
	};
    
    @SuppressWarnings("deprecation")
	public void execute(JdbcTemplate template) throws Exception {
    	
        if (!tableExists(template, "version")) {
        	
            LOG.info("Database table 'version' not found.  Creating it.");
            template.execute("create table version (version int not null)");
            template.execute("insert into version values (200)");
            LOG.info("Database table 'version' was created successfully.");

            if (!tableExists(template, "role")) {
                LOG.info("Database table 'role' not found.  Creating it.");
                template.execute("create table role (" +
                                 "id int not null," +
                                 "name varchar not null," +
                                 "primary key (id))");
                template.execute("insert into role values (1, 'admin')");
                template.execute("insert into role values (2, 'download')");
                template.execute("insert into role values (3, 'upload')");
                template.execute("insert into role values (4, 'playlist')");
                template.execute("insert into role values (5, 'coverart')");
                template.execute("insert into role values (6, 'comment')");
                template.execute("insert into role values (7, 'podcast')");
                template.execute("insert into role values (8, 'stream')");
                template.execute("insert into role values (9, 'settings')");
                template.execute("insert into role values (10, 'jukebox')");
                template.execute("insert into role values (11, 'share')");
                template.execute("insert into role values (12, 'search')");
                template.execute("insert into role values (13, 'lastfm')");
                template.execute("insert into role values (14, 'image')");
                template.execute("insert into role values (15, 'video')");
                template.execute("insert into role values (16, 'video_conversion')");
                template.execute("insert into role values (17, 'audio_conversion')");
                
                LOG.info("Database table 'role' was created successfully.");
            }

            if (!tableExists(template, "user")) {
                LOG.info("Database table 'user' not found.  Creating it.");
                template.execute("create table user (" +
                                 "username varchar not null," +
                                 "password varchar not null," +
                                 "bytes_streamed bigint default 0 not null," +
                                 "bytes_downloaded bigint default 0 not null," +
                                 "bytes_uploaded bigint default 0 not null," +
                                 "ldap_authenticated boolean default false  not null," +
                                 "email varchar(256)," +
                                 "group_id integer default 0 not null," +
                                 "locked boolean default false not null," +
                                 "comment varchar(256)," +
                                 "primary key (username))");
                LOG.info("Database table 'user' was created successfully.");
                template.execute("insert into user values ('admin', 'admin', 0, 0, 0, false, NULL, 0, 0, '#admin user')");
                
            }

            if (!tableExists(template, "user_security")) {
                LOG.info("Database table 'user_security' not found.  Creating it.");
                template.execute("create cached table user_security (" +
                                 "username varchar not null," +
                                 "token varchar(256) not null," +
                				 "foreign key (username) references user(username) on delete cascade)");
                LOG.info("Database table 'user_security' was created successfully.");
            }
            
            if (!tableExists(template, "user_role")) {
                LOG.info("Database table 'user_role' not found.  Creating it.");
                template.execute("create table user_role (" +
                                 "username varchar not null," +
                                 "role_id int not null," +
                                 "primary key (username, role_id)," +
                                 "foreign key (username) references user(username)," +
                                 "foreign key (role_id) references role(id))");
                
                template.execute("insert into user_role values ('admin', 1)");
                template.execute("insert into user_role values ('admin', 2)");
                template.execute("insert into user_role values ('admin', 3)");
                template.execute("insert into user_role values ('admin', 4)");
                template.execute("insert into user_role values ('admin', 5)");
                template.execute("insert into user_role values ('admin', 6)");
                template.execute("insert into user_role values ('admin', 7)");
                template.execute("insert into user_role values ('admin', 8)");
                template.execute("insert into user_role values ('admin', 9)");
                template.execute("insert into user_role values ('admin', 10)");
                template.execute("insert into user_role values ('admin', 11)");
                template.execute("insert into user_role values ('admin', 12)");
                template.execute("insert into user_role values ('admin', 13)");
                template.execute("insert into user_role values ('admin', 14)");
                template.execute("insert into user_role values ('admin', 15)");
                template.execute("insert into user_role values ('admin', 16)");
                template.execute("insert into user_role values ('admin', 17)");
                
                LOG.info("Database table 'user_role' was created successfully.");
            }            
           
            if (!tableExists(template, "music_folder")) {
                LOG.info("Database table 'music_folder' not found.  Creating it.");
                template.execute("create table music_folder (" +
                                 "id identity," +
                                 "path varchar not null," +
                                 "name varchar not null," +
                                 "enabled boolean not null," +
								 "changed datetime default now not null," + 
								 "mf_index int default 1 not null," +			  
								 "mf_type int default 1 not null," +			  
                				 "mf_groupby int default 0 not null," + 
                				 "mf_timer datetime," +  
                				 "mf_interval int default 360000)");
                LOG.info("Database table 'music_folder' was created successfully.");
                 
                template.execute("insert into music_folder values (null, '" + Util.getDefaultMusicFolder() + "', 'Music', true, now(), 1, 1, 1, now(), 86400)");
                LOG.info("Database insert folder into table 'music_folder' was created successfully.");
            }

            if (!tableExists(template, "music_file_info")) {
                LOG.info("Database table 'music_file_info' not found.  Creating it.");
                template.execute("create cached table music_file_info (" +
                                 "id identity," +
                                 "path varchar not null," +
                                 "rating int," +
                                 "comment varchar," +
                                 "play_count int," +
                                 "last_played datetime)");
                LOG.info("Database table 'music_file_info' was created successfully.");
                LOG.info("Converting database column 'music_file_info.path' to varchar_ignorecase.");
                template.execute("alter table music_file_info alter column path varchar_ignorecase");
                template.execute("create index idx_music_file_info_path on music_file_info(path)");
                LOG.info("Database column 'music_file_info.path' was converted successfully.");
            }

            if (!tableExists(template, "internet_radio")) {
                LOG.info("Database table 'internet_radio' not found.  Creating it.");
                template.execute("create table internet_radio (" +
                                 "id identity," +
                                 "name varchar not null," +
                                 "stream_url varchar not null," +
                                 "homepage_url varchar," +
                                 "enabled boolean not null)");
                LOG.info("Database table 'internet_radio' was created successfully.");
            }

            if (!tableExists(template, "player")) {
                LOG.info("Database table 'player' not found.  Creating it.");
                template.execute("create table player (" +
                                 "id int not null," +
                                 "name varchar," +
                                 "type varchar," +
                                 "username varchar," +
                                 "ip_address varchar," +
                                 "auto_control_enabled boolean not null," +
                                 "last_seen datetime," +
                                 "cover_art_scheme varchar not null," +
                                 "transcode_scheme varchar not null," +
                                 "primary key (id))");
                LOG.info("Database table 'player' was created successfully.");
            }

            if (!columnExists(template, "dynamic_ip", "player")) {
                LOG.info("Database column 'player.dynamic_ip' not found.  Creating it.");
                template.execute("alter table player add dynamic_ip boolean default true not null");
                LOG.info("Database column 'player.dynamic_ip' was added successfully.");
            }

            if (template.queryForInt("select count(*) from role where id = 6") == 0) {
                LOG.info("Role 'comment' not found in database. Creating it.");
                template.execute("insert into role values (6, 'comment')");
                template.execute("insert into user_role " +
                                 "select distinct u.username, 6 from user u, user_role ur " +
                                 "where u.username = ur.username and ur.role_id in (1, 5)");
                LOG.info("Role 'comment' was created successfully.");
            }
            
            if (!tableExists(template, "user_settings")) {
                LOG.info("Database table 'user_settings' not found.  Creating it.");
                template.execute("create table user_settings (" +
                                 "username varchar not null," +
                                 "locale varchar," +
                                 "theme_id varchar," +
                                 "final_version_notification boolean default true not null," +
                                 "beta_version_notification boolean default false not null," +
                                 "main_caption_cutoff int default 35 not null," +
                                 "main_disc_number boolean default false not null," +
                                 "main_track_number boolean default true not null," +
                                 "main_artist boolean default true not null," +
                                 "main_album boolean default false not null," +
                                 "main_genre boolean default false not null," +
                                 "main_mood boolean default false not null," +
                                 "main_year boolean default false not null," +
                                 "main_bit_rate boolean default false not null," +
                                 "main_duration boolean default true not null," +
                                 "main_format boolean default false not null," +
                                 "main_file_size boolean default false not null," +
                                 "main_bpm boolean default false not null," +
                                 "main_composer boolean default false not null," +
                                 "playlist_caption_cutoff int default 35 not null," +
                                 "playlist_disc_number boolean default false not null," +
                                 "playlist_track_number boolean default false not null," +
                                 "playlist_artist boolean default true not null," +
                                 "playlist_album boolean default true not null," +
                                 "playlist_genre boolean default false not null," +
                                 "playlist_mood boolean default false not null," +
                                 "playlist_year boolean default true not null," +
                                 "playlist_bit_rate boolean default false not null," +
                                 "playlist_duration boolean default true not null," +
                                 "playlist_format boolean default true not null," +
                                 "playlist_file_size boolean default true not null," +
                                 "playlist_bpm boolean default false not null," +
                                 "playlist_composer boolean default false not null," +
                                 "primary key (username)," +
                                 "foreign key (username) references user(username) on delete cascade)");
                LOG.info("Database table 'user_settings' was created successfully.");
            }

            if (!tableExists(template, "transcoding")) {
                LOG.info("Database table 'transcoding' not found.  Creating it.");
                template.execute("create table transcoding (" +
                                 "id identity," +
                                 "name varchar not null," +
                                 "source_format varchar not null," +
                                 "target_format varchar not null," +
                                 "step1 varchar not null," +
                                 "step2 varchar," +
                                 "step3 varchar," +
                                 "enabled boolean not null)");

//TODO: complete remove table                
                
//                template.execute("insert into transcoding values(null,'wav > mp3', 'wav', 'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");
//                template.execute("insert into transcoding values(null,'flac > mp3','flac','mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");
//                template.execute("insert into transcoding values(null,'ogg > mp3' ,'ogg' ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");
//                template.execute("insert into transcoding values(null,'wma > mp3' ,'wma' ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");
//                template.execute("insert into transcoding values(null,'m4a > mp3' ,'m4a' ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,false)");
//                template.execute("insert into transcoding values(null,'aac > mp3' ,'aac' ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,false)");
//                template.execute("insert into transcoding values(null,'ape > mp3' ,'ape' ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");
//                template.execute("insert into transcoding values(null,'mpc > mp3' ,'mpc' ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");
//                template.execute("insert into transcoding values(null,'mv > mp3'  ,'mv'  ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");
//                template.execute("insert into transcoding values(null,'shn > mp3' ,'shn' ,'mp3','ffmpeg -i %s -v 0 -f wav -','lame -b %b --tt %t --ta %a --tl %l -S --resample 44.1 - -',null,true)");

                LOG.info("Database table 'transcoding' was created successfully.");
            }

            if (!tableExists(template, "player_transcoding")) {
                LOG.info("Database table 'player_transcoding' not found.  Creating it.");
                template.execute("create table player_transcoding (" +
                                 "player_id int not null," +
                                 "transcoding_id int not null," +
                                 "primary key (player_id, transcoding_id)," +
                                 "foreign key (player_id) references player(id) on delete cascade," +
                                 "foreign key (transcoding_id) references transcoding(id) on delete cascade)");
                LOG.info("Database table 'player_transcoding' was created successfully.");
            }

            if (!tableExists(template, "user_rating")) {
                LOG.info("Database table 'user_rating' not found.  Creating it.");
                template.execute("create table user_rating (" +
                                 "username varchar not null," +
                                 "path varchar not null," +
                                 "rating double not null," +
                                 "primary key (username, path)," +
                                 "foreign key (username) references user(username) on delete cascade)");
                LOG.info("Database table 'user_rating' was created successfully.");

                template.execute("insert into user_rating select 'admin', path, rating from music_file_info " +
                                 "where rating is not null and rating > 0");
                LOG.info("Migrated data from 'music_file_info' to 'user_rating'.");
            }
            
            if (!columnExists(template, "last_fm_enabled", "user_settings")) {
                LOG.info("Database columns 'user_settings.last_fm_*' not found.  Creating them.");
                template.execute("alter table user_settings add last_fm_enabled boolean default false not null");
                template.execute("alter table user_settings add last_fm_username varchar null");
                template.execute("alter table user_settings add last_fm_password varchar null");
                LOG.info("Database columns 'user_settings.last_fm_*' were added successfully.");
            }

            if (!columnExists(template, "transcode_scheme", "user_settings")) {
                LOG.info("Database column 'user_settings.transcode_scheme' not found.  Creating it.");
                template.execute("alter table user_settings add transcode_scheme varchar default '" +
                                 TranscodeScheme.OFF.name() + "' not null");
                LOG.info("Database column 'user_settings.transcode_scheme' was added successfully.");
            }
            
            if (!columnExists(template, "enabled", "music_file_info")) {
                LOG.info("Database column 'music_file_info.enabled' not found.  Creating it.");
                template.execute("alter table music_file_info add enabled boolean default true not null");
                LOG.info("Database column 'music_file_info.enabled' was added successfully.");
            }

            if (!columnExists(template, "default_active", "transcoding")) {
                LOG.info("Database column 'transcoding.default_active' not found.  Creating it.");
                template.execute("alter table transcoding add default_active boolean default true not null");
                LOG.info("Database column 'transcoding.default_active' was added successfully.");
            }
            
            if (!columnExists(template, "show_now_playing", "user_settings")) {
                LOG.info("Database column 'user_settings.show_now_playing' not found.  Creating it.");
                template.execute("alter table user_settings add show_now_playing boolean default false not null");
                LOG.info("Database column 'user_settings.show_now_playing' was added successfully.");
            }

            if (!columnExists(template, "selected_music_folder_id", "user_settings")) {
                LOG.info("Database column 'user_settings.selected_music_folder_id' not found.  Creating it.");
                template.execute("alter table user_settings add selected_music_folder_id int default -1 not null");
                LOG.info("Database column 'user_settings.selected_music_folder_id' was added successfully.");
            }

            if (!tableExists(template, "podcast_channel")) {
                LOG.info("Database table 'podcast_channel' not found.  Creating it.");
                template.execute("create table podcast_channel (" +
                                 "id identity," +
                                 "url varchar not null," +
                                 "title varchar," +
                                 "description varchar," +
                                 "status varchar not null," +
                                 "error_message varchar)");
                LOG.info("Database table 'podcast_channel' was created successfully.");
            }

            if (!tableExists(template, "podcast_episode")) {
                LOG.info("Database table 'podcast_episode' not found.  Creating it.");
                template.execute("create table podcast_episode (" +
                                 "id identity," +
                                 "channel_id int not null," +
                                 "url varchar not null," +
                                 "path varchar," +
                                 "title varchar," +
                                 "description varchar," +
                                 "publish_date datetime," +
                                 "duration varchar," +
                                 "bytes_total bigint," +
                                 "bytes_downloaded bigint," +
                                 "status varchar not null," +
                                 "error_message varchar," +
                                 "foreign key (channel_id) references podcast_channel(id) on delete cascade)");
                LOG.info("Database table 'podcast_episode' was created successfully.");
            }

            if (template.queryForInt("select count(*) from role where id = 7") == 0) {
                LOG.info("Role 'podcast' not found in database. Creating it.");
                template.execute("insert into role values (7, 'podcast')");
                template.execute("insert into user_role " +
                                 "select distinct u.username, 7 from user u, user_role ur " +
                                 "where u.username = ur.username and ur.role_id = 1");
                LOG.info("Role 'podcast' was created successfully.");
            }

            if (!columnExists(template, "client_side_playlist", "player")) {
                LOG.info("Database column 'player.client_side_playlist' not found.  Creating it.");
                template.execute("alter table player add client_side_playlist boolean default false not null");
                LOG.info("Database column 'player.client_side_playlist' was added successfully.");
            }
            
            if (!columnExists(template, "ldap_authenticated", "user")) {
                LOG.info("Database column 'user.ldap_authenticated' not found.  Creating it.");
                template.execute("alter table user add ldap_authenticated boolean default false not null");
                LOG.info("Database column 'user.ldap_authenticated' was added successfully.");
            }

            if (!columnExists(template, "party_mode_enabled", "user_settings")) {
                LOG.info("Database column 'user_settings.party_mode_enabled' not found.  Creating it.");
                template.execute("alter table user_settings add party_mode_enabled boolean default false not null");
                LOG.info("Database column 'user_settings.party_mode_enabled' was added successfully.");
            }
            
            if (!columnExists(template, "now_playing_allowed", "user_settings")) {
                LOG.info("Database column 'user_settings.now_playing_allowed' not found.  Creating it.");
                template.execute("alter table user_settings add now_playing_allowed boolean default true not null");
                LOG.info("Database column 'user_settings.now_playing_allowed' was added successfully.");
            }

            if (!columnExists(template, "web_player_default", "user_settings")) {
                LOG.info("Database column 'user_settings.web_player_default' not found.  Creating it.");
                template.execute("alter table user_settings add web_player_default boolean default false not null");
                LOG.info("Database column 'user_settings.web_player_default' was added successfully.");
            }

            if (template.queryForInt("select count(*) from role where id = 8") == 0) {
                LOG.info("Role 'stream' not found in database. Creating it.");
                template.execute("insert into role values (8, 'stream')");
                template.execute("insert into user_role select distinct u.username, 8 from user u");
                LOG.info("Role 'stream' was created successfully.");
            }

            if (!tableExists(template, "system_avatar")) {
                LOG.info("Database table 'system_avatar' not found.  Creating it.");
                template.execute("create table system_avatar (" +
                                 "id identity," +
                                 "name varchar," +
                                 "created_date datetime not null," +
                                 "mime_type varchar not null," +
                                 "width int not null," +
                                 "height int not null," +
                                 "data binary not null)");
                LOG.info("Database table 'system_avatar' was created successfully.");
            }

            if (!tableExists(template, "custom_avatar")) {
                LOG.info("Database table 'custom_avatar' not found.  Creating it.");
                template.execute("create table custom_avatar (" +
                                 "id identity," +
                                 "name varchar," +
                                 "created_date datetime not null," +
                                 "mime_type varchar not null," +
                                 "width int not null," +
                                 "height int not null," +
                                 "data binary not null," +
                                 "username varchar not null," +
                                 "foreign key (username) references user(username) on delete cascade)");
                LOG.info("Database table 'custom_avatar' was created successfully.");
            }

            if (!columnExists(template, "avatar_scheme", "user_settings")) {
                LOG.info("Database column 'user_settings.avatar_scheme' not found.  Creating it.");
                template.execute("alter table user_settings add avatar_scheme varchar default 'NONE' not null");
                LOG.info("Database column 'user_settings.avatar_scheme' was added successfully.");
            }

            if (!columnExists(template, "system_avatar_id", "user_settings")) {
                LOG.info("Database column 'user_settings.system_avatar_id' not found.  Creating it.");
                template.execute("alter table user_settings add system_avatar_id int");
                template.execute("alter table user_settings add foreign key (system_avatar_id) references system_avatar(id)");
                LOG.info("Database column 'user_settings.system_avatar_id' was added successfully.");
            }

    		// Import new avatar templates
    		if (template.queryForInt("select count(*) from system_avatar where name = '001'") == 0) {
    			
    			LOG.info("Reset avatar_scheme for users. Update it.");
    			template.execute("update user_settings set avatar_scheme='NONE', system_avatar_id=null where avatar_scheme='SYSTEM'");
    			template.execute("delete from system_avatar");  
    			
    			LOG.info("Avatar templates not found in database. Creating it.");
    			for (String avatar : AVATARS) {
    				createAvatar(template, avatar);		
    			}
    			LOG.info("Avatar templates created successfully.");
    			
    			template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='001') where avatar_scheme='NONE'");
    			template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='010') where username='admin'");               
    			template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='002') where username='guest'");            
    			template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='004') where username='demo'"); 
    			LOG.info("Avatar templates updated successfully.");
    		}         
            
            
            if (!columnExists(template, "jukebox", "player")) {
                 LOG.info("Database column 'player.jukebox' not found.  Creating it.");
                 template.execute("alter table player add jukebox boolean default false not null");
                 LOG.info("Database column 'player.jukebox' was added successfully.");
             }
            
            if (!columnExists(template, "technology", "player")) {
                LOG.info("Database column 'player.technology' not found.  Creating it.");
                template.execute("alter table player add technology varchar default 'WEB' not null");
                LOG.info("Database column 'player.technology' was added successfully.");
            }
            
            if (!columnExists(template, "changed", "music_folder")) {
                LOG.info("Database column 'music_folder.changed' not found.  Creating it.");
                template.execute("alter table music_folder add changed datetime default now not null");
                LOG.info("Database column 'music_folder.changed' was added successfully.");
            }

            if (!columnExists(template, "changed", "internet_radio")) {
                LOG.info("Database column 'internet_radio.changed' not found.  Creating it.");
                template.execute("alter table internet_radio add changed datetime default now not null");
                LOG.info("Database column 'internet_radio.changed' was added successfully.");
            }

            if (!columnExists(template, "changed", "user_settings")) {
                LOG.info("Database column 'user_settings.changed' not found.  Creating it.");
                template.execute("alter table user_settings add changed datetime default now not null");
                LOG.info("Database column 'user_settings.changed' was added successfully.");
            }
            
            if (!columnExists(template, "client_id", "player")) {
                LOG.info("Database column 'player.client_id' not found.  Creating it.");
                template.execute("alter table player add client_id varchar");
                LOG.info("Database column 'player.client_id' was added successfully.");
            }

            if (!columnExists(template, "show_chat", "user_settings")) {
                LOG.info("Database column 'user_settings.show_chat' not found.  Creating it.");
                template.execute("alter table user_settings add show_chat boolean default false not null");
                LOG.info("Database column 'user_settings.show_chat' was added successfully.");
            }            
            
            if (!tableExists(template, "share")) {
                LOG.info("Table 'share' not found in database. Creating it.");
                template.execute("create cached table share (" +
                        "id identity," +
                        "name varchar not null," +
                        "description varchar," +
                        "username varchar not null," +
                        "created datetime not null," +
                        "expires datetime," +
                        "last_visited datetime," +
                        "visit_count int default 0 not null," +
                        "unique (name)," +
                        "foreign key (username) references user(username) on delete cascade)");
                template.execute("create index idx_share_name on share(name)");

                LOG.info("Table 'share' was created successfully.");
                LOG.info("Table 'share_file' not found in database. Creating it.");
                template.execute("create cached table share_file (" +
                        "id identity," +
                        "share_id int not null," +
                        "path varchar not null," +
                        "foreign key (share_id) references share(id) on delete cascade)");
                LOG.info("Table 'share_file' was created successfully.");
            }
            
            if (!tableExists(template, "transcoding2")) {
                LOG.info("Database table 'transcoding2' not found.  Creating it.");
                template.execute("create table transcoding2 (" +
                                 "id identity," +
                                 "name varchar not null," +
                                 "source_formats varchar not null," +
                                 "target_format varchar not null," +
                                 "step1 varchar not null," +
                                 "step2 varchar," +
                                 "step3 varchar)");
              					
                LOG.info("Database table 'transcoding2' was created successfully.");
            }

            if (!columnExists(template, "default_active", "transcoding2")) {
                LOG.info("Database column 'transcoding2.default_active' not found.  Creating it.");
                template.execute("alter table transcoding2 add default_active boolean default true not null");
                LOG.info("Database column 'transcoding2.default_active' was added successfully.");
            }            
            
            if (!tableExists(template, "player_transcoding2")) {
                LOG.info("Database table 'player_transcoding2' not found.  Creating it.");
                template.execute("create table player_transcoding2 (" +
                                 "player_id int not null," +
                                 "transcoding_id int not null," +
                                 "primary key (player_id, transcoding_id)," +
                                 "foreign key (player_id) references player(id) on delete cascade," +
                                 "foreign key (transcoding_id) references transcoding2(id) on delete cascade)");

                template.execute("delete from transcoding2");
                template.execute("delete from player_transcoding2");
                template.execute("insert into transcoding2(name, source_formats, target_format, step1) values('wtv->flv', 'wtv', 'flv', 'ffmpeg -ss %o -i %s -c:v libx264 -preset fast -async 30 -b %bk -r 23-.976 -s 720x360 -ar 44100 -ac 2 -v 0 -f flv -threads 0 -')");		
                template.execute("insert into transcoding2(name, source_formats, target_format, step1) values('hdrun->flv', 'hdrun', 'flv', 'ffmpeg -ss %o -i %z -c:v libx264 -preset superfast -async 1 -b %bk -s %wx%h -ar 44100 -ac 2 -v 0 -f flv -threads 0 -')");		
                template.execute("insert into transcoding2(name, source_formats, target_format, step1) values('tv->flv', 'tv', 'flv', 'ffmpeg -ss %o -i %p -c:v libx264 -preset superfast -async 1 -b %bk -s %wx%h -ar 44100 -ac 2 -v 0 -f flv -threads 0 -')");
                template.execute("insert into transcoding2(name, source_formats, target_format, step1) values('audio->mp3', 'aac aif aiff ape dff dsf flac m4a mpc ogg oga opus shn wav wma', 'mp3', 'ffmpeg -i %s -map 0:a:0 -b:a %bk -v 0 -f mp3 -')");        
                template.execute("insert into transcoding2(name, source_formats, target_format, step1) values('mod->mp3', '669 alm amd amf dbm dmf emod far flx fnk gdm gmc gtk hsc imf it j2b liq m15 mdl med mgt mod mtm mtn mtp okt psm ptm rad rtm s3m sfx stm stx ult umx wow xm', 'mp3', 'ffmpeg -i %s -map 0:a:0 -b:a %bk -v 0 -f mp3 -')");
                template.execute("insert into player_transcoding2(player_id, transcoding_id) select distinct p.id, t.id from player p, transcoding2 t");
                
                LOG.info("Database table 'player_transcoding2' was created successfully.");
            }

            if (!tableExists(template, "media_file")) {
                LOG.info("Database table 'media_file' not found.  Creating it.");
                template.execute("create cached table media_file (" +
                        "id identity," +
                        "path varchar not null," +
                        "folder varchar," +
                        "type varchar not null," +
                        "override boolean not null," +
                        "format varchar," +
                        "data varchar," +					
                        "title varchar," +
                        "album varchar," +
                        "album_name varchar," +
                        "artist varchar," +
                        "album_artist varchar," +
                        "disc_number int," +
                        "track_number int," +
                        "year int," +
                        "genre varchar," +
                        "mood varchar," +
                        "bpm double," +
                        "composer varchar," +
                        "bit_rate int," +
                        "variable_bit_rate boolean not null," +
                        "duration_seconds int," +
                        "file_size bigint," +
                        "width int," +
                        "height int," +
                        "cover_art_path varchar," +
                        "parent_path varchar," +
                        "play_count int not null," +
                        "last_played datetime," +
                        "comment varchar," +
                        "created datetime not null," +
                        "changed datetime not null," +
                        "last_scanned datetime not null," +
                        "first_scanned datetime not null," +
                        "children_last_updated datetime not null," +
                        "present boolean not null," +
                        "version int not null," +
                        "unique (path))");

                template.execute("create index idx_media_file_id on media_file(id)");
                template.execute("create index idx_media_file_path on media_file(path)");
                template.execute("create index idx_media_file_parent_path on media_file(parent_path)");
                template.execute("create index idx_media_file_type on media_file(type)");
                template.execute("create index idx_media_file_data on media_file(data)");
                template.execute("create index idx_media_file_album on media_file(album)");
                template.execute("create index idx_media_file_album_name on media_file(album_name)");
                template.execute("create index idx_media_file_artist on media_file(artist)");
                template.execute("create index idx_media_file_album_artist on media_file(album_artist)");
                template.execute("create index idx_media_file_present on media_file(present)");
                template.execute("create index idx_media_file_genre on media_file(genre)");
                template.execute("create index idx_media_file_mood on media_file(mood)");
                template.execute("create index idx_media_file_bpm on media_file(bpm)");
                template.execute("create index idx_media_file_composer on media_file(composer)");
                template.execute("create index idx_media_file_play_count on media_file(play_count)");
                template.execute("create index idx_media_file_created on media_file(created)");
                template.execute("create index idx_media_file_last_played on media_file(last_played)");

                LOG.info("Database table 'media_file' was created successfully.");
            }

            if (!tableExists(template, "artist")) {
                LOG.info("Database table 'artist' not found.  Creating it.");
                template.execute("create cached table artist (" +
                        "id identity," +
                        "name varchar not null," +
                        "artist_folder varchar," +
                        "cover_art_path varchar," +
                        "last_scanned datetime not null," +
                        "present boolean not null," +
                        "album_count int default 0 not null," +
                        "play_count int default 0 not null," +
                        "song_count int default 0 not null," +
                        "topplay_found int default 0 not null," +
                        "topplay_count int default 0 not null," +             
                        "unique (name))");

                template.execute("create index idx_artist_name on artist(name)");
                template.execute("create index idx_artist_present on artist(present)");
	            template.execute("create index idx_artist_artist_folder_name on artist(artist_folder, name)");

                LOG.info("Database table 'artist' was created successfully.");
            }

            if (!tableExists(template, "album")) {
                LOG.info("Database table 'album' not found.  Creating it.");
                template.execute("create cached table album (" +
                        "id identity," +
                        "path varchar not null," +
                        "name varchar not null," +
                        "nameid3 varchar," +
                        "SetName varchar," +
                        "artist varchar not null," +
                        "song_count int default 0 not null," +
                        "duration_seconds int default 0 not null," +
                        "cover_art_path varchar," +
                        "play_count int default 0 not null," +
                        "last_played datetime," +
                        "comment varchar," +
                        "created datetime not null," +
                        "last_scanned datetime not null," +
                        "genre varchar," +
                        "year int," +
                        "mediaFileId int," +
                        "present boolean not null," +
                        "unique (artist, name))");

                template.execute("create index idx_album_artist_name on album(artist, name)");
     			template.execute("create index idx_album_nameid3 on album(nameid3)");
     			template.execute("create index idx_album_SetName on album(SetName)");
                template.execute("create index idx_album_play_count on album(play_count)");
                template.execute("create index idx_album_last_played on album(last_played)");
                template.execute("create index idx_album_present on album(present)");
	            template.execute("create index idx_album_mediaFileId on album(mediaFileId)");
	            template.execute("create index idx_album_genre on album(genre)");
	            template.execute("create index idx_album_year on album(year)");

	            //Added new index
				LOG.info("Database index was added successfully.");
                LOG.info("Database table 'album' was created successfully.");
            }

            // Added in 4.7.beta3
            if (!rowExists(template, "table_name='ALBUM' and column_name='NAME' and ordinal_position=1","information_schema.system_indexinfo")) {
                template.execute("create index idx_album_name on album(name)");
            }

            if (!tableExists(template, "starred_media_file")) {
                LOG.info("Database table 'starred_media_file' not found.  Creating it.");
                template.execute("create table starred_media_file (" +
                        "id identity," +
                        "media_file_id int not null," +
                        "username varchar not null," +
                        "created datetime not null," +
                        "foreign key (media_file_id) references media_file(id) on delete cascade,"+
                        "foreign key (username) references user(username) on delete cascade," +
                        "unique (media_file_id, username))");

                template.execute("create index idx_starred_media_file_media_file_id on starred_media_file(media_file_id)");
                template.execute("create index idx_starred_media_file_username on starred_media_file(username)");

                LOG.info("Database table 'starred_media_file' was created successfully.");
            }

            if (!tableExists(template, "starred_album")) {
                LOG.info("Database table 'starred_album' not found.  Creating it.");
                template.execute("create table starred_album (" +
                        "id identity," +
                        "album_id int not null," +
                        "username varchar not null," +
                        "created datetime not null," +
                        "foreign key (album_id) references album(id) on delete cascade," +
                        "foreign key (username) references user(username) on delete cascade," +
                        "unique (album_id, username))");

                template.execute("create index idx_starred_album_album_id on starred_album(album_id)");
                template.execute("create index idx_starred_album_username on starred_album(username)");

                LOG.info("Database table 'starred_album' was created successfully.");
            }

            if (!tableExists(template, "starred_artist")) {
                LOG.info("Database table 'starred_artist' not found.  Creating it.");
                template.execute("create table starred_artist (" +
                        "id identity," +
                        "artist_id int not null," +
                        "username varchar not null," +
                        "created datetime not null," +
                        "foreign key (artist_id) references artist(id) on delete cascade,"+
                        "foreign key (username) references user(username) on delete cascade," +
                        "unique (artist_id, username))");

                template.execute("create index idx_starred_artist_artist_id on starred_artist(artist_id)");
                template.execute("create index idx_starred_artist_username on starred_artist(username)");

                LOG.info("Database table 'starred_artist' was created successfully.");
            }

            if (!tableExists(template, "playlist")) {
                LOG.info("Database table 'playlist' not found.  Creating it.");
                template.execute("create table playlist (" +
                        "id identity," +
                        "username varchar not null," +
                        "is_public boolean not null," +
                        "name varchar not null," +
                        "comment varchar," +
                        "file_count int default 0 not null," +
                        "duration_seconds int default 0 not null," +
                        "created datetime not null," +
                        "changed datetime not null," +
                        "foreign key (username) references user(username) on delete cascade)");

                LOG.info("Database table 'playlist' was created successfully.");
            }

            if (!columnExists(template, "imported_from", "playlist")) {
                LOG.info("Database column 'playlist.imported_from' not found.  Creating it.");
                template.execute("alter table playlist add imported_from varchar");
                LOG.info("Database column 'playlist.imported_from' was added successfully.");
            }

            if (!tableExists(template, "playlist_file")) {
                LOG.info("Database table 'playlist_file' not found.  Creating it.");
                template.execute("create cached table playlist_file (" +
                        "id identity," +
                        "playlist_id int not null," +
                        "media_file_id int not null," +
                        "foreign key (playlist_id) references playlist(id) on delete cascade," +
                        "foreign key (media_file_id) references media_file(id) on delete cascade)");

                LOG.info("Database table 'playlist_file' was created successfully.");
            }

            if (!tableExists(template, "playlist_user")) {
                LOG.info("Database table 'playlist_user' not found.  Creating it.");
                template.execute("create table playlist_user (" +
                        "id identity," +
                        "playlist_id int not null," +
                        "username varchar not null," +
                        "unique(playlist_id, username)," +
                        "foreign key (playlist_id) references playlist(id) on delete cascade," +
                        "foreign key (username) references user(username) on delete cascade)");

                LOG.info("Database table 'playlist_user' was created successfully.");
            }

            if (!tableExists(template, "bookmark")) {
                LOG.info("Database table 'bookmark' not found.  Creating it.");
                template.execute("create table bookmark (" +
                        "id identity," +
                        "media_file_id int not null," +
                        "position_millis bigint not null," +
                        "username varchar not null," +
                        "comment varchar," +
                        "created datetime not null," +
                        "changed datetime not null," +
                        "foreign key (media_file_id) references media_file(id) on delete cascade,"+
                        "foreign key (username) references user(username) on delete cascade," +
                        "unique (media_file_id, username))");

                template.execute("create index idx_bookmark_media_file_id on bookmark(media_file_id)");
                template.execute("create index idx_bookmark_username on bookmark(username)");

                LOG.info("Database table 'bookmark' was created successfully.");
            }
            
            // Added in 4.7.beta3
            if (!rowExists(template, "table_name='ALBUM' and column_name='NAME' and ordinal_position=1", "information_schema.system_indexinfo")) {
                template.execute("create index idx_album_name on album(name)");
            }		
    		
    		// Added new Usersettings
            if (!columnExists(template, "list_type", "user_settings")) {
                LOG.info("Database column 'user_settings.list_type' not found.  Creating it.");
                template.execute("alter table user_settings add list_type varchar default 'random' not null");
                LOG.info("Database column 'user_settings.list_type' was added successfully.");
            }

    		// Update user_settings
            if (!columnExists(template, "list_rows", "user_settings")) {
                LOG.info("Database column 'user_settings.list_rows' not found.  Creating it.");
                template.execute("alter table user_settings add list_rows int default 2");
                LOG.info("Database column 'user_settings.list_rows' was added successfully.");
            }

            if (!columnExists(template, "list_columns", "user_settings")) {
                LOG.info("Database column 'user_settings.list_columns' not found.  Creating it.");
                template.execute("alter table user_settings add list_columns int default 5");
                LOG.info("Database column 'user_settings.list_columns' was added successfully.");
            }

            if (!columnExists(template, "playqueue_resize", "user_settings")) {
                LOG.info("Database column 'user_settings.playqueue_resize' not found.  Creating it.");
                template.execute("alter table user_settings add playqueue_resize boolean default false not null");
                LOG.info("Database column 'user_settings.playqueue_resize' was added successfully.");
            }

            if (!columnExists(template, "leftframe_resize", "user_settings")) {
                LOG.info("Database column 'user_settings.leftframe_resize' not found.  Creating it.");
                template.execute("alter table user_settings add leftframe_resize boolean default false not null");
                LOG.info("Database column 'user_settings.leftframe_resize' was added successfully.");
            }

    	    if (!columnExists(template, "leftframe_resize", "user_settings")) {
                LOG.info("Database column 'user_settings.leftframe_resize' not found.  Creating it.");
                template.execute("alter table user_settings add leftframe_resize boolean default false not null");
                LOG.info("Database column 'user_settings.leftframe_resize' was added successfully.");
            }

    		// Update album Table
            if (!columnExists(template, "SetName", "album")) {
                LOG.info("Database column 'album.SetName' not found.  Creating it.");
                template.execute("alter table album add SetName varchar");
    			template.execute("create index idx_album_SetName on album(SetName)");
                LOG.info("Database column 'album.SetName' was added successfully.");
    			}		
    	
    		// Update media_file
            if (!columnExists(template, "override", "media_file")) {
                LOG.info("Database column 'media_file.override' not found.  Creating it.");
                template.execute("alter table media_file add override boolean default false not null");
                LOG.info("Database column 'media_file.override' was added successfully.");
            }		
    	
            if (!columnExists(template, "album_name", "media_file")) {
                LOG.info("Database column 'media_file.album_name' not found.  Creating it.");
                template.execute("alter table media_file add album_name varchar");
                template.execute("create index idx_media_file_album_name on media_file(album_name)");
                LOG.info("Database column 'media_file.album_name' was added successfully.");
    			}		

            if (!columnExists(template, "year", "album")) {
                LOG.info("Database column 'album.year' not found.  Creating it.");
                template.execute("alter table album add year int");
                LOG.info("Database column 'album.year' was added successfully.");
            }

            if (!columnExists(template, "genre", "album")) {
                LOG.info("Database column 'album.genre' not found.  Creating it.");
                template.execute("alter table album add genre varchar");
                LOG.info("Database column 'album.genre' was added successfully.");
            }

            if (!tableExists(template, "genre")) {
                LOG.info("Database table 'genre' not found.  Creating it.");
                template.execute("create table genre (" +
                        "name varchar not null," +
                        "song_count int not null)");
                LOG.info("Database table 'genre' was created successfully.");
            }

            if (!columnExists(template, "album_count", "genre")) {
                LOG.info("Database column 'genre.album_count' not found.  Creating it.");
                template.execute("alter table genre add album_count int default 0 not null");
                LOG.info("Database column 'genre.album_count' was added successfully.");
            }
            
            if (!columnExists(template, "artist_count", "genre")) {
                LOG.info("Database column 'genre.artist_count' not found.  Creating it.");
                template.execute("alter table genre add artist_count int default 0 not null");
                LOG.info("Database column 'genre.artist_count' was added successfully.");
            }

    		// Add Statistic Table
            if (!tableExists(template, "statistic_user")) {
                LOG.info("Database table 'statistic_user' not found. Creating it.");
                template.execute("create table statistic_user (" +
                        "id identity," +
                        "username varchar not null," +
                        "media_file_id int not null," +
                        "played datetime not null," +
                        "foreign key (media_file_id) references media_file(id) on delete cascade,"+
                        "foreign key (username) references user(username) on delete cascade)");

                template.execute("create index idx_statistic_user_media_file_id on statistic_user(media_file_id)");
                template.execute("create index idx_statistic_user_username on statistic_user(username)");

                LOG.info("Database table 'statistic_user' was created successfully.");
            }		
                
    		////////////////////

    		// Add Hot Recommmed Table
    		if (!tableExists(template, "hot_rating")) {
    			LOG.info("Database table 'hot_rating' not found. Creating it.");
    			template.execute("create table hot_rating (" +
    							 "username varchar not null," +
    							 "path varchar not null," +
    							 "id int not null," +
    							 "primary key (username, path)," +
    							 "foreign key (username) references user(username) on delete cascade)");
    		LOG.info("Database table 'hot_rating' was created successfully.");            
                
            }	
    		
    			
            if (!columnExists(template, "mf_index", "music_folder")) {
                LOG.info("Database column 'music_folder.mf_index' not found. Creating it.");
                template.execute("alter table music_folder add mf_index int default 1 not null");
                LOG.info("Database column 'music_folder.mf_index' was added successfully.");
            }			

    		////////////////////
    		
                template.execute("create index idx_starred_media_file_media_file_id_username on starred_media_file(media_file_id, username)");
                template.execute("create index idx_starred_media_file_created on starred_media_file(created)");
                LOG.info("Database index 'idx_starred_media_file_media_file_id_username' was added successfully.");
                LOG.info("Database index 'idx_starred_media_file_created' was added successfully.");
    		
    		////////////////////
    		
			// Added new Usersettings
			if (!columnExists(template, "customscrollbar", "user_settings")) {
				LOG.info("Database column 'user_settings.customscrollbar' not found. Creating it.");
				template.execute("alter table user_settings add customscrollbar boolean default true not null");
				LOG.info("Database column 'user_settings.customscrollbar' was added successfully.");
			}
            ////////////////////

    		// new Access Control
			// Add Group Table
			if (!tableExists(template, "user_group")) {
				LOG.info("Database table 'user_group' not found. Creating it.");
				template.execute("create table user_group (" +
						"id identity, " +
						"name varchar not null, " +
						"audio_default_bitrate int default 0 not null, " +
						"audio_max_bitrate int default 0 not null, " +
						"video_default_bitrate int default 1000 not null, " +
						"video_max_bitrate int default 10000 not null)");
				LOG.info("Database table 'user_group' was created successfully.");
			}	
			
			// Add Group Access Table
			if (!tableExists(template, "user_group_access")) {
				LOG.info("Database table 'user_group_access' not found. Creating it.");
				template.execute("create table user_group_access (" +
								 "user_group_id integer not null, " +
								 "music_folder_id integer not null, " +
								 "enabled boolean default true not null, " + 
								 "primary key (user_group_id, music_folder_id)," +
								 "foreign key (user_group_id) references user_group(id) on delete cascade," +
								 "foreign key (music_folder_id) references music_folder(id) on delete cascade)");
			LOG.info("Database table 'user_group_access' was created successfully.");            

            template.execute("create index idx_user_group_access_user_group_id_music_folder_id_enabled on user_group_access(user_group_id, music_folder_id, enabled)");
            LOG.info("Database index 'idx_user_group_access_user_group_id_music_folder_id_enabled' was added successfully.");
			}	
    			template.execute("insert into user_group (id, name, video_default_bitrate, video_max_bitrate) values (0, 'ALL', 1000, 5000)");
    			template.execute("insert into user_group (id, name, video_default_bitrate, video_max_bitrate) values (1, 'GUEST', 1000, 2000)");
    			template.execute("insert into user_group (id, name, video_default_bitrate, video_max_bitrate) values (2, 'FAMILY', 2000, 10000)");
    			template.execute("insert into user_group (id, name, video_default_bitrate, video_max_bitrate) values (3, 'FRIENDS', 2000, 10000)");
    			template.execute("insert into user_group (id, name, video_default_bitrate, video_max_bitrate) values (4, 'LIMITED', 500, 1000)");
    			template.execute("alter table user add constraint fk_group_id foreign key (group_id) references user_group (id)");
    			LOG.info("Database table 'user' was updated successfully.");            

    		// Reset Access to default
                template.execute("delete from user_group_access");
    			template.execute("insert into user_group_access (user_group_id, music_folder_id, enabled)" + 
    							 "(select distinct user_group.id as user_group_id, music_folder.id as music_folder_id, 'true' as enabled from public.user_group user_group, public.music_folder music_folder)");
    		
            // Added new albumartist
			if (!columnExists(template, "albumartist", "album")) {
				
				LOG.info("Database column 'album.albumartist' not found. Creating it.");
				template.execute("alter table album add albumartist varchar");
				LOG.info("Database column 'album.albumartist' was added successfully.");
				
	            template.execute("create index idx_album_artist_albumartist_name on album(artist, albumartist, name)");
	            template.execute("create index idx_album_albumartist_name on album(albumartist, name)");
	            LOG.info("Index was created successfully.");
			}
        
        	//Added new artist_folder
			if (!columnExists(template, "artist_folder", "artist")) {
				LOG.info("Database column 'artist.artist_folder' not found. Creating it.");
				template.execute("alter table artist add artist_folder varchar");
				LOG.info("Database column 'artist.artist_folder' was added successfully.");
			}

			// Added new albumartist
			if (!columnExists(template, "play_count", "artist")) {
				LOG.info("Database column 'artist.play_count' not found. Creating it.");
				template.execute("alter table artist add play_count int default 0 not null");
				LOG.info("Database column 'artist.play_count' was added successfully.");
			}
				
            // Added new albumartist
			if (!columnExists(template, "song_count", "artist")) {
				LOG.info("Database column 'artist.song_count' not found. Creating it.");
				template.execute("alter table artist add song_count int default 0 not null");
				LOG.info("Database column 'artist.song_count' was added successfully.");
			}
        
        
        	//Added new artist_folder
			if (!columnExists(template, "mediaFileId", "album")) {
				LOG.info("Database column 'album.mediaFileId' not found. Creating it.");
				template.execute("alter table album add mediaFileId int");
				LOG.info("Database column 'album.mediaFileId' was added successfully.");
			}

			// Added new albumartist
			if (!columnExists(template, "genre", "album")) {
				LOG.info("Database column 'album.genre' not found. Creating it.");
				template.execute("alter table album add genre varchar");
				LOG.info("Database column 'album.genre' was added successfully.");
			}
				
            // Added new albumartist
			if (!columnExists(template, "year", "album")) {
				LOG.info("Database column 'album.year' not found. Creating it.");
				template.execute("alter table album add year int");
				LOG.info("Database column 'album.year' was added successfully.");
			}
           
			// Added new Usersettings
			if (!columnExists(template, "customaccordion", "user_settings")) {
				LOG.info("Database column 'user_settings.customaccordion' not found. Creating it.");
				template.execute("alter table user_settings add customaccordion boolean default false not null");
				LOG.info("Database column 'user_settings.customaccordion' was added successfully.");
			}
    			
			// Added new Usersettings
			if (!columnExists(template, "autohide_chat", "user_settings")) {
				LOG.info("Database column 'user_settings.autohide_chat' not found. Creating it.");
				template.execute("alter table user_settings add autohide_chat boolean default false not null");
				LOG.info("Database column 'user_settings.autohide_chat' was added successfully.");
			}
            
			// Added new audio_default_bitrate
			if (!columnExists(template, "audio_default_bitrate", "user_group")) {
				LOG.info("Database column 'user_group.audio_default_bitrate' not found. Creating it.");
				template.execute("alter table user_group add audio_default_bitrate int default 0 not null");
				LOG.info("Database column 'user_group.audio_default_bitrate' was added successfully.");
			}

			// Added new audio_max_bitrate
			if (!columnExists(template, "audio_max_bitrate", "user_group")) {
				LOG.info("Database column 'user_group.audio_max_bitrate' not found. Creating it.");
				template.execute("alter table user_group add audio_max_bitrate int default 0 not null");
				LOG.info("Database column 'user_group.audio_max_bitrate' was added successfully.");
			}		            
            
			// Added new video_default_bitrate
			if (!columnExists(template, "video_default_bitrate", "user_group")) {
				LOG.info("Database column 'user_group.video_default_bitrate' not found. Creating it.");
				template.execute("alter table user_group add video_default_bitrate int default 1000 not null");
				LOG.info("Database column 'user_group.video_default_bitrate' was added successfully.");
			}

			// Added new video_max_bitrate
			if (!columnExists(template, "video_max_bitrate", "user_group")) {
				LOG.info("Database column 'user_group.video_max_bitrate' not found. Creating it.");
				template.execute("alter table user_group add video_max_bitrate int default 10000 not null");
				LOG.info("Database column 'user_group.video_max_bitrate' was added successfully.");
			}			
            
    		// Added new albumartist
        	if (!columnExists(template, "mood", "media_file")) {
        		LOG.info("Database column 'media_file.mood' not found. Creating it.");
        		template.execute("alter table media_file add mood varchar");
        		LOG.info("Database column 'media_file.mood' was added successfully.");
        	}

    		// Added new albumartist
        	if (!columnExists(template, "first_scanned", "media_file")) {
        		LOG.info("Database column 'media_file.first_scanned' not found. Creating it.");
        		template.execute("alter table media_file add first_scanned datetime");
        		LOG.info("Database column 'media_file.first_scanned' was added successfully.");
        	}

            // Added new albumartist
			if (!columnExists(template, "nameid3", "album")) {
				
				LOG.info("Database column 'album.nameid3' not found. Creating it.");
				template.execute("alter table album add nameid3 varchar");
				LOG.info("Database column 'album.nameid3' was added successfully.");

				template.execute("create index idx_album_nameid3 on album(nameid3)");
	            LOG.info("Index was created successfully.");

				template.execute("delete from album");
				LOG.info("Database table ablum cleared successfully.");
   			}

            // Added new albumartist
			if (!columnExists(template, "genre", "artist")) {
				
				LOG.info("Database column 'artist.genre' not found. Creating it.");
				template.execute("alter table artist add genre varchar");
				LOG.info("Database column 'artist.genre' was added successfully.");

				template.execute("create index idx_artist_genre on artist(genre)");
	            LOG.info("Index was created successfully.");
          
			}
            	
    		// Update 
            if (!tableExists(template, "statistic_server")) {
                LOG.info("database table 'statistic_server' not found.  creating it.");
                template.execute("create cached table statistic_server (" +
                        "id identity," +
                        "music_folder_id int not null," +
                		"artist_all int," + 
                		"artist_album int," +
                		"albums int," +
                		"genre int," +
                		"music_files int," + 
                		"music_files_size bigint," +
                		"video_files int," + 
                		"video_files_size bigint," +
                		"podcast_files int," +
                		"podcast_files_size bigint," +	 
                		"audiobooks_files int," +
                		"audiobooks_files_size bigint," +
                        "foreign key (music_folder_id) references music_folder(id) on delete cascade,"+
                        "unique (music_folder_id))");

                template.execute("create index idx_statistic_server_id on statistic_server(id)");
                template.execute("create index idx_statistic_server_music_folder_id on statistic_server(music_folder_id)");

                LOG.info("Database table 'statistic_server' was created successfully.");                
   			}
                
			// Added new Usersettings
			if (!columnExists(template, "selected_genre", "user_settings")) {
				LOG.info("Database column 'user_settings.selected_genre' not found. Creating it.");
				template.execute("alter table user_settings add selected_genre varchar");
				LOG.info("Database column 'user_settings.selected_genre' was added successfully.");
			}
                
    		// Added new albumartist
        	if (!columnExists(template, "sharelevel", "playlist")) {
        		LOG.info("Database column 'playlist.sharelevel' not found. Creating it.");
        		template.execute("alter table playlist add sharelevel int");
        		LOG.info("Database column 'playlist.sharelevel' was added successfully.");
        	}
 
			// Added new Usersettings
			if (!columnExists(template, "main_mood", "user_settings")) {
				LOG.info("Database column 'user_settings.main_mood' not found. Creating it.");
				template.execute("alter table user_settings add main_mood boolean default false not null");
				LOG.info("Database column 'user_settings.main_mood' was added successfully.");
			}
			
			// Added new Usersettings
			if (!columnExists(template, "playlist_mood", "user_settings")) {
				LOG.info("Database column 'user_settings.playlist_mood' not found. Creating it.");
				template.execute("alter table user_settings add playlist_mood boolean default false not null");
				LOG.info("Database column 'user_settings.playlist_mood' was added successfully.");
			}
            
			if (!columnExists(template, "locked", "user")) {
				template.execute("alter table user add locked boolean default false not null");
				LOG.info("Database table 'user' was updated successfully.");            
   			}

    		// Added new albumartist
        	if (!columnExists(template, "rank", "media_file")) {
        		LOG.info("Database column 'media_file.rank' not found. Creating it.");
        		template.execute("alter table media_file add rank int default 0 not null");
        		LOG.info("Database column 'media_file.rank' was added successfully.");
        	}

			if (!columnExists(template, "comment", "user")) {
				template.execute("alter table user add comment varchar");
				LOG.info("Database table 'user' was updated successfully.");            
			}
          
    		// Update user_settings
          if (!columnExists(template, "button_rank", "user_settings")) {
              LOG.info("Database column 'user_settings.button_rank' not found. Creating it.");
              template.execute("alter table user_settings add button_rank boolean default true not null");
              LOG.info("Database column 'user_settings.button_rank' was added successfully.");
          }

          if (!columnExists(template, "button_starred", "user_settings")) {
              LOG.info("Database column 'user_settings.button_starred' not found. Creating it.");
              template.execute("alter table user_settings add button_starred boolean default true not null");
              LOG.info("Database column 'user_settings.button_starred' was added successfully.");
          }

          if (!columnExists(template, "button_play", "user_settings")) {
              LOG.info("Database column 'user_settings.button_play' not found. Creating it.");
              template.execute("alter table user_settings add button_play boolean default true not null");
              LOG.info("Database column 'user_settings.button_play' was added successfully.");
          }

          if (!columnExists(template, "button_playadd", "user_settings")) {
              LOG.info("Database column 'user_settings.button_playadd' not found. Creating it.");
              template.execute("alter table user_settings add button_playadd boolean default true not null");
              LOG.info("Database column 'user_settings.button_playadd' was added successfully.");
          }
          
          if (!columnExists(template, "button_playmore", "user_settings")) {
              LOG.info("Database column 'user_settings.button_playmore' not found. Creating it.");
              template.execute("alter table user_settings add button_playmore boolean default false not null");
              LOG.info("Database column 'user_settings.button_playmore' was added successfully.");
          }

          if (!columnExists(template, "button_addcontext", "user_settings")) {
              LOG.info("Database column 'user_settings.button_addcontext' not found. Creating it.");
              template.execute("alter table user_settings add button_addcontext boolean default true not null");
              LOG.info("Database column 'user_settings.button_addcontext' was added successfully.");
          }

          if (!columnExists(template, "button_addnext", "user_settings")) {
              LOG.info("Database column 'user_settings.button_addnext' not found. Creating it.");
              template.execute("alter table user_settings add button_addnext boolean default false not null");
              LOG.info("Database column 'user_settings.button_addnext' was added successfully.");
          }

          if (!columnExists(template, "button_addlast", "user_settings")) {
              LOG.info("Database column 'user_settings.button_addlast' not found. Creating it.");
              template.execute("alter table user_settings add button_addlast boolean default false not null");
              LOG.info("Database column 'user_settings.button_addlast' was added successfully.");
          }
          
          if (!columnExists(template, "button_download", "user_settings")) {
              LOG.info("Database column 'user_settings.button_download' not found. Creating it.");
              template.execute("alter table user_settings add button_download boolean default true not null");
              LOG.info("Database column 'user_settings.button_download' was added successfully.");
          }

          if (!columnExists(template, "button_youtube", "user_settings")) {
              LOG.info("Database column 'user_settings.button_youtube' not found. Creating it.");
              template.execute("alter table user_settings add button_youtube boolean default true not null");
              LOG.info("Database column 'user_settings.button_youtube' was added successfully.");
          }

          if (!columnExists(template, "main_disc_number", "user_settings")) {
              LOG.info("Database column 'user_settings.main_disc_number' not found. Creating it.");
              template.execute("alter table user_settings add main_disc_number boolean default false not null");
              LOG.info("Database column 'user_settings.main_disc_number' was added successfully.");
          }

          if (!columnExists(template, "playlist_disc_number", "user_settings")) {
              LOG.info("Database column 'user_settings.playlist_disc_number' not found. Creating it.");
              template.execute("alter table user_settings add playlist_disc_number boolean default false not null");
              LOG.info("Database column 'user_settings.playlist_disc_number' was added successfully.");
          }      
          
		  if (!columnExists(template, "mf_type", "music_folder")) {
			  template.execute("alter table music_folder add mf_type int default 1 not null");
			  LOG.info("Database table 'music_folder' was updated successfully.");            
		  }
		  
		  if (!columnExists(template, "mf_groupby", "music_folder")) {
			  template.execute("alter table music_folder add mf_groupby int default 0 not null");
			  LOG.info("Database table 'music_folder' was updated successfully.");            
		  }
		  
			if (!columnExists(template, "topplay_found", "artist")) {
				LOG.info("Database column 'artist.topplay_found' not found. Creating it.");
				
				template.execute("alter table artist add topplay_found int default 0 not null");
				LOG.info("Database column 'artist.topplay_found' was added successfully.");
			}	
			
			if (!columnExists(template, "topplay_count", "artist")) {
				LOG.info("Database column 'artist.topplay_count' not found. Creating it.");
				
				template.execute("alter table artist add topplay_count int default 0 not null");
				LOG.info("Database column 'artist.topplay_count' was added successfully.");
			}						
    			 
			
			// Update user_settings
		      if (!columnExists(template, "button_rank", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_rank' not found. Creating it.");
		          template.execute("alter table user_settings add button_rank boolean default true not null");
		          LOG.info("Database column 'user_settings.button_rank' was added successfully.");
		      }

		      if (!columnExists(template, "button_starred", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_starred' not found. Creating it.");
		          template.execute("alter table user_settings add button_starred boolean default true not null");
		          LOG.info("Database column 'user_settings.button_starred' was added successfully.");
		      }

		      if (!columnExists(template, "button_play", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_play' not found. Creating it.");
		          template.execute("alter table user_settings add button_play boolean default true not null");
		          LOG.info("Database column 'user_settings.button_play' was added successfully.");
		      }

		      if (!columnExists(template, "button_playadd", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_playadd' not found. Creating it.");
		          template.execute("alter table user_settings add button_playadd boolean default true not null");
		          LOG.info("Database column 'user_settings.button_playadd' was added successfully.");
		      }
		      
		      if (!columnExists(template, "button_playmore", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_playmore' not found. Creating it.");
		          template.execute("alter table user_settings add button_playmore boolean default false not null");
		          LOG.info("Database column 'user_settings.button_playmore' was added successfully.");
		      }

		      if (!columnExists(template, "button_addcontext", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_addcontext' not found. Creating it.");
		          template.execute("alter table user_settings add button_addcontext boolean default true not null");
		          LOG.info("Database column 'user_settings.button_addcontext' was added successfully.");
		      }

		      if (!columnExists(template, "button_addnext", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_addnext' not found. Creating it.");
		          template.execute("alter table user_settings add button_addnext boolean default false not null");
		          LOG.info("Database column 'user_settings.button_addnext' was added successfully.");
		      }

		      if (!columnExists(template, "button_addlast", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_addlast' not found. Creating it.");
		          template.execute("alter table user_settings add button_addlast boolean default false not null");
		          LOG.info("Database column 'user_settings.button_addlast' was added successfully.");
		      }
		      
		      if (!columnExists(template, "button_download", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_download' not found. Creating it.");
		          template.execute("alter table user_settings add button_download boolean default true not null");
		          LOG.info("Database column 'user_settings.button_download' was added successfully.");
		      }

		      if (!columnExists(template, "button_youtube", "user_settings")) {
		          LOG.info("Database column 'user_settings.button_youtube' not found. Creating it.");
		          template.execute("alter table user_settings add button_youtube boolean default true not null");
		          LOG.info("Database column 'user_settings.button_youtube' was added successfully.");
		      }

		      if (!columnExists(template, "main_disc_number", "user_settings")) {
		          LOG.info("Database column 'user_settings.main_disc_number' not found. Creating it.");
		          template.execute("alter table user_settings add main_disc_number boolean default false not null");
		          LOG.info("Database column 'user_settings.main_disc_number' was added successfully.");
		      }

		      if (!columnExists(template, "playlist_disc_number", "user_settings")) {
		          LOG.info("Database column 'user_settings.playlist_disc_number' not found. Creating it.");
		          template.execute("alter table user_settings add playlist_disc_number boolean default false not null");
		          LOG.info("Database column 'user_settings.playlist_disc_number' was added successfully.");
		      }
		      
		        if (!columnExists(template, "view_as", "user_settings")) {
		            LOG.info("Database column 'user_settings.view_as' not found.  Creating it.");
		            template.execute("alter table user_settings add view_as varchar default 'mixed' not null");
		            LOG.info("Database column 'user_settings.view_as' was added successfully.");
		        }		      

        		// Added new offline_count
            	if (!columnExists(template, "offline_count", "playlist")) {
            		LOG.info("Database column 'playlist.offline_count' not found. Creating it.");
            		template.execute("alter table playlist add offline_count int default 0 not null");
            		LOG.info("Database column 'playlist.offline_count' was added successfully.");
            	}
//                template.execute("insert into music_folder values (null, '" + Util.getDefaultMusicFolder() + "', 'Music', true, now() ,1 , 1, 1)");
        }  
   	}            
    
    @SuppressWarnings("deprecation")
	private void createAvatar(JdbcTemplate template, String avatar) {
            if (template.queryForInt("select count(*) from system_avatar where name = ?", new Object[]{avatar}) == 0) {

                InputStream in = null;
                try {
                    in = getClass().getResourceAsStream(avatar + ".png");
                    byte[] imageData = IOUtils.toByteArray(in);
                    template.update("insert into system_avatar values (null, ?, ?, ?, ?, ?, ?)",
                                    new Object[]{avatar, new Date(), "image/png", 48, 48, imageData});
                    LOG.info("Created avatar '" + avatar + "'.");
                } catch (IOException x) {
                    LOG.error("Failed to create avatar '" + avatar + "'.", x);
                } finally {
                    IOUtils.closeQuietly(in);
                }
            }
        }            
            
    }

