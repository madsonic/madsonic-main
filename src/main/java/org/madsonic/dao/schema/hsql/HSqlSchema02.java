/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao.schema.hsql;

import org.springframework.jdbc.core.JdbcTemplate;
import org.madsonic.Logger;
import org.madsonic.dao.schema.Schema;

/**
 * Used for creating and evolving the database schema.
 * This class implements the database schema for Madsonic version 6.0.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class HSqlSchema02 extends Schema {

    private static final Logger LOG = Logger.getLogger(HSqlSchema02.class);

    @SuppressWarnings("deprecation")
	@Override
    public void execute(JdbcTemplate template) {

        if (template.queryForInt("select count(*) from version where version = 202") == 0) {
            LOG.info("Updating database schema to version 202.");
            template.execute("insert into version values (202)");
        }

        if (!rowExists(template, "table_name='PODCAST_EPISODE' and column_name='URL' and ordinal_position=1", "information_schema.system_indexinfo")) {
            template.execute("create index idx_podcast_episode_url on podcast_episode(url)");
            LOG.info("Created index for podcast_episode.url");
        }

        if (!tableExists(template, "audio_ad")) {
            LOG.info("Database table 'audio_ad' not found.  Creating it.");
            template.execute("create table audio_ad (" +
                             "id identity," +
                             "media_file_id int," +
                             "weight double," +
                             "comment varchar," +
                             "enabled boolean default false not null," +
                             "created datetime not null," +
                             "changed datetime not null," +
                             "foreign key (media_file_id) references media_file(id) on delete cascade)");
            LOG.info("Database table 'audio_ad' was created successfully.");
        }

        if (!tableExists(template, "play_queue")) {
            LOG.info("Database table 'play_queue' not found.  Creating it.");
            template.execute("create table play_queue (" +
                             "id identity," +
                             "username varchar not null," +
                             "current int," +
                             "position_millis bigint," +
                             "changed datetime not null," +
                             "changed_by varchar not null," +
                             "foreign key (username) references user(username) on delete cascade)");
            LOG.info("Database table 'play_queue' was created successfully.");
        }

        if (!tableExists(template, "play_queue_file")) {
            LOG.info("Database table 'play_queue_file' not found.  Creating it.");
            template.execute("create cached table play_queue_file (" +
                             "id identity," +
                             "sequence_id int not null," +
                             "play_queue_id int not null," +
                             "media_file_id int not null," +
                             "foreign key (play_queue_id) references play_queue(id) on delete cascade," +
                             "foreign key (media_file_id) references media_file(id) on delete cascade)");
            LOG.info("Database table 'play_queue_file' was created successfully.");
        }        

        if (!tableExists(template, "music_folder_tasks")) {
            LOG.info("Database table 'music_folder_tasks' not found.  Creating it.");
            template.execute("create cached table music_folder_tasks (" +
                             "id identity," +
                             "music_folder_id int not null," +
                             "found int default 0 not null," +
                             "scan datetime not null," +
                             "status varchar," +
                             "result varchar," +
                             "comment varchar," +
                             "enabled boolean default true not null," +
                             "foreign key (music_folder_id) references music_folder(id) on delete cascade)");
            LOG.info("Database table 'music_folder_tasks' was created successfully.");
        }        
        
        if (template.queryForInt("select count(*) from version where version = 205") == 0) {
            LOG.info("Updating database schema to version 205.");
            template.execute("insert into version values (205)");
    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='001') where avatar_scheme='NONE'");
    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='010') where username='admin'");               
    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='001') where username='guest'");            
    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='003') where username='demo'");
    		LOG.info("Avatar templates updated successfully.");        
        }
    		
			// Update user_settings
		      if (!columnExists(template, "main_bpm", "user_settings")) {
		          LOG.info("Database column 'user_settings.main_bpm' not found. Creating it.");
		          template.execute("alter table user_settings add main_bpm boolean default false not null");
		          LOG.info("Database column 'user_settings.main_bpm' was added successfully.");
		      }    		
    		
				// Update user_settings
		      if (!columnExists(template, "playlist_bpm", "user_settings")) {
		          LOG.info("Database column 'user_settings.playlist_bpm' not found. Creating it.");
		          template.execute("alter table user_settings add playlist_bpm boolean default false not null");
		          LOG.info("Database column 'user_settings.playlist_bpm' was added successfully.");
		      }    		

				// Update user_settings
		      if (!columnExists(template, "main_composer", "user_settings")) {
		          LOG.info("Database column 'user_settings.main_composer' not found. Creating it.");
		          template.execute("alter table user_settings add main_composer boolean default false not null");
		          LOG.info("Database column 'user_settings.main_composer' was added successfully.");
		      }    		
    		
				// Update user_settings
		      if (!columnExists(template, "playlist_composer", "user_settings")) {
		          LOG.info("Database column 'user_settings.playlist_composer' not found. Creating it.");
		          template.execute("alter table user_settings add playlist_composer boolean default false not null");
		          LOG.info("Database column 'user_settings.playlist_composer' was added successfully.");
		      }   		      
		      
		      if (!columnExists(template, "image_url", "podcast_channel")) {
		            LOG.info("Database column 'podcast_channel.image_url' not found.  Creating it.");
		            template.execute("alter table podcast_channel add image_url varchar");
		            LOG.info("Database column 'podcast_channel.image_url' was added successfully.");
      		  }

		        if (!columnExists(template, "show_artist_info", "user_settings")) {
		            LOG.info("Database column 'user_settings.show_artist_info' not found.  Creating it.");
		            template.execute("alter table user_settings add show_artist_info boolean default true not null");
		            LOG.info("Database column 'user_settings.show_artist_info' was added successfully.");
		        }		      
		      
		        if (!columnExists(template, "song_notification", "user_settings")) {
		            LOG.info("Database column 'user_settings.song_notification' not found.  Creating it.");
		            template.execute("alter table user_settings add song_notification boolean default true not null");
		            LOG.info("Database column 'user_settings.song_notification' was added successfully.");
		        }	
		        
		        if (template.queryForInt("select count(*) from user_group where name = 'LDAP'") == 0) {
		            LOG.info("Group 'LDAP' not found in database. Creating it.");
		   			template.execute("insert into user_group (id, name, video_default_bitrate, video_max_bitrate) values (NULL, 'LDAP', 1000, 2000)");
					template.execute("insert into user_group_access (user_group_id, music_folder_id, enabled) (select distinct user_group.id as user_group_id, music_folder.id as music_folder_id, false as enabled from user_group, music_folder where user_group.name = 'LDAP')");
		        }

		        if (template.queryForInt("select count(*) from user_group where name = 'SIGNUP'") == 0) {
		            LOG.info("Group 'SIGNUP' not found in database. Creating it.");
	       			template.execute("insert into user_group (id, name, video_default_bitrate, video_max_bitrate) values (NULL, 'SIGNUP', 1000, 2000)");
					template.execute("insert into user_group_access (user_group_id, music_folder_id, enabled) (select distinct user_group.id as user_group_id, music_folder.id as music_folder_id, false as enabled from user_group, music_folder where user_group.name = 'SIGNUP')");
		        }
		        
                if (!rowExists(template, "table_name='PODCAST_EPISODE' and column_name='URL' and ordinal_position=1", "information_schema.system_indexinfo")) {
                    template.execute("create index idx_podcast_episode_url on podcast_episode(url)");
                    LOG.info("Created index for podcast_episode.url");
                }

                if (!columnExists(template, "image_url", "podcast_channel")) {
                    LOG.info("Database column 'podcast_channel.image_url' not found.  Creating it.");
                    template.execute("alter table podcast_channel add image_url varchar");
                    LOG.info("Database column 'podcast_channel.image_url' was added successfully.");
                }                
                
		        if (template.queryForInt("select count(*) from role where name = 'image'") == 0) {
		            LOG.info("new roles not found in database. Creating it.");
	                template.execute("insert into role values (NULL, 'image')");
		        }
		        
		        if (template.queryForInt("select count(*) from role where name = 'video'") == 0) {
		            LOG.info("new roles not found in database. Creating it.");
	                template.execute("insert into role values (NULL, 'video')");
		        }		        
		        
                if (!columnExists(template, "approved", "user")) {
                    LOG.info("Database column 'user.approved' not found. Creating it.");
                    template.execute("alter table user add approved boolean default false not null");
                    LOG.info("Database column 'user.approved' was added successfully.");
                    template.execute("update user set approved=true");
                    LOG.info("Existing user updated successfully.");
                }
                
                if (!columnExists(template, "button_lyric", "user_settings")) {
                    LOG.info("Database column 'user_settings.button_lyric' not found. Creating it.");
                    template.execute("alter table user_settings add button_lyric boolean default false not null");
                    LOG.info("Database column 'user_settings.button_lyric' was added successfully.");
                }              
                
//                if (!columnExists(template, "show_side_bar", "user_settings")) {
//                    LOG.info("Database column 'user_settings.show_side_bar' not found.  Creating it.");
//                    template.execute("alter table user_settings add show_side_bar boolean default true not null");
//                    LOG.info("Database column 'user_settings.show_side_bar' was added successfully.");
//                }     
//                
//                if (!columnExists(template, "show_right_bar", "user_settings")) {
//                    LOG.info("Database column 'user_settings.show_right_bar' not found.  Creating it.");
//                    template.execute("alter table user_settings add show_right_bar boolean default false not null");
//                    LOG.info("Database column 'user_settings.show_right_bar' was added successfully.");
//                }
                
                if (template.queryForInt("select count(*) from version where version = 206") == 0) {
                    LOG.info("Updating database schema to version 206.");
                    template.execute("insert into version values (206)");

                    LOG.info("Deleting obsolete video transcodings.");
                    template.execute("delete from transcoding2 where name in ('flv/h264 video', 'mkv video')");
                }

                if (!tableExists(template, "video_conversion")) {
                    LOG.info("Database table 'video_conversion' not found.  Creating it.");
                    template.execute("create table video_conversion (" +
                                     "id identity," +
                                     "media_file_id int not null," +
                                     "audio_track_id int," +
                                     "username varchar not null," +
                                     "status varchar not null," +
                                     "progress_seconds int," +
                                     "created datetime not null," +
                                     "changed datetime not null," +
                                     "started datetime," +
                                     "foreign key (media_file_id) references media_file(id) on delete cascade)");

                    template.execute("create index idx_video_conversion_media_file_id on video_conversion(media_file_id)");
                    template.execute("create index idx_video_conversion_status on video_conversion(status)");

                    LOG.info("Database table 'video_conversion' was created successfully.");
                }
                
                if (!columnExists(template, "target_file", "video_conversion")) {
                    template.execute("alter table video_conversion add target_file varchar");
                    template.execute("alter table video_conversion add log_file varchar");
                    template.execute("alter table video_conversion add bit_rate int");
                }                

                if (!tableExists(template, "audio_conversion")) {
                    LOG.info("Database table 'audio_conversion' not found.  Creating it.");
                    template.execute("create table audio_conversion (" +
                                     "id identity," +
                                     "media_file_id int not null," +
                                     "username varchar not null," +
                                     "status varchar not null," +
                                     "target_file varchar," +
                                     "target_format varchar," +
                                     "log_file varchar," +
                                     "bit_rate int," +
                                     "progress_seconds int," +
                                     "created datetime not null," +
                                     "changed datetime not null," +
                                     "started datetime," +
                                     "foreign key (media_file_id) references media_file(id) on delete cascade)");

                    template.execute("create index idx_audio_conversion_media_file_id on audio_conversion(media_file_id)");
                    template.execute("create index idx_audio_conversion_status on audio_conversion(status)");

                    LOG.info("Database table 'audio_conversion' was created successfully.");
                }                
                
				// cleanup typo
                if (columnExists(template, "bitrate", "audio_conversion")) {
                    template.execute("alter table audio_conversion drop bitrate");
                }                
                
                if (!columnExists(template, "target_file", "audio_conversion")) {
                    template.execute("alter table audio_conversion add target_file varchar");
                    template.execute("alter table audio_conversion add log_file varchar");
                    template.execute("alter table audio_conversion add bit_rate int");
                }                

                if (!columnExists(template, "target_format", "audio_conversion")) {
                    template.execute("alter table audio_conversion add target_format varchar");
                }                
                
                if (template.queryForInt("select count(*) from role where id = 16") == 0) {
                    LOG.info("Role 'video_conversion' not found in database. Creating it.");
                    template.execute("insert into role values (16, 'video_conversion')");
                    template.execute("insert into user_role " +
                                     "select distinct u.username, 16 from user u, user_role ur " +
                                     "where u.username = ur.username and ur.role_id = 1");
                    LOG.info("Role 'video_conversion' was created successfully.");
                }
  
                if (template.queryForInt("select count(*) from role where id = 17") == 0) {
                    LOG.info("Role 'audio_conversion' not found in database. Creating it.");
                    template.execute("insert into role values (17, 'audio_conversion')");
                    template.execute("insert into user_role " +
                                     "select distinct u.username, 17 from user u, user_role ur " +
                                     "where u.username = ur.username and ur.role_id = 1");
                    LOG.info("Role 'audio_conversion' was created successfully.");
                }    
                
                if (!tableExists(template, "nodes")) {
                    LOG.info("Database table 'nodes' not found.  Creating it.");
                    template.execute("create table nodes (" +
                                     "id identity," +
                                     "url varchar," +
                                     "name varchar," +
                                     "online boolean default false not null," +
                                     "enabled boolean default false not null)") ;
                    
                    LOG.info("Database table 'nodes' was created successfully.");
                }                
                
                if (!columnExists(template, "show_left_bar", "user_settings")) {
                    LOG.info("Database column 'user_settings.show_left_bar' not found.  Creating it.");
                    template.execute("alter table user_settings add show_left_bar boolean default false not null");
                    LOG.info("Database column 'user_settings.show_left_bar' was added successfully.");
                }                
                
                if (!tableExists(template, "loved_media_file")) {
                    LOG.info("Database table 'loved_media_file' not found. Creating it.");
                    template.execute("create table loved_media_file (" +
                            "id identity," +
                            "media_file_id int not null," +
                            "username varchar not null," +
                            "created datetime not null," +
                            "foreign key (media_file_id) references media_file(id) on delete cascade,"+
                            "foreign key (username) references user(username) on delete cascade," +
                            "unique (media_file_id, username))");

                    template.execute("create index idx_loved_media_file_media_file_id on starred_media_file(media_file_id)");
                    template.execute("create index idx_loved_media_file_username on loved_media_file(username)");

                    LOG.info("Database table 'loved_media_file' was created successfully.");
                }   
                
//                if (template.queryForInt("select count(*) from version where version = 207") == 0) {
//                    LOG.info("Updating database schema to version 207.");
//                    template.execute("insert into version values (207)");

                    if (columnExists(template, "autohide_chat", "user_settings")) {
                    	template.execute("alter table user_settings drop column autohide_chat");
                        LOG.info("Deleting obsolete usersettings autohide_chat.");
                    }
                    
                    if (columnExists(template, "list_rows", "user_settings")) {
                    	template.execute("alter table user_settings drop column list_rows");
                        LOG.info("Deleting obsolete usersettings list_rows.");
                    }

                    if (columnExists(template, "list_columns", "user_settings")) {
                    	template.execute("alter table user_settings drop column list_columns");
                        LOG.info("Deleting obsolete usersettings list_columns.");
                    }  
                    
                    if (columnExists(template, "show_right_bar", "user_settings")) {
                    	template.execute("alter table user_settings drop column show_right_bar");
                        LOG.info("Deleting obsolete usersettings show_right_bar.");
                    }                       

                    if (columnExists(template, "show_side_bar", "user_settings")) {
                    	template.execute("alter table user_settings drop column show_side_bar");
                        LOG.info("Deleting obsolete usersettings show_side_bar.");
                    }

                    if (!columnExists(template, "show_left_shrinked", "user_settings")) {
                        LOG.info("Database column 'user_settings.show_left_shrinked' not found.  Creating it.");
                        template.execute("alter table user_settings add show_left_shrinked boolean default false not null");
                        LOG.info("Database column 'user_settings.show_left_shrinked' was added successfully.");
                      }                    
                    
                    if (!columnExists(template, "show_left_panel", "user_settings")) {
                        LOG.info("Database column 'user_settings.show_left_panel' not found.  Creating it.");
                        template.execute("alter table user_settings add show_left_panel boolean default false not null");
                        LOG.info("Database column 'user_settings.show_left_panel' was added successfully.");
                      }                    
                    
                    if (!columnExists(template, "show_side_panel", "user_settings")) {
                      LOG.info("Database column 'user_settings.show_side_panel' not found.  Creating it.");
                      template.execute("alter table user_settings add show_side_panel boolean default true not null");
                      LOG.info("Database column 'user_settings.show_side_panel' was added successfully.");
                    }                     

                    if (!columnExists(template, "show_right_panel", "user_settings")) {
                      LOG.info("Database column 'user_settings.show_right_panel' not found.  Creating it.");
                      template.execute("alter table user_settings add show_right_panel boolean default false not null");
                      LOG.info("Database column 'user_settings.show_right_panel' was added successfully.");
                    }  

                    if (!columnExists(template, "button_loved", "user_settings")) {
                        LOG.info("Database column 'user_settings.button_loved' not found. Creating it.");
                        template.execute("alter table user_settings add button_loved boolean default false not null");
                        LOG.info("Database column 'user_settings.button_loved' was added successfully.");
                    }                     
                    
//                }                
                
                
    }
}
