/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao.schema.mysql;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.base.Splitter;

import org.madsonic.Logger;
import org.madsonic.dao.schema.Schema;
import org.madsonic.util.StringUtil;

/**
 * Used for creating and evolving the database schema.
 * This class implementes the database schema for MySQL.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MySqlSchema01 extends Schema {
    private static final Logger LOG = Logger.getLogger(MySqlSchema01.class);

    @SuppressWarnings("deprecation")
	public void execute(JdbcTemplate template) throws Exception {
        if (!tableExists(template, "version")) {
            LOG.info("Database table 'version' not found.  Creating it.");
            template.execute("create table version (version int not null)");
            template.execute("insert into version values (200)");
            LOG.info("Database table 'version' was created successfully.");

            InputStream in = getClass().getResourceAsStream("mysql_schema_01.sql");
            String statements = IOUtils.toString(in, StringUtil.ENCODING_UTF8);
            for (String statement : Splitter.on(";").omitEmptyStrings().trimResults().split(statements)) {
                template.execute(statement);
                LOG.info(statement);
            }
        }
        
        if (template.queryForInt("select count(*) from version where version = 205") == 0) {
            LOG.info("Updating database schema to version 205.");
            template.execute("insert into version values (205)");

    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='001') where avatar_scheme='NONE'");
    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='010') where username='admin'");               
    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='002') where username='guest'");            
    		template.execute("update user_settings set avatar_scheme='SYSTEM', system_avatar_id=(select id from system_avatar where name='004') where username='demo'"); 
    		LOG.info("Avatar templates updated successfully.");        
            
        }        
        
    }
}
