/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.madsonic.domain.AudioConversion;

/**
 * Provides database services for audio conversions.
 *
 * @author Martin Karel
 */
public class AudioConversionDao extends AbstractDao {
 
    private static final String COLUMNS = "id, media_file_id, username, status, target_file, target_format, " +
                                          "log_file, bit_rate, progress_seconds, created, changed, started";

    private AudioConversionRowMapper rowMapper = new AudioConversionRowMapper();

    public synchronized void createAudioConversion(AudioConversion conversion) {
       
        update("insert into audio_conversion (" + COLUMNS + ") values (" + questionMarks(COLUMNS) + ")", null,
               conversion.getMediaFileId(), conversion.getUsername(), conversion.getStatus().name(),
               conversion.getTargetFile(), conversion.getTargetFormat(), conversion.getLogFile(), conversion.getBitRate(), conversion.getProgressSeconds(), conversion.getCreated(), conversion.getChanged(), conversion.getStarted());
    }

    public synchronized void updateProgress(Integer id, Integer progressSeconds) {
        update("update audio_conversion set progress_seconds=? where id=?", progressSeconds, id);
    }

    public synchronized void updateStatus(Integer id, AudioConversion.Status status) {
        Date changed = new Date();
        Date started = status == AudioConversion.Status.IN_PROGRESS ? changed : null;
        update("update audio_conversion set status=?, changed=?, started=? where id=?", status.name(), changed, started, id);
    }

    public synchronized AudioConversion getAudioConversionForFile(int mediaFileId) {
        return queryOne("select " + COLUMNS + " from audio_conversion where media_file_id=? order by created desc",
                        rowMapper, mediaFileId);
    }

    public synchronized List<AudioConversion> getAllAudioConversions() {
        return query("select " + COLUMNS + " from audio_conversion order by created desc", rowMapper);
    }

    public synchronized void deleteAudioConversionsForFile(Integer mediaFileId) {
        update("delete from audio_conversion where media_file_id=?", mediaFileId);
    }

    public synchronized void deleteAudioConversion(int id) {
        update("delete from audio_conversion where id=?", id);
    }

    public synchronized AudioConversion getAudioConversionById(Integer id) {
        return queryOne("select " + COLUMNS + " from audio_conversion where id=?", rowMapper, id);
    }

    public synchronized List<AudioConversion> getAudioConversionsByStatus(AudioConversion.Status status) {
        return query("select " + COLUMNS + " from audio_conversion where status=? order by created",
                     rowMapper, status.name());
    }

    private static class AudioConversionRowMapper implements ParameterizedRowMapper<AudioConversion> {
        public AudioConversion mapRow(ResultSet rs, int rowNum) throws SQLException {

            Integer bitRate = rs.getInt(8);
            if (bitRate == 0) {
                bitRate = null;
            }

            return new AudioConversion(rs.getInt(1), rs.getInt(2), rs.getString(3), AudioConversion.Status.valueOf(rs.getString(4)),
            						   rs.getString(5), rs.getString(6), rs.getString(7), bitRate, rs.getInt(9), rs.getTimestamp(10), rs.getTimestamp(11), rs.getTimestamp(12));
        }
    }
}
