/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import org.madsonic.Logger;
import org.madsonic.domain.Transcoding;

/**
 * Provides database services for transcoding configurations.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class TranscodingDao extends AbstractDao {

    private static final Logger LOG = Logger.getLogger(TranscodingDao.class);
    private static final String COLUMNS = "id, name, source_formats, target_format, step1, step2, step3, default_active";
    private TranscodingRowMapper rowMapper = new TranscodingRowMapper();

    /**
     * Returns all transcodings.
     *
     * @return Possibly empty list of all transcodings.
     */
    public List<Transcoding> getAllTranscodings() {
        String sql = "select " + COLUMNS + " from transcoding2";
        return query(sql, rowMapper);
    }

    /**
     * Returns all active transcodings for the given player.
     *
     * @param playerId The player ID.
     * @return All active transcodings for the player.
     */
    public List<Transcoding> getTranscodingsForPlayer(String playerId) {
        String sql = "select " + COLUMNS + " from transcoding2, player_transcoding2 " +
                "where player_transcoding2.player_id = ? " +
                "and   player_transcoding2.transcoding_id = transcoding2.id";
        return query(sql, rowMapper, playerId);
    }

    /**
     * Sets the list of active transcodings for the given player.
     *
     * @param playerId       The player ID.
     * @param transcodingIds ID's of the active transcodings.
     */
    public void setTranscodingsForPlayer(String playerId, int[] transcodingIds) {
        update("delete from player_transcoding2 where player_id = ?", playerId);
        String sql = "insert into player_transcoding2(player_id, transcoding_id) values (?, ?)";
        for (int transcodingId : transcodingIds) {
            update(sql, playerId, transcodingId);
        }
    }

    /**
     * Creates a new transcoding.
     *
     * @param transcoding The transcoding to create.
     */
    @SuppressWarnings("deprecation")
	public synchronized void createTranscoding(Transcoding transcoding) {
        int id = getJdbcTemplate().queryForInt("select max(id) + 1 from transcoding2");
        transcoding.setId(id);
        String sql = "insert into transcoding2 (" + COLUMNS + ") values (" + questionMarks(COLUMNS) + ")";
        update(sql, transcoding.getId(), transcoding.getName(), transcoding.getSourceFormats(),
                transcoding.getTargetFormat(), transcoding.getStep1(),
                transcoding.getStep2(), transcoding.getStep3(), transcoding.isDefaultActive());
        LOG.info("Created transcoding " + transcoding.getName());
    }

    /**
     * Deletes the transcoding with the given ID.
     *
     * @param id The transcoding ID.
     */
    public void deleteTranscoding(Integer id) {
        String sql = "delete from transcoding2 where id=?";
        update(sql, id);
        LOG.info("Deleted transcoding with ID " + id);
    }

    public void reset2Subsonic() {
        update("delete from transcoding2");
        update("delete from player_transcoding2");
        update("insert into transcoding2(name, source_formats, target_format, step1) values('mp3 audio','ogg oga aac m4a flac wav wma aif aiff ape mpc shn', 'mp3', 'ffmpeg -i %s -map 0:0 -b:a %bk -v 0 -f mp3 -')");
        update("insert into player_transcoding2(player_id, transcoding_id) select distinct p.id, t.id from player p, transcoding2 t");
        LOG.info("##Transcoding reseted to Subsonic defaults");
    }    

    public void reset2MadsonicDefault() {
        update("delete from transcoding2");
        update("delete from player_transcoding2");
        update("insert into transcoding2(name, source_formats, target_format, step1) values('hdrun->flv', 'hdrun', 'flv', 'ffmpeg -ss %o -i %z -c:v libx264 -preset superfast -async 1 -b %bk -s %wx%h -ar 44100 -ac 2 -v 0 -f flv -threads 0 -')");        
        update("insert into transcoding2(name, source_formats, target_format, step1) values('wtv->flv', 'wtv', 'flv', 'ffmpeg -ss %o -i %s -c:v libx264 -preset fast -async 30 -b %bk -r 23-.976 -s 720x360 -ar 44100 -ac 2 -v 0 -f flv -threads 0 -')");
        update("insert into transcoding2(name, source_formats, target_format, step1) values('tv->flv', 'tv', 'flv', 'ffmpeg -ss %o -i %p -c:v libx264 -preset superfast -async 1 -b %bk -s %wx%h -ar 44100 -ac 2 -v 0 -f flv -threads 0 -')");
        update("insert into transcoding2(name, source_formats, target_format, step1) values('audio->mp3', 'aac aif aiff ape dff dsf flac m4a mpc ogg oga opus shn wav wma', 'mp3', 'ffmpeg -i %s -map 0:a:0 -b:a %bk -v 0 -f mp3 -')");        
        update("insert into transcoding2(name, source_formats, target_format, step1) values('mod->mp3', '669 alm amd amf dbm dmf emod far flx fnk gdm gmc gtk hsc imf it j2b liq m15 mdl med mgt mod mtm mtn mtp okt psm ptm rad rtm s3m sfx stm stx ult umx wow xm', 'mp3', 'ffmpeg -i %s -map 0:a:0 -b:a %bk -v 0 -f mp3 -')");
       
        update("insert into player_transcoding2(player_id, transcoding_id) select distinct p.id, t.id from player p, transcoding2 t");
        LOG.info("##Transcoding reseted to Madsonic defaults");
    }    
    
    /**
     * Updates the given transcoding.
     *
     * @param transcoding The transcoding to update.
     */
    public void updateTranscoding(Transcoding transcoding) {
        String sql = "update transcoding2 set name=?, source_formats=?, target_format=?, step1=?, step2=?, step3=?, default_active=? where id=?";
        update(sql, transcoding.getName(), transcoding.getSourceFormats(),
                transcoding.getTargetFormat(), transcoding.getStep1(), transcoding.getStep2(),
                transcoding.getStep3(), transcoding.isDefaultActive(), transcoding.getId());
    }

    private static class TranscodingRowMapper implements ParameterizedRowMapper<Transcoding> {
        public Transcoding mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Transcoding(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
                    rs.getString(6), rs.getString(7), rs.getBoolean(8));
        }
    }
}
