/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.madsonic.domain.HotFlag;
import org.madsonic.domain.MediaFile;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

/**
 * Provides database services for hot ratings.
 *
 * @author Martin Karel
 */
public class HotDao extends AbstractDao {

    private static final String COLUMNS = "username, path, id";
    private final HotRowMapper rowMapper = new HotRowMapper();
    
	/**
     * Sets the hot rating for a media file and a given user.
     *
     * @param username  The user name.
     * @param mediaFile The media file.
     */
    public void setHotFlag(String username, MediaFile mediaFile) {

        update("delete from hot_rating where username=? and path=?", username, mediaFile.getPath());
        update("insert into hot_rating values(?, ?, ?)", username, mediaFile.getPath(), mediaFile.getId());
    }
    /**
     * 
     * @param username The user name
     * @param mediaFile The media file.
     * @return int value of mediaFile default 0
     */
    public int getHotFlag(String username, MediaFile mediaFile) {
        String sql = "select id from hot_rating where username=? and path=?";
        return queryForInt(sql, 0, username, mediaFile.getPath());
    }
	
    public void deleteHotFlag(MediaFile mediaFile) {
      update("delete from hot_rating where path=?", mediaFile.getPath());
    }

	public int getCountHotFlag() {
	    String sql = "select count(*) from hot_rating";
	    return queryForInt(sql, 0);
	}   
    
    
    /**
     * Returns paths for the highest rated music files.
     *
     * @param offset Number of files to skip.
     * @param count  Maximum number of files to return.
     * @return Paths for the highest rated music files.
     */
    public List<String> getHotRated(int offset, int count) {
        if (count < 1) {
            return new ArrayList<String>();
        }
        String sql = "select path from hot_rating group by path " +
                "order by path desc limit " + count + " offset " + offset;
        return queryForStrings(sql);
    }    

    public List<String> getRandomHotRated(int offset, int count) {
        if (count < 1) {
            return new ArrayList<String>();
        }

        String sql = "select path from hot_rating group by path " +
                " limit " + count + " offset " + offset;
        return queryForStrings(sql);
    }  

    private static class HotRowMapper implements ParameterizedRowMapper<HotFlag> {
        public HotFlag mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new HotFlag(rs.getString(1), rs.getString(2), rs.getInt(3));
        }
    }
	
}
