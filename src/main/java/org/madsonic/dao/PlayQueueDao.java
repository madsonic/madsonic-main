/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import org.madsonic.domain.SavedPlayQueue;

/**
 * Provides database services for play queues
 *
 * @author Sindre Mehus, Martin Karel
 */
public class PlayQueueDao extends AbstractDao {

    private static final String COLUMNS = "id, username, current, position_millis, changed, changed_by";
    private final RowMapper<SavedPlayQueue> rowMapper = new PlayQueueMapper();

    public synchronized SavedPlayQueue getPlayQueue(String username) {
        SavedPlayQueue playQueue = queryOne("select * from play_queue where username = ?", rowMapper, new Object[]{username});
        if (playQueue == null) {
            return null;
        }
        List<Integer> mediaFileIds = queryForInts("select media_file_id from play_queue_file where play_queue_id = ? order by sequence_id", playQueue.getId());
        playQueue.setMediaFileIds(mediaFileIds);
        return playQueue;
    }

    public synchronized void savePlayQueue(SavedPlayQueue playQueue) {
    	getJdbcTemplate().update("delete from play_queue where username=?", playQueue.getUsername());
    	getJdbcTemplate().update("insert into play_queue(" + COLUMNS + ") values (" + questionMarks(COLUMNS) + ")",
               null, playQueue.getUsername(), playQueue.getCurrentMediaFileId(), playQueue.getPositionMillis(),
               playQueue.getChanged(), playQueue.getChangedBy());
        int id = queryForInt("select max(id) from play_queue", 0);
        playQueue.setId(id);

    	int sequence = 0;
        for (Integer mediaFileId : playQueue.getMediaFileIds()) {
            sequence++;
            getJdbcTemplate().update("insert into play_queue_file(sequence_id, play_queue_id, media_file_id) values (?, ?, ?)",sequence, id, mediaFileId);
        }
    }

    private static class PlayQueueMapper implements ParameterizedRowMapper<SavedPlayQueue> {
        public SavedPlayQueue mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new SavedPlayQueue(rs.getInt(1),
                                      rs.getString(2),
                                      null,
                                      rs.getInt(3),
                                      rs.getLong(4),
                                      rs.getTimestamp(5),
                                      rs.getString(6));
        }
    }
}
