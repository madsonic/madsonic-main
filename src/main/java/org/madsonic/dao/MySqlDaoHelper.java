/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import javax.sql.DataSource;

import org.madsonic.Logger;
import org.madsonic.dao.schema.Schema;
import org.madsonic.dao.schema.mysql.MySqlSchema01;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.apache.commons.dbcp.BasicDataSource;
/**
 * DAO helper class which creates the data source, and updates the database schema.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MySqlDaoHelper implements DaoHelper {

    private static final Logger LOG = Logger.getLogger(MySqlDaoHelper.class);
    private final String jdbcUrl;

    private Schema[] schemas = {new MySqlSchema01()};
    
    private DataSource dataSource;

    public MySqlDaoHelper(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
        dataSource = createDataSource();
        checkDatabase();
    }

    /**
     * Returns a JDBC template for performing database operations.
     *
     * @return A JDBC template.
     */
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    private DataSource createDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl(jdbcUrl);
        ds.setUsername("root");
        ds.setPassword("");

        return ds;
    }

    private void checkDatabase() {
        LOG.info("Checking MySQL database schema.");
        try {
            for (Schema schema : schemas) {
                schema.execute(getJdbcTemplate());
            }
            LOG.info("Done checking MySQL database schema.");
        } catch (Exception x) {
            LOG.error("Failed to initialize MySQL database.", x);
        }
    }
}
