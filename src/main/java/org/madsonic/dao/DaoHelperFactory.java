/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.dao;

import org.apache.commons.lang.StringUtils;

import org.madsonic.Logger;

/**
 * @author Martin Karel
 * @version $Id$
 */
public class DaoHelperFactory {

    private static final Logger LOG = Logger.getLogger(DaoHelperFactory.class);

    public static DaoHelper create() {
        String jdbcUrl = StringUtils.trimToNull(System.getProperty("madsonic.db"));

        // Madsonic internal HSQLDB Connector
        if (jdbcUrl == null) {
            return new HsqlDaoHelper();
        }

        // Madsonic external MySQL Connector
        if (jdbcUrl.contains("mysql")) {
            return new MySqlDaoHelper(jdbcUrl);
        }

        // Madsonic external PostgreSQL Connector
        if (jdbcUrl.contains("postgres")) {
            return new PostgreSqlDaoHelper(jdbcUrl);
        }
        
        LOG.error("Unsupported JDBC url:" + jdbcUrl + ". Reverting to HSQL. Check system property 'madsonic.db'.");
        return new HsqlDaoHelper();
    }
}
