/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import net.sf.ehcache.Ehcache;

import org.madsonic.Logger;
import org.madsonic.dao.AccessRightDao;
import org.madsonic.dao.GroupDao;
import org.madsonic.dao.UserDao;
import org.madsonic.domain.AccessGroup;
import org.madsonic.domain.AccessToken;
import org.madsonic.domain.AvatarScheme;
import org.madsonic.domain.Group;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MusicFolder;
import org.madsonic.domain.TranscodeScheme;
import org.madsonic.domain.User;
import org.madsonic.domain.UserSettings;
import org.madsonic.util.FileUtil;


/**
 * Provides security-related services for authentication and authorization.
 *
 * @author Sindre Mehus, Martin Karel
 */

@SuppressWarnings("deprecation")
public class SecurityService implements InitializingBean, UserDetailsService {

    private static final Logger LOG = Logger.getLogger(SecurityService.class);

    private GroupDao groupDao;
    private AccessRightDao accessRightDao;
    private UserDao userDao;
    private SettingsService settingsService;
    private Ehcache userCache;

    private PasswordEncoder passwordEncoder;  
    private SaltSource saltSource;
    
	/**
	 * After instantiation, check if this is an old Madsonic instance, still
	 * storing passwords in clear-text. If so, update to salted hashing.
	 */
	public void afterPropertiesSet() throws Exception {
		JdbcTemplate template = userDao.getJdbcTemplate();
		
		if (template.queryForInt("select count(*) from version where version = 201") == 0) {
            LOG.info("Updating database schema to version 201. (securing user passwords)");
            template.execute("insert into version values (201)");

    		for (User user : getAllUsers()) {
    			setSecureUserToken(user);
    			setSecurePassword(user);
    			updateUser(user);
    		}
		}
	}
	public void updateSecurePassword(User user) {
		setSecureUserToken(user);		
		setSecurePassword(user);
		updateUser(user);
	}
	
	public void setSecurePassword(User user) {
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<GrantedAuthority>());
		String encodedPassword = passwordEncoder.encodePassword(userDetails.getPassword(), saltSource.getSalt(userDetails));
		user.setPassword(encodedPassword);
	}
	
	public void setSecureUserToken(User user) {
		userDao.updateToken(user);
	}
		
	public String getSecureUserToken(String username) {
		return userDao.getUserToken(username);
	}
	
    /**
     * Locates the user based on the username.
     *
     * @param username The username presented to the {@link DaoAuthenticationProvider}
     * @return A fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no GrantedAuthority.
     * @throws DataAccessException       If user could not be found for a repository-specific reason.
     */
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {

    	boolean unlocked = true;
        User user = getUserByName(username);
        
        if (user == null) {
            throw new UsernameNotFoundException("User \"" + username + "\" was not found.");
        }
        // block disabled/unapproved user at logon
        if (username.equalsIgnoreCase("default") || user.isLocked() || !user.isApproved() ) {
        	unlocked = false;
        }
        String[] roles = userDao.getRolesForUser(username);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(roles.length);
        for (int i = 0; i < roles.length; i++) {
            authorities.add(new GrantedAuthorityImpl("ROLE_" + roles[i].toUpperCase()));
        }
        // If user is LDAP authenticated, disable user. 
        // The proper authentication should in that case be done by MadsonicLdapBindAuthenticator.
        boolean enabled = !user.isLdapAuthenticated();

        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), enabled, true, true, unlocked, authorities);
    }

    /**
     * Returns the currently logged-in user for the given HTTP request.
     *
     * @param request The HTTP request.
     * @return The logged-in user, or <code>null</code>.
     */
    public User getCurrentUser(HttpServletRequest request) {
        String username = getCurrentUsername(request);
        return username == null ? null : userDao.getUserByName(username);
    }

    /**
     * Returns the name of the currently logged-in user.
     *
     * @param request The HTTP request.
     * @return The name of the logged-in user, or <code>null</code>.
     */
    public String getCurrentUsername(HttpServletRequest request) {
        return new SecurityContextHolderAwareRequestWrapper(request, null).getRemoteUser();
    }

    /**
     * Returns the user with the given username.
     *
     * @param username The username used when logging in.
     * @return The user, or <code>null</code> if not found.
     */
    public User getUserByName(String username) {
        return userDao.getUserByName(username);
    }

    /**
     * Returns the user with the given email address.
     *
     * @param email The email address.
     * @return The user, or <code>null</code> if not found.
     */
    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    /**
     * Returns the GroupId with the given email address.
     *
     * @param request
     * @return if found integer or <code>null</code> if not found.
     */
	public int getCurrentUserGroupId(HttpServletRequest request) {
		String username = getCurrentUsername(request);
		if (username == null) {
			return 1;
		}
		return userDao.getUserAccess(username);
	}
    
    /**
     * Returns the GroupId with the given email address.
     *
     * @param username
     * @return if found integer or <code>null</code> if not found.
     */
	public int getCurrentUserGroupId(String username) {
		return username == null ? null : userDao.getUserAccess(username);
	}
	
	
    public AccessGroup getAccessGroup(){
    	AccessGroup accessGroup = new AccessGroup();
    	List<Group> allGroups = groupDao.getAllGroups();
    	accessGroup = accessRightDao.getAllAccessToken(allGroups);
		return accessGroup;
    }
    
    public List<AccessToken> getAllAccessToken(){
    	AccessGroup accessGroup = new AccessGroup();
    	List<Group> allGroups = groupDao.getAllGroups();
    	accessGroup = accessRightDao.getAllAccessToken(allGroups);
		return accessGroup.getAccessToken();
    }
    
    public void updateAccessRight(int user_group_id, int music_folder_id, boolean isEnabled){
    	accessRightDao.updateAccessRight(user_group_id, music_folder_id, isEnabled);
    }
    
    /**
     * Returns all users.
     *
     * @return Possibly empty array of all users.
     */
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }
    
    
    /**
     * Returns all users.
     *
     * @return Possibly empty array of all users.
     */
    public List<User> getApproveUsers() {
        return userDao.getApproveUsers();
    }

    /**
     * Returns all groups.
     *
     * @return Possibly empty array of all groups.
     */
    public List<Group> GetAllGroups(){
    	return groupDao.getAllGroups();
    }

    /**
     * Returns id from Group.
     *
     * @return Possibly empty array of GroupId.
     */
    public int GetIdfromGroup(Group group){
    	return groupDao.getIdAllGroups(group);
    }
    
    public int GetIdfromGroupName(String group){
    	return groupDao.getGroupByName(group);
    }
    
    public void createGroup(Group group) {
        groupDao.createGroup(group);
        LOG.info("Created Access Group: " + group.getName());
        group.setId(GetIdfromGroup(group));
        groupDao.insertGroupAccess(group.getId());
        LOG.info("Created Default Access for Group: " + group.getName());
    }
    
    public int getUserGroupVideoDefault(int id){
        return groupDao.getUserGroupVideoDefault(id);
    }
    
    public int getUserGroupVideoMax(int id){
        return groupDao.getUserGroupVideoMax(id);
    }
    
    public void updateGroup(Group group) {
        groupDao.updateGroup(group);
        LOG.debug("Updated Access Group: " + group.getName());
    }

	public void deleteGroup(Group group) {
        groupDao.deleteGroup(group);
        LOG.info("Deleted Access Group: " + group.getName());
	}

	public void resetControl() {
		groupDao.resetGroup();	
	    LOG.info("All Access Groups reseted!!!");
	}	
	
    /**
     * Returns whether the given user has administrative rights.
     */
    public boolean isAdmin(String username) {
        if (User.USERNAME_ADMIN.equals(username)) {
            return true;
        }
        User user = getUserByName(username);
        return user != null && user.isAdminRole();
    }

    public void checkDemoAccount(){
    	
        User demoUser = userDao.getUserByName(User.USERNAME_DEMO);
        
        // check demo User
        if (demoUser == null){
	        userDao.createUser(new User(User.USERNAME_DEMO, RandomStringUtils.randomAlphanumeric(30), null, false, 0, 0, 0, 1, true, "#demo user", true));
	        demoUser = userDao.getUserByName(User.USERNAME_DEMO);
	        demoUser.setLdapAuthenticated(false);
	        demoUser.setStreamRole(true);
	        setSecureUserToken(demoUser);
			setSecurePassword(demoUser);
			demoUser.setLocked(true);
	        userDao.updateUser(demoUser);
	        UserSettings userSettings = settingsService.getUserSettings(User.USERNAME_DEMO);
	        userSettings.setAvatarScheme(AvatarScheme.SYSTEM);
	        userSettings.setSystemAvatarId(0);
	        userDao.updateUserSettings(settingsService.getUserSettings(User.USERNAME_DEMO));
	        LOG.debug("Created user " + User.USERNAME_DEMO);

        } else {
	        UserSettings userSettings = userDao.getUserSettings(User.USERNAME_DEMO);
	        demoUser.setLdapAuthenticated(false);
	     // demoUser.setStreamRole(false);
	     // demoUser.setLocked(true);
	        userDao.updateUser(demoUser);
	        userDao.updateUserSettings(userSettings);
	        LOG.debug("Updated user " + User.USERNAME_DEMO);
        }    	
    }
    
    public void createLdapAccount(User ldapUser){
    
    	// check requirements
    	checkDefaultAccount();
        User defaultUser = userDao.getUserByName(User.USERNAME_DEFAULT);    
        
		ldapUser.setAdminRole(defaultUser.isAdminRole());
		ldapUser.setStreamRole(defaultUser.isStreamRole());
		ldapUser.setDownloadRole(defaultUser.isDownloadRole());
		ldapUser.setUploadRole(defaultUser.isUploadRole());
		ldapUser.setPlaylistRole(defaultUser.isPlaylistRole());
		ldapUser.setCoverArtRole(defaultUser.isCoverArtRole());
		ldapUser.setCommentRole(defaultUser.isCommentRole());
		ldapUser.setPodcastRole(defaultUser.isPodcastRole());
		ldapUser.setJukeboxRole(defaultUser.isJukeboxRole());
		ldapUser.setShareRole(defaultUser.isShareRole());
		ldapUser.setSearchRole(defaultUser.isSearchRole());
		ldapUser.setLastFMRole(defaultUser.isLastFMRole());
		ldapUser.setSettingsRole(defaultUser.isSettingsRole());
		ldapUser.setImageRole(defaultUser.isImageRole());
		ldapUser.setVideoRole(defaultUser.isVideoRole());
		ldapUser.setVideoConversionRole(defaultUser.isVideoConversionRole());
		ldapUser.setAudioConversionRole(defaultUser.isAudioConversionRole());
		
        userDao.updateUser(ldapUser);
        
        UserSettings ldapUserSettings = userDao.getUserSettings(ldapUser.getUsername());
        ldapUserSettings.setAvatarScheme(AvatarScheme.SYSTEM);
        ldapUserSettings.setSystemAvatarId(8);
        ldapUserSettings.setShowLeftBar(true);
        userDao.updateUserSettings(ldapUserSettings);
    }

    
    public void createSignUpAccount(User SignUpUser){
        
    	// check requirements
    	checkDefaultAccount();
        User defaultUser = userDao.getUserByName(User.USERNAME_DEFAULT);    
        
        SignUpUser.setAdminRole(defaultUser.isAdminRole());
        SignUpUser.setStreamRole(defaultUser.isStreamRole());
        SignUpUser.setDownloadRole(defaultUser.isDownloadRole());
        SignUpUser.setUploadRole(defaultUser.isUploadRole());
        SignUpUser.setPlaylistRole(defaultUser.isPlaylistRole());
        SignUpUser.setCoverArtRole(defaultUser.isCoverArtRole());
		SignUpUser.setCommentRole(defaultUser.isCommentRole());
		SignUpUser.setPodcastRole(defaultUser.isPodcastRole());
		SignUpUser.setJukeboxRole(defaultUser.isJukeboxRole());
		SignUpUser.setShareRole(defaultUser.isShareRole());
		SignUpUser.setSearchRole(defaultUser.isSearchRole());
		SignUpUser.setLastFMRole(defaultUser.isLastFMRole());
		SignUpUser.setSettingsRole(defaultUser.isSettingsRole());
		SignUpUser.setImageRole(defaultUser.isImageRole());
		SignUpUser.setVideoRole(defaultUser.isVideoRole());
		SignUpUser.setVideoConversionRole(defaultUser.isVideoConversionRole());
		SignUpUser.setAudioConversionRole(defaultUser.isAudioConversionRole());
		
        userDao.updateUser(SignUpUser);
        
        UserSettings ldapUserSettings = userDao.getUserSettings(SignUpUser.getUsername());
        ldapUserSettings.setAvatarScheme(AvatarScheme.SYSTEM);
        ldapUserSettings.setSystemAvatarId(8);
        userDao.updateUserSettings(ldapUserSettings);
    }  
    
    
    
    public void checkGuestAccount(){
        User guestUser = userDao.getUserByName(User.USERNAME_GUEST);
        
        // check guest User
        if (guestUser == null){
            userDao.createUser(new User(User.USERNAME_GUEST, RandomStringUtils.randomAlphanumeric(30), null, false, 0, 0, 0, 1, false, "#guest account", true));
	        guestUser = userDao.getUserByName(User.USERNAME_GUEST);
	        guestUser.setLdapAuthenticated(false);
	        guestUser.setStreamRole(true);
	        setSecureUserToken(guestUser);
			setSecurePassword(guestUser);
	        userDao.updateUser(guestUser);
	        userDao.updateUserSettings(settingsService.getUserSettings(User.USERNAME_GUEST));
	        UserSettings userSettings = userDao.getUserSettings(User.USERNAME_GUEST);
	        userSettings.setAvatarScheme(AvatarScheme.SYSTEM);
	        userSettings.setSystemAvatarId(2); // 1
	        userSettings.setTranscodeScheme(TranscodeScheme.MAX_192);
	        userDao.updateUserSettings(userSettings);

	        LOG.debug("Created user " + User.USERNAME_GUEST);
        } else {
	        UserSettings userSettings = userDao.getUserSettings(User.USERNAME_GUEST);
	        guestUser.setLdapAuthenticated(false);
	        guestUser.setStreamRole(true);
	        userDao.updateUserSettings(userSettings);
	        LOG.debug("Updated user " + User.USERNAME_GUEST);
        }    	
    }

    public void checkNodeAccount(){
        User nodeUser = userDao.getUserByName(User.USERNAME_NODE);
        
        // check node User
        if (nodeUser == null){
            userDao.createUser(new User(User.USERNAME_NODE, RandomStringUtils.randomAlphanumeric(30), null, false, 0, 0, 0, 1, false, "#node account", true));
            nodeUser = userDao.getUserByName(User.USERNAME_NODE);
            nodeUser.setLdapAuthenticated(false);
            nodeUser.setStreamRole(true);
	        setSecureUserToken(nodeUser);
			setSecurePassword(nodeUser);
	        userDao.updateUser(nodeUser);
	        userDao.updateUserSettings(settingsService.getUserSettings(User.USERNAME_NODE));
	        UserSettings userSettings = userDao.getUserSettings(User.USERNAME_NODE);
	        userSettings.setAvatarScheme(AvatarScheme.SYSTEM);
	        userSettings.setSystemAvatarId(3); // 1
	        userSettings.setTranscodeScheme(TranscodeScheme.OFF);
	        userDao.updateUserSettings(userSettings);

	        LOG.debug("Created user " + User.USERNAME_NODE);
        } else {
	        UserSettings userSettings = userDao.getUserSettings(User.USERNAME_NODE);
	        nodeUser.setLdapAuthenticated(false);
	        nodeUser.setStreamRole(true);
	        userDao.updateUserSettings(userSettings);
	        LOG.debug("Updated user " + User.USERNAME_NODE);
        }    	
    }    
    
    
    /**
     * check/creates a default/guest user.
     */
    public void checkDefaultAccount(){
    	
        User defaultUser = userDao.getUserByName(User.USERNAME_DEFAULT);
        
        // check default User
        if (defaultUser == null){
	        userDao.createUser(new User(User.USERNAME_DEFAULT, RandomStringUtils.randomAlphanumeric(30), null, false, 0, 0, 0, 0, true, "#default user", true));
	        defaultUser = userDao.getUserByName(User.USERNAME_DEFAULT);
	        setSecureUserToken(defaultUser);
			setSecurePassword(defaultUser);
	        userDao.updateUser(defaultUser);
	        UserSettings userSettings = settingsService.getUserSettings(User.USERNAME_DEFAULT);
	        userSettings.setAvatarScheme(AvatarScheme.SYSTEM);
	        userSettings.setSystemAvatarId(1);
	        
	        userDao.updateUserSettings(userSettings);
	        LOG.debug("Created user " + User.USERNAME_DEFAULT);

        } else {
	        UserSettings userSettings = userDao.getUserSettings(User.USERNAME_DEFAULT);
	        defaultUser.setLocked(true); //forced
	        userDao.updateUser(defaultUser);
	        userDao.updateUserSettings(userSettings);
	        LOG.debug("Updated user " + User.USERNAME_DEFAULT);
        }
    }
    
    public void resetStats() {
    	userDao.resetStats();
    }
    
    
    /**
     * Creates a new user.
     *
     * @param user The user to create.
     */
    public void createUser(User user) {
        userDao.createUser(user);
        LOG.info("Created user " + user.getUsername());
    }
    
    public void cloneUser(User user) {
        userDao.createUser(user);
        userDao.updateUserSettings(settingsService.getDefaultUserSettings(user.getUsername()));
        LOG.debug("Cloned from default user: " + user.getUsername());
    }    
    
    /**
     * Deletes the user with the given username.
     *
     * @param username The username.
     */
    public void deleteUser(String username) {
        userDao.deleteUser(username);
        LOG.info(" Deleted user " + username);
        userCache.remove(username);
    }

    /**
     * Updates the given user.
     *
     * @param user The user to update.
     */
    public void updateUser(User user) {
        userDao.updateUser(user);
        userCache.remove(user.getUsername());
    }

    /**
     * Updates the byte counts for given user.
     *
     * @param user                 The user to update, may be <code>null</code>.
     * @param bytesStreamedDelta   Increment bytes streamed count with this value.
     * @param bytesDownloadedDelta Increment bytes downloaded count with this value.
     * @param bytesUploadedDelta   Increment bytes uploaded count with this value.
     */
    public void updateUserByteCounts(User user, long bytesStreamedDelta, long bytesDownloadedDelta, long bytesUploadedDelta) {
        if (user == null) {
            return;
        }

        user.setBytesStreamed(user.getBytesStreamed() + bytesStreamedDelta);
        user.setBytesDownloaded(user.getBytesDownloaded() + bytesDownloadedDelta);
        user.setBytesUploaded(user.getBytesUploaded() + bytesUploadedDelta);

        userDao.updateUser(user);
    }
    
    public boolean isAccessAllowed(MediaFile song, int user_group_id){
    	File file = new File(song.getPath());
        return isInMusicFolder(file, user_group_id) || isInPodcastFolder(file);    	
    }
    
    public boolean isAccessAllowed(File file, int user_group_id){
        return isInMusicFolder(file, user_group_id) || isInPodcastFolder(file);    	
    	//TODO:isAccessAllowed
    }

    public boolean isReadAllowed(File file) {
        // Allowed to read from both music folder and podcast folder.
        return isInMusicFolder(file) || isInPodcastFolder(file);
    }    
    
    /**
     * Returns whether the given file may be read.
     *
     * @return Whether the given file may be read.
     */
    public boolean isReadAllowed(File file, int user_group_id) {
        // Allowed to read from both music folder and podcast folder.
        return isInMusicFolder(file, user_group_id) || isInPodcastFolder(file);
    }

    /**
     * Returns whether the given file may be written, created or deleted.
     *
     * @return Whether the given file may be written, created or deleted.
     */
    public boolean isWriteAllowed(File file) {
        // Only allowed to write podcasts, artist art or cover art.
        boolean isPodcast = isInPodcastFolder(file);
        boolean isCoverArt = isInMusicFolder(file) && file.getName().toLowerCase().startsWith("cover.");
        boolean isArtistArt = isInMusicFolder(file) && file.getName().toLowerCase().startsWith("artist.");
        
        return isPodcast || isCoverArt || isArtistArt;
    }

    /**
     * Returns whether the given file may be uploaded.
     *
     * @return Whether the given file may be uploaded.
     */
    public boolean isUploadAllowed(File file) {
        return isInUploadFolder(file) || isInMusicFolder(file) && !FileUtil.exists(file);
    }
    
    /**
     * Returns whether the given file is located in the upload folders.
     *
     * @param file The file in question.
     * @return Whether the given file is located in the upload folders.
     */
    private boolean isInUploadFolder(File file) {
        if (isFileInFolder(file.getPath(), settingsService.getUploadFolder())) {
            return true;
        }
    return false;
    }

	/**
     * Returns whether the given file is located in one of the music folders (or any of their sub-folders).
     *
     * @param file The file in question.
     * @return Whether the given file is located in one of the music folders.
     */
    private boolean isInMusicFolder(File file, int user_group_id) {
        return getMusicFolderForFile(file, user_group_id ) != null;
    }
    
    private MusicFolder getMusicFolderForFile(File file, int user_group_id) {
        List<MusicFolder> folders = settingsService.getAllMusicFolders(false, true, user_group_id);
        String path = file.getPath();
        for (MusicFolder folder : folders) {
            if (isFileInFolder(path, folder.getPath().getPath())) {
                return folder;
            }
        }
        return null;
    }

    private boolean isInMusicFolder(File file) {
        return getMusicFolderForFile(file) != null;
    }
    
    private MusicFolder getMusicFolderForFile(File file) {
    	if (file == null) {
    		return null;
    	}
    	
        List<MusicFolder> folders = settingsService.getAllMusicFolders(false, true);
        String path = file.getPath();
        for (MusicFolder folder : folders) {
            if (isFileInFolder(path, folder.getPath().getPath())) {
                return folder;
            }
        }
        return null;
    }
    
    /**
     * Returns whether the given file is located in the Podcast folder (or any of its sub-folders).
     *
     * @param file The file in question.
     * @return Whether the given file is located in the Podcast folder.
     */
    private boolean isInPodcastFolder(File file) {
    	if (file == null) {
    		return false;
    	}
    	
        String podcastFolder = settingsService.getPodcastFolder();
        return isFileInFolder(file.getPath(), podcastFolder);
    }

    public String getRootFolderForFile(File file) {
        MusicFolder folder = getMusicFolderForFile(file, 0);
        if (folder != null) {
            return folder.getPath().getPath();
        }

        if (isInPodcastFolder(file)) {
            return settingsService.getPodcastFolder();
        }
        return null;
    }

    public boolean isAuthenticated(MediaFile mediaFile, HttpServletRequest request) {
        boolean ok = mediaFile.getHash().equals(request.getParameter("auth"));
        if (!ok) {
            LOG.warn("Unauthenticated access attempt: " + mediaFile.getPath() + " hash: " + request.getParameter("auth") + " <> " + mediaFile.getHash() );
        }
        return ok;
    }

    /**
     * Returns whether the given file is located in the given folder (or any of its sub-folders).
     * If the given file contains the expression ".." (indicating a reference to the parent directory),
     * this method will return <code>false</code>.
     *
     * @param file   The file in question.
     * @param folder The folder in question.
     * @return Whether the given file is located in the given folder.
     */
    public boolean isFileInFolder(String file, String folder) {
        // Deny access if file contains ".." surrounded by slashes (or end of line).
        if (file.matches(".*(/|\\\\)\\.\\.(/|\\\\|$).*")) {
            return false;
        }

        // Convert slashes.
        file = file.replace('\\', '/');
        folder = folder.replace('\\', '/');

        return file.toUpperCase().startsWith(folder.toUpperCase());
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

	public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

	public void setAccessRightDao(AccessRightDao accessRightDao) {
        this.accessRightDao = accessRightDao;
    }
	
    public void setUserCache(Ehcache userCache) {
        this.userCache = userCache;
    }

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}
}