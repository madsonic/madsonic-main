/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.service;

import org.madsonic.Logger;

public class UserService {

	   private static final Logger LOG = Logger.getLogger(UserService.class);

	    private SecurityService securityService;
	    
	    public void init() {
	        CheckUser();
	    }
	    
	    public void CheckUser() {
	        Runnable runnable = new Runnable() {
	            public void run() {
	                try {
	                    LOG.debug("Checking User accounts ...");
	                    
	                    securityService.checkDefaultAccount();
	                    LOG.debug("Checking Default User. Done");
	                    
	                    securityService.checkGuestAccount();
	                    LOG.debug("Checking Guest User. Done");

					    securityService.checkNodeAccount();
						LOG.debug("Checking Node User. Done");
                    
						// demo user	                    
						// securityService.checkDemoAccount();
						// LOG.debug("Checking Demo User. Done");

	                    
	                } catch (Throwable x) {
	                    LOG.error("Failed to check User service: " + x, x);
	                }
	            }
	        };
	        new Thread(runnable).start();
	    }
	    
	    public void setSecurityService(SecurityService securityService) {
	        this.securityService = securityService;
	    }    
}
