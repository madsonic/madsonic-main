/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jfree.util.Log;
import org.madsonic.Logger;
import org.madsonic.command.MusicFolderSettingsCommand.MusicFolderInfo;
import org.madsonic.dao.AvatarDao;
import org.madsonic.dao.GroupDao;
import org.madsonic.dao.InternetRadioDao;
import org.madsonic.dao.MusicFolderDao;
import org.madsonic.dao.UserDao;
import org.madsonic.domain.Avatar;
import org.madsonic.domain.AvatarScheme;
import org.madsonic.domain.InternetRadio;
import org.madsonic.domain.LdapGroupSyncType;
import org.madsonic.domain.LicenseInfo;
import org.madsonic.domain.MediaLibraryStatistics;
import org.madsonic.domain.MusicFolder;
import org.madsonic.domain.MusicFolderComparator;
import org.madsonic.domain.ServerVersion;
import org.madsonic.domain.Theme;
import org.madsonic.domain.UrlRedirectType;
import org.madsonic.domain.UserSettings;
import org.madsonic.service.sonos.SonosRatingType;
import org.madsonic.util.FileUtil;
import org.madsonic.util.OperatingSystem;
import org.madsonic.util.StringUtil;
import org.madsonic.util.Util;

/**
 * Provides persistent storage of application settings and preferences.
 *
 * @author Sindre Mehus, Martin Karel
 */
@SuppressWarnings("deprecation")
public class SettingsService {
 
	// Madsonic home directory.
	private static final File MADSONIC_HOME_WINDOWS = new File("c:/madsonic");
	private static final File MADSONIC_HOME_OTHER = new File("/var/madsonic");
	private static final File MADSONIC_HOME_MAC = new File("/Library/Application Support/Madsonic");

	// Number of free trial days.
    public static final long TRIAL_DAYS = 30L;

	// Global settings 
	private static final String KEY_PAGE_TITLE = "PageTitle";
	private static final String KEY_ALL_INDEX_STRING = "AllIndexString";
	private static final String KEY_MUSIC_INDEX_STRING = "MusicIndexString";
	private static final String KEY_VIDEO_INDEX_STRING = "VideoIndexString";
	private static final String KEY_IMAGE_INDEX_STRING = "ImageIndexString";
	private static final String KEY_INDEX0_STRING = "Index0String";
	private static final String KEY_INDEX1_STRING = "Index1String";
	private static final String KEY_INDEX2_STRING = "Index2String";
	private static final String KEY_INDEX3_STRING = "Index3String";
	private static final String KEY_INDEX4_STRING = "Index4String";
	private static final String KEY_IGNORED_ARTICLES = "IgnoredArticles";
	private static final String KEY_SHORTCUTS = "Shortcuts";
	private static final String KEY_UPLOAD_FOLDER = "UploadFolder";
	private static final String KEY_TRANSCODE_FOLDER = "TranscodeFolder";
	private static final String KEY_CONVERSATION_FOLDER = "ConversationFolder";
	private static final String KEY_VIDEO_CONVERSATION_FOLDER = "VideoConversationFolder";
	private static final String KEY_AUDIO_CONVERSATION_FOLDER = "AudioConversationFolder";
	private static final String KEY_PLAYLIST_IMPORT_FOLDER = "PlaylistImportFolder";
	private static final String KEY_PLAYLIST_EXPORT_FOLDER = "PlaylistExportFolder";
	private static final String KEY_PLAYLIST_BACKUP_FOLDER = "PlaylistBackupFolder";
	private static final String KEY_MOD_FILE_TYPES = "ModFileTypes";
	private static final String KEY_MUSIC_FILE_TYPES = "MusicFileTypes1";
	private static final String KEY_VIDEO_FILE_TYPES = "VideoFileTypes";
	private static final String KEY_IMAGE_FILE_TYPES = "ImageFileTypes";
	private static final String KEY_COVER_ART_FILE_TYPES = "CoverArtFileTypes";
	private static final String KEY_COVER_ART_LIMIT = "CoverArtLimit";
    private static final String KEY_COVER_ART_CONCURRENCY = "CoverArtConcurrency";
	private static final String KEY_WELCOME_TITLE = "WelcomeTitle";
	private static final String KEY_WELCOME_SUBTITLE = "WelcomeSubtitle";
	private static final String KEY_WELCOME_MESSAGE = "WelcomeMessage2";
	private static final String KEY_ADMIN_MESSAGE = "AdminMessage";
	private static final String KEY_LOGIN_MESSAGE = "LoginMessage";
	private static final String KEY_LOCALE_LANGUAGE = "LocaleLanguage";
	private static final String KEY_LOCALE_COUNTRY = "LocaleCountry";
	private static final String KEY_LOCALE_VARIANT = "LocaleVariant";
	private static final String KEY_THEME_ID = "Theme";
	private static final String KEY_LISTTYPE = "ListType";
	private static final String KEY_NEWADDED_TIMESPAN = "NewAddedTimeSpan";
	private static final String KEY_INDEX_CREATION_INTERVAL = "IndexCreationInterval";
	private static final String KEY_INDEX_CREATION_HOUR = "IndexCreationHour";
	private static final String KEY_FAST_CACHE_ENABLED = "FastCacheEnabled";
	private static final String KEY_PODCAST_UPDATE_INTERVAL = "PodcastUpdateInterval";
	private static final String KEY_PODCAST_FOLDER = "PodcastFolder";
	private static final String KEY_PODCAST_EPISODE_RETENTION_COUNT = "PodcastEpisodeRetentionCount";
	private static final String KEY_PODCAST_EPISODE_DOWNLOAD_COUNT = "PodcastEpisodeDownloadCount";
	private static final String KEY_PODCAST_EPISODE_DOWNLOAD_LIMIT = "PodcastEpisodeDownloadLimit";
	private static final String KEY_DOWNLOAD_BITRATE_LIMIT = "DownloadBitrateLimit";
	private static final String KEY_UPLOAD_BITRATE_LIMIT = "UploadBitrateLimit";
	private static final String KEY_STREAM_PORT = "StreamPort";
	private static final String KEY_LICENSE_EMAIL = "LicenseEmail";
	private static final String KEY_LICENSE_CODE = "LicenseCode";
	private static final String KEY_LICENSE_DATE = "LicenseDate";
	private static final String KEY_DOWNSAMPLING_COMMAND = "DownsamplingCommand4";
	private static final String KEY_HLS_COMMAND = "HlsCommand";
	private static final String KEY_DASH_COMMAND = "DashCommand";
	private static final String KEY_JUKEBOX_COMMAND = "JukeboxCommand";
    private static final String KEY_VIDEO_IMAGE_COMMAND = "VideoImageCommand";
    private static final String KEY_TV_IMAGE_COMMAND = "TVImageCommand";
	private static final String KEY_REWRITE_URL = "RewriteUrl";
	private static final String KEY_LDAP_ENABLED = "LdapEnabled";
	private static final String KEY_LDAP_URL = "LdapUrl";
	private static final String KEY_LDAP_MANAGER_DN = "LdapManagerDn";
	private static final String KEY_LDAP_MANAGER_PASSWORD = "LdapManagerPassword";
	private static final String KEY_LDAP_GROUP_SEARCH_BASE = "LdapGroupSearchBase";
	private static final String KEY_LDAP_SEARCH_FILTER = "LdapSearchFilter";
	private static final String KEY_LDAP_GROUP_FILTER = "LdapGroupFilter";
	private static final String KEY_LDAP_GROUP_ATTRIB = "LdapGroupAttrib";
	private static final String KEY_LDAP_AUTO_SHADOWING = "LdapAutoShadowing";
	private static final String KEY_LDAP_AUTO_MAPPING = "LdapAutoMapping";
	private static final String KEY_LDAP_GROUP_SYNC_TYPE = "LdapGroupSyncType";
	private static final String KEY_LDAP_GROUP_AUTO_SYNC = "LdapGroupAutoSync";	
	private static final String KEY_GETTING_STARTED_ENABLED = "GettingStartedEnabled";
	private static final String KEY_PORT_FORWARDING_ENABLED = "PortForwardingEnabled";
	private static final String KEY_PORT = "Port";
	private static final String KEY_HTTPS_PORT = "HttpsPort";
	private static final String KEY_URL_REDIRECTION_ENABLED = "UrlRedirectionEnabled";
    private static final String KEY_URL_REDIRECT_TYPE = "UrlRedirectType";
	private static final String KEY_URL_REDIRECT_FROM = "UrlRedirectFrom";
	private static final String KEY_URL_REDIRECT_CONTEXT_PATH = "UrlRedirectContextPath";
    private static final String KEY_URL_REDIRECT_CUSTOM_URL = "UrlRedirectCustomUrl";
    private static final String KEY_APP_REDIRECT_ENABLED = "AppRedirectEnabled";
	private static final String KEY_SERVER_ID = "ServerId";
	private static final String KEY_SETTINGS_CHANGED = "SettingsChanged";
	private static final String KEY_LAST_SCANNED = "LastScanned";
	private static final String KEY_ORGANIZE_BY_FOLDER_STRUCTURE = "OrganizeByFolderStructure";
	private static final String KEY_ORGANIZE_BY_GENRE_MAP = "OrganizeByGenreMap";
	private static final String KEY_SHOW_ALBUMS_YEAR = "ShowAlbumsYear";
	private static final String KEY_SHOW_ALBUMS_YEAR_API = "ShowAlbumsYearAPI";
	private static final String KEY_SORT_ALBUMS_BY_FOLDER = "SortAlbumsByFolder";
	private static final String KEY_SORT_MEDIAFILEFOLDER = "SortMediaFileFolder";
	private static final String KEY_HOME_SHARING = "UseHomeSharing";
	private static final String KEY_SHOW_GENERIC_ARTIST_ART = "ShowGenericArtistArt";
	private static final String KEY_SHOW_SHORTCUTS_ALWAYS = "ShowShortcutsAlways";
	private static final String KEY_SHOW_QUICK_EDIT = "ShowQuickEdit";
	private static final String KEY_USED_VIDEO_PLAYER = "usedVideoPlayer";
	private static final String KEY_OWN_GENRE_ENABLED = "ownGenreEnabled";
	private static final String KEY_PLAYLIST_ENABLED = "PlaylistEnabled";
	private static final String KEY_MEDIA_LIBRARY_STATISTICS = "MediaLibraryStatistics";
	private static final String KEY_TRIAL_EXPIRES = "TrialExpires";
	private static final String KEY_RSS_ENABLED = "RssFeedEnabled";

	private static final String KEY_PLAYLIST_EXPORT_MODE = "PlaylistExportMode";
	private static final String KEY_SCAN_MODE = "ScanMode";
	private static final String KEY_COVER_ART_HQ = "CoverArtHighQuality";
	private static final String KEY_CUSTOM_LOGO = "CustomLogo";
	
//	@Deprecated private static final String KEY_SORT_FILES_BY_FILENAME = "SortFilesByFilename";
	@Deprecated private static final String KEY_PLAYQUEUE_RESIZE = "PlayQueueResize";
	@Deprecated private static final String KEY_LEFTFRAME_RESIZE = "LeftFrameResize";
	
	private static final String KEY_INDEXLIST_SIZE = "IndexListSize";
	private static final String KEY_PLAYQUEUE_SIZE = "PlayQueueSize";
	private static final String KEY_LEFTFRAME_SIZE = "LeftFrameSize";    
	private static final String KEY_CUSTOMSCROLLBAR = "CustomScrollbar";
	private static final String KEY_CUSTOMACCORDION = "CustomAccordion";
	private static final String KEY_ICON_HOME = "ShowIconHome";
	private static final String KEY_ICON_INDEX = "ShowIconIndex";
	private static final String KEY_ICON_ARTIST = "ShowIconArtist";
	private static final String KEY_ICON_PLAYING = "ShowIconPlaying";
	private static final String KEY_ICON_BOOKMARK = "ShowIconBookmark";
	private static final String KEY_ICON_INTERNET_RADIO = "ShowIconInternetRadio";
	private static final String KEY_ICON_STARRED = "ShowIconStarred";
	private static final String KEY_ICON_LOVED = "ShowIconLoved";
	private static final String KEY_ICON_RADIO = "ShowIconRadio";
	private static final String KEY_ICON_PODAST = "ShowIconPodcast";
	private static final String KEY_ICON_PODAST_MANAGE = "ShowIconPodcastManage";
	private static final String KEY_ICON_SETTINGS = "ShowIconSettings";
	private static final String KEY_ICON_STATUS = "ShowIconStatus";
	private static final String KEY_ICON_SOCIAL = "ShowIconSocial";
	private static final String KEY_ICON_HISTORY = "ShowIconHistory";
	private static final String KEY_ICON_STATISTICS = "ShowIconStatistics";
	private static final String KEY_ICON_PLAYLISTS = "ShowIconPlaylists";
	private static final String KEY_ICON_PLAYLIST_EDITOR = "ShowIconPlaylistEditor";
	private static final String KEY_ICON_PLAYLIST_MANAGE = "ShowIconPlaylistManage";
	private static final String KEY_ICON_MORE = "ShowIconMore";    
	private static final String KEY_ICON_RANDOM = "ShowIconRandom";    
	private static final String KEY_ICON_ABOUT = "ShowIconAbout";    
	private static final String KEY_ICON_GENRE = "ShowIconGenre";    
	private static final String KEY_ICON_MOODS = "ShowIconMoods";
	private static final String KEY_ICON_ADMINS = "ShowIconAdmins";  
	private static final String KEY_ICON_COVER = "ShowIconCover";
	private static final String KEY_DLNA_ENABLED = "DlnaEnabled";
	private static final String KEY_DLNA_SERVER_NAME = "DlnaServerName";
    private static final String KEY_SONOS_ENABLED = "SonosEnabled";
    private static final String KEY_SONOS_SERVICE_NAME = "SonosServiceName";
    private static final String KEY_SONOS_SERVICE_ID = "SonosServiceId";
    private static final String KEY_SONOS_RATING_TYPE = "SonosRatingType";
    private static final String KEY_SIGNUP_ENABLED = "SignupEnabled";
    private static final String KEY_EMULATOR_ENABLED = "EmulatorEnabled";
    private static final String KEY_NODES_ENABLED = "NodesEnabled";
    private static final String KEY_SIGNUP_SERVICE_NAME = "SignupServiceName";
    private static final String KEY_AUDIO_AD_ENABLED = "AudioAdEnabled";
    private static final String KEY_AUDIO_AD_FREQUENCY = "AudioAdFrequency";
	private static final String KEY_FOLDERPARSING_ENABLED = "FolderParsingEnabled";
	private static final String KEY_ALBUMSETPARSING_ENABLED = "AlbumSetParsingEnabled";
	private static final String KEY_HOME_RANDOM = "ShowHomeRandom";    
	private static final String KEY_HOME_NEW_ADDED = "ShowHomeNewAdded"; 
	private static final String KEY_HOME_HOT_RATED = "ShowHomeHotRated"; 
	private static final String KEY_HOME_ALLARTIST = "ShowHomeAllArtist"; 
	private static final String KEY_HOME_STARRED_ARTIST = "ShowHomeStarredArtist"; 
	private static final String KEY_HOME_STARRED_ALBUM = "ShowHomeStarredAlbum"; 
	private static final String KEY_HOME_ALBUMTIP = "ShowHomeAblumTip"; 
	private static final String KEY_HOME_TOP_RATED = "ShowHomeTopRated"; 
	private static final String KEY_HOME_MOSTPLAYED = "ShowHomeMostPlayed"; 
	private static final String KEY_HOME_LASTPLAYED = "ShowHomeLastPlayed"; 
	private static final String KEY_HOME_DECADE = "ShowHomeDecade"; 
	private static final String KEY_HOME_GENRE = "ShowHomeGenre"; 
	private static final String KEY_HOME_NAME = "ShowHomeName"; 
	private static final String KEY_HOME_TOP100 = "ShowHomeTop100"; 
	private static final String KEY_HOME_NEW100 = "ShowHomeNew100"; 
	private static final String KEY_HOME_PAGER_TOP = "ShowHomePagerTop"; 
	private static final String KEY_HOME_PAGER_BOTTOM = "ShowHomePagerBottom"; 
	private static final String KEY_LOGFILE_REVERSE = "LogfileReverse";
	private static final String KEY_LOGFILE_LEVEL = "LogfileLevel";
	private static final String KEY_PANDORA_ARTIST_TOPTRACK = "PandoraResultArtistTopTrack";
	private static final String KEY_PANDORA_ALBUM = "PandoraResultAlbum";
	private static final String KEY_PANDORA_ARTIST = "PandoraResultArtist";
	private static final String KEY_PANDORA_GENRE = "PandoraResultGenre";
	private static final String KEY_PANDORA_MOOD = "PandoraResultMood";
	private static final String KEY_PANDORA_SIMILAR = "PandoraResultSimilar";
    private static final String KEY_ARTIST_BIO_SCAN = "ArtistBioScan";
    private static final String KEY_ARTIST_BIO_LAST_UPDATED = "ArtistBioLastUpdated";
	private static final String KEY_LASTFM_TOPTRACK_SEARCH = "LastFMTopTrackSearch";
	private static final String KEY_LASTFM_RESULT_SIZE = "LastFMResultSize";
	private static final String KEY_LASTFM_LANGUAGE = "LastFMLanguage";
	
	@Deprecated
	private static final String KEY_FEATURE_SNOW = "FeatureSnow";	
	private static final String KEY_DEBUG_OUTPUT = "DebugOutput";	
	private static final String KEY_SHOW_CREATELINK = "CreateLink";	
	private static final String KEY_SHOW_MEDIATYPE = "MediaType";
	
	// Madsonic Default values.
	private static final String DEFAULT_PAGE_TITLE = "Madsonic";
	private static final String DEFAULT_ALL_INDEX_STRING = "1";
	private static final String DEFAULT_MUSIC_INDEX_STRING = "1";
	private static final String DEFAULT_VIDEO_INDEX_STRING = "2";
	private static final String DEFAULT_IMAGE_INDEX_STRING = "2";
	private static final String DEFAULT_INDEX0_STRING = "#(!#0123456789) A B C D E F G H I J K L M N O P Q R S T U V W X Y Z";
	private static final String DEFAULT_INDEX1_STRING = "# ! 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z";
	private static final String DEFAULT_INDEX2_STRING = "# A-F(ABCDEF) G-L(GHIJKL) M-S(MNOPQRS) T-Z(TUVWXYZ)";
	private static final String DEFAULT_INDEX3_STRING = "# 0-9(0123456789) A-Z(ABCDEFGHIJKLMNOPQRSTUVWXYZ)";
	private static final String DEFAULT_INDEX4_STRING = "# A-M(ABCDEFGHIJKLM) N-Z(NOPQRSTUVWXYZ)";
	private static final String DEFAULT_IGNORED_ARTICLES = "The El La Los Las Le Les";
	private static final String DEFAULT_SHORTCUTS = "New Incoming Podcasts";
	private static String DEFAULT_UPLOAD_FOLDER = Util.getDefaultUploadFolder();
	private static File DEFAULT_TRANSCODE_FOLDER = new File(getMadsonicHome(), "transcode");
	private static File DEFAULT_CONVERSION_FOLDER = new File(getMadsonicHome(), "conversion");
	private static String DEFAULT_VIDEO_CONVERSION_FOLDER = new File(DEFAULT_CONVERSION_FOLDER, "video").getPath();
	private static String DEFAULT_AUDIO_CONVERSION_FOLDER = new File(DEFAULT_CONVERSION_FOLDER, "audio").getPath();
    private static final String KEY_VIDEO_CONVERSION_DISK_LIMIT = "VideoConversionDiskLimit";
    private static final String KEY_AUDIO_CONVERSION_DISK_LIMIT = "AudioConversionDiskLimit";
    private static final int DEFAULT_AUDIO_CONVERSION_DISK_LIMIT = 0;
    private static final int DEFAULT_VIDEO_CONVERSION_DISK_LIMIT = 0;
	private static final String DEFAULT_PLAYLIST_IMPORT_FOLDER = Util.getDefaultPlaylistImportFolder();
	private static final String DEFAULT_PLAYLIST_EXPORT_FOLDER = Util.getDefaultPlaylistExportFolder();
	private static final String DEFAULT_PLAYLIST_BACKUP_FOLDER = Util.getDefaultPlaylistBackupFolder();
	private static final String DEFAULT_MOD_FILE_TYPES = "669 alm amd amf dbm dmf emod far flx fnk gdm gmc gtk hsc imf it j2b liq m15 mdl med mgt mod mtm mtn mtp okt psm ptm rad rtm s3m sfx stm stx ult umx wow xm";
	private static final String DEFAULT_MUSIC_FILE_TYPES = "aac aif aiff ape dsf dff flac m4a mp3 mpc oga ogg opus shn wav wma";
	private static final String DEFAULT_VIDEO_FILE_TYPES = "avi divx flv hdrun m2ts m4v mkv mov mp4 mpeg mpg ogv ts tv url wmv wtv";
	private static final String DEFAULT_IMAGE_FILE_TYPES = "jpg jpeg png gif bmp";
	
	private static final String DEFAULT_COVER_ART_FILE_TYPES = "front.jpg front.png cover.jpg cover.png folder.jpg folder.jpeg folder.png folder.gif jpg jpeg gif png";
	private static final boolean DEFAULT_COVER_ART_HQ =  false;

	private static final int DEFAULT_COVER_ART_LIMIT = 50;
    private static final int DEFAULT_COVER_ART_CONCURRENCY = 5;

    private static final String DEFAULT_WELCOME_TITLE = "Welcome to Madsonic!";
	private static final String DEFAULT_WELCOME_SUBTITLE = null;
	private static final String DEFAULT_WELCOME_MESSAGE = "__Welcome to Madsonic v6!__\n" +
			"\\\\ \\\\\n" +
			"Madsonic is a free, web-based media streamer, providing ubiquitous access to your music. \n" +
			"\\\\ \\\\\n" +
			"Use it to share your music with friends, or to listen to your own music while at work. You can stream to multiple " +
			"players simultaneously, for instance to one player in your kitchen and another in your living room.\n" +
			"\\\\ \\\\\n" +
			"To change or remove this message, log in with administrator rights and go to {link:Settings > Advanced|advancedSettings.view} or \\\\\n" +
			"{link:Settings > General|generalSettings.view}.";

	private static final String DEFAULT_ADMIN_MESSAGE = "__Welcome to Madsonic Admin Panel!__\n" +
			"\\\\ \\\\\n" +
			"Settings > {link:General|generalSettings.view} \\\\\n" +
			"Settings > {link:Mediafolder|musicFolderSettings.view} \\\\\n" +
			"Settings > {link:Folderaccess|accessSettings.view} \\\\\n" +
			"Settings > {link:Securitygroups|groupSettings.view} \\\\\n" +
			"\\\\\n" +
			"Settings > User > Admin User > {link:Profile|userSettings.view?userIndex=0&usrAct=edit} > {link: Settings|profileSettings.view?profile=admin} \\\\\n" +
			"Settings > User > Guest User > {link:Profile|userSettings.view?userIndex=3&usrAct=edit} > {link: Settings|profileSettings.view?profile=guest} \\\\\n" +
			"Settings > User > Default User > {link:Profile|userSettings.view?userIndex=1&usrAct=edit} > {link: Settings|profileSettings.view?profile=default} \\\\\n" +
			"\\\\\n" +
			"To change this Panel, go to {link:Settings > Advanced|advancedSettings.view}.";
	
	private static final String DEFAULT_LOGIN_MESSAGE = null;
	private static final String DEFAULT_LOCALE_LANGUAGE = "en";
	private static final String DEFAULT_LOCALE_COUNTRY = "";
	private static final String DEFAULT_LOCALE_VARIANT = "";
    private static final String DEFAULT_THEME_ID = "madsonic_4klight";
	private static final String DEFAULT_LISTTYPE = "random";
	private static final String DEFAULT_NEWADDED_TIMESPAN = "ThreeMonth";
	private static final boolean DEFAULT_SHOW_SHORTCUTS_ALWAYS = false;
	private static final int DEFAULT_INDEX_CREATION_INTERVAL = 1;
	private static final int DEFAULT_INDEX_CREATION_HOUR = 3;
	private static final boolean DEFAULT_FAST_CACHE_ENABLED = false;
	private static final int DEFAULT_PODCAST_UPDATE_INTERVAL = 24;
	private static final String DEFAULT_PODCAST_FOLDER = Util.getDefaultPodcastFolder();
	private static final int DEFAULT_PODCAST_EPISODE_RETENTION_COUNT = 10;
	private static final int DEFAULT_PODCAST_EPISODE_DOWNLOAD_COUNT = 1;
	private static final int DEFAULT_PODCAST_EPISODE_DOWNLOAD_LIMIT = 1;
	private static final long DEFAULT_DOWNLOAD_BITRATE_LIMIT = 0;
	private static final long DEFAULT_UPLOAD_BITRATE_LIMIT = 0;
	private static final int DEFAULT_STREAM_PORT = 0;
	private static final String DEFAULT_LICENSE_EMAIL = null;
	private static final String DEFAULT_LICENSE_CODE = null;
	private static final String DEFAULT_LICENSE_DATE = null;
	private static final String DEFAULT_DOWNSAMPLING_COMMAND = "ffmpeg -i %s -map 0:a:0 -b:a %bk -ar 44100 -v 0 -f mp3 -";
	private static final String DEFAULT_HLS_COMMAND = "ffmpeg -ss %o -t %d -i %s -s %wx%h -v 0 -b:v %bk -maxrate %bk -bufsize 256k -flags -global_header -map 0:v:0 -map 0:%k -ac 2 -f mpegts -c:v libx264 -preset superfast -c:a aac -b:a 96k -strict -2 -threads 0 -copyts -";
	private static final String DEFAULT_DASH_COMMAND = "ffmpeg -ss %o -t %d -i %s -async 1 -b:v %bk -s %wx%h -ar 44100 -ac 2 -v 0 -f webm -c:v vp8 -preset superfast -c:a libmp3lame -threads 0 -";
	private static final String DEFAULT_JUKEBOX_COMMAND = "ffmpeg -ss %o -i %s -map 0:0 -v 0 -ar 44100 -ac 2 -f s16be -";
    private static final String DEFAULT_VIDEO_IMAGE_COMMAND = "ffmpeg -r 1 -ss %o -t 1 -i %s -s %wx%h -v 0 -f mjpeg -";
    private static final String DEFAULT_TV_IMAGE_COMMAND = "ffmpeg -r 1 -ss 1 -t 1 -i %p -s %wx%h -v 0 -f mjpeg -";
	private static final boolean DEFAULT_REWRITE_URL = true;
	private static final boolean DEFAULT_LDAP_ENABLED = false;
	private static final String DEFAULT_LDAP_URL = "ldap://localhost:389/dc=domain,dc=com";
	private static final String DEFAULT_LDAP_MANAGER_DN = null;
	private static final String DEFAULT_LDAP_MANAGER_PASSWORD = null;
	private static final String DEFAULT_LDAP_GROUP_SEARCH_BASE = "ou=groups";
	private static final String DEFAULT_LDAP_SEARCH_FILTER = "(sAMAccountName={0})";
	private static final String DEFAULT_LDAP_GROUP_FILTER = "(member={0})";
	private static final String DEFAULT_LDAP_GROUP_ATTRIB = "cn";
	private static final boolean DEFAULT_LDAP_AUTO_SHADOWING = false;
	private static final boolean DEFAULT_LDAP_AUTO_MAPPING = false;	
    private static final LdapGroupSyncType DEFAULT_LDAP_GROUP_SYNC_TYPE = LdapGroupSyncType.MERGE;
	private static final boolean DEFAULT_LDAP_GROUP_AUTO_SYNC = true;	
	private static final boolean DEFAULT_PORT_FORWARDING_ENABLED = false;
	private static final boolean DEFAULT_GETTING_STARTED_ENABLED = true;
    private static final boolean DEFAULT_URL_REDIRECTION_ENABLED = false;
	private static final int DEFAULT_PORT = 80;
	private static final int DEFAULT_HTTPS_PORT = 0;
    private static final UrlRedirectType DEFAULT_URL_REDIRECT_TYPE = UrlRedirectType.CUSTOM;
	private static final String DEFAULT_URL_REDIRECT_FROM = "yourname";
    private static final String DEFAULT_URL_REDIRECT_CONTEXT_PATH = System.getProperty("madsonic.contextPath", "").replaceAll("/", "");
    private static final String DEFAULT_URL_REDIRECT_CUSTOM_URL = "http://localhost:4040";
    private static final boolean DEFAULT_APP_REDIRECT_ENABLED = false;
	private static final String DEFAULT_SERVER_ID = null;
	private static final long DEFAULT_SETTINGS_CHANGED = 0L;
	private static final boolean DEFAULT_ORGANIZE_BY_FOLDER_STRUCTURE = true;
	private static final boolean DEFAULT_ORGANIZE_BY_GENRE_MAP = false;
	private static final boolean DEFAULT_SHOW_ALBUMS_YEAR = true;
	private static final boolean DEFAULT_SHOW_ALBUMS_YEAR_API = false;
	private static final boolean DEFAULT_SORT_ALBUMS_BY_FOLDER = false;
	private static final boolean DEFAULT_SORT_MEDIAFILEFOLDER = true;
	private static final boolean DEFAULT_HOME_SHARING = true;
	private static final boolean DEFAULT_SHOW_GENERIC_ARTIST_ART = true;
	private static final boolean DEFAULT_SHOW_QUICK_EDIT = false;
	private static final boolean DEFAULT_OWN_GENRE_ENABLED = false;
	private static final boolean DEFAULT_PLAYLIST_ENABLED = false;
	private static final String DEFAULT_USED_VIDEO_PLAYER = "CHROMECAST";
	private static final String DEFAULT_MEDIA_LIBRARY_STATISTICS = "0 0 0 0 0 0 0 0 0 0";
	private static final String DEFAULT_TRIAL_EXPIRES = null;
	
	private static final String DEFAULT_PLAYLIST_EXPORT_MODE = "M3U8";	
	private static final String DEFAULT_SCAN_MODE = "NATIVE";
	private static final boolean DEFAULT_CUSTOM_LOGO = false;	
	
//	@Deprecated private static final boolean DEFAULT_SORT_FILES_BY_FILENAME = false;
	@Deprecated private static final boolean DEFAULT_PLAYQUEUE_RESIZE = true;
	@Deprecated	private static final boolean DEFAULT_LEFTFRAME_RESIZE = false;
	
	private static final int DEFAULT_INDEXLIST_SIZE = 15;
	private static final int DEFAULT_PLAYQUEUE_SIZE = 330;
	private static final int DEFAULT_LEFTFRAME_SIZE = 280;
	private static final boolean DEFAULT_CUSTOMSCROLLBAR = true;
	private static final boolean DEFAULT_CUSTOMACCORDION = false;
	private static final boolean DEFAULT_DLNA_ENABLED = false;
	private static final String DEFAULT_DLNA_SERVER_NAME = "Madsonic DLNA";
    private static final int DEFAULT_SONOS_SERVICE_ID = 244;
    private static final boolean DEFAULT_SONOS_ENABLED = false;
    private static final String DEFAULT_SONOS_SERVICE_NAME = "Madsonic SONOS";
    private static final SonosRatingType DEFAULT_SONOS_RATING_TYPE = SonosRatingType.STARRED;
    private static final boolean DEFAULT_SIGNUP_ENABLED = false;
    private static final String DEFAULT_SIGNUP_SERVICE_NAME = "Madsonic SignUp";
    private static final boolean DEFAULT_EMULATOR_ENABLED = true;
    private static final boolean DEFAULT_NODES_ENABLED = false;
    private static final double DEFAULT_AUDIO_AD_FREQUENCY = 0.20;
    private static final boolean DEFAULT_AUDIO_AD_ENABLED = false;
	private static final boolean DEFAULT_FOLDERPARSING_ENABLED = true;
	private static final boolean DEFAULT_ALBUMSETPARSING_ENABLED = true;
	private static final boolean DEFAULT_LOGFILE_REVERSE = false;
	private static final String DEFAULT_LOGFILE_LEVEL = "INFO";
	private static final boolean DEFAULT_DEBUG_OUTPUT = false;
	private static final boolean DEFAULT_SHOW_CREATELINK = false;
	private static final boolean DEFAULT_SHOW_MEDIATYPE = false;
	
	private static final boolean DEFAULT_ICON_HOME = true;
	private static final boolean DEFAULT_ICON_INDEX = true;
	private static final boolean DEFAULT_ICON_ARTIST = false;
	private static final boolean DEFAULT_ICON_PLAYING = false;
	private static final boolean DEFAULT_ICON_STARRED = true;
	private static final boolean DEFAULT_ICON_LOVED = false;
	private static final boolean DEFAULT_ICON_BOOKMARK = false;
	private static final boolean DEFAULT_ICON_INTERNET_RADIO = true;
	private static final boolean DEFAULT_ICON_RADIO = true;
	private static final boolean DEFAULT_ICON_PODAST = true;
	private static final boolean DEFAULT_ICON_PODAST_MANAGE = false;
	private static final boolean DEFAULT_ICON_SETTINGS = true;
	private static final boolean DEFAULT_ICON_STATUS = false;
	private static final boolean DEFAULT_ICON_SOCIAL = true;
	private static final boolean DEFAULT_ICON_HISTORY = false;
	private static final boolean DEFAULT_ICON_STATISTICS = false;
	private static final boolean DEFAULT_ICON_PLAYLISTS = true;    
	private static final boolean DEFAULT_ICON_PLAYLIST_EDITOR = false;    
	private static final boolean DEFAULT_ICON_PLAYLIST_MANAGE = false;    
	private static final boolean DEFAULT_ICON_MORE = true;    
	private static final boolean DEFAULT_ICON_RANDOM = true;    
	private static final boolean DEFAULT_ICON_ABOUT = true;    
	private static final boolean DEFAULT_ICON_GENRE = true;    
	private static final boolean DEFAULT_ICON_MOODS = false;    
	private static final boolean DEFAULT_ICON_ADMINS = false;    
	private static final boolean DEFAULT_ICON_COVER = true;    
	private static final boolean DEFAULT_HOME_RANDOM = true;   
	private static final boolean DEFAULT_HOME_NEW_ADDED = true; 
	private static final boolean DEFAULT_HOME_HOT_RATED = true;
	private static final boolean DEFAULT_HOME_ALLARTIST = true;
	private static final boolean DEFAULT_HOME_STARRED_ARTIST = true;
	private static final boolean DEFAULT_HOME_STARRED_ALBUM = true;
	private static final boolean DEFAULT_HOME_ALBUMTIP = true;
	private static final boolean DEFAULT_HOME_TOP_RATED = true;
	private static final boolean DEFAULT_HOME_MOSTPLAYED = true;
	private static final boolean DEFAULT_HOME_LASTPLAYED = true;
	private static final boolean DEFAULT_HOME_DECADE = true;
	private static final boolean DEFAULT_HOME_GENRE = true;
	private static final boolean DEFAULT_HOME_NAME = false;
	private static final boolean DEFAULT_HOME_TOP100 = false;
	private static final boolean DEFAULT_HOME_NEW100 = false;
	private static final boolean DEFAULT_HOME_PAGER_TOP = true;
	private static final boolean DEFAULT_HOME_PAGER_BOTTOM = false;
	private static final int DEFAULT_PANDORA_ALBUM = 1;
	private static final int DEFAULT_PANDORA_ARTIST = 2;
	private static final int DEFAULT_PANDORA_ARTIST_TOPTRACK = 3;
	private static final int DEFAULT_PANDORA_GENRE = 8;
	private static final int DEFAULT_PANDORA_MOOD = 10;
	private static final int DEFAULT_PANDORA_SIMILAR = 12;
	public static final int DEFAULT_GROUP_ALL = -1;
	public static final int DEFAULT_GROUP_NONE = 0;
	public static final int DEFAULT_GROUP_MUSIC = 1;
	public static final int DEFAULT_GROUP_VIDEO = 2;
	public static final int DEFAULT_GROUP_MOVIES = 3;
	public static final int DEFAULT_GROUP_SERIES = 4;
	public static final int DEFAULT_GROUP_IMAGES = 5;
	public static final int DEFAULT_GROUP_TV = 6;
	private static final boolean DEFAULT_FEATURE_SNOW = false;
    private static final boolean DEFAULT_ARTIST_BIO_SCAN = false;
	private static final boolean DEFAULT_LASTFM_TOPTRACK_SEARCH = false;
    private static final long DEFAULT_ARTIST_BIO_LAST_UPDATED = 0L;
	private static final int DEFAULT_LASTFM_RESULT_SIZE = 20;
	private static final String DEFAULT_LASTFM_LANGUAGE = "AUTO";
	private static final boolean DEFAULT_RSS_ENABLED = false;

	// Array of obsolete keys.  Used to clean property file.
	private static final List<String> OBSOLETE_KEYS = Arrays.asList("PortForwardingPublicPort", "PortForwardingLocalPort",
			"DownsamplingCommand", "DownsamplingCommand2", "DownsamplingCommand3", "AutoCoverBatch", "MusicMask",
			"VideoMask", "CoverArtMask, HlsCommand3", "HlsCommand2", "JukeboxCommand", "UrlRedirectTrialExpires", 
			"VideoTrialExpires", "PandoraResult", "HTML5Enabled", "MusicFileTypes", "UrlRedirectCustomHost", "MadsonicUrl", "usedVideoPlayer" );
	
    private static final String BACKEND_VALIDATE_KEY_URL = getBackendUrl() + "/backend/validateLicenseKey.view";
    private static final String BACKEND_VALIDATE_LIC_URL = getBackendUrl() + "/backend/validateLicense.view";
	private static final String LOCALES_FILE = "/org/madsonic/i18n/locales.txt";
	private static final String THEMES_FILE = "/org/madsonic/theme/themes.txt";
	private static final Logger LOG = Logger.getLogger(SettingsService.class);

	private static Properties properties = new Properties();
	private List<Theme> themes;
	private List<Locale> locales;
	private InternetRadioDao internetRadioDao;
	private MusicFolderDao musicFolderDao;
	private GroupDao groupDao;
	private UserDao userDao;
	private AvatarDao avatarDao;
	private VersionService versionService;
	private TranscodingService transcodingService;	
	
	private String[] cachedCoverArtFileTypesArray;
	private String[] cachedModFileTypesArray;
	private String[] cachedMusicFileTypesArray;
	private String[] cachedVideoFileTypesArray;
	private String[] cachedImageFileTypesArray;
	
	private List<MusicFolder> cachedMusicFolders;

	private static File madsonicHome;
	private static File madsonicUpload;
	private static File madsonicTranscode;
	private static File madsonicConversion;
	
	private String licenseCode = null;
	private boolean licenseValidated = true;
	
	private Date licenseExpires;

	private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> licenseValidationFuture;

    private static final long LICENSE_VALIDATION_DELAY_HOURS = 12;
	private static final long LOCAL_IP_LOOKUP_DELAY_SECONDS = 90;
	
	private String localIpAddress;

	public SettingsService() {
		File propertyFile = getPropertyFile();

		if (propertyFile.exists()) {
			FileInputStream in = null;
			try {
				in = new FileInputStream(propertyFile);
				properties.load(in);
			} catch (Exception x) {
				LOG.error("Unable to read from property file.", x);
			} finally {
				IOUtils.closeQuietly(in);
			}

			// Remove obsolete properties.
			for (Iterator<Object> iterator = properties.keySet().iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();
				if (OBSOLETE_KEYS.contains(key)) {
                    LOG.info("Removing obsolete property [" + key + ']');
					iterator.remove();
				}
			}
		}

		// Start trial.
		if (getTrialExpires() == null) {
			Date expiryDate = new Date(System.currentTimeMillis() + TRIAL_DAYS * 24L * 3600L * 1000L);
			setTrialExpires(expiryDate);
		}

		save(false);
	}

	/**
	 * Register in service locator so that non-Spring objects can access me.
	 * This method is invoked automatically by Spring.
	 * @throws IOException 
	 */
	public void init() throws IOException {
        logServerInfo();
		ServiceLocator.setSettingsService(this);
		ServerVersion localVersion = versionService.getLocalVersion();
		scheduleLocalIpAddressLookup();
        scheduleLicenseValidation();
    }

    private void logServerInfo() {
        LOG.info("Java: " + System.getProperty("java.version") +
                 ", OS: " + System.getProperty("os.name"));
        LOG.info("ffmpeg: " + transcodingService.ffmpegInfo());
        LOG.info("Madsonic scanMode: " + this.getScanMode().toString());
    }
   
	public void save() {
		save(true);
	}

	public void save(boolean updateChangedDate) {
		if (updateChangedDate) {
            setLong(KEY_SETTINGS_CHANGED, System.currentTimeMillis());
		}

		OutputStream out = null;
		try {
			out = new FileOutputStream(getPropertyFile());
			properties.store(out, "Madsonic preferences.  NOTE: This file is automatically generated.");
		} catch (Exception x) {
			LOG.error("Unable to write to property file.", x);
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	private File getPropertyFile() {
		return new File(getMadsonicHome(), "madsonic.properties");
	}

	/**
	 * Returns the Madsonic home directory.
	 *
	 * @return The Madsonic home directory, if it exists.
	 * @throws RuntimeException If directory doesn't exist.
	 */
	public synchronized static File getMadsonicHome() {

		if (madsonicHome != null) {
			return madsonicHome;
		}

		File home = null;
		String overrideHome = Util.checkPath(System.getProperty("madsonic.home"));
		
		if (overrideHome != null) {
			home = new File(overrideHome);
		} else {
			
		    OperatingSystem os = OperatingSystem.userOS;
	        switch (os) {
	            case WINDOWS:    home = MADSONIC_HOME_WINDOWS; break;
	            case OSX:        home = MADSONIC_HOME_MAC; break;
	            case LINUX:      home = MADSONIC_HOME_OTHER; break;
	            case UNKNOWN:    home = MADSONIC_HOME_OTHER; break;
	            default: break;
	        }
		}

		// Attempt to create home directory if it doesn't exist.
		if (!home.exists() || !home.isDirectory()) {
			boolean success = home.mkdirs();
			if (success) {
				madsonicHome = home;
			} else {
				String message = "The directory " + home + " does not exist. Please create it and make it writable. " +
						"(You can override the directory location by specifying -Dmadsonic.home=... when " +
						"starting the servlet container.)";
				System.err.println("ERROR: " + message);
			}
		} else {
			madsonicHome = home;
		}

		return home;
	}



	public synchronized static File getMadsonicUpload() {
		File upload;

		String overrideUpload = System.getProperty("madsonic.defaultUploadFolder");
		if (overrideUpload != null) {
			upload = new File(overrideUpload);
		} else {

			String tmp = properties.getProperty(KEY_UPLOAD_FOLDER, null);

			if (tmp != null) {
				upload = new File(tmp);
				//;
			} else {
				upload = new File(Util.getDefaultUploadFolder());
			}
		}

		// Attempt to create upload directory if it doesn't exist.
		if (!upload.exists() || !upload.isDirectory()) {
			boolean success = upload.mkdirs();
			if (success) {
				madsonicUpload = upload;
			} else {
				String message = "The directory " + upload + " does not exist. Please create it and make it writable. " +
						"(You can override the directory location by specifying -Dmadsonic.defaultUploadFolder=... when " +
						"starting the servlet container.)";
				System.err.println("ERROR: " + message);
			}
		} else {
			madsonicUpload = upload;
		}
		return upload;
	}

	public synchronized static File getMadsonicTranscode() {
		if (madsonicTranscode != null) {
			return madsonicTranscode;
		}
		File defaultTranscode = new File (properties.getProperty(KEY_TRANSCODE_FOLDER, DEFAULT_TRANSCODE_FOLDER.toString()));
		String overrideTranscode = System.getProperty("madsonic.defaultTranscodeFolder");
		
		if (overrideTranscode != null && !overrideTranscode.isEmpty()) {
			defaultTranscode = new File (overrideTranscode);
		}
		madsonicTranscode = defaultTranscode; 

		if (madsonicTranscode.exists()) {
			setMadsonicTranscode(madsonicTranscode.toString());
			LOG.info("write transcode path to property file: " + madsonicTranscode.toString());
		} else {
			LOG.error("failed to find transcode path: " + madsonicTranscode.toString());
		}
		return madsonicTranscode;
	}
	
	public synchronized static File getMadsonicConversion() {
		if (madsonicConversion != null) {
			return madsonicConversion;
		}
		File defaultConversion = new File (properties.getProperty(KEY_CONVERSATION_FOLDER, DEFAULT_CONVERSION_FOLDER.toString()));
		String overrideConversion = System.getProperty("madsonic.defaultConversionFolder");
		
		if (defaultConversion.exists()) {
			if (defaultConversion != null) {
				madsonicConversion = defaultConversion;
			}
		} else {
			madsonicConversion = overrideConversion == null ? DEFAULT_CONVERSION_FOLDER : new File(overrideConversion);
		}
		
		if (madsonicConversion.exists()) {
			setMadsonicTranscode(DEFAULT_CONVERSION_FOLDER.toString());
			LOG.info("write conversion path to property file.");
        } else {
			LOG.error("failed to find conversion path.");
        }
        return madsonicConversion;
	}	
	
	public static void setMadsonicTranscode(String transcodeFolder) {
		File newFolder = new File(transcodeFolder);
		if (newFolder != null) {
			madsonicTranscode = newFolder;
			properties.setProperty(KEY_TRANSCODE_FOLDER, newFolder.toString());
		}
	}
	
	public String getPageTitle() {
		return properties.getProperty(KEY_PAGE_TITLE, DEFAULT_PAGE_TITLE);
	}

	public void setPageTitle(String pageTitle) {
		properties.setProperty(KEY_PAGE_TITLE, pageTitle);
	}

	public String getUploadFolder() {
		return properties.getProperty(KEY_UPLOAD_FOLDER, DEFAULT_UPLOAD_FOLDER); 
	}

	public void setUploadFolder(String uploadFolder) {
		properties.setProperty(KEY_UPLOAD_FOLDER, uploadFolder);
	}

    private int getInt(String key, int defaultValue) {
        return Integer.valueOf(properties.getProperty(key, String.valueOf(defaultValue)));
    }

    private void setInt(String key, int value) {
        setProperty(key, String.valueOf(value));
    }

    private long getLong(String key, long defaultValue) {
        return Long.valueOf(properties.getProperty(key, String.valueOf(defaultValue)));
    }

    private void setLong(String key, long value) {
        setProperty(key, String.valueOf(value));
    }

    private double getDouble(String key, double defaultValue) {
        return Double.valueOf(properties.getProperty(key, String.valueOf(defaultValue)));
    }

    private void setDouble(String key, double value) {
        setProperty(key, String.valueOf(value));
    }

	private boolean getBoolean(String key, boolean defaultValue) {
		return Boolean.valueOf(properties.getProperty(key, String.valueOf(defaultValue)));
	}

	private void setBoolean(String key, boolean value) {
		setProperty(key, String.valueOf(value));
	}

	private String getString(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}

	private void setString(String key, String value) {
		setProperty(key, value);
	}

	public String getAllIndexString() {
		return properties.getProperty(KEY_ALL_INDEX_STRING, DEFAULT_ALL_INDEX_STRING);
	}

	public void setAllIndexString(String indexString) {
		setProperty(KEY_ALL_INDEX_STRING, indexString);
	}    

	public String getMusicIndexString() {
		return properties.getProperty(KEY_MUSIC_INDEX_STRING, DEFAULT_MUSIC_INDEX_STRING);
	}

	public void setMusicIndexString(String indexString) {
		setProperty(KEY_MUSIC_INDEX_STRING, indexString);
	}

	public String getVideoIndexString() {
		return properties.getProperty(KEY_VIDEO_INDEX_STRING, DEFAULT_VIDEO_INDEX_STRING);
	}

	public void setVideoIndexString(String indexString) {
		setProperty(KEY_VIDEO_INDEX_STRING, indexString);
	}    


	public String getImageIndexString() {
		return properties.getProperty(KEY_IMAGE_INDEX_STRING, DEFAULT_IMAGE_INDEX_STRING);
	}

	public void setImageIndexString(String indexString) {
		setProperty(KEY_IMAGE_INDEX_STRING, indexString);
	}  	
	
	public String getIndex1String() {
		return properties.getProperty(KEY_INDEX1_STRING, DEFAULT_INDEX1_STRING);
	}

	public String getIndex0String() {
		return properties.getProperty(KEY_INDEX0_STRING, DEFAULT_INDEX0_STRING);
	}
	
	public void setIndex1String(String indexString) {
		setProperty(KEY_INDEX1_STRING, indexString);
	}

	public String getIndex2String() {
		return properties.getProperty(KEY_INDEX2_STRING, DEFAULT_INDEX2_STRING);
	}

	public void setIndex2String(String indexString) {
		setProperty(KEY_INDEX2_STRING, indexString);
	}

	public int getPandoraResultArtistTopTrack() {
		return Integer.valueOf(properties.getProperty(KEY_PANDORA_ARTIST_TOPTRACK, String.valueOf(DEFAULT_PANDORA_ARTIST_TOPTRACK)));
	}

	public void setPandoraResultArtistTopTrack(int size) {
		setProperty(KEY_PANDORA_ARTIST_TOPTRACK, String.valueOf(size));
	}

	public int getPandoraResultAlbum() {
		return Integer.valueOf(properties.getProperty(KEY_PANDORA_ALBUM, String.valueOf(DEFAULT_PANDORA_ALBUM)));
	}

	public void setPandoraResultAlbum(int size) {
		setProperty(KEY_PANDORA_ALBUM, String.valueOf(size));
	}

	public int getPandoraResultArtist() {
		return Integer.valueOf(properties.getProperty(KEY_PANDORA_ARTIST, String.valueOf(DEFAULT_PANDORA_ARTIST)));
	}

	public void setPandoraResultArtist(int size) {
		setProperty(KEY_PANDORA_ARTIST, String.valueOf(size));
	}
	public int getPandoraResultGenre() {
		return Integer.valueOf(properties.getProperty(KEY_PANDORA_GENRE, String.valueOf(DEFAULT_PANDORA_GENRE)));
	}

	public void setPandoraResultGenre(int size) {
		setProperty(KEY_PANDORA_GENRE, String.valueOf(size));
	}
	public int getPandoraResultMood() {
		return Integer.valueOf(properties.getProperty(KEY_PANDORA_MOOD, String.valueOf(DEFAULT_PANDORA_MOOD)));
	}

	public void setPandoraResultMood(int size) {
		setProperty(KEY_PANDORA_MOOD, String.valueOf(size));
	}

	public int getPandoraResultSimilar() {
		return Integer.valueOf(properties.getProperty(KEY_PANDORA_SIMILAR, String.valueOf(DEFAULT_PANDORA_SIMILAR)));
	}

	public void setPandoraResultSimilar(int size) {
		setProperty(KEY_PANDORA_SIMILAR, String.valueOf(size));
	}

	public String getIndex3String() {
		return properties.getProperty(KEY_INDEX3_STRING, DEFAULT_INDEX3_STRING);
	}

	public void setIndex3String(String indexString) {
		setProperty(KEY_INDEX3_STRING, indexString);
	}

	public String getIndex4String() {
		return properties.getProperty(KEY_INDEX4_STRING, DEFAULT_INDEX4_STRING);
	}

	public void setIndex4String(String indexString) {
		setProperty(KEY_INDEX4_STRING, indexString);
	}


	public String getIgnoredArticles() {
		return properties.getProperty(KEY_IGNORED_ARTICLES, DEFAULT_IGNORED_ARTICLES);
	}

	public String[] getIgnoredArticlesAsArray() {
		return getIgnoredArticles().split("\\s+");
	}

	public void setIgnoredArticles(String ignoredArticles) {
		setProperty(KEY_IGNORED_ARTICLES, ignoredArticles);
	}

	public String getShortcuts() {
		return properties.getProperty(KEY_SHORTCUTS, DEFAULT_SHORTCUTS);
	}

	public String[] getShortcutsAsArray() {
		return StringUtil.split(getShortcuts());
	}

	public void setShortcuts(String shortcuts) {
		setProperty(KEY_SHORTCUTS, shortcuts);
	}

	public String getPlaylistImportFolder() {
		return properties.getProperty(KEY_PLAYLIST_IMPORT_FOLDER, DEFAULT_PLAYLIST_IMPORT_FOLDER);
	}

	public void setPlaylistImportFolder(String playlistFolder) {
		setProperty(KEY_PLAYLIST_IMPORT_FOLDER, playlistFolder);
	}


	public void setPlaylistExportFolder(String playlistExportFolder) {
		setProperty(KEY_PLAYLIST_EXPORT_FOLDER, playlistExportFolder);
	}

	public String getPlaylistExportFolder() {
		return properties.getProperty(KEY_PLAYLIST_EXPORT_FOLDER, DEFAULT_PLAYLIST_EXPORT_FOLDER);
	}

	public void setPlaylistBackupFolder(String playlistExportFolder) {
		setProperty(KEY_PLAYLIST_BACKUP_FOLDER, playlistExportFolder);
	}

	public String getPlaylistBackupFolder() {
		return properties.getProperty(KEY_PLAYLIST_BACKUP_FOLDER, DEFAULT_PLAYLIST_BACKUP_FOLDER);
	}    

	public String getModFileTypes() {
		return properties.getProperty(KEY_MOD_FILE_TYPES, DEFAULT_MOD_FILE_TYPES);
	}

	public synchronized void setModFileTypes(String fileTypes) {
		setProperty(KEY_MOD_FILE_TYPES, fileTypes);
		cachedModFileTypesArray = null;
	}

	public synchronized String[] getModFileTypesAsArray() {
		if (cachedModFileTypesArray == null) {
			cachedModFileTypesArray = toStringArray(getModFileTypes());
		}
		return cachedModFileTypesArray;
	}

	public String getMusicFileTypes() {
		return properties.getProperty(KEY_MUSIC_FILE_TYPES, DEFAULT_MUSIC_FILE_TYPES);
	}

	public synchronized void setMusicFileTypes(String fileTypes) {
		setProperty(KEY_MUSIC_FILE_TYPES, fileTypes);
		cachedMusicFileTypesArray = null;
	}

	public synchronized String[] getMusicFileTypesAsArray() {
		if (cachedMusicFileTypesArray == null) {
			cachedMusicFileTypesArray = toStringArray(getMusicFileTypes());
		}
		return cachedMusicFileTypesArray;
	}

	public String getVideoFileTypes() {
		return properties.getProperty(KEY_VIDEO_FILE_TYPES, DEFAULT_VIDEO_FILE_TYPES);
	}

	public synchronized void setVideoFileTypes(String fileTypes) {
		setProperty(KEY_VIDEO_FILE_TYPES, fileTypes);
		cachedVideoFileTypesArray = null;
	}

	public synchronized String[] getVideoFileTypesAsArray() {
		if (cachedVideoFileTypesArray == null) {
			cachedVideoFileTypesArray = toStringArray(getVideoFileTypes());
		}
		return cachedVideoFileTypesArray;
	}

	public String getImageFileTypes() {
		return properties.getProperty(KEY_IMAGE_FILE_TYPES, DEFAULT_IMAGE_FILE_TYPES);
	}

	public synchronized void setImageFileTypes(String fileTypes) {
		setProperty(KEY_IMAGE_FILE_TYPES, fileTypes);
		cachedImageFileTypesArray = null;
	}

	public synchronized String[] getImageFileTypesAsArray() {
		if (cachedImageFileTypesArray == null) {
			cachedImageFileTypesArray = toStringArray(getImageFileTypes());
		}
		return cachedImageFileTypesArray;
	}	
	
	public String getCoverArtFileTypes() {
		return properties.getProperty(KEY_COVER_ART_FILE_TYPES, DEFAULT_COVER_ART_FILE_TYPES);
	}

	public synchronized void setCoverArtFileTypes(String fileTypes) {
		setProperty(KEY_COVER_ART_FILE_TYPES, fileTypes);
		cachedCoverArtFileTypesArray = null;
	}

	public synchronized String[] getCoverArtFileTypesAsArray() {
		if (cachedCoverArtFileTypesArray == null) {
			cachedCoverArtFileTypesArray = toStringArray(getCoverArtFileTypes());
		}
		return cachedCoverArtFileTypesArray;
	}

	public int getCoverArtLimit() {
        return getInt(KEY_COVER_ART_LIMIT, DEFAULT_COVER_ART_LIMIT);
	}

	public void setCoverArtLimit(int limit) {
        setInt(KEY_COVER_ART_LIMIT, limit);
    }

    public int getCoverArtConcurrency() {
        return getInt(KEY_COVER_ART_CONCURRENCY, DEFAULT_COVER_ART_CONCURRENCY);
	}

	public String getWelcomeTitle() {
		return StringUtils.trimToNull(properties.getProperty(KEY_WELCOME_TITLE, DEFAULT_WELCOME_TITLE));
	}

	public void setWelcomeTitle(String title) {
		setProperty(KEY_WELCOME_TITLE, title);
	}

	public String getWelcomeSubtitle() {
		return StringUtils.trimToNull(properties.getProperty(KEY_WELCOME_SUBTITLE, DEFAULT_WELCOME_SUBTITLE));
	}

	public void setWelcomeSubtitle(String subtitle) {
		setProperty(KEY_WELCOME_SUBTITLE, subtitle);
	}

	public String getWelcomeMessage() {
		return StringUtils.trimToNull(properties.getProperty(KEY_WELCOME_MESSAGE, DEFAULT_WELCOME_MESSAGE));
	}

	public void setWelcomeMessage(String message) {
		setProperty(KEY_WELCOME_MESSAGE, message);
	}

	public String getAdminMessage() {
		return StringUtils.trimToNull(properties.getProperty(KEY_ADMIN_MESSAGE, DEFAULT_ADMIN_MESSAGE));
	}

	public void setAdminMessage(String message) {
		setProperty(KEY_ADMIN_MESSAGE, message);
	}	
	
	public String getLoginMessage() {
		return StringUtils.trimToNull(properties.getProperty(KEY_LOGIN_MESSAGE, DEFAULT_LOGIN_MESSAGE));
	}

	public void setLoginMessage(String message) {
		setProperty(KEY_LOGIN_MESSAGE, message);
	}

	/**
	 * Returns the number of days between automatic index creation, of -1 if automatic index
	 * creation is disabled.
	 */
	public int getIndexCreationInterval() {
        return getInt(KEY_INDEX_CREATION_INTERVAL, DEFAULT_INDEX_CREATION_INTERVAL);
	}

	/**
	 * Sets the number of days between automatic index creation, of -1 if automatic index
	 * creation is disabled.
	 */
	public void setIndexCreationInterval(int days) {
        setInt(KEY_INDEX_CREATION_INTERVAL, days);
	}

	/**
	 * Returns the hour of day (0 - 23) when automatic index creation should run.
	 */
	public int getIndexCreationHour() {
        return getInt(KEY_INDEX_CREATION_HOUR, DEFAULT_INDEX_CREATION_HOUR);
	}

	/**
	 * Sets the hour of day (0 - 23) when automatic index creation should run.
	 */
	public void setIndexCreationHour(int hour) {
        setInt(KEY_INDEX_CREATION_HOUR, hour);
	}

	public boolean isFastCacheEnabled() {
		return getBoolean(KEY_FAST_CACHE_ENABLED, DEFAULT_FAST_CACHE_ENABLED);
	}

	public void setFastCacheEnabled(boolean enabled) {
		setBoolean(KEY_FAST_CACHE_ENABLED, enabled);
	}

	/**
	 * Returns the number of hours between Podcast updates, of -1 if automatic updates
	 * are disabled.
	 */
	public int getPodcastUpdateInterval() {
        return getInt(KEY_PODCAST_UPDATE_INTERVAL, DEFAULT_PODCAST_UPDATE_INTERVAL);
	}

	/**
	 * Sets the number of hours between Podcast updates, of -1 if automatic updates
	 * are disabled.
	 */
	public void setPodcastUpdateInterval(int hours) {
        setInt(KEY_PODCAST_UPDATE_INTERVAL, hours);
	}

	/**
	 * Returns the number of Podcast episodes to keep (-1 to keep all).
	 */
	public int getPodcastEpisodeRetentionCount() {
        return getInt(KEY_PODCAST_EPISODE_RETENTION_COUNT, DEFAULT_PODCAST_EPISODE_RETENTION_COUNT);
	}

	/**
	 * Sets the number of Podcast episodes to keep (-1 to keep all).
	 */
	public void setPodcastEpisodeRetentionCount(int count) {
        setInt(KEY_PODCAST_EPISODE_RETENTION_COUNT, count);
	}

	/**
	 * Returns the number of Podcast episodes to download (-1 to download all).
	 */
	public int getPodcastEpisodeDownloadCount() {
        return getInt(KEY_PODCAST_EPISODE_DOWNLOAD_COUNT, DEFAULT_PODCAST_EPISODE_DOWNLOAD_COUNT);
	}

	/**
	 * Sets the number of Podcast episodes to download (-1 to download all).
	 */
	public void setPodcastEpisodeDownloadCount(int count) {
        setInt(KEY_PODCAST_EPISODE_DOWNLOAD_COUNT, count);
	}

	public int getPodcastEpisodeDownloadLimit() {
		return getInt(KEY_PODCAST_EPISODE_DOWNLOAD_LIMIT, DEFAULT_PODCAST_EPISODE_DOWNLOAD_LIMIT);
	}

	public void setPodcastEpisodeDownloadLimit(int count) {
		setInt(KEY_PODCAST_EPISODE_DOWNLOAD_LIMIT, count);
	}    

	/**
	 * Returns the Podcast download folder.
	 */
	public String getPodcastFolder() {
		return properties.getProperty(KEY_PODCAST_FOLDER, DEFAULT_PODCAST_FOLDER);
	}

	/**
	 * Sets the Podcast download folder.
	 */
	public void setPodcastFolder(String folder) {
		setProperty(KEY_PODCAST_FOLDER, folder);
	}

	/**
	 * @return The download bitrate limit in Kbit/s. Zero if unlimited.
	 */
	public long getDownloadBitrateLimit() {
		return Long.parseLong(properties.getProperty(KEY_DOWNLOAD_BITRATE_LIMIT, "" + DEFAULT_DOWNLOAD_BITRATE_LIMIT));
	}

	/**
	 * @param limit The download bitrate limit in Kbit/s. Zero if unlimited.
	 */
	public void setDownloadBitrateLimit(long limit) {
		setProperty(KEY_DOWNLOAD_BITRATE_LIMIT, "" + limit);
	}

	/**
	 * @return The upload bitrate limit in Kbit/s. Zero if unlimited.
	 */
	public long getUploadBitrateLimit() {
        return getLong(KEY_UPLOAD_BITRATE_LIMIT, DEFAULT_UPLOAD_BITRATE_LIMIT);
	}

	/**
	 * @param limit The upload bitrate limit in Kbit/s. Zero if unlimited.
	 */
	public void setUploadBitrateLimit(long limit) {
        setLong(KEY_UPLOAD_BITRATE_LIMIT, limit);
	}

	/**
	 * @return The non-SSL stream port. Zero if disabled.
	 */
	public int getStreamPort() {
        return getInt(KEY_STREAM_PORT, DEFAULT_STREAM_PORT);
	}

	/**
	 * @param port The non-SSL stream port. Zero if disabled.
	 */
	public void setStreamPort(int port) {
        setInt(KEY_STREAM_PORT, port);
	}

	public String getLicenseEmail() {
		return properties.getProperty(KEY_LICENSE_EMAIL, DEFAULT_LICENSE_EMAIL);
	}

	public void setLicenseEmail(String email) {
		setProperty(KEY_LICENSE_EMAIL, email);
	}

	public String getLicenseCode() {
		return properties.getProperty(KEY_LICENSE_CODE, DEFAULT_LICENSE_CODE);
	}

	public void setLicenseCode(String code) {
		setProperty(KEY_LICENSE_CODE, code);
	}

	public Date getLicenseDate() {
		String value = properties.getProperty(KEY_LICENSE_DATE, DEFAULT_LICENSE_DATE);
		return value == null ? null : new Date(Long.parseLong(value));
	}

	public void setLicenseDate(Date date) {
		String value = (date == null ? null : String.valueOf(date.getTime()));
		setProperty(KEY_LICENSE_DATE, value);
	}

    public boolean isLicenseValid() {
        return isLicenseValid(getLicenseEmail(), getLicenseCode()) && licenseValidated;
    }

    public boolean isLicenseValid(String email, String license) {
        if (email == null || license == null) {
            return false;
        }
        if (licenseCode == null) {
            return isLicenseKeyValid(email, license);
        } 
    	return true; 
    }

    public boolean isLicenseKeyValid(String email, String license) {
        if (email == null || license == null) {
            return false;
        }
        boolean licenseKeyValid = false;
        CloseableHttpClient client = HttpClientBuilder.create().build();
		
		try {
	        String version = versionService.getLocalVersion().toString();
	        HttpGet method = new HttpGet(BACKEND_VALIDATE_KEY_URL + "?email=" + StringUtil.urlEncode(email) + "&key=" + license + "&version=" + version);
	        RequestConfig requestConfig = RequestConfig.custom()
	                .setSocketTimeout(60000)
	                .setConnectTimeout(60000)
	                .setConnectionRequestTimeout(60000)
	                .build();
	        method.setConfig(requestConfig);  			
	        ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String content = client.execute(method, responseHandler);
			licenseKeyValid = content != null && !content.contains("false");
			if (licenseKeyValid) licenseCode = license;
			return licenseKeyValid;
			
        } catch (Throwable x) {
			LOG.warn("Failed to validate Madsonic key.", x);
		} finally {
            try {
				client.close();
			} catch (IOException e) {
	            // ignore 
			}
		}
        return false;
    }    
    
	public LicenseInfo getLicenseInfo() {
		Date trialExpires = getTrialExpires();
		Date now = new Date();
		boolean trialValid = trialExpires.after(now);
		trialValid = trialExpires.before(new Date(System.currentTimeMillis() + TRIAL_DAYS * 24L * 3600L * 1000L - 1L)) ;
		long trialDaysLeft = trialValid ? (trialExpires.getTime() - now.getTime()) / (24L * 3600L * 1000L) : 0L;
		return new LicenseInfo(getLicenseEmail(), isLicenseValid(), trialExpires, trialDaysLeft, licenseExpires);
	}

	public String getDownsamplingCommand() {
		return properties.getProperty(KEY_DOWNSAMPLING_COMMAND, DEFAULT_DOWNSAMPLING_COMMAND);
	}

	public void setDownsamplingCommand(String command) {
		setProperty(KEY_DOWNSAMPLING_COMMAND, command);
	}

	public String getHlsCommand() {
		return properties.getProperty(KEY_HLS_COMMAND, DEFAULT_HLS_COMMAND);
	}

	public void setHlsCommand(String command) {
		setProperty(KEY_HLS_COMMAND, command);
	}

	public String getDashCommand() {
		return properties.getProperty(KEY_DASH_COMMAND, DEFAULT_DASH_COMMAND);
	}

	public void setDashCommand(String command) {
		setProperty(KEY_DASH_COMMAND, command);
	}	
	
	public String getJukeboxCommand() {
		return properties.getProperty(KEY_JUKEBOX_COMMAND, DEFAULT_JUKEBOX_COMMAND);
	}

    public String getVideoImageCommand() {
        return properties.getProperty(KEY_VIDEO_IMAGE_COMMAND, DEFAULT_VIDEO_IMAGE_COMMAND);
    }

    public String getTVImageCommand() {
        return properties.getProperty(KEY_TV_IMAGE_COMMAND, DEFAULT_TV_IMAGE_COMMAND);
    }    
    
	public boolean isShowShortcuts() {
		return getBoolean(KEY_SHOW_SHORTCUTS_ALWAYS, DEFAULT_SHOW_SHORTCUTS_ALWAYS);
	}

	public void setShowShortcuts(boolean b) {
		setBoolean(KEY_SHOW_SHORTCUTS_ALWAYS, b);
	}

	public boolean isShowQuickEdit() {
		return getBoolean(KEY_SHOW_QUICK_EDIT, DEFAULT_SHOW_QUICK_EDIT);
	}

	public void setShowQuickEdit(boolean b) {
		setBoolean(KEY_SHOW_QUICK_EDIT, b);
	}

	public String getUsedVideoPlayer() {
		return properties.getProperty(KEY_USED_VIDEO_PLAYER, DEFAULT_USED_VIDEO_PLAYER);
	}    

	public void setUsedVideoPlayer(String s) {
		setString(KEY_USED_VIDEO_PLAYER, s);
	}

	public boolean isOwnGenreEnabled() {
		return getBoolean(KEY_OWN_GENRE_ENABLED, DEFAULT_OWN_GENRE_ENABLED);
	}

	public void setOwnGenreEnabled(boolean b) {
		setBoolean(KEY_OWN_GENRE_ENABLED, b);
	}

	public boolean isPlaylistEnabled() {
		return getBoolean(KEY_PLAYLIST_ENABLED, DEFAULT_PLAYLIST_ENABLED);
	}

	public void setPlaylistEnabled(boolean b) {
		setBoolean(KEY_PLAYLIST_ENABLED, b);
	}

	public boolean isRewriteUrlEnabled() {
		return getBoolean(KEY_REWRITE_URL, DEFAULT_REWRITE_URL);
	}

	public void setRewriteUrlEnabled(boolean rewriteUrl) {
		setBoolean(KEY_REWRITE_URL, rewriteUrl);
	}

	public boolean isLdapEnabled() {
		return getBoolean(KEY_LDAP_ENABLED, DEFAULT_LDAP_ENABLED);
	}

	public void setLdapEnabled(boolean ldapEnabled) {
		setBoolean(KEY_LDAP_ENABLED, ldapEnabled);
	}

	public String getLdapUrl() {
		return properties.getProperty(KEY_LDAP_URL, DEFAULT_LDAP_URL);
	}

	public void setLdapUrl(String ldapUrl) {
		properties.setProperty(KEY_LDAP_URL, ldapUrl);
	}

	public String getLdapSearchFilter() {
		return properties.getProperty(KEY_LDAP_SEARCH_FILTER, DEFAULT_LDAP_SEARCH_FILTER);
	}

	public void setLdapSearchFilter(String ldapSearchFilter) {
		properties.setProperty(KEY_LDAP_SEARCH_FILTER, ldapSearchFilter);
	}


	public String getLdapGroupSearchBase() {
		return properties.getProperty(KEY_LDAP_GROUP_SEARCH_BASE, DEFAULT_LDAP_GROUP_SEARCH_BASE);
	}

	public void setLdapGroupSearchBase(String ldapGroupSearchBase) {
		properties.setProperty(KEY_LDAP_GROUP_SEARCH_BASE, ldapGroupSearchBase);
	}	
	
	public String getLdapGroupFilter() {
		return properties.getProperty(KEY_LDAP_GROUP_FILTER, DEFAULT_LDAP_GROUP_FILTER);
	}

	public void setLdapGroupFilter(String ldapGroupFilter) {
		properties.setProperty(KEY_LDAP_GROUP_FILTER, ldapGroupFilter);
	}

	public String getLdapGroupAttrib() {
		return properties.getProperty(KEY_LDAP_GROUP_ATTRIB, DEFAULT_LDAP_GROUP_ATTRIB);
	}

	public void setLdapGroupAttrib(String ldapGroupAttrib) {
		properties.setProperty(KEY_LDAP_GROUP_ATTRIB, ldapGroupAttrib);
	}
	
	public String getLdapManagerDn() {
		return properties.getProperty(KEY_LDAP_MANAGER_DN, DEFAULT_LDAP_MANAGER_DN);
	}

	public void setLdapManagerDn(String ldapManagerDn) {
		properties.setProperty(KEY_LDAP_MANAGER_DN, ldapManagerDn);
	}

	public String getLdapManagerPassword() {
		String s = properties.getProperty(KEY_LDAP_MANAGER_PASSWORD, DEFAULT_LDAP_MANAGER_PASSWORD);
		try {
			return StringUtil.utf8HexDecode(s);
		} catch (Exception x) {
			LOG.warn("Failed to decode LDAP manager password.", x);
			return s;
		}
	}

	public void setLdapManagerPassword(String ldapManagerPassword) {
		try {
			ldapManagerPassword = StringUtil.utf8HexEncode(ldapManagerPassword);
		} catch (Exception x) {
			LOG.warn("Failed to encode LDAP manager password.", x);
		}
		properties.setProperty(KEY_LDAP_MANAGER_PASSWORD, ldapManagerPassword);
	}

	public boolean isLdapAutoShadowing() {
		return getBoolean(KEY_LDAP_AUTO_SHADOWING, DEFAULT_LDAP_AUTO_SHADOWING);
	}

	public void setLdapAutoShadowing(boolean ldapAutoShadowing) {
		setBoolean(KEY_LDAP_AUTO_SHADOWING, ldapAutoShadowing);
	}

	public boolean isLdapAutoMapping() {
		return getBoolean(KEY_LDAP_AUTO_MAPPING, DEFAULT_LDAP_AUTO_MAPPING);
	}

	public void setLdapAutoMapping(boolean ldapAutoMapping) {
		setBoolean(KEY_LDAP_AUTO_MAPPING, ldapAutoMapping);
	}	
	
    public LdapGroupSyncType getLdapGroupSyncType() {
        return LdapGroupSyncType.valueOf(properties.getProperty(KEY_LDAP_GROUP_SYNC_TYPE, DEFAULT_LDAP_GROUP_SYNC_TYPE.name()));
    }

    public void setLdapGroupSyncType(LdapGroupSyncType ldapGroupSyncType) {
        properties.setProperty(KEY_LDAP_GROUP_SYNC_TYPE, ldapGroupSyncType.name());
    }	
	
	public boolean isLdapGroupAutoSync() {
		return getBoolean(KEY_LDAP_GROUP_AUTO_SYNC, DEFAULT_LDAP_GROUP_AUTO_SYNC);
	}

	public void setLdapGroupAutoSync(boolean ldapGroupAutoSync) {
		setBoolean(KEY_LDAP_GROUP_AUTO_SYNC, ldapGroupAutoSync);
	}	
	
	public boolean isGettingStartedEnabled() {
		return getBoolean(KEY_GETTING_STARTED_ENABLED, DEFAULT_GETTING_STARTED_ENABLED);
	}

	public void setGettingStartedEnabled(boolean isGettingStartedEnabled) {
		setBoolean(KEY_GETTING_STARTED_ENABLED, isGettingStartedEnabled);
	}

	public boolean isPortForwardingEnabled() {
		return getBoolean(KEY_PORT_FORWARDING_ENABLED, DEFAULT_PORT_FORWARDING_ENABLED);
	}

	public void setPortForwardingEnabled(boolean isPortForwardingEnabled) {
		setBoolean(KEY_PORT_FORWARDING_ENABLED, isPortForwardingEnabled);
	}

	public int getPort() {
        return getInt(KEY_PORT, DEFAULT_PORT);
	}

	public void setPort(int port) {
        setInt(KEY_PORT, port);
	}

	public int getHttpsPort() {
        return getInt(KEY_HTTPS_PORT, DEFAULT_HTTPS_PORT);
	}

	public void setHttpsPort(int httpsPort) {
        setInt(KEY_HTTPS_PORT, httpsPort);
	}

	public boolean isUrlRedirectionEnabled() {
		return getBoolean(KEY_URL_REDIRECTION_ENABLED, DEFAULT_URL_REDIRECTION_ENABLED);
	}

	public void setUrlRedirectionEnabled(boolean isUrlRedirectionEnabled) {
		setBoolean(KEY_URL_REDIRECTION_ENABLED, isUrlRedirectionEnabled);
	}

    public String getUrlRedirectUrl() {
        if (getUrlRedirectType() == UrlRedirectType.NORMAL) {
            return "http://" + getUrlRedirectFrom() + ".madsonic.org";
        }
        return StringUtils.removeEnd(getUrlRedirectCustomUrl(), "/");
    }

	public String getUrlRedirectFrom() {
		return properties.getProperty(KEY_URL_REDIRECT_FROM, DEFAULT_URL_REDIRECT_FROM);
	}

	public void setUrlRedirectFrom(String urlRedirectFrom) {
		properties.setProperty(KEY_URL_REDIRECT_FROM, urlRedirectFrom);
	}

    public UrlRedirectType getUrlRedirectType() {
        return UrlRedirectType.valueOf(properties.getProperty(KEY_URL_REDIRECT_TYPE, DEFAULT_URL_REDIRECT_TYPE.name()));
    }

    public void setUrlRedirectType(UrlRedirectType urlRedirectType) {
        properties.setProperty(KEY_URL_REDIRECT_TYPE, urlRedirectType.name());
    }
    
	public Date getTrialExpires() {
		String value = properties.getProperty(KEY_TRIAL_EXPIRES, DEFAULT_TRIAL_EXPIRES);
		return value == null ? null : new Date(Long.parseLong(value));
	}

    public void setTrialExpires(Date date) {
        String value = (date == null ? null : String.valueOf(date.getTime()));
        setProperty(KEY_TRIAL_EXPIRES, value);
    }

	public String getUrlRedirectContextPath() {
		return properties.getProperty(KEY_URL_REDIRECT_CONTEXT_PATH, DEFAULT_URL_REDIRECT_CONTEXT_PATH);
	}

	public void setUrlRedirectContextPath(String contextPath) {
		properties.setProperty(KEY_URL_REDIRECT_CONTEXT_PATH, contextPath);
	}

    public String getUrlRedirectCustomUrl() {
        return StringUtils.trimToNull(properties.getProperty(KEY_URL_REDIRECT_CUSTOM_URL, DEFAULT_URL_REDIRECT_CUSTOM_URL));
    }

    public void setUrlRedirectCustomUrl(String customUrl) {
        properties.setProperty(KEY_URL_REDIRECT_CUSTOM_URL, customUrl);
    }

    public boolean isAppRedirectEnabled() {
        return getBoolean(KEY_APP_REDIRECT_ENABLED, DEFAULT_APP_REDIRECT_ENABLED);
    }

    public void setAppRedirectEnabled(boolean enabled) {
        setBoolean(KEY_APP_REDIRECT_ENABLED, enabled);
    }    
    
	public String getServerId() {
		return properties.getProperty(KEY_SERVER_ID, DEFAULT_SERVER_ID);
	}

	public void setServerId(String serverId) {
		properties.setProperty(KEY_SERVER_ID, serverId);
	}

    public double getAudioAdFrequency() {
        return getDouble(KEY_AUDIO_AD_FREQUENCY, DEFAULT_AUDIO_AD_FREQUENCY);
    }

    public void setAudioAdFrequency(double frequency) {
        setDouble(KEY_AUDIO_AD_FREQUENCY, frequency);
    }

    public boolean isAudioAdEnabled() {
        return getBoolean(KEY_AUDIO_AD_ENABLED, DEFAULT_AUDIO_AD_ENABLED);
    }

    public void setAudioAdEnabled(boolean enabled) {
        setBoolean(KEY_AUDIO_AD_ENABLED, enabled);
    }
    
	public long getSettingsChanged() {
        return getLong(KEY_SETTINGS_CHANGED, DEFAULT_SETTINGS_CHANGED);
	}

	public Date getLastScanned() {
		String lastScanned = properties.getProperty(KEY_LAST_SCANNED);
		return lastScanned == null ? null : new Date(Long.parseLong(lastScanned));
	}

	public void setLastScanned(Date date) {
		if (date == null) {
			properties.remove(KEY_LAST_SCANNED);
		} else {
            setLong(KEY_LAST_SCANNED, date.getTime());
		}
	}

	public boolean isOrganizeByFolderStructure() {
		return getBoolean(KEY_ORGANIZE_BY_FOLDER_STRUCTURE, DEFAULT_ORGANIZE_BY_FOLDER_STRUCTURE);
	}

	public void setOrganizeByFolderStructure(boolean b) {
		setBoolean(KEY_ORGANIZE_BY_FOLDER_STRUCTURE, b);
	}

	public boolean isOrganizeByGenreMap() {
		return getBoolean(KEY_ORGANIZE_BY_GENRE_MAP, DEFAULT_ORGANIZE_BY_GENRE_MAP);
	}

	public void setOrganizeByGenreMap(boolean b) {
		setBoolean(KEY_ORGANIZE_BY_GENRE_MAP, b);
	}
	
	public boolean isShowAlbumsYear() {
		return getBoolean(KEY_SHOW_ALBUMS_YEAR, DEFAULT_SHOW_ALBUMS_YEAR);
	}

	public void setShowAlbumsYear(boolean b) {
		setBoolean(KEY_SHOW_ALBUMS_YEAR, b);
	}

	public boolean isShowAlbumsYearApi() {
		return getBoolean(KEY_SHOW_ALBUMS_YEAR_API, DEFAULT_SHOW_ALBUMS_YEAR_API);
	}

	public void setShowAlbumsYearApi(boolean b) {
		setBoolean(KEY_SHOW_ALBUMS_YEAR_API, b);
	}

	public boolean isSortAlbumsByFolder() {
		return getBoolean(KEY_SORT_ALBUMS_BY_FOLDER, DEFAULT_SORT_ALBUMS_BY_FOLDER);
	}

	public void setSortAlbumsByFolder(boolean b) {
		setBoolean(KEY_SORT_ALBUMS_BY_FOLDER, b);
	}

//	public boolean isSortFilesByFilename() {
//		return getBoolean(KEY_SORT_FILES_BY_FILENAME, DEFAULT_SORT_FILES_BY_FILENAME);
//	}

//	public void setSortFilesByFilename(boolean b) {
//		setBoolean(KEY_SORT_FILES_BY_FILENAME, b);
//	}

	public boolean isCoverArtHQ() {
		return getBoolean(KEY_COVER_ART_HQ, DEFAULT_COVER_ART_HQ);
	}

	public void setCoverArtHQ(boolean b) {
		setBoolean(KEY_COVER_ART_HQ, b);
	}    

	public boolean isRssEnabled() {
		return getBoolean(KEY_RSS_ENABLED, DEFAULT_RSS_ENABLED);
	}

	public void setRssEnabled(boolean isRssEnabled) {
		setBoolean(KEY_RSS_ENABLED, isRssEnabled);
	}
	
	
	public boolean isSortMediaFileFolder() {
		return getBoolean(KEY_SORT_MEDIAFILEFOLDER, DEFAULT_SORT_MEDIAFILEFOLDER);
	}

	public void setSortMediaFileFolder(boolean b) {
		setBoolean(KEY_SORT_MEDIAFILEFOLDER, b);
	}    

    public String getScanMode() {
    	return properties.getProperty(KEY_SCAN_MODE, DEFAULT_SCAN_MODE); 
    }
    
    public void setScanMode(String scanMode) {
    	properties.setProperty(KEY_SCAN_MODE, scanMode);
    }	
    
    public String getPlaylistExportMode() {
    	return properties.getProperty(KEY_PLAYLIST_EXPORT_MODE, DEFAULT_PLAYLIST_EXPORT_MODE); 
    }
    
    public void setPlaylistEportMode(String playlistExportMode) {
    	properties.setProperty(KEY_PLAYLIST_EXPORT_MODE, playlistExportMode);
    }    
    
    
	public boolean isUseHomeSharing() {
		return getBoolean(KEY_HOME_SHARING, DEFAULT_HOME_SHARING);
	}

	public void setUseHomeSharing(boolean b) {
		setBoolean(KEY_HOME_SHARING, b);
	}

	public boolean isShowGenericArtistArt() {
		return getBoolean(KEY_SHOW_GENERIC_ARTIST_ART, DEFAULT_SHOW_GENERIC_ARTIST_ART);
	}

	public void setShowGenericArtistArt(boolean b) {
		setBoolean(KEY_SHOW_GENERIC_ARTIST_ART, b);
	}	

	public int getLeftframeSize() {
		return Integer.valueOf(properties.getProperty(KEY_LEFTFRAME_SIZE, String.valueOf(DEFAULT_LEFTFRAME_SIZE)));
	}

	public void setLeftframeSize(int size) {
		setProperty(KEY_LEFTFRAME_SIZE, String.valueOf(size));
	}

	public boolean showIconHome() {
		return getBoolean(KEY_ICON_HOME, DEFAULT_ICON_HOME);
	}
	
	public void setshowIconHome(boolean b) {
		setBoolean(KEY_ICON_HOME, b);
	}
	
	public boolean showIconIndex() {
		return getBoolean(KEY_ICON_INDEX, DEFAULT_ICON_INDEX);
	}
	
	public void setshowIconIndex(boolean b) {
		setBoolean(KEY_ICON_INDEX, b);
	}
	
	public boolean showIconArtist() {
		return getBoolean(KEY_ICON_ARTIST, DEFAULT_ICON_ARTIST);
	}
	
	public void setshowIconArtist(boolean b) {
		setBoolean(KEY_ICON_ARTIST, b);
	}
	
	public boolean showIconPlaying() {
		return getBoolean(KEY_ICON_PLAYING, DEFAULT_ICON_PLAYING);
	}
	
	public void setshowIconPlaying(boolean b) {
		setBoolean(KEY_ICON_PLAYING, b);
	}
	
	public boolean showIconStarred() {
		return getBoolean(KEY_ICON_STARRED, DEFAULT_ICON_STARRED);
	}
	
	public void setshowIconStarred(boolean b) {
		setBoolean(KEY_ICON_STARRED, b);
	}

	public boolean showIconLoved() {
		return getBoolean(KEY_ICON_LOVED, DEFAULT_ICON_LOVED);
	}
	public void setshowIconLoved(boolean b) {
		setBoolean(KEY_ICON_LOVED, b);
	}	
	
	public boolean showIconBookmark() {
		return getBoolean(KEY_ICON_BOOKMARK, DEFAULT_ICON_BOOKMARK);
	}
	public void setshowIconBookmark(boolean b) {
		setBoolean(KEY_ICON_BOOKMARK, b);
	} 	

	public boolean showIconInternetRadio() {
		return getBoolean(KEY_ICON_INTERNET_RADIO, DEFAULT_ICON_INTERNET_RADIO);
	}
	public void setshowIconInternetRadio(boolean b) {
		setBoolean(KEY_ICON_INTERNET_RADIO, b);
	} 	
	
	public boolean showIconRadio() {
		return getBoolean(KEY_ICON_RADIO, DEFAULT_ICON_RADIO);
	}
	public void setshowIconRadio(boolean b) {
		setBoolean(KEY_ICON_RADIO, b);
	}
	
	public boolean showIconPodcast() {
		return getBoolean(KEY_ICON_PODAST, DEFAULT_ICON_PODAST);
	}
	
	public void setshowIconPodcast(boolean b) {
		setBoolean(KEY_ICON_PODAST, b);
	}
	
	public boolean showIconPodcastManage() {
		return getBoolean(KEY_ICON_PODAST_MANAGE, DEFAULT_ICON_PODAST_MANAGE);
	}
	
	public void setshowIconPodcastManage(boolean b) {
		setBoolean(KEY_ICON_PODAST_MANAGE, b);
	}
	
	public boolean showIconSettings() {
		return getBoolean(KEY_ICON_SETTINGS, DEFAULT_ICON_SETTINGS);
	}
	
	public void setshowIconSettings(boolean b) {
		setBoolean(KEY_ICON_SETTINGS, b);
	}
	
	public boolean showIconStatus() {
		return getBoolean(KEY_ICON_STATUS, DEFAULT_ICON_STATUS);
	}
	
	public void setshowIconStatus(boolean b) {
		setBoolean(KEY_ICON_STATUS, b);
	}
	
	public boolean showIconSocial() {
		return getBoolean(KEY_ICON_SOCIAL, DEFAULT_ICON_SOCIAL);
	}
	public void setshowIconSocial(boolean b) {
		setBoolean(KEY_ICON_SOCIAL, b);
	}
	public boolean showIconHistory() {
		return getBoolean(KEY_ICON_HISTORY, DEFAULT_ICON_HISTORY);
	}
	public void setshowIconHistory(boolean b) {
		setBoolean(KEY_ICON_HISTORY, b);
	}        
	public boolean showIconStatistics() {
		return getBoolean(KEY_ICON_STATISTICS, DEFAULT_ICON_STATISTICS);
	}
	public void setshowIconStatistics(boolean b) {
		setBoolean(KEY_ICON_STATISTICS, b);
	}        
	public boolean showIconPlaylists() {
		return getBoolean(KEY_ICON_PLAYLISTS, DEFAULT_ICON_PLAYLISTS);
	}
	public void setshowIconPlaylists(boolean b) {
		setBoolean(KEY_ICON_PLAYLISTS, b);
	}        
	public boolean showIconPlaylistEditor() {
		return getBoolean(KEY_ICON_PLAYLIST_EDITOR, DEFAULT_ICON_PLAYLIST_EDITOR);
	}
	public void setshowIconPlaylistEditor(boolean b) {
		setBoolean(KEY_ICON_PLAYLIST_EDITOR, b);
	}
	public boolean showIconPlaylistManage() {
		return getBoolean(KEY_ICON_PLAYLIST_MANAGE, DEFAULT_ICON_PLAYLIST_MANAGE);
	}
	public void setshowIconPlaylistManage(boolean b) {
		setBoolean(KEY_ICON_PLAYLIST_MANAGE, b);
	}        
	public boolean showIconMore() {
		return getBoolean(KEY_ICON_MORE, DEFAULT_ICON_MORE);
	}
	public void setshowIconMore(boolean b) {
		setBoolean(KEY_ICON_MORE, b);
	}
	public boolean showIconRandom() {
		return getBoolean(KEY_ICON_RANDOM, DEFAULT_ICON_RANDOM);
	}
	public void setshowIconRandom(boolean b) {
		setBoolean(KEY_ICON_RANDOM, b);
	}        
	public boolean showIconAbout() {
		return getBoolean(KEY_ICON_ABOUT, DEFAULT_ICON_ABOUT);
	}
	public void setshowIconAbout(boolean b) {
		setBoolean(KEY_ICON_ABOUT, b);
	}  
	public boolean showIconGenre() {
		return getBoolean(KEY_ICON_GENRE, DEFAULT_ICON_GENRE);
	}
	public void setshowIconGenre(boolean b) {
		setBoolean(KEY_ICON_GENRE, b);
	}  
	public boolean showIconMoods() {
		return getBoolean(KEY_ICON_MOODS, DEFAULT_ICON_MOODS);
	}
	public void setshowIconMoods(boolean b) {
		setBoolean(KEY_ICON_MOODS, b);
	}      

	public boolean showIconCover() {
		return getBoolean(KEY_ICON_COVER, DEFAULT_ICON_COVER);
	}
	public void setshowIconCover(boolean b) {
		setBoolean(KEY_ICON_COVER, b);
	}      

	public boolean showIconAdmins() {
		return getBoolean(KEY_ICON_ADMINS, DEFAULT_ICON_ADMINS);
	}
	public void setshowIconAdmins(boolean b) {
		setBoolean(KEY_ICON_ADMINS, b);
	}      

	public boolean showHomeRandom() {
		return getBoolean(KEY_HOME_RANDOM, DEFAULT_HOME_RANDOM);
	}
	public void setHomeRandom(boolean b) {
		setBoolean(KEY_HOME_RANDOM, b);
	}      

	public boolean showHomeNewAdded() {
		return getBoolean(KEY_HOME_NEW_ADDED, DEFAULT_HOME_NEW_ADDED);
	}
	public void setHomeNewAdded(boolean b) {
		setBoolean(KEY_HOME_NEW_ADDED, b);
	} 

	public boolean showHomeHotRated() {
		return getBoolean(KEY_HOME_HOT_RATED, DEFAULT_HOME_HOT_RATED);
	}
	public void setHomeHotRated(boolean b) {
		setBoolean(KEY_HOME_HOT_RATED, b);
	}     

	public boolean showHomeAllArtist() {
		return getBoolean(KEY_HOME_ALLARTIST, DEFAULT_HOME_ALLARTIST);
	}
	public void setHomeAllArtist(boolean b) {
		setBoolean(KEY_HOME_ALLARTIST, b);
	}     

	public boolean showHomeStarredArtist() {
		return getBoolean(KEY_HOME_STARRED_ARTIST, DEFAULT_HOME_STARRED_ARTIST);
	}

	public void setHomeStarredArtist(boolean b) {
		setBoolean(KEY_HOME_STARRED_ARTIST, b);
	}     

	public boolean showHomeStarredAlbum() {
		return getBoolean(KEY_HOME_STARRED_ALBUM, DEFAULT_HOME_STARRED_ALBUM);
	}

	public void setHomeStarredAlbum(boolean b) {
		setBoolean(KEY_HOME_STARRED_ALBUM, b);
	}     

	public boolean showHomeAlbumTip() {
		return getBoolean(KEY_HOME_ALBUMTIP, DEFAULT_HOME_ALBUMTIP);
	}

	public void setHomeAlbumTip(boolean b) {
		setBoolean(KEY_HOME_ALBUMTIP, b);
	}  

	public boolean showHomeTopRated() {
		return getBoolean(KEY_HOME_TOP_RATED, DEFAULT_HOME_TOP_RATED);
	}

	public void setHomeTopRated(boolean b) {
		setBoolean(KEY_HOME_TOP_RATED, b);
	}  

	public boolean showHomeMostPlayed() {
		return getBoolean(KEY_HOME_MOSTPLAYED, DEFAULT_HOME_MOSTPLAYED);
	}

	public void setHomeMostPlayed(boolean b) {
		setBoolean(KEY_HOME_MOSTPLAYED, b);
	}

	public boolean showHomeLastPlayed() {
		return getBoolean(KEY_HOME_LASTPLAYED, DEFAULT_HOME_LASTPLAYED);
	}

	public void setHomeLastPlayed(boolean b) {
		setBoolean(KEY_HOME_LASTPLAYED, b);
	}

	public boolean showHomeDecade() {
		return getBoolean(KEY_HOME_DECADE, DEFAULT_HOME_DECADE);
	}

	public void setHomeDecade(boolean b) {
		setBoolean(KEY_HOME_DECADE, b);
	}

	public boolean showHomeGenre() {
		return getBoolean(KEY_HOME_GENRE, DEFAULT_HOME_GENRE);
	}

	public void setHomeGenre(boolean b) {
		setBoolean(KEY_HOME_GENRE, b);
	}

	public boolean showHomeName() {
		return getBoolean(KEY_HOME_NAME, DEFAULT_HOME_NAME);
	}

	public void setHomeName(boolean b) {
		setBoolean(KEY_HOME_NAME, b);
	}

	public boolean isSnowEnabled() {
		return getBoolean(KEY_FEATURE_SNOW, DEFAULT_FEATURE_SNOW);
	}

	public void setSnowEnabled(boolean b) {
		setBoolean(KEY_FEATURE_SNOW, b);
	}	
	
	public boolean showHomeTop100() {
		return getBoolean(KEY_HOME_TOP100, DEFAULT_HOME_TOP100);
	}

	public void setHomeTop100(boolean b) {
		setBoolean(KEY_HOME_TOP100, b);
	}

	public boolean showHomeNew100() {
		return getBoolean(KEY_HOME_NEW100, DEFAULT_HOME_NEW100);
	}

	public void setHomeNew100(boolean b) {
		setBoolean(KEY_HOME_NEW100, b);
	}

	public int getIndexListSize() {
		return Integer.valueOf(properties.getProperty(KEY_INDEXLIST_SIZE, String.valueOf(DEFAULT_INDEXLIST_SIZE)));
	}

	public void setIndexListSize(int size) {
		setProperty(KEY_INDEXLIST_SIZE, String.valueOf(size));
	}	
	
	public int getPlayqueueSize() {
		return Integer.valueOf(properties.getProperty(KEY_PLAYQUEUE_SIZE, String.valueOf(DEFAULT_PLAYQUEUE_SIZE)));
	}

	public void setPlayqueueSize(int size) {
		setProperty(KEY_PLAYQUEUE_SIZE, String.valueOf(size));
	}

	public MediaLibraryStatistics getMediaLibraryStatistics() {
		return MediaLibraryStatistics.parse(getString(KEY_MEDIA_LIBRARY_STATISTICS, DEFAULT_MEDIA_LIBRARY_STATISTICS));
	}

	public void setMediaLibraryStatistics(MediaLibraryStatistics statistics) {
		setString(KEY_MEDIA_LIBRARY_STATISTICS, statistics.format());
	}
	/**
	 * Returns the locale (for language, date format etc).
	 *
	 * @return The locale.
	 */
	 public Locale getLocale() {
		String language = properties.getProperty(KEY_LOCALE_LANGUAGE, DEFAULT_LOCALE_LANGUAGE);
		String country = properties.getProperty(KEY_LOCALE_COUNTRY, DEFAULT_LOCALE_COUNTRY);
		String variant = properties.getProperty(KEY_LOCALE_VARIANT, DEFAULT_LOCALE_VARIANT);

		return new Locale(language, country, variant);
	}

	/**
	 * Sets the locale (for language, date format etc.)
	 *
	 * @param locale The locale.
	 */
	 public void setLocale(Locale locale) {
		setProperty(KEY_LOCALE_LANGUAGE, locale.getLanguage());
		setProperty(KEY_LOCALE_COUNTRY, locale.getCountry());
		setProperty(KEY_LOCALE_VARIANT, locale.getVariant());
	}

	public String getListType() {
		return properties.getProperty(KEY_LISTTYPE, DEFAULT_LISTTYPE);
	}

	public void setListType(String listType) {
		setProperty(KEY_LISTTYPE, listType);
	}

	public void setNewaddedTimespan(String TimeSpan) {
		setProperty(KEY_NEWADDED_TIMESPAN, TimeSpan);
	}

	public String getNewaddedTimespan() {
		return properties.getProperty(KEY_NEWADDED_TIMESPAN, DEFAULT_NEWADDED_TIMESPAN);
	}

	@Deprecated
	public boolean getPlayQueueResize() {
		return getBoolean(KEY_PLAYQUEUE_RESIZE, DEFAULT_PLAYQUEUE_RESIZE);		
	}

	@Deprecated
	public void setPlayQueueResize(boolean ResizeEnabled) {
		setBoolean(KEY_PLAYQUEUE_RESIZE, ResizeEnabled);
	}

	@Deprecated
	public boolean getLeftFrameResize() {
		return getBoolean(KEY_LEFTFRAME_RESIZE, DEFAULT_LEFTFRAME_RESIZE);		
	}

	public void setLeftFrameResize(boolean ResizeEnabled) {
		setBoolean(KEY_LEFTFRAME_RESIZE, ResizeEnabled);
	}


	public boolean getCustomScrollbar() {
		return getBoolean(KEY_CUSTOMSCROLLBAR, DEFAULT_CUSTOMSCROLLBAR);		
	}

	public void setCustomScrollbar(boolean CustomScrollbarEnabled) {
		setBoolean(KEY_CUSTOMSCROLLBAR, CustomScrollbarEnabled);
	}

	public boolean getCustomAccordion() {
		return getBoolean(KEY_CUSTOMACCORDION, DEFAULT_CUSTOMACCORDION);		
	}

	public void setCustomAccordion(boolean CustomAccordionEnabled) {
		setBoolean(KEY_CUSTOMACCORDION, CustomAccordionEnabled);
	}	

	public boolean getCustomLogo() {
		return getBoolean(KEY_CUSTOM_LOGO, DEFAULT_CUSTOM_LOGO);		
	}

	public void setCustomLogo(boolean CustomLogoEnabled) {
		setBoolean(KEY_CUSTOM_LOGO, CustomLogoEnabled);
	}	
	
	/**
	 * Returns the ID of the theme to use.
	 *
	 * @return The theme ID.
	 */
	public String getThemeId() {
		return properties.getProperty(KEY_THEME_ID, DEFAULT_THEME_ID);
	}

	/**
	 * Sets the ID of the theme to use.
	 *
	 * @param themeId The theme ID
	 */
	public void setThemeId(String themeId) {
		setProperty(KEY_THEME_ID, themeId);
	}

	/**
	 * Returns a list of available themes.
	 *
	 * @return A list of available themes.
	 */
	public synchronized Theme[] getAvailableThemes() {
		if (themes == null) {
			themes = new ArrayList<Theme>();
			try {
				InputStream in = SettingsService.class.getResourceAsStream(THEMES_FILE);
				String[] lines = StringUtil.readLines(in);
				for (String line : lines) {
					String[] elements = StringUtil.split(line);
					if (elements.length == 2) {
						themes.add(new Theme(elements[0], elements[1]));
                    } else if (elements.length == 3) {
                        themes.add(new Theme(elements[0], elements[1], elements[2]));
					} else {
						LOG.warn("Failed to parse theme from line: [" + line + "].");
					}
				}
			} catch (IOException x) {
				LOG.error("Failed to resolve list of themes.", x);
                themes.add(new Theme("default", "Madsonic default"));
			}
		}
		return themes.toArray(new Theme[themes.size()]);
	}

	/**
	 * Returns a list of available locales.
	 *
	 * @return A list of available locales.
	 */
	public synchronized Locale[] getAvailableLocales() {
		if (locales == null) {
			locales = new ArrayList<Locale>();
			try {
				InputStream in = SettingsService.class.getResourceAsStream(LOCALES_FILE);
				String[] lines = StringUtil.readLines(in);

				for (String line : lines) {
					locales.add(parseLocale(line));
				}

			} catch (IOException x) {
				LOG.error("Failed to resolve list of locales.", x);
				locales.add(Locale.ENGLISH);
			}
		}
		return locales.toArray(new Locale[locales.size()]);
	}

	private Locale parseLocale(String line) {
		String[] s = line.split("_");
		String language = s[0];
		String country = "";
		String variant = "";

		if (s.length > 1) {
			country = s[1];
		}
		if (s.length > 2) {
			variant = s[2];
		}
		return new Locale(language, country, variant);
	}

	/**
	 * Returns the "brand" name. Normally, this is just "Madsonic".
	 *
	 * @return The brand name.
	 */
	public String getBrand() {
		return "Madsonic";
	}

	/**
	 * Returns all music folders. Non-existing and disabled folders are not included.
	 *
	 * @return Possibly empty list of all music folders.
	 */
	public List<MusicFolder> getAllMusicFolders() {
		return getAllMusicFolders(false, false);
	}


    public MusicFolder getMusicFolderByPath(String path) {
        for (MusicFolder musicFolder : getAllMusicFolders()) {
            if (musicFolder.getPath().getPath().equals(path)) {
                return musicFolder;
            }
        }
        return null;
    }

	public List<MusicFolder> getMusicFoldersForGroup(int user_group_id) {
        return getAllMusicFolders(false, false, user_group_id);
	}

    /**
     * Returns all music folders.
     *
     * @param includeDisabled Whether to include disabled folders.
     * @param includeNonExisting Whether to include non-existing folders.
     * @return Possibly empty list of all music folders.
     */
	public List<MusicFolder> getAllMusicFolders(boolean includeDisabled, boolean includeNonExisting) {
            cachedMusicFolders = musicFolderDao.getAllMusicFolders();
		List<MusicFolder> result = new ArrayList<MusicFolder>(cachedMusicFolders.size());
		for (MusicFolder folder : cachedMusicFolders) {
			if ((includeDisabled || folder.isEnabled()) && (includeNonExisting || FileUtil.exists(folder.getPath()))) {
				result.add(folder);
			}
		}
		return result;
	}

	/**
	 * Returns all music folders.
	 *
	 * @param includeDisabled Whether to include disabled folders.
	 * @param includeNonExisting Whether to include non-existing folders.
	 * @return Possibly empty list of all music folders.
	 */
	public List<MusicFolder> getAllMusicFolders(boolean includeDisabled, boolean includeNonExisting, int user_group_id) {
		cachedMusicFolders = null; // Turn of cache
		if (cachedMusicFolders == null) {
			cachedMusicFolders = musicFolderDao.getAllMusicFolders(user_group_id);
		}

		List<MusicFolder> result = new ArrayList<MusicFolder>(cachedMusicFolders.size());
		for (MusicFolder folder : cachedMusicFolders) {
			if ((includeDisabled || folder.isEnabled()) && (includeNonExisting || FileUtil.exists(folder.getPath()))) {
				result.add(folder);
			}
		}
		return result;
	}

	public List<MusicFolder> getAllMusicFolders(int usergroupId, boolean sort) {
		return getAllMusicFolders(usergroupId, sort, DEFAULT_GROUP_ALL);
	}

	public List<MusicFolder> getAllMusicTypeFolders(int usergroupId, boolean sort) {

		cachedMusicFolders = new ArrayList<MusicFolder>(); 
		cachedMusicFolders = musicFolderDao.getAllMusicTypeFolders(usergroupId);

		List<MusicFolder> result = new ArrayList<MusicFolder>(cachedMusicFolders.size());
		for (MusicFolder folder : cachedMusicFolders) {
			if ((false || folder.isEnabled()) && (false || FileUtil.exists(folder.getPath()))) {
				result.add(folder);
			}
		}
		if (sort){
			Comparator<MusicFolder> comparator = new MusicFolderComparator();
			Set<MusicFolder> set = new TreeSet<MusicFolder>(comparator);
			set.addAll(result);
			result = new ArrayList<MusicFolder>(set);
		}
		return result;
		
		
	}
	
	
	/**
	 * Returns the music folder with the given group ID.
	 *
	 * @param usergroupId The ID.
	 * @return The music folder with the given ID, or <code>null</code> if not found.
	 */	
	public List<MusicFolder> getAllMusicFolders(int usergroupId, boolean sort, int groupby) {

		cachedMusicFolders = new ArrayList<MusicFolder>(); // Turn off cache
		if (cachedMusicFolders.size() < 1) {
			if (groupby == DEFAULT_GROUP_ALL)   {cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId);}
			if (groupby == DEFAULT_GROUP_MUSIC) {cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId, DEFAULT_GROUP_MUSIC);} 
			if (groupby == DEFAULT_GROUP_VIDEO) {cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId, DEFAULT_GROUP_VIDEO);}
			if (groupby == DEFAULT_GROUP_MOVIES){cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId, DEFAULT_GROUP_MOVIES);}
			if (groupby == DEFAULT_GROUP_SERIES){cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId, DEFAULT_GROUP_SERIES);}
			if (groupby == DEFAULT_GROUP_IMAGES){cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId, DEFAULT_GROUP_IMAGES);}
			if (groupby == DEFAULT_GROUP_TV)    {cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId, DEFAULT_GROUP_TV );}
			if (groupby == DEFAULT_GROUP_NONE)  {cachedMusicFolders = musicFolderDao.getAllMusicFolders(usergroupId, DEFAULT_GROUP_NONE );}
		}

		List<MusicFolder> result = new ArrayList<MusicFolder>(cachedMusicFolders.size());
		for (MusicFolder folder : cachedMusicFolders) {
			if ((false || folder.isEnabled()) && (false || FileUtil.exists(folder.getPath()))) {
				result.add(folder);
			}
		}
		if (sort){

			// TODO: new musicFolder Sort
			Comparator<MusicFolder> comparator = new MusicFolderComparator();
			Set<MusicFolder> set = new TreeSet<MusicFolder>(comparator);
			set.addAll(result);
			result = new ArrayList<MusicFolder>(set);
		}
		return result;
	}    

	/**
	 * Returns the music folder with the given ID.
	 *
	 * @param id The ID.
	 * @return The music folder with the given ID, or <code>null</code> if not found.
	 */
	public MusicFolder getMusicFolderById(Integer id) {
		List<MusicFolder> all = getAllMusicFolders();
		for (MusicFolder folder : all) {
			if (id.equals(folder.getId())) {
				return folder;
			}
		}
		return null;
	}

	/**
	 * Creates a new music folder.
	 *
	 * @param musicFolder The music folder to create.
	 */
	public void createMusicFolder(MusicFolder musicFolder) {
		musicFolderDao.createMusicFolder(musicFolder);
		LOG.info("## Created MediaFolder: " + musicFolder.getName());
		groupDao.insertMusicFolderAccess(musicFolderDao.getMusicFolderId(musicFolder.getPath().toString()));
		LOG.info("## Created Default Access for MediaFolder: " + musicFolder.getName());
		cachedMusicFolders = null;
	}


	/**
	 * Deletes the music folder with the given ID.
	 *
	 * @param id The ID of the music folder to delete.
	 */
	public void deleteMusicFolder(Integer id) {
		musicFolderDao.deleteMusicFolder(id);
		cachedMusicFolders = null;
	}

	/**
	 * Updates the given music folder.
	 *
	 * @param musicFolder The music folder to update.
	 */
	public void updateMusicFolder(MusicFolder musicFolder) {
		musicFolderDao.updateMusicFolder(musicFolder);
		cachedMusicFolders = null;
	}

	/**
	 * Returns all internet radio stations. Disabled stations are not returned.
	 *
	 * @return Possibly empty list of all internet radio stations.
	 */
	public List<InternetRadio> getAllInternetRadios() {
		return getAllInternetRadios(false);
	}

	/**
	 * Returns the internet radio station with the given ID.
	 *
	 * @param id The ID.
	 * @return The internet radio station with the given ID, or <code>null</code> if not found.
	 */
	public InternetRadio getInternetRadioById(Integer id) {
		for (InternetRadio radio : getAllInternetRadios()) {
			if (id.equals(radio.getId())) {
				return radio;
			}
		}
		return null;
	}

	/**
	 * Returns all internet radio stations.
	 *
	 * @param includeAll Whether disabled stations should be included.
	 * @return Possibly empty list of all internet radio stations.
	 */
	public List<InternetRadio> getAllInternetRadios(boolean includeAll) {
		List<InternetRadio> all = internetRadioDao.getAllInternetRadios();
		List<InternetRadio> result = new ArrayList<InternetRadio>(all.size());
		for (InternetRadio folder : all) {
			if (includeAll || folder.isEnabled()) {
				result.add(folder);
			}
		}
		return result;
	}

	/**
	 * Creates a new internet radio station.
	 *
	 * @param radio The internet radio station to create.
	 */
	public void createInternetRadio(InternetRadio radio) {
		internetRadioDao.createInternetRadio(radio);
	}

	/**
	 * Deletes the internet radio station with the given ID.
	 *
	 * @param id The internet radio station ID.
	 */
	public void deleteInternetRadio(Integer id) {
		internetRadioDao.deleteInternetRadio(id);
	}

	/**
	 * Updates the given internet radio station.
	 *
	 * @param radio The internet radio station to update.
	 */
	public void updateInternetRadio(InternetRadio radio) {
		internetRadioDao.updateInternetRadio(radio);
	}

	/**
	 * Returns settings for the given user.
	 *
	 * @param username The username.
	 * @return User-specific settings. Never <code>null</code>.
	 */
	public UserSettings getUserSettings(String username) {
		UserSettings settings = userDao.getUserSettings(username);
		return settings == null ? createDefaultUserSettings(username) : settings;
	}

	public UserSettings getDefaultUserSettings(String username) {
		UserSettings settings = userDao.getUserSettings(username);
		return settings == null ? cloneDefaultUserSettings(username) : settings;
	}    

	public UserSettings cloneDefaultUserSettings(String username) {
		UserSettings settings = new UserSettings(username);
		UserSettings defaultSettings = userDao.getUserSettings("default");

		settings.setFinalVersionNotificationEnabled(defaultSettings.isFinalVersionNotificationEnabled());
		settings.setBetaVersionNotificationEnabled(defaultSettings.isBetaVersionNotificationEnabled());
		settings.setShowChatEnabled(defaultSettings.isShowChatEnabled());
		settings.setShowNowPlayingEnabled(defaultSettings.isShowNowPlayingEnabled());
		settings.setNowPlayingAllowed(defaultSettings.isNowPlayingAllowed());
		settings.setPartyModeEnabled(defaultSettings.isPartyModeEnabled());
		settings.setShowArtistInfoEnabled(defaultSettings.isShowArtistInfoEnabled());
		settings.setLastFmEnabled(defaultSettings.isLastFmEnabled());
		settings.setLastFmUsername(defaultSettings.getLastFmUsername());
		settings.setLastFmPassword(defaultSettings.getLastFmPassword());
		settings.setLocale(defaultSettings.getLocale());
		settings.setThemeId(defaultSettings.getThemeId());
		settings.setListType(defaultSettings.getListType());
//		settings.setPlayQueueResize(defaultSettings.getPlayQueueResize());
		settings.setLeftFrameResize(defaultSettings.getLeftFrameResize());
		settings.setCustomScrollbarEnabled(defaultSettings.isCustomScrollbarEnabled());
		settings.setCustomAccordionEnabled(defaultSettings.isCustomAccordionEnabled());
		settings.setShowLeftBar(true);
		settings.setShowLeftBarShrinked(defaultSettings.isShowLeftBarShrinked());
		settings.setShowLeftPanel(defaultSettings.isShowLeftPanel());
		settings.setShowRightPanel(defaultSettings.isShowRightPanel());
		settings.setShowSidePanel(defaultSettings.isShowSidePanel());
		settings.setChanged(new Date());

		UserSettings.Visibility playlist = settings.getPlaylistVisibility();
		playlist.setCaptionCutoff(defaultSettings.getPlaylistVisibility().getCaptionCutoff());
		playlist.setDiscNumberVisible(defaultSettings.getPlaylistVisibility().isDiscNumberVisible());
		playlist.setTrackNumberVisible(defaultSettings.getPlaylistVisibility().isTrackNumberVisible());
		playlist.setArtistVisible(defaultSettings.getPlaylistVisibility().isArtistVisible());
		playlist.setAlbumVisible(defaultSettings.getPlaylistVisibility().isAlbumVisible());
		playlist.setGenreVisible(defaultSettings.getPlaylistVisibility().isGenreVisible());
		playlist.setMoodVisible(defaultSettings.getPlaylistVisibility().isMoodVisible());
		playlist.setYearVisible(defaultSettings.getPlaylistVisibility().isYearVisible());
		playlist.setDurationVisible(defaultSettings.getPlaylistVisibility().isDurationVisible());
		playlist.setBitRateVisible(defaultSettings.getPlaylistVisibility().isBitRateVisible());
		playlist.setFormatVisible(defaultSettings.getPlaylistVisibility().isFormatVisible());
		playlist.setFileSizeVisible(defaultSettings.getPlaylistVisibility().isFileSizeVisible());
		playlist.setComposerVisible(defaultSettings.getPlaylistVisibility().isComposerVisible());
		playlist.setBpmVisible(defaultSettings.getPlaylistVisibility().isBpmVisible());

		UserSettings.Visibility main = settings.getMainVisibility();
		main.setCaptionCutoff(defaultSettings.getMainVisibility().getCaptionCutoff());
		main.setTrackNumberVisible(defaultSettings.getMainVisibility().isTrackNumberVisible());
		main.setDiscNumberVisible(defaultSettings.getMainVisibility().isDiscNumberVisible());
		main.setArtistVisible(defaultSettings.getMainVisibility().isArtistVisible());
		main.setAlbumVisible(defaultSettings.getMainVisibility().isAlbumVisible());
		main.setDurationVisible(defaultSettings.getMainVisibility().isDurationVisible());
		main.setGenreVisible(defaultSettings.getMainVisibility().isGenreVisible());
		main.setMoodVisible(defaultSettings.getMainVisibility().isMoodVisible());
		main.setYearVisible(defaultSettings.getMainVisibility().isYearVisible());
		main.setDurationVisible(defaultSettings.getMainVisibility().isDurationVisible());
		main.setBitRateVisible(defaultSettings.getMainVisibility().isBitRateVisible());
		main.setFormatVisible(defaultSettings.getMainVisibility().isFormatVisible());
		main.setFileSizeVisible(defaultSettings.getMainVisibility().isFileSizeVisible());
		main.setComposerVisible(defaultSettings.getMainVisibility().isComposerVisible());
		main.setBpmVisible(defaultSettings.getMainVisibility().isBpmVisible());
		
		UserSettings.ButtonVisibility button = settings.getButtonVisibility();
		button.setRankVisible(defaultSettings.getButtonVisibility().isRankVisible());
		button.setLovedVisible(defaultSettings.getButtonVisibility().isLovedVisible());
		button.setStarredVisible(defaultSettings.getButtonVisibility().isStarredVisible());
		button.setPlayVisible(defaultSettings.getButtonVisibility().isPlayVisible());
		button.setPlayAddVisible(defaultSettings.getButtonVisibility().isPlayAddVisible());
		button.setPlayMoreVisible(defaultSettings.getButtonVisibility().isPlayMoreVisible());
		button.setAddContextVisible(defaultSettings.getButtonVisibility().isAddContextVisible());
		button.setAddNextVisible(defaultSettings.getButtonVisibility().isAddNextVisible());
		button.setAddLastVisible(defaultSettings.getButtonVisibility().isAddLastVisible());
		button.setDownloadVisible(defaultSettings.getButtonVisibility().isDownloadVisible());
		button.setYoutubeVisible(defaultSettings.getButtonVisibility().isYoutubeVisible());
		button.setLyricVisible(defaultSettings.getButtonVisibility().isLyricVisible());
		
		return settings;
	}


	private UserSettings createDefaultUserSettings(String username) {
		UserSettings settings = new UserSettings(username);
		settings.setFinalVersionNotificationEnabled(true);
		settings.setBetaVersionNotificationEnabled(true);
		settings.setShowNowPlayingEnabled(true);
		settings.setShowArtistInfoEnabled(true);
		settings.setShowChatEnabled(false);
		settings.setPartyModeEnabled(false);
		settings.setNowPlayingAllowed(true);
		settings.setShowNowPlayingEnabled(true);
		settings.setShowLeftBar(true);
		settings.setShowLeftBarShrinked(false);
		settings.setShowLeftPanel(false);
		settings.setShowRightPanel(false);
		settings.setShowSidePanel(true);
		settings.setLastFmEnabled(false);
		settings.setLastFmUsername(null);
		settings.setLastFmPassword(null);
		settings.setChanged(new Date());
		settings.setListType(getListType());
		settings.setPlayQueueResize(false);
		settings.setLeftFrameResize(false);
		settings.setCustomScrollbarEnabled(true);
		settings.setCustomAccordionEnabled(false);
		settings.setAvatarScheme(AvatarScheme.SYSTEM);
		settings.setSystemAvatarId(1);
		
		UserSettings.Visibility playlist = settings.getPlaylistVisibility();
		playlist.setCaptionCutoff(50);
		playlist.setArtistVisible(true);
		playlist.setAlbumVisible(true);
		playlist.setGenreVisible(true);
		playlist.setMoodVisible(true);
		playlist.setYearVisible(true);
		playlist.setDurationVisible(true);
		playlist.setBitRateVisible(true);
		playlist.setFormatVisible(true);
		playlist.setFileSizeVisible(true);

		UserSettings.Visibility main = settings.getMainVisibility();
		main.setCaptionCutoff(60);
		main.setTrackNumberVisible(false);
		main.setArtistVisible(true);
		main.setDurationVisible(true);
		main.setGenreVisible(true);
		main.setMoodVisible(true);

		UserSettings.ButtonVisibility button = settings.getButtonVisibility();
		button.setRankVisible(true);
		button.setLovedVisible(false);
		button.setStarredVisible(true);
		button.setPlayVisible(true);
		button.setPlayAddVisible(false);
		button.setPlayMoreVisible(false);
		button.setAddContextVisible(true);
		button.setAddNextVisible(false);
		button.setAddLastVisible(false);
		button.setDownloadVisible(true);
		button.setYoutubeVisible(false);
		button.setLyricVisible(false);

		return settings;
	}

	/**
	 * Updates settings for the given username.
	 *
	 * @param settings The user-specific settings.
	 */
	public void updateUserSettings(UserSettings settings) {
		userDao.updateUserSettings(settings);
	}

	/**
	 * Returns all system avatars.
	 *
	 * @return All system avatars.
	 */
	public List<Avatar> getAllSystemAvatars() {
		return avatarDao.getAllSystemAvatars();
	}

	/**
	 * Returns the system avatar with the given ID.
	 *
	 * @param id The system avatar ID.
	 * @return The avatar or <code>null</code> if not found.
	 */
	public Avatar getSystemAvatar(int id) {
		return avatarDao.getSystemAvatar(id);
	}

	/**
	 * Returns the custom avatar for the given user.
	 *
	 * @param username The username.
	 * @return The avatar or <code>null</code> if not found.
	 */
	public Avatar getCustomAvatar(String username) {
		return avatarDao.getCustomAvatar(username);
	}

	/**
	 * Sets the custom avatar for the given user.
	 *
	 * @param avatar   The avatar, or <code>null</code> to remove the avatar.
	 * @param username The username.
	 */
	public void setCustomAvatar(Avatar avatar, String username) {
		avatarDao.setCustomAvatar(avatar, username);
	}

	public boolean isDlnaEnabled() {
		return getBoolean(KEY_DLNA_ENABLED, DEFAULT_DLNA_ENABLED);
	}

	public void setDlnaEnabled(boolean dlnaEnabled) {
		setBoolean(KEY_DLNA_ENABLED, dlnaEnabled);
	}


	public String getDlnaServerName() {
		return getString(KEY_DLNA_SERVER_NAME, DEFAULT_DLNA_SERVER_NAME);
	}

	public void setDlnaServerName(String dlnaServerName) {
		setString(KEY_DLNA_SERVER_NAME, dlnaServerName);
	}

    public boolean isSonosEnabled() {
        return getBoolean(KEY_SONOS_ENABLED, DEFAULT_SONOS_ENABLED);
    }

    public void setSonosEnabled(boolean sonosEnabled) {
        setBoolean(KEY_SONOS_ENABLED, sonosEnabled);
    }

    public String getSonosServiceName() {
        return getString(KEY_SONOS_SERVICE_NAME, DEFAULT_SONOS_SERVICE_NAME);
    }

    public void setSonosServiceName(String sonosServiceName) {
        setString(KEY_SONOS_SERVICE_NAME, sonosServiceName);
    }

    public int getSonosServiceId() {
        return getInt(KEY_SONOS_SERVICE_ID, DEFAULT_SONOS_SERVICE_ID);
    }

    public void setSonosServiceId(int sonosServiceid) {
        setInt(KEY_SONOS_SERVICE_ID, sonosServiceid);
    }
    
    public SonosRatingType getSonosRatingType() {
        return SonosRatingType.valueOf(properties.getProperty(KEY_SONOS_RATING_TYPE, DEFAULT_SONOS_RATING_TYPE.name()));
    }

    public void setSonosRatingType(SonosRatingType sonosRatingType) {
        properties.setProperty(KEY_SONOS_RATING_TYPE, sonosRatingType.name());
    }    

	public boolean isSignupEnabled() {
	       return getBoolean(KEY_SIGNUP_ENABLED, DEFAULT_SIGNUP_ENABLED);
	}
    
    public void setSignupEnabled(boolean signupEnabled) {
        setBoolean(KEY_SIGNUP_ENABLED, signupEnabled);
    }	
	
	public String getSignupServiceName() {
        return getString(KEY_SIGNUP_SERVICE_NAME, DEFAULT_SIGNUP_SERVICE_NAME);
    }

    public void setSignupServiceName(String signupServiceName) {
        setString(KEY_SIGNUP_SERVICE_NAME, signupServiceName);
    }
    
	public boolean isEmulatorEnabled() {
	       return getBoolean(KEY_EMULATOR_ENABLED, DEFAULT_EMULATOR_ENABLED);
	}
    
    public void setEmulatorEnabled(boolean emulatorEnabled) {
        setBoolean(KEY_EMULATOR_ENABLED, emulatorEnabled);
    }		
	
	public boolean isNodesEnabled() {
	       return getBoolean(KEY_NODES_ENABLED, DEFAULT_NODES_ENABLED);
	}
 
	 public void setNodesEnabled(boolean nodesEnabled) {
	     setBoolean(KEY_NODES_ENABLED, nodesEnabled);
	 }		
    
	public boolean isFolderParsingEnabled() {
		return getBoolean(KEY_FOLDERPARSING_ENABLED, DEFAULT_FOLDERPARSING_ENABLED);
	}

	public void setFolderParsingEnabled(boolean folderParsingEnabledEnabled) {
		setBoolean(KEY_FOLDERPARSING_ENABLED, folderParsingEnabledEnabled);
	}    


	public boolean isAlbumSetParsingEnabled() {
		return getBoolean(KEY_ALBUMSETPARSING_ENABLED, DEFAULT_ALBUMSETPARSING_ENABLED);
	}

	public void setAlbumSetParsingEnabled(boolean albumSetParsingEnabledEnabled) {
		setBoolean(KEY_ALBUMSETPARSING_ENABLED, albumSetParsingEnabledEnabled);
	}        

	public boolean isLogfileReverse() {
		return getBoolean(KEY_LOGFILE_REVERSE, DEFAULT_LOGFILE_REVERSE);
	}

	public void setLogfileReverse(boolean logfileReverse) {
		setBoolean(KEY_LOGFILE_REVERSE, logfileReverse);
	}        

	public boolean isDebugOutput() {
	       return getBoolean(KEY_DEBUG_OUTPUT, DEFAULT_DEBUG_OUTPUT);
	}
 
	public void setDebugOutput(boolean debug) {
	     setBoolean(KEY_DEBUG_OUTPUT, debug);
	}		

	
	public boolean isShowCreateLink() {
	       return getBoolean(KEY_SHOW_CREATELINK, DEFAULT_SHOW_CREATELINK);
	}

	public void setShowCreateLink(boolean b) {
	     setBoolean(KEY_SHOW_CREATELINK, b);
	}
	
	public boolean isShowMediaType() {
	       return getBoolean(KEY_SHOW_MEDIATYPE, DEFAULT_SHOW_MEDIATYPE);
	}

	public void setShowMediaType(boolean b) {
	     setBoolean(KEY_SHOW_MEDIATYPE, b);
	}	
	
	public String getLogLevel() {
		return properties.getProperty(KEY_LOGFILE_LEVEL, DEFAULT_LOGFILE_LEVEL);
	}

	public static String getLogfileLevel() {
		return properties.getProperty(KEY_LOGFILE_LEVEL, DEFAULT_LOGFILE_LEVEL);
	}
	
	public void setLogfileLevel(String level) {
		setProperty(KEY_LOGFILE_LEVEL, level);
	}

    public long getArtistBioLastUpdated() {
        return getLong(KEY_ARTIST_BIO_LAST_UPDATED, DEFAULT_ARTIST_BIO_LAST_UPDATED);
    }

    public void setArtistBioLastUpdated(long updated) {
        setLong(KEY_ARTIST_BIO_LAST_UPDATED, updated);
    }

    public String getVideoConversionDirectory() {
        return getString(KEY_VIDEO_CONVERSATION_FOLDER, DEFAULT_VIDEO_CONVERSION_FOLDER);
    }

    public void setVideoConversionDirectory(String dir) {
        setString(KEY_VIDEO_CONVERSATION_FOLDER, dir);
    }

    public String getAudioConversionDirectory() {
        return getString(KEY_AUDIO_CONVERSATION_FOLDER, DEFAULT_AUDIO_CONVERSION_FOLDER);
    }

    public void setAudioConversionDirectory(String dir) {
        setString(KEY_AUDIO_CONVERSATION_FOLDER, dir);
    }    
    
    public int getVideoConversionDiskLimit() {
        return getInt(KEY_VIDEO_CONVERSION_DISK_LIMIT, DEFAULT_VIDEO_CONVERSION_DISK_LIMIT);
    }

    public void setVideoConversionDiskLimit(int limit) {
        setInt(KEY_VIDEO_CONVERSION_DISK_LIMIT, limit);
    }

    public int getAudioConversionDiskLimit() {
        return getInt(KEY_AUDIO_CONVERSION_DISK_LIMIT, DEFAULT_AUDIO_CONVERSION_DISK_LIMIT);
    }

    public void setAudioConversionDiskLimit(int limit) {
        setInt(KEY_AUDIO_CONVERSION_DISK_LIMIT, limit);
    }    
    
	public String getLocalIpAddress() {
		return localIpAddress;
    }

    /**
     * Rewrites an URL to make it accessible from remote clients.
     */
    public String rewriteRemoteUrl(String localUrl) {
        return StringUtil.rewriteRemoteUrl(localUrl, isUrlRedirectionEnabled(), getUrlRedirectType(), getUrlRedirectFrom(),
                                           getUrlRedirectCustomUrl(), getUrlRedirectContextPath(), getLocalIpAddress(),
                                           getPort());
    }

	private void setProperty(String key, String value) {
		if (value == null) {
			properties.remove(key);
		} else {
			properties.setProperty(key, value);
		}
	}

	private String[] toStringArray(String s) {
		List<String> result = new ArrayList<String>();
		StringTokenizer tokenizer = new StringTokenizer(s, " ");
		while (tokenizer.hasMoreTokens()) {
			result.add(tokenizer.nextToken());
		}

		return result.toArray(new String[result.size()]);
	}

	public void validateLicense() {
		String email = getLicenseEmail();
		Date date = getLicenseDate();

		if (email == null || date == null) {
			licenseValidated = false;
			return;
		}

		try {			
	        CloseableHttpClient client = HttpClientBuilder.create().build();
	        HttpGet method = new HttpGet(BACKEND_VALIDATE_LIC_URL + 
		        							"?email=" + StringUtil.urlEncode(email) + 
		        							"&date=" + date.getTime() + 
		        							"&version=" + versionService.getLocalVersion());
	        
	        //TODO: add MADSONIC ARGUMENTS FOR PROXY
			String proxySchema = "http";
	        String proxyServer = "localhost";
			int proxyPort = 8080;
			
	        boolean proxyEnabled = false;

	        if (proxyEnabled) {
				HttpHost proxy = new HttpHost(proxyServer, proxyPort, proxySchema);
		        RequestConfig requestConfig = RequestConfig.custom()
		                .setProxy(proxy)	        
		                .setSocketTimeout(120000)
		                .setConnectTimeout(120000)
		                .setConnectionRequestTimeout(120000)
		                .build();
	    		method.setConfig(requestConfig);  
	    		
	        } else {
		        RequestConfig requestConfig = RequestConfig.custom()
		                .setSocketTimeout(120000)
		                .setConnectTimeout(120000)
		                .setConnectionRequestTimeout(120000)
		                .build();
	    		method.setConfig(requestConfig);  
	        }
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String content = client.execute(method, responseHandler);
            licenseValidated = content != null && !content.contains("false");
			if (!licenseValidated) {
                LOG.warn("License key is not valid.");
			} 
			String[] lines = StringUtils.split(content);
			if (lines.length > 1) {
				licenseExpires = new Date(Long.parseLong(lines[1]));
			}

		} catch (Throwable x) {
            LOG.warn("Failed to validate license.", x);
		} finally {
           // LOG.warn("Failed to close validation request.");
		}
	}

    public synchronized void scheduleLicenseValidation() {
        if (licenseValidationFuture != null) {
            licenseValidationFuture.cancel(true);
        }
        Runnable task = new Runnable() {
            public void run() {
                validateLicense();
            }
        };
        licenseValidated = true;
        licenseExpires = null;
        licenseValidationFuture = executor.scheduleWithFixedDelay(task, 0L, LICENSE_VALIDATION_DELAY_HOURS, TimeUnit.HOURS);
    }

	private void scheduleLocalIpAddressLookup() {
		Runnable task = new Runnable() {
			public void run() {
				localIpAddress = Util.getLocalIpAddress();
			}
		};
		executor.scheduleWithFixedDelay(task, 0L, LOCAL_IP_LOOKUP_DELAY_SECONDS, TimeUnit.SECONDS);
	}

	public static void setMadsonicUpload(File madsonicUpload) {
		SettingsService.madsonicUpload = madsonicUpload;
	}

	public void setLastFMResultSize(int size) {
		setProperty(KEY_LASTFM_RESULT_SIZE, "" + size);	
	}

	public String getLastFMLanguage() {
		return properties.getProperty(KEY_LASTFM_LANGUAGE, DEFAULT_LASTFM_LANGUAGE);
	}
	
	public void setLastFMLanguage(String locale) {
		setProperty(KEY_LASTFM_LANGUAGE, locale);	
	}
	
	public int getLastFMResultSize() {
		return Integer.parseInt(properties.getProperty(KEY_LASTFM_RESULT_SIZE, "" + DEFAULT_LASTFM_RESULT_SIZE));
	}
	
	public boolean getLastFMArtistBioScan() {
		return getBoolean(KEY_ARTIST_BIO_SCAN, DEFAULT_ARTIST_BIO_SCAN);
	}

	public void setLastFMArtistBioScan(boolean b) {
		setBoolean(KEY_ARTIST_BIO_SCAN, b);
	}
	
	public boolean getLastFMTopTrackSearch() {
		return getBoolean(KEY_LASTFM_TOPTRACK_SEARCH, DEFAULT_LASTFM_TOPTRACK_SEARCH);
	}

	public void setLastFMTopTrackSearch(boolean b) {
		setBoolean(KEY_LASTFM_TOPTRACK_SEARCH, b);
	}

	public void setHomePagerTop(boolean b) {
		setBoolean(KEY_HOME_PAGER_TOP, b);
	}

	public boolean showHomePagerTop() {
		return getBoolean(KEY_HOME_PAGER_TOP, DEFAULT_HOME_PAGER_TOP);
	}

	public void setHomePagerBottom(boolean b) {
		setBoolean(KEY_HOME_PAGER_BOTTOM, b);
	}

	public boolean showHomePagerBottom() {
		return getBoolean(KEY_HOME_PAGER_BOTTOM, DEFAULT_HOME_PAGER_BOTTOM);

	}  

    public static String getBackendUrl() {
        return "true".equals(System.getProperty("madsonic.test")) ? "http://localhost:8082" : "https://www.madsonic.org";
    }	

    public ServerVersion getVersion() {
    	return versionService.getLocalVersion();
    }
    
	public void setInternetRadioDao(InternetRadioDao internetRadioDao) {
		this.internetRadioDao = internetRadioDao;
	}

	public void setMusicFolderDao(MusicFolderDao musicFolderDao) {
		this.musicFolderDao = musicFolderDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setAvatarDao(AvatarDao avatarDao) {
		this.avatarDao = avatarDao;
	}

	public void setVersionService(VersionService versionService) {
		this.versionService = versionService;
	}

	public void setTranscodingService(TranscodingService transcodingService) {
		this.transcodingService = transcodingService;
	}	
	
	public GroupDao getGroupDao() {
		return groupDao;
	}    

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	public MusicFolderInfo updateFolderTask(MusicFolderInfo musicFolderInfo) {
		
		Date now = new Date();
		Date timer = musicFolderInfo.getTimer();
		
		switch (timer.compareTo(now)) {
		    case -1:  // System.out.println("sooner"); break;
		    case 0:   // System.out.println("equal"); break;
		    case 1:   // System.out.println("later"); break;
		    default:  // System.out.println("Invalid results from date comparison"); break;
		}
		
		return musicFolderInfo;
	}

}
