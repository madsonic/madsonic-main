/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service.sonos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;

import org.madsonic.Logger;
import org.madsonic.util.HttpUtil;
import org.madsonic.util.Pair;
import org.madsonic.util.StringUtil;

/**
 * @author Sindre Mehus, Martin Karel
 * @version $Id$
 */
@SuppressWarnings("deprecation")
public class SonosServiceRegistration {

    private static final Logger LOG = Logger.getLogger(SonosServiceRegistration.class);

    private static final String presentationMapVersion = "15";
    private static final String stringVersion = "8";
    
    public void setEnabled(String madsonicBaseUrl, String sonosControllerIp, boolean enabled, String sonosServiceName, int sonosServiceId) throws IOException {
        String localUrl = madsonicBaseUrl + "ws/Sonos";
        String controllerUrl = String.format("http://%s:1400/customsd", sonosControllerIp);

        LOG.info((enabled ? "Enabling" : "Disabling") + " Sonos music service, using Sonos controller IP " + sonosControllerIp +
                 ", SID " + sonosServiceId + ", presentationMap " + presentationMapVersion + ", stringVersion " + stringVersion + ", and Madsonic URL " + localUrl);

        List<Pair<String, String>> params = new ArrayList<Pair<String, String>>();
        params.add(Pair.create("sid", String.valueOf(sonosServiceId)));

		String token = getCsrfToken(controllerUrl);
		if (token != null) {
			params.add(Pair.create("csrfToken", token));
		}

        if (enabled) {
            params.add(Pair.create("name", sonosServiceName));
            params.add(Pair.create("uri", localUrl));
            params.add(Pair.create("secureUri", localUrl));
            params.add(Pair.create("pollInterval", "1200"));
            params.add(Pair.create("authType", "UserId"));
            params.add(Pair.create("containerType", "MService"));
            params.add(Pair.create("caps", "search"));
            params.add(Pair.create("caps", "trFavorites"));
            params.add(Pair.create("caps", "alFavorites"));
            params.add(Pair.create("caps", "ucPlaylists"));
            params.add(Pair.create("caps", "extendedMD"));
            params.add(Pair.create("presentationMapVersion", presentationMapVersion));
            params.add(Pair.create("presentationMapUri", madsonicBaseUrl + "sonos/presentationMap.xml"));
            params.add(Pair.create("stringsVersion", stringVersion));
            params.add(Pair.create("stringsUri", madsonicBaseUrl + "sonos/strings.xml"));
        }

        String result = execute(controllerUrl, params);
        LOG.info("Sonos controller returned: " + result);
    }

    private String execute(String url, List<Pair<String, String>> parameters) throws IOException {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        for (Pair<String, String> parameter : parameters) {
            params.add(new BasicNameValuePair(parameter.getFirst(), parameter.getSecond()));
        }

        HttpPost request = new HttpPost(url);
		request.setEntity(new UrlEncodedFormEntity(params));
		return HttpUtil.httpRequest(request, 10, 10);
    }
	
	private static String getCsrfToken(String url) throws IOException {
		String response = HttpUtil.httpGet(url, 10, 10);
		Pattern pattern = Pattern.compile(".*?\"csrfToken\" value=\"(.+?)\".*");
		Matcher matcher = pattern.matcher(response);
		return matcher.matches() ? matcher.group(1) : null;
	}

}
