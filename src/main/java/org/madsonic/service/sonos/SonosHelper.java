/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service.sonos;

import static org.madsonic.domain.MediaFile.MediaType.ARTIST;
import static org.madsonic.domain.MediaFile.MediaType.MUSIC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.ServletRequestUtils;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.madsonic.controller.CoverArtController;
import org.madsonic.dao.MediaFileDao;

import org.madsonic.domain.CoverArtScheme;
import org.madsonic.domain.Genre;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MusicFolder;
import org.madsonic.domain.MusicFolderContent;
import org.madsonic.domain.MusicIndex;
import org.madsonic.domain.Player;
import org.madsonic.domain.PlayerTechnology;
import org.madsonic.domain.Playlist;
import org.madsonic.domain.PodcastChannel;
import org.madsonic.domain.PodcastEpisode;
import org.madsonic.domain.PodcastStatus;
import org.madsonic.domain.RandomSearchCriteria;
import org.madsonic.domain.SearchCriteria;
import org.madsonic.domain.SearchResult;
import org.madsonic.domain.TrackListType;
import org.madsonic.domain.AlbumListType;
import org.madsonic.domain.ChartListType;
import org.madsonic.domain.ArtistListType;
import org.madsonic.domain.ArtistBio;

import org.madsonic.service.LastFmServiceBasic;
import org.madsonic.service.MediaFileService;
import org.madsonic.service.MusicIndexService;
import org.madsonic.service.PlayerService;
import org.madsonic.service.PlaylistService;
import org.madsonic.service.PodcastService;
import org.madsonic.service.RatingService;
import org.madsonic.service.SearchService;
import org.madsonic.service.SecurityService;
import org.madsonic.service.SettingsService;
import org.madsonic.service.SonosService;
import org.madsonic.service.TranscodingService;

import org.madsonic.util.StringUtil;
import org.madsonic.util.Util;

import com.sonos.services._1.AbstractMedia;
import com.sonos.services._1.AlbumArtUrl;
import com.sonos.services._1.DynamicData;
import com.sonos.services._1.ItemType;
import com.sonos.services._1.MediaCollection;
import com.sonos.services._1.MediaList;
import com.sonos.services._1.MediaMetadata;
import com.sonos.services._1.Property;
import com.sonos.services._1.TrackMetadata;

/**
 * @author Sindre Mehus, Martin Karel
 */
public class SonosHelper {

    public static final String MADSONIC_CLIENT_ID = "sonos";

    private MediaFileService mediaFileService;
    private PlaylistService playlistService;
    private PlayerService playerService;
    private TranscodingService transcodingService;
    private SettingsService settingsService;
    private MusicIndexService musicIndexService;
    private SearchService searchService;
    private MediaFileDao mediaFileDao;
    private RatingService ratingService;
    private SecurityService securityService;    
    private LastFmServiceBasic lastFmServiceBasic;
    private PodcastService podcastService;
    
    public List<AbstractMedia> forRoot(HttpServletRequest request) {
    	
        MediaMetadata shuffle = new MediaMetadata();
        shuffle.setItemType(ItemType.PROGRAM);
        shuffle.setId(SonosService.ID_SHUFFLE);
        shuffle.setTitle("Shuffle Play");

        MediaMetadata follow = new MediaMetadata();
        follow.setItemType(ItemType.PROGRAM);
        follow.setId(SonosService.ID_FOLLOW);
        follow.setTitle("FollowMe Play");
        
//        MediaCollection chartlists = new MediaCollection();
//        chartlists.setItemType(ItemType.COLLECTION);
//        chartlists.setId(SonosService.ID_CHARTLISTS);
//        chartlists.setTitle("Chart Lists");        
//        AlbumArtUrl chartlistsArtUrl = new AlbumArtUrl();
//        chartlistsArtUrl.setValue(getSonosArtUrl(SonosService.ID_CHARTLISTS, request));
//        chartlists.setAlbumArtURI(chartlistsArtUrl);
        
        MediaCollection artistlists = new MediaCollection();
        artistlists.setItemType(ItemType.COLLECTION);
        artistlists.setId(SonosService.ID_ARTISTLISTS);
        artistlists.setTitle("Artist Lists");
        AlbumArtUrl artistlistsArtUrl = new AlbumArtUrl();
        artistlistsArtUrl.setValue(getSonosArtUrl(SonosService.ID_ARTISTLISTS, request));
        artistlists.setAlbumArtURI(artistlistsArtUrl);
        
        MediaCollection albumlists = new MediaCollection();
        albumlists.setItemType(ItemType.COLLECTION);
        albumlists.setId(SonosService.ID_ALBUMLISTS);
        albumlists.setTitle("Album Lists");
        AlbumArtUrl albumlistsArtUrl = new AlbumArtUrl();
        albumlistsArtUrl.setValue(getSonosArtUrl(SonosService.ID_ALBUMLISTS, request));
        albumlists.setAlbumArtURI(albumlistsArtUrl);

        MediaCollection tracklists = new MediaCollection();
        tracklists.setItemType(ItemType.COLLECTION);
        tracklists.setId(SonosService.ID_TRACKLISTS);
        tracklists.setTitle("Track Lists");
        AlbumArtUrl tracklistsArtUrl = new AlbumArtUrl();
        tracklistsArtUrl.setValue(getSonosArtUrl(SonosService.ID_TRACKLISTS, request));
        tracklists.setAlbumArtURI(tracklistsArtUrl);        
        
        MediaCollection library = new MediaCollection();
        library.setItemType(ItemType.COLLECTION);
        library.setId(SonosService.ID_LIBRARY);
        library.setTitle("Browse Library");
        AlbumArtUrl libraryArtUrl = new AlbumArtUrl();
        libraryArtUrl.setValue(getSonosArtUrl(SonosService.ID_LIBRARY, request));
        library.setAlbumArtURI(libraryArtUrl);

        MediaCollection playlists = new MediaCollection();
        playlists.setItemType(ItemType.ALBUM_LIST);
        playlists.setId(SonosService.ID_PLAYLISTS);
        playlists.setTitle("Playlists");
        playlists.setUserContent(true);
        playlists.setReadOnly(false);
        AlbumArtUrl playlistsArtUrl = new AlbumArtUrl();
        playlistsArtUrl.setValue(getSonosArtUrl(SonosService.ID_PLAYLISTS, request));
        playlists.setAlbumArtURI(playlistsArtUrl);

        MediaCollection starred = new MediaCollection();
        starred.setItemType(ItemType.FAVORITES);
        starred.setId(SonosService.ID_STARRED);
        starred.setTitle("Starred");
        AlbumArtUrl starredArtUrl = new AlbumArtUrl();
        starredArtUrl.setValue(getSonosArtUrl(SonosService.ID_STARRED, request));
        starred.setAlbumArtURI(starredArtUrl);

        MediaCollection loved = new MediaCollection();
        loved.setItemType(ItemType.FAVORITES);
        loved.setId(SonosService.ID_LOVED);
        loved.setTitle("Loved");
        AlbumArtUrl lovedArtUrl = new AlbumArtUrl();
        lovedArtUrl.setValue(getSonosArtUrl(SonosService.ID_LOVED, request));
        loved.setAlbumArtURI(lovedArtUrl);        
        
        MediaCollection podcasts = new MediaCollection();
        podcasts.setItemType(ItemType.ALBUM_LIST);
        podcasts.setId(SonosService.ID_PODCASTS);
        podcasts.setTitle("Podcasts");
        AlbumArtUrl podcastsArtUrl = new AlbumArtUrl();
        podcastsArtUrl.setValue(getSonosArtUrl(SonosService.ID_PODCASTS, request));
        podcasts.setAlbumArtURI(podcastsArtUrl);

        List<AbstractMedia> rootList = Arrays.asList(shuffle, follow, library, artistlists, albumlists, tracklists, playlists, loved, starred, podcasts);
      	return rootList;
    }

    public List<AbstractMedia> forFollowMe(int count, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        List<MusicFolder> musicFolders = settingsService.getMusicFoldersForGroup(userGroupId);
        return forFollowMusicFolder(musicFolders, count, username, request);
    }    
    
    public List<AbstractMedia> forShuffle(int count, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        List<MusicFolder> musicFolders = settingsService.getMusicFoldersForGroup(userGroupId);
        return forShuffleMusicFolder(musicFolders, count, username, request);
    }

    public List<AbstractMedia> forShuffleMusicFolder(int id, int count, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        List<MusicFolder> musicFolders = settingsService.getMusicFoldersForGroup(userGroupId);
        return forShuffleMusicFolder(musicFolders, count, username, request);
    }

    private List<AbstractMedia> forFollowMusicFolder(List<MusicFolder> musicFolders, int count, String username, HttpServletRequest request) {
        List<MediaFile> albums = searchService.getRandomAlbums(count, musicFolders);    	
        List<MediaFile> songs = new ArrayList<MediaFile>();
        for (MediaFile album : albums) {
            for (MediaFile file : filterMusic(mediaFileService.getChildrenOf(album, true, false, false))) {
                songs.add(file);
            }
        }
        Collections.shuffle(songs);
        songs = songs.subList(0, Math.min(count, songs.size()));
        return forMediaFiles(songs, username, request);
    }    
    
    private List<AbstractMedia> forShuffleMusicFolder(List<MusicFolder> musicFolders, int count, String username, HttpServletRequest request) {
        List<MediaFile> albums = searchService.getRandomAlbums(40, musicFolders);
        List<MediaFile> songs = new ArrayList<MediaFile>();
        for (MediaFile album : albums) {
            for (MediaFile file : filterMusic(mediaFileService.getChildrenOf(album, true, false, false))) {
                songs.add(file);
            }
        }
        Collections.shuffle(songs);
        songs = songs.subList(0, Math.min(count, songs.size()));
        return forMediaFiles(songs, username, request);
    }

    public List<AbstractMedia> forShuffleArtist(int mediaFileId, int count, String username, HttpServletRequest request) {
        MediaFile artist = mediaFileService.getMediaFile(mediaFileId);
        List<MediaFile> songs = filterMusic(mediaFileService.getDescendantsOf(artist, false));
        Collections.shuffle(songs);
        songs = songs.subList(0, Math.min(count, songs.size()));
        return forMediaFiles(songs, username, request);
    }

    public List<AbstractMedia> forShuffleAlbumList(AlbumListType albumListType, int count, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        AlbumList albumList = createAlbumList(albumListType, 0, 40, userGroupId, username);
        List<MediaFile> songs = new ArrayList<MediaFile>();
        for (MediaFile album : albumList.getAlbums()) {
            songs.addAll(filterMusic(mediaFileService.getChildrenOf(album, true, false, false)));
        }
        Collections.shuffle(songs);
        songs = songs.subList(0, Math.min(count, songs.size()));
        return forMediaFiles(songs, username, request);
    }

    public List<AbstractMedia> forRadioArtist(int mediaFileId, int count, String username, HttpServletRequest request) {
    	try {
    		MediaFile artist = mediaFileService.getMediaFile(mediaFileId);
    		List<MediaFile> songs = filterMusic(lastFmServiceBasic.getSimilarSongs(artist, count));
    		Collections.shuffle(songs);
    		songs = songs.subList(0, Math.min(count, songs.size()));
    		return forMediaFiles(songs, username, request);
    	} catch (IOException ex) {
    		//TODO:
    	}
    	return null;
    } 

    public List<AbstractMedia> forTopSongs(int mediaFileId, String username, HttpServletRequest request) {
        MediaFile artist = mediaFileService.getMediaFile(mediaFileId); 
        int userGroupId = securityService.getCurrentUserGroupId(username);
        List<MusicFolder> musicFolders = settingsService.getMusicFoldersForGroup(userGroupId);        
        List<MediaFile> songs = filterMusic(lastFmServiceBasic.getTopSongs(artist, 50, musicFolders));
        return forMediaFiles(songs, username, request);
    }

    public List<AbstractMedia> forLibrary(String username, HttpServletRequest request) {
        List<AbstractMedia> result = new ArrayList<AbstractMedia>();
        int userGroupId = securityService.getCurrentUserGroupId(username);
        List<MusicFolder> musicFolders = settingsService.getMusicFoldersForGroup(userGroupId);
        if (musicFolders.size() == 1) {
            return forMusicFolder(musicFolders.get(0), username, request);
        }

        for (MusicFolder musicFolder : musicFolders) {
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setItemType(ItemType.COLLECTION);
            mediaCollection.setId(SonosService.ID_MUSICFOLDER_PREFIX + musicFolder.getId());
            mediaCollection.setTitle(musicFolder.getName());
            result.add(mediaCollection);
        }
        return result;
    }

    public List<AbstractMedia> forMusicFolder(int musicFolderId, String username, HttpServletRequest request) {
        return forMusicFolder(settingsService.getMusicFolderById(musicFolderId), username, request);
    }

    public List<AbstractMedia> forMusicFolder(MusicFolder musicFolder, String username, HttpServletRequest request) {
        try {
            List<AbstractMedia> result = new ArrayList<AbstractMedia>();
            MediaMetadata shuffle = new MediaMetadata();
            shuffle.setItemType(ItemType.PROGRAM);
            shuffle.setId(SonosService.ID_SHUFFLE_MUSICFOLDER_PREFIX + musicFolder.getId());
            shuffle.setTitle("Shuffle Play");
            result.add(shuffle);

            for (MediaFile shortcut : musicIndexService.getShortcutsX(Arrays.asList(musicFolder))) {
                result.add(forDirectory(shortcut, request, username));
            }
            
            //TODO: FIX to new Content
            MusicFolderContent musicFolderContent = musicIndexService.getMusicFolderContentX(Arrays.asList(musicFolder), false);
            for (List<MusicIndex.SortableArtistWithMediaFiles> artists : musicFolderContent.getIndexedArtists().values()) {
                for (MusicIndex.SortableArtistWithMediaFiles artist : artists) {
                    for (MediaFile artistMediaFile : artist.getMediaFiles()) {
                        result.add(forDirectory(artistMediaFile, request, username));
                    }
                }
            }
            for (MediaFile song : musicFolderContent.getSingleSongs()) {
                if (song.isAudio()) {
                    result.add(forSong(song, username, request));
                }
            }
            return result;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<AbstractMedia> forDirectoryContent(int mediaFileId, String username, HttpServletRequest request) {
        List<AbstractMedia> result = new ArrayList<AbstractMedia>();
        MediaFile dir = mediaFileService.getMediaFile(mediaFileId);
        List<MediaFile> children = dir.isFile() ? Arrays.asList(dir) : mediaFileService.getChildrenOf(dir, true, true, true);
        boolean isArtist = true;
        for (MediaFile child : children) {
            if (child.isDirectory()) {
                result.add(forDirectory(child, request, username));
                isArtist &= child.isAlbum();
            } else if (child.isAudio()) {
                isArtist = false;
                result.add(forSong(child, username, request));
            }
        }

        if (isArtist) {
            MediaMetadata shuffle = new MediaMetadata();
            shuffle.setItemType(ItemType.PROGRAM);
            shuffle.setId(SonosService.ID_SHUFFLE_ARTIST_PREFIX + mediaFileId);
            shuffle.setTitle(String.format("Shuffle Play  - %s", dir.getName()));
            result.add(0, shuffle);

            MediaMetadata radio = new MediaMetadata();
            radio.setItemType(ItemType.PROGRAM);
            radio.setId(SonosService.ID_RADIO_ARTIST_PREFIX + mediaFileId);
            radio.setTitle(String.format("Artist Radio - %s", dir.getName()));
            result.add(1, radio);

            MediaCollection topSongs = new MediaCollection();
            topSongs.setItemType(ItemType.TRACK);
            topSongs.setCanPlay(true);
            topSongs.setId(SonosService.ID_TOP_SONGS_PREFIX + mediaFileId);
            topSongs.setTitle(String.format("Top Songs - %s", dir.getName()));
            result.add(2, topSongs);
        }

        return result;
    }

    private MediaCollection forDirectory(MediaFile dir, HttpServletRequest request, String username) {
    	
        MediaCollection mediaCollection = new MediaCollection();
        mediaCollection.setId(String.valueOf(dir.getId()));

        if (settingsService.getSonosRatingType() == SonosRatingType.STARRED) {
            mediaFileService.populateStarredDate(dir, username);
            mediaCollection.setIsFavorite(dir.getStarredDate() != null);
        } else {
            mediaFileService.populateLovedDate(dir, username);
            mediaCollection.setIsFavorite(dir.getLovedDate() != null);
        }
        
        if (dir.isAlbum()) {
            mediaCollection.setItemType(ItemType.ALBUM);
            mediaCollection.setArtist(dir.getArtist());
            mediaCollection.setTitle(dir.getAlbumName());
            mediaCollection.setCanPlay(true);
            AlbumArtUrl albumArtUrl = new AlbumArtUrl();
            albumArtUrl.setValue(getCoverArtUrl(dir, request));
            mediaCollection.setAlbumArtURI(albumArtUrl);
            
        } else {
            mediaCollection.setItemType(ItemType.ARTIST);
            mediaCollection.setTitle(dir.getName());
            AlbumArtUrl albumArtUrl = new AlbumArtUrl();
            albumArtUrl.setValue(getCoverArtUrl(dir, request));
            mediaCollection.setAlbumArtURI(albumArtUrl);            
            if (lastFmServiceBasic.isArtistBioCached(dir.getName())) {
                ArtistBio artistBio = lastFmServiceBasic.getArtistBio(dir);
                if (artistBio != null && artistBio.getSmallImageUrl() != null) {
                    AlbumArtUrl artistImageUrl = new AlbumArtUrl();
                    artistImageUrl.setValue(artistBio.getMediumImageUrl());
                    mediaCollection.setAlbumArtURI(artistImageUrl);
                }
            }
        } 
        return mediaCollection;
    }

    public List<MediaCollection> forPlaylists(String username, HttpServletRequest request) {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (Playlist playlist : playlistService.getReadablePlaylistsForUser(username, null, "asc")) {
            MediaCollection mediaCollection = new MediaCollection();
            AlbumArtUrl albumArtUrl = new AlbumArtUrl();
            albumArtUrl.setValue(getCoverArtUrl(CoverArtController.PLAYLIST_COVERART_PREFIX + playlist.getId(), request));
            mediaCollection.setId(SonosService.ID_PLAYLIST_PREFIX + playlist.getId());
            mediaCollection.setCanPlay(true);
            mediaCollection.setReadOnly(!username.equals(playlist.getUsername()));
            mediaCollection.setRenameable(username.equals(playlist.getUsername()));
            mediaCollection.setUserContent(false);
            mediaCollection.setItemType(ItemType.PLAYLIST);
            mediaCollection.setArtist(playlist.getUsername());
            mediaCollection.setTitle(playlist.getName());
            mediaCollection.setAlbumArtURI(albumArtUrl);
            result.add(mediaCollection);
        }
        return result;
    }

    public List<MediaCollection> forChartLists() {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (ChartListType chartListType : ChartListType.values()) {
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setId(SonosService.ID_CHARTLIST_PREFIX + chartListType.getId());
            mediaCollection.setItemType(ItemType.ALBUM_LIST);
            mediaCollection.setTitle(chartListType.getDescription());
            result.add(mediaCollection);
        }
        return result;
    }      
    
    public List<MediaCollection> forAlbumLists() {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (AlbumListType albumListType : AlbumListType.values()) {
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setId(SonosService.ID_ALBUMLIST_PREFIX + albumListType.getId());
            mediaCollection.setItemType(ItemType.ALBUM_LIST);
            mediaCollection.setTitle(albumListType.getDescription());
            result.add(mediaCollection);
        }
        return result;
    }    

    public List<MediaCollection> forTrackLists() {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (TrackListType trackListType : TrackListType.values()) {
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setId(SonosService.ID_TRACKLIST_PREFIX + trackListType.getId());
            mediaCollection.setItemType(ItemType.TRACK_LIST);
            mediaCollection.setTitle(trackListType.getDescription());
            if (trackListType != TrackListType.DECADE && trackListType != TrackListType.GENRE) {
                mediaCollection.setCanPlay(true);
            }
            result.add(mediaCollection);
        }
        return result;
    }     
    
    public List<MediaCollection> forArtistLists() {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (ArtistListType artistListType : ArtistListType.values()) {
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setId(SonosService.ID_ARTISTLIST_PREFIX + artistListType.getId());
            mediaCollection.setItemType(ItemType.CONTAINER);
            mediaCollection.setTitle(artistListType.getDescription());
            result.add(mediaCollection);
        }
        return result;
    }    
    
    public List<MediaCollection> forPodcastChannels(HttpServletRequest request) {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (PodcastChannel channel : podcastService.getAllChannels()) {
            AlbumArtUrl albumArtUri = new AlbumArtUrl();
            albumArtUri.setValue(getCoverArtUrl(CoverArtController.PODCAST_COVERART_PREFIX + channel.getId(), request));
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setId(SonosService.ID_PODCAST_CHANNEL_PREFIX + channel.getId());
            mediaCollection.setTitle(channel.getTitle());
            mediaCollection.setItemType(ItemType.TRACK);
            mediaCollection.setAlbumArtURI(albumArtUri);
            result.add(mediaCollection);
        }
        return result;
    }

    public List<AbstractMedia> forPodcastChannel(int channelId, String username, HttpServletRequest request) {
        List<AbstractMedia> result = new ArrayList<AbstractMedia>();
        for (PodcastEpisode episode : podcastService.getEpisodes(channelId)) {
            if (episode.getStatus() == PodcastStatus.COMPLETED) {
                Integer mediaFileId = episode.getMediaFileId();
                MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
                if (mediaFile != null) {
                    result.add(forMediaFile(mediaFile, username, request));
                }
            }
        }
        return result;
    }

    public MediaList forChartList(ChartListType chartListType, int offset, int count, String username, HttpServletRequest request) {

    	//TODO:FINISH! CHARTS
        MediaList mediaList = new MediaList();
        int userGroupId = securityService.getCurrentUserGroupId(username);
        AlbumList albumList = createChartList( chartListType, offset, count, userGroupId, username);
        for (MediaFile album : albumList.getAlbums()) {
            mediaList.getMediaCollectionOrMediaMetadata().add(forDirectory(album, request, username));
        }
        mediaList.setIndex(offset);
        mediaList.setCount(mediaList.getMediaCollectionOrMediaMetadata().size());
        mediaList.setTotal(albumList.getTotal() + 1);
        return mediaList;
    
    }
    
    public MediaList forArtistList(ArtistListType artistListType, int offset, int count, String username, HttpServletRequest request) {
        if (artistListType == ArtistListType.GENRE) {
            return forArtistGenres(offset, count);
        }
        MediaList mediaList = new MediaList();
        boolean includeShuffle = offset == 0;
        if (includeShuffle) {
            count--;
            MediaMetadata shuffle = new MediaMetadata();
            shuffle.setItemType(ItemType.PROGRAM);
            shuffle.setId(SonosService.ID_SHUFFLE_ALBUMLIST_PREFIX + artistListType.getId());
            shuffle.setTitle(String.format("Shuffle Play - %s", artistListType.getDescription()));
            mediaList.getMediaCollectionOrMediaMetadata().add(shuffle);
        }
        int userGroupId = securityService.getCurrentUserGroupId(username);
        AlbumList albumList = createArtistList(artistListType, offset - (includeShuffle ? 0 : 1), count, userGroupId, username);
        for (MediaFile album : albumList.getAlbums()) {
            mediaList.getMediaCollectionOrMediaMetadata().add(forDirectory(album, request, username));
        }

        mediaList.setIndex(offset);
        mediaList.setCount(mediaList.getMediaCollectionOrMediaMetadata().size());
        mediaList.setTotal(albumList.getTotal() + 1);
        return mediaList;
    }    
    
    public MediaList forAlbumList(AlbumListType albumListType, int offset, int count, String username, HttpServletRequest request) {
        if (albumListType == AlbumListType.DECADE) {
            return forDecades(offset, count);
        }
        if (albumListType == AlbumListType.GENRE) {
            return forGenres(offset, count);
        }

        MediaList mediaList = new MediaList();

        boolean includeShuffle = offset == 0;
        if (includeShuffle) {
            count--;
            MediaMetadata shuffle = new MediaMetadata();
            shuffle.setItemType(ItemType.PROGRAM);
            shuffle.setId(SonosService.ID_SHUFFLE_ALBUMLIST_PREFIX + albumListType.getId());
            shuffle.setTitle(String.format("Shuffle Play - %s", albumListType.getDescription()));
            mediaList.getMediaCollectionOrMediaMetadata().add(shuffle);
        }
        int userGroupId = securityService.getCurrentUserGroupId(username);
        AlbumList albumList = createAlbumList(albumListType, offset - (includeShuffle ? 0 : 1), count, userGroupId, username);
        for (MediaFile album : albumList.getAlbums()) {
            mediaList.getMediaCollectionOrMediaMetadata().add(forDirectory(album, request, username));
        }

        mediaList.setIndex(offset);
        mediaList.setCount(mediaList.getMediaCollectionOrMediaMetadata().size());
        mediaList.setTotal(albumList.getTotal() + 1);
        return mediaList;
    }

    public MediaList forTrackList(TrackListType trackListType, int offset, int count, String username, HttpServletRequest request) {
        if (trackListType == TrackListType.DECADE) {
            return forTrackDecades(offset, count);
        }
        if (trackListType == TrackListType.GENRE) {
            return forTrackGenres(offset, count);
        }
        MediaList mediaList = new MediaList();
        int userGroupId = securityService.getCurrentUserGroupId(username);
        TrackList trackList = createTrackList(trackListType, offset, count, userGroupId, username);
        for (MediaFile track : trackList.getTracks()) {
            mediaList.getMediaCollectionOrMediaMetadata().add(forSong(track, username, request));
        }
        mediaList.setIndex(offset);
        mediaList.setCount(mediaList.getMediaCollectionOrMediaMetadata().size());
        mediaList.setTotal(trackList.getTotal() + 1);
        return mediaList;
    }    
    
    private AlbumList createArtistList(ArtistListType artistListType, int offset, int count, int userGroupId, String username) {
        List<MediaFile> artists = Collections.emptyList();
        int total = 0;
        switch (artistListType) {
        case NEWEST:
        	artists = mediaFileDao.getHistory(offset, count, userGroupId, ARTIST.name());
            total = mediaFileDao.getHistory(0, Integer.MAX_VALUE, userGroupId, ARTIST.name()).size();
            break;
        case STARRED:
        	artists = mediaFileService.getStarredArtists(offset, count, username);
            total = mediaFileService.getStarredArtists(0, Integer.MAX_VALUE, username).size();
            break;
        case ALPHABETICAL:
        	artists = mediaFileService.getArtists(offset, count, username, userGroupId);
            total = mediaFileService.getArtists(0, Integer.MAX_VALUE, username, userGroupId).size();
            break;
		default:
			break;
	    }
    return new AlbumList(artists, total);
    }
    
    private AlbumList createChartList(ChartListType chartListType, int offset, int count, int userGroupId, String username) {
    	
    	//TODO:FINISH! CHARTS
        List<MediaFile> albums = Collections.emptyList();
        int total = 0;
        switch (chartListType) {
            case US:
                break;
            case DE:
                break;
		default:
			break;
        }
        return new AlbumList(albums, total);
    }    
    
    private AlbumList createAlbumList(AlbumListType albumListType, int offset, int count, int userGroupId, String username) {
    	
        List<MusicFolder> musicFolders = settingsService.getMusicFoldersForGroup(userGroupId);
        List<MediaFile> albums = Collections.emptyList();
        int total = 0;
        switch (albumListType) {
            case RANDOM:
                albums = searchService.getRandomAlbums(count, musicFolders);
                total = mediaFileService.getAlbumCount(musicFolders);
                break;
            case NEWEST:
                albums = mediaFileService.getNewestAlbums(offset, count, userGroupId);
                total = mediaFileService.getAlbumCount(musicFolders);
                break;
            case STARRED:
                albums = mediaFileService.getStarredAlbums(offset, count, username);
                total = mediaFileService.getStarredAlbumCount(username, musicFolders);
                break;
            case HIGHEST:
                albums = ratingService.getHighestRatedAlbums(offset, count, userGroupId);
                total = ratingService.getRatedAlbumCount(username, musicFolders);
                break;
            case FREQUENT:
                albums = mediaFileService.getMostFrequentlyPlayedAlbums(offset, count, userGroupId);
                total = mediaFileService.getPlayedAlbumCount(musicFolders);
                break;
            case RECENT:
                albums = mediaFileService.getMostRecentlyPlayedAlbums(offset, count, userGroupId);
                total = mediaFileService.getPlayedAlbumCount(musicFolders);
                break;
            case ALPHABETICAL:
                albums = mediaFileService.getAlphabeticalAlbums(offset, count, true, userGroupId);
                total = mediaFileService.getAlbumCount(musicFolders);
                break;
		default:
			break;
        }
        return new AlbumList(albums, total);
    }
    
    private TrackList createTrackList(TrackListType trackListType, int offset, int count, int userGroupId, String username) {
        List<MediaFile> tracks = Collections.emptyList();
        int total = 0;
        switch (trackListType) {
            case RANDOM:
                RandomSearchCriteria searchCriteria = new RandomSearchCriteria(count, null, null, null, null);
            	tracks = searchService.getRandomSongs(searchCriteria);
            	total = tracks.size();
                break;
            case NEWEST:
            	tracks = mediaFileDao.getHistory(offset, count, userGroupId, MUSIC.name());
            	total = tracks.size();
                break;
            case FREQUENT:
            	tracks = mediaFileDao.getTopPlayedCountForUser(offset, count, username);
            	total = tracks.size();
                break;
            case RECENT:
            	tracks = mediaFileDao.getLastPlayedCountForUser(offset, count, username);
            	total = tracks.size();
                break;
		default:
			break;
        }
        tracks = filterMusic(tracks);
        return new TrackList(tracks, total);
    }    

    private MediaList forDecades(int offset, int count) {
        List<MediaCollection> mediaCollections = new ArrayList<MediaCollection>();
        int currentDecade = Calendar.getInstance().get(Calendar.YEAR) / 10;
        for (int i = 0; i < 10; i++) {
            int decade = (currentDecade - i) * 10;
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setItemType(ItemType.ALBUM_LIST);
            mediaCollection.setId(SonosService.ID_ALBUM_DECADE_PREFIX + decade);
            mediaCollection.setTitle(String.valueOf(decade));
            mediaCollections.add(mediaCollection);
        }
        return createSubList(offset, count, mediaCollections);
    }

    private MediaList forGenres(int offset, int count) {
        List<MediaCollection> mediaCollections = new ArrayList<MediaCollection>();
        List<Genre> genres = mediaFileService.getGenres(true);
        for (int i = 0; i < genres.size(); i++) {
            Genre genre = genres.get(i);
            if (genre.getAlbumCount() > 0) {
                MediaCollection mediaCollection = new MediaCollection();
                mediaCollection.setItemType(ItemType.ALBUM_LIST);
                mediaCollection.setId(SonosService.ID_ALBUM_GENRE_PREFIX + i);
                mediaCollection.setTitle(genre.getName() + " (" + genre.getAlbumCount() + ")");
                mediaCollections.add(mediaCollection);
            }
        }
        return createSubList(offset, count, mediaCollections);
    }
    
    private MediaList forTrackDecades(int offset, int count) {
    	
        List<MediaCollection> mediaCollections = new ArrayList<MediaCollection>();
        int currentDecade = Calendar.getInstance().get(Calendar.YEAR) / 10;
        for (int i = 0; i < 10; i++) {
            int decade = (currentDecade - i) * 10;
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setItemType(ItemType.TRACK_LIST);
            mediaCollection.setId(SonosService.ID_TRACK_DECADE_PREFIX + decade);
            mediaCollection.setTitle(String.valueOf(decade));
            mediaCollection.setCanPlay(true);            
            mediaCollections.add(mediaCollection);
        }
        return createSubList(offset, count, mediaCollections);    	
    }    
    
    private MediaList forTrackGenres(int offset, int count) {
        List<MediaCollection> mediaCollections = new ArrayList<MediaCollection>();
        List<Genre> genres = mediaFileService.getGenres(true);
        for (int i = 0; i < genres.size(); i++) {
            Genre genre = genres.get(i);
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setItemType(ItemType.TRACK_LIST);
            mediaCollection.setId(SonosService.ID_TRACK_GENRE_PREFIX + i);
            mediaCollection.setTitle(genre.getName() + " (" + genre.getSongCount() + ")");
            mediaCollection.setCanPlay(true);
            mediaCollections.add(mediaCollection);
        }
        return createSubList(offset, count, mediaCollections);
    }    
    
    private MediaList forArtistGenres(int offset, int count) {
        List<MediaCollection> mediaCollections = new ArrayList<MediaCollection>();
        List<Genre> genres = mediaFileService.getGenreArtistList();
        for (int i = 0; i < genres.size(); i++) {
            Genre genre = genres.get(i);
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.setItemType(ItemType.ALBUM_LIST);
            mediaCollection.setId(SonosService.ID_ARTIST_GENRE_PREFIX + i);
            mediaCollection.setTitle(genre.getName() + " (" + genre.getArtistCount() + ")");
            mediaCollections.add(mediaCollection);
        }
        return createSubList(offset, count, mediaCollections);
    }    
    
    public List<MediaCollection> forDecade(int decade, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (MediaFile album : mediaFileService.getAlbumsByYear(0, Integer.MAX_VALUE, decade, decade + 9, userGroupId)) {
            result.add(forDirectory(album, request, username));
        }
        return result;
    }

    public List<AbstractMedia> forTrackDecade(int decade, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        List<MediaFile> songs = mediaFileDao.getSongsByYear(0, Integer.MAX_VALUE, decade, decade + 9, userGroupId); 
        return forMediaFiles(songs, username, request);
    }  
    
    public List<MediaMetadata> forTrackGenre(int genreIndex, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        Genre genre = mediaFileService.getGenres(true).get(genreIndex);
        List<MediaMetadata> result = new ArrayList<MediaMetadata>();
        List<MediaFile> songs = mediaFileDao.getSongsByGenre(0, Integer.MAX_VALUE, genre.getName(), userGroupId);
        for (MediaFile song : songs) {
            if (song.isAudio()) {
                result.add(forSong(song, username, request));
            }
        }
        return result;
    }

    public List<MediaCollection> forGenre(int genreIndex, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        Genre genre = mediaFileService.getGenres(true).get(genreIndex);
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (MediaFile album : mediaFileService.getAlbumsByGenre(0, Integer.MAX_VALUE, genre.getName(), userGroupId)) {
            result.add(forDirectory(album, request, username));
        }
        return result;
    }

    public List<MediaCollection> forArtistGenre(int genreIndex, String username, HttpServletRequest request) {
        int userGroupId = securityService.getCurrentUserGroupId(username);
        Genre genre = mediaFileService.getGenreArtistList().get(genreIndex);
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (MediaFile album : mediaFileDao.getArtistsByGenre(genre.getName(), 0, Integer.MAX_VALUE, userGroupId)) {
            result.add(forDirectory(album, request, username));
        }
        return result;
    }    
    
    public List<MediaMetadata> forPlaylist(int playlistId, String username, HttpServletRequest request) {
        List<MediaMetadata> result = new ArrayList<MediaMetadata>();
        for (MediaFile song : playlistService.getFilesInPlaylist(playlistId)) {
            if (song.isAudio()) {
                result.add(forSong(song, username, request));
            }
        }
        return result;
    }

    public List<MediaCollection> forLoved() {
        MediaCollection songs = new MediaCollection();
        songs.setItemType(ItemType.TRACK_LIST);
        songs.setId(SonosService.ID_LOVED_SONGS);
        songs.setCanPlay(true);
        songs.setTitle("Loved Songs");
        return Arrays.asList(songs);
    }    
    
    public List<MediaMetadata> forLovedSongs(String username, HttpServletRequest request) {
        List<MediaMetadata> result = new ArrayList<MediaMetadata>();
        for (MediaFile song : mediaFileDao.getLovedFiles(0, Integer.MAX_VALUE, username)) {
            if (song.isAudio()) {
                result.add(forSong(song, username, request));
            }
        }
        return result;
    }
    
    public List<MediaCollection> forStarred() {
        MediaCollection artists = new MediaCollection();
        artists.setItemType(ItemType.ARTIST);
        artists.setId(SonosService.ID_STARRED_ARTISTS);
        artists.setTitle("Starred Artists");

        MediaCollection albums = new MediaCollection();
        albums.setItemType(ItemType.ALBUM_LIST);
        albums.setId(SonosService.ID_STARRED_ALBUMS);
        albums.setTitle("Starred Albums");

        MediaCollection songs = new MediaCollection();
        songs.setItemType(ItemType.TRACK_LIST);
        songs.setId(SonosService.ID_STARRED_SONGS);
        songs.setCanPlay(true);
        songs.setTitle("Starred Songs");

        return Arrays.asList(artists, albums, songs);
    }

    public List<MediaCollection> forStarredArtists(String username, HttpServletRequest request) {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (MediaFile artist : mediaFileDao.getStarredDirectories(0, Integer.MAX_VALUE, username)) {
            MediaCollection mediaCollection = forDirectory(artist, request, username);
            mediaCollection.setItemType(ItemType.ARTIST);
            result.add(mediaCollection);
        }
        return result;
    }

    public List<MediaCollection> forStarredAlbums(String username, HttpServletRequest request) {
        List<MediaCollection> result = new ArrayList<MediaCollection>();
        for (MediaFile album : mediaFileDao.getStarredAlbums(0, Integer.MAX_VALUE, username )) {
            MediaCollection mediaCollection = forDirectory(album, request, username);
            mediaCollection.setItemType(ItemType.ALBUM);
            result.add(mediaCollection);
        }
        return result;
    }

    public List<MediaMetadata> forStarredSongs(String username, HttpServletRequest request) {
        List<MediaMetadata> result = new ArrayList<MediaMetadata>();
        for (MediaFile song : mediaFileDao.getStarredFiles(0, Integer.MAX_VALUE, username)) {
            if (song.isAudio()) {
                result.add(forSong(song, username, request));
            }
        }
        return result;
    }
    
    public List<MediaCollection> forSearchCategories() {
        MediaCollection artists = new MediaCollection();
        artists.setItemType(ItemType.ARTIST);
        artists.setId(SonosService.ID_SEARCH_ARTISTS);
        artists.setTitle("Artists");

        MediaCollection albums = new MediaCollection();
        albums.setItemType(ItemType.ALBUM);
        albums.setId(SonosService.ID_SEARCH_ALBUMS);
        albums.setTitle("Albums");

        MediaCollection songs = new MediaCollection();
        songs.setItemType(ItemType.TRACK);
        songs.setId(SonosService.ID_SEARCH_SONGS);
        songs.setTitle("Songs");

        return Arrays.asList(artists, albums, songs);
    }

    public MediaList forSearch(String query, int offset, int count, SearchService.IndexType indexType, String username, HttpServletRequest request) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCount(count);
        searchCriteria.setOffset(offset);
        searchCriteria.setQuery(query);
        int userGroupId = securityService.getCurrentUserGroupId(username);
        SearchResult searchResult = searchService.search(searchCriteria, indexType, userGroupId);
        MediaList result = new MediaList();
        result.setTotal(searchResult.getTotalHits());
        result.setIndex(offset);
        result.setCount(searchResult.getMediaFiles().size());
        for (MediaFile mediaFile : searchResult.getMediaFiles()) {
            result.getMediaCollectionOrMediaMetadata().add(forMediaFile(mediaFile, username, request));
        }
        return result;
    }

    public List<AbstractMedia> forSimilarArtists(int mediaFileId, String username, HttpServletRequest request) {
        MediaFile mediaFile = mediaFileService.getMediaFile(mediaFileId);
        List<MediaFile> similarArtists = lastFmServiceBasic.getSimilarArtists(mediaFile, 100, false);
        return forMediaFiles(similarArtists, username, request);
    }

    private List<AbstractMedia> forMediaFiles(List<MediaFile> mediaFiles, String username, HttpServletRequest request) {
        List<AbstractMedia> result = new ArrayList<AbstractMedia>();
        for (MediaFile mediaFile : mediaFiles) {
            result.add(forMediaFile(mediaFile, username, request));
        }
        return result;
    }

    public AbstractMedia forMediaFile(MediaFile mediaFile, String username, HttpServletRequest request) {
        return mediaFile.isFile() ? forSong(mediaFile, username, request) : forDirectory(mediaFile, request, username);
    }

    public MediaMetadata forSong(MediaFile song, String username, HttpServletRequest request) {
        Player player = createPlayerIfNecessary(username);
        String suffix = transcodingService.getSuffix(player, song, null);
        
        MediaMetadata result = new MediaMetadata();
        result.setId(String.valueOf(song.getId()));
        result.setItemType(ItemType.TRACK);
        result.setMimeType(StringUtil.getMimeType(suffix, true));
        result.setTitle(song.getTitle());
        result.setGenre(song.getGenre());
        
        DynamicData dynamicData = new DynamicData();
        
        if (settingsService.getSonosRatingType() == SonosRatingType.STARRED) {
            mediaFileService.populateStarredDate(song, username);
            result.setIsFavorite(song.getStarredDate() != null);
            Property starredProperty = new Property();
            starredProperty.setName("isStarred");
            starredProperty.setValue(song.getStarredDate() == null ? "0" : "1");
            dynamicData.getProperty().add(starredProperty);
        	
        } else {
            mediaFileService.populateLovedDate(song, username);
            result.setIsFavorite(song.getLovedDate() != null);
            Property lovedProperty = new Property();
            lovedProperty.setName("isLoved");
            lovedProperty.setValue(song.getLovedDate() == null ? "0" : "1");
            dynamicData.getProperty().add(lovedProperty);        
        }
        result.setDynamic(dynamicData);

        AlbumArtUrl albumArtUrl = new AlbumArtUrl();
        albumArtUrl.setValue(getCoverArtUrl(song, request));

        TrackMetadata trackMetadata = new TrackMetadata();
        trackMetadata.setArtist(song.getArtist());
        trackMetadata.setAlbumArtist(song.getAlbumArtist());
        trackMetadata.setAlbum(song.getAlbumName());
        trackMetadata.setAlbumArtURI(albumArtUrl);
        trackMetadata.setDuration(song.getDurationSeconds());
        trackMetadata.setTrackNumber(song.getTrackNumber());
        
        MediaFile parent = mediaFileService.getParentOf(song);
        if (parent != null && parent.isAlbum()) {
            trackMetadata.setAlbumId(String.valueOf(parent.getId()));
        }
        result.setTrackMetadata(trackMetadata);
        return result;
    }

    public void star(int id, String username) {
        mediaFileDao.starMediaFile(id, username);
    }

    public void unstar(int id, String username) {
        mediaFileDao.unstarMediaFile(id, username);
    }

    public void love(int id, String username) {
        mediaFileDao.loveMediaFile(id, username);
    }

    public void unlove(int id, String username) {
        mediaFileDao.unloveMediaFile(id, username);
    }    
    
    private String getCoverArtUrl(String id, HttpServletRequest request) {
    	int size = settingsService.isCoverArtHQ() ? 600 : CoverArtScheme.POSTER.getSize();
        return getBaseUrl(request) + "coverArt.view?id=" + id + "&size=" + size;
    }

    private String getCoverArtUrl(MediaFile file, HttpServletRequest request) {
    	int size = settingsService.isCoverArtHQ() ? 600 : CoverArtScheme.POSTER.getSize();
        return getBaseUrl(request) + "coverArt.view?id=" + file.getId() + "&auth=" + file.getHash() + "&size=" + size;
    }

    private String getSonosArtUrl(String id, HttpServletRequest request) {
    	return getBaseUrl(request) + "sonosArt.view?q=" + id +"&size=0";
    }
    
    public static MediaList createSubList(int index, int count, List<? extends AbstractMedia> media) {
        MediaList result = new MediaList();
        List<? extends AbstractMedia> selectedMedia = Util.subList(media, index, count);

        result.setIndex(index);
        result.setCount(selectedMedia.size());
        result.setTotal(media.size());
        result.getMediaCollectionOrMediaMetadata().addAll(selectedMedia);

        return result;
    }

    private List<MediaFile> filterMusic(List<MediaFile> files) {
        return Lists.newArrayList(Iterables.filter(files, new Predicate<MediaFile>() {
            @Override
            public boolean apply(MediaFile input) {
                return input.getMediaType() == MediaFile.MediaType.MUSIC;
            }
        }));
    }

    public String getMediaURI(int mediaFileId, String username, HttpServletRequest request) {
        Player player = createPlayerIfNecessary(username);
        MediaFile song = mediaFileService.getMediaFile(mediaFileId);

        return getBaseUrl(request) + "stream?id=" + song.getId() + "&auth=" + song.getHash() + "&player=" + player.getId();
    }

    private Player createPlayerIfNecessary(String username) {
        List<Player> players = playerService.getPlayersForUserAndClientId(username, MADSONIC_CLIENT_ID);

        // If not found, create it.
        if (players.isEmpty()) {
            Player player = new Player();
            player.setUsername(username);
            player.setClientId(MADSONIC_CLIENT_ID);
            player.setName("Sonos");
            player.setTechnology(PlayerTechnology.EXTERNAL_WITH_PLAYLIST);
            playerService.createPlayer(player);
            players = playerService.getPlayersForUserAndClientId(username, MADSONIC_CLIENT_ID);
        }
        return players.get(0);
    }

    public String getBaseUrl(HttpServletRequest request) {
        int port = settingsService.getPort();
        String contextPath = settingsService.getUrlRedirectContextPath();

        // Note that the server IP can be overridden by the "ip" parameter. Used when Madsonic and Sonos are
        // on different networks.
        String ip = settingsService.getLocalIpAddress();
        if (request != null) {
            ip = ServletRequestUtils.getStringParameter(request, "ip", ip);
        }

        // Note: Serving media and cover art with http (as opposed to https) works when using jetty and MadsonicDeployer.
        StringBuilder url = new StringBuilder("http://")
                .append(ip)
                .append(":")
                .append(port)
                .append("/");

        if (StringUtils.isNotEmpty(contextPath)) {
            url.append(contextPath).append("/");
        }
        return url.toString();
    }

    public void setPlaylistService(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    public void setPlayerService(PlayerService playerService) {
        this.playerService = playerService;
    }

    public void setTranscodingService(TranscodingService transcodingService) {
        this.transcodingService = transcodingService;
    }    
    
    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setMusicIndexService(MusicIndexService musicIndexService) {
        this.musicIndexService = musicIndexService;
    }

    public void setMediaFileDao(MediaFileDao mediaFileDao) {
        this.mediaFileDao = mediaFileDao;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setRatingService(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    public void setLastFmServiceBasic(LastFmServiceBasic lastFmServiceBasic) {
        this.lastFmServiceBasic = lastFmServiceBasic;
    }

    public void setPodcastService(PodcastService podcastService) {
        this.podcastService = podcastService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
    
}