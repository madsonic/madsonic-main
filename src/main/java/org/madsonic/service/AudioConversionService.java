/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.madsonic.Logger;
import org.madsonic.dao.AudioConversionDao;
import org.madsonic.domain.AudioConversion;
import org.madsonic.domain.MediaFile;
import org.madsonic.service.metadata.MetaData;
import org.madsonic.service.metadata.MetaDataParser;
import org.madsonic.service.metadata.MetaDataParserFactory;
import org.madsonic.util.Util;

import com.google.common.collect.Iterables;

/**
 * @author Martin Karel
 * @version $Id$
 */
public class AudioConversionService {

    private static final Logger LOG = Logger.getLogger(AudioConversionService.class);

    private MediaFileService mediaFileService;
    private TranscodingService transcodingService;
    private SettingsService settingsService;
    private AudioConversionDao audioConversionDao;
    private AudioConverter audioConverter;
    private MetaDataParserFactory metaDataParserFactory;

    public void init() {
        audioConverter = new AudioConverter();
        audioConverter.start();

        for (AudioConversion conversion : audioConversionDao.getAudioConversionsByStatus(AudioConversion.Status.IN_PROGRESS)) {
            deleteAudioConversion(conversion);
        }
    }

    public void createAudioConversion(AudioConversion conversion) {
    	audioConversionDao.deleteAudioConversionsForFile(conversion.getMediaFileId());
    	audioConversionDao.createAudioConversion(conversion);
    }

    public void deleteAudioConversion(AudioConversion conversion) {
        if (conversion.getTargetFile() != null) {
            File targetFile = new File(conversion.getTargetFile());
            if (targetFile.exists()) {
                targetFile.delete();
            }
        }
        if (conversion.getLogFile() != null) {
            File logFile = new File(conversion.getLogFile());
            if (logFile.exists()) {
                logFile.delete();
            }
        }

        audioConversionDao.deleteAudioConversion(conversion.getId());
        audioConverter.cancel(conversion);
    }

    public AudioConversion getAudioConversionForFile(int mediaFileId) {
        AudioConversion conversion = audioConversionDao.getAudioConversionForFile(mediaFileId);
        if (conversion == null) {
            return null;
        }

        List<AudioConversion> conversions = deleteIfFileMissing(Collections.singletonList(conversion));
        return Iterables.getFirst(conversions, null);
    }

    public List<AudioConversion> getAllAudioConversions() {
        return deleteIfFileMissing(audioConversionDao.getAllAudioConversions());
    }

    public MetaData getAudioMetaData(MediaFile audio) {
        MetaDataParser parser = metaDataParserFactory.getParser(audio.getFile());
        return parser != null ? parser.getMetaData(audio.getFile()) : null;
    }

    private List<AudioConversion> deleteIfFileMissing(List<AudioConversion> conversions) {
        List<AudioConversion> result = new ArrayList<AudioConversion>();
        for (AudioConversion conversion : conversions) {
            if (conversion.getStatus() == AudioConversion.Status.COMPLETED &&
                (conversion.getTargetFile() == null || !(new File(conversion.getTargetFile()).exists()))) {
                deleteAudioConversion(conversion);
            } else {
                result.add(conversion);
            }
        }
        return result;
    }

    public void setAudioConversionDao(AudioConversionDao audioConversionDao) {
        this.audioConversionDao = audioConversionDao;
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }

    public void setTranscodingService(TranscodingService transcodingService) {
        this.transcodingService = transcodingService;
    }

    public void setMetaDataParserFactory(MetaDataParserFactory metaDataParserFactory) {
        this.metaDataParserFactory = metaDataParserFactory;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public class AudioConverter extends Thread{

        private AudioConversion conversion;
        private Process process;
        private MediaFile mediaFile;

        public AudioConverter() {
            setDaemon(true);
        }

        @Override
        public void run() {
            while (true) {
                Util.sleep(5000L);
                List<AudioConversion> conversions = audioConversionDao.getAudioConversionsByStatus(AudioConversion.Status.NEW);
                if (!conversions.isEmpty()) {
                    conversion = conversions.get(0);
                    convert();
                }
            }
        }

        public void cancel(AudioConversion conversion) {
            if (process != null && this.conversion != null && this.conversion.getId().equals(conversion.getId())) {
                LOG.info("Killing conversion process for " + mediaFile);
                process.destroy();
            }
        }
        
        public void convert() {
            audioConversionDao.updateStatus(conversion.getId(), AudioConversion.Status.IN_PROGRESS);

            mediaFile = mediaFileService.getMediaFile(conversion.getMediaFileId());
            try {
                checkDiskLimit();
				
            	int bitrate = conversion.getBitRate();
            	String  targetFormat = conversion.getTargetFormat();
            	
                LOG.info("Starting " + targetFormat + " " + bitrate + "k audio conversion of: " + mediaFile);

                File logFile = new File(conversion.getLogFile());
                File targetFile = new File(conversion.getTargetFile());
                
                if (!targetFile.getParentFile().canWrite()) {
                    throw new Exception("Write access denied to " + targetFile);
                }

                List<String> command = buildFFmpegCommand(targetFile, targetFormat, bitrate);

                StringBuffer buf = new StringBuffer("Starting " + " converter " + targetFormat + " bitrate: " + bitrate + "k: ");
                for (String s : command) {
                    buf.append(s).append(" ");
                }
                LOG.debug(buf);

                process = new ProcessBuilder(command).redirectErrorStream(true).start();

                new ProcessReaderThread(process, conversion, logFile).start();
                int retval = process.waitFor();
                LOG.info("ffmpeg exit value: " + retval);

                boolean success =
                        audioConversionDao.getAudioConversionById(conversion.getId()) != null  // conversion was canceled (i.e., removed)
                        && targetFile.exists()
                        && targetFile.length() > 0;

                if (success) {
                    LOG.info("Completed audio conversion of: " + mediaFile);
                    audioConversionDao.updateStatus(conversion.getId(), AudioConversion.Status.COMPLETED);
                } else {
                    LOG.error("An error occurred while converting audio " + mediaFile + ". See log file " + logFile.getAbsolutePath());
                    audioConversionDao.updateStatus(conversion.getId(), AudioConversion.Status.ERROR);
                }

            } catch (Exception x) {
                LOG.error("An error occurred while converting audio " + mediaFile + ": " + x, x);
                audioConversionDao.updateStatus(conversion.getId(), AudioConversion.Status.ERROR);
            }
        }

        private void checkDiskLimit() {
            int limitInGB = settingsService.getAudioConversionDiskLimit();
            if (limitInGB == 0) {
                return;
            }
            long limitInBytes = limitInGB * FileUtils.ONE_GB;

            File dir = new File(settingsService.getAudioConversionDirectory());
            if (!dir.canRead() || !dir.isDirectory()){
                return;
            }

            List<File> files = new ArrayList<File>();
            long usedBytes = 0;
            for (File file : dir.listFiles((FileFilter) FileFileFilter.FILE)) {
                files.add(file);
                usedBytes += file.length();
            }

            if (usedBytes < limitInBytes) {
                return;
            }

            // Sort files by modification date.
            Collections.sort(files, new Comparator<File>() {
                @Override
                public int compare(File a, File b) {
                    long lastModifiedA = a.lastModified();
                    long lastModifiedB = b.lastModified();
                    if (lastModifiedA < lastModifiedB) {
                        return -1;
                    }
                    if (lastModifiedA > lastModifiedB) {
                        return 1;
                    }
                    return 0;
                }
            });

            // Delete files until we're below the limit.
            while (usedBytes > limitInBytes && !files.isEmpty()) {
                File victim = files.remove(0);
                usedBytes -= victim.length();
                victim.delete();
                LOG.info("Deleted converted audio file " + victim);
            }
        }

        private List<String> buildFFmpegCommand(File targetFile, String targetFormat, Integer bitrate) {
            List<String> command = new ArrayList<String>();

            command.add(transcodingService.getTranscodeDirectory() + File.separator + "ffmpeg");
            command.add("-i");
            command.add(mediaFile.getFile().getAbsolutePath());
            
            if ("ogg".contains(targetFormat)) {
	            command.add("-strict");
	            command.add("experimental");
	            command.add("-c:a");
	            command.add("vorbis");
	            command.add("-aq");
	            command.add("100");
            }
            if ("mp3".contains(targetFormat)) {
                command.add("-map");
                command.add("0:0");
	            command.add("-b:a");
	            command.add(bitrate + "k");
            }
            command.add("-f");
            command.add(targetFormat);
            command.add(targetFile.getAbsolutePath());
            return command;
        }
    }

    private class ProcessReaderThread extends Thread {
        private final Process process;
        private final AudioConversion conversion;
        private final File logFile;

        public ProcessReaderThread(Process process, AudioConversion conversion, File logFile) {
            this.process = process;
            this.conversion = conversion;
            this.logFile = logFile;
            setDaemon(true);
        }

        @Override
        public void run() {
        	
        	// :: MP3 :: size=    2975kB time=00:02:06.62 bitrate= 192.5kbits/s speed=16.7x  
            // :: OGG :: frame=    1 fps=0.2 q=-0.0 size=       9kB time=00:00:32.29 bitrate=   2.3kbits/s speed=5.83x  
        	
            Pattern pattern = Pattern.compile(".*time=(\\d+):(\\d+):(\\d+).(\\d+).*");
            BufferedReader reader = null;
            BufferedWriter logWriter = null;
            try {
                reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                logWriter = new BufferedWriter(new FileWriter(logFile));

                for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                    logWriter.append(line);
                    logWriter.newLine();
                    logWriter.flush();

                    Matcher matcher = pattern.matcher(line);
                    if (matcher.matches()) {
                        int hours = Integer.parseInt(matcher.group(1));
                        int minutes = Integer.parseInt(matcher.group(2));
                        int seconds = Integer.parseInt(matcher.group(3));

                        int progress = hours * 3600 + minutes * 60 + seconds;
                        audioConversionDao.updateProgress(conversion.getId(), progress);
                    }
                }
            } catch (IOException e) {
                LOG.warn("Error when reading output from audio converter.", e);
            } finally {
                IOUtils.closeQuietly(reader);
                IOUtils.closeQuietly(logWriter);
            }
        }
    }
}

