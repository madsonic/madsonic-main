/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.service.node;

import org.madsonic.domain.MediaFile;

/**
 * @author Martin Karel
 *
 */
public interface VirtualDataLine {
	
    public void start();
    
    public void stop();
    
    public void close();
    
    public boolean isOpen();
    
    public boolean isEnded();
    
    public int getPosition();
    
	public void open(MediaFile mediaFile);
	
}
