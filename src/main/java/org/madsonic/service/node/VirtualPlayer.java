/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.service.node;

import java.util.concurrent.atomic.AtomicReference;

import javax.sound.sampled.FloatControl;

import org.madsonic.Logger;
import org.madsonic.domain.MediaFile;
import org.madsonic.service.NodeService;

import static org.madsonic.service.node.VirtualPlayer.State.*;

/**
 * A virtual wrapper player.
 * <p/>
 * Supports pause and resume.
 *
 * @author Martin Karel
 * @version $Id$
 */
public class VirtualPlayer {

    public static final float DEFAULT_GAIN = 0.75f;

	private static final Logger LOG = Logger.getLogger(NodeService.class);

    private final Listener listener;
    private final VirtualDataLine line;
    private final AtomicReference<State> state = new AtomicReference<State>(PAUSED);
    private FloatControl gainControl;

    public VirtualPlayer(MediaFile mediaFile, Listener listener) throws Exception {
    	this.listener = listener;
        line = new VirtualDataLineImpl(mediaFile);
        setGain(DEFAULT_GAIN);
        new AudioDataWriter();
    }

    /**
     * Starts (or resumes) the player.  This only has effect if the current state is
     * {@link State#PAUSED}.
     */
    public synchronized void play() {
        if (state.get() == PAUSED) {
            line.start();
            setState(PLAYING);
        }
    }

    /**
     * Pauses the player.  This only has effect if the current state is
     * {@link State#PLAYING}.
     */
    public synchronized void pause() {
        if (state.get() == PLAYING) {
            setState(PAUSED);
            line.stop();
        }
    }

    /**
     * Closes the player, releasing all resources. After this the player state is
     * {@link State#CLOSED} (unless the current state is {@link State#EOM}).
     */
    public synchronized void close() {
        if (state.get() != CLOSED && state.get() != EOM) {
            setState(CLOSED);
        }

        try {
            line.stop();
        } catch (Throwable x) {
            LOG.warn("Failed to stop player: " + x, x);
        } 
    }

    /**
     * Returns the player state.
     */
    public State getState() {
        return state.get();
    }

    /**
     * Sets the gain.
     *
     * @param gain The gain between 0.0 and 1.0.
     */
    public void setGain(float gain) {
        if (gainControl != null) {

            double minGainDB = gainControl.getMinimum();
            double maxGainDB = Math.min(0.0, gainControl.getMaximum());  // Don't use positive gain to avoid distortion.
            double ampGainDB = 0.5f * maxGainDB - minGainDB;
            double cste = Math.log(10.0) / 20;
            double valueDB = minGainDB + (1 / cste) * Math.log(1 + (Math.exp(cste * ampGainDB) - 1) * gain);

            valueDB = Math.min(valueDB, maxGainDB);
            valueDB = Math.max(valueDB, minGainDB);

            gainControl.setValue((float) valueDB);
        }
    }

    /**
     * Returns the position in seconds.
     */
    public int getPosition() {
        return (int) (line.getPosition());
    }

    private void setState(State state) {
        if (this.state.getAndSet(state) != state && listener != null) {
            listener.stateChanged(this, state);
        }
    }

	private class AudioDataWriter implements Runnable {

        public AudioDataWriter() {
            new Thread(this).start();
        }

        public void run() {
            try {
            	
                while (true) {
                    switch (state.get()) {
                        case CLOSED:
                        	
                        case EOM:
                            return;
                            
                        case PAUSED:
                            Thread.sleep(250);
                            break;
                            
                        case PLAYING:
                        	if (line.isEnded()) {
                            	setState(EOM);
                        	}
                    }
                }
            } catch (Throwable x) {
                LOG.warn("Error when emulate audio data: " + x, x);
            } finally {
                close();
            }
        }
    }

    public interface Listener {
        void stateChanged(VirtualPlayer player, State state);
    }

    public static enum State {
        PAUSED,
        PLAYING,
        CLOSED,
        EOM
    }
}
