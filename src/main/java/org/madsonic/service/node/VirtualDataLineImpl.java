/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.service.node;

import org.madsonic.domain.MediaFile;

public class VirtualDataLineImpl implements VirtualDataLine {

	private int offset;
	
	private MediaTimer counter;
	private MediaFile mediaFile = new MediaFile();
	
	private boolean open = false;
	private boolean ended = false;
	
	public VirtualDataLineImpl(MediaFile mediaFile){
		this.mediaFile = mediaFile;
	}

	public MediaFile getMediafile() {
		return mediaFile;
	}	
	
	@Override
	public void start() {
		counter = new MediaTimer(mediaFile.getDurationSeconds());		
		counter.start();
		open = true;
	}

	@Override
	public void stop() {
		counter.requestStop();
		open = false;
	}

	@Override
	public void close() {
		open = false;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override	
	public boolean isEnded() {
		return ended;
	}

	@Override
	public int getPosition() {
        return counter.getPosition();
	}

	@Override
	public void open(MediaFile mediaFile) {
		this.mediaFile = mediaFile;
	}
		
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @param ended the ended to set
	 */
	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	class MediaTimer extends Thread {
		
		   private int counter = 0;
		   private int duration = 0;
		   
		   private volatile boolean stop = false;
		   
	    public MediaTimer(int duration) {
			this.duration = duration;
		}
		   
		public void run() {
		      while (!stop && counter < this.duration) {
		         counter++;
		         try { sleep(1000);
		         } catch (InterruptedException e) {}
		      }
		      if (counter >= this.duration) {
		    	  System.out.println("!EOF (c:=");
		    	  ended = true;
		    	  open = false;
		      }
		      
		      if (stop)
		      System.out.println("Detected stop");
	    	  ended = true;
	    	  open = false;
		   }
		   public void requestStop() {
		      stop = true;
		   }
		   
		   public int getPosition() {
			return this.counter;
		   }
		}
}
