/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.service;

import static org.madsonic.service.node.VirtualPlayer.State.EOM;

import java.util.List;

import org.madsonic.Logger;
import org.madsonic.dao.NodeDao;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.Node;
import org.madsonic.domain.PlayQueue;
import org.madsonic.domain.Player;
import org.madsonic.domain.User;
import org.madsonic.service.node.VirtualPlayer;
import org.madsonic.util.FileUtil;

/**
 * Plays music on the remotely node audio device.
 *
 * @author Martin Karel
 */
public class NodeService implements VirtualPlayer.Listener {

	private static final Logger LOG = Logger.getLogger(NodeService.class);	

	private SecurityService securityService;
	private VirtualPlayer virtualPlayer;	
	private NodeDao nodeDao;	

	private int offset;
	private boolean mute;
	private Player player;
	private MediaFile currentPlayingFile;    
	private float gain = VirtualPlayer.DEFAULT_GAIN;

	/**
	 * Updates the node by starting or pausing playback on the remotly audio device.
	 *
	 * @param player The player in question.
	 * @param offset Start playing after this many seconds into the track.
	 */
	public synchronized void updateNodes(Player player, int offset) throws Exception {
		User user = securityService.getUserByName(player.getUsername());

		if (!user.isJukeboxRole()) {
			LOG.warn(user.getUsername() + " is not authorized for node playback.");
			return;
		}

		if (player.getPlayQueue().getStatus() == PlayQueue.Status.PLAYING) {
			this.player = player;
			MediaFile result;
			synchronized (player.getPlayQueue()) {
				result = player.getPlayQueue().getCurrentFile();
			}
			play(result, offset);
		} else {
			if (virtualPlayer != null) {
				virtualPlayer.pause();
			}
		}	
	}

	private synchronized void play(MediaFile file, int offset) {
		try {
			// Resume if possible.
			boolean sameFile = file != null && file.equals(currentPlayingFile);
			boolean paused = virtualPlayer != null && virtualPlayer.getState() == VirtualPlayer.State.PAUSED;
			if (sameFile && paused && offset == 0) {
				virtualPlayer.play();
			} else {
				this.offset = offset;
				if (virtualPlayer != null) {
					virtualPlayer.close();
					if (currentPlayingFile != null) {
						onSongEnd(currentPlayingFile);
					}
				}
				if (file != null) {
					virtualPlayer = new VirtualPlayer(file, this);
					virtualPlayer.setGain(mute ? 0 : gain);
					virtualPlayer.play();
					onSongStart(file);
				}
			}
			currentPlayingFile = file;

		} catch (Exception x) {
			LOG.error("Error in node: " + x, x);
		}    	
	}

	public synchronized void stateChanged(VirtualPlayer virtualPlayer, VirtualPlayer.State state) {
		if (state == EOM) {
			player.getPlayQueue().next();
			MediaFile result;
			synchronized (player.getPlayQueue()) {
				result = player.getPlayQueue().getCurrentFile();
			}
			play(result, 0);
		}
	}

	private void onSongStart(MediaFile file) {
		LOG.info(player.getUsername() + " start sync to nodes: \"" + FileUtil.getShortPath(file.getFile()) + "\"");
	}

	private void onSongEnd(MediaFile file) {
		LOG.info(player.getUsername() + " stop sync to nodes: \"" + FileUtil.getShortPath(file.getFile()) + "\"");
	}    

	public List<Node> getAllNodes(){
		return nodeDao.getAllNodes();
	}

	public void createNode(Node node) {
		nodeDao.createNode(node);  
	}

	public void getNodeByUrl(String url) {
		nodeDao.getNode(url);  
	}
	
	public void deleteNode(Integer id) {
		nodeDao.deleteNode(id);  
	}

	public void updateNode(Node node) {
		nodeDao.updateNode(node);  
	}

	public float getGain() {
		return gain;
	}

	public void setGain(float gain) {
		this.gain = gain;
		if (gain > 0) {
			mute = false;
		}
		if (virtualPlayer != null) {
			virtualPlayer.setGain(gain);
		}
	}

	public int getPosition() {
		if (virtualPlayer != null && virtualPlayer.getState() == EOM) {
			return 0;
		}
		return virtualPlayer == null ? 0 : offset + virtualPlayer.getPosition();
	}

	public Player getPlayer() {
		return player;
	}    

	public void setNodeDao(NodeDao nodeDao) {
		this.nodeDao = nodeDao;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

}
