/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;

import org.madsonic.Logger;

import org.madsonic.core.api.Constants;
import org.madsonic.dao.ArtistDao;
import org.madsonic.dao.MediaFileDao;

import org.madsonic.domain.AlbumNotes;
import org.madsonic.domain.ArtistBio;
import org.madsonic.domain.LastFMCoverArt;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MusicFolder;

import org.madsonic.lastfm.Album;
import org.madsonic.lastfm.Artist;
import org.madsonic.lastfm.Caller;
import org.madsonic.lastfm.ImageSize;
import org.madsonic.lastfm.Track;
import org.madsonic.lastfm.cache.Cache;

/**
 * Provides services from the Last.fm REST API.
 *
 * @author Sindre Mehus, Martin Karel
 * @version $Id$
 */
public class LastFmServiceBasic {

    private static final String LAST_FM_KEY = Constants.LAST_FM_KEY;
    private static final Logger LOG = Logger.getLogger(LastFmServiceBasic.class);
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
	
    
    private MediaFileService mediaFileService;
    private SettingsService settingsService;
    private MediaFileDao mediaFileDao;
    private ArtistDao artistDao;
    private LastFmCache cache;

	Map<String, String> storedCorrectionMap = new HashMap<String, String>();
    
    public void init() {
        Caller caller = Caller.getInstance();
        caller.setUserAgent("Madsonic");
        File cacheDir = new File(SettingsService.getMadsonicHome(), "lastfmcache");
        cache = new LastFmCache(cacheDir);
        caller.setCache(cache);
        executor.execute(new ArtistBioLoader());
    }

    
	public String getCorrection(String artistName){
		String stripedArtist = stripNonValidXMLCharacters(artistName);
		String requestedArtist = null;
		if (stripedArtist != null && stripedArtist != ""){
			try {
				String requestedArtistfromMap = storedCorrectionMap.get(stripedArtist)  ;
				if (requestedArtistfromMap == null) {
					requestedArtist = (org.madsonic.lastfm.Artist.getCorrection(stripedArtist, LAST_FM_KEY)).getName();
					if (!stripedArtist.equalsIgnoreCase(requestedArtist)){
						storedCorrectionMap.put(stripedArtist, requestedArtist);
//						LOG.debug("getCorrection store artistName: '" + stripedArtist + "'");
					}
				} else {
					requestedArtist = requestedArtistfromMap;
//					LOG.debug("getCorrection cached artistName: '" + stripedArtist + "'");
				}
			} 
			catch (NullPointerException ex) {
				LOG.error("getCorrection error artistName '" + stripedArtist + "'");
			}
		}
		return requestedArtist;
	}    
    
    /**
     * Returns similar artists, using last.fm REST API.
     *
     * @param mediaFile         The media file (song, album or artist).
     * @param count             Max number of similar artists to return.
     * @param includeNotPresent Whether to include artists that are not present in the media library.
     * @return Similar artists, ordered by presence then similarity.
     */
    public List<MediaFile> getSimilarArtists(MediaFile mediaFile, int count, boolean includeNotPresent) {
        List<MediaFile> result = new ArrayList<MediaFile>();
        if (mediaFile == null) {
            return result;
        }

        String artistName = getArtistName(mediaFile);
        try {
            Collection<Artist> similarArtists = Artist.getSimilar(getCanonicalArtistName(artistName), LAST_FM_KEY);

            if (similarArtists == null) {
                LOG.debug("No similar artists found for " + artistName);
            	return result;
            }
            
            // First select artists that are present.
            for (Artist lastFmArtist : similarArtists) {
                MediaFile similarArtist = mediaFileDao.getArtistByName(lastFmArtist.getName());
                if (similarArtist != null) {
                    result.add(similarArtist);
                    if (result.size() == count) {
                        return result;
                    }
                }
            }

            // Then fill up with non-present artists
            if (includeNotPresent) {
                for (Artist lastFmArtist : similarArtists) {
                    MediaFile similarArtist = mediaFileDao.getArtistByName(lastFmArtist.getName());
                    if (similarArtist == null) {
                        MediaFile notPresentArtist = new MediaFile();
                        notPresentArtist.setId(-1);
                        notPresentArtist.setArtist(lastFmArtist.getName());
                        result.add(notPresentArtist);
                        if (result.size() == count) {
                            return result;
                        }
                    }
                }
            }

        } catch (Throwable x) {
            LOG.warn("Failed to find similar artists for " + artistName);
        }
        return result;
    }

    public List<MediaFile> getTopAlbumArtists(MediaFile mediaFile, int count, boolean includeNotPresent) {
        List<MediaFile> result = new ArrayList<MediaFile>();
        if (mediaFile == null) {
            return result;
        }

        String artistName = getArtistName(mediaFile);
        try {
        	
            Collection<Album> topAlbums = org.madsonic.lastfm.Artist.getTopAlbums(artistName, LAST_FM_KEY, 5);

            // First select albums that are present.
            for (Album lastfmAlbum : topAlbums) {
            	
                MediaFile topAlbum = mediaFileDao.getAlbumByName(lastfmAlbum.getName());
                
                if (topAlbum != null) {
                    result.add(topAlbum);
                    if (result.size() == count) {
                        return result;
                    }
                }
            }

        } catch (Throwable x) {
            LOG.warn("Failed to find similar artists for " + artistName, x);
        }
        return result;
    }    
 
    public List<MediaFile> getTopTrackArtists(MediaFile mediaFile, int count, boolean includeNotPresent) {
        List<MediaFile> result = new ArrayList<MediaFile>();
        if (mediaFile == null) {
            return result;
        }

        String artistName = getArtistName(mediaFile);
		String tmpArtistName = getCorrection(artistName);
		
        try {
    	 	Collection<Track> topTracks = org.madsonic.lastfm.Artist.getTopTracks(tmpArtistName, LAST_FM_KEY, count);
    	 	
            // First select tracks that are present.
            for (Track lastfmTrack : topTracks) {
                MediaFile topTrack = mediaFileDao.getTrackByName(lastfmTrack.getName());
                if (topTrack != null) {
                    result.add(topTrack);
                    if (result.size() == count) {
                        return result;
                    }
                }
            }

        } catch (Throwable x) {
            LOG.warn("Failed to find similar artists for " + artistName, x);
        }
        return result;
    }       
    
    /**
     * Returns similar artists, using last.fm REST API.
     *
     * @param artist            The artist.
     * @param count             Max number of similar artists to return.
     * @param includeNotPresent Whether to include artists that are not present in the media library.
     * @return Similar artists, ordered by presence then similarity.
     */
    public List<org.madsonic.domain.Artist> getSimilarArtists(org.madsonic.domain.Artist artist, int count, boolean includeNotPresent) {
        List<org.madsonic.domain.Artist> result = new ArrayList<org.madsonic.domain.Artist>();

        try {
            // First select artists that are present.
            Collection<Artist> similarArtists = Artist.getSimilar(getCanonicalArtistName(artist.getName()), LAST_FM_KEY);
            for (Artist lastFmArtist : similarArtists) {
            	org.madsonic.domain.Artist similarArtist = artistDao.getArtist(lastFmArtist.getName());
                if (similarArtist != null) {
                    result.add(similarArtist);
                    if (result.size() == count) {
                        return result;
                    }
                }
            }
            // Then fill up with non-present artists
            if (includeNotPresent) {
                for (Artist lastFmArtist : similarArtists) {
                    org.madsonic.domain.Artist similarArtist = artistDao.getArtist(lastFmArtist.getName());
                    if (similarArtist == null) {
                        org.madsonic.domain.Artist notPresentArtist = new org.madsonic.domain.Artist();
                        notPresentArtist.setId(-1);
                        notPresentArtist.setName(lastFmArtist.getName());
                        result.add(notPresentArtist);
                        if (result.size() == count) {
                            return result;
                        }
                    }
                }
            }

        } catch (Throwable x) {
            LOG.warn("Failed to find similar artists for " + artist.getName(), x);
        }
        return result;
    }

    /**
     * Returns songs from similar artists, using last.fm REST API. Typically used for artist radio features.
     *
     * @param artist The artist.
     * @param count  Max number of songs to return.
     * @return Songs from similar artists;
     */
    public List<MediaFile> getSimilarSongs(org.madsonic.domain.Artist artist, int count) throws IOException {
        List<MediaFile> similarSongs = new ArrayList<MediaFile>();

        similarSongs.addAll(mediaFileDao.getSongsByArtist(artist.getName(), 0, 1000));
        for (org.madsonic.domain.Artist similarArtist : getSimilarArtists(artist, 100, false)) {
            similarSongs.addAll(mediaFileDao.getSongsByArtist(similarArtist.getName(), 0, 1000));
        }
        Collections.shuffle(similarSongs);
        return similarSongs.subList(0, Math.min(count, similarSongs.size()));
    }

    /**
     * Returns songs from similar artists, using last.fm REST API. Typically used for artist radio features.
     *
     * @param mediaFile The media file (song, album or artist).
     * @param count     Max number of songs to return.
     * @return Songs from similar artists;
     */
    public List<MediaFile> getSimilarSongs(MediaFile mediaFile, int count) throws IOException {
        List<MediaFile> similarSongs = new ArrayList<MediaFile>();

        String artistName = getArtistName(mediaFile);
        MediaFile artist = mediaFileDao.getArtistByName(artistName);
        if (artist != null) {
            similarSongs.addAll(mediaFileService.getRandomSongsForParent(artist, count));
        }

        for (MediaFile similarArtist : getSimilarArtists(mediaFile, 100, false)) {
            similarSongs.addAll(mediaFileService.getRandomSongsForParent(similarArtist, count));
        }
        Collections.shuffle(similarSongs);
        return similarSongs.subList(0, Math.min(count, similarSongs.size()));
    }

    /**
     * Returns album notes and images.
     *
     * @param mediaFile The media file (song or album).
     * @return Album notes.
     */
    public AlbumNotes getAlbumNotes(MediaFile mediaFile) {
        return getAlbumNotes(getCanonicalArtistName(getArtistName(mediaFile)), mediaFile.getAlbumName());
    }

    /**
     * Returns album notes and images.
     *
     * @param album The album.
     * @return Album notes.
     */
    public AlbumNotes getAlbumNotes(org.madsonic.domain.Album album) {
        return getAlbumNotes(getCanonicalArtistName(album.getArtist()), album.getName());
    }

    /**
     * Returns album notes and images.
     *
     * @param artist The artist name.
     * @param album The album name.
     * @return Album notes.
     */
    private AlbumNotes getAlbumNotes(String artist, String album) {
        if (artist == null || album == null || artist.equalsIgnoreCase(album)) {
            return null;
        }
        try {
    	 	Locale lastFMLocale = new Locale(getLastFMLanguage());
    	 	
            Album info = Album.getInfo(artist, album, lastFMLocale, null, LAST_FM_KEY);
            if (info == null) {
                return null;
            }
            return new AlbumNotes(processWikiText(info.getWikiSummary(),false),
                                 info.getMbid(),
                                 info.getUrl(),
                                 info.getImageURL(ImageSize.MEDIUM),
                                 info.getImageURL(ImageSize.LARGE),
                                 info.getImageURL(ImageSize.MEGA));
        } catch (Throwable x) {
            LOG.warn("Failed to find album notes for " + artist + " - " + album, x);
            return null;
        }
    }

    public List<LastFMCoverArt> searchCoverArt(String artist, String album) {
        if (artist == null && album == null) {
            return Collections.emptyList();
        }
        try {
            StringBuilder query = new StringBuilder();
            if (artist != null) {
                query.append(artist).append(" ");
            }
            if (album != null) {
                query.append(album);
            }

            Collection<Album> matches = Album.search(query.toString(), LAST_FM_KEY);
            return FluentIterable.from(matches)
                                 .transform(new Function<Album, LastFMCoverArt>() {
                                     @Override
                                     public LastFMCoverArt apply(Album album) {
                                         return convert(album);
                                     }
                                 })
                                 .filter(Predicates.notNull())
                                 .toList();
        } catch (Throwable x) {
            LOG.warn("Failed to search for cover art for " + artist + " - " + album, x);
            return Collections.emptyList();
        }
    }

    private LastFMCoverArt convert(Album album) {
        String imageUrl = null;
        for (ImageSize imageSize : Lists.reverse(Arrays.asList(ImageSize.values()))) {
            imageUrl = StringUtils.trimToNull(album.getImageURL(imageSize));
            if (imageUrl != null) {
                break;
            }
        }

        return imageUrl == null ? null : new LastFMCoverArt(imageUrl, album.getArtist(), album.getName());
    }

    /**
     * Returns artist bio and images.
     *
     * @param mediaFile The media file (song, album or artist).
     * @return Artist bio.
     */
    public ArtistBio getArtistBio(MediaFile mediaFile, boolean raw ) {
        return getArtistBio(getCanonicalArtistName(getArtistName(mediaFile)), raw);
    }

    public ArtistBio getArtistBio(MediaFile mediaFile) {
        return getArtistBio(getCanonicalArtistName(getArtistName(mediaFile)), false);
    }    
    
    /**
     * Returns artist bio and images.
     *
     * @param artist The artist.
     * @return Artist bio.
     */
    public ArtistBio getArtistBio(org.madsonic.domain.Artist artist, boolean raw) {
        return getArtistBio(getCanonicalArtistName(artist.getName()), raw);
    } 
       
    /**
     * Returns whether the bio for the given artist is cached.
     */
    public boolean isArtistBioCached(String artist) {
        Map<String, String> params = new TreeMap<String, String>();
        params.put("artist", artist);

        String key = Cache.createCacheEntryName("artist.getInfo", params);
        return cache.isCached(key);
    }

    /**
     * Returns top songs for the given artist, using last.fm REST API.
     *
     * @param artist       The artist.
     * @param count        Max number of songs to return.
     * @param musicFolders Only return songs present in these folders.
     * @return Top songs for artist.
     */
    public List<MediaFile> getTopSongs(MediaFile artist, int count, List<MusicFolder> musicFolders) {
        return getTopSongs(artist.getName(), count, musicFolders);
    }

    /**
     * Returns top songs for the given artist, using last.fm REST API.
     *
     * @param artistName   The artist name.
     * @param count        Max number of songs to return.
     * @param musicFolders Only return songs present in these folders.
     * @return Top songs for artist.
     */
    public List<MediaFile> getTopSongs(String artistName, int count, List<MusicFolder> musicFolders) {
        try {
            if (StringUtils.isBlank(artistName) || count <= 0) {
                return Collections.emptyList();
            }

            List<MediaFile> result = new ArrayList<MediaFile>();
            for (Track topTrack : Artist.getTopTracks(artistName, LAST_FM_KEY, 30)) {
                MediaFile song = mediaFileDao.getSongByArtistAndTitle(artistName, topTrack.getName(), musicFolders);
                if (song != null) {
                    result.add(song);
                    if (result.size() == count) {
                        return result;
                    }
                }
            }
            return result;
        } catch (Throwable x) {
            LOG.warn("Failed to find top songs for " + artistName, x);
            return Collections.emptyList();
        }
    }

    private ArtistBio getArtistBio(String artistName, boolean raw) {
        try {
            if (artistName == null) {
                return null;
            }
			String tmpArtistName = getCorrection(artistName);
    	 	Locale lastFMLocale = new Locale(getLastFMLanguage()) ;
    	 	
            Artist info = Artist.getInfo(tmpArtistName, lastFMLocale, null, LAST_FM_KEY);
            if (info == null) {
                return null;
            }
            String wikiText = processWikiText(info.getWikiSummary(), raw);
            String shortWikiText = "";
            
            if (wikiText != null) {
                shortWikiText = shortenWikiText(wikiText);
            }
            
            return new ArtistBio(shortWikiText,
            					 wikiText,
                                 info.getMbid(),
                                 info.getUrl(),
                                 info.getImageURL(ImageSize.MEDIUM),
                                 info.getImageURL(ImageSize.LARGE),
                                 info.getImageURL(ImageSize.MEGA));
        } catch (Throwable x) {
            LOG.warn("Failed to find artist bio for " + artistName, x);
            return null;
        }
    }
    
    private String getCanonicalArtistName(String artistName) {
        try {
            if (artistName == null) {
                return null;
            }
			String tmpArtistName = getCorrection(artistName);
    	 	Locale lastFMLocale = new Locale(getLastFMLanguage()) ;
            Artist info = Artist.getInfo(tmpArtistName, lastFMLocale, null, LAST_FM_KEY);
            if (info == null) {
                return null;
            }
            String biography = processWikiText(info.getWikiSummary(), true);
            String redirectedArtistName = getRedirectedArtist(biography);
            return redirectedArtistName != null ? redirectedArtistName : artistName;
        } catch (Throwable x) {
            LOG.warn("Failed to find artist bio for " + artistName, x);
            return null;
        }
    }

    private String shortenWikiText (String text) {
		return text.substring(0, text.indexOf(". ") + 1);
    }

    private String getRedirectedArtist(String biography) {
        /*
         This is mistagged for <a target='_blank' href="http://www.last.fm/music/The+Boomtown+Rats" class="bbcode_artist">The Boomtown Rats</a>;
         it would help Last.fm if you could correct your tags.
         <a target='_blank' href="http://www.last.fm/music/+noredirect/Boomtown+Rats">Boomtown Rats on Last.fm</a>.

        -- or --

         Fix your tags to <a target='_blank' href="http://www.last.fm/music/The+Chemical+Brothers" class="bbcode_artist">The Chemical Brothers</a>
         <a target='_blank' href="http://www.last.fm/music/+noredirect/Chemical+Brothers">Chemical Brothers on Last.fm</a>.
        */

        if (biography == null) {
            return null;
        }
        Pattern pattern = Pattern.compile("((This is mistagged for)|(Fix your tags to)).*class=\"bbcode_artist\">(.*?)</a>");
        Matcher matcher = pattern.matcher(biography);
        if (matcher.find()) {
            return matcher.group(4);
        }
        return null;
    }

    private String processWikiText(String text, boolean raw) {
        if (text == null) {
            return null;
        }

        /*
         System of a Down is an Armenian American <a href="http://www.last.fm/tag/alternative%20metal" class="bbcode_tag" rel="tag">alternative metal</a> band,
         formed in 1994 in Los Angeles, California, USA. All four members are of Armenian descent, and are widely known for their outspoken views expressed in
         many of their songs confronting the Armenian Genocide of 1915 by the Ottoman Empire and the ongoing War on Terror by the US government. The band
         consists of <a href="http://www.last.fm/music/Serj+Tankian" class="bbcode_artist">Serj Tankian</a> (vocals), Daron Malakian (vocals, guitar),
         Shavo Odadjian (bass, vocals) and John Dolmayan (drums).
         <a href="http://www.last.fm/music/System+of+a+Down">Read more about System of a Down on Last.fm</a>.
         User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.
         */

    	text = text.replaceAll("Warning! Deleting this.*", "");
        text = text.replaceAll("User-contributed text.*", "");
    	text = text.replaceAll("Read more about Various Artists on Last.fm.*", "");
    	
    	if (!raw) {
            text = text.replaceAll("<a ", "<a target='_blank' ");        
            text = text.replaceAll("<a target='_blank' href=\"https://www.last.fm/music/", "<a target='main' href=\"search.jsp?&q=");
            text = text.replaceAll("<a target='_blank' href=\"http://www.last.fm/music/", "<a target='main' href=\"search.jsp?&q=");
            text = text.replaceAll("<a target='_blank' href=\"https://www.last.fm/tag/", "<a target='main' href=\"search.jsp?&q=");        
            text = text.replaceAll("<a target='_blank' href=\"http://www.last.fm/tag/", "<a target='main' href=\"search.jsp?&q=");        
        	
            text = text.replaceAll("on Last.fm", "on Madsonic");
        	text = text.replaceAll("Read more about", "<br>Search more about");
        	text = text.replaceAll("Read more on", "<br>Search more on");
    	}
        
        text = text.replace("\n", " ");
        
        text = StringUtils.trimToNull(text);

        if (text != null && text.startsWith("This is an incorrect tag")) {
            return null;
        }

        return text;
    }

    private String getArtistName(MediaFile mediaFile) {
        String artistName = mediaFile.getName();
        if (mediaFile.isAlbum() || mediaFile.isFile()) {
            artistName = mediaFile.getAlbumArtist() != null ? mediaFile.getAlbumArtist() : mediaFile.getArtist();
        }
        return artistName;
    }

    private String stripNonValidXMLCharacters(String in) {
		StringBuilder out = new StringBuilder(); // Used to hold the output.
		char current; // Used to reference the current character.

		if (in == null || ("".equals(in))) return ""; // vacancy test.
		for (int i = 0; i < in.length(); i++) {
			current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
			if ((current == 0x9) ||
					(current == 0xA) ||
					(current == 0xB) ||                
					(current == 0xD) ||
					(current == 0x1f) ||                
					((current >= 0x20) && (current <= 0xD7FF)) ||
					((current >= 0xE000) && (current <= 0xFFFD)) ||
					((current >= 0x10000) && (current <= 0x10FFFF)))
				out.append(current);
		}
		return out.toString();
	}    
    
	public void setLastFMLanguage(String lastFMLocal) {
		settingsService.setLastFMLanguage(lastFMLocal);
		settingsService.save();
	}

	public String getLastFMLanguage() {
		String local = settingsService.getLastFMLanguage();
		if ("AUTO".equalsIgnoreCase(local)) {
			Locale LastFMLocale = new Locale(settingsService.getLocale().toString()) ; 	
			return LastFMLocale.toString();
		}
		return local;
	}    
    
    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }    
    
    public void setMediaFileDao(MediaFileDao mediaFileDao) {
        this.mediaFileDao = mediaFileDao;
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }

    public void setArtistDao(ArtistDao artistDao) {
        this.artistDao = artistDao;
    }
    
    private class ArtistBioLoader implements Runnable {

        private static final long ARTIST_BIO_UPDATE_INTERVAL = 3 * 30 * 24 * 3600 * 1000L; // 3 months

        @Override
        public void run() {
        	
        	if (settingsService.getLastFMArtistBioScan()) {
        	
	            long lastUpdated = settingsService.getArtistBioLastUpdated();
	            if (System.currentTimeMillis() - lastUpdated > ARTIST_BIO_UPDATE_INTERVAL) {
	                for (String artist : mediaFileDao.getArtistNames()) {
	                    if (!isArtistBioCached(artist)) {
	                        try {
	                            Thread.sleep(5000L);
	                            getArtistBio(artist, true);
	                            LOG.debug("Fetched artist bio for " + artist);
	                        } catch (Exception x) {
	                            LOG.warn("Failed to get artist bio for " + artist, x);
	                        }
	                    }
	                }
	                settingsService.setArtistBioLastUpdated(System.currentTimeMillis());
	                settingsService.save(false);
	            }
        	}
        }
    }
}
