/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import org.madsonic.Logger;
import org.madsonic.dao.MediaFileDao;
import org.madsonic.dao.PlaylistDao;
import org.madsonic.domain.MediaFile;
import org.madsonic.domain.MusicFolder;
import org.madsonic.domain.Playlist;
import org.madsonic.domain.PlaylistResult;
import org.madsonic.domain.URLParamDecode;
import org.madsonic.domain.URLParamEncoder;
import org.madsonic.domain.User;
import org.madsonic.util.OperatingSystem;
import org.madsonic.util.Pair;
import org.madsonic.util.StringUtil;

/**
 * Provides services for loading and saving playlists to and from persistent storage.
 *
 * @author Sindre Mehus, Martin Karel
 * @see org.madsonic.domain.PlayQueue
 */
public class PlaylistService {

    private static final Logger LOG = Logger.getLogger(PlaylistService.class);
    private MediaFileService mediaFileService;
    private MediaFileDao mediaFileDao;
    private PlaylistDao playlistDao;
    private SecurityService securityService;
    private SettingsService settingsService;

    
    public List<Playlist> getAllPlaylists() {
        return sort(playlistDao.getAllPlaylists()); 
    }

    private List<Playlist> sort(List<Playlist> playlists) {
        Collections.sort(playlists, new PlaylistComparator());
        return playlists;
    }
    
    public List<Playlist> getAllPlaylists(String sortCriteria) {
        return playlistDao.getAllPlaylists(sortCriteria);
    }

    public List<Playlist> getReadablePlaylistsForUser(String username, String sortCriteria, String direction) {
    	
    	if (direction == null || direction == "") {
    		direction = "asc";
    	}
    	if (sortCriteria == null || sortCriteria == "") {
    		sortCriteria = direction.contains("asc") ? "created desc" : "created asc";
    	}
    	if (sortCriteria.contains("name")) {
    		sortCriteria = direction.contains("asc") ? "name asc" : "name desc";
    	}
    	if (sortCriteria.contains("created")) {
    		sortCriteria = direction.contains("asc") ? "created asc" : "created desc";    		
    	}
    	if (sortCriteria.contains("changed")) {
    		sortCriteria = direction.contains("asc") ? "changed asc" : "changed desc";        		
    	}
    	if (sortCriteria.contains("files")) {
			sortCriteria = direction.contains("asc") ? "file_count asc" : "file_count desc";         		
    	}
    	if (sortCriteria.contains("length")) {
    		sortCriteria = direction.contains("asc") ? "duration_seconds asc" : "duration_seconds desc";               		
    	}
    	
        // Admin users are allowed to modify all playlists that are visible to them.
        if (securityService.isAdmin(username)) {
            return getAllPlaylists(sortCriteria);
        }
    	
        return playlistDao.getReadablePlaylistsForUser(username, sortCriteria);
    }

    public List<Playlist> getWritablePlaylistsForUser(String username, String sortCriteria, String direction) {

    	if (direction == null || direction == "") {
    		direction = "asc";
    	}    	
    	if (sortCriteria == null || sortCriteria == "") {
    		sortCriteria = direction.contains("asc") ? "created desc" : "created asc";
    	}
    	if (sortCriteria.contains("name")) {
    		sortCriteria = direction.contains("asc") ? "name asc" : "name desc";
    	}
    	if (sortCriteria.contains("created")) {
    		sortCriteria = direction.contains("asc") ? "created asc" : "created desc";    		
    	}
    	if (sortCriteria.contains("changed")) {
    		sortCriteria = direction.contains("asc") ? "changed asc" : "changed desc";        		
    	}
    	if (sortCriteria.contains("files")) {
			sortCriteria = direction.contains("asc") ? "file_count asc" : "file_count desc";         		
    	}
    	if (sortCriteria.contains("length")) {
    		sortCriteria = direction.contains("asc") ? "duration_seconds asc" : "duration_seconds desc";         		
    	}
    	
        // Admin users are allowed to modify all playlists that are visible to them.
        if (securityService.isAdmin(username)) {
            return getReadablePlaylistsForUser(username, sortCriteria, direction);
        }

        return playlistDao.getWritablePlaylistsForUser(username, sortCriteria);
    }

    public Playlist getPlaylist(int id) {
        return playlistDao.getPlaylist(id);
    }

    public List<String> getPlaylistUsers(int playlistId) {
        return playlistDao.getPlaylistUsers(playlistId);
    }

    public List<MediaFile> getFilesInPlaylist(int id) {
        return getFilesInPlaylist(id, true);
    }

    public List<MediaFile> getFilesInPlaylist(int id, boolean includeNotPresent) {
        List<MediaFile> files = mediaFileDao.getFilesInPlaylist(id);
        if (includeNotPresent) {
            return files;
        }
        List<MediaFile> presentFiles = new ArrayList<MediaFile>(files.size());
        for (MediaFile file : files) {
            if (file.isPresent()) {
                presentFiles.add(file);
            }
        }
        return presentFiles;
    }

	public List<MediaFile> getOfflineFilesInPlaylist(int id) {
        return mediaFileDao.getOfflineFilesInPlaylist(id);
    }    
    
    public void setFilesInPlaylist(int id, List<MediaFile> files) {
        playlistDao.setFilesInPlaylist(id, files);
    }
        
    public void setAllFilesInPlaylist(int id, List<MediaFile> onlineFiles, List<MediaFile> offlineFiles) {
        playlistDao.setAllFilesInPlaylist(id, onlineFiles, offlineFiles);
    }
    
    public void createPlaylist(Playlist playlist) {
        playlistDao.createPlaylist(playlist);
    }
 
    public void addPlaylistUser(int playlistId, String username) {
        playlistDao.addPlaylistUser(playlistId, username);
    }

    public void deletePlaylistUser(int playlistId, String username) {
        playlistDao.deletePlaylistUser(playlistId, username);
    }

    public boolean isReadAllowed(Playlist playlist, String username) {
        if (username == null) {
            return false;
        }
        // Admin users are allowed to read all playlists.
        if (securityService.isAdmin(username) || username.equals(playlist.getUsername()) || playlist.isPublic()) {
            return true;
        }
        return playlistDao.getPlaylistUsers(playlist.getId()).contains(username);
    }

    public boolean isWriteAllowed(Playlist playlist, String username) {
        // Admin users are allowed to write to all playlists.
        if (securityService.isAdmin(username)) {
            return true;
        }
        return username != null && username.equals(playlist.getUsername());
    }

    public void deletePlaylist(int id) {
        playlistDao.deletePlaylist(id);
    }

    public void updatePlaylist(Playlist playlist) {
        playlistDao.updatePlaylist(playlist);
    }

    public Playlist importPlaylist(String username, String playlistName, String fileName, String format, InputStream inputStream, Playlist existingPlaylist) throws Exception {
        PlaylistFormat playlistFormat = getPlaylistFormat(format);
        if (playlistFormat == null) {
            throw new Exception("Unsupported playlist format: " + format);
        }
        
        PlaylistResult playlistResult = parseFiles(IOUtils.toByteArray(inputStream), playlistFormat);
        boolean isPublic = playlistResult.isPublic();
        String owner = playlistResult.getUsername();
        LOG.debug("User '" + owner + "' found in Playlist.");
        if (owner != null && securityService.getUserByName(owner) != null) {
        	 username = owner;
        } else {
           LOG.warn("Fallback user '" + username + "' set in Playlist.");        	
        };        

        Pair<List<MediaFile>, List<String>> result = playlistResult.getMediaFiles();
        if (result.getFirst().isEmpty() && !result.getSecond().isEmpty()) {
            throw new Exception("No songs in the playlist were found.");
        }
        for (String error : result.getSecond()) {
            LOG.warn("File in playlist '" + fileName + "' not found: " + error);
        }

        Date now = new Date();
        Playlist playlist;
        if (existingPlaylist == null) {
            playlist = new Playlist();
            playlist.setUsername(username);
            playlist.setCreated(now);
            playlist.setChanged(now);
            playlist.setPublic(isPublic);
            playlist.setShareLevel(isPublic ? 1 : 0);
            playlist.setName(playlistName);
            playlist.setComment("Auto-imported from " + fileName);
            playlist.setImportedFrom(fileName);
            createPlaylist(playlist);
        } else {
            playlist = existingPlaylist;
        }

        setFilesInPlaylist(playlist.getId(), result.getFirst());

        return playlist;
    }

    private PlaylistResult parseFiles(byte[] playlist, PlaylistFormat playlistFormat) throws Exception {
    	PlaylistResult playlistResult = null;
        Pair<List<MediaFile>, List<String>> result = null;

        // Try with multiple encodings; use the one that finds the most files.
        String[] encodings = {StringUtil.ENCODING_LATIN, StringUtil.ENCODING_UTF8, Charset.defaultCharset().name()};
        for (String encoding : encodings) {
        	playlistResult = parseFilesWithEncoding(playlist, playlistFormat, encoding);
            Pair<List<MediaFile>, List<String>> files = playlistResult.getMediaFiles();
            if (result == null || result.getFirst().size() < files.getFirst().size()) {
                result = files;
                playlistResult.setMediaFiles(result);
            }
        }
        return playlistResult;
    }

    private PlaylistResult parseFilesWithEncoding(byte[] playlist, PlaylistFormat playlistFormat, String encoding) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(playlist), encoding));
        return playlistFormat.parse(reader, mediaFileService);
    }

    public void exportPlaylist(int id, OutputStream out) throws Exception {
        String playlistExportMode = settingsService.getPlaylistExportMode();
        Playlist playlist = playlistDao.getPlaylist(id);
        
		switch (playlistExportMode) {
		case "M3U8":
			PrintWriter outm3uPlaylist = new PrintWriter(new OutputStreamWriter(out, StringUtil.ENCODING_UTF8));
			new M3UFormat().format(getFilesInPlaylist(playlist.getId()), outm3uPlaylist, playlist.getUsername(), playlist.isPublic());
	        outm3uPlaylist.close();
	        break;
		case "XSPF":
			PrintWriter outXspfPlaylist = new PrintWriter(new OutputStreamWriter(out, StringUtil.ENCODING_UTF8));
	        new XSPFFormat().format(getFilesInPlaylist(playlist.getId()), outXspfPlaylist, playlist.getUsername(), playlist.isPublic());
	        outXspfPlaylist.close();
	        break;
		case "WPL":
			PrintWriter wplPlsPlaylist = new PrintWriter(new OutputStreamWriter(out, StringUtil.ENCODING_UTF8));
	        new WPLFormat().format(getFilesInPlaylist(playlist.getId()), wplPlsPlaylist, playlist.getUsername(), playlist.isPublic()); 
	        wplPlsPlaylist.close(); 
	        break;	        
		case "PLS":
			PrintWriter outPlsPlaylist = new PrintWriter(new OutputStreamWriter(out, StringUtil.ENCODING_UTF8));
	        new PLSFormat().format(getFilesInPlaylist(playlist.getId()), outPlsPlaylist, playlist.getUsername(), playlist.isPublic()); 
	        outPlsPlaylist.close(); 
	        break;	        
		}        
    }
    
    public void backupAllPlaylists() throws Exception {

    	String playlistBackupFolderPath = settingsService.getPlaylistBackupFolder();

    	for (Playlist playlist : playlistDao.getAllPlaylists()) {
    		if (playlist.getFileCount() > 0 && getFilesInPlaylist(playlist.getId()).size() > 0) {
    			String playlistExportMode = settingsService.getPlaylistExportMode();
    			switch (playlistExportMode) {
    				case "M3U8":
    	    			FileWriter fstream_m3u8 = new FileWriter(playlistBackupFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".m3u8");
    	    			PrintWriter outm3uPlaylist = new PrintWriter(fstream_m3u8);
    	    			new M3UFormat().format(getFilesInPlaylist(playlist.getId()), outm3uPlaylist, playlist.getUsername(), playlist.isPublic());
    	    	        outm3uPlaylist.close();
    	    	        break;
    				case "XSPF":
    	    			FileWriter fstream_xspf = new FileWriter(playlistBackupFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".xspf");
    	    			PrintWriter outXspfPlaylist = new PrintWriter(fstream_xspf);  
    	    	        new XSPFFormat().format(getFilesInPlaylist(playlist.getId()), outXspfPlaylist, playlist.getUsername(), playlist.isPublic()); 
    	    	        outXspfPlaylist.close();
    	    	        break;
    				case "WPL":
    	    			FileWriter fstream_wpl = new FileWriter(playlistBackupFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".wpl");
    	    			PrintWriter wplPlsPlaylist = new PrintWriter(fstream_wpl);  
    	    	        new WPLFormat().format(getFilesInPlaylist(playlist.getId()), wplPlsPlaylist, playlist.getUsername(), playlist.isPublic()); 
    	    	        wplPlsPlaylist.close(); 
    	    	        break;
    				case "PLS":
    	    			FileWriter fstream_pls = new FileWriter(playlistBackupFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".pls");
    	    			PrintWriter outPlsPlaylist = new PrintWriter(fstream_pls);  
    	    	        new PLSFormat().format(getFilesInPlaylist(playlist.getId()), outPlsPlaylist, playlist.getUsername(), playlist.isPublic()); 
    	    	        outPlsPlaylist.close();
    	    	        break;
    			}
    			LOG.info("Backup: '" + playlist.getName() + "' successfully, entry size: " + getFilesInPlaylist(playlist.getId()).size());
    		} else {
    			LOG.warn("Backup: '" + playlist.getName() + "' playlist skiped, entry size: " + getFilesInPlaylist(playlist.getId()).size());
    		}
    	}
    }
    
    public void exportAllPlaylists() throws Exception {
    	
    String playlistExportFolderPath = settingsService.getPlaylistExportFolder();
        
    for (Playlist playlist : playlistDao.getAllPlaylists()) {
    	
		String playlistExportMode = settingsService.getPlaylistExportMode();
		switch (playlistExportMode) {
			case "M3U8":
				FileWriter fstream_m3u8 = new FileWriter(playlistExportFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".m3u8");
				PrintWriter outm3uPlaylist = new PrintWriter(fstream_m3u8);
				new M3UFormat().format(getFilesInPlaylist(playlist.getId()), outm3uPlaylist, playlist.getUsername(), playlist.isPublic());
		        outm3uPlaylist.close();
		        break;
			case "XSPF":
				FileWriter fstream_xspf = new FileWriter(playlistExportFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".xspf");
				PrintWriter outXspfPlaylist = new PrintWriter(fstream_xspf);  
		        new XSPFFormat().format(getFilesInPlaylist(playlist.getId()), outXspfPlaylist, playlist.getUsername(), playlist.isPublic()); 
		        outXspfPlaylist.close();
		        break;
			case "WPL":
				FileWriter fstream_wpl = new FileWriter(playlistExportFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".wpl");
				PrintWriter outWplPlaylist = new PrintWriter(fstream_wpl);  
		        new WPLFormat().format(getFilesInPlaylist(playlist.getId()), outWplPlaylist, playlist.getUsername(), playlist.isPublic()); 
		        outWplPlaylist.close();
		        break;		        
			case "PLS":
				FileWriter fstream_pls = new FileWriter(playlistExportFolderPath + "/" + StringUtil.fileSystemSafe(playlist.getName()) + ".pls");
				PrintWriter outPlsPlaylist = new PrintWriter(fstream_pls);  
		        new PLSFormat().format(getFilesInPlaylist(playlist.getId()), outPlsPlaylist, playlist.getUsername(), playlist.isPublic()); 
		        outPlsPlaylist.close();  
		        break;		        
		}
		
	  }    
        LOG.info("Completed export of " + playlistDao.getAllPlaylists().size() + " playlists to folder " + playlistExportFolderPath);
    }
    
    public void recoveryPlaylists() {
        try {
            LOG.info("Starting playlist recovery.");
            doImportPlaylists(settingsService.getPlaylistBackupFolder());
            
            LOG.info("Completed playlist recovery.");
            
        } catch (Throwable x) {
            LOG.warn("Failed to recover playlists: " + x, x);
        }
    }
    
    public void importPlaylists() {
        try {
            LOG.info("Starting playlist import.");
            doImportPlaylists(settingsService.getPlaylistImportFolder());
            
            LOG.info("Completed playlist import.");
            
        } catch (Throwable x) {
            LOG.warn("Failed to import playlists: " + x, x);
        }
    }

    public void cleanupPlaylistsBackupFolder() {
        try {
            LOG.info("Starting playlist cleanup.");
            doCleanupPlaylistsFolder(settingsService.getPlaylistBackupFolder());
            
            LOG.info("Completed playlist cleanup.");
            
        } catch (Throwable x) {
            LOG.warn("Failed to cleanup playlists: " + x, x);
        }
    }    
    
    private void doCleanupPlaylistsFolder(String path) throws Exception {
        String playlistFolderPath = path;
        if (playlistFolderPath == null) {
            return;
        }
        File playlistFolder = new File(playlistFolderPath);
        if (!playlistFolder.exists()) {
            return;
        }

        for (File file : playlistFolder.listFiles()) {
            try {
                file.delete();
            } catch (Exception x) {
                LOG.warn("Failed to delete playlist " + file + ". " + x.getMessage());
            }
        }
    }    
    
    private void doImportPlaylists(String path) throws Exception {
        String playlistFolderPath = path;
        if (playlistFolderPath == null) {
            return;
        }
        File playlistFolder = new File(playlistFolderPath);
        if (!playlistFolder.exists()) {
            return;
        }

        List<Playlist> allPlaylists = playlistDao.getAllPlaylists();
        for (File file : playlistFolder.listFiles()) {
            try {
                importPlaylistIfUpdated(file, allPlaylists);
            } catch (Exception x) {
                LOG.warn("Failed to auto-import playlist " + file + ". " + x.getMessage());
            }
        }
    }

    public void updatePlaylistStatistics() {
        try {
            LOG.info("Starting playlist statistics update.");
            doUpdatePlaylistStatistics();
            LOG.info("Completed playlist statistics update.");
        } catch (Throwable x) {
            LOG.warn("Failed to update playlist statistics: " + x, x);
        }
    }
    private void doUpdatePlaylistStatistics() {
        for (Playlist playlist : playlistDao.getAllPlaylists()) {
            List<MediaFile> onlineFiles = getFilesInPlaylist(playlist.getId());
            List<MediaFile> offlineFiles = getOfflineFilesInPlaylist(playlist.getId());
            setAllFilesInPlaylist(playlist.getId(), onlineFiles, offlineFiles);
        }
    }

    private void importPlaylistIfUpdated(File file, List<Playlist> allPlaylists) throws Exception {
        String format = FilenameUtils.getExtension(file.getPath());
        if (getPlaylistFormat(format) == null) {
            return;
        }

        String fileName = file.getName();
        Playlist existingPlaylist = null;
        for (Playlist playlist : allPlaylists) {
            if (fileName.equalsIgnoreCase(playlist.getImportedFrom())) {
                existingPlaylist = playlist;
                if (file.lastModified() <= playlist.getChanged().getTime()) {
                	LOG.info("Playlist " + playlist.getName() + " already imported and not changed since.");
                    return;
                }
            }
        }
        InputStream in = new FileInputStream(file);
        try {
            importPlaylist(User.USERNAME_ADMIN, FilenameUtils.getBaseName(fileName), fileName, format, in, existingPlaylist);
            LOG.info("Auto-imported playlist " + file);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    private PlaylistFormat getPlaylistFormat(String format) {
        if (format == null) {
            return null;
        }
        if (format.equalsIgnoreCase("m3u") || format.equalsIgnoreCase("m3u8")) {
            return new M3UFormat();
        }
        if (format.equalsIgnoreCase("pls")) {
            return new PLSFormat();
        }
        if (format.equalsIgnoreCase("wpl")) {
            return new WPLFormat();
        }
        if (format.equalsIgnoreCase("xspf")) {
            return new XSPFFormat();
        }
        if (format.equalsIgnoreCase("xml")) {
            return new ItunesFormat();
        }         
        
        return null;
    }

    public void setPlaylistDao(PlaylistDao playlistDao) {
        this.playlistDao = playlistDao;
    }

    public void setMediaFileDao(MediaFileDao mediaFileDao) {
        this.mediaFileDao = mediaFileDao;
    }

    public void setMediaFileService(MediaFileService mediaFileService) {
        this.mediaFileService = mediaFileService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }
    /**
     * Abstract superclass for playlist formats.
     */
    private abstract class PlaylistFormat {
    	
        public abstract PlaylistResult parse(BufferedReader reader, MediaFileService mediaFileService) throws Exception;

		public abstract void format(List<MediaFile> files, PrintWriter writer, String username, boolean isPublic) throws Exception;

        protected MediaFile getMediaFile(String path) {
            try {
                File file = new File(path);
                if (!file.exists()) {
                    return null;
                }

                file = normalizePath(file);
                if (file == null) {
                    return null;
                }
                
                MediaFile mediaFile = mediaFileService.getMediaFile(file);
                if (mediaFile != null && mediaFile.exists()) {
                    return mediaFile;
                }
                
//TODO:search
//                String artist = "x";
//                String title = "x";
//                
//                MediaFile searchFile = mediaFileService.getMediaFile(artist, title);
//                if (searchFile != null && searchFile.exists()) {
//                    return searchFile;
//                }
                
            } catch (SecurityException x) {
                // Ignored
            } catch (IOException x) {
                // Ignored
            }
            return null;
        }

        /**
         * Paths in an external playlist may not have the same upper/lower case as in the (case sensitive) media_file table.
         * This methods attempts to normalize the external path to match the one stored in the table.
         */
        private File normalizePath(File file) throws IOException {

            // Only relevant for Windows where paths are case insensitive.
            if (!OperatingSystem.isWindows()) {
                return file;
            }

            // Find the most specific music folder.
            String canonicalPath = file.getCanonicalPath();
            MusicFolder containingMusicFolder = null;
            for (MusicFolder musicFolder : settingsService.getAllMusicFolders()) {
                String musicFolderPath = musicFolder.getPath().getPath();
                if (canonicalPath.toLowerCase().startsWith(musicFolderPath.toLowerCase())) {
                    if (containingMusicFolder == null || containingMusicFolder.getPath().length() < musicFolderPath.length()) {
                        containingMusicFolder = musicFolder;
                    }
                }
            }
           if (containingMusicFolder == null) {
                return null;
            }

            return new File(containingMusicFolder.getPath().getPath() + canonicalPath.substring(containingMusicFolder.getPath().getPath().length()));
            // TODO: Consider slashes.
        }
    }

    private class M3UFormat extends PlaylistFormat {
    	
        public PlaylistResult parse(BufferedReader reader, MediaFileService mediaFileService) throws IOException {
            List<MediaFile> ok = new ArrayList<MediaFile>();
            List<String> error = new ArrayList<String>();
            PlaylistResult result = new PlaylistResult();
            
            String line = reader.readLine();
            while (line != null) {
                String artist= "unknown";
                String title = "unknown";
                if (line.startsWith("#EXTINF:")) {
                	String[] parser = line.substring(line.indexOf(",")+1, line.length()).split("-");
                	artist = parser[0] == null ? "" : parser[0];
                	title = parser[1] == null ? "" : parser[1];
                    line = reader.readLine();
                }
                
                if (line.startsWith("#EXTUSR:")) {
                	result.setUsername(line.substring(8));
                }                 
                
                if (line.startsWith("#EXTSEC:")) {
                	result.setPublic(line.substring(8).contains("public") ? true : false);
                }                
                
                if (!line.startsWith("#")) {
                    MediaFile file = getMediaFile(line);
                    if (file != null) {
                        ok.add(file);
                    } else {
                    	if (artist != "unknown" && title != "unknown") {
                    		file = mediaFileService.getMediaFile(artist, title);
                    	}
                        if (file != null) {
                            ok.add(file);
                        } else {
                        error.add(line);
                        }
                    }
                }
                line = reader.readLine();
            }
            
            result.setMediaFiles(new Pair<List<MediaFile>, List<String>>(ok, error));
            return result;
        }

        public void format(List<MediaFile> files, PrintWriter writer, String username, boolean isPublic) throws IOException {
            writer.println("#EXTM3U");
            writer.println("#EXTUSR:" + username);
            writer.println("#EXTSEC:" + ( isPublic ? "public" : "private"));
            
            for (MediaFile file : files) {
            	
            	String extinf = "#EXTINF:";
            	
            	if (file.getDurationSeconds() == null){
					extinf = extinf +"0,";
            	}else {
            		extinf = extinf + file.getDurationSeconds() + ",";
            	}
            	
            	if (file.getArtist() == null){
					extinf = extinf +"Unknown";
            	}else {
            		extinf = extinf + file.getArtist();
            	}

            	if (file.getTitle() == null){
					extinf = extinf +" - Unknown";
            	}else {
            		extinf = extinf + " - " + file.getTitle();
            	}
                writer.println(extinf);
            	
                writer.println(file.getPath());
            }	
            if (writer.checkError()) {
                throw new IOException("Error when writing playlist");
            }
        }
        
        public void format(List<MediaFile> files, BufferedWriter writer, String username, boolean isPublic) throws IOException {
            	writer.write("#EXTM3U");
            	writer.newLine();
                writer.write("#EXTUSR:" + username);
                writer.newLine();
                writer.write("#EXTSEC:" + ( isPublic ? "public" : "private"));
                writer.newLine();
                
            for (MediaFile file : files) {
            	
            	String extinf = "#EXTINF:";
            	
            	if (file.getDurationSeconds() == null){
					extinf = extinf +"0,";
            	}else {
            		extinf = extinf + file.getDurationSeconds() + ",";
            	}
            	
            	if (file.getArtist() == null){
					extinf = extinf +"Unknown";
            	}else {
            		extinf = extinf + file.getArtist();
            	}

            	if (file.getTitle() == null){
					extinf = extinf +" - Unknown";
            	}else {
            		extinf = extinf + " - " + file.getTitle();
            	}
            	
                writer.write(extinf);
            	writer.newLine();
                writer.write(file.getPath());
            	writer.newLine();
            }
        }        
        
    }

    /**
     * Implementation of PLS playlist format.
     */
    private class PLSFormat extends PlaylistFormat {
        public PlaylistResult parse(BufferedReader reader, MediaFileService mediaFileService) throws IOException {
            List<MediaFile> ok = new ArrayList<MediaFile>();
            List<String> error = new ArrayList<String>();
            PlaylistResult result = new PlaylistResult();
            
            Pattern pattern = Pattern.compile("File\\d+=(.*)$");
            String line = reader.readLine();
            while (line != null) {

                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    String path = matcher.group(1);
                    MediaFile file = getMediaFile(path);
                    if (file != null) {
                        ok.add(file);
                    } else {
                        error.add(path);
                    }
                }
                
                if (line.startsWith("Info=")) {
                	result.setPublic(line.substring(5).contains("public") ? true : false);
                } 

                if (line.startsWith("Creator=")) {
                	result.setUsername(line.substring(8));
                }
                
                line = reader.readLine();
            }
            result.setMediaFiles(new Pair<List<MediaFile>, List<String>>(ok, error));
            return result;
        }

        public void format(List<MediaFile> files, PrintWriter writer, String username, boolean isPublic) throws IOException {
            writer.println("[playlist]");
            int counter = 0;

            for (MediaFile file : files) {
                counter++;
                writer.println("File" + counter + '=' + file.getPath());
            }
            writer.println("NumberOfEntries=" + counter);
            writer.println("Version=2");
            writer.println("Creator=" + username ); 
            writer.println("Info=" + (isPublic ? "public" : "private"));            

            if (writer.checkError()) {
                throw new IOException("Error when writing playlist.");
            }
        }
    }

    /**
     * Implementation of XSPF (http://www.xspf.org/) playlist format.
     */
    private class XSPFFormat extends PlaylistFormat {
    	
        public PlaylistResult parse(BufferedReader reader, MediaFileService mediaFileService) throws IOException {
            List<MediaFile> ok = new ArrayList<MediaFile>();
            List<String> error = new ArrayList<String>();
            PlaylistResult result = new PlaylistResult();            

            SAXBuilder builder = new SAXBuilder();
            Document document;
            try {
                document = builder.build(reader);
            } catch (JDOMException x) {
                LOG.warn("Failed to parse XSPF playlist.", x);
                throw new IOException("Failed to parse XSPF playlist.");
            }

            Element root = document.getRootElement();
            Namespace ns = root.getNamespace();
            Element info = root.getChild("info", ns);
    		Element creator = root.getChild("creator", ns);
            Element trackList = root.getChild("trackList", ns);
            List<?> tracks = trackList.getChildren("track", ns);

            for (Object obj : tracks) {
                Element track = (Element) obj;
                String location = track.getChildText("location", ns);
                if (location != null && location.startsWith("file:///")) {
                    location = location.replaceFirst("file:///", "");
                    MediaFile file = getMediaFile(URLParamDecode.decode(location));
                    if (file != null) {
                        ok.add(file);
                    } else {
                        error.add(URLParamDecode.decode(location));
                    }
                }
            }

            result.setUsername(creator.getText());
            result.setPublic(info.getName().contains("public") ? true : false);
            result.setMediaFiles(new Pair<List<MediaFile>, List<String>>(ok, error));
            return result;
        }
        
        public void format(List<MediaFile> files, PrintWriter writer, String username, boolean isPublic) throws IOException {
            writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            writer.println("<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\">");
            writer.println("    <trackList>");
            for (MediaFile file : files) {
            	String encodedPath = URLParamEncoder.encode(file.getPath());
                writer.println("        <track><location>file:///" + encodedPath + "</location></track>");
            }
            writer.println("    </trackList>");
            writer.println("    <info>" + (isPublic ? "public" : "private") + "</info>");            
            writer.println("    <creator>" + username + "</creator>"); 
            writer.println("</playlist>");

            if (writer.checkError()) {
                throw new IOException("Error when writing playlist.");
            }
        }
    }
    
    /**
     * Implementation of WPL (https://en.wikipedia.org/wiki/Windows_Media_Player_Playlist/) playlist format.
     */    
    private class WPLFormat extends PlaylistFormat {

		@Override
		public PlaylistResult parse(BufferedReader reader, MediaFileService mediaFileService) throws IOException {
            List<MediaFile> ok = new ArrayList<MediaFile>();
            List<String> error = new ArrayList<String>();
            PlaylistResult result = new PlaylistResult();            

            SAXBuilder builder = new SAXBuilder();
            Document document;
            try {
                document = builder.build(reader);
            } catch (JDOMException x) {
                LOG.warn("Failed to parse WPL playlist.", x);
                throw new IOException("Failed to parse WPL playlist.");
            }

            Element root = document.getRootElement();
            Namespace ns = root.getNamespace();
            Element head = root.getChild("head", ns);
            Element body = root.getChild("body", ns);
    		Element info = head.getChild("info", ns);    		
    		Element creator = head.getChild("author", ns);
            Element trackList = body.getChild("seq", ns);
            List<?> tracks = trackList.getChildren("media", ns);

            for (Object obj : tracks) {
                Element track = (Element) obj;
                String location = track.getAttribute("src").getValue();
                if (location != null) {
                    MediaFile file = getMediaFile(location);
                    if (file != null) {
                        ok.add(file);
                    } else {
                        error.add(location);
                    }
                }
            }

            result.setUsername(creator.getText());
            result.setPublic(info.getName().contains("public") ? true : false);
            result.setMediaFiles(new Pair<List<MediaFile>, List<String>>(ok, error));
            return result;
		}

		@Override
		public void format(List<MediaFile> files, PrintWriter writer, String username, boolean isPublic) throws IOException {
            writer.println("<?wpl version=\"1.0\"?>");
            writer.println("<smil>");
            writer.println("    <head>");            
      		writer.println("        <meta name=\"Generator\" content=\"Madsonic Server -- " + settingsService.getVersion().toString() + "\"/>");
            writer.println("        <meta name=\"ItemCount\" content=\"" + files.size() + "\"/>");
            writer.println("        <meta name=\"ContentPartnerListID\"/>");
            writer.println("        <meta name=\"ContentPartnerNameType\"/>");
            writer.println("        <meta name=\"ContentPartnerName\"/>");
            writer.println("        <meta name=\"Subtitle\"/>");
            writer.println("        <author>" + username + "</author>");
            writer.println("        <info>" + (isPublic ? "public" : "private") + "</info>");            
            writer.println("     </head>");
            writer.println("     <body>");
            writer.println("        <seq>");
            for (MediaFile file : files) {
            writer.println("            <media src=\"" + file.getPath() + "\"/>"); }
            writer.println("        </seq>");
            writer.println("    </body>");
            writer.println("</smil>");         

            if (writer.checkError()) {
                throw new IOException("Error when writing playlist.");
            }
		 }
    }

    /**
     *  Implementation of iTunes (http://www.apple.com/itunes/) playlist format.
     */
    private class ItunesFormat extends PlaylistFormat {

		//TODO: iTunes parse
    	@Override
		public PlaylistResult parse(BufferedReader reader, MediaFileService mediaFileService) throws Exception {
			throw new Exception("ITunes parsing not yet implemented!");
		}

		//TODO: iTunes format    	
		@Override
		public void format(List<MediaFile> files, PrintWriter writer, String username, boolean isPublic) throws Exception {
			throw new Exception("ITunes Format not yet implemented!");
		}
    }    
    
    private static class PlaylistComparator implements Comparator<Playlist> {
        @Override
        public int compare(Playlist p1, Playlist p2) {
            return p1.getName().compareTo(p2.getName());
        }
    }
}
