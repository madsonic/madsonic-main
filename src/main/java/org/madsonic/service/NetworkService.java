/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import org.madsonic.Logger;
import org.madsonic.domain.UrlRedirectType;
import org.madsonic.service.network.DiscoveryService;
import org.madsonic.service.upnp.ClingRouter;
import org.madsonic.service.upnp.NATPMPRouter;
import org.madsonic.service.upnp.Router;
import org.madsonic.util.StringUtil;

/**
 * Provides network-related services, including port forwarding on UPnP routers and
 * URL redirection from http://xxxx.madsonic.org.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class NetworkService {
 
    private static final Logger LOG = Logger.getLogger(NetworkService.class);
    private static final long PORT_FORWARDING_DELAY = 3600L;
    private static final long URL_REDIRECTION_DELAY = 2 * 3600L;

    private static final String URL_REDIRECTION_REGISTER_URL = getBackendUrl() + "/backend/redirect/register.view";
    private static final String URL_REDIRECTION_UNREGISTER_URL = getBackendUrl() + "/backend/redirect/unregister.view";
    private static final String URL_REDIRECTION_TEST_URL = getBackendUrl() + "/backend/redirect/test.view";

    private SettingsService settingsService;
    private UPnPService upnpService;
    
    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(4);
    private final PortForwardingTask portForwardingTask = new PortForwardingTask();
    private final URLRedirectionTask urlRedirectionTask = new URLRedirectionTask();
    
    private Future<?> portForwardingFuture;
    private Future<?> urlRedirectionFuture;

    private final Status portForwardingStatus = new Status();
    private final Status urlRedirectionStatus = new Status();
    
    private boolean testUrlRedirection;

    public void init() {
    	
        initPortForwarding(10);
        initUrlRedirection(false);
        
        LOG.debug("Check backend interface ...");
        // LOG.debug(URL_REDIRECTION_TEST_URL);
        
        if (settingsService.isNodesEnabled()) {
            LOG.debug("Start Madsonic Node Discovery ...");
			DiscoveryService discoveryService = DiscoveryService.getInstance();
			discoveryService.startTask();
        } else {
            LOG.debug("Madsonic Node Discovery is disabled.");
        }
    }

    /**
     * Configures UPnP port forwarding.
     */
    public synchronized void initPortForwarding(int initialDelaySeconds) {
        portForwardingStatus.setText("Idle");
        if (portForwardingFuture != null) {
            portForwardingFuture.cancel(true);
        }
        portForwardingFuture = executor.scheduleWithFixedDelay(portForwardingTask, initialDelaySeconds, PORT_FORWARDING_DELAY, TimeUnit.SECONDS);
    }

    /**
     * Configures URL redirection.
     *
     * @param test Whether to test that the redirection works.
     */
    public synchronized void initUrlRedirection(boolean test) {
        urlRedirectionStatus.setText("Idle");
        if (urlRedirectionFuture != null) {
            urlRedirectionFuture.cancel(true);
        }
        testUrlRedirection = test;
        urlRedirectionFuture = executor.scheduleWithFixedDelay(urlRedirectionTask, 0L, URL_REDIRECTION_DELAY, TimeUnit.SECONDS);
    }

    public Status getPortForwardingStatus() {
        return portForwardingStatus;
    }

    public Status getURLRedirecionStatus() {
        return urlRedirectionStatus;
    }

    public static String getBackendUrl() {
        return "true".equals(System.getProperty("madsonic.test")) ? "http://localhost:8090" : "https://www.madsonic.org";
    }

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setUpnpService(UPnPService upnpService) {
        this.upnpService = upnpService;
    }

    private class PortForwardingTask extends Task {

        @Override
        protected void execute() {

            boolean enabled = settingsService.isPortForwardingEnabled();
            portForwardingStatus.setText("Looking for router...");
            Router router = findRouter();
            if (router == null) {
                LOG.warn("No UPnP router found.");
                portForwardingStatus.setText("No router found.");
            } else {

                portForwardingStatus.setText("Router found.");

                int port = settingsService.getPort();
                int httpsPort = settingsService.getHttpsPort();

                // Create new NAT entry.
                if (enabled) {
                    try {
                        router.addPortMapping(port, port, 0);
                        String message = "Successfully forwarding port " + port;

                        if (httpsPort != 0 && httpsPort != port) {
                            router.addPortMapping(httpsPort, httpsPort, 0);
                            message += " and port " + httpsPort;
                        }
                        message += ".";

                        LOG.info(message);
                        portForwardingStatus.setText(message);
                    } catch (Throwable x) {
                        String message = "Failed to create port forwarding.";
                        LOG.warn(message, x);
                        portForwardingStatus.setText(message + " See log for details.");
                    }
                }

                // Delete NAT entry.
                else {
                    try {
                        router.deletePortMapping(port, port);
                        LOG.info("Deleted port mapping for port " + port);
                        if (httpsPort != 0 && httpsPort != port) {
                            router.deletePortMapping(httpsPort, httpsPort);
                            LOG.info("Deleted port mapping for port " + httpsPort);
                        }
                    } catch (Throwable x) {
                        LOG.warn("Failed to delete port mapping.", x);
                    }
                    portForwardingStatus.setText("Port forwarding disabled.");
                }
            }

            //  Don't do it again if disabled.
            if (!enabled && portForwardingFuture != null) {
                portForwardingFuture.cancel(false);
            }
        }

        private Router findRouter() {

            try {
                Router router = ClingRouter.findRouter(upnpService);
                if (router != null) {
                	LOG.debug("Find router using Cling library.");
                    return router;
                }
            } catch (NullPointerException x) {
                LOG.warn("Failed to find UPnP router using Cling library [UPnP/DLNA service is disabled].");
            } catch (Throwable x) {
                LOG.warn("Failed to find UPnP router using Cling library.");
            }

            try {
                Router router = NATPMPRouter.findRouter();
                if (router != null) {
                	LOG.debug("Find router using NAT-PMP library.");
                    return router;
                }
            } catch (Throwable x) {
                LOG.warn("Failed to find NAT-PMP router.", x);
            }

            return null;
        }
    }

    private class URLRedirectionTask extends Task {

        @Override
        protected void execute() {

            boolean enable = settingsService.isUrlRedirectionEnabled() && settingsService.getUrlRedirectType() == UrlRedirectType.NORMAL;
            HttpPost request = new HttpPost(enable ? URL_REDIRECTION_REGISTER_URL : URL_REDIRECTION_UNREGISTER_URL);

            int port = settingsService.getPort();
            int httpsPort = settingsService.getHttpsPort();
            
            boolean trial = !settingsService.isLicenseValid();
            Date trialExpires = settingsService.getTrialExpires();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("serverId", settingsService.getServerId()));
            params.add(new BasicNameValuePair("redirectFrom", settingsService.getUrlRedirectFrom()));
            params.add(new BasicNameValuePair("port", String.valueOf(port)));
            params.add(new BasicNameValuePair("httpsPort", String.valueOf(httpsPort)));
            params.add(new BasicNameValuePair("localIp", settingsService.getLocalIpAddress()));
            params.add(new BasicNameValuePair("localPort", String.valueOf(port)));
            params.add(new BasicNameValuePair("contextPath", settingsService.getUrlRedirectContextPath()));
            params.add(new BasicNameValuePair("trial", String.valueOf(trial)));
            if (trial && trialExpires != null) {
                params.add(new BasicNameValuePair("trialExpires", String.valueOf(trialExpires.getTime())));
            } else {
                params.add(new BasicNameValuePair("licenseHolder", settingsService.getLicenseEmail()));
            }
            
            CloseableHttpClient client = HttpClientBuilder.create().build();

            try {
                urlRedirectionStatus.setText(enable ? "Registering web address ..." : "Unregistering web address ...");
                request.setEntity(new UrlEncodedFormEntity(params, StringUtil.ENCODING_UTF8));

                HttpResponse response = client.execute(request);
                StatusLine status = response.getStatusLine();

                switch (status.getStatusCode()) {
                    case HttpStatus.SC_BAD_REQUEST:
                        urlRedirectionStatus.setText(EntityUtils.toString(response.getEntity()));
                        testUrlRedirection = false;
                        break;
                    case HttpStatus.SC_OK:
                        urlRedirectionStatus.setText(enable ? "Successfully registered web address." : "Web address disabled.");
                        break;
                    default:
                        testUrlRedirection = false;
                        throw new IOException(status.getStatusCode() + " " + status.getReasonPhrase());
                }

            } catch (Throwable x) {
                LOG.warn(enable ? "Failed to register web address." : "Failed to unregister web address.", x);
                urlRedirectionStatus.setText(enable ? ("Failed to register web address. " + x.getMessage() +
                        " (" + x.getClass().getSimpleName() + ")") : "Web address disabled.");
            } finally {
                try {
					client.close();
				} catch (IOException e) {
		            LOG.warn("Failed to close network request.");
				}
            }

            // Test redirection, but only once.
            if (testUrlRedirection) {
                testUrlRedirection = false;
                try {
					testUrlRedirection();
				} catch (KeyManagementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KeyStoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }

            //  Don't do it again if disabled.
            if (!enable && urlRedirectionFuture != null) {
                urlRedirectionFuture.cancel(false);
            }
        }

        private void testUrlRedirection() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

            String urlToTest;
            String url = URL_REDIRECTION_TEST_URL;
            if (settingsService.getUrlRedirectType() == UrlRedirectType.NORMAL) {
                url += "?redirectFrom=" + settingsService.getUrlRedirectFrom();
                urlToTest = settingsService.getUrlRedirectFrom() + ".madsonic.org";
            } else {
                url += "?customUrl=" + settingsService.getUrlRedirectCustomUrl();
                urlToTest = settingsService.getUrlRedirectCustomUrl();
            }
            
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(url);
            try {
                RequestConfig requestConfig = RequestConfig.custom()
                  .setSocketTimeout(10 * 60 * 1000) // 10 minutes
                  .setConnectTimeout(2 * 60 * 1000) // 2 minutes
                  .setConnectionRequestTimeout(2 * 60 * 1000) // 2 minutes
                  .build();
                request.setConfig(requestConfig);            	
                urlRedirectionStatus.setText("Testing web address " + urlToTest + ". Please wait ...");
				String response = httpClient.execute(request, new BasicResponseHandler());
				urlRedirectionStatus.setText(response);
                
            } catch (Throwable x) {
                LOG.warn("Failed to test web address.", x);
                urlRedirectionStatus.setText("Failed to test web address. " + x.getMessage() + " (" + x.getClass().getSimpleName() + ")");
            } finally {
            	try {
            		httpClient.close();
				} catch (IOException e) { 
		            LOG.warn("Failed to close test request.");
				}
            }
        }
    }

    private abstract class Task implements Runnable {
        public void run() {
            String name = getClass().getSimpleName();
            try {
                execute();
            } catch (Throwable x) {
                LOG.error("Error executing " + name + ": " + x.getMessage(), x);
            }
        }
        protected abstract void execute();
    }

    public static class Status {

        private String text;
        private Date date;

        public void setText(String text) {
            this.text = text;
            date = new Date();
        }

        public String getText() {
            return text;
        }

        public Date getDate() {
            return date;
        }
    }
}
