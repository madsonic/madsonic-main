/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service.metadata;

import org.madsonic.Logger;
import org.madsonic.domain.MediaFile;
import org.madsonic.io.InputStreamReaderThread;
import org.madsonic.service.ServiceLocator;
import org.madsonic.service.TranscodingService;
import org.madsonic.util.StringUtil;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses meta data from media files using FFmpeg (http://ffmpeg.org/).
 * <p/>
 * Currently duration, bitrate, dimension and Metadata: ARTIST, ALBUM, TITLE are supported.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class FFmpegParser extends MetaDataParser {

	private static final Logger LOG = Logger.getLogger(FFmpegParser.class);

	private static final Pattern TRACK_PATTERN = Pattern.compile("Stream #0.(\\d+)(\\((.*?)\\))?: (.*?): (\\w+)");
	private static final Pattern DURATION_PATTERN = Pattern.compile("Duration:\\s(\\d+):(\\d+):(\\d+).(\\d+)");
	private static final Pattern BITRATE_PATTERN = Pattern.compile("bitrate:\\s(\\d+) kb/s");
	private static final Pattern DIMENSION_PATTERN = Pattern.compile("Video.*?, (\\d+)x(\\d+)");
	private static final Pattern PAR_PATTERN = Pattern.compile("PAR (\\d+):(\\d+)");

	private static final Pattern ARTIST_PATTERN = Pattern.compile("ARTIST\\s.*:.(.*)");
	private static final Pattern TITLE_PATTERN = Pattern.compile("TITLE\\s.*:.(.*)");
	private static final Pattern ALBUM_PATTERN = Pattern.compile("ALBUM\\s.*:.(.*)");
	private static final Pattern DATE_PATTERN = Pattern.compile("DATE\\s.*:.(.*)");

	private TranscodingService transcodingService;

	/**
	 * Parses meta data for the given music file. No guessing or reformatting is done.
	 *
	 *
	 * @param file The music file to parse.
	 * @return Meta data for the file.
	 */
	@Override
	public MetaData getRawMetaData(File file) {

		MetaData metaData = new MetaData();

		try {

			File ffmpeg = new File(transcodingService.getTranscodeDirectory(), "ffmpeg");

			String[] command = new String[]{ffmpeg.getAbsolutePath(), "-i", file.getAbsolutePath()};
			Process process = Runtime.getRuntime().exec(command);
			InputStream stdout = process.getInputStream();
			InputStream stderr = process.getErrorStream();

			// Consume stdout, we're not interested in that.
			new InputStreamReaderThread(stdout, "ffmpeg", true).start();

			// Read everything from stderr.  It will contain text similar to:
			// Input #0, matroska,webm, from 'Planes 2.mkv':
			// Duration: 01:23:38.59, start: 0.000000, bitrate: 3196 kb/s
			//     Stream #0:0(eng): Video: h264 (Constrained Baseline), yuv420p, 1280x720 [SAR 1:1 DAR 16:9], 23.98 fps, 23.98 tbr, 1k tbn, 47.95 tbc (default)
			//     Stream #0:1(eng): Audio: mp3, 44100 Hz, stereo, s16p, 128 kb/s (default)
			//     Stream #0:2(eng): Subtitle: hdmv_pgs_subtitle (default)
			//     Stream #0:3(dan): Audio: mp3, 44100 Hz, stereo, s16p, 128 kb/s (default)
			//     Stream #0:4(fin): Audio: mp3, 44100 Hz, stereo, s16p, 128 kb/s (default)
			//     Stream #0:5(nor): Audio: mp3, 44100 Hz, stereo, s16p, 128 kb/s (default)
			//     Stream #0:6(swe): Audio: mp3, 44100 Hz, stereo, s16p, 128 kb/s (default)
			//     Stream #0:0: Audio: opus, 48000 Hz, stereo, fltp
			//     Metadata:
			//     ARTIST          : Ehren Starks
			//     TITLE           : Paper Lights
			//     ALBUM           : Lines Build Walls
			//     DATE            : 2005-09-05
			//     COPYRIGHT       : Copyright 2005 Ehren Starks
			//     LICENSE         : http://creativecommons.org/licenses/by-nc-sa/1.0/
			//     ORGANIZATION    : magnatune.com

			String[] lines = StringUtil.readLines(stderr);

			Integer width = null;
			Integer height = null;
			Double par = 1.0;
			for (String line : lines) {

				Matcher matcher = ARTIST_PATTERN.matcher(line);
				if (matcher.find()) {
					metaData.setArtist(matcher.group(1));
				}            	

				matcher = ALBUM_PATTERN.matcher(line);
				if (matcher.find()) {
					metaData.setAlbumName(matcher.group(1));
				}            	

				matcher = TITLE_PATTERN.matcher(line);
				if (matcher.find()) {
					metaData.setTitle(matcher.group(1));
				}            	

				matcher = DURATION_PATTERN.matcher(line);
				if (matcher.find()) {
					int hours = Integer.parseInt(matcher.group(1));
					int minutes = Integer.parseInt(matcher.group(2));
					int seconds = Integer.parseInt(matcher.group(3));
					metaData.setDurationSeconds(hours * 3600 + minutes * 60 + seconds);
				}
				
				matcher = DATE_PATTERN.matcher(line);
				if (matcher.find()) {
					int year = Integer.valueOf(dateToYear(matcher.group(1)));
					metaData.setYear(year);
				}            	

				matcher = TRACK_PATTERN.matcher(line);
				if (matcher.find()) {
					int id = Integer.parseInt(matcher.group(1));
					String type = matcher.group(4);
					String language = matcher.group(3);
					String codec = matcher.group(5);
					metaData.addTrack(new Track(id, type, language, codec));
				}

				matcher = BITRATE_PATTERN.matcher(line);
				if (matcher.find()) {
					metaData.setBitRate(Integer.valueOf(matcher.group(1)));
				}

				matcher = DIMENSION_PATTERN.matcher(line);
				if (matcher.find()) {
					width = Integer.valueOf(matcher.group(1));
					height = Integer.valueOf(matcher.group(2));
				}

				// PAR = Pixel Aspect Rate
						matcher = PAR_PATTERN.matcher(line);
						if (matcher.find()) {
							int a = Integer.parseInt(matcher.group(1));
							int b = Integer.parseInt(matcher.group(2));
							if (a > 0 && b > 0) {
								par = (double) a / (double) b;
							}
						}
			}

			if (width != null && height != null) {
				width = (int) Math.round(width.doubleValue() * par);
				metaData.setWidth(width);
				metaData.setHeight(height);
			}


		} catch (Throwable x) {
			LOG.warn("Error when parsing metadata in " + file, x);
		}

		return metaData;
	}

	private String dateToYear(String date) {
		if (date == null) {
			return "-1";}

		String yr = date;
		if (date.length() > 4) {
			try {
				if (date.matches("^\\d{4}.*")) {
					int i = Integer.parseInt(date.substring(0,4));
					if (isValidYear(i)) {
						yr = date.substring(0,4);
					}
				}
				if (date.matches(".*\\d{4}$") && yr.equals(date)){
					int i = Integer.parseInt(date.substring(date.length()-4));
					if (isValidYear(i)) {
						yr = date.substring(date.length()-4);
					}
				}
			}
			catch (NumberFormatException nfe) {} // Ignore NFEs 
			catch (NullPointerException npe) {}	 // Ignore NPEs 
		}
		return yr;
	}

	/**
	 * Return true if integer appears to be a valid year
	 */
	private boolean isValidYear(Integer i) {
		boolean val = false;
		if (i>1000 && i<2100) {
			val = true;
		}
		return val;
	}    


	/**
	 * Not supported.
	 */
	@Override
	public void setMetaData(MediaFile file, MetaData metaData) {
		throw new RuntimeException("setMetaData() not supported in " + getClass().getSimpleName());
	}

	/**
	 * Returns whether this parser supports tag editing (using the {@link #setMetaData} method).
	 *
	 * @return Always false.
	 */
	@Override
	public boolean isEditingSupported() {
		return false;
	}

	/**
	 * Returns whether this parser is applicable to the given file.
	 *
	 * @param file The file in question.
	 * @return Whether this parser is applicable to the given file.
	 */
	@Override
	public boolean isApplicable(File file) {
		String format = FilenameUtils.getExtension(file.getName()).toLowerCase();

		for (String s : ServiceLocator.getSettingsService().getVideoFileTypesAsArray()) {
			if (format.equals(s)) {
				return true;
			}
		}
		return false;
	}

	public void setTranscodingService(TranscodingService transcodingService) {
		this.transcodingService = transcodingService;
	}
}