/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Subsonic, Copyright 2004-2016 (C) Sindre Mehus
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */

package org.madsonic.service.metadata;

import java.util.Arrays;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

/**
 * A track within a media file container (e.g., mp4 or mkv)
 *
 * @author Sindre Mehus
 * @version $Id$
 */
public class Track {

    private final int id;
    private final String type;
    private final String language;
    private final String codec;

    public Track(int id, String type, String language, String codec) {
        this.id = id;
        this.type = StringUtils.trimToNull(type);
        this.language = StringUtils.trimToNull(language);
        this.codec = StringUtils.trimToNull(codec);
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getLanguage() {
        return language;
    }

    public String getLanguageName()
    {
      if (this.language == null) {
        return String.valueOf(this.id);
      }
      Locale locale = new Locale(this.language);
      String languageName = StringUtils.trimToNull(locale.getDisplayLanguage(Locale.ENGLISH));
      return languageName == null ? this.language : languageName;
    }    
    
    public String getCodec() {
        return codec;
    }

    @Override
    public String toString() {
        return id + " " + type + " " + language + " " + codec;
    }

    public boolean isAudio() {
        return "Audio".equals(type);
    }

    public boolean isVideo() {
        return "Video".equals(type) && !"mjpeg".equals(codec) && !"bmp".equals(codec);
    }

    public boolean isStreamable() {
        return Arrays.asList("h264", "aac", "mp3").contains(codec);
    }
}
