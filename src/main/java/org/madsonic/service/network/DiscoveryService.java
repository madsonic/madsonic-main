/*
 * This file is part of Madsonic.
 *
 *  Madsonic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Madsonic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based upon Madsonic, Copyright 2012-2022 (C) Martin Karel
 *  
 */
package org.madsonic.service.network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.madsonic.Logger;
import org.madsonic.dao.DaoHelper;
import org.madsonic.dao.HsqlDaoHelper;
import org.madsonic.dao.NodeDao;
import org.madsonic.domain.Node;
import org.madsonic.util.StringUtil;

public class DiscoveryService implements Runnable {

	private static final Logger LOG = Logger.getLogger(DiscoveryService.class);

	DatagramSocket socket;

	private NodeDao nodeDao;
	private DaoHelper daoHelper;
	private Thread discoveryThread;

	private boolean isStarted = false;

	String registerRequestData = "REGISTER_MADSONIC_NODE_REQUEST_";
	String registerRespondData = "REGISTER_MADSONIC_NODE_DONE_";
	
	String unregisterRequestData = "UNREGISTER_MADSONIC_NODE_REQUEST_";
	String unregisterRespondData = "UNREGISTER_MADSONIC_NODE_DONE_";

	byte[] sendRegisterRespondeData = registerRespondData.getBytes();
	byte[] sendUnregisterRespondData = unregisterRespondData.getBytes();

	public void startTask() {
		discoveryThread = new Thread(this);
		discoveryThread.start();
	}

	public void run() {
		try {
			//Keep a socket open to listen to all the UDP trafic that is destined for this port
			socket = new DatagramSocket(8888, InetAddress.getByName("0.0.0.0"));
			socket.setBroadcast(true);

			while (!Thread.interrupted()){

				if ((!isStarted)) {
					isStarted = true;
					LOG.debug("Ready to receive broadcast from nodes!");
				}
				//Receive a packet
				byte[] recvBuf = new byte[15000];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				socket.receive(packet);

				//See if the packet holds the right command (message)
				String message = new String(packet.getData()).trim();

				String regServer = message.replace(registerRequestData, "");
				String unregServer = message.replace(unregisterRequestData, "");

				//Packet received
				LOG.debug("Discovery packet received from: " + packet.getAddress().getHostAddress());
				LOG.debug("Packet received; data: " + message);

				// REGISTER_MADSONIC_NODE_REQUEST
				if (message.replace(regServer, "").equals(registerRequestData)) {
					byte[] sendData = sendRegisterRespondeData;
					DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, packet.getAddress(), packet.getPort());
					socket.send(sendPacket);
					LOG.debug("Sent packet to: " + sendPacket.getAddress().getHostAddress());

					String[] srv = StringUtil.utf8HexDecode(regServer).split(":");
					String url = "http://" + srv[0] + ":" + srv[1];
					
					if (nodeDao == null) registerDaoHelper();
					Node node = nodeDao.getNode(url);
					if (node != null) {
						node.setOnline(true);
						nodeDao.updateNode(node);
					} else {
						node = new Node(null, url, "NODE-" + sendPacket.getAddress().getHostAddress(), true, true);
						nodeDao.createNode(node);
					}
				}
				// UNREGISTER_MADSONIC_NODE_REQUEST
				if (message.replace(unregServer, "").equals(unregisterRequestData)) {	
					byte[] sendData = sendUnregisterRespondData;
					DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, packet.getAddress(), packet.getPort());
					socket.send(sendPacket);
					LOG.debug("Sent packet to: " + sendPacket.getAddress().getHostAddress());

					String[] srv = StringUtil.utf8HexDecode(unregServer).split(":");
					String url = "http://" + sendPacket.getAddress().getHostAddress() + ":" + srv[1];
					
					if (nodeDao == null) registerDaoHelper();
					Node node = nodeDao.getNode(url);
					if (node != null) {
						node.setOnline(false);
						nodeDao.updateNode(node);
					} 
				}			
			}
			isStarted = false;

		} catch (Exception ex) {
			LOG.error("Error on DiscoveryService: " + ex.getMessage());		
		}
	}

	public void stopTask() {
		discoveryThread.interrupt();
		isStarted = false;
		socket.close();
		try {
			discoveryThread.join();
		} catch (InterruptedException ex) {
			// Unexpected interruption
			ex.printStackTrace();
			System.exit(1);
		}
	}

	public boolean isRunning() {
		if (discoveryThread == null) return false;
		return discoveryThread.isAlive();
	}	

	public static void main(String[] args) throws Exception {
		DiscoveryService discoveryService = DiscoveryService.getInstance();
		discoveryService.startTask();
		Thread.sleep(2000);
		if (discoveryService.isRunning()) {
			discoveryService.stopTask();
		}
		Thread.sleep(1000);
		String msg = discoveryService.isRunning() ? "Thread still runnig )x:=" : "Thread is stoped! (c;=";
		System.out.println(msg);
	}	  

	public static DiscoveryService getInstance() {
		return DiscoveryThreadHolder.INSTANCE;
	}

	private static class DiscoveryThreadHolder {
		private static final DiscoveryService INSTANCE = new DiscoveryService();
	}

	private void registerDaoHelper() {
		daoHelper = new HsqlDaoHelper();
		nodeDao = new NodeDao();
		nodeDao.setDaoHelper(daoHelper);
	}

	public void setNodeDao(NodeDao nodeDao) {
		this.nodeDao = nodeDao;
	}

	public boolean isStarted() {
		return isStarted;
	}

	public void setStarted(boolean isStarted) {
		this.isStarted = isStarted;
	}	
}