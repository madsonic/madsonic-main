<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%@ include file="include.jsp" %>

<%--
Creates HTML for displaying the rating stars.
PARAMETERS
  id: MediaFile id
  path: Album path. May be null if readonly.
  readonly: Whether rating can be changed.
  rating: The rating, an integer from 0 (no rating), through 10 (lowest rating), to 50 (highest rating).
--%>

<c:forEach var="i" begin="1" end="5">

    <madsonic:url value="setRating.view" var="ratingUrl">
        <madsonic:param name="id" value="${param.id}"/>
        <madsonic:param name="action" value="rating"/>
        <madsonic:param name="rating" value="${i}"/>
    </madsonic:url>

    <div style="display:inline" style="vertical-align: -webkit-baseline-middle;">
    <c:if test="${not param.readonly}"><a href="${ratingUrl}"></c:if> 
    <c:choose>
        <c:when test="${param.rating ge i * 10}">
            <div class="icon-wrapper"><i class="fa fa-star starred clickable custom-icon-main" title="<fmt:message key="common.star"/>"><span class="fix-editor">&nbsp;</span></i></div>
        </c:when>
        <c:when test="${param.rating ge i*10 - 7 and param.rating le i*10 - 3}">
            <div class="icon-wrapper"><i class="fa fa-star-half-o starred clickable custom-icon-main" title="<fmt:message key="common.star"/>"><span class="fix-editor">&nbsp;</span></i></div>
        </c:when>
        <c:otherwise>
            <div class="icon-wrapper"><i class="fa fa-star-o starred clickable custom-icon-main" title="<fmt:message key="common.star"/>"><span class="fix-editor">&nbsp;</span></i></div>
        </c:otherwise>
    </c:choose>
    <c:if test="${not param.readonly}"></a></c:if>
    </div>

</c:forEach>

<madsonic:url value="setRating.view" var="clearRatingUrl">
    <madsonic:param name="id" value="${param.id}"/>
    <madsonic:param name="action" value="rating"/>
    <madsonic:param name="rating" value="0"/>
</madsonic:url>

<c:if test="${not param.readonly}">
    <a href="${clearRatingUrl}">        
    <div class="icon-wrapper"><i class="fa fa-close clickable custom-icon-main warning" title="<fmt:message key="rating.clearrating"/>"><span class="fix-editor">&nbsp;</span></i></div>
    </a>
</c:if>
