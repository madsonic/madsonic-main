<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>      
    
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">    
    
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>
        
    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-2.1.2.js"/>"></script>    

    <style type="text/css">
    
        .artistindex {
            opacity: 1.0;
            clear: both;
            position: fixed;
            top: 0;
            right: 0;
            margin: 0px 0px 0px 0px;
            padding: 5px 5px 5px 5px;
            height: 100%;
            /*  z-index: 1000; */
            text-align: center;
        }
    
        .browse-selected-music-folder {
            padding: 0.4em 0.5em;
            border:1px solid #<spring:theme code="detailColor"/>;
            margin-right: 2em;
            border-radius: 3px 3px 3px 3px;
        }
        .browse-index-shortcut {
            padding-bottom: 8px;
            font-size: 75%;
            line-height: 100%;
            font-weight: 300;
        }
        .browse-index-title {
            clear: both;
            font-size: 2em;
            line-height: 100%;
            padding-top: 10px;
            padding-bottom: 5px;
            border-radius: 3px 3px 3px 3px;
            font-weight: 600;
        }
        .browse-artist {
            float: left;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            width: ${model.indexListSize}em;
            padding: 0.2em 1.5em 0.2em 1.5em;
            font-weight: 400;
        }
        .browse-artist-large {
            width: 10em;
        }        
    </style>

    <script type="text/javascript" language="javascript">

        function toggleStar(mediaFileId, element) {
            starService.star(mediaFileId, !$(element).hasClass("fa-star"));
            $(element).toggleClass("fa-star fa-star-o starred");
        }

        function filterArtists(element) {
            var filter = $(element).val().toLowerCase();
            $(".browse-artist").each(function() {
                var artist = $(this).text().toLowerCase();
                $(this).toggle(artist.indexOf(filter) != -1);
            });

            $(".browse-index-title").each(function() {
                var show = $(this).nextUntil(".browse-index-title", ".browse-artist:visible").length > 0;
                $(this).toggle(show);
            });
        }
        
        function changeMusicFolder(musicFolderId) {
            multiService.setSelectedMusicFolder(musicFolderId, refresh);
        }

        function changeGenre(genre) {
            multiService.setSelectedGenre(genre, refresh);
        }        
        
        function refresh() {
            top.main.location.href = "artists.view";
        }        
    </script>
</head>

<body class="bgcolor1 mainframe" style="margin-left:20px; margin-right:40px;">

  <div id="content_index" class="content_main"> <!-- CONTENT -->
	
	<div id="container" class="container"> <!-- CONTAINER -->

<a name="top"></a>
<h1 style="padding-bottom:2.8em">  
<img src="<spring:theme code="indexImage"/>" width="32" title="<fmt:message key="artists.artists"/>">&nbsp;&nbsp;<fmt:message key="artists.artists"/></h1>
    <div class="bgcolor2 artistindex" style="">
        <h2>
            <div id="anchor_list">
            <div class="browse-index-shortcut"><i class="fa fa-arrow-up fa-fw icon clickable" onclick="location.href='#top'"></i></div>
            <c:forEach items="${model.indexes}" var="index">
                <div class="browse-index-shortcut"><a href="#${index.index}" lnk="${index.index}">${index.index}</a></div>
            </c:forEach>
            </div>
        </h2>
    </div>
    <div style="padding-bottom:3em">

	<span style="white-space: nowrap;">
    <i class="fa fa-folder-open-o fa-fw icon"></i>    
    <select name="musicFolderId" id="musicFolder" onchange="changeMusicFolder(options[selectedIndex].value);" style="margin-right:20px">
        <option value="-1"><fmt:message key="left.allfolders"/></option>
        
        <c:if test="${model.MusicFolderEnabled eq true}">
        <option ${model.selectedMusicFolderId == -2 ? "selected" : ""} value="-2"><fmt:message key="left.allmusic"/></option>            
        </c:if>
        <c:if test="${model.VideoFolderEnabled eq true}">
        <option ${model.selectedMusicFolderId == -3 ? "selected" : ""} value="-3"><fmt:message key="left.allvideos"/></option>
        </c:if>
        <c:if test="${model.MoviesFolderEnabled eq true}">
        <option ${model.selectedMusicFolderId == -4 ? "selected" : ""} value="-4"><fmt:message key="left.allmovies"/></option>
        </c:if>
        <c:if test="${model.SeriesFolderEnabled eq true}">
        <option ${model.selectedMusicFolderId == -5 ? "selected" : ""} value="-5"><fmt:message key="left.allseries"/></option>
        </c:if>
        <c:if test="${model.ImagesFolderEnabled eq true}">
        <option ${model.selectedMusicFolderId == -6 ? "selected" : ""} value="-6"><fmt:message key="left.allimages"/></option>
        </c:if>
        <c:if test="${model.TVFolderEnabled eq true}">
        <option ${model.selectedMusicFolderId == -7 ? "selected" : ""} value="-7"><fmt:message key="left.alltv"/></option>
        </c:if>
        <c:if test="${fn:length(model.musicFolders) > 0}">
        <option value="-1">------------</option>
        </c:if>
        <c:forEach items="${model.musicFolders}" var="musicFolder">
            <option ${model.selectedMusicFolder.id == musicFolder.id ? "selected" : ""} value="${musicFolder.id}">${musicFolder.name}</option>
        </c:forEach>
    </select>
	</span>
        
	<span style="white-space: nowrap;">		
    <i class="fa fa-cube fa-fw icon"></i>
    <select name="genreAll" id="genreAll" onchange="changeGenre(options[selectedIndex].value);" style="margin-right:20px;margin-bottom:5px;">
        <option value=""><fmt:message key="left.allgenres"/></option>
        <c:forEach items="${model.allGenres}" var="genre">
            <option ${genre eq model.selectedGenre ? "selected" : ""} value="${genre}">${genre}</option>
        </c:forEach>
    </select> 
	</span>

    <div style="float:right; padding-right:5em;white-space: nowrap;">
		<span>
			<c:choose>
				<c:when test="${model.scanning}">
					<a href="artists.view"><i class="fa fa-refresh fa-fw icon control"></i>&nbsp;</a>
				</c:when>
				<c:otherwise>
					<a href="artists.view?refresh=true" title="<fmt:message key="common.refresh"/>"><i class="fa fa-refresh fa-fw icon control"></i>&nbsp;</a>
				</c:otherwise>
			</c:choose>
		</span>	
        <input type="text" size="20" placeholder="<fmt:message key="common.filter"/>" onclick="select();" onkeyup="filterArtists(this)">
    </div>    
</div>

<div style="padding-bottom:0.4em">
    <c:forEach items="${model.shortcuts}" var="shortcut">
        <madsonic:url value="main.view" var="mainUrl">
            <madsonic:param name="id" value="${shortcut.id}"/>
        </madsonic:url>
        <input type="button" style="margin-right:0.6em; margin-bottom:0.6em" value="${fn:escapeXml(shortcut.name)}" onclick="location.href='${mainUrl}'">
    </c:forEach>
</div>

<c:forEach items="${model.indexedArtists}" var="entry">
    <a name="${fn:escapeXml(entry.key.index)}"></a>
    <div class="browse-index-title">${fn:escapeXml(entry.key.index)}</div>

    <c:forEach items="${entry.value}" var="artist">
        <madsonic:url value="main.view" var="mainUrl">
            <c:forEach items="${artist.mediaFiles}" var="mediaFile">
                <madsonic:param name="id" value="${mediaFile.id}"/>
            </c:forEach>
        </madsonic:url>
        
        <div class="browse-artist">
            <a target="main" href="${mainUrl}" title="${fn:escapeXml(artist.name)}">${fn:escapeXml(artist.name)}</a>
        </div>
    </c:forEach>
</c:forEach>

<div style="clear:both; padding-top:2em"></div>

<table class="music">
    <c:forEach items="${model.singleSongs}" var="song" varStatus="loopStatus">
        <%--@elvariable id="song" type="org.madsonic.domain.MediaFile"--%>
        <tr style="margin:0;padding:0;border:0">
            <c:import url="playButtons.jsp">
                <c:param name="id" value="${song.id}"/>
                <c:param name="video" value="${song.video and model.player.web}"/>
                <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode}"/>
                <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory)}"/>
                <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode}"/>
                <c:param name="starEnabled" value="true"/>
                <c:param name="starred" value="${not empty song.starredDate}"/>
                <c:param name="asTable" value="true"/>
            </c:import>

            <c:if test="${model.visibility.trackNumberVisible}">
                <td class="fit rightalign">
                    <span class="detail">${song.trackNumber}</span>
                </td>
            </c:if>

            <td class="truncate">
                <span class="songTitle" title="${fn:escapeXml(song.title)}">${fn:escapeXml(song.title)}</span>
            </td>

            <c:if test="${model.visibility.albumVisible}">
                <td class="truncate">
                    <span class="detail" title="${fn:escapeXml(song.albumName)}">${fn:escapeXml(song.albumName)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.artistVisible}">
                <td class="truncate">
                    <span class="detail" title="${fn:escapeXml(song.artist)}">${fn:escapeXml(song.artist)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.genreVisible}">
                <td class="fit rightalign">
                    <span class="detail">${fn:escapeXml(song.genre)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.yearVisible}">
                <td class="fit rightalign">
                    <span class="detail">${song.year}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.formatVisible}">
                <td class="fit rightalign">
                    <span class="detail">${fn:toLowerCase(song.format)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.fileSizeVisible}">
                <td class="fit rightalign">
                    <span class="detail"><sub:formatBytes bytes="${song.fileSize}"/></span>
                </td>
            </c:if>

            <c:if test="${model.visibility.durationVisible}">
                <td class="fit rightalign">
                    <span class="detail">${song.durationString}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.bitRateVisible}">
                <td class="fit rightalign">
                    <span class="detail">
                        <c:if test="${not empty song.bitRate}">
                            ${song.bitRate} Kbps ${song.variableBitRate ? "vbr" : ""}
                        </c:if>
                        <c:if test="${song.video and not empty song.width and not empty song.height}">
                            (${song.width}x${song.height})
                        </c:if>
                    </span>
                </td>
            </c:if>
        </tr>
    </c:forEach>
</table>

<div style="padding-top:2em"></div>

	
	</div> <!-- CONTENT -->
</div> <!-- CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_index").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:false,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>

</body> 
   
</html>