<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">	
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="internetRadio"/>
    <c:param name="toast" value="${model.reload}"/>
</c:import>
<br>
<form method="post" action="internetRadioSettings.view">
<table class="indent">
    <tr>
        <th><fmt:message key="internetradiosettings.name"/></th>
        <th><fmt:message key="internetradiosettings.streamurl"/></th>
        <th><fmt:message key="internetradiosettings.homepageurl"/></th>
        <th style="padding-left:1em"><fmt:message key="internetradiosettings.enabled"/></th>
        <th style="padding-left:1em"><fmt:message key="common.delete"/></th>
    </tr>

    <c:forEach items="${model.internetRadios}" var="radio">
        <tr>
            <td><input type="text" name="name[${radio.id}]" size="20" value="${radio.name}"/></td>
            <td><input type="text" name="streamUrl[${radio.id}]" size="60" value="${radio.streamUrl}"/></td>
            <td><input type="text" name="homepageUrl[${radio.id}]" size="40" value="${radio.homepageUrl}"/></td>
            <td align="center" style="padding-left:1em"><input type="checkbox" ${radio.enabled ? "checked" : ""} name="enabled[${radio.id}]" class="checkbox"/></td>
            <td align="center" style="padding-left:1em"><input type="checkbox" name="delete[${radio.id}]" class="checkbox"/></td>
        </tr>
    </c:forEach>

    <c:if test="${not empty model.internetRadios}">
    <tr>
        <th colspan="5" align="left" style="padding-top:1em"><fmt:message key="internetradiosettings.add"/></th>
    </tr>
    </c:if>

    <tr>
        <td><input type="text" name="name" size="20" placeholder="<fmt:message key="internetradiosettings.name"/>"/></td>
        <td><input type="text" name="streamUrl" size="60" placeholder="<fmt:message key="internetradiosettings.streamurl"/>"/></td>
        <td><input type="text" name="homepageUrl" size="40" placeholder="<fmt:message key="internetradiosettings.homepageurl"/>"/></td>
        <td align="center" style="padding-left:1em"><input name="enabled" checked type="checkbox" class="checkbox"/></td>
        <td/>
    </tr>

    <tr>
        <td style="padding-top:1.5em" colspan="5">
            <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
            <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">
        </td>
    </tr>
</table>
</form>


<c:if test="${not empty model.error}">
    <p class="warning"><fmt:message key="${model.error}"/></p>
</c:if>

<c:if test="${model.reload}">
    <script language="javascript" type="text/javascript">parent.frames.left.location.href="left.view?"</script>
</c:if>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
	(function($){
		$(window).load(function(){
			$("#content_main").mCustomScrollbar({
				axis:"y",
				scrollInertia:600, 		 /*scrolling inertia: integer (milliseconds)*/
				mouseWheel:true, 		 /*mousewheel support: boolean*/
				mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
				autoDraggerLength:true,  /*auto-adjust scrollbar dragger length: boolean*/
				autoHideScrollbar:true,  /*auto-hide scrollbar when idle*/        
				alwaysShowScrollbar:false,
				advanced:{      updateOnBrowserResize:true,            /*for layouts based on percentages: boolean*/
								updateOnContentResize:true, 		   /*for dynamic content: boolean*/
								autoExpandHorizontalScroll:false },    /*for horizontal scrolling: boolean*/
				scrollButtons:{ enable:true, 						   /*scroll buttons support: boolean*/
								scrollType:"continuous", 			   /*scroll buttons scrolling type: "continuous", "pixels"*/
								scrollSpeed:"auto", 				   /*scroll buttons continuous scrolling speed: integer, "auto"*/
								scrollAmount:40 },  				   /*scroll buttons pixels scroll amount: integer (pixels)*/
								theme:"${model.customScrollbarTheme}", /*scroll buttons theme*/
								scrollbarPosition:"inside"
			});
		});
	})(jQuery);
		
    $("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
    });		
    </script>
</c:if>


</body>
</html>