<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%--@elvariable id="command" type="org.madsonic.command.IconSettingsCommand"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>  	
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    <style>
    td {
        padding: 0 60px 10px 0;
    }
    </style>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="icon"/>
    <c:param name="toast" value="${command.toast}"/>
</c:import>
<br>
<form:form method="post" action="iconSettings.view" commandName="command">

  <table style="white-space:nowrap" width="30%" class="indent">
  <tr>
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='homeImage'/>"/>
    <form:checkbox path="showIconHome" id="showIconHome"/><label for="showIconHome">Home</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='indexImage'/>"/>
    <form:checkbox path="showIconIndex" id="showIconIndex"/><label for="showIconIndex">Index</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='artistImage'/>"/>
    <form:checkbox path="showIconArtist" id="showIconArtist"/><label for="showIconArtist">Artist</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='nowPlayingImage'/>"/>
	<form:checkbox path="showIconPlaying" id="showIconPlaying"/><label for="showIconPlaying">Playing</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='discoverImage'/>"/>
	<form:checkbox path="showIconCover" id="showIconCover"/><label for="showIconCover">Discover</label></td>
    
    </tr>
    <tr>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='bookmarkImage'/>"/>
	<form:checkbox path="showIconBookmark" id="showIconBookmark"/><label for="showIconBookmark">Bookmark</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='coverImage'/>"/>
	<form:checkbox path="showIconStarred" id="showIconStarred"/><label for="showIconStarred">Starred</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='starOnImage'/>"/>
	<form:checkbox path="showIconLoved" id="showIconLoved"/><label for="showIconLoved">Loved</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='genresImage'/>"/>
	<form:checkbox path="showIconGenre" id="showIconGenre" disabled="false"/><label for="showIconGenre">Genre</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='moodsImage'/>"/>
	<form:checkbox path="showIconMoods" id="showIconMoods"/><label for="showIconMoods">Moods</label></td>
    
    </tr>
    
    <tr>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='radioImage'/>"/>
	<form:checkbox path="showIconRadio" id="showIconRadio"/><label for="showIconRadio">Radio</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='randomImage'/>"/>
	<form:checkbox path="showIconRandom" id="showIconRandom"/><label for="showIconRandom">AutoDJ</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='playlistImage'/>"/>
	<form:checkbox path="showIconPlaylists" id="showIconPlaylists"/><label for="showIconPlaylists">Playlists</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='playlistImage'/>"/>
	<form:checkbox path="showIconPlaylistManage" id="showIconPlaylistManage"/><label for="showIconPlaylistManage">Playlist Manage</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='playlistImage'/>"/>
	<form:checkbox path="showIconPlaylistEditor" id="showIconPlaylistEditor"/><label for="showIconPlaylistEditor">Playlist Editor</label></td>
    
    </tr>
	
    <tr>
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='podcastImage'/>"/>
	<form:checkbox path="showIconPodcast" id="showIconPodcast"/><label for="showIconPodcast">Podcast</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='podcastImage'/>"/>
	<form:checkbox path="showIconPodcastManage" id="showIconPodcastManage"/><label for="showIconPodcastManage">Podcast Manage</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='statusImage'/>"/>
	<form:checkbox path="showIconStatus" id="showIconStatus" /><label for="showIconStatus">Status</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='chatImage'/>"/>
	<form:checkbox path="showIconSocial" id="showIconSocial"/><label for="showIconSocial">Broadcast</label></td>
    
	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='historyImage'/>"/>
	<form:checkbox path="showIconHistory" id="showIconHistory"/><label for="showIconHistory">History</label></td>
    
    </tr>
    <tr>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='chartImage'/>"/>
	<form:checkbox path="showIconStatistics" id="showIconStatistics"/><label for="showIconStatistics">Statistics</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='moreImage'/>"/>
	<form:checkbox path="showIconMore" id="showIconMore"/><label for="showIconMore">More</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='aboutImage'/>"/>
	<form:checkbox path="showIconAbout" id="showIconAbout"/><label for="showIconAbout">About</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='streamImage'/>"/>
	<form:checkbox path="showIconInternetRadio" id="showIconInternetRadio"/><label for="showIconInternetRadio">Internet Radio</label></td>

	<td><img class="hover-effect-kenburn" width="32" src="<spring:theme code='settingsImage'/>"/>
	<form:checkbox path="showIconSettings" id="showIconSettings" disabled="true"/><label for="showIconSettings">Settings</label></td>
</tr>
</table>

	<table>
        <tr><td colspan="2">&nbsp;</td></tr>	
		<tr>
            <td> </td>
            <td><form:checkbox path="showIconAdmins" id="showIconAdmins"/><label for="showIconAdmins">use Settings also for Admin Accounts</label></td>
        </tr>		
        <tr><td colspan="2">&nbsp;</td></tr>	
		
        <tr>
            <td colspan="2" style="padding-top:1.5em">
                <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
                <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">
            </td>
        </tr>

    </table>
</form:form>

<c:if test="${command.reloadNeeded}">
    <script language="javascript" type="text/javascript">
        parent.frames.left.location.href="left.view?";
    </script>
</c:if>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

</body>

</html>