<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body class="mainframe bgcolor1">

<h1>
    <img src="<spring:theme code="donateImage"/>" alt=""/>
    <fmt:message key="madsonic.title"/>
</h1>
<c:if test="${not empty command.path}">
    <madsonic:url value="main.view" var="backUrl">
        <madsonic:param name="path" value="${command.path}"/>
    </madsonic:url>
    <div class="back"><a href="${backUrl}">
        <fmt:message key="common.back"/>
    </a></div>
    <br/>
</c:if>

<div style="width:50em; max-width:50em">

<fmt:message key="madsonic.textbefore"><fmt:param value="${command.brand}"/></fmt:message>

<table cellpadding="10">
    <tr>
        <td>
            <table>
            
                <tr>
                <td>
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="6XRWXAYCBWVKU">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif" width="1" height="1">
                    </form>

                </td>
                </tr>
                <tr>
                    <td class="detail" style="text-align:center;"></td>
                </tr>
            </table>
        </td>
	</tr>
</table>
</div>
</body>
</html>