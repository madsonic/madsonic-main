<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%--@elvariable id="command" type="org.madsonic.command.PremiumSettingsCommand"--%>
<html>
<head>
<%@ include file="head.jsp" %>
<%@ include file="jquery.jsp" %>
<%@ include file="customScrollbar.jsp" %>
	
<link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">
<script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    
<style type="text/css">

.list-boxes li {
  width: 6%;
  font-size: 12px;
  float: left;
  height: 85px;
  padding: 10px;
  line-height: 1.4;
  text-align: center;
}
</style>
        
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="premium"/>
    <c:param name="restricted" value="${not command.user.adminRole}"/>
    <c:param name="toast" value="${command.toast}"/>
</c:import>


<h1>
    <img src="<spring:theme code="donateImage"/>" alt=""/>
    <span style="vertical-align: middle"><fmt:message key="premium.title"/></span>
</h1>
<div style="width:75em; max-width:75em">
    <div class="container-fluid"></div>
    <ul class="list-unstyled list-boxes" style="list-style-type:none;" id="iconbar">
        <li><a class="text-center" href="javascript:;"><i class="fa fa-music fa-3x"></i> <br>Radio</a></li>
        <li><a class="text-center" href="javascript:;"><i class="fa fa-camera fa-3x"></i> <br>Photos</a></li>
        <li><a class="text-center" href="javascript:;"><i class="fa fa-film fa-3x"></i> <br>Videos</a></li>
        <li><a class="text-center" href="javascript:;"><i class="fa fa-laptop fa-3x"></i> <br>DLNA</a></li>
        <li><a class="text-center" href="javascript:;"><i class="fa fa-play-circle-o fa-3x"></i> <br>Sonos</a></li>
      	<li><a class="text-center" href="javascript:;"><i class="fa fa-share fa-3x"></i> <br>Sharing</a></li>
        <li><a class="text-center" href="javascript:;"><i class="fa fa-book fa-3x"></i> <br>LDAP</a></li>
      	<li><a class="text-center" href="javascript:;"><i class="fa fa-database fa-3x"></i> <br>Sync</a></li>
      	<li><a class="text-center" href="javascript:;"><i class="fa fa-code fa-3x"></i> <br>REST</a></li>
      	<li><a class="text-center" href="javascript:;"><i class="fa fa-sign-in fa-3x"></i> <br>Social</a></li>
        <li><a class="text-center" href="javascript:;"><i class="fa fa-connectdevelop fa-3x"></i> <br>Node</a></li>
        <li><a class="text-center" href="javascript:;"><i class="fa fa-refresh fa-3x"></i> <br>Conversion</a></li>        
    </ul>
</div>

<c:if test="${not empty command.path}">
    <madsonic:url value="main.view" var="backUrl">
        <madsonic:param name="path" value="${command.path}"/>
    </madsonic:url>
    <div class="back"><a href="${backUrl}">
        <fmt:message key="common.back"/>
    </a></div>
</c:if>
 
<div style="width:50em; max-width:50em">

    <fmt:message key="premium.text"/>
    
    <fmt:formatDate value="${command.licenseInfo.licenseExpires}" dateStyle="long" var="expirationDate"/>

	<br>
    <c:if test="${command.licenseInfo.licenseValid}">
        <c:choose>
            <c:when test="${empty command.licenseInfo.licenseExpires}">
                <p >    <b class="info"><fmt:message key="premium.licensed"/></b></p>
                
                 
            </c:when>
            <c:otherwise>
                <p class="warning"><b><fmt:message key="premium.licensedexpires"><fmt:param value="${expirationDate}"/></fmt:message></b></p>
            </c:otherwise>
        </c:choose>
		
        <c:if test="${not command.forceChange and not command.submissionError}">
            <p class="warning">
                <fmt:message key="premium.licensedto"><fmt:param value="${command.licenseInfo.licenseEmail}"/></fmt:message>
            </p>
            <c:if test="${command.user.adminRole}">
                <div class="forward"><a href="premiumSettings.view?change"><fmt:message key="premium.forcechange"/></a></div>
            </c:if>
        </c:if>
    </c:if>

    <c:if test="${not command.licenseInfo.licenseValid}">
        <c:if test="${not empty command.licenseInfo.licenseExpires}">
            <p><b><fmt:message key="premium.licensedexpired"><fmt:param value="${expirationDate}"/></fmt:message></b></p>
        </c:if>
        <p class="forward" style="font-size:1.2em;margin-left: 1em"><b><a href="http://beta.madsonic.org/pages/premium.jsp" target="_blank">
            <fmt:message key="premium.getpremium"/>
            <c:if test="${command.licenseInfo.trialDaysLeft gt 0}">
                &ndash; <fmt:message key="common.trialdaysleft"><fmt:param value="${command.licenseInfo.trialDaysLeft}"/></fmt:message>
            </c:if>
        </a></b></p>

        <p><fmt:message key="premium.register"/></p>
    </c:if>

    <c:if test="${not command.licenseInfo.licenseValid or command.forceChange or command.submissionError}">
        <form:form commandName="command" method="post" action="premiumSettings.view">
            <form:hidden path="path"/>
            <table>
                <tr>
                    <td><fmt:message key="premium.register.email"/></td>
                    <td>
                        <form:input path="licenseInfo.licenseEmail" size="60"/>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="premium.register.license"/></td>
                    <td>
                        <form:input path="licenseCode" size="60"/>
                    </td>
                    <td><input type="submit" value="<fmt:message key="common.ok"/>"/></td>
                </tr>
                <tr>
                    <td/>
                    <td class="warning"><form:errors path="licenseCode"/></td>
                </tr>
            </table>
        </form:form>

        <p><fmt:message key="premium.resend"/></p>
    </c:if>

    </div> 

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">    

		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);

$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	
</body>
</html>

