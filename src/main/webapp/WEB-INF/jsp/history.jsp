<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
	
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/lovedTrackService.js"/>"></script>	    
    <script type="text/javascript" language="javascript">

    function toggleStar(mediaFileId, element) {
        starService.star(mediaFileId, !$(element).hasClass("fa-star"));
        $(element).toggleClass("fa-star fa-star-o starred");
    }    
   
    function toggleLoved(mediaFileId, element) {
        lovedTrackService.love(mediaFileId, !$(element).hasClass("fa-heart"));
        $(element).toggleClass("fa-heart fa-heart-o loved");
    }
    </script>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
    <img src="<spring:theme code="historyImage"/>" width="32" alt="">
    <fmt:message key="history.title"/>
</h1>

<h2>
    <c:forTokens items="artist album songs audiobook podcast video videoset" delims=" " var="cat" varStatus="loopStatus">
        <c:if test="${loopStatus.count > 1}">&nbsp;<img src="<spring:theme code="sepImage"/>">&nbsp;</c:if>
        <madsonic:url var="url" value="history.view">
            <madsonic:param name="listType" value="${cat}"/>
        </madsonic:url>
        <c:choose>
            <c:when test="${model.listType eq cat}">
                <span class="headerSelected"><fmt:message key="history.${cat}.title"/></span>
            </c:when>
            <c:otherwise>
                <a href="${url}"><fmt:message key="history.${cat}.title"/></a>
            </c:otherwise>
        </c:choose>
    </c:forTokens>
</h2>

<c:choose>
	<c:when test="${model.listType eq 'audio'}">
	<!-- audio --> 
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${model.listType eq 'audiobook'}">
	<!-- audiobook -->
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${model.listType eq 'podcast'}">
	<!-- podcast --> 
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${model.listType eq 'video'}">
	<!-- video --> 
	</c:when>
</c:choose>

<c:if test="${not empty model.songs}">
	<h2></h2>
	
	<table style="padding-top:2em">
		<tr >
		</tr>
		<tr>
			<madsonic:url value="history.view" var="previousUrl">
				<madsonic:param name="listOffset" value="${model.listOffset - model.listSize}"/>
				<madsonic:param name="listType" value="${model.listType}"/>
				</madsonic:url>
			<madsonic:url value="history.view" var="nextUrl">
				<madsonic:param name="listOffset" value="${model.listOffset + model.listSize}"/>
				<madsonic:param name="listType" value="${model.listType}"/>
				</madsonic:url>

			<c:if test="${model.listOffset eq 0}">
				<c:set var="htmlclass" value="disabled"/>
			</c:if>
			
			<td style="padding-right:1.5em"><i class="fa fa-chevron-left icon control"></i>&nbsp;<a href="${previousUrl}" class="${htmlclass}"><fmt:message key="common.previous"/></a></td>
			<td style="padding-right:1.5em"><fmt:message key="history.title"><fmt:param value="${model.listOffset + 1}"/><fmt:param value="${model.listOffset + model.listSize}"/></fmt:message></td>

			<c:if test="${fn:length(model.songs) eq model.listSize}">
				<td><i class="fa fa-chevron-right icon control"></i>&nbsp;<a href="${nextUrl}"><fmt:message key="common.next"/></a></td>
			</c:if>
			
		</tr>
	</table>
	<br>
	<table class="music" style="border-collapse:collapse">
		<c:forEach items="${model.songs}" var="song" varStatus="loopStatus">

			<madsonic:url value="/main.view" var="parentMainUrl">
				<madsonic:param name="path" value="${song.parentPath}"/>
			</madsonic:url>

			<madsonic:url value="/main.view" var="currentMainUrl">
				<madsonic:param name="path" value="${song.path}"/>
			</madsonic:url>			
			
			<c:choose>
				<c:when test="${model.listType eq 'artist' or model.listType eq 'album' or model.listType eq 'videoset'}">
						<c:set var="mainUrl" value="${currentMainUrl}"/>
				</c:when>
				<c:otherwise>
						<c:set var="mainUrl" value="${parentMainUrl}"/>
				</c:otherwise>
			</c:choose>

			<tr>
               <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"}>
				<c:import url="playAddDownload.jsp">
					<c:param name="id" value="${song.id}"/>
					<c:param name="playEnabled" value="${model.user.streamRole and not model.partyModeEnabled}"/>
					<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyModeEnabled or not song.directory)}"/>
					<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyModeEnabled}"/>
					<c:param name="starEnabled" value="true"/>
					<c:param name="starred" value="${not empty song.starredDate}"/>
					<c:param name="video" value="${model.listType eq 'video'}"/>
					<c:param name="asTable" value="false"/>
				</c:import>
				</td>
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "fit"} style="padding-left:0.25em;padding-right:1.25em">
					<str:truncateNicely upper="40">${song.title}</str:truncateNicely>
				</td>
				
				<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
				<a href="${mainUrl}"><span class="detail"><str:truncateNicely upper="40">${song.albumName}</str:truncateNicely></span></a>
				</td>

				<c:if test="${model.listType ne 'artist'}">
				<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
				<c:if test="${model.listType eq 'album'}"><a href="${parentMainUrl}"></c:if>
				<span class="detail"><str:truncateNicely upper="40">${song.artist}</str:truncateNicely></span>
				<c:if test="${model.listType eq 'album'}"></a></c:if>
				</td>
				</c:if>
				
				
				<c:if test="${model.listType ne 'album'}">				
				<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
					<span class="detail">${fn:substring(song.lastPlayed, 0, 16)}</span>
				</td>
				</c:if>				
				<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.15em">
					<span class="detail">${fn:substring(song.firstScanned, 0, 19)}</span>
				</td>
				</tr>
				
		</c:forEach>
	</table>

</c:if>


</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:880, /*scrolling inertia: integer (milliseconds)*/
  		    scrollEasing:"easeOutCubic", /*scrolling easing: string*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$(".content_main").resize(function(e){
	$(".content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</body>
</html>