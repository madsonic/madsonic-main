<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>      
</head>


<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="transcoding"/>
    <c:param name="toast" value="${model.toast}"/>
</c:import>

<form method="post" action="transcodingSettings.view">
<table class="indent">
    <tr>
        <th><fmt:message key="transcodingsettings.name"/></th>
        <th><fmt:message key="transcodingsettings.sourceformat"/></th>
        <th><fmt:message key="transcodingsettings.targetformat"/></th>
        <th><fmt:message key="transcodingsettings.step1"/></th>
        <th><fmt:message key="transcodingsettings.step2"/></th>
        <th style="padding-left:1em"><fmt:message key="common.delete"/></th>
    </tr>

    <c:forEach items="${model.transcodings}" var="transcoding">
        <tr>
            <td><input style="font-family:monospace" type="text" name="name[${transcoding.id}]" size="15" value="${transcoding.name}"/></td>
            <td><input style="font-family:monospace" type="text" name="sourceFormats[${transcoding.id}]" size="50" value="${transcoding.sourceFormats}"/></td>
            <td><input style="font-family:monospace" type="text" name="targetFormat[${transcoding.id}]" size="10" value="${transcoding.targetFormat}"/></td>
            <td><input style="font-family:monospace" type="text" name="step1[${transcoding.id}]" size="60" value="${transcoding.step1}"/></td>
            <td><input style="font-family:monospace" type="text" name="step2[${transcoding.id}]" size="35" value="${transcoding.step2}"/></td>
            <td align="center" style="padding-left:1em"><input type="checkbox" name="delete[${transcoding.id}]" class="checkbox"/></td>
        </tr>
    </c:forEach>

    <tr>
        <th colspan="6" align="left" style="padding-top:1em"><fmt:message key="transcodingsettings.add"/></th>
    </tr>

    <tr>
        <td><input style="font-family:monospace" type="text" name="name" size="10" placeholder="<fmt:message key="transcodingsettings.name"/>" value="${model.newTranscoding.name}"/></td>
        <td><input style="font-family:monospace" type="text" name="sourceFormats" size="50" placeholder="<fmt:message key="transcodingsettings.sourceformat"/>" value="${model.newTranscoding.sourceFormats}"/></td>
        <td><input style="font-family:monospace" type="text" name="targetFormat" size="10" placeholder="<fmt:message key="transcodingsettings.targetformat"/>" value="${model.newTranscoding.targetFormat}"/></td>
        <td><input style="font-family:monospace" type="text" name="step1" size="60" placeholder="<fmt:message key="transcodingsettings.step1"/>" value="${model.newTranscoding.step1}"/></td>
        <td><input style="font-family:monospace" type="text" name="step2" size="35" placeholder="<fmt:message key="transcodingsettings.step2"/>" value="${model.newTranscoding.step2}"/></td>
        <td/>
    </tr>

    <tr>
        <td colspan="6" style="padding-top:0.1em">
            <input type="checkbox" id="defaultActive" name="defaultActive" class="checkbox" checked/>
            <label for="defaultActive"><fmt:message key="transcodingsettings.defaultactive"/></label>
        </td>
    </tr>
</table>


    <table style="white-space:nowrap" class="indent">
        <tr>
            <td style="font-weight: bold;">
                transcode path</td>
            <td>
                <input style="font-family:monospace" type="text" name="transcodeDirectory" size="149" value="${model.transcodeDirectory}"/>
            </td>
        </tr> 
    
        <tr>
            <td style="font-weight: bold;">
                <fmt:message key="advancedsettings.downsamplecommand"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="downsamplecommand"/></c:import>
            </td>
            <td>
                <input style="font-family:monospace" type="text" name="downsampleCommand" size="149" value="${model.downsampleCommand}"/>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <fmt:message key="advancedsettings.hlscommand"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="hlscommand"/></c:import>
            </td>
            <td>
                <input style="font-family:monospace" type="text" name="hlsCommand" size="149" value="${model.hlsCommand}"/>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <fmt:message key="advancedsettings.dashcommand"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="dashcommand"/></c:import>
            </td>
            <td>
                <input style="font-family:monospace" type="text" name="dashCommand" size="149" value="${model.dashCommand}"/>
            </td>
        </tr>
    </table>


    <p style="padding-top:0.75em">
        <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
        <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'" style="margin-right:1.3em">
        <a href="http://beta.madsonic.org/pages/transcoding.jsp" target="_blank"><fmt:message key="transcodingsettings.recommended"/></a>
    </p>

</form>

<c:if test="${not empty model.error}">
    <p class="warning"><fmt:message key="${model.error}"/></p>
</c:if>

<div>
    <fmt:message key="transcodingsettings.info"><fmt:param value="${model.transcodeDirectory}"/><fmt:param value="${model.brand}"/></fmt:message>
</div>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:950, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
        
$("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>

</body>

</html>
