<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <%@ include file="head.jsp" %>
    <script type="text/javascript">
        if (window != window.top) {
            top.location.href = location.href;
        }
    </script>
    <style>
    body.splash canvas {
        position: fixed;
        top: 0;
        left: 0;
        width: auto;
        height: auto;
        z-index: -999;
        opacity: 0.38;
    }
    
    #container {
        display: inline;
        margin-top: -70px;
        width: 100%;
        height: 100%;
    }
    .centered {
        display: inline-block; vertical-align: middle; width: 60%;
    }

    #index-content div.centered {
        display: block;
        margin: 0 auto;
    }

    .bgcolor2 {
        opacity: 0.88;
    }
    div#poweredby {
     position: absolute;
     bottom: 0;
     right: 0;
     display: block;
    padding: 5px;
    }
    </style>
</head>

<body onLoad="document.getElementById('j_username').focus();" class="mainframe splash"> 

<div id="startup">
  <div id="container">
    <center>
        <div class="centered">
            <form action="<c:url value="/j_spring_security_check"/>" method="POST">
                    <div class="loginsplash bgcolor2" align="center" style="min-width:450px;max-width:600px;border:1px solid black; padding:10px 50px 20px 50px; margin-top:160px">
                    <div style="margin-bottom:1em;max-width:50em;text-align:left;"><madsonic:wiki text="${model.loginMessage}"/></div> 
                    <table>
                    
                    <tr>
                    <td colspan="1" align="left" style="padding-bottom:15px">
					
					<c:if test="${!model.customlogo}">
						<img src="<spring:theme code="logoImage"/>" width="200" alt="">
					</c:if>
					
					<c:if test="${model.customlogo}">
					<img id="logo-icon" src="coverArt.view?logo=1" width="200" alt=""> 
					</c:if>

                    </td>
                    </tr>
                    
                    <tr>
                    <td align="right" style="padding-bottom:5px;padding-right:10px"><fmt:message key="login.username"/></td>
                    <td align="left"  style="padding-bottom:5px;padding-left: 15px;"><input type="text" id="j_username" name="j_username" placeholder="Username ..." style="width:15em;" tabindex="1"></td>
                    </tr>
                    <tr>
                    <td align="right" style="padding-bottom:5px;padding-right:10px;"><fmt:message key="login.password"/></td>
                    <td align="left"  style="padding-bottom:5px;padding-left: 15px;"><input type="password" id="j_password" name="j_password" placeholder="Password ..." style="width:15em" tabindex="2"></td>
                    </tr>
          
                    <c:if test="${model.inactive}">
                    <tr>
                    <td align="right"></td>
                    <td style="padding-bottom: 5px;padding-top: 8px;">
                    <b class="warning" style="margin-bottom: 10px;padding-left: 22px;"><fmt:message key="login.inactive"/></b>
                    </td>
                    </tr>
                    </c:if>

          
                    <c:if test="${model.error}">
                    <tr>
                    <td align="right"></td>
                    <td style="padding-bottom: 5px;padding-top: 8px;">
                    <b class="warning" style="margin-bottom: 10px;padding-left: 22px;"><fmt:message key="login.error"/></b>
                    </td>
                    </tr>
                    </c:if>
                    
                    <c:if test="${model.error}">                    
                    <tr> 
                    <td align="right" ></td>
                    </tr>

                    <tr>
                    <td align="right" style="padding-bottom: 10px;"><a href="recover.view"><input style="width:90px;" name="recover" type="button" value="Recover" tabindex="6"></a></td>
                    <td align="left" style="padding-bottom: 10px;padding-left: 25px;" class="detail"><fmt:message key="login.recover"/></td>
                    </tr>
                    </c:if>                    
                    
                    <tr>
                    <td align="right" style="padding-bottom: 6px;">
                    <input name="submit" type="submit" style="width:90px;" value="<fmt:message key="login.login"/>" tabindex="5">
                    </td>
                    <td align="left" style="padding-left: 20px;" class="detail"> 
                    <input type="checkbox" name="_spring_security_remember_me" id="remember" class="checkbox" style="vertical-align: middle;" tabindex="3">
                    <label for="remember"><fmt:message key="login.remember"/></label>                    
                    </td>
                    </tr>

                    <tr>
                    <td align="right"></td>
                    </tr>
                    
                    <c:if test="${model.signup}">
                    <tr>
                    <td align="right"style="padding-bottom: 10px;"><a href="signup.view"><input class="signup" name="signup" type="button" style="width:90px;" value="SignUp" tabindex="4"></a>
                    </td>
                    </tr>
                    </c:if>
                    
                    <c:if test="${model.logout}">
                    <tr><td colspan="2" style="padding-top:10px"><b><fmt:message key="login.logout"/></b></td></tr>
                    </c:if>
                    

                    
                    </table>
                    <c:if test="${model.insecure}">
                    <p style="margin-top: 20px;"><b class="warning"><fmt:message key="login.insecure"><fmt:param value="${model.brand}"/></fmt:message></b></p>
                    </c:if>
                </div>
            </form>
        </div>
    </center>
  </div>
</div>
<div id="poweredby">
<a href="http://www.madsonic.org">powered by Madsonic</a></div> 
</div>
</body>
</html>
