<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%--@elvariable id="command" type="org.madsonic.command.MusicFolderSettingsCommand"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    <c:choose>
        <c:when test="${customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose> 

    <link rel="stylesheet" media="all" type="text/css" href="<c:url value="/script/timepicker/jquery-ui-timepicker-addon.css"/>"/>
    <script type="text/javascript" src="<c:url value="/script/timepicker/jquery-ui-timepicker-addon.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/timepicker/jquery-ui-sliderAccess.js"/>"></script>
    
    <script type="text/javascript">
    
        function init() {
            $("#newMusicFolderName").attr("placeholder", "<fmt:message key="musicfoldersettings.name"/>");
            $("#newMusicFolderPath").attr("placeholder", "<fmt:message key="musicfoldersettings.path"/>");
            $("#newMusicFolderTimer").attr("placeholder", "Scan Timer");
            
            <c:if test="${command.reload}">
	            parent.frames.leftPanel.location.href="leftPanel.view?";
            </c:if>
        }
        
    </script>
</head>

<body class="mainframe bgcolor1" onload="init()"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="musicFolder"/>
    <c:param name="toast" value="${command.reload}"/>
</c:import>
<br>
<form:form commandName="command" action="musicFolderSettings.view" method="post">

<table class="indent">
    <tr>
        <th><fmt:message key="musicfoldersettings.name"/></th>
        <th><fmt:message key="musicfoldersettings.path"/></th>
        <th style="padding-left:1em">Index</th>
        <th style="padding-left:1em">Type</th>
        <th style="padding-left:1em">Group</th>        
        <th style="padding-left:1em">Scan</th>        
        <th style="padding-left:1em">Interval</th>        
        <th style="padding-left:1em"><fmt:message key="musicfoldersettings.enabled"/></th>
        <th style="padding-left:1em"><fmt:message key="common.delete"/></th>
        <th></th>
    </tr>

    <c:forEach items="${command.musicFolders}" var="folder" varStatus="loopStatus">
        <tr>
            <td><form:input path="musicFolders[${loopStatus.count-1}].name" size="25"/></td>
            <td><form:input path="musicFolders[${loopStatus.count-1}].path" size="45"/></td>
            <td><form:select path="musicFolders[${loopStatus.count-1}].index" size="1">
                <form:option value="1" label="Index 1 (all)"/>
                <form:option value="2" label="Index 2 "/>
                <form:option value="3" label="Index 3 "/>
                <form:option value="4" label="Index 4 "/>
                </form:select></td>
            <td><form:select path="musicFolders[${loopStatus.count-1}].type" size="1">
                <form:option value="1" label="Music - Artists"/>
                <form:option value="2" label="Music - Various Artists"/>
                <form:option value="3" label="Music - Compilation"/>
                <form:option value="4" label="Music - Podcasts"/>
                <form:option value="5" label="Music - Others"/>
                <form:option value="6" label="Music - Flat"/>
                <form:option value="7" label="Video - Series"/>
                <form:option value="8" label="Video - Movies"/>
                <form:option value="9" label="Video - Web"/>
                <form:option value="10" label="Video - TV"/>
                <form:option value="11" label="Image - Collection"/>
                <form:option value="12" label="Image - Flat"/>
                <form:option value="13" label="Others"/>
                </form:select></td>
                
            <td><form:select path="musicFolders[${loopStatus.count-1}].group" size="1">
                <form:option value="0" label="dont group"/>
                <form:option value="1" label="Music"/>
                <form:option value="2" label="Video"/>
                <form:option value="3" label="Movies"/>
                <form:option value="4" label="Series"/>
                <form:option value="5" label="Images"/>
                <form:option value="6" label="TV"/>
                </form:select></td>

            <td>                
            <fmt:formatDate value="${folder.timer}" type="both" pattern="yyyy/MM/dd HH:mm" var="theFormattedDate" />
            <form:input path="musicFolders[${loopStatus.count-1}].timer" id="mf${loopStatus.count-1}" value="${theFormattedDate}" size="16"/>
            </td> 
            <td>                
            <form:select path="musicFolders[${loopStatus.count-1}].interval">
                <fmt:message key="musicfoldersettings.interval.never" var="never"/>
                <fmt:message key="musicfoldersettings.interval.one" var="one"/>
                <form:option value="-1" label="${never}"/>
                <form:option value="120" label="2 Min."/> 
                <form:option value="300" label="5 Min."/> 
                <form:option value="600" label="10 Min."/>
                <form:option value="1800" label="30 Min."/>
                <form:option value="3600" label="1 Hour"/>
                <form:option value="10800" label="3 Hour"/>
                <form:option value="21600" label="6 Hour"/>
                <form:option value="43200" label="12 Hour"/>
                <form:option value="86400" label="1 Day"/>
                <form:option value="604800" label="1 Week"/>
                <form:option value="2419200" label="1 Month"/>
                <form:option value="7257600" label="3 Months"/>
                <form:option value="14515200" label="6 Months"/>
                <form:option value="29030400" label="1 Year"/>
            </form:select>
            </td> 
            
            <td align="center" style="padding-left:1em"><form:checkbox path="musicFolders[${loopStatus.count-1}].enabled" cssClass="checkbox"/></td>
            <td align="center" style="padding-left:1em"><form:checkbox path="musicFolders[${loopStatus.count-1}].delete" cssClass="checkbox"/></td>
            
            <td><c:if test="${not folder.existing}"><span class="warning"><fmt:message key="musicfoldersettings.notfound"/></span></c:if></td>
        </tr>
    </c:forEach>
    <tr>
        <td>&nbsp;</td>
    </tr>

    <c:if test="${not empty command.musicFolders}">
    <tr>
        <th colspan="6" align="left" style="padding-top:1em"><fmt:message key="musicfoldersettings.add"/></th>
    </tr>
    </c:if>

    <tr>
        <td><form:input id="newMusicFolderName" path="newMusicFolder.name" size="25"/></td>
        <td><form:input id="newMusicFolderPath" path="newMusicFolder.path" size="45"/></td>
        <td><form:select path="newMusicFolder.index" size="1">
        <form:option value="1" label="Index 1 (all)"/>
        <form:option value="2" label="Index 2 "/>
        <form:option value="3" label="Index 3 "/>
        <form:option value="4" label="Index 4 "/>
        </form:select></td>    
            <td><form:select path="newMusicFolder.type" size="1">
                <form:option value="1" label="Music - Artists"/>
                <form:option value="2" label="Music - Various Artists"/>
                <form:option value="3" label="Music - Compilation"/>
                <form:option value="4" label="Music - Podcasts"/>
                <form:option value="5" label="Music - Others"/>
                <form:option value="6" label="Music - Flat"/>
                <form:option value="7" label="Video - Series"/>
                <form:option value="8" label="Video - Movies"/>
                <form:option value="9" label="Video - Web"/>
                <form:option value="10" label="Video - TV"/>
                <form:option value="11" label="Image - Collection"/>
                <form:option value="12" label="Image - Flat"/>
                <form:option value="13" label="Others"/>
        </form:select></td>
        
            <td><form:select path="newMusicFolder.group" size="1">
                <form:option value="1" label="Music"/>
                <form:option value="2" label="Video"/>
                <form:option value="3" label="Movies"/>
                <form:option value="4" label="Series"/>
                <form:option value="5" label="Images"/>
                <form:option value="6" label="TV"/>
                <form:option value="0" label="dont group"/>
                </form:select></td>        

            <td>
                <c:set var="now" value="<%=new java.util.Date(1451606400000L)%>" />
                <fmt:formatDate value="${now}"  type="both" pattern="yyyy/MM/dd HH:mm" var="theFormattedDate" />
                <form:input path="newMusicFolder.timer" id="newMusicFolderTimer" value="${theFormattedDate}" size="16"/>
            </td>
            
            <td><form:select path="newMusicFolder.interval">
                <fmt:message key="musicfoldersettings.interval.never" var="never"/>
                <fmt:message key="musicfoldersettings.interval.one" var="one"/>
                <form:option value="-1" label="${never}"/>
                <form:option value="120" label="2 Min."/> 
                <form:option value="300" label="5 Min."/> 
                <form:option value="600" label="10 Min."/>
                <form:option value="1800" label="30 Min."/>
                <form:option value="3600" label="1 Hour"/>
                <form:option value="10800" label="3 Hour"/>
                <form:option value="21600" label="6 Hour"/>
                <form:option value="43200" label="12 Hour"/>
                <form:option value="86400" label="1 Day"/>
                <form:option value="604800" selected="true" label="1 Week"/>
                <form:option value="2419200" label="1 Month"/>
                <form:option value="7257600" label="3 Months"/>
                <form:option value="14515200" label="6 Months"/>
                <form:option value="29030400" label="1 Year"/>
            </form:select>
            </td>   
        <td align="center" style="padding-left:1em"><form:checkbox path="newMusicFolder.enabled" cssClass="checkbox"/></td>
        <td></td>
    </tr>

</table>

    <div style="padding-top: 1.2em;padding-bottom: 1.3em;padding-right: 0.5em;padding-left: 0.3em;">
        <span style="white-space: nowrap">
            <fmt:message key="musicfoldersettings.scan"/>
            <form:select path="interval">
                <fmt:message key="musicfoldersettings.interval.never" var="never"/>
                <fmt:message key="musicfoldersettings.interval.one" var="one"/>
                <form:option value="-1" label="${never}"/>
                <form:option value="1" label="${one}"/>

                <c:forTokens items="2 3 7 14 30 60 90" delims=" " var="interval">
                    <fmt:message key="musicfoldersettings.interval.many" var="many"><fmt:param value="${interval}"/></fmt:message>
                    <form:option value="${interval}" label="${many}"/>
                </c:forTokens>
            </form:select>
            <form:select path="hour">
                <c:forEach begin="0" end="23" var="hour">
                    <fmt:message key="musicfoldersettings.hour" var="hourLabel"><fmt:param value="${hour}"/></fmt:message>
                    <form:option value="${hour}" label="${hourLabel}"/>
                </c:forEach>
            </form:select>
        </span>
    </div> 

    <br>
    <p>
        <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
        <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">
    </p>    
    <br>
    <p class="forward"><a href="musicFolderSettings.view?scanNow"><fmt:message key="musicfoldersettings.scannow"/></a></p>
    <br>
    <c:if test="${command.scanning}">
    <p class="warning" style="width:60%"><i class="fa fa-info-circle fa-lg fa-fw icon" style="color: #FFC107"></i> <i class="fa fa-refresh fa-spin"></i> <fmt:message key="musicfoldersettings.nowscanning"/></p><br>
    </c:if>
    <div style="padding-bottom: 0.3em;">
        <form:checkbox path="fastCache" cssClass="checkbox" id="fastCache"/>
        <form:label path="fastCache"><fmt:message key="musicfoldersettings.fastcache"/></form:label>
    </div>
    <p class="detail" style="width:60%;white-space:normal;margin-left:1.8em">
        <fmt:message key="musicfoldersettings.fastcache.description"/>
    </p>
    <div style="padding-bottom: 0.3em;">
        <form:checkbox path="organizeByGenreMap" cssClass="checkbox" id="organizeByGenreMap"/>
        <form:label path="organizeByGenreMap"><fmt:message key="musicfoldersettings.organizebygenremap"/></form:label>
    </div>
    <p class="detail" style="width:60%;white-space:normal;">
        <fmt:message key="musicfoldersettings.organizebygenremap.description"/>
    </p>
    <!--
    <div>
        <form:checkbox path="organizeByFolderStructure" cssClass="checkbox"  disabled="true" id="organizeByFolderStructure"/>
        <form:label path="organizeByFolderStructure"><fmt:message key="musicfoldersettings.organizebyfolderstructure"/></form:label>
    </div>
    <p class="detail" style="width:60%;white-space:normal;">
        <fmt:message key="musicfoldersettings.organizebyfolderstructure.description"/>
    </p>
    -->
    <br>
</form:form>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${customScrollbar}">
<script type="text/javascript">    

        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>    

<script type="text/javascript">
    var minDate = new Date();
    minDate.setHours(minDate.getHours()-24);
    $('#newMusicFolderTimer').datetimepicker({minDate: new Date(), dateFormat: "yy/mm/dd", timeFormat: "HH:mm", numberOfMonths: 2, minuteGrid: 10, stepMinute: 10, addSliderAccess: true, sliderAccessArgs: { touchonly: false } });
    <c:forEach items="${command.musicFolders}" var="folder" varStatus="loopStatus">
      $('#mf${loopStatus.count-1}').datetimepicker({ minDate: minDate, dateFormat: "yy/mm/dd", timeFormat: "HH:mm", numberOfMonths: 2, minuteGrid: 10, stepMinute: 10, addSliderAccess: true, sliderAccessArgs: { touchonly: false } });
    </c:forEach>
</script> 
</body>
</html>