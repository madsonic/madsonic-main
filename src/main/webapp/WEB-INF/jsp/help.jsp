<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
    
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->
    
<c:choose>
    <c:when test="${empty model.buildDate}">
        <fmt:message key="common.unknown" var="buildDateString"/>
    </c:when>
    <c:otherwise>
        <fmt:formatDate value="${model.buildDate}" dateStyle="short" pattern="yyyyMMdd" var="buildDateString"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${empty model.buildTime}">
        <fmt:message key="common.unknown" var="buildTimeString"/>
    </c:when>
    <c:otherwise>
        <fmt:formatDate value="${model.buildTime}" type="time" pattern="hhmm" var="buildTimeString"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${empty model.localVersion}">
        <fmt:message key="common.unknown" var="versionString"/>
    </c:when>
    <c:otherwise>
        <c:set var="versionString" value="${model.localVersion}"/>
    </c:otherwise>
</c:choose>

<h1>
    <img src="<spring:theme code="iconImage"/>" width="32" alt="">
    <fmt:message key="help.title"><fmt:param value="${model.brand}"/></fmt:message>
</h1>

<c:if test="${model.newVersionAvailable}">
	
    <p class="warning"><img src="icons/default/new.png" width="40" height="40" title="New Version"/> 
    <fmt:message key="help.upgrade"><fmt:param value="${model.brand}"/><fmt:param value="${model.latestVersion}"/></fmt:message>
	</p>
</c:if>

<table style="min-width: 350px;max-width: 860px;width: 100%;" class="ruleTable indent">
    <tr><td class="ruleTableHeader"><fmt:message key="help.premium.title"/></td><td class="ruleTableCell status">Madsonic Premium Edition, for personal use only as described below.</td></tr>
    <c:set var="licenseInfo" value="${model.licenseInfo}"/>
    <tr><td class="ruleTableHeader"></td><td class="ruleTableCell warning">
    <fmt:formatDate value="${model.licenseInfo.licenseExpires}" dateStyle="long" var="expirationDate"/>
    <c:if test="${model.licenseInfo.licenseValid}">
        <c:choose>
            <c:when test="${empty model.licenseInfo.licenseExpires}">
            <p class="status"><b><fmt:message key="premium.licensed"/></b></p>
            <p class="warning"><fmt:message key="premium.licensedto"><fmt:param value="${model.licenseInfo.licenseEmail}"/></fmt:message></p>            
            </c:when>
            <c:otherwise>
                <c:if test="${not model.user.adminRole}">
                <p class="status"><b><fmt:message key="premium.licensedexpires"><fmt:param value="${expirationDate}"/> </fmt:message></b></p>       
                </c:if>
                <c:if test="${model.user.adminRole}">
                <p class="status"><fmt:message key="premium.licenseduntil"><fmt:param value="${model.licenseInfo.licenseEmail}"/><fmt:param value="${expirationDate}"/> </fmt:message>       
                </c:if>
                <c:if test="${model.user.adminRole}">
                    (<fmt:message key="premium.change"/>)
                </c:if>
                </p>
            </c:otherwise>
        </c:choose>
    </c:if>
   <fmt:message key="premium.text"/>
    
    <c:if test="${not model.licenseInfo.licenseValid}">
        <c:if test="${not empty model.licenseInfo.licenseExpires}">
            <p><b><fmt:message key="premium.licensedexpired"><fmt:param value="${expirationDate}"/></fmt:message></b></p>
        </c:if>
        <p class="forward" style="font-size:1.2em;margin-left: 1em"><b><a href="http://beta.madsonic.org/pages/premium.jsp" target="_blank">
            <fmt:message key="premium.getpremium"/>
            <c:if test="${model.licenseInfo.trialDaysLeft gt 0}">
                &ndash; <fmt:message key="common.trialdaysleft"><fmt:param value="${model.licenseInfo.trialDaysLeft}"/></fmt:message>
            </c:if>
        </a></b></p>
    </c:if>

    </td>
    </tr>
    
    <tr><td class="ruleTableHeader"><fmt:message key="help.version.title"/></td><td class="ruleTableCell">MADSONIC <b>${versionString}.${buildDateString}.${buildTimeString} </b> - IP address <b>[${model.localIp}]</b></td></tr>
    <tr><td class="ruleTableHeader">REST</td><td class="ruleTableCell">MADSONIC <b>REST API v${model.madsonicRestInfo}</b>, SUBSONIC <b>REST API v${model.subsonicRestInfo}</b></td></tr>
    <tr><td class="ruleTableHeader"><fmt:message key="help.server.title"/></td><td class="ruleTableCell">${model.serverInfo} (<madsonic:formatBytes bytes="${model.usedMemory}"/> / <madsonic:formatBytes bytes="${model.totalMemory}"/>)</td></tr>
    <tr><td class="ruleTableHeader"></td><td class="ruleTableCell">${model.cacheInfo}</td></tr>
    <tr><td class="ruleTableHeader"></td><td class="ruleTableCell">Spring.Framework v${model.springInfo}, Spring.Security v${model.databaseInfo}, HyperSQL DataBase v2.3.4</td></tr>
	<tr><td class="ruleTableHeader">Transcoder</td>
	<c:choose>
    <c:when test="${!model.transcoderFound}">
		<td class="ruleTableCell error">${model.ffmpegInfo}</td>
    </c:when>
    <c:otherwise>
		<td class="ruleTableCell info">${model.ffmpegInfo}</td>
    </c:otherwise>
	</c:choose>
	</tr>
    <tr><td class="ruleTableHeader">location</td><td class="ruleTableCell"><b>${model.transcoderPath}</b></td></tr>
    
    <tr><td class="ruleTableHeader"><fmt:message key="help.license.title"/></td>
    <td class="ruleTableCell"><a href="http://www.gnu.org/copyleft/gpl.html" target="_blank">
    <img style="float:right;margin-left: 10px" alt="GPL 3.0" src="<c:url value="/icons/default/gpl.png"/>"></a>
	<fmt:message key="help.license.text"><fmt:param value="${model.brand}"/></fmt:message></td></tr>
    
    <tr><td class="ruleTableHeader"><fmt:message key="help.contact.title"/></td><td class="ruleTableCell">
    <a href="<c:url value="/madsonic.view"/>"><img style="float:right;margin-right: 20px" src="<spring:theme code="paypalImage"/>" alt=""></a>
    <fmt:message key="help.contactmadsonic.text"><fmt:param value="${model.brand}"/></fmt:message>
    </td>
    </tr>
    <tr><td class="ruleTableHeader"><fmt:message key="help.madsonic_homepage.title"/></td>
    <td class="ruleTableCell"><a target="_blank" href="http://www.madsonic.org/">www.madsonic.org</a></td></tr>
    <tr><td class="ruleTableHeader"><fmt:message key="help.madsonic_forum.title"/></td><td class="ruleTableCell"><a target="_blank" href="http://forum.madsonic.org/">forum.madsonic.org</a> </td></tr>
	</table>
    
</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:true, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:false,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>    
    
</body></html>