<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%--@elvariable id="command" type="org.madsonic.command.AdminSettingsCommand"--%>
<html>
<head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">
    
    <c:choose>
        <c:when test="${customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-1.4.2.js"/>"></script>  
    <script type='text/javascript' src="<c:url value="/script/jquery.dropdownReplacement-0.5.3.js"/>"></script>
    
    <link rel="stylesheet" href="<c:url value="/style/jquery.dropdownReplacement.css"/>" type="text/css">    
   
<style>
.artisthub {
    background-size: cover;
}
</style>
    
</head>

<body class="mainframe bgcolor1"> 

<div id="content_main" class="content_main"> 
  
<div id="container" class="container">

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<script type="text/javascript" src="<c:url value="/script/jquery.easing.min.js"/>"></script> 
<script type="text/javascript" src="<c:url value="/script/jquery.easypiechart.min.js"/>"></script> 

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="admin"/>
    <c:param name="toast" value="${model.toast}"/>
</c:import>
<br>

<div style="width:470px;float:left;min-height:160px;" class="artisthub" > 

<span class="chart" data-percent="${statAudioPrecent}">
    <span class="easypievalue">${statAudio}</span> <span class="cap">${statAudioCap}</span>
    <br><br><span class="label">Audio</span>
</span>

<span class="chart" data-percent="${statVideoPrecent}">
    <span class="easypievalue">${statVideo}</span> <span class="cap">${statVideoCap}</span>
    <br><br><span class="label">Video</span>
</span>

<span class="chart" data-percent="${statPodcastPrecent}">
    <span class="easypievalue">${statPodcast}</span> <span class="cap">${statPodcastCap}</span>
    <br><br><span class="label">Podcast</span>
</span>

<span class="chart" data-percent="${statAudiobookPrecent}">
    <span class="easypievalue">${statAudiobook}</span> <span class="cap">${statAudiobookCap}</span>
    <br><br><span class="label">Audiobooks</span>
</span>

</div>

<div style="width: 470px;float: left;" class="artisthub" > 

<span class="chart" data-percent="${statDBPrecent}">
    <span class="easypievalue">${statDB}</span> <span class="cap">${statDBCap}</span>
    <br><br><span class="label">Database</span>
</span>

<span class="chart" data-percent="${statPlayerPrecent}">
    <span class="easypievalue">${statPlayer}</span>
    <br><br><span class="label">Connections</span>
</span>

<span class="chart" data-percent="${statUserPrecent}">
    <span class="easypievalue">${statUser}</span> <span class="cap">${statUserCap}</span>
    <br><br><span class="label">User</span>
</span>

<span class="chart" data-percent="${statMemory}">
    <span class="percent"></span>
    <br><br><span class="label">Memory</span>
</span>

</div>

<div style="width:100%;margin-top:5px;float:left;min-height:300px;"> 

<form:form method="post" action="adminSettings.view?" commandName="command">

    <table style="white-space:nowrap" class="indent">
	   
        <tr>
            <td colspan="2" style="padding-top:1.0em;padding-bottom:1em">
                <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
                <input type="button" value="<fmt:message key="common.cancel"/>" onclick="parent.frames.main.location.href='nowPlaying.view'">
            </td>
        </tr>	   

       <tr><td colspan="2">&nbsp;<p class="forward"><a href="adminSettings.view?scanNow"><fmt:message key="musicfoldersettings.scannow"/></a></p>
       </td>
	   </tr>

		
        <tr>
            <td><fmt:message key="generalsettings.language"/></td>
            <td>
                <form:select path="localeIndex" cssStyle="display:none">
                    <c:forEach items="${command.locales}" var="locale" varStatus="loopStatus">
                        <form:option value="${loopStatus.count - 1}" label="${locale}"/>
                    </c:forEach>
                </form:select>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="language"/></c:import>
            </td>
        </tr>

        <tr>
            <td><fmt:message key="generalsettings.theme"/></td>
            <td>
            
                <div style="float:right;clear: both;position: absolute;right: 5px;top: 625px;">
                     <img id="preview1" src="" alt="" >
                </div>
            
                <form:select path="themeIndex" cssStyle="display:none" onchange="changePreview(this.options[selectedIndex].label)">
                    <c:forEach items="${command.themes}" var="theme" varStatus="loopStatus">
                        <form:option value="${loopStatus.count - 1}" label="${theme.name}"/>
                    </c:forEach>
                </form:select>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="theme"/></c:import>
            </td>
        </tr>
        
        <tr>
        <td>&nbsp;</td>
        </tr>
        <tr>
            <td><fmt:message key="generalsettings.scanmode"/></td>
            <td>
                <form:select path="scanMode" cssStyle="display:none">
                        <form:option value="NATIVE"/>
                        <form:option value="MIXED"/>
                </form:select>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="scanmode"/></c:import>
            </td>
        </tr>        
        
        <tr>
            <td><fmt:message key="generalsettings.logfileLevel"/></td>
            <td>
                <form:select path="logfileLevel" cssStyle="display:none">
                        <form:option value="OFF"/>
                        <form:option value="ERROR"/>
                        <form:option value="WARN"/>
                        <form:option value="INFO"/>
                        <form:option value="DEBUG"/>
                </form:select>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="logfileLevel"/></c:import>
            </td>
        </tr>

        <tr>
            <td><fmt:message key="generalsettings.playlistexportmode"/></td>
            <td>
                <form:select path="playlistExportMode" cssStyle="display:none">
                        <form:option value="M3U8"/>
                        <form:option value="PLS"/>
                        <form:option value="WPL"/>
                        <form:option value="XSPF"/>
                </form:select>
				
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="playlistExportMode"/></c:import>
            </td>
        </tr>		
		
        <tr>
            <td><fmt:message key="generalsettings.indexListSize"/></td>
            <td>
                <form:select path="indexListSize" cssStyle="display:none">
                        <form:option value="5"/>
                        <form:option value="10"/>
                        <form:option value="15"/>
                        <form:option value="25"/>
                        <form:option value="35"/>
                </form:select>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="indexListSize"/></c:import>
            </td>
        </tr>        
     
        <tr>
            <td>
                <br>
                <label for="coverArtHQ"><fmt:message key="generalsettings.coverArtHQ"/></label>
                <div style="margin:0 0 0 5px;display:inline;">
                    <c:import url="helpToolTip.jsp"><c:param name="topic" value="coverArtHQ"/></c:import>
                </div> 
                
                
            </td>
            <td>
                <br>
                <form:checkbox path="coverArtHQ" id="coverArtHQ"/>
            </td>
        </tr>

        <tr>
            <td>
                <label for="customLogo"><fmt:message key="generalsettings.customLogo"/></label>
                <div style="margin:0 0 0 5px;display:inline;">
                    <c:import url="helpToolTip.jsp"><c:param name="topic" value="customLogo"/></c:import>
                </div> 
            </td>
            <td>
                <form:checkbox path="customLogo" id="customLogo"/>
				
            </td>
        </tr>

        <tr>
            <td>
                <label for="appRedirect"><fmt:message key="generalsettings.appredirect"/></label>
                <div style="margin:0 0 0 5px;display:inline;">
                    <c:import url="helpToolTip.jsp"><c:param name="topic" value="appRedirect"/></c:import>
                </div> 
            </td>
            <td>
                <form:checkbox path="appRedirect" id="appRedirect"/>
            </td>
        </tr>		
		
        <tr>
            <td>
                <label for="snowEnabled"><fmt:message key="generalsettings.snowEnabled"/></label>
            </td>
            <td>
                <form:checkbox path="snowEnabled" id="snowEnabled"/>
            </td>
        </tr>
		
        <tr>
            <td>
				<br>
                <label for="debugOutput"><fmt:message key="generalsettings.debugOutput"/></label>
                <div style="margin:0 0 0 5px; display:inline;">
                    <c:import url="helpToolTip.jsp"><c:param name="topic" value="debugOutput"/></c:import>
                </div>                
            </td>
            <td>
				<br>
                <form:checkbox path="debugOutput" id="debugOutput"/>
            </td>
        </tr>        
        
        <tr>
            <td style="min-width: 165px;">
                <label for="showCreateLink"><fmt:message key="generalsettings.showCreateLink"/></label>
            </td>
            <td>
                <form:checkbox path="showCreateLink" id="showCreateLink"/>
            </td>
        </tr>

        <tr>
            <td>
                <label for="showMediaType"><fmt:message key="generalsettings.showMediaType"/></label>
            </td>
            <td>
                <form:checkbox path="showMediaType" id="showMediaType"/>
            </td>
        </tr>        
        
    </table>
    
</form:form>

<br>

<div style="position:absolute;left:490px;width:400px;top:370px;padding: 0 1em 0 1em;border-left: 1px solid #696969;border-left:1px solid #<spring:theme code="detailColor"/>">
    <madsonic:wiki text="${adminMessage}"/>
</div>

<div id="easyPieChartBarColor" class="easyPieChartBarColor"></div>
<div id="easyPieChartScaleColor" class="easyPieChartScaleColor"></div>
<div id="easyPieChartTrackColor" class="easyPieChartTrackColor"></div>

<script>

var barColor = window.getComputedStyle(document.getElementById('easyPieChartBarColor'), null).getPropertyValue('color');
var scaleColor = window.getComputedStyle(document.getElementById('easyPieChartScaleColor'), null).getPropertyValue('color');
var trackColor = window.getComputedStyle(document.getElementById('easyPieChartTrackColor'), null).getPropertyValue('color');

$(function() {
    $('.chart').easyPieChart({
        barColor: barColor,
        scaleColor: scaleColor,
        trackColor: trackColor,
        scaleLength: 5,        
        size:90,
        animate: 2000,
        lineCap:'butt',
        lineWidth: 10,
        easing: 'easeOutBounce',
        onStep: function(from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
            $(this.el).find('.dataGB').text(Math.round(percent));
            $(this.el).find('.dataMB').text(Math.round(percent));
        }        
    });
});

  //Function to convert hex format to a rgb color
function rgb2hex(rgb) {
 var hexDigits = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];
 rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
 function hex(x) {
  return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
 }
 return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}
</script>

<c:if test="${command.reloadNeeded}">
    <script language="javascript" type="text/javascript">
        parent.frames.left.location.href="left.view?";
        parent.frames.leftPanel.location.href="leftPanel.view?";
        parent.frames.playQueue.location.href="playQueue.view?";
        parent.frames.right.location.href="right.view?";
        parent.frames.sidePanel.location.href="nowListening.view?";
    </script>
</c:if>

<c:if test="${command.fullReloadNeeded}">
    <script language="javascript" type="text/javascript">
        parent.location.href="index.view?";
    </script>
</c:if>

</div>

	</div>
<!-- CONTENT -->
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#localeIndex").dropdownReplacement({selectCssWidth: 180, optionsDisplayNum: 10 });
    $("#themeIndex").dropdownReplacement({selectCssWidth: 180, optionsDisplayNum: 10, });
    $("#indexListSize").dropdownReplacement({selectCssWidth: 100, optionsDisplayNum: 10, });
    $("#logfileLevel").dropdownReplacement({selectCssWidth: 100, optionsDisplayNum: 10, });
    $("#scanMode").dropdownReplacement({selectCssWidth: 100, optionsDisplayNum: 10, });
    $("#playlistExportMode").dropdownReplacement({selectCssWidth: 100, optionsDisplayNum: 10, });
});    
</script>

<c:if test="${customScrollbar}">
    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/
                    alwaysShowScrollbar:true,
                    advanced:{ autoExpandHorizontalScroll: true },
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>    
</body>
</html>
