<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.madsonic.util.STokenUtils" %>
<%
  String siteKey = "6Lfffg0TAAAAAKFOx9mpGgm8UlIjBbzYI3klZhSh";
  String siteSecret = "6Lfffg0TAAAAAPEg8i3OBsVn-zbcHCPfaW7A1g-7";
%>

<html>
<head>
    <%@ include file="head.jsp" %>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <style>
    input {
        color: #222 !important;
        border: 1px solid #ddd;
        background-image: url("../icons/madsonic_black/bg-white.jpg");
    }
      
    div#poweredby {
        position: absolute;
        bottom: 0;
        right: 0;
        display: block;
        padding: 5px;
    }
</style>    

</head>
<body class="mainframe splash" onload="document.getElementById('username').focus()">

<form action="signup.view" method="POST">

<center>
    <div class="loginsplash bgcolor2" align="center" style="min-width:450px;max-width:600px;border:1px solid black; padding:10px 50px 20px 50px; margin-top:160px">
    <div style="margin-bottom:1em;max-width:50em;text-align:left;"><madsonic:wiki text="${model.loginMessage}"/></div> 
        <div style="margin-left: auto; margin-right: auto; max-width:50em">
            <table>
                <tr>
                <td colspan="1" align="left" style="padding-bottom:15px">
				
				<c:if test="${model.customlogo}">
					<img id="logo-icon" src="coverArt.view?logo=1" width="200" alt=""> 
				</c:if>				
				
				<c:if test="${!model.customlogo}">
					<img src="<spring:theme code="logoImage"/>" width="200" alt=""> 
				</c:if>
                </td>
                <td>
				<b style="font-size:16px;">${model.signupMessage}</b>
                </td>
                </tr>            

                <tr>
                <td colspan="1" align="left" style="padding-bottom:15px">
                </td>
                </tr>            
				
                <tr>
                <td align="right" style="padding-bottom:5px;padding-right:10px"><fmt:message key="signup.username"/></td>
                <td align="left" style="padding-bottom:5px;padding-left: 15px;"><input type="text" id="username" name="username" placeholder="<fmt:message key="signup.username"/> ..." style="width:15em;" tabindex="1"></td>
                </tr>
                
                <tr>
                <td align="right" style="padding-bottom:5px;padding-right:10px;"><fmt:message key="signup.password"/></td>
                <td align="left"  style="padding-bottom:5px;padding-left:15px;"><input type="password" id="password" name="password" placeholder="<fmt:message key="signup.password"/> ..." style="width:15em" tabindex="2"></td>
                </tr>
				
                <tr>
                <td align="right" style="padding-bottom:5px;padding-right:10px;"><fmt:message key="signup.email"/></td>
                <td align="left" style="padding-bottom:5px;padding-left: 15px;"><input id="email" name="email" placeholder="<fmt:message key="signup.email"/> ..." style="width:15em" tabindex="3"></td>
                </tr>

                <tr>
                <td align="right" style="padding-bottom:5px;padding-right:10px;"><fmt:message key="signup.fullname"/></td>
                <td align="left" style="padding-bottom:5px;padding-left: 15px;"><input id="name" name="name" placeholder="<fmt:message key="signup.fullname"/> ..." style="width:15em" tabindex="4"></td>
                </tr>

                <tr>
                <td align="right" style="padding-bottom:5px;padding-right:10px;"><fmt:message key="signup.comment"/></td>
                <td align="left" style="padding-bottom:0px;padding-left: 15px;"><input id="comment" name="comment" placeholder="<fmt:message key="signup.comment"/> ..." style="width:15em" tabindex="5"></td>
                </tr>

                <c:if test="${not empty model.captcha}">
                <tr>
                <td align="right"></td>
                <td align="left" style="padding-top:10px">
                <div class="g-recaptcha" style="padding-left:15px" tabindex="6" data-sitekey=<%=siteKey%> data-stoken=<%=STokenUtils.createSToken(siteSecret)%>></div>
                </td>
                </tr>                
                </c:if>
                
                <tr>
                <td align="right" style="padding-bottom:10px;padding-left: 15px;">
                <input class="signup" name="register" type="submit" style="width:90px;" value="Register" tabindex="7">
                </td>
               
                <tr>
                <td align="right"><a href="signup.view"><a href="login.view"><input name="back" type="button" style="width:90px;" value="back" tabindex="8"></a>
                </td>
                </tr>
                
                <c:if test="${not empty model.error}">
                <tr>
                <td align="right"></td>
                <td align="left" style="padding-left:20px">
                    <p style="padding-top: 1em" class="warning"><fmt:message key="${model.error}"/></p>
                </td>
                </tr>
                </c:if>                
            </table>
        </div>
    </div>
	</center>
    
</form>
<div id="poweredby">
<a href="http://www.madsonic.org">powered by Madsonic</a></div> 
</div>
</body>
</html>
