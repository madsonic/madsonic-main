<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">  
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/nowPlayingService.js"/>"></script>    
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>     
	<c:if test="${model.snowEnabled}">
	<script type="text/javascript" src="<c:url value="/script/snow/js/ThreeCanvas.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/snow/js/Snow.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/snow/js/SnowScript.js"/>"></script>
 	</c:if>
	<c:choose>
	<c:when test="${model.customScrollbar}">
	<link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
	<script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>
	</c:when><c:otherwise>
	<link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
	<script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.js"/>"></script>
	<style>  
		.content_left {overflow-y: -moz-scrollbars-none;}
	</style>  
	</c:otherwise>
	</c:choose>	
	<style>  
		_:-moz-tree-row(hover), .leftframe {
			overflow: hidden;
		}
        canvas{
                position:fixed;
                top: 0;
                left: 0;
                width: 300px;
				height: 900px;
                opacity: 0.9;
                z-index: -1;        
        }		
	</style> 
    
    <script type="text/javascript" language="javascript">
	
        startGetScanningStatusTimer();

        function startGetScanningStatusTimer() {
            nowPlayingService.getScanningStatus(getScanningStatusCallback);
        }
        
        function getScanningStatusCallback(scanInfo) {
            dwr.util.setValue("scanCount", scanInfo.count);
            if (scanInfo.scanning) {
                    jQuery("#scanningStatus").show();
                    setTimeout("startGetScanningStatusTimer()", 1000);
            } else {
                jQuery("#scanningStatus").hide();
                setTimeout("startGetScanningStatusTimer()", 15000);
            }
        }
    
        function changeMusicFolder(musicFolderId) {
            multiService.setSelectedMusicFolder(musicFolderId, function() {
                var mainLocation = top.main.location.href;
                if (mainLocation.indexOf("/home.view") != -1) {
                    top.main.location.href = mainLocation;
                } else {
                    top.main.location.href = "artists.view";
                }
            });
        }
        
        function changeMusicGenre(genre) {
               multiService.setSelectedGenre(genre);
                var mainLocation = top.main.location.href;
                if (mainLocation.indexOf("/home.view") != -1) {
                    top.main.location.href = mainLocation;
                } else {
                    top.main.location.href = "artists.view";
                }               
        }        
        
        var previousQuery = "";
        var instantSearchTimeout;

        function triggerInstantSearch(event) {
            if (event.keyCode == 27) { // Escape key
                $("#query").blur();
                return;
            }
            if (instantSearchTimeout) {
                window.clearTimeout(instantSearchTimeout);
            }
            instantSearchTimeout = window.setTimeout(executeInstantSearch, 300);
        }
        
        function executeInstantSearch() {
            var query = $("#query").val().trim();
            if (query.length > 1 && query != previousQuery) {
                previousQuery = query;
                document.searchForm.submit();
            }
        }       

        function toggleLeftBar(show) {
            $("#hide-side-bar").toggle(show);
            $("#show-side-bar").toggle(!show);
            parent.toggleLeftBar(show);
            multiService.setShowLeftBar(show);
            toggleControl(show);
        }

        function toggleLeftPanel(show) {
            $("#hide-left-bar").toggle(show);
            $("#show-left-bar").toggle(!show);
            $("#hide-side-bar").toggle(!show);
            $("#show-side-bar").toggle(show);
            
            <c:if test="${not model.showLeftBarShrinked}">
            parent.toggleLeftBar(show);
            parent.toggleLeftPanel(!show);
            toggleControl(show);
            </c:if>
            
            <c:if test="${model.showLeftBarShrinked}">
            parent.toggleLeftBar(false);
            parent.toggleLeftPanel(!show);
            toggleControl(false);
            </c:if>
        }

        function showStatistic() {
            $('#statistics').show('blind');
            $('#hideStatistics').show();
            $('#showStatistics').hide();
        }

        function hideStatistic() {
            $('#statistics').hide('blind');
            $('#hideStatistics').hide();
            $('#showStatistics').show();
        }
        
        function keyboardShortcut(action, param) {
            if (action == "toggleLeftBar") {
                if ($("#show-side-bar").is(":visible")) {
                    $("#show-side-bar").click();
                } else if ($("#hide-side-bar").is(":visible")) {
                    $("#hide-side-bar").click();
                }
            } else if (action == "toggleLeftPanel") {
                if ($("#show-left-bar").is(":visible")) {
                    $("#show-left-bar").click();
                } else if ($("#hide-left-bar").is(":visible")) {
                    $("#hide-left-bar").click();
                }
            } else if (action == "showHome") {
                showPage("home.view");
            } else if (action == "showIndex") {
                showPage(param ? "artists.view#" + param : "artists.view");
            } else if (action == "showStarred") {
                showPage("starred.view");
            } else if (action == "showRadio") {
                showPage("radio.view");
            } else if (action == "showGenre") {
                showPage("genres.view");
            } else if (action == "showDJ") {
                showPage("random.view");
            } else if (action == "showPlaylists") {
                showPage("playlists.view");
            } else if (action == "showPodcasts") {
                showPage("podcastChannels.view");
            } else if (action == "showSettings") {
                showPage("settings.view");
            } else if (action == "showMore") {
                showPage(param ? "more.view#" + param : "more.view");
            } else if (action == "showAbout") {
                showPage("help.view");
            } else if (action == "search") {
                toggleSideBar(true);
                toggleControl(true);
                $("#query").focus().select();
            }
        }    
        
        function showPage(url) {
            parent.frames.main.location.href = url;
        }
        
        function onHome() {
            showPage("home.view?listType=${model.listType}");
        }
        
        function newwindow() {
            window.open('upload.view?','jav','width=750,height=270,resizable=no,scrollbars=yes,toolbar=no,status=yes');
        }
    </script>
    
</head>

<body class="leftframe bgcolor2"> <!-- BODY -->
<div id="content_left" class="content_left"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:if test="${model.snowEnabled}">
<script type="text/javascript" language="javascript">
	init();
</script>
</c:if>
<canvas id="canvas"></canvas>

<a name="top"></a>

<fmt:message key="left.home" var="home"/>
<fmt:message key="left.artist" var="artist"/>
<fmt:message key="left.artists" var="artists"/>
<fmt:message key="left.now_playing" var="nowPlaying"/>
<fmt:message key="left.loved" var="loved"/>
<fmt:message key="left.starred" var="starred"/>
<fmt:message key="left.genres" var="genres"/>
<fmt:message key="left.discover" var="discover"/>
<fmt:message key="left.settings" var="settings"/>
<fmt:message key="left.status" var="status"/>
<fmt:message key="left.radio" var="radio"/>
<fmt:message key="left.moods" var="moods"/>
<fmt:message key="left.history" var="history"/>
<fmt:message key="left.statistics" var="statistics"/>
<fmt:message key="left.playlists" var="playlists"/>
<fmt:message key="left.editor" var="editor"/>
<fmt:message key="left.db" var="db"/>
<fmt:message key="left.logs" var="logs"/>
<fmt:message key="left.upload" var="upload"/>
<fmt:message key="left.podcast" var="podcast"/>
<fmt:message key="left.more" var="more"/>
<fmt:message key="left.random" var="random"/>
<fmt:message key="left.chat" var="chat"/>
<fmt:message key="left.help" var="help"/>
<fmt:message key="left.search" var="search"/>
<fmt:message key="left.profile" var="profile"/>
<fmt:message key="left.lastfm" var="lastfm"/>
<fmt:message key="left.shared" var="shared"/>

<c:if test="${!model.customlogo}">
 <img id="logo-icon" src="<spring:theme code="iconImage"/>" width="30" onclick="onHome()" alt="" style="margin: 0 0 0 5px;padding:10px 0 0 0;"> 
</c:if>

<c:if test="${model.customlogo}">
<img id="logo-icon" src="coverArt.view?icon=1" width="30" alt="" onclick="onHome()" alt="" style="margin: 0 0 4px 4px;padding: 6px 0 0 0;"> 
</c:if>

<Table>
<tr>
    <td style="padding-left:0.2em;padding-top:15px">
        <form method="post" action="search.view" target="main" name="searchForm">
            <table><tr>
                <td style="padding-right: 5px;">
                <c:choose>    
                <c:when test="${model.showLeftPanel}">
                    <c:if test="${model.showLeftBarShrinked}">
                    <img id="show-left-bar" src="<spring:theme code="viewAsMultiImage"/>" onclick="toggleLeftPanel(true)" alt="" style="display:block; margin:0 4px 6px 0;padding: 4px 0 0 0;">
                    <img id="hide-left-bar" src="<spring:theme code="viewAsCloseImage"/>" onclick="toggleLeftPanel(false)" alt="" style="display:none; margin:0 4px 6px 0;padding: 4px 0 0 0;">
                    </c:if>
                    <c:if test="${not model.showLeftBarShrinked}">
                    <img id="show-left-bar" src="<spring:theme code="viewAsMultiImage"/>" onclick="toggleLeftPanel(true)" alt="" style="display:none; margin:0 4px 6px 0;padding: 4px 0 0 0;">
                    <img id="hide-left-bar" src="<spring:theme code="viewAsCloseImage"/>" onclick="toggleLeftPanel(false)" alt="" style="display:block; margin:0 4px 6px 0;padding: 4px 0 0 0;">
                    </c:if>
                </c:when>
                <c:otherwise>
                    <img id="show-side-bar" src="<spring:theme code="viewAsListImage"/>" onclick="toggleLeftBar(true)" alt="" style="display:${model.showLeftBar ? 'none' : 'block'};  margin: 0 4px 6px 0;padding: 4px 0 0 0;">
                    <img id="hide-side-bar" src="<spring:theme code="viewAsOpenImage"/>" onclick="toggleLeftBar(false)" alt="" style="display:${model.showLeftBar ? 'block' : 'none'}; margin: 0 4px 6px 0;padding: 4px 0 0 0;">
                </c:otherwise>
                </c:choose>        
                </td>
                <td><div id="searchInput" style="display:none;"> <input type="text" name="query" id="query" size="15" placeholder="${search} ..." onclick="select();" onkeyup="triggerInstantSearch(event);"></div> </td>
                <td><div id="searchButton" style="display:none;"> <a href="javascript:document.searchForm.submit()"><img src="<spring:theme code="searchImage"/>" alt="${search}" style="width:20px" title="${search}"></a></div></td>
            </tr></table>
        </form>
    </td>
</tr>
</table>

    <a href="help.view" target="main">
	
	<c:if test="${model.customlogo}">
	<div style="clear:both;position:absolute;top:10px;left:52px;">
	<img src="coverArt.view?logo=1" width="160" alt="" title="<fmt:message key="common.help"/>">
	</div>
	</c:if>
	
	<c:if test="${!model.customlogo}">
	<div style="clear:both;position:absolute;top:15px;left:52px;">
	<img src="<spring:theme code="logoImage"/>" width="160" title="<fmt:message key="common.help"/>" alt="">
	</div>
	</c:if>
	</a>

<div id="scanningStatus" class="warning" style="display:none;padding-left:11px;z-index:1000;padding-bottom:10px">
    <i class="fa fa-refresh fa-spin" style="margin-right:14px;font-size: 18px;"></i>     
    <fmt:message key="main.scanning"/> <span id="scanCount"></span>
</div>  

    <div class="topHeader">
    <c:choose>    
       <c:when test="${model.licenseInfo.licenseValid}">
       <!--
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="donateImage"/>" title="${logs}">
        <a href="premiumSettings.view" target="main"><fmt:message key="common.gotpremium"/></a> -->
        </c:when>
        <c:otherwise>
            <table><tr>
            <td>
                <a href="premiumSettings.view" target="main"><img class="topHeaderIcon hover-effect-kenburn" style="left:-3px;" src="<spring:theme code="donateImage"/>" title="${logs}"></a>
            </td>
            <td>
                <a href="premiumSettings.view" target="main"><fmt:message key="common.getpremium"/></a></td></tr>
                <tr>
                <td>
                </td>
                <td>
                <c:if test="${model.licenseInfo.trialDaysLeft gt 0}">        
                <fmt:message key="common.trialdaysleft"><fmt:param value="${model.licenseInfo.trialDaysLeft}"/></fmt:message>
                </c:if>
                </td>
                </tr>
            </table>
            <div style="padding-bottom:10px;"> </div>            
        </c:otherwise>
    </c:choose>        
    </div>

    <c:if test="${model.newVersionAvailable}">  
        <c:if test="${model.NotificationEnabled}"> 
            <c:if test="${model.user.adminRole}">  
                <div class="topHeader">
                    <a href="help.view" target="main">
                    <img class="topHeaderIcon hover-effect-kenburn" src="<c:url value="/icons/default/new.png"/>" title="Version ${model.latestVersion}">
                    <fmt:message key="left.upgrade"/></a>
                </div>
            </c:if>
        </c:if>
    </c:if>    
    
    <div class="topHeader">
        <a href="j_spring_security_logout" target="_top">
        <fmt:message key="left.logout" var="logout"><fmt:param value="${model.user.username}"/></fmt:message>
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="logoffImage"/>" title="${logout}"> ${fn:escapeXml(logout)}</a>
    </div>

    <c:if test="${model.user.settingsRole}">    
    <div class="topHeader">
       <a href="shareSettings.view?" target="main">
       <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="shareImage"/>" title="${shared}">
       <fmt:message key="left.shared"/></a>
    </div>
   </c:if> 	
	
    <c:if test="${model.showIconLastFM}">    
    <div class="topHeader">
        <a href="lastfmUser.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="lastfmLargeImage"/>" title="${lastfm}">
        <fmt:message key="left.lastfm"/></a>
    </div>
   </c:if> 
    
    <c:if test="${model.user.settingsRole}">    
    <madsonic:url value="avatar.view" var="avatarUrl">
        <madsonic:param name="username" value="${model.user.username}"/>
    </madsonic:url>
    <div class="topHeader">
       <a href="personalSettings.view?" target="main">
       <img class="topHeaderIcon hover-effect-kenburn" src="${avatarUrl}" title="${profile}">
       <fmt:message key="left.profile"/></a>
    </div>
   </c:if> 

    <c:if test="${model.showIconSettings}">            
        <c:if test="${model.user.settingsRole || model.user.adminRole}">
			<div class="topHeader">
				<a href="settings.view?" target="main">
				<img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="settingsImage"/>" title="${settings}">
				<fmt:message key="left.settings"/></a>
			</div>
        </c:if>
    </c:if>

    <c:if test="${model.user.adminRole}">
    <c:if test="${model.user.username eq 'admin'}">
    <div class="topHeader">
        <a href="db.view" target="_blank">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="dbImage"/>" title="${db}">
        <fmt:message key="left.db"/></a>
    </div>    
   </c:if>
   </c:if>
    
    <c:if test="${model.user.adminRole}">
    <div class="topHeader">
        <a href="log.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="logsImage"/>" title="${logs}">
        <fmt:message key="left.logs"/></a>
    </div>
   </c:if>
   
    <div id="stats" style="display:${model.showLeftBar ? 'block' : 'none'};">   
   
    <span id="showStatistics" style="display:block">    
        <div class="topHeader">
        <a href="javascript:noop()" onclick="showStatistic()">
            <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="chartsImage"/>" title="<fmt:message key="left.showstatistic"/>">
            <fmt:message key="left.showstatistic"/></a>
        </div>
    </span>

    <span id="hideStatistics" style="display:none">    
        <div class="topHeader">
        <a href="javascript:noop()" onclick="hideStatistic()">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="chartsImage"/>" title="<fmt:message key="left.hidestatistic"/>">
        <fmt:message key="left.hidestatistic"/></a>
        </div>
    </span>

    <c:if test="${model.user.adminRole}">
    <div class="topHeader">
        <span id="advancedStatistics" style="display:block">    
        <a href="serverStatus.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="chartsImage"/>" title="<fmt:message key="left.advancedstatistics"/>">
        <fmt:message key="left.advancedstatistics"/></a>
        </span>
    </div>
    </c:if>
    
    <c:if test="${model.statistics.songCount gt 0}">
    <div id="statistics" style="display:none;margin-left:40px;margin-top:10px;">
    <h2 class="bgcolor1"><fmt:message key="left.statistic"/></h2>
        <div class="detail" style="margin-left:5px;margin-top:10px;">
            <fmt:message key="left.statistics">
                <fmt:param value="${model.statistics.artistCount}"/>
                <fmt:param value="${model.statistics.albumArtistCount}"/>
                <fmt:param value="${model.statistics.albumCount}"/>
                <fmt:param value="${model.statistics.genreCount}"/>
                <fmt:param value="${model.statistics.songCount}"/>
                <fmt:param value="${model.statistics.videoCount}"/>
                <fmt:param value="${model.statistics.podcastCount}"/>
                <fmt:param value="${model.statistics.audiobookCount}"/>
                <fmt:param value="${model.bytes}"/>
                <fmt:param value="${model.hours}"/>
            </fmt:message>
        </div>
    </div>
    </c:if>    

    </div>
	
   
    <div style="padding-bottom:20px;"> </div>
    
    <c:if test="${model.showIconHome}">
    <div class="topHeader">
        <madsonic:url value="home.view" var="homeUrl">
            <madsonic:param name="listType" value="${model.listType}"/>
        </madsonic:url>
        <a href="${homeUrl}" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="homeImage"/>" title="${home}">
        <fmt:message key="left.home"/></a>
    </div>
   </c:if>

   <c:if test="${model.showIconIndex}">
   <div class="topHeader">
        <a href="artists.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="indexImage"/>" title="${artists}">
        <fmt:message key="left.artists"/></a>
    </div>
   </c:if>
    
    <c:if test="${model.showIconArtist}">
    <div class="topHeader">
        <a href="artist.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="artistImage"/>" title="${artist}">
        <fmt:message key="left.artist"/></a>
    </div>
   </c:if>

    <c:if test="${model.showIconPlaying}">   
    <div class="topHeader">
    <a href="nowPlaying.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="nowPlayingImage"/>" title="${nowPlaying}">
        <fmt:message key="left.now_playing"/></a>
    </div>
   </c:if>

    <c:if test="${model.showIconCover}">   
    <div class="topHeader">
        <a href="discover.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="discoverImage"/>" title="${discover}">
        <fmt:message key="left.discover"/></a>
    </div>
   </c:if>
  
    <c:if test="${model.showIconBookmark}">   
    <div class="topHeader">
        <a href="bookmark.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="bookmarkImage"/>" title="${bookmark}">
        <fmt:message key="left.bookmark"/></a>
    </div>  
   </c:if>

    <c:if test="${model.showIconStarred}">  
    <div class="topHeader">
        <a href="starred.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="coverImage"/>" title="${starred}">
        <fmt:message key="left.starred"/></a>
    </div>  
   </c:if>

    <c:if test="${model.showIconLoved}">  
    <div class="topHeader">
        <a href="loved.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="starOnImage"/>" title="${loved}">
        <fmt:message key="left.loved"/></a>
    </div>  
   </c:if>
      
    <c:if test="${model.showIconGenre}">
    <div class="topHeader">
        <a href="genres.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="genresImage"/>" title="${genres}">
        <fmt:message key="left.genres"/></a>
    </div>  
   </c:if>

    <c:if test="${model.showIconMoods}">
    <div class="topHeader">
        <a href="moods.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="moodsImage"/>" title="${moods}">
        <fmt:message key="left.moods"/></a>
    </div>  
   </c:if>
    
    <c:if test="${model.showIconRadio}">
    <div class="topHeader">
        <a href="radio.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="radioImage"/>" title="${radio}">
        <fmt:message key="left.tagradio"/></a>
    </div>  
   </c:if>

    <c:if test="${model.showIconRandom}">
    <div class="topHeader">
        <a href="random.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="randomImage"/>" title="${random}">
        <fmt:message key="left.random"/></a>
    </div>  
   </c:if>

    <c:if test="${model.showIconPlaylists}">
    <div class="topHeader">
        <a href="playlists.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="playlistImage"/>" title="${playlists}">
        <fmt:message key="left.playlists"/></a>
    </div>  
   </c:if>

 	<!--
	<c:if test="${model.showIconPlaylistManage}">
    <div class="topHeader">
        <a href="loadPlaylist.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="playlistImage"/>" title="${playlists}">
        ${playlists} Manage</a>
    </div>
    </c:if>
   -->            
    <c:if test="${model.showIconPlaylistEditor}">            
    <div class="topHeader">
        <a href="playlistEditor.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="playlistEditImage"/>" title="${editor}">
        ${playlists} ${editor}</a>
    </div>        
    </c:if>      
   
    <c:if test="${model.showIconPodcast}">
    <div class="topHeader">
        <a href="podcastChannels.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="podcastLargeImage"/>" title="${podcast}">
        <fmt:message key="left.podcast"/></a>
    </div>      
   </c:if>
    

    <c:if test="${model.showIconPodcastManage}">
    <div class="topHeader">
        <a href="podcastReceiver.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="podcastLargeImage"/>" title="${podcast}">
        <fmt:message key="left.podcast"/> Manage</a>
    </div>      
   </c:if>

   
    <c:if test="${model.showIconStatus}">
    <div class="topHeader">
        <a href="status.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="statusImage"/>" title="${status}">
        <fmt:message key="left.status"/></a>
    </div>  
   </c:if>

    <c:if test="${model.showIconSocial}">
    <div class="topHeader">
        <a href="chat.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="chatImage"/>" title="${chat}">
        <fmt:message key="left.chat"/></a>
    </div>  
    </c:if>

    <c:if test="${model.showIconHistory}">
    <div class="topHeader">
        <a href="history.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="historyImage"/>" title="${history}">
        <fmt:message key="left.history"/></a>
    </div>      
    </c:if>
   
    <c:if test="${model.showIconStatistics}">
    <div class="topHeader">
        <a href="statistics.view?" target="main">
        <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="chartImage"/>" title="${statistics}">
        <fmt:message key="left.statistic"/></a>
    </div>    
    </c:if>   
       
    <c:if test="${model.showIconMore}">
    <div class="topHeader">
       <a href="more.view?" target="main">
       <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="moreImage"/>" title="${more}">
        <fmt:message key="left.more"/></a>
    </div>
   </c:if>

   <c:if test="${model.user.uploadRole}">
   <div class="topHeader">
    <a href="javascript:newwindow()" target="main">
    <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="loadImage_mini"/>" title="${upload}">
    ${upload}</a>
   </div>
   </c:if>

   <c:if test="${model.showIconInternetRadio}">
    <div class="topHeader">
       <a href="internetRadio.view?" target="main">
       <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="streamImage"/>" title="<fmt:message key="left.radio"/>">
        <fmt:message key="left.radio"/></a>
    </div>
   </c:if>
   
    <c:if test="${model.showIconAbout}">
    <div class="topHeader">
       <a href="help.view?" target="main">
       <img class="topHeaderIcon hover-effect-kenburn" src="<spring:theme code="aboutImage"/>" title="${help}">
        <fmt:message key="help.title"><fmt:param value="${model.brand}"/></fmt:message></a>
    </div> 
   </c:if>
    
	<div style="padding-top:20px"/>
	
    <c:if test="${!model.showLeftBar}">
    <script type="text/javascript" language="javascript">
        $("#searchInput").hide('swing');
        $("#searchButton").hide('swing');
    </script>
    </c:if>    
    <c:if test="${model.showLeftBar}">
    <script type="text/javascript" language="javascript">
        $("#searchInput").show('swing');
        $("#searchButton").show('swing');
    </script>
    </c:if>    

</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->
 
<c:if test="${not model.customScrollbar}">
<script type="text/javascript">        
		jQuery('html,body').bind('mousewheel', function(event) {
		event.preventDefault();
		var scrollTop = this.scrollTop;
		this.scrollTop = (scrollTop + ((event.deltaY * event.deltaFactor) * -1));
		//console.log(event.deltaY, event.deltaFactor, event.originalEvent.deltaMode, event.originalEvent.wheelDelta);
		}); 
</script>    
</c:if>
 
<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
	(function($){
		$(window).load(function(){
			<c:if test="${model.showLeftBar}">
				enableScrollbar();
			</c:if>				
		});
	})(jQuery);
</script>    
</c:if>
	
<script>
function enableScrollbar() {
	$("#content_left").mCustomScrollbar({
	axis:"y",
	scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
	mouseWheel:true, /*mousewheel support: boolean*/
	mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
	autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
	autoHideScrollbar:true, /*auto-hide scrollbar when idle*/       
	alwaysShowScrollbar:false,
	advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
					updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
					autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
	scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
					scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
					scrollSpeed:"auto",      /*scroll buttons continuous scrolling speed: integer, "auto"*/
					scrollAmount:10 },       /*scroll buttons pixels scroll amount: integer (pixels)*/
					theme:"${model.customScrollbarTheme}",
					scrollbarPosition:"inside"
	});	
}

function toggleControl(show) {
    if (show) {
        $("#searchInput").show('swing');
        $("#searchButton").show('swing');
        $("#stats").show('swing');
		<c:if test="${model.customScrollbar}">
			enableScrollbar();
		</c:if>
    } else {
        hideStatistic();
        $("#searchInput").hide('swing');
        $("#searchButton").hide('swing');
        $("#stats").hide('swing');
		<c:if test="${model.customScrollbar}">
			$("#content_left").mCustomScrollbar("destroy");
		</c:if>
    }
}

$('.topHeader').click(function(){
$('.topHeader').removeClass("active");
$(this).addClass('active');
});
</script>
</body>
</html>