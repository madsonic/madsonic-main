<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--@elvariable id="command" type="org.madsonic.command.passwordsettingscommand"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <c:choose>
        <c:when test="${customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>
	
</head>
<body class="mainframe bgcolor1">

<div id="content_main" class="content_main">
<!-- CONTENT -->
<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="password"/>
    <c:param name="toast" value="${command.toast}"/>
    <c:param name="restricted" value="true"/>
    <c:param name="demo" value="${fn:startsWith(command.username, 'demo')}"/>    
</c:import>

<c:choose>

    <c:when test="${command.ldapAuthenticated}">
        <p><fmt:message key="usersettings.passwordnotsupportedforldap"/></p>
    </c:when>

    <c:otherwise> 

    <c:if test="${not fn:startsWith(command.username, 'demo')}">
        <fmt:message key="passwordsettings.title" var="title"><fmt:param>${command.username}</fmt:param></fmt:message>
        <h2>${fn:escapeXml(title)}</h2>
        <form:form method="post" action="passwordSettings.view" commandName="command">
            <table class="indent">
                <tr>
                    <td><fmt:message key="usersettings.newpassword"/></td>
                    <td><form:password path="password"/></td>
                    <td class="warning"><form:errors path="password"/></td>
                </tr>
                <tr>
                    <td><fmt:message key="usersettings.confirmpassword"/></td>
                    <td><form:password path="confirmPassword"/></td>
                    <td/>
                </tr>
                <tr>
                    <td colspan="3" style="padding-top:1.5em">
                        <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
                        <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">
                    </td>
                </tr>

            </table>
        </form:form>
    </c:if>
        
    </c:otherwise>
</c:choose>

<!-- CONTENT -->
</div>

</body>
<c:if test="${customScrollbar}">
<script type="text/javascript">    

		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);

$(".content_main").resize(function(e){
	$(".content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</html>
