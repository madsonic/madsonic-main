<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.madsonic.util.STokenUtils" %>
<%
  String siteKey = "6Lfffg0TAAAAAKFOx9mpGgm8UlIjBbzYI3klZhSh";
  String siteSecret = "6Lfffg0TAAAAAPEg8i3OBsVn-zbcHCPfaW7A1g-7";
%>

<html>
<head>
    <%@ include file="head.jsp" %>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <style>
    input {
        color: #222 !important;
        border: 1px solid #ddd;
        background-image: url("../icons/madsonic_black/bg-white.jpg");
    }
      
    div#poweredby {
        position: absolute;
        bottom: 0;
        right: 0;
        display: block;
        padding: 5px;
    }
</style>    

</head>
<body class="mainframe bgcolor1 splash" onload="document.getElementById('usernameOrEmail').focus()">

<form action="recover.view" method="POST">
<center>
    <div class="bgcolor2 loginsplash" style="border:1px solid black; padding:20px 50px 20px 50px; margin-top:200px;max-width:50em;">

        <div style="margin-left: auto; margin-right: auto; max-width:50em">

            <h1><fmt:message key="recover.title"/></h1>
            
            <p style="color:#222; padding-top: 1em; padding-bottom: 0.5em"><fmt:message key="recover.text"/></p>

            <c:if test="${empty model.sentTo}">
                <input type="text" id="usernameOrEmail" name="usernameOrEmail" style="width:18em;margin-right: 1em">
                <input name="submit" type="submit" value="<fmt:message key="recover.send"/>">
            </c:if>

            <c:if test="${not empty model.captcha}">
                <p style="padding-top: 1em">
                <div class="g-recaptcha" style="padding-left:18px" data-sitekey=<%=siteKey%> data-stoken=<%=STokenUtils.createSToken(siteSecret)%>></div>
                </p>
            </c:if>

            <c:if test="${not empty model.sentTo}">
                <p style="padding-top: 1em"><fmt:message key="recover.success"><fmt:param value="${model.sentTo}"/></fmt:message></p>
            </c:if>

            <c:if test="${not empty model.error}">
                <p style="padding-top: 1em" class="warning"><fmt:message key="${model.error}"/></p>
            </c:if>

            <div class="back" style="padding-left: 60px;margin-top: 1.5em; background-position: center center;"><a href="login.view"><fmt:message key="common.back"/></a></div>

        </div>
    </div>
	</center>
</form>
<div id="poweredby">
<a href="http://www.madsonic.org">powered by Madsonic</a></div> 
</div>
</body>
</html>
