<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--@elvariable id="model" type="java.util.Map"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
	
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
    <style>
        audio {
            margin-left: 2em;
            margin-right: 2em;
        }
    </style>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1 style="margin-bottom:15px">
<img src="<spring:theme code="streamImage"/>" width="32" alt=""> <fmt:message key="left.radio"/>
</h1>

<c:if test="${empty model.stations}">
    <p><em><fmt:message key="internetradio.empty"/></em></p>
</c:if>

<c:if test="${not empty model.stations}">
    <table class="music indent" style="line-height: 400%;">
        <c:forEach items="${model.stations}" var="station">
            <tr>
                <td class="fit">
                    <span style="margin-left: 2em"><b>${fn:escapeXml(station.name)}</b></span>
                </td>
                <td class="fit">
                    <audio controls preload="none" src="${station.streamUrl}"></audio>
                </td>
                <td class="fit">
                    <i class="material-icons clickable" onclick="window.open('${station.streamUrl}', '_blank');">open_in_new</i>
                </td>
                <td class="truncate"></td>
            </tr>
        </c:forEach>
    </table>
</c:if>


<c:if test="${model.user.adminRole}">
    <div style="padding-top:2em;">
        <i class="fa fa-cog fa-lg fa-fw icon"></i>&nbsp;&nbsp;<a href="internetRadioSettings.view"><fmt:message key="internetradio.settings"/></a>
    </div>
</c:if>

<div style="padding-top:2em"></div>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:true, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:false,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>

</body>
</html>
