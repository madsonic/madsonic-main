    <%@ include file="include.jsp" %>
    <%@ include file="piwik.jsp" %>
    <title>${model.pageTitle}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:set var="styleSheet"><spring:theme code="styleSheet"/></c:set>
    <c:set var="faviconImage"><spring:theme code="faviconImage"/></c:set>
    <link rel="shortcut icon" href="<c:url value="/${faviconImage}"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/${styleSheet}"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">    
    <script type="text/javascript" src="<c:url value="/script/mousetrap-1.6.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/mousetrap-madsonic.js"/>"></script>