<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--@elvariable id="model" type="java.util.Map"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>    
	
	<script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/playlistService.js"/>"></script>
    <script type="text/javascript" language="javascript">
	
        function createEmptyPlaylist() {
            playlistService.createPlaylist(playlistCallback);
        }
		
        function playlistCallback(playlist) {
            location.href = "playlist.view?id=" + playlist.id;
        }
		
        function confirmAction(onSuccessFunction, functionArg) {
            $('#dialog-delete').dialog({
                modal: false,
                autoOpen: false,
                title: 'Confirm',
                width: 350,
                height: 180,
                buttons: {
                    "<fmt:message key="common.delete"/>": function() {
                        $(this).dialog('close');
                        onSuccessFunction(functionArg);
                    },
                    "<fmt:message key="common.cancel"/>": function() {
                        $(this).dialog('close');
                    }
                }
            });
            $("#dialog-delete").dialog('open');
            
        };

        function onDeletePlaylist(deleteUrl) {
            location.href = deleteUrl;
        }		
		
    </script>
</head>

<body class="mainframe bgcolor1"> 					<!-- BODY -->
	<div id="content_main" class="content_main"> <!-- CONTENT -->
		<div id="container" class="container"> <!-- CONTAINER -->
		
			<div style="position: absolute;top:0;right:10px;padding: 20px 0 0 0;z-index:1000">
				<Table>
				<tr>
					<td style="padding-left:0.2em; padding-bottom:0em;">
						<%@ include file="viewSelectorPlaylist.jsp" %>
					</td>
				</tr>
				</table>
			</div>
			
			<div id="dialog-delete" title="<fmt:message key="common.confirm"/>" style="display: none;">
				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
					<fmt:message key="playlist2.confirmdelete"/></p>
			</div>

		<h1>
			<img src="<spring:theme code="playlistImage"/>" width="32" alt="">
			<fmt:message key="left.playlists"/>
		</h1>

		<c:if test="${empty model.playlists}">
			<br><p><em><fmt:message key="playlist2.noplaylists"/></em></p>
		</c:if>

		<c:if test="${not empty model.playlists}">

		<madsonic:url var="sortByName" value="playlists.view">
		<madsonic:param name="listType" value="name"/>
		<madsonic:param name="direction" value="${model.direction}"/>
		</madsonic:url>

		<madsonic:url var="sortByCreated" value="playlists.view">
		<madsonic:param name="listType" value="created"/>
		<madsonic:param name="direction" value="${model.direction}"/>
		</madsonic:url>

		<madsonic:url var="sortByChanged" value="playlists.view">
		<madsonic:param name="listType" value="changed"/>
		<madsonic:param name="direction" value="${model.direction}"/>
		</madsonic:url>

		<madsonic:url var="sortByFiles" value="playlists.view">
		<madsonic:param name="listType" value="files"/>
		<madsonic:param name="direction" value="${model.direction}"/>
		</madsonic:url>

		<madsonic:url var="sortByLength" value="playlists.view">
		<madsonic:param name="listType" value="length"/>
		<madsonic:param name="direction" value="${model.direction}"/>
		</madsonic:url>

		<h2>
		<fmt:message key="playlists.sortby"/>&nbsp;&nbsp;<a href="${sortByName}"><fmt:message key="playlists.sortbyname"/></a> <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt=""> 
				 <a href="${sortByCreated}"><fmt:message key="playlists.sortbycreated"/></a> <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt=""> 
				 <a href="${sortByChanged}"><fmt:message key="playlists.sortbychanged"/></a> <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt=""> 
				 <a href="${sortByFiles}"><fmt:message key="playlists.sortbyfiles"/></a> <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt=""> 
				 <a href="${sortByLength}"><fmt:message key="playlists.sortbylength"/></a>
		</h2>
		</c:if>

		<br>

		<c:choose>
			<c:when test="${model.viewAs eq 'list'}">
				<table class="music">
					<c:forEach items="${model.playlists}" var="playlist">
						<tr>
							<td class="fit"><i class="fa fa-play clickable icon custom-icon" onclick="parent.playQueue.onPlayPlaylist(${playlist.id}, false)"></i></td>
							
							<td class="fit"><i class="fa fa-plus clickable icon custom-icon" onclick="parent.playQueue.onPlayPlaylist(${playlist.id}, true)" ></i></td>

							<c:if test="${model.user.downloadRole}">
							<td class="fit">
								<madsonic:url value="download.view" var="downloadUrl"><madsonic:param name="playlist" value="${playlist.id}"/></madsonic:url>
								<a href="${downloadUrl}"><i class="fa fa-download clickable icon custom-icon control"></i></a>
							</td>
							</c:if>					
							
							<c:if test="${playlist.isPublic()}">
							<td class="fit" style="padding-left:5px;"><i class="fa fa-globe custom-icon" title="public Playlist"></i></td>
							</c:if>
							
							<c:if test="${!playlist.isPublic()}">
							<td class="fit" style="padding-left:5px;"><i class="fa fa-lock custom-icon" title="private Playlist"></i></td>
							</c:if>
							
							<td class="truncate" style="padding-left:10px;"><a href="playlist.view?id=${playlist.id}">${fn:escapeXml(playlist.name)}</a></td>
							
							<td class="truncate" style="padding-left:10px;">${fn:escapeXml(playlist.comment)}</td>
							
							<td align="center" class="fit truncate detail" style="padding-left:20px;">${playlist.username}</td>
							
							<c:if test="${model.listType ne 'changed'}">
							<td align="center" class="fit detail" style="padding-left:20px;">
							<fmt:formatDate value="${playlist.created}" type="date" dateStyle="medium" var="creationDate"/>${creationDate} 
							<fmt:formatDate value="${playlist.created}" type="time" timeStyle="Short" var="creationTime"/>${creationTime}					
							</td>
							</c:if>
								
							<c:if test="${model.listType eq 'changed'}">
							<td align="center" class="fit detail" style="padding-left:20px;">					
							<fmt:formatDate value="${playlist.changed}" type="date" dateStyle="medium" var="changedDate"/>${changedDate} 
							<fmt:formatDate value="${playlist.changed}" type="time" timeStyle="Short" var="changedTime"/>${changedTime}					
							</td>
							</c:if>					
							
							<td align="center" class="fit rightalign detail" style="padding-left:20px;"><span class="online">${playlist.fileCount}</span> <img src="<spring:theme code="sepImage"/>" alt=""> <span class="offline">${playlist.offlineCount}</span></td>

							<td class="fit rightalign detail">${playlist.durationAsString}</td>
							
							<c:if test="${model.user.playlistRole}">
							<td class="fit" style="padding-right:5px;">
								<madsonic:url value="deletePlaylist.view" var="deleteUrl"><madsonic:param name="id" value="${playlist.id}"/></madsonic:url>
								<a href="javascript:void(0)" onClick="javascript:confirmAction(onDeletePlaylist,'${deleteUrl}');"><i class="fa fa-remove clickable icon custom-icon warning"></i></a>
							</td>
							</c:if>					
						</tr>
					</c:forEach>
				</table>
				<br>
			</c:when>
			<c:otherwise>
				<c:forEach items="${model.playlists}" var="playlist" varStatus="loopStatus">
					<c:set var="caption2">
						<i class="fa fa-file accent"></i> <span class="online">${playlist.fileCount}</span>|<span class="offline">${playlist.offlineCount}</span>&nbsp;&nbsp;<i class="fa fa-music accent"></i> ${playlist.durationAsString}
					</c:set>
					<c:set var="caption3">
						<i class="fa fa-calendar accent"></i> 
						<fmt:formatDate value="${playlist.created}" type="date" dateStyle="short" var="creationDate"/>${creationDate}
						<i class="fa fa-clock-o accent"></i> 
						<fmt:formatDate value="${playlist.created}" type="time" timeStyle="short" var="creationTime"/>${creationTime}
					</c:set>
					<div class="albumThumb">
						<c:import url="playlistArt.jsp">
							<c:param name="playlistId" value="${playlist.id}"/>
							<c:param name="coverArtSize" value="${model.coverArtSize}"/>
							<c:param name="coverArtHQ" value="${model.coverArtHQ}"/>             
							<c:param name="caption1" value="${fn:escapeXml(playlist.name)}"/>
							<c:param name="caption2" value="${caption2}"/>
							<c:param name="caption3" value="${caption3}"/>            
							<c:param name="captionCount" value="3"/>
							<c:param name="showLink" value="true"/>
							<c:param name="appearAfter" value="${loopStatus.count * 20}"/>
							<c:param name="showSmoother" value="true"/>
						</c:import>
					</div>
				</c:forEach>
			</c:otherwise>
		</c:choose>

		<div style="padding-top:1em; padding-bottom:2em">
			<span style="padding-right:3em"><i class="fa fa-music fa-lg fa-fw icon accent"></i>&nbsp;&nbsp;<a href="javascript:noop()" onclick="createEmptyPlaylist()"><fmt:message key="left.createplaylist"/></a></span>
			<span style="padding-right:3em"><i class="fa fa-download fa-lg fa-fw icon accent"></i>&nbsp;&nbsp;<a href="importPlaylist.view"><fmt:message key="left.importplaylist"/></a></span>
			<c:if test="${model.user.adminRole}">
			<span><i class="fa fa-cogs fa-lg fa-fw icon accent"></i>&nbsp;&nbsp;<a href="playlistSettings.view"><fmt:message key="left.playlistSettings"/></a></span>	
			</c:if>
		</div>

	</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">

    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:650, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:false,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>  

</body>    
</html>
