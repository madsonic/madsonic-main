<%--
  ~ This file is part of Madsonic.
  ~
  ~  Madsonic is free software: you can redistribute it and/or modify
  ~  it under the terms of the GNU General Public License as published by
  ~  the Free Software Foundation, either version 3 of the License, or
  ~  (at your option) any later version.
  ~
  ~  Madsonic is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~  Copyright 2015 (C) Sindre Mehus
  --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div style="float:right;padding-right:1em">
    <c:url value="main.view" var="changeGridViewUrl">
        <c:param name="id" value="${model.dir.id}"/>
    </c:url>
    
    <c:url value="main.view" var="changeListViewUrl">
        <c:param name="id" value="${model.dir.id}"/>
        <c:param name="type" value="default"/>
    </c:url>    
    
    <a href="${changeListViewUrl}"><img src="<spring:theme code="viewAsListImage"/>" alt="" style="margin-right:8px"/></a>
    <a href=""><img src="<spring:theme code="viewAsGridImage"/>" class="viewSelected" alt="" style="margin-right:8px"/></a>
  
</div>

