<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%@ include file="include.jsp" %>

<%--
PARAMETERS
  albumId: ID of album.
  playlistId: ID of playlist.  
  podcastChannelId: ID of podcast channel 
  coverArtSize: Height and width of cover art.
  typArtist: Default for Artist Cover
  typVideo: Default Video cover
  albumName: Album name to display as caption and img alt.
  artistName: Artistname
  showLink: Whether to make the cover art image link to the album page.
  showZoom: Whether to display a link for zooming the cover art.
  showArtist: Whether to display a link for changing the cover art.
  showChange: Whether to display a link for changing the cover art.
  showCaption: Whether to display the album name as a caption below the image.
  captionLength: Truncate caption after this many characters.
  appearAfter: Fade in after this many milliseconds, or nil if no fading in should happen.
  showPlayAlbum: Display PlayAlbum Button
  showAddAlbum: Display AddAlbum Button
  showTopTrack: Display TopTrack Button
  extraPadding: 8px bottom padding
  showSmoother: more opacity effect
--%>

<c:set var="height" value="${param.coverArtSize + 8}"/>
<c:set var="opacity" value="${empty param.appearAfter ? 1 : 0}"/>

<c:choose>
    <c:when test="${empty param.coverArtSize}">
        <c:set var="width" value="auto"/>
    </c:when>
    <c:otherwise>
        <c:set var="width" value="${param.coverArtSize + 8}"/>
    </c:otherwise>
</c:choose>

<c:if test="${param.typVideo}">
    <c:set var="height" value="${param.coverArtSize *1.35}"/>
</c:if>

<str:randomString count="5" type="alphabet" var="divId"/>
<str:randomString count="5" type="alphabet" var="imgId"/>
<str:randomString count="5" type="alphabet" var="playId"/>
<str:randomString count="5" type="alphabet" var="addId"/>
<str:randomString count="5" type="alphabet" var="topId"/>
<str:randomString count="5" type="alphabet" var="backId"/>
    
<div style="width:${width}px; max-width:${width}px; height:${height}px; max-height:${height}px;" title="${param.albumName}" id="${divId}">

    <c:if test="${not empty param.albumId}">
        <madsonic:url value="main.view" var="mainUrl">
            <madsonic:param name="id" value="${param.albumId}"/>
        </madsonic:url>
    </c:if>

    <c:if test="${not empty param.podcastChannelId}">
         <madsonic:url value="podcastChannel.view" var="mainUrl">
             <madsonic:param name="id" value="${param.podcastChannelId}"/>
         </madsonic:url>
    </c:if>
    
    <c:if test="${not empty param.playlistId}">
        <madsonic:url value="playlist.view" var="mainUrl">
            <madsonic:param name="id" value="${param.playlistId}"/>
        </madsonic:url>    
    </c:if>

    <madsonic:url value="/coverArt.view" var="coverArtUrl">
        <c:if test="${not empty param.albumId}">
            <madsonic:param name="id" value="${param.albumId}"/>
        </c:if>
            
        <c:if test="${not empty param.coverArtSize}">
            <madsonic:param name="size" value="${param.coverArtSize}"/>
        </c:if>
        <c:if test="${not empty param.podcastChannelId}">
            <madsonic:param name="id" value="pod-${param.podcastChannelId}"/>
        </c:if>
        <c:if test="${not empty param.playlistId}">
            <madsonic:param name="id" value="pl-${param.playlistId}"/>
        </c:if>        
        <c:if test="${param.typArtist eq 'true'}">
            <madsonic:param name="typArtist" value="${param.typArtist}"/>
        </c:if>
        <c:if test="${param.typVideo eq 'true'}">
            <madsonic:param name="typVideo" value="true"/>
        </c:if>        
    </madsonic:url>
    
    <madsonic:url value="/coverArt.view" var="zoomCoverArtUrl">
        <madsonic:param name="id" value="${param.albumId}"/>
    </madsonic:url>
    
    <div style="position: relative; width: 0; height: 0">
        <a href="#" onclick="top.playQueue.onPlayTopTrack('${param.artistName}'); return false;">
        <img src="icons/default/toptrack.png" id="${topId}" style="position: relative; top: ${param.coverArtSize -30}px; left: ${param.coverArtSize -75}px; z-index:100; display:none;" ></a>
    </div>    
    
    <div style="position: relative; width: 0; height: 0">
        <a href="#" onclick="top.playQueue.onPlay(${param.albumId}); return false;">
        <img src="icons/default/playalbum.png" id="${playId}" style="position: relative; top: ${param.coverArtSize -30}px; left: ${param.coverArtSize -52}px; z-index:100; display:none;" ></a>
    </div>
    
    <div style="position: relative; width: 0; height: 0">
        <a href="#" onclick="top.playQueue.onAdd(${param.albumId}); return false;">
        <img src="icons/default/addalbum.png" id="${addId}" style="position: relative; top: ${param.coverArtSize -30}px; left: ${param.coverArtSize -29}px; z-index:100; display:none;" ></a>
    </div>

    <c:choose>
        <c:when test="${param.showLink}"><a href="${mainUrl}" title="${param.albumName}"></c:when>
        <c:when test="${param.showZoom}"><a href="${zoomCoverArtUrl}" rel="zoom" title="${param.albumName}"></c:when>
    </c:choose>
    <div style="position: relative; width: 0; height: 0">
    
    <c:if test="${param.typVideo}">
        <img src="icons/default/test.png" id="${backId}" width="${param.coverArtSize +1}" height="${param.coverArtSize * 1.3 +1}" style="position: relative; bottom: 0px; left: 0px;z-index:90; display:none; opacity: 0.4;" > 
    </c:if>
    
    <c:if test="${not param.typVideo}">
        <img src="icons/default/test.png" id="${backId}" width="${param.coverArtSize +1}" height="${param.coverArtSize +1}" style="position: relative; bottom: 0px; left: 0px;z-index:90; display:none; opacity: 0.4;" > 
    </c:if>
    
    </div>
    <c:if test="${param.showLink or param.showZoom}"></a></c:if>
    
    <c:choose>
        <c:when test="${param.showLink}"><a href="${mainUrl}" title="${param.albumName}"></c:when>
        <c:when test="${param.showZoom}"><a href="${zoomCoverArtUrl}" rel="zoom" title="${param.albumName}"></c:when>
    </c:choose>
    <img src="${coverArtUrl}" id="${imgId}" style="display:none;" alt="${param.albumName}">
    <c:if test="${param.showLink or param.showZoom}"></a></c:if>
    
</div>

     <script type="text/javascript">

        $(document).ready(function () {
            setTimeout("$('#${imgId}').fadeIn(500)", ${param.appearAfter});
            
            <c:if test="${empty param.typArtist}">
                setTimeout("$('#${backId}').fadeIn(400)", ${param.appearAfter});            
            </c:if>
        });

         $("#${divId}").mouseenter(function () {

            <c:if test="${param.showPlayAlbum and not param.typVideo}">
                $("#${playId}").stop(1,1).fadeIn(600);
            </c:if>

            <c:if test="${param.showAddAlbum and not param.typVideo}">
                $("#${addId}").stop(1,1).fadeIn(500);
            </c:if>
            
            <c:if test="${param.showTopTrack or param.showArtist and not param.typVideo}">
                $("#${topId}").stop(1,1).fadeIn(700);
            </c:if>            
            
            <c:choose>
                <c:when test="${param.showSmoother eq true}">   $("#${backId}").stop(1,1).animate({opacity: 0.1}, 250);</c:when>
                <c:when test="${param.showSmoother eq false}">  $("#${backId}").stop(1,1).animate({opacity: 0.1}, 250); </c:when>
            </c:choose>
         });
         
        $("#${divId}").mouseleave(function () {
            $("#${topId}").stop(1,1).fadeOut(1400);
            $("#${playId}").stop(1,1).fadeOut(1200);
            $("#${addId}").stop(1,1).fadeOut(1000);
            
            
            <c:choose>
                <c:when test="${param.showSmoother eq true}"> $("#${backId}").stop(1.1).animate({opacity: 0.4}, 450); </c:when>
                <c:when test="${param.showSmoother eq false}"> $("#${backId}").stop(1.1).animate({opacity: 0.8}, 450); </c:when>
            </c:choose>            
            
        });

    </script> 

<div style="text-align:left;padding-left:2px; padding-top:2px;padding-bottom:6px;">

    <c:if test="${param.showArtist}">
        <madsonic:url value="/artist.view" var="ArtistUrl">
            <madsonic:param name="name" value="${param.artistName}"/>
            <madsonic:param name="showAlbum" value="true"/>
        </madsonic:url>
        <a class="detail" href="${ArtistUrl}"><img src="icons/default/artist.png" width="16" alt=""></a>
    </c:if>

    <c:if test="${param.showArtist and param.showChange}">
         
    </c:if>

    <c:if test="${param.showChange}">
        <madsonic:url value="/changeCoverArt.view" var="changeCoverArtUrl">
            <madsonic:param name="id" value="${param.albumId}"/>
            <c:if test="${param.showArtist and param.typArtist}">
                <madsonic:param name="isArtist" value="true"/>
            </c:if>            
            
        </madsonic:url>
        <a class="detail" href="${changeCoverArtUrl}"><img src="icons/default/camera.png" width="16" alt=""></a>
    </c:if>
    
    <c:if test="${param.showZoom}">
        <a class="detail" rel="zoom" title="${param.albumName}" href="${zoomCoverArtUrl}"> <img src="icons/default/display.png" width="16" alt=""> </a>
    </c:if>

    <c:choose>
        <c:when test="${fn:startsWith(param.albumName,'[')}">
            <c:if test="${not param.showZoom and not param.showChange and param.showCaption}">
                     <span class="detailmini"><str:truncateNicely upper="${param.captionLength}">${fn:split(param.albumName,']')[1]}</str:truncateNicely></span>
            </c:if>
        </c:when>
        <c:otherwise>
            <c:if test="${not param.showZoom and not param.showChange and param.showCaption}">
                <c:if test="${param.captionLength > 0}">
                     <span class="detailmini"><str:truncateNicely upper="${param.captionLength}">${param.albumName}</str:truncateNicely></span>
                </c:if>
            </c:if>
        </c:otherwise>
    </c:choose>
</div>