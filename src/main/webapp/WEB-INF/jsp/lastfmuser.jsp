<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    
    <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
    <c:if test="${model.customScrollbar}">
    <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>
    </c:if>
    <!-- .content_main{position:absolute; left:0px; top:0px; margin-left:10px; margin-top:5px; width:99%; height:95%; padding:0 0;overflow:auto;} -->

</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
	<img src="<spring:theme code="lastfmLargeImage"/>" width="32" alt="">
	LastFM Match
</h1>

<c:if test="${noLastFMUser}">
 no LastFM User config found
</c:if>	

<table width="80%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="col" align="left" style="vertical-align: top;">
		<h1>Loved Tracks</h1><br>
		<c:forEach items="${model.lovedTracks}" var="lovedTrack">

			<p class="dense">

			<madsonic:url value="main.view" var="mediaFileIdUrl">
				<madsonic:param name="id" value="${lovedTrack.id}"/>
			</madsonic:url>

			<c:if test="${lovedTrack.id == '0'}">
			<str:truncateNicely upper="30">${lovedTrack.artist}</str:truncateNicely>
			</c:if>	

			<c:if test="${lovedTrack.id != '0'}">
			<a target="main" href="${mediaFileIdUrl}"><str:truncateNicely upper="30">${lovedTrack.artist}</str:truncateNicely></a> 
			</c:if>	
			-
			<madsonic:url value="main.view" var="albumIdUrl">
				<madsonic:param name="id" value="${lovedTrack.trackNumber}"/>
			</madsonic:url>
			
 			<c:if test="${lovedTrack.trackNumber == '0'}">
			<str:truncateNicely upper="30">${lovedTrack.title}</str:truncateNicely>
			</c:if>	

			<c:if test="${lovedTrack.trackNumber != '0'}">

			<a target="main" href="${albumIdUrl}"><str:truncateNicely upper="30">${lovedTrack.title}</str:truncateNicely></a> 
			</c:if>	
			</p>

		</c:forEach>
		<!--
		<br><br>		
		<h1>Chart Tracks</h1><br>
			<c:forEach items="${model.chartTracks}" var="chartTrack">
			<p class="dense">
			<str:truncateNicely upper="35">${chartTrack}</str:truncateNicely><br>
			</p>
			</c:forEach>
		-->	
	</th>
    <th scope="col" align="left" style="vertical-align: top;">
		<h1>Top-Albums</h1><br>
		
			<c:forEach items="${model.topAlbums}" var="topAlbum">
			<p class="dense">

			<madsonic:url value="main.view" var="mediaFileIdUrl">
				<madsonic:param name="id" value="${topAlbum.id}"/>
			</madsonic:url>

			<c:if test="${topAlbum.id == '0'}">
			<str:truncateNicely upper="30">${topAlbum.artist}</str:truncateNicely>
			</c:if>	

			<c:if test="${topAlbum.id != '0'}">
			<a target="main" href="${mediaFileIdUrl}"><str:truncateNicely upper="30">${topAlbum.artist}</str:truncateNicely></a> 
			</c:if>	
			-
			<madsonic:url value="main.view" var="albumIdUrl">
				<madsonic:param name="id" value="${topAlbum.mediaFileId}"/>
			</madsonic:url>
			
 			<c:if test="${topAlbum.mediaFileId == '0'}">
			<str:truncateNicely upper="30">${topAlbum.name}</str:truncateNicely>
			</c:if>	

			<c:if test="${topAlbum.mediaFileId != '0'}">

			<a target="main" href="${albumIdUrl}"><str:truncateNicely upper="30">${topAlbum.name}</str:truncateNicely></a> 
			</c:if>	
			</p>
			</c:forEach>		
		
			<!--  		
			<c:forEach items="${model.topAlbums}" var="topAlbum">
			<str:truncateNicely upper="40">${topAlbum}</str:truncateNicely><br>
			</c:forEach> -->
	</th>
    <th scope="col" align="left" style="vertical-align: top;">
		<h1>Top-Artist</h1><br>
			<c:forEach items="${model.topArtists}" var="topArtist">
			<p class="dense">
		
			<madsonic:url value="main.view" var="mediaFileIdUrl">
				<madsonic:param name="id" value="${topArtist.id}"/>
			</madsonic:url>

			<c:if test="${topArtist.id == '0'}">
			<str:truncateNicely upper="30">${topArtist.name}</str:truncateNicely>
			</c:if>	

			<c:if test="${topArtist.id != '0'}">
			<a target="main" href="${mediaFileIdUrl}"><str:truncateNicely upper="30">${topArtist.name}</str:truncateNicely></a> 
			</c:if>	
			</p>
			
			</c:forEach>
		<br><br>	
		<h1>Top-Tags</h1><br>
			<c:forEach items="${model.topTags}" var="topTag">
			<p class="dense">
			<str:truncateNicely upper="30">${topTag}</str:truncateNicely><br>
			</p>
			</c:forEach>
	</th>
  </tr>
</table>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_home").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:990, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:false, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:20 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
                
            });
        })(jQuery);
    </script>
</c:if>
</body>
</html>