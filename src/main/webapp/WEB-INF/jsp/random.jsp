<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<html><head>
    <%@ include file="head.jsp" %>
	<%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>  
	
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script> 
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-1.4.2.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.dropdownReplacement-0.5.3.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/style/jquery.dropdownReplacement.css"/>" type="text/css">
	
    <style type="text/css">
    .list-boxes li {
      width: 12.5%;
      font-size: 12px;
      height: 65px;
      list-style-type: none;
      padding: 10px;
      line-height: 1.4;
      text-align: center;
    }
    </style>
    
	<script type="text/javascript">
    $(document).ready(function() {
        $("#sizeSelect").dropdownReplacement({selectCssWidth: 180, optionsDisplayNum: 15});
        $("#genreSelect").dropdownReplacement({selectCssWidth: 180, optionsDisplayNum: 15});
        $("#moodsSelect").dropdownReplacement({selectCssWidth: 180, optionsDisplayNum: 15});
        $("#yearSelect").dropdownReplacement({selectCssWidth: 180, optionsDisplayNum: 20});
        $("#musicFolderSelect").dropdownReplacement({selectCssWidth: 180, optionsDisplayNum: 10});
    });

	function playRandom() {
		var numsize = document.getElementsByName("size")[0].selectedIndex;
		var count = document.getElementsByName("size")[0].options[numsize].value;
		var numgenre = document.getElementsByName("genre")[0].selectedIndex;
		var genre = document.getElementsByName("genre")[0].options[numgenre].value;
		var nummood = document.getElementsByName("moods")[0].selectedIndex;
		var mood = document.getElementsByName("moods")[0].options[nummood].value;
		var numyear = document.getElementsByName("year")[0].selectedIndex;
		var year = document.getElementsByName("year")[0].options[numyear].value;
		var numfolderid = document.getElementsByName("musicFolderId")[0].selectedIndex;
		var musicFolderId = document.getElementsByName("musicFolderId")[0].options[numfolderid].value;
		parent.playQueue.onPlayRandomRadio(count, genre, mood, year, musicFolderId);
	}
	</script>	
</head>
<body class="mainframe bgcolor1"> 
 <div id="content_main" class="content_main"> 
 <div id="container" class="container">

<h1>
    <img id="pageimage" src="<spring:theme code="randomImage"/>" width="32" alt="" />
    <fmt:message key="more.random.title"/><span class="desc"></span>
</h1>
<div style="width:50em; max-width:50em">
    <div class="container-fluid"></div>
    <ul class="list-unstyled list-boxes" id="iconbar">
        <li><a class="text-center"><i class="fa fa-heartbeat fa-5x accent"></i> <br>AutoDJ</a></li>
    </ul>
</div>
<br>
<c:if test="${model.user.streamRole}">
		<form>	
        <table>
            <tr>
                <td style="min-width: 200px;"><fmt:message key="more.random.text"/></td>
                <td>
                    <select name="size" id="sizeSelect" style="display:none">
                        <option value="1"><fmt:message key="more.random.song"><fmt:param value="1"/></fmt:message></option>					
                        <option value="3"><fmt:message key="more.random.songs"><fmt:param value="3"/></fmt:message></option>
                        <option value="5"><fmt:message key="more.random.songs"><fmt:param value="5"/></fmt:message></option>
                        <option value="8"><fmt:message key="more.random.songs"><fmt:param value="8"/></fmt:message></option>
                        <option value="10"><fmt:message key="more.random.songs"><fmt:param value="10"/></fmt:message></option>
                        <option value="20" selected="true"><fmt:message key="more.random.songs"><fmt:param value="20"/></fmt:message></option>
                        <option value="30"><fmt:message key="more.random.songs"><fmt:param value="30"/></fmt:message></option>
                        <option value="50"><fmt:message key="more.random.songs"><fmt:param value="50"/></fmt:message></option>
                            <option value="80"><fmt:message key="more.random.songs"><fmt:param value="80"/></fmt:message></option>
                            <option value="90"><fmt:message key="more.random.songs"><fmt:param value="90"/></fmt:message></option>
                            <option value="100"><fmt:message key="more.random.songs"><fmt:param value="100"/></fmt:message></option>
                            <option value="150"><fmt:message key="more.random.songs"><fmt:param value="150"/></fmt:message></option>
                            <option value="200"><fmt:message key="more.random.songs"><fmt:param value="200"/></fmt:message></option>
                            <option value="250"><fmt:message key="more.random.songs"><fmt:param value="250"/></fmt:message></option>
                    </select>
                </td>
            </tr>
            <tr>			
                <td><fmt:message key="more.random.year"/></td>
                <td>
                    <select name="year" id="yearSelect" style="display:none">
                        <option value="any"><fmt:message key="more.random.anyyear"/></option>

                        <c:forEach begin="0" end="${model.currentYear - 2005}" var="yearOffset">
                            <c:set var="year" value="${model.currentYear - yearOffset}"/>
                            <option value="${year} ${year}">${year}</option>
                        </c:forEach>
                        <option value="2015 2020">2015 &ndash; 2020</option>
                        <option value="2010 2015">2010 &ndash; 2015</option>
                        <option value="2000 2010">2000 &ndash; 2010</option>
                        <option value="1990 2000">1990 &ndash; 2000</option>
                        <option value="1980 1990">1980 &ndash; 1990</option>
                        <option value="1970 1980">1970 &ndash; 1980</option>
                        <option value="1960 1970">1960 &ndash; 1970</option>
                        <option value="1950 1960">1950 &ndash; 1960</option>
                        <option value="0 1949">&lt; 1950</option>
                        <option value="0 1900">&lt; 1900</option>                        
                    </select>
                </td>
            </tr>            
            <tr>			
                <td><fmt:message key="more.random.genre"/></td>
                <td>
                    <select name="genre" id="genreSelect" style="display:none">
                        <option value="any"><fmt:message key="more.random.anygenre"/></option>
                        <c:forEach items="${model.genres}" var="genre">
                            <option value="${genre.name}"><str:truncateNicely upper="20">${genre.name} (${genre.songCount})</str:truncateNicely></option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>			
                <td><fmt:message key="more.random.moods"/></td>
                <td>
                    <select name="moods" id="moodsSelect" style="display:none">
                        <option value="any"><fmt:message key="more.random.anymoods"/></option>
                        <c:forEach items="${model.moods}" var="moods">
                            <option value="${moods}"><str:truncateNicely upper="20">${moods}</str:truncateNicely></option>
                        </c:forEach>
                    </select>
                </td>
            </tr>			
            <tr>			
                <td><fmt:message key="more.random.folder"/></td>
                <td>
                    <select name="musicFolderId" id="musicFolderSelect" style="display:none">
                        <option value="-1"><fmt:message key="more.random.anyfolder"/></option>
                        <c:forEach items="${model.musicFolders}" var="musicFolder">
                                <option value="${musicFolder.id}">${musicFolder.name}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
			
            <tr>
			<td></td>
            </tr>
			
            <tr>	
                <td>
				</td>				
                <td>
				<br>
					<input type="button" value="Play Random Radio!" onClick="playRandom();">
                </td>
            </tr>
        </table>
    </form>
</c:if>

</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:880, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$(".content_main").resize(function(e){
    $(".content_main").mCustomScrollbar("update");
});
</script>
</c:if>   
</body>
</html>