<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
	
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
    <img src="<spring:theme code="chartsImage"/>" width="32" alt="">
    Server Status
    <!-- <fmt:message key="status.title"/> -->
</h1>

<table width="98%" class="ruleTable indent">

        <madsonic:url value="/folderChart.view" var="chartUrl1">
            <madsonic:param name="type" value="song"/>
        </madsonic:url>

        <madsonic:url value="/folderChart.view" var="chartUrl2">
            <madsonic:param name="type" value="songSize"/>
        </madsonic:url>

        <madsonic:url value="/folderChart.view" var="chartUrl3">
            <madsonic:param name="type" value="video"/>
        </madsonic:url>

        <madsonic:url value="/folderChart.view" var="chartUrl4">
            <madsonic:param name="type" value="videoSize"/>
        </madsonic:url>

        <madsonic:url value="/folderChart.view" var="chartUrl5">
            <madsonic:param name="type" value="podcast"/>
        </madsonic:url>

        <madsonic:url value="/folderChart.view" var="chartUrl6">
            <madsonic:param name="type" value="podcastSize"/>
        </madsonic:url>

        <madsonic:url value="/folderChart.view" var="chartUrl7">
            <madsonic:param name="type" value="audiobook"/>
        </madsonic:url>        

        <madsonic:url value="/folderChart.view" var="chartUrl8">
            <madsonic:param name="type" value="audiobookSize"/>
        </madsonic:url>        

        <madsonic:url value="/folderChart.view" var="chartUrl9">
            <madsonic:param name="type" value="album"/>
        </madsonic:url>        
        
        <tr>
        <td style="width:100px; border: 1px;">Albums</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl9}" alt=""></td>
        </tr>
        <tr>
        <td>Audio Files</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl1}" alt=""></td>
        </tr>
        <tr>
        <td>Audio FileSize (MB)</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl2}" alt=""></td>
        </tr>
        <tr>
        <td>Video Files</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl3}" alt=""></td>
        </tr>
        <tr>
        <td>Video Filesize (MB)</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl4}" alt=""></td>
        </tr>
        <tr>
        <td>Podcasts</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl5}" alt=""></td>
        </tr>
        <tr>
        <td>Podcasts Filesize (MB)</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl6}" alt=""></td>
        </tr>
        <tr>
        <td>Audiobooks</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl7}" alt=""></td>
        </tr>
        <tr>
        <td>Audiobooks Filesize (MB)</td>
            <td style="border: 1px;" class="ruleTableCell" width="${model.chartWidth * 0.8}"><img width="${model.chartWidth * 0.8}" height="${model.chartHeight}" src="${chartUrl8}" alt=""></td>
        </tr>
        
</table>
<div class="forward"><a href="serverStatus.view?"><fmt:message key="common.refresh"/></a></div>
<!-- CONTENT -->
</div>

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:900, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:false, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:10 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$(".content_main").resize(function(e){
    $(".content_main").mCustomScrollbar("update");
});
</script>
</c:if>   
</body>
</html>