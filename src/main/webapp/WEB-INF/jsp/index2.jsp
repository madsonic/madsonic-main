<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<html>
<head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <%@ include file="bootstrap.jsp" %>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">       
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
</head>
<style>
.jumbotron {
    padding-top: 30px;
    padding-bottom: 30px;
    margin-bottom: 30px;
    color: inherit;
    background-color: #111;
}

body {
    color: #eee;
}

.btn-warning {
    color: #fff;
    background-color: #f0ad4e;
    border-color: #eea236;
}

a:link, a:active, a:visited, a:link *, a:active *, a:visited * {
    color: #fff;
}
</style>
<body class="bgcolor2 index">

   
   <!-- Navigation -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
        
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">
                  <img src="icons/madsonic/icon.png" width="30px" alt="">
                </a>
            </div>
            
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#settings">Settings</a>
                    </li>
					
                    <li>
                        <a href="#profil"> Profil</a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Support <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#version">version</a>
                            </li>
                            <li>
                                <a href="#contact">about</a>
                            </li>     
                            </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </div>



    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <img src="<spring:theme code="logoImage"/>" width="250px" alt="">  

        <h2>Hello, world!</h2>
        <p>This is a simple template test.</p>
      </div>
    </div>    
    
    <div class="container">
      <div class="jumbotron">
        <h2>Madsonic</h2>
        <p>Resize this responsive page to see the effect!</p> 
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
      
      </div>
      
        <!-- Call to Action Section -->
        <div class="jumbotron">
            <div class="row">
                <div class="col-md-10">
                    <p>Take a closer look ...</p>
                </div>

                <div class="col-md-3" style="margin-top:5px;">
                    <a class="btn btn-lg btn-default btn-block btn-warning" href="#index">Index</a>
                </div>
				
                <div class="col-md-3" style="margin-top:5px;">
                    <a class="btn btn-lg btn-default btn-block btn-info" href="#artists">Artists</a>
                </div>
				
				<div class="col-md-3" style="margin-top:5px;">
					<a class="btn btn-lg btn-success btn-block" href="#download">Download</a>
				</div>				
            </div>
        </div>	
      
      
      <div class="jumbotron">
        <h4>Audio Controls</h4>
        <p> 
        <i id="cast-idle" class="material-icons" title="<fmt:message key="videoPlayer.cast"/>">cast</i>
        <i id="cast-active" class="material-icons" title="<fmt:message key="videoPlayer.cast"/>">cast_connected</i>
        <i id="cc-on" class="material-icons" title="<fmt:message key="videoPlayer.subtitles"/>">closed_caption</i>
        <i id="cc-off" class="material-icons" title="<fmt:message key="videoPlayer.subtitles"/>">closed_caption</i>
        <i id="fullscreen" class="material-icons" title="<fmt:message key="videoPlayer.fullscreen"/>">fullscreen</i>
        <i id="new-window" class="material-icons" title="<fmt:message key="videoPlayer.newwindow"/>">open_in_new</i>
        <i id="share" class="material-icons" title="<fmt:message key="main.sharealbum"/>">share</i>
        <i id="download" class="material-icons" title="<fmt:message key="common.download"/>">file_download</i>
        </p>
        <select id="audio-track">
            <option value="1">Language: en</option>
            <option value="1">Language: fr</option>
            <option value="1">Language: de</option>
        </select>
      </div>
      
      <div class="jumbotron">      
          <div class="row">      
            <div class="col-sm-12">
              <h3>Player</h3>
                <p>
                    <span id="fastpreviousButton" class="fa-stack fa-lg" onclick="">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-fast-backward fa-stack-1x fa-inverse"></i>
                    </span> 

                    <span id="previousButton" class="fa-stack fa-lg" onclick="">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-step-backward fa-stack-1x fa-inverse"></i>
                    </span> 

                    <span id="startButton" class="fa-stack fa-lg" onclick="">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-play-circle fa-stack-2x"></i>
                    </span>
            
                    <span id="stopButton" class="fa-stack fa-lg" onclick="">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-pause-circle fa-stack-2x"></i>
                    </span>
            
                    <span id="bufferButton" class="fa-stack fa-lg" onclick="">
                        <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                        <i class="fa fa-refresh fa-stack-1x fa-spin"></i>
                    </span>
            
                    <span id="nextButton" class="fa-stack fa-lg" onclick="">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-step-forward fa-stack-1x fa-inverse"></i>
                    </span>             

                    <span id="fastnextButton" class="fa-stack fa-lg" onclick="">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-fast-forward fa-stack-1x fa-inverse"></i>
                    </span>                           
                </p>              
              
            </div>
          </div>
      </div>
      
      <div class="row">
        <div class="col-sm-4">
          <h3>Column 1</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
        </div>
        <div class="col-sm-4">
          <h3>Column 2</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
        </div>
        <div class="col-sm-4">
          <h3>Column 3</h3> 
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
        </div>
      </div>
    </div>
    
</body>
</html>

