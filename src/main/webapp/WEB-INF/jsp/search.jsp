<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--@elvariable id="command" type="org.madsonic.command.SearchCommand"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    <script type="text/javascript" language="javascript">
        function showMoreArtists() {
            $('.artistRow').show(); $('#moreArtists').hide();
        }
        function showMoreAlbums() {
            $('.albumRow').show(); $('#moreAlbums').hide();
        }
        function showMoreSongs() {
            $('.songRow').show();$('#moreSongs').hide();
        }
    </script>
</head>
<body class="mainframe bgcolor1">

<h1>
    <img src="<spring:theme code="searchImage"/>" width="32" alt=""/>
    <fmt:message key="search.title"/>
</h1>

<div class="searchbox">

<c:if test="${command.inputVisible}">
<form:form commandName="command" method="post" action="search.view?&input=visible" name="searchForm">
    <table>
        <tr>
            <td><fmt:message key="search.query"/></td>
            <td style="padding-left:0.25em"><form:input path="query" size="50" style="width: 300px;"/></td>
            <td style="padding-left:0.25em"><input type="submit" onclick="search(0)" value="<fmt:message key="search.search"/>"/></td>
        </tr>
    </table>
</form:form>
</c:if>

<c:if test="${command.indexBeingCreated}">
    <p class="warning"><fmt:message key="search.index"/></p>
</c:if>

<c:if test="${not command.indexBeingCreated and empty command.artists and empty command.albums and empty command.songs and empty command.videos}">
    <p class="warning"><fmt:message key="search.hits.none"/></p>
</c:if>

<c:if test="${not empty command.artists}">
    <h1 style="padding:20px 0 20px 0;"><fmt:message key="search.hits.artists"/></h1>    
    
    <table class="music content" style="border-collapse:collapse">
        <c:forEach items="${command.artists}" var="match" varStatus="loopStatus">

            <madsonic:url value="/main.view" var="mainUrl">
                <madsonic:param name="id" value="${match.id}"/>
            </madsonic:url>

            <tr class="artistRow" ${loopStatus.count > 6 ? "style='display:none'" : ""}>
                <td class="fit" style="padding-left:0.5em;padding-right:1.5em;">
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${match.id}"/>
                    <c:param name="playEnabled" value="${command.user.streamRole and not command.partyModeEnabled}"/>
                    <c:param name="addEnabled" value="${command.user.streamRole and (not command.partyModeEnabled or not match.directory)}"/>
                    <c:param name="downloadEnabled" value="${command.user.downloadRole and not command.partyModeEnabled}"/>
                    <c:param name="starEnabled" value="false"/>
                    <c:param name="starred" value="${not empty match.starredDate}"/>
                    <c:param name="video" value="${match.video and model.player.web}"/>
                    <c:param name="asTable" value="false"/>
                </c:import>
                </td>
                <td class="fit bgcolor2">
                    <c:import url="coverArtThumb.jsp">
                        <c:param name="albumId" value="${match.id}"/>
                        <c:param name="auth" value="${match.hash}"/>
                        <c:param name="artistName" value="${match.name}"/>
                        <c:param name="coverArtSize" value="80"/>
                        <c:param name="scale" value="0.5"/>
                        <c:param name="showLink" value="true"/>
                        <c:param name="showZoom" value="false"/>
                        <c:param name="showChange" value="false"/>
                        <c:param name="showArtist" value="false"/>
                        <c:param name="typArtist" value="true"/>
                        <c:param name="appearAfter" value="5"/>
                        
                    </c:import>
                </td>
        
                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1.75em;padding-right:1.25em">
                    <a href="${mainUrl}">${match.name}</a>
                </td>
            </tr>

            </c:forEach>
    </table>
    <c:if test="${fn:length(command.artists) gt 6}">
        <div id="moreArtists" class="forward" style="margin-left:8px;"><a href="javascript:showMoreArtists()"><fmt:message key="search.hits.more"/></a></div>
    </c:if>
</c:if>

<c:if test="${not empty command.albums}">
    <h1 style="padding:30px 0 15px 0;"><fmt:message key="search.hits.albums"/></h1>    
    
    <table class="music content" style="border-collapse:collapse">
        <c:forEach items="${command.albums}" var="match" varStatus="loopStatus">

            <madsonic:url value="/main.view" var="mainUrl">
                <madsonic:param name="id" value="${match.id}"/>
            </madsonic:url>

            <tr class="albumRow" ${loopStatus.count > 8 ? "style='display:none'" : ""}>
                <td class="fit" style="padding-left:0.5em;padding-right:1.5em;">
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${match.id}"/>
                    <c:param name="playEnabled" value="${command.user.streamRole and not command.partyModeEnabled}"/>
                    <c:param name="addEnabled" value="${command.user.streamRole and (not command.partyModeEnabled or not match.directory)}"/>
                    <c:param name="downloadEnabled" value="${command.user.downloadRole and not command.partyModeEnabled}"/>
                    <c:param name="starEnabled" value="false"/>
                    <c:param name="starred" value="${not empty match.starredDate}"/>
                    <c:param name="video" value="${match.video and model.player.web}"/>
                    <c:param name="asTable" value="false"/>
                </c:import>
                </td>
                <td class="fit bgcolor2">
                    <c:import url="coverArtThumb.jsp">
                        <c:param name="albumId" value="${match.id}"/>
                        <c:param name="auth" value="${match.hash}"/>
                        <c:param name="artistName" value="${match.name}"/>
                        <c:param name="coverArtSize" value="80"/>
                        <c:param name="scale" value="0.5"/>
                        <c:param name="showLink" value="true"/>
                        <c:param name="showZoom" value="false"/>
                        <c:param name="showChange" value="false"/>
                        <c:param name="showArtist" value="false"/>
                        <c:param name="typArtist" value="true"/>
                        <c:param name="appearAfter" value="5"/>
                        
                    </c:import>
                </td>
                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:1.75em;padding-right:1.25em">
                    <a href="${mainUrl}"><str:truncateNicely upper="55">${match.albumSetName}</str:truncateNicely></a>
                </td>
                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1.75em;padding-right:1.25em">
                    <span class="detail"><str:truncateNicely upper="55">${match.artist}</str:truncateNicely></span>
                </td>
            </tr>
            </c:forEach>
    </table>
    <c:if test="${fn:length(command.albums) gt 8}">
        <div id="moreAlbums" class="forward" style="margin-left:8px;"><a href="javascript:showMoreAlbums()"><fmt:message key="search.hits.more"/></a></div>
    </c:if>
</c:if>

<c:if test="${not empty command.songs}">
    <h1 style="padding:30px 0 15px 0;"><fmt:message key="search.hits.songs"/></h1>
    
    <table class="music content" style="border-collapse:collapse; empty-cells: show;">
        <c:forEach items="${command.songs}" var="match" varStatus="loopStatus">
        
            <madsonic:url value="/main.view" var="mainUrl">
                <madsonic:param name="path" value="${match.parentPath}"/>
            </madsonic:url>
            <tr class="songRow" ${loopStatus.count > 20 ? "style='display:none'" : ""}>
                <td class="fit" style="padding-left:0.5em;padding-right:1.5em;">
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${match.id}"/>
                    <c:param name="playEnabled" value="${command.user.streamRole and not command.partyModeEnabled}"/>
                    <c:param name="addEnabled" value="${command.user.streamRole and (not command.partyModeEnabled or not match.directory)}"/>
                    <c:param name="downloadEnabled" value="${command.user.downloadRole and not command.partyModeEnabled}"/>
                    <c:param name="starEnabled" value="false"/>
                    <c:param name="starred" value="${not empty match.starredDate}"/>
                    <c:param name="video" value="${match.video}"/>
                    <c:param name="asTable" value="false"/>
                </c:import>
                </td>                                
                <td class="fit bgcolor2">                                
                    <c:import url="coverArtThumb.jsp">
                        <c:param name="albumId" value="${match.id}"/>
                        <c:param name="auth" value="${match.hash}"/>
                        <c:param name="artistName" value="${match.name}"/>
                        <c:param name="coverArtSize" value="80"/>
                        <c:param name="scale" value="0.5"/>
                        <c:param name="showLink" value="true"/>
                        <c:param name="showZoom" value="false"/>
                        <c:param name="showChange" value="false"/>
                        <c:param name="showArtist" value="false"/>
                        <c:param name="typArtist" value="true"/>
                        <c:param name="appearAfter" value="5"/>
                        
                    </c:import>
                </td>
                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:1.75em;padding-right:1.25em">
                    <str:truncateNicely upper="60">${match.title}</str:truncateNicely>
        </td>

                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-right:1.75em">
                    <a href="${mainUrl}"><span class="detail"><str:truncateNicely upper="60">${match.albumName}</str:truncateNicely></span></a>
                </td>

                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.75em">
                    <span class="detail"><str:truncateNicely upper="60">${match.artist}</str:truncateNicely></span>
                </td>
            </tr>

            </c:forEach>
    </table>
<c:if test="${fn:length(command.songs) gt 15}">
    <div id="moreSongs" class="forward" style="margin-left:8px;"><a href="javascript:showMoreSongs()"><fmt:message key="search.hits.more"/></a></div>
</c:if>
</c:if>

<c:if test="${not empty command.videos}">
    <h1 style="padding:30px 0 15px 0;">Videos</h1>
    
    <table class="music content" style="border-collapse:collapse; empty-cells: show;">
        <c:forEach items="${command.videos}" var="match" varStatus="loopStatus">

            <madsonic:url value="/main.view" var="mainUrl">
                <madsonic:param name="path" value="${match.parentPath}"/>
            </madsonic:url>

            <tr class="songRow" ${loopStatus.count > 20 ? "style='display:none'" : ""}>
                    <td class="fit" style="padding-left:0.5em;width:83px;">
                    <c:import url="playAddDownload.jsp">
                        <c:param name="id" value="${match.id}"/>
                        <c:param name="playEnabled" value="${command.user.streamRole and not command.partyModeEnabled}"/>
                        <c:param name="addEnabled" value="${command.user.streamRole and (not command.partyModeEnabled or not match.directory)}"/>
                        <c:param name="downloadEnabled" value="${command.user.downloadRole and not command.partyModeEnabled}"/>
                        <c:param name="starEnabled" value="false"/>
                        <c:param name="starred" value="${not empty match.starredDate}"/>
                        <c:param name="video" value="${match.video and command.player.web}"/>
                        <c:param name="asTable" value="false"/>
                    </c:import>
                    </td>
                    <td class="fit bgcolor2">
                        <c:import url="coverArtThumb.jsp">
                            <c:param name="albumId" value="${match.id}"/>
                            <c:param name="auth" value="${match.hash}"/>
                            <c:param name="artistName" value="${match.name}"/>
                            <c:param name="coverArtSize" value="80"/>
                            <c:param name="scale" value="0.5"/>
                            <c:param name="showLink" value="true"/>
                            <c:param name="showZoom" value="false"/>
                            <c:param name="showChange" value="false"/>
                            <c:param name="showArtist" value="false"/>
                            <c:param name="typArtist" value="true"/>
                            <c:param name="appearAfter" value="5"/>
                        </c:import>
                    </td>

                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:1.75em;padding-right:1.25em">
                    <str:truncateNicely upper="60">${match.title}</str:truncateNicely>
                </td>

                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-right:1.75em">
                    <a href="${mainUrl}"><span class="detail"><str:truncateNicely upper="60">${match.albumName}</str:truncateNicely></span></a>
                </td>

                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.75em">
                    <span class="detail"><str:truncateNicely upper="60">${match.artist}</str:truncateNicely></span>
                </td>
            </tr>

            </c:forEach>
    </table>
    <c:if test="${fn:length(command.videos) gt 15}">
        <div id="moreVideos" class="forward" style="margin-left:8px;"><a href="#" onclick="$('.songRow').show();$('#moreVideos').hide(); "><fmt:message key="search.hits.more"/></a></div>
    </c:if>
</c:if>

<div>

</body></html>