<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->
 
<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="folder"/>
    <c:param name="toast" value="${model.toast}"/>
    <c:param name="done" value="${model.done}"/>
    <c:param name="warn" value="${model.warn}"/>
    <c:param name="warnInfo" value="${model.warnInfo}"/>
    <c:param name="bug" value="${model.bug}"/>	
    <c:param name="bugInfo" value="${model.bugInfo}"/>		
</c:import>
<c:if test="${model.scanning}">
	<p class="warning" style="width:65%"><i class="fa fa-info-circle fa-lg fa-fw icon" style="color: #FFC107"></i> <fmt:message key="musicfoldersettings.nowscanning"/></p>
</c:if>
<br>

<p class="forward"><a href="folderSettings.view?scanNow">Normal Rescan</a></p>

<p class="detail" style="padding-left: 20px;width:65%;white-space:normal;margin-top:-10px;"><fmt:message key="cleanupsettings.scannow"/></p> 
<br>
<p class="forward"><a href="folderSettings.view?FullscanNow"><fmt:message key="cleanupsettings.fullscan.title"/></a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-10px;"><fmt:message key="cleanupsettings.fullscan"/></p> 
<br>
<p class="forward"><a href="folderSettings.view?FullCleanupNow"><fmt:message key="cleanupsettings.fullcleanupscan.title"/></a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-10px;"><fmt:message key="cleanupsettings.fullcleanupscan"/></p> 
<br>
<p class="forward"><a href="folderSettings.view?expunge"><fmt:message key="cleanupsettings.expunge.title"/></a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-10px;"><fmt:message key="cleanupsettings.expunge"/></p>
<br>
<p class="forward"><a href="folderSettings.view?defrag"><fmt:message key="cleanupsettings.defrag.title"/></a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-10px;"><fmt:message key="cleanupsettings.defrag"/></p>

<c:if test="${not empty model.error}">
    <p class="warning"><fmt:message key="${model.error}"/></p>
</c:if>

<c:if test="${model.reload}">
    <script language="javascript" type="text/javascript">parent.frames.leftPanel.location.href="leftPanel.view?"</script>
</c:if>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">    

        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/       
                    alwaysShowScrollbar:true,
                    advanced:{ autoExpandHorizontalScroll: true },                
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);
        
        $("#content_main").mCustomScrollbar("update");


$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</body>

</html>