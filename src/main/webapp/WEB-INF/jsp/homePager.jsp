<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%--
PARAMETERS
  cat: cateagory
--%>

		<table>
        
		<tr>
       <c:if test="${model.listType ne 'decade' and model.listType ne 'genre' and model.listType ne 'tip'}">        
		<td>
        <i class="fa fa-folder-open-o fa-fw icon"></i>    
            <select name="musicFolderId" id="musicFolder" style="margin-right:20px;display:none;">
                <option value="-1"><fmt:message key="left.allfolders"/></option>
                
                <c:if test="${model.MusicFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -2 ? "selected" : ""} value="-2"><fmt:message key="left.allmusic"/></option>            
                </c:if>
                <c:if test="${model.VideoFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -3 ? "selected" : ""} value="-3"><fmt:message key="left.allvideos"/></option>
                </c:if>
                <c:if test="${model.MoviesFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -4 ? "selected" : ""} value="-4"><fmt:message key="left.allmovies"/></option>
                </c:if>
                <c:if test="${model.SeriesFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -5 ? "selected" : ""} value="-5"><fmt:message key="left.allseries"/></option>
                </c:if>
                <c:if test="${model.ImagesFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -6 ? "selected" : ""} value="-6"><fmt:message key="left.allimages"/></option>
                </c:if>
                <c:if test="${model.TVFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -7 ? "selected" : ""} value="-7"><fmt:message key="left.alltv"/></option>
                </c:if>
                <c:if test="${fn:length(model.musicFolders) > 0}">
                <option value="-1">------------</option>
                </c:if>
                <c:forEach items="${model.musicFolders}" var="musicFolder">
                    <option ${model.selectedMusicFolder.id == musicFolder.id ? "selected" : ""} value="${musicFolder.id}">${musicFolder.name}</option>
                </c:forEach>
            </select>
		</td>        
        </c:if>        

        
		   <c:if test="${model.listType eq 'decade'}">
				<td style="padding-right: 1em">
					<fmt:message key="home.decade.text"/>
				</td>
				<td>
					<select name="decade" id="decadeSelect" class="inputWithIcon vcenter" style="display:none">
						<c:forEach items="${model.decades}" var="decade">
							<option
								${decade eq model.decade ? "selected" : ""} value="${decade}">${decade}</option>
						</c:forEach>
					</select>
				</td>
			</c:if>
			<c:if test="${model.listType eq 'genre'}">
				<td style="padding-right: 1em">
					<fmt:message key="home.genre.text"/>
				</td>
				<td>
					<select name="genre" id="genre" class="inputWithIcon vcenter" style="display:none">
						<c:forEach items="${model.genres}" var="genre">
							<option ${genre.name eq model.genre ? "selected" : ""} value="${genre.name}">${genre.name} (${genre.albumCount})</option>
						</c:forEach>
					</select>
				</td>
			</c:if>
            <c:if test="${model.listType ne 'genre' && model.listType ne 'decade'}">
			<td class="accent" style="font-weight:bold;font-size:14px;padding: 0 10px 4px 10px;">${listTypeCat}</td>
			</c:if>
			<c:choose>
			<c:when test="${model.listType eq 'random'}">
				<td><i class="fa fa-chevron-right icon control"></i>&nbsp;<a href="home.view?listType=random"><fmt:message key="common.more"/></a></td>
			</c:when>
			
			<c:otherwise>
			
				<madsonic:url value="home.view" var="previousUrl">
					<madsonic:param name="listType" value="${model.listType}"/>
					<c:if test="${model.listType eq 'genre'}">
						<madsonic:param name="genre" value="${model.genre}"/>
					</c:if>					
					<madsonic:param name="listOffset" value="${model.listOffset - model.listSize}"/>
				</madsonic:url>
				
				<madsonic:url value="home.view" var="nextUrl">
					<madsonic:param name="listType" value="${model.listType}"/>
					<c:if test="${model.listType eq 'genre'}">
						<madsonic:param name="genre" value="${model.genre}"/>
					</c:if>
					<madsonic:param name="listOffset" value="${model.listOffset + model.listSize}"/>
				</madsonic:url>
				
				<c:if test="${model.listOffset gt 0}">
					<td style="padding-left:1.5em;padding-right:1.5em"><i class="fa fa-chevron-left icon control"></i>&nbsp;<a href="${previousUrl}"><fmt:message key="common.previous"/></a></td>
                </c:if>

				<c:if test="${model.listType eq 'allArtist' || model.listType eq 'starredArtist' }"> 
					<td style="padding-left:1.5em;padding-right:1.5em"><fmt:message key="home.artists"><fmt:param value="${model.listOffset + 1}"/><fmt:param value="${model.listOffset + model.listSize}"/></fmt:message></td>
				</c:if>
	
				<c:if test="${model.listType ne 'allArtist' && model.listType ne 'starredArtist' }">
					<td style="padding-left:1.5em;padding-right:1.5em"><fmt:message key="home.albums"><fmt:param value="${model.listOffset + 1}"/><fmt:param value="${model.listOffset + model.listSize}"/></fmt:message></td>
				</c:if>
				
                <c:if test="${fn:length(model.albums) eq model.listSize}">
                    <td><i class="fa fa-chevron-right icon control"></i>&nbsp;<a href="${nextUrl}"><fmt:message key="common.next"/></a></td>
                </c:if>
            </c:otherwise>
        </c:choose>

            </tr>
        </table>
