<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>

<html>
<head>
    <%@ include file="head.jsp" %>
    <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">    
</head>

<body class="mainframe bgcolor1">

<div id="content_main" class="content_main">

<h1>
    <img src="<spring:theme code="errorImage"/>" alt=""/>
    <fmt:message key="notFound.title"/>
</h1>

    <p style="padding:10px;">
	<fmt:message key="notFound.text"/>
    </p>
	
	<br>
<div class="forward" style="float:left;padding-right:10pt"><a href="javascript:top.location.reload(true)"><fmt:message key="notFound.reload"/></a></div>
<div class="forward" style="float:left"><a href="musicFolderSettings.view"><fmt:message key="notFound.scan"/></a></div>

</div>
</body>
</html>