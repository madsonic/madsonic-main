<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <%@ include file="customScrollbar.jsp" %>     
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->
 
<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="api"/>
    <c:param name="toast" value="${model.toast}"/>
</c:import>

    <br>
    <div>
        <input type="checkbox" name="emulatorEnabled" id="emulatorEnabled" class="checkbox" 
        <c:if test="${model.emulatorEnabled}">checked="checked" disabled="disabled"</c:if>/>
        <label for="emulatorEnabled"><fmt:message key="apisettings.enabled"/></label>
    </div>
    <br>
    <p class="detail" style="width:60%;white-space:normal">
        <fmt:message key="apisettings.description"/>
    </p>

    <c:set var="licenseInfo" value="${model.licenseInfo}"/>
    <%@ include file="licenseNotice.jsp" %>

</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">
    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>    

</body>
</html>