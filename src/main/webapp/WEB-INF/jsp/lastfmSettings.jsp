<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="lastfm"/>
    <c:param name="toast" value="${model.toast}"/>
    <c:param name="done" value="${model.done}"/>
    <c:param name="warn" value="${model.warn}"/>
    <c:param name="warnInfo" value="${model.warnInfo}"/>
    <c:param name="bug" value="${model.bug}"/>	
    <c:param name="bugInfo" value="${model.bugInfo}"/>	
</c:import>
<br>
<p class="forward"><a href="lastfmSettings.view?ScanNow"><fmt:message key="lastfmSettings.artistcover.title"/></a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="lastfmSettings.artistcover"/></p>
<br><!--
<p class="forward"><a href="lastfmSettings.view?ScanInfo"><fmt:message key="lastfmSettings.artistsummary.title"/> (Full)</a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="lastfmSettings.artistsummaryinfo1"/></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="lastfmSettings.artistsummaryinfo2"/></p>
<br>
<p class="forward"><a href="lastfmSettings.view?ScanNewInfo"><fmt:message key="lastfmSettings.artistsummary.title"/> (only new)</a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="lastfmSettings.artistsummaryinfo1"/></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="lastfmSettings.artistsummaryinfo2"/></p>
<br>-->
<p class="forward"><a href="lastfmSettings.view?CleanupArtist">LastFM Artist Cleanup</a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;">Cleanup unknown/incomplete artist entries</p>
<br>
<p class="forward"><a href="lastfmSettings.view?CleanupArtistTopTracks">LastFM Artist TopTracks Cleanup</a></p>
<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;">Cleanup cached artist TopTracks entries</p>
<br>

<table>

<form method="post" action="lastfmSettings.view">

<tr>
<td>Enable Last.fm Artistbio scheduled scan (restart needed)</td>
<td>
<c:choose>
	<c:when test="${model.LastFMArtistBioScan}">
		<input name="lastFMArtistBioScan" checked type="checkbox" class="checkbox" id="lastFMArtistBioScan"/>
	</c:when>
	<c:otherwise>
		<input name="lastFMArtistBioScan" type="checkbox" class="checkbox" id="lastFMArtistBioScan"/>
	</c:otherwise>
</c:choose>

</td>
</tr>
<tr>
<td><br></td>
</tr>
<tr>
<td>Enable Last.fm TopTrack search</td>
<td>
<c:choose>
	<c:when test="${model.LastFMTopTrackSearch}">
		<input name="lastFMTopTrackSearch" checked type="checkbox" class="checkbox" id="lastFMTopTrackSearch"/>
	</c:when>
	<c:otherwise>
		<input name="lastFMTopTrackSearch" type="checkbox" class="checkbox" id="lastFMTopTrackSearch"/>
	</c:otherwise>
</c:choose>

</td>
</tr>
<tr>
<td><br></td>
</tr>
<tr>
<td>Select Last.fm TopTrack result size</td>
<td>
<select name="lastFMResultSize">
<c:if test="${model.LastFMResultSize eq '10'}"><option selected="selected">10</option></c:if>
<c:if test="${model.LastFMResultSize ne '10'}"><option>10</option></c:if>
<c:if test="${model.LastFMResultSize eq '20'}"><option selected="selected">20</option></c:if>
<c:if test="${model.LastFMResultSize ne '20'}"><option>20</option></c:if>
<c:if test="${model.LastFMResultSize eq '30'}"><option selected="selected">30</option></c:if>
<c:if test="${model.LastFMResultSize ne '30'}"><option>30</option></c:if>
<c:if test="${model.LastFMResultSize eq '40'}"><option selected="selected">40</option></c:if>
<c:if test="${model.LastFMResultSize ne '40'}"><option>40</option></c:if>
<c:if test="${model.LastFMResultSize eq '60'}"><option selected="selected">60</option></c:if>
<c:if test="${model.LastFMResultSize ne '60'}"><option>60</option></c:if>
<c:if test="${model.LastFMResultSize eq '80'}"><option selected="selected">80</option></c:if>
<c:if test="${model.LastFMResultSize ne '80'}"><option>80</option></c:if>
<c:if test="${model.LastFMResultSize eq '99'}"><option selected="selected">99</option></c:if>
<c:if test="${model.LastFMResultSize ne '99'}"><option>99</option></c:if>
</select>
</td></tr>
<tr><td><br></td></tr>
<tr>
<td>
Set lastFM Language
</td>
<td>
<select name="lastFMLanguage">
<c:if test="${model.LastFMLanguage eq 'AUTO'}"><option selected="selected">AUTO</option></c:if>
<c:if test="${model.LastFMLanguage ne 'AUTO'}"><option>AUTO</option></c:if>
<c:if test="${model.LastFMLanguage eq 'en'}"><option selected="selected">en</option></c:if>
<c:if test="${model.LastFMLanguage ne 'en'}"><option>en</option></c:if>
<c:if test="${model.LastFMLanguage eq 'de'}"><option selected="selected">de</option></c:if>
<c:if test="${model.LastFMLanguage ne 'de'}"><option>de</option></c:if>
<c:if test="${model.LastFMLanguage eq 'it'}"><option selected="selected">it</option></c:if>
<c:if test="${model.LastFMLanguage ne 'it'}"><option>it</option></c:if>
<c:if test="${model.LastFMLanguage eq 'fr'}"><option selected="selected">fr</option></c:if>
<c:if test="${model.LastFMLanguage ne 'fr'}"><option>fr</option></c:if>
</select>
</td>
</tr>
<td><br></td>
<tr>
<td>
<input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
</td>
</tr>
</form>
</table>

<c:if test="${not empty model.error}">
    <p class="warning"><fmt:message key="${model.error}"/></p>
</c:if>

<c:if test="${model.reload}">
    <script language="javascript" type="text/javascript">parent.frames.leftPanel.location.href="leftPanel.view?"</script>
</c:if>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

</body>
</html>