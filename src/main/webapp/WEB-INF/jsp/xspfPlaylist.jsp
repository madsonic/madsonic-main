<?xml version="1.0" encoding="utf-8"?>
<%@ include file="include.jsp" %>
<%@ page language="java" contentType="text/xml; charset=utf-8" pageEncoding="iso-8859-1" %>

<playlist version="0" xmlns="http://xspf.org/ns/0/">
    <trackList>

<c:forEach var="song" items="${model.songs}">

    <madsonic:url value="/stream" var="streamUrl">
        <madsonic:param name="path" value="${song.musicFile.path}"/>
    </madsonic:url>

    <madsonic:url value="coverArt.view" var="coverArtUrl">
        <madsonic:param name="size" value="200"/>
        <c:if test="${not empty song.coverArtFile}">
            <madsonic:param name="path" value="${song.coverArtFile.path}"/>
        </c:if>
    </madsonic:url>

        <track>
            <location>${streamUrl}</location>
            <image>${coverArtUrl}</image>
            <annotation>${song.musicFile.metaData.artist} - ${song.musicFile.title}</annotation>
        </track>

</c:forEach>

    </trackList>
</playlist>