<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
	
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/lovedTrackService.js"/>"></script>	    
    <script type="text/javascript" language="javascript">

    function toggleStar(mediaFileId, element) {
        starService.star(mediaFileId, !$(element).hasClass("fa-star"));
        $(element).toggleClass("fa-star fa-star-o starred");
    }    
   
    function toggleLoved(mediaFileId, element) {
        lovedTrackService.love(mediaFileId, !$(element).hasClass("fa-heart"));
        $(element).toggleClass("fa-heart fa-heart-o loved");
    }
    </script>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
    <img src="<spring:theme code="chartImage"/>" width="32" alt="">
    <fmt:message key="statistics.title"/>
</h1>

<h2>
    <c:if test="${model.current.isAdminRole()}">
    <select name="username"  onchange="location='statistics.view?user=' + this.options[this.selectedIndex].value;">
        <c:forEach items="${model.users}" var="userx">
        <option ${userx.username eq model.user.username ? "selected" : ""} value="${userx.username}">${userx.username}</option>
        </c:forEach>
    </select>
    </c:if>

    <c:if test="${not model.current.isAdminRole()}">
    <fmt:message key="statistics.yours"/>
    </c:if>
    
    <span style="margin: 0 10px 0 0;"></span>
    
    <c:forTokens items="lastplayed topplayed" delims=" " var="cat" varStatus="loopStatus">
        <c:if test="${loopStatus.count > 1}">&nbsp;<img src="<spring:theme code="sepImage"/>">&nbsp;</c:if>
        <madsonic:url var="url" value="statistics.view">
            <madsonic:param name="listType" value="${cat}"/>
            <madsonic:param name="user" value="${model.user.username}"/>
        </madsonic:url>
        <c:choose>
            <c:when test="${model.listType eq cat}">
                <span class="headerSelected"><fmt:message key="statistics.${cat}.title"/></span>
            </c:when>
            <c:otherwise>
                <a href="${url}"><fmt:message key="statistics.${cat}.title"/></a>
            </c:otherwise>
        </c:choose>
    </c:forTokens>
    <span style="margin: 0 10px 0 10px;"> <fmt:message key="statistics.others"/> </span>
    <c:forTokens items="otheruser overall users" delims=" " var="cat" varStatus="loopStatus">
        <c:if test="${loopStatus.count > 1}">&nbsp;<img src="<spring:theme code="sepImage"/>">&nbsp;</c:if>
        <madsonic:url var="url" value="statistics.view">
            <madsonic:param name="listType" value="${cat}"/>
        </madsonic:url>
        <c:choose>
            <c:when test="${model.listType eq cat}">
                <span class="headerSelected"><fmt:message key="statistics.${cat}.title"/></span>
            </c:when>
            <c:otherwise>
                <a href="${url}"><fmt:message key="statistics.${cat}.title"/></a>
            </c:otherwise>
        </c:choose>
    </c:forTokens>    
    
</h2>
<br>
<c:choose>
	<c:when test="${model.listType eq 'lastplayed'}">
	<!-- lastplayed -->
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${model.listType eq 'topplayed'}">
	<!-- topplayed -->
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${model.listType eq 'otheruser'}">
	<!-- otheruser -->
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${model.listType eq 'overall'}">
	<!-- overall -->
	</c:when>
</c:choose>

<c:choose>
	<c:when test="${model.listType eq 'users'}">
	<!-- user Stats -->
	<br>
			<table>
			<tr>
				<th><fmt:message key="home.chart.total"/></th>
				<th><fmt:message key="home.chart.stream"/></th>
			</tr>
			<tr>
				<td><img src="<c:url value="/userChart.view"><c:param name="type" value="total"/></c:url>" width="90%" alt=""></td>
				<td><img src="<c:url value="/userChart.view"><c:param name="type" value="stream"/></c:url>"  width="90%" alt=""></td>
			</tr>
			<tr>
			<td>
				<br>
			</td>
			</tr>
			<tr>
				<th><fmt:message key="home.chart.download"/></th>
				<th><fmt:message key="home.chart.upload"/></th>
			</tr>
			<tr>
				<td><img src="<c:url value="/userChart.view"><c:param name="type" value="download"/></c:url>"  width="90%" alt=""></td>
				<td><img src="<c:url value="/userChart.view"><c:param name="type" value="upload"/></c:url>"  width="90%" alt=""></td>
			</tr>
	</table>
	</c:when>
        <c:otherwise>
			<c:if test="${not empty model.songs}">
				<table>
					<tr style="padding-top:2.5em">
					</tr>
					<tr>
						<madsonic:url value="statistics.view" var="previousUrl">
							<madsonic:param name="listOffset" value="${model.listOffset - model.listSize}"/>
							<madsonic:param name="listType" value="${model.listType}"/>
							</madsonic:url>
						<madsonic:url value="statistics.view" var="nextUrl">
							<madsonic:param name="listOffset" value="${model.listOffset + model.listSize}"/>
							<madsonic:param name="listType" value="${model.listType}"/>
							</madsonic:url>

                    <c:if test="${model.listOffset gt 0}">
                        <td style="padding-right:1.5em"><i class="fa fa-chevron-left icon control"></i>&nbsp;<a href="${previousUrl}"><fmt:message key="common.previous"/></a></td>
                    </c:if>
						<td style="padding-right:1.5em"><fmt:message key="statistics.title"><fmt:param value="${model.listOffset + 1}"/><fmt:param value="${model.listOffset + model.listSize}"/></fmt:message></td>
                    <c:if test="${fn:length(model.songs) eq model.listSize}">                        
						<td><i class="fa fa-chevron-right icon control"></i>&nbsp;<a href="${nextUrl}"><fmt:message key="common.next"/></a></td>				
                    </c:if>
						
					</tr>
				</table>
				<br>
				<table class="music" style="border-collapse:collapse">
					<c:forEach items="${model.songs}" var="song" varStatus="loopStatus">

						<madsonic:url value="/main.view" var="mainUrl">
							<madsonic:param name="path" value="${song.parentPath}"/>
						</madsonic:url>

						<tr>
                            <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em">
                            <c:import url="coverArtThumb.jsp">
                                        <c:param name="albumId" value="${song.id}"/>
                                        <c:param name="auth" value="${song.hash}"/>
                                        <c:param name="artistName" value="${song.name}"/>
                                        <c:param name="coverArtSize" value="50"/>
                                        <c:param name="scale" value="0.5"/>
                                        <c:param name="showLink" value="true"/>
                                        <c:param name="showZoom" value="false"/>
                                        <c:param name="showChange" value="false"/>
                                        <c:param name="showArtist" value="false"/>
                                        <c:param name="typArtist" value="true"/>
                                        <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                            </c:import>
                            </td>                            
                        
                            <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0.5em">
							<c:import url="playAddDownload.jsp">
								<c:param name="id" value="${song.id}"/>
								<c:param name="video" value="${song.video and model.player.web}"/>
								<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
								<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
								<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
								<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addContextVisible}"/>
								<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addNextVisible}"/>
								<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addLastVisible}"/>						
								<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
								<c:param name="artist" value="${fn:escapeXml(song.artist)}"/>
								<c:param name="title" value="${song.title}"/>
								<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
								<c:param name="starred" value="${not empty song.starredDate}"/>
                                <c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
                                <c:param name="loved" value="${not empty song.lovedDate}"/>                                
								<c:param name="asTable" value="false"/>
								<c:param name="YoutubeEnabled" value="${model.buttonVisibility.youtubeVisible}"/>							
							</c:import>
                            </td>
                            
							<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:0.25em;padding-right:1.25em">
								<str:truncateNicely upper="40">${song.title}</str:truncateNicely>
							</td>

							<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
								<a href="${mainUrl}"><span class="detail"><str:truncateNicely upper="40">${song.albumName}</str:truncateNicely></span></a>
								
							</td>

							<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
								<span class="detail"><str:truncateNicely upper="40">${song.artist}</str:truncateNicely></span>
								
							</td>
							<c:choose>
								<c:when test="${model.listType eq 'topplayed'}">
									<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
										<span class="detailcolor">(${song.playCount}x)</span>
									</td>
								</c:when>
							</c:choose>
							<c:choose>
								<c:when test="${model.listType eq 'overall'}">
									<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
										<span class="detailcolor">(${song.playCount}x)</span>
									</td>
								</c:when>
							</c:choose>				
							<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:0.75em">
								<span class="detail">${fn:substring(song.lastPlayed, 0, 16)}</span>
							</td>
							</tr>
							
					</c:forEach>
				</table>
			</c:if>
</c:otherwise>	
</c:choose>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:880, /*scrolling inertia: integer (milliseconds)*/
  		    scrollEasing:"easeOutCubic", /*scrolling easing: string*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$(".content_main").resize(function(e){
    $(".content_main").mCustomScrollbar("update");
});
</script>
</c:if>    

</body>
</html>