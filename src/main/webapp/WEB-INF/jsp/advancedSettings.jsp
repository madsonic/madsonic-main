<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%--@elvariable id="command" type="org.madsonic.command.AdvancedSettingsCommand"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>

    <c:choose>
        <c:when test="${customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="advanced"/>
    <c:param name="toast" value="${command.toast}"/>
</c:import>
<br>
<form:form method="post" action="advancedSettings.view" commandName="command">
    <table style="white-space:nowrap;" class="indent">
        <tr>
            <td><fmt:message key="advancedsettings.downloadlimit"/></td>
            <td>
                <form:input path="downloadLimit" size="8"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="downloadlimit"/></c:import>
            </td>
        </tr>
        <tr>
            <td><fmt:message key="advancedsettings.uploadlimit"/></td>
            <td>
                <form:input path="uploadLimit" size="8"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="uploadlimit"/></c:import>
            </td>
        </tr>
        <tr>
            <td><!-- <fmt:message key="advancedsettings.info"/> --><br></td>
        </tr>
        <tr>
            <td><fmt:message key="generalsettings.pagetitle"/></td>
            <td>
                <form:input path="pageTitle" size="80"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="pagetitle"/></c:import>
            </td>
        </tr>
        
        <tr>
            <td><fmt:message key="generalsettings.welcometitle"/></td>
            <td>
                <form:input path="welcomeTitle" size="80"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="welcomemessage"/></c:import>
            </td>
        </tr>
        <tr>
            <td><fmt:message key="generalsettings.welcomesubtitle"/></td>
            <td>
                <form:input path="welcomeSubtitle" size="80" />
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="welcomemessage"/></c:import>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;"><br><fmt:message key="generalsettings.adminmessage"/></td>
            <td>
                <br>
                <form:textarea path="adminMessage" rows="20" cols="80" />
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="adminmessage"/></c:import>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;"><fmt:message key="generalsettings.welcomemessage"/></td>
            <td>
                <form:textarea path="welcomeMessage" rows="20" cols="80" />
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="welcomemessage"/></c:import>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;"><fmt:message key="generalsettings.loginmessage"/></td>
            <td>
                <form:textarea path="loginMessage" rows="8" cols="80" />
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="loginmessage"/></c:import>
                <fmt:message key="main.wiki"/>
            </td>
        </tr>
        
    </table>
  
    <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
    <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">

</form:form>

<c:if test="${command.reloadNeeded}">
    <script language="javascript" type="text/javascript">
        parent.frames.leftPanel.location.href="leftPanel.view?";
        parent.frames.playQueue.location.href="playQueue.view?";
    </script>
</c:if>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${customScrollbar}">
    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:950, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
        
        $("#content_main").mCustomScrollbar("update");


$("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>    

</body>

</html>