  <c:if test="${not model.customScrollbar}">
<a name="top"></a>
</c:if>    

<div id="content_artist" class="content_artist2"> <!-- CONTENT -->
  <div id="container" class="container"> <!-- CONTAINER -->
    <div class="bgcolor2 artistindex" style="opacity: 1.0; clear: both; position: fixed; top: 0; right: 0; left: 0px; padding: 0.15em 0.15em 0.15em 0.15em; height: 45px; z-index: 1000">

        <c:if test="${model.customScrollbar}">
        <a id="top"></a>
        </c:if>    
        <div style="padding-bottom:1.0em; padding-left: 10px; font-size: 14pt;">
        <c:if test="${model.customScrollbar}">
            <h2>
            <div id="anchor_list">
            <c:forEach items="${model.indexes}" var="index">
                <a href="?name=${index.index}" lnk="${index.index}">${index.index}</a>
            </c:forEach>
            </div>
            </h2>
        </c:if>    
        <c:if test="${not model.customScrollbar}">
            <h2>
            <div id="anchor_list">
            <c:forEach items="${model.indexes}" var="index">
                <a href="?name=${index.index}" accesskey="${index.index}">${index.index}</a>
            </c:forEach>
            </div>
            </h2>
        </c:if>    
        </div>
    </div>

    <div style="margin-top: 60px; padding: 0.20em 0.15em 0.15em 0.15em; z-index: 100">
    
    <fmt:message key="common.search" var="search"/>
    
    <form method="get" action="artist.view" target="main" name="artistForm">
        <table><tr>
            <td><input type="text" name="name" id="query" size="30" placeholder="${search} ..." style="padding-left:8px;margin-right:4px;" onclick="select();"></td>
            <td><a href="javascript:document.artistForm.submit()"><img src="<spring:theme code="searchImage"/>" width="32" alt="${search}" title="${search}"></a></td>
        </tr></table>
    </form>

    <c:if test="${model.indexedArtistHub == null }">
     <i>Select Artist Index.</i>
    </c:if>    

    <c:forEach items="${model.indexedArtistHub}" var="entry">

    <table width="60%" border="0" cellspacing="0" cellpadding="4" style="border-collapse:collapse;white-space:nowrap;margin-top:10px;">
      <tr>
        <c:forEach items="${entry.value}" var="artist" varStatus="status">
        <td style="vertical-align: top;">

        <table class="artisthub" width="540px" border="0" cellspacing="0" cellpadding="2" style="white-space:nowrap; padding-left: 10px;padding-top: 10px;">
        <tr><td colspan="4">
        <!-- 02 -->
        <madsonic:url value="main.view" var="mainUrl">
            <c:forEach items="${artist.mediaFiles}" var="mediaFile">
            <madsonic:param name="id" value="${mediaFile.id}"/>
            </c:forEach>
        </madsonic:url>
        
        <h1 style="margin: 0 0 5px;">
        <a target="main" href="${mainUrl}"><str:truncateNicely upper="${model.captionCutoff}">${artist.name}</str:truncateNicely></a> 
        </h1>
        <!-- 02 -->
        </td>
        </tr>
        <tr style="width:100%;">

        <!-- 01 -->
        <td style="vertical-align:top;width:120px;">

        <c:import url="coverArt.jsp">
        <c:param name="albumId" value="${artist.mediaFiles[0].id}"/>
        <c:param name="auth" value="${artist.mediaFiles[0].hash}"/>
        <c:param name="albumName" value="${mediaFile.name}"/>
        <c:param name="coverArtSize" value="114"/>
        <c:param name="typArtist" value="true"/>
        <c:param name="showLink" value="true"/>
        <c:param name="showZoom" value="false"/>
        <c:param name="showChange" value="false"/>
        <c:param name="showCaption" value="true"/>
        <c:param name="appearAfter" value="0"/>
        </c:import>
        
        <madsonic:url value="/changeCoverArt.view" var="changeCoverArtUrl">
            <madsonic:param name="id" value="${artist.mediaFiles[0].id}"/>
            <madsonic:param name="isArtist" value="true"/>
        </madsonic:url>
        <div style="opacity: 0.70; clear: both; position: relative; top: -36px; left: 85px; padding: 0.25em 0.15em 0.15em 0.15em; max-width: 16px; z-index: 0 !important;">
            <a class="detailmini" href="${changeCoverArtUrl}" title="Change Artist Cover" alt="Change"><img src="<spring:theme code="editArtistImage"/>"></a>
        </div>
           </div>
        <!-- 01 -->
          </td>
          <td width="190" style="vertical-align:top;">

        <!-- 03-07 -->
        <table width="192px" border="0" cellspacing="0" cellpadding="1">
        
    <!--    <td class="detailcolor" style="width: 60px;">albums</td> -->
        
        <tr><td class="detailcolor" style="width: 55px;">albums</td> <td class="detailcolordark">${artist.albumCount}</td></tr>
        <tr><td class="detailcolor">songs</td> <td class="detailcolordark">${artist.songCount}</td></tr>
        <tr><td class="detailcolor">played</td> <td class="detailcolordark">${artist.playCount}</td></tr>
        <tr><td class="detailcolor">year</td> <td class="detailcolordark">${artist.albums[0].year}</td></tr>
        <tr><td class="detailcolor">genre</td> <td class="detailmini">${artist.albums[0].genre}</td></tr>
        <tr><td class="detailcolor" style="padding-top:4px">
        
        
        <div style="opacity: 0.70; clear: both; position: relative; top: 0px; left: 0px; padding: 0.25em 0.15em 0.15em 0.15em; max-width: 16px;">
        <c:choose>
          <c:when test="${model.showAlbum}">
            <span id="showAlbums_${artist.albums[0].mediaFileId}" style="display:none">
            <a href="javascript:noop()"onclick="showAlbums(${artist.albums[0].mediaFileId})">
            <img src="<spring:theme code="albumsImage"/>" title="Show other albums from Artist"></a></span>
            <span id="hideAlbums_${artist.albums[0].mediaFileId}" style="display:inline">
            <a href="javascript:noop()" onclick="hideAlbums(${artist.albums[0].mediaFileId})">
            <img src="<spring:theme code="albumsImage"/>" title="Hide albums"></a></span>
          </c:when>
          <c:otherwise>
            <span id="showAlbums_${artist.albums[0].mediaFileId}" style="display:inline">
            <a href="javascript:noop()"onclick="showAlbums(${artist.albums[0].mediaFileId})">
            <img src="<spring:theme code="albumsImage"/>" title="Show other albums from Artist"></a></span>
            <span id="hideAlbums_${artist.albums[0].mediaFileId}" style="display:none">
            <a href="javascript:noop()" onclick="hideAlbums(${artist.albums[0].mediaFileId})">
            <img src="<spring:theme code="albumsImage"/>" title="Hide albums"></a></span>
          </c:otherwise>
        </c:choose>
        </div>
        
        </td>
        
        <td class="detailcolor" style="padding-top:10px;vertical-align:top;">

        </td>
        </table>
        <!-- 03-07 -->

        </td>
        <td>

        <!-- 08 -->
        
        <td width="180" style="vertical-align:top;padding-top:20px;">
        <span class="image_stack" style="margin-top:-150px;"> 
                <c:forEach items="${artist.albums}" var="album" varStatus="loopStatus">
                <c:choose>
                  <c:when test="${loopStatus.count <= 3}">
                    <c:import url="artistAlbums.jsp">
                        <c:param name="albumId" value="${album.mediaFileId}"/>
                        <c:param name="auth" value="${album.getHash()}"/>
                        <c:param name="albumName" value="${album.name}"/>
                        <c:param name="coverArtSize" value="50"/>
                        <c:param name="showLink" value="true"/>
                        <c:param name="showZoom" value="false"/>
                        <c:param name="showChange" value="false"/>
                        <c:param name="showCaption" value="false"/>
                        <c:param name="appearAfter" value="0"/>
                        <c:param name="count" value="${loopStatus.count}"/>
                    </c:import>
                  </c:when>
                  <c:otherwise>
                  </c:otherwise>
                </c:choose>
                </c:forEach>
        </span>
        <!-- 08 -->
        </td>
        </tr>
        <tr>
          <td colspan="4">
            <!-- 10 -->

                <c:choose>
                  <c:when test="${model.showAlbum}">
                    <div id="album_stack_${artist.albums[0].mediaFileId}" style="margin-top:0px;margin-bottom:15px;display:block">
                  </c:when>
                  <c:otherwise>
                    <div id="album_stack_${artist.albums[0].mediaFileId}" style="margin-top:0px;margin-bottom:15px;display:none">
                  </c:otherwise>
                </c:choose>

            <c:forEach items="${artist.albums}" var="album" varStatus="loopStatus">
            
                <c:import url="playAddDownload.jsp">
                <c:param name="id" value="${album.mediaFileId}"/>
                <c:param name="video" value="false"/>
                <c:param name="playEnabled" value="true"/>
                <c:param name="playAddEnabled" value="false"/>
                <c:param name="addEnabled" value="true"/>
                <c:param name="downloadEnabled" value="false"/>
                <c:param name="artist" value="${album.name}"/>
                <c:param name="title" value="${album.name}"/>
                <c:param name="starEnabled" value="false"/>
                <c:param name="starred" value=""/>
                <c:param name="asTable" value="false"/>
                <c:param name="YoutubeEnabled" value="false"/>
                </c:import>                
                
                <!-- <span class="detailmini"><c:if test="${album.playCount < 100}">0</c:if><c:if test="${album.playCount < 10}">0</c:if>${album.playCount}</span> |  -->
                <!-- <span class="detailmini" style="display:inline;font-family: verdana, arial, sans-serif;"><c:if test="${album.songCount < 100}">0</c:if><c:if test="${album.songCount < 10}">0</c:if>${album.songCount}</span>
                <img src="icons/default/note.png" width="10" height="10" title="Tracks" style="margin-right: 5px;"/> -->

                <div style="display: inline-flex;display:-webkit-inline-box;padding-left:8px;padding-right:8px;">
                
                <c:choose>
                  <c:when test="${album.playCount < 100}">
                        <div style="width:${(album.playCount)/2}px;height:8px;background-color:#0F0;" title="Played ${album.playCount} times"></div>
                  </c:when>
                  <c:otherwise>
                    <div style="width:50px; height:8px;background-color:#0F0;" title="Played ${album.playCount} times"></div>
                    <img src="icons/default/plus.png" width="8" height="8" style="position:relative; left:-10px;"/>
                  </c:otherwise>
                </c:choose>
                
                <c:choose>
                  <c:when test="${album.playCount < 100}">
                        <div style="width:${(100-album.playCount)/2}px;height:8px;background-color:#888;" title="Played ${album.playCount} times"></div>
                  </c:when>
                </c:choose>
                </div>
    
                <c:choose>
                  <c:when test="${album.playCount > 100}">
                    <c:if test="${album.year == 0}"><span class="detail" style="padding-right: 10px;margin-left: -10px;">[0000]</span></c:if>
                    <c:if test="${album.year > 1}"><span class="detailcolor"  style="padding-right: 10px;margin-left: -10px;">[${album.year}]</span></c:if>
                  </c:when>
                  <c:otherwise>
                    <c:if test="${album.year == 0}"><span class="detail" style="padding-right: 10px;">[0000]</span></c:if>
                    <c:if test="${album.year > 1}"><span class="detailcolor"  style="padding-right: 10px;">[${album.year}]</span></c:if>
                  </c:otherwise>
                </c:choose>
                
                <madsonic:url value="main.view" var="mediaFileIdUrl">
                    <madsonic:param name="id" value="${album.mediaFileId}"/>
                </madsonic:url>
                <a target="main" href="${mediaFileIdUrl}"><str:truncateNicely upper="${model.captionCutoff}">${album.name}</str:truncateNicely></a> 

                <c:if test="${album.songCount ne null}"> - 
                <span class="detailcolor">${album.songCount}</span>
                </c:if>

                <c:if test="${album.genre ne null}"> - 
                <span class="detailcolor">${album.genre}</span>
                </c:if>
                <br>
                </c:forEach>
            </div>
            <td>
            </td>
            <!-- 10 -->
        </td>
    
        </tr>
        </table>
    </td>
        <c:choose>
            <c:when test="${status.count % 2 == 1}">
            </c:when>
            <c:otherwise>
            </tr>
            </c:otherwise>
        </c:choose>
        </c:forEach>
</c:forEach>
  </tr>
</table>

