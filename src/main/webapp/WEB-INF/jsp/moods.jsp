<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
	
    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-1.4.2.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.dropdownReplacement-0.5.3.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/style/jquery.dropdownReplacement.css"/>" type="text/css">
    <script type="text/javascript">
    $(document).ready(function() {
        $("#yearSelect").dropdownReplacement({selectCssWidth: 80});
        $("#countSelect").dropdownReplacement({selectCssWidth: 70});
    });
    </script>
    
<style type="text/css">
span.off {
    cursor: pointer;
    float:left;
    padding: 5px 10px;
    margin-right: 5px;
    margin-bottom: 5px;    
    background: #FFF;
    color: #000;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    border-radius: 7px;
    border: solid 1px #CCC;
    -webkit-transition-duration: 0.1s;
    -moz-transition-duration: 0.1s;
    transition-duration: 0.1s;
    -webkit-user-select:none;
    -moz-user-select:none;
    -ms-user-select:none;
    user-select:none;
    white-space: nowrap;
}

span.on {
    cursor: pointer;
    float:left;
    padding: 5px 10px;
    margin-right: 5px;
    margin-bottom: 5px;    
    background: #D2F5FF;
    color: #000;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    border-radius: 7px;
    border: solid 1px #999;
    -webkit-transition-duration: 0.1s;
    -moz-transition-duration: 0.1s;
    transition-duration: 0.1s;
    -webkit-user-select:none;
    -moz-user-select:none;
    -ms-user-select:none;
    user-select:none;
    white-space: nowrap;
}

span.off:hover {
    background: #D2F5FF;opacity:0.7;
    border: solid 1px #FFF;
    text-decoration: none;
    }
    
input {
    margin-right:5px;
}    
    
    </style>
    
<script type="text/javascript">

function changeClass(elem, className1,className2) {
    elem.className = (elem.className == className1)?className2:className1;
}
function playMoodRadio() {
    var moods = new Array();
    var e = document.getElementsByTagName("span");
    for (var i = 0; i < e.length; i++) {
        if (e[i].className == "on") {
            moods.push(e[i].firstChild.data);
        }
    }
    var num = document.getElementsByName("MoodRadioPlayCount")[0].selectedIndex;
    var playcount = document.getElementsByName("MoodRadioPlayCount")[0].options[num].text;
    var numyear = document.getElementsByName("MoodYear")[0].selectedIndex;
    var moodsyear = document.getElementsByName("MoodYear")[0].options[numyear].value;

    parent.playQueue.onPlayMoodRadio(moods, playcount, moodsyear);
}
</script>

</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
    <img src="<spring:theme code="moodsImage"/>" width="32" alt="">
    Moods
</h1>
<c:choose>
    <c:when test="${empty model.moods}">
        <p>Please scan your library before</a>.
    </c:when>
    <c:otherwise>
        <p style="padding:10px">Choose one or more moods.</p>

        <div>
        <c:forEach items="${model.moods}" var="mood">
            <span class="off" onclick='changeClass(this,"on","off");'>${mood}</span>
        </c:forEach>
        </div>
        <div style="clear:both"/>
        
        <br>
        <form style="display: inline-flex;">        
        <select name="MoodYear" id="yearSelect">
                <option value="any"><fmt:message key="more.random.anyyear"/></option>

                <c:forEach begin="0" end="${model.currentYear - 2005}" var="yearOffset">
                    <c:set var="year" value="${model.currentYear - yearOffset}"/>
                    <option value="${year} ${year}">${year}</option>
                </c:forEach>

                <option value="2010 2015">2010 &ndash; 2015</option>
                <option value="2005 2010">2005 &ndash; 2010</option>
                <option value="2000 2005">2000 &ndash; 2005</option>
                <option value="1990 2000">1990 &ndash; 2000</option>
                <option value="1980 1990">1980 &ndash; 1990</option>
                <option value="1970 1980">1970 &ndash; 1980</option>
                <option value="1960 1970">1960 &ndash; 1970</option>
                <option value="1950 1960">1950 &ndash; 1960</option>
                <option value="0 1949">&lt; 1950</option>
                <option value="0 1900">&lt; 1900</option>
        </select>

        <select name="MoodRadioPlayCount" id="countSelect">
            <option>10</option>
            <option>15</option>
            <option>25</option>
            <option>50</option>
            <option>75</option>
            <option>100</option>
            <option>150</option>
            <option>200</option>
            </select>

        <input type="button" value="Play Mood Radio!" onClick="playMoodRadio();">
        </form>

    </c:otherwise>
</c:choose>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">
    (function($){
        $(window).load(function(){

                $("#content_main").mCustomScrollbar({
                axis:"y",
                scrollInertia:450, 
                mouseWheel:true, 
                mouseWheelPixels:"auto", 
                autoDraggerLength:true, 
                autoHideScrollbar:false,
                scrollButtons:{ enable:true, 
                                scrollType:"continuous", 
                                scrollSpeed:"auto", 
                                scrollAmount:40 },
                                theme:"${model.customScrollbarTheme}",
                                scrollbarPosition:"inside"
            });
        });
    })(jQuery);
        
    $("#content_main").resize(function(e){
        $("#content_main").mCustomScrollbar("update");
    });

</script>
</c:if>

</body>
</html>
