<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%@ include file="include.jsp" %>

<link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">

<%--
PARAMETERS
  albumId: ID of album.
  playlistId: ID of playlist.
  podcastChannelId: ID of podcast channel
  auth: Authentication token
  coverArtHQ: HD Coverart
  coverArtSize: Height and width of cover art.
  caption1: Caption line 1
  caption2: Caption line 2
  caption3: Caption line 3
  captionCount: Number of caption lines to display (default 0)
  showLink: Whether to make the cover art image link to the album page.
  showZoom: Whether to display a link for zooming the cover art.
  showChange: Whether to display a link for changing the cover art.
  appearAfter: Fade in after this many milliseconds, or nil if no fading in should happen.
  showSmoother: more opacity effect
--%>

<c:choose>
    <c:when test="${empty param.coverArtSize}">
        <c:set var="size" value="auto"/>
    </c:when>
    <c:otherwise>
        <c:set var="size" value="${param.coverArtSize}px"/>
    </c:otherwise>
</c:choose>

<c:set var="captionCount" value="${empty param.captionCount ? 0 : param.captionCount}"/>

<str:randomString count="5" type="alphabet" var="divId"/>
<str:randomString count="5" type="alphabet" var="imgId"/>
<str:randomString count="5" type="alphabet" var="playId"/>
<str:randomString count="5" type="alphabet" var="addId"/>
<str:randomString count="5" type="alphabet" var="backId"/>

<div class="coverart">
    <div style="width:${size};max-width:${size};height:${size};max-height:${size};cursor:pointer" title="${param.caption1}" id="${divId}">

        <c:if test="${not empty param.albumId}">
            <c:url value="main.view" var="targetUrl">
                <c:param name="id" value="${param.albumId}"/>
            </c:url>
        </c:if>
        <c:if test="${not empty param.playlistId}">
            <c:url value="playlist.view" var="targetUrl">
                <c:param name="id" value="${param.playlistId}"/>
            </c:url>
        </c:if>
        <c:if test="${not empty param.podcastChannelId}">
            <c:url value="podcastChannel.view" var="targetUrl">
                <c:param name="id" value="${param.podcastChannelId}"/>
            </c:url>
        </c:if>

        <c:url value="/coverArt.view" var="coverArtUrl">
            <c:if test="${not empty param.coverArtSize}">
                <c:choose>
                    <c:when test="${param.coverArtHQ}">
                        <c:param name="size" value="${param.coverArtSize * 2}"/>
                    </c:when>
                    <c:otherwise>
                        <c:param name="size" value="${param.coverArtSize}"/>
                    </c:otherwise>
                </c:choose>
            </c:if>
            <c:if test="${not empty param.albumId}">
                <c:param name="id" value="${param.albumId}"/>
            </c:if>
            <c:if test="${not empty param.podcastChannelId}">
                <c:param name="id" value="pod-${param.podcastChannelId}"/>
            </c:if>
            <c:if test="${not empty param.playlistId}">
                <c:param name="id" value="pl-${param.playlistId}"/>
            </c:if>
        <c:if test="${not empty param.auth}">
            <c:param name="auth" value="${param.auth}"/>
        </c:if>
        </c:url>
        <c:url value="/coverArt.view" var="zoomCoverArtUrl">
            <c:param name="id" value="${param.albumId}"/>
        </c:url>

        <div style="position: relative; width: 0; height: 0">
            <a href="#" onclick="parent.playQueue.onPlay(${param.albumId}); return false;">
            <img src="icons/default/playalbum.png" id="${playId}" style="position: relative; top: ${param.coverArtSize -30}px; left: ${param.coverArtSize -52}px; z-index:100; display:none;" ></a>
        </div>
    
        <div style="position: relative; width: 0; height: 0">
            <a href="#" onclick="parent.playQueue.onAdd(${param.albumId}); return false;">
            <img src="icons/default/addalbum.png" id="${addId}" style="position: relative; top: ${param.coverArtSize -30}px; left: ${param.coverArtSize -29}px; z-index:100; display:none;" ></a>
        </div>        
        
        <c:choose>
        <c:when test="${param.showLink}"><a href="${targetUrl}" title="${param.caption1}"></c:when>
        </c:choose>
        
        <div style="position: relative; width: 0; height: 0">
            <c:if test="${param.typVideo}">
                <img src="icons/default/test.png" id="${backId}" width="${param.coverArtSize +1}" height="${param.coverArtSize * 1.3 +1}" style="position: relative; bottom: 0px; left: 0px;z-index:90; display:none; opacity: 0.5;" >
            </c:if>
            
            <c:if test="${not param.typVideo}">
                <img src="icons/default/test.png" id="${backId}" width="${param.coverArtSize +1}" height="${param.coverArtSize +1}" style="position: relative; bottom: 0px; left: 0px;z-index:90; display:none; opacity: 0.5;" >
            </c:if>
        </div>   
        
        <img src="${coverArtUrl}" id="${imgId}" width="${param.coverArtSize}" class="dropshadow" alt="${param.albumName}">
        <c:if test="${param.showLink}"></a></c:if>
    </div>

    <c:if test="${captionCount gt 0}">
        <div class="caption1" style="width:${param.coverArtSize - 16}px;margin-top:5px"><a href="${targetUrl}" title="${param.caption1}">${param.caption1}</a></div>
    </c:if>
    <c:if test="${captionCount gt 1}">
        <div class="caption2" style="width:${param.coverArtSize - 16}px">${param.caption2}&nbsp;</div>
    </c:if>
    <c:if test="${captionCount gt 2}">
        <div class="caption3" style="width:${param.coverArtSize - 16}px">${param.caption3}&nbsp;</div>
    </c:if>
</div>

<c:if test="${param.showChange or param.showZoom}">
    <div style="padding-top:6px;text-align:right">
        <c:if test="${param.showChange}">
            <c:url value="/changeCoverArt.view" var="changeCoverArtUrl">
                <c:param name="id" value="${param.albumId}"/>
            </c:url>
            <a class="detail" href="${changeCoverArtUrl}"><fmt:message key="coverart.change"/></a>
        </c:if>

        <c:if test="${param.showZoom and param.showChange}">
            |
        </c:if>

        <c:if test="${param.showZoom}">
            <a class="detail" rel="zoom" title="${param.caption1}" href="${zoomCoverArtUrl}"><fmt:message key="coverart.zoom"/></a>
        </c:if>
    </div>
</c:if>

<script type="text/javascript">
    $(document).ready(function () {
        setTimeout("$('#${imgId}').fadeIn(500)", ${empty param.appearAfter ? 0 : param.appearAfter});
    });

    $("#${divId}").mouseover(function () {
        $("#${playId}").stop(1,1).fadeIn(600);
        $("#${addId}").stop(1,1).fadeIn(500);
        $("#${imgId}").animate({opacity: 0.8}, 150);
    });
        
    $("#${divId}").mouseleave(function () {
        $("#${playId}").stop(1,1).fadeOut(1200);
        $("#${addId}").stop(1,1).fadeOut(1000);
        $("#${imgId}").animate({opacity: 1.0}, 150);
    });
    
    $("#${playId}").click(function () {
        <c:choose>
        <c:when test="${not empty param.albumId}">
        parent.playQueue.onPlay(${param.albumId});
        </c:when>
        <c:otherwise>
        parent.playQueue.onPlayPlaylist('${param.playlistId}', false);
        </c:otherwise>
        </c:choose>
	
        <c:if test="${not empty param.podcastChannelId}">
        parent.playQueue.onPlayPodcastChannel(${param.podcastChannelId});
        </c:if>	
    });
    
    
    $("#${addId}").click(function () {
        <c:choose>
        <c:when test="${not empty param.albumId}">
        parent.playQueue.onAdd(${param.albumId});
        </c:when>
        <c:otherwise>
        parent.playQueue.onAddPlaylist('${param.playlistId}', false);
        </c:otherwise>
        </c:choose>
    });
</script>

