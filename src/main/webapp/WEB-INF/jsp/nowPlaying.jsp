<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>

<html>
<head>
    <%@ include file="head.jsp" %>
	<%@ include file="jquery.jsp" %> 	
	    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/script/prototype.js"/>"></script>    
        <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/dwr/interface/nowPlayingService.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>        
</head>

<body class="bgcolor1 playingframe" style="padding-top:0em" onload="init()">

<script type="text/javascript">
    function init() {
        dwr.engine.setErrorHandler(null);
    }
</script> 
        
    <!-- This script uses AJAX to periodically retrieve what all users are playing. -->
    <script type="text/javascript" language="javascript">        
        
        startGetNowPlayingTimer();
		
        function startGetNowPlayingTimer() {
            nowPlayingService.getNowPlaying(getNowPlayingCallback);
            setTimeout("startGetNowPlayingTimer()", 30000);
        }

        function getNowPlayingCallback(nowPlaying) {
        
            var html = nowPlaying.length == 0 ? "" : "<h2><fmt:message key="main.nowplaying"/></h2><table style='width:100%'>";
            for (var i = 0; i < nowPlaying.length; i++) {
                html += "<tr><td colspan='2' class='detail' style='padding-top:1em;white-space:nowrap'>";

                html += "<tr><td class='detail' style='padding-bottom:0.2em'>" +
                        "<a title='" + nowPlaying[i].tooltip + "' target='main' href='" + nowPlaying[i].albumUrl + "'>";

                if (nowPlaying[i].artist != null) {
                    html += "<span class='artistTitle'>" + nowPlaying[i].artist + "</span><br/>";
                }
                html += "<span class='songTitle'>" + nowPlaying[i].title + "</span></a><br/>";
                html += "</td><td>" +
                        "<a title='" + nowPlaying[i].tooltip + "' target='main' href='" + nowPlaying[i].albumUrl + "'>" +
                        "<img src='" + nowPlaying[i].coverArtZoomUrl + "' class='dropshadow' height='35' width='35'></a>" +
                        "</td></tr>";
            loadArtistInfo(nowPlaying[0].mediaId);                         
            }
            html += "</table>";
            $('nowPlaying').innerHTML = html;
        }
        
        function loadArtistInfo(mediaId) {
            multiService.getArtistInfo(mediaId, 8, function (artistInfo) {
                if (artistInfo.similarArtists.length > 0) {
                    var html = "";
                    for (var i = 0; i < artistInfo.similarArtists.length; i++) {
                        html += "<a href='main.view?id=" + artistInfo.similarArtists[i].mediaFileId + "' target='main'>" + escapeHtml(artistInfo.similarArtists[i].artistName) + "</a>";
                        if (i < artistInfo.similarArtists.length - 1) {
                            html += " <span class='similar-artist-divider'><br></span> ";
                        }
                    }
                    jQuery("#similarArtists").html(html);
                    jQuery("#similarArtists").show();
                    jQuery("#similarArtistsTitle").show();
                    jQuery("#similarArtistsRadio").show();
                    jQuery("#artistBio").show();
                }

                if (artistInfo.artistBio && artistInfo.artistBio.biography) {
                    $("#artistBio").append(artistInfo.artistBio.biography);
                    if (artistInfo.artistBio.mediumImageUrl) {
                        jQuery("#artistImage").attr("src", artistInfo.artistBio.mediumImageUrl);
                        jQuery("#artistImage").show();
                    }
                }
            });
        }          
        
    </script>

    <div id="nowPlaying"></div>
    
    <table style="width:50%;padding-top:2em;clear:both">
    <tr>
        <td rowspan="4" style="vertical-align: top">
            <img id="artistImage" class="dropshadow" alt="" style="margin-right: 1em; display: none">
        </td>
        <td id="artistBio" style="padding-bottom: 0.5em"></td>
    </tr>
    <tr><td style="padding-bottom: 0.5em">
        <span id="similarArtistsTitle" style="padding-right: 0.5em;display: none"><fmt:message key="main.similarartists"/>:</span>
        <span id="similarArtists"></span>
    </td></tr>
    <tr><td>
        <div id="similarArtistsRadio" class="forward" style="display: none">
            <a href="javascript:playSimilar()"><fmt:message key="main.startradio"/></a>
        </div>
    </td></tr>
    <tr><td style="height: 100%"></td></tr>
</table>


</body>
</html>