<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--@elvariable id="command" type="org.madsonic.command.UserSettingsCommand"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>

    <c:choose>
        <c:when test="${customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>	
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="user"/>
    <c:param name="restricted" value="false"/>
    <c:param name="toast" value="${command.toast}"/>
</c:import>
<br>
<script type="text/javascript" language="javascript">
    function enablePasswordChangeFields() {
        var changePasswordCheckbox = $("#passwordChange");
        var ldapCheckbox = $("#ldapAuthenticated");
        var passwordChangeTable = $("#passwordChangeTable");
        var passwordChangeCheckboxTable = $("#passwordChangeCheckboxTable");

        if (changePasswordCheckbox && changePasswordCheckbox.is(":checked") && (ldapCheckbox == null || !ldapCheckbox.is(":checked"))) {
            passwordChangeTable.show();
        } else {
            passwordChangeTable.hide();
        }

        if (changePasswordCheckbox) {
            if (ldapCheckbox && ldapCheckbox.is(":checked")) {
                passwordChangeCheckboxTable.hide();
            } else {
                passwordChangeCheckboxTable.show();
            }
        }
    }
</script>

<table class="indent">
    <tr>
        <td style="width:120;"><b>Select Task</b></td>
        <td>
            <select name="action" style="width:200px;" onchange="location='userSettings.view?usrAct=' + ( value);">
                <option value="edit">-- edit User --</option>
                <option value="new" ${command.newUser eq true ? "selected" : ""}>-- new User --</option> 
				<option value="clone" ${command.newClone eq true ? "selected" : ""}>-- clone User --</option> 
            </select>
                <div style="margin:0 0 0 5px;display:inline;">
                    <c:import url="helpToolTip.jsp"><c:param name="topic" value="editUser"/></c:import>
                </div> 
        </td>
    </tr>

    <tr>
        <td><b></b></td>
        <td>
		<c:if test="${command.newUser ne true and command.newClone ne true}">
            <select name="username"  style="width:200px;" onchange="location='userSettings.view?userIndex=' + (selectedIndex - 1 ) + '&usrAct=edit';">
				<option value="" selected>-- select User --</option>
                <c:forEach items="${command.users}" var="user">
                
                <c:if test="${user.ldapAuthenticated eq true}">
                  <c:set var="suffix" value="[LDAP]"/>
				</c:if>
                <c:if test="${user.ldapAuthenticated ne true}">
                  <c:set var="suffix" value=""/>
				</c:if>
                <option ${user.username eq command.username ? "selected" : ""} value="${user.username}">${user.username} ${suffix}</option>
                </c:forEach>
            </select>
		</c:if>
        </td>
    </tr>
	
    <tr>
        <td><b> </b></td>
        <td>
		<c:if test="${command.newUser ne true and command.newClone ne true}">
			<c:if test="${command.username ne null}">

			<madsonic:url value="profileSettings.view" var="editSettingsUrl">
				<madsonic:param name="profile" value="${command.username}"/>
			</madsonic:url>

				<div class="forward" style="float:left">
					<a href="${editSettingsUrl}">edit Usersettings</a>
				</div>
			</c:if>
		</c:if>
        </td>
    </tr>	
</table>
<p/>
<c:if test="${command.username ne null or command.newUser eq true}">
	
<form:form method="post" action="userSettings.view" commandName="command">
    <c:if test="${not command.admin}">
        <table>
            <tr>
                <td><form:checkbox path="locked" id="locked" cssClass="checkbox"/></td>
                <td><label for="locked"><fmt:message key="usersettings.locked"/></label></td>
            </tr>
		
            <tr>
                <td><form:checkbox path="adminRole" id="admin" cssClass="checkbox"/></td>
                <td><label for="admin"><fmt:message key="usersettings.admin"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="settingsRole" id="settings" cssClass="checkbox"/></td>
                <td><label for="settings"><fmt:message key="usersettings.settings"/></label></td>
            </tr>
            <tr>
                <td style="padding-top:1em"><form:checkbox path="streamRole" id="stream" cssClass="checkbox"/></td>
                <td style="padding-top:1em"><label for="stream"><fmt:message key="usersettings.stream"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="searchRole" id="search" cssClass="checkbox"/></td>
                <td><label for="search"><fmt:message key="usersettings.search"/></label></td>
            </tr>			
            <tr>
                <td><form:checkbox path="jukeboxRole" id="jukebox" cssClass="checkbox"/></td>
                <td><label for="jukebox"><fmt:message key="usersettings.jukebox"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="downloadRole" id="download" cssClass="checkbox"/></td>
                <td><label for="download"><fmt:message key="usersettings.download"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="uploadRole" id="upload" cssClass="checkbox"/></td>
                <td><label for="upload"><fmt:message key="usersettings.upload"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="shareRole" id="share" cssClass="checkbox"/></td>
                <td><label for="share"><fmt:message key="usersettings.share"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="coverArtRole" id="coverArt" cssClass="checkbox"/></td>
                <td><label for="coverArt"><fmt:message key="usersettings.coverart"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="commentRole" id="comment" cssClass="checkbox"/></td>
                <td><label for="comment"><fmt:message key="usersettings.comment"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="lastFMRole" id="lastfm" cssClass="checkbox"/></td>
                <td><label for="lastfm"><fmt:message key="usersettings.lastfm"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="videoConversionRole" id="videoConversion" cssClass="checkbox"/></td>
                <td><label for="videoConversion"><fmt:message key="usersettings.videoconversion"/></label></td>
            </tr>
            <tr>
                <td><form:checkbox path="audioConversionRole" id="audioConversion" cssClass="checkbox"/></td>
                <td><label for="audioConversion"><fmt:message key="usersettings.audioconversion"/></label></td>
            </tr>	    
            <tr>
                <td><form:checkbox path="podcastRole" id="podcast" cssClass="checkbox"/></td>
                <td><label for="podcast"><fmt:message key="usersettings.podcast"/></label></td>
            </tr>
		</table>
    </c:if>

    <c:if test="${not command.admin}">
    <table class="indent">
		<tr>
            <td style="width:120;">User Level </td>
			<td><form:select path="groupId" id="groupId" size="1">
			<c:forEach items="${command.groups}" var="group">
				<form:option value="${group.id}" label="${group.name}"/>
			</c:forEach>
			</form:select>
			</td>
		    <td><c:import url="helpToolTip.jsp"><c:param name="topic" value="access"/></c:import></td>
			</tr>
    </table>
    </c:if>
	 
    <table class="indent">
        <tr>
            <td style="width:120;"><fmt:message key="playersettings.maxbitrate"/></td>
            <td>
                <form:select path="transcodeSchemeName" cssStyle="width:8em">
                    <c:forEach items="${command.transcodeSchemeHolders}" var="transcodeSchemeHolder">
                        <form:option value="${transcodeSchemeHolder.name}" label="${transcodeSchemeHolder.description}"/>
                    </c:forEach>
                </form:select>
            </td>
            <td><c:import url="helpToolTip.jsp"><c:param name="topic" value="transcode"/></c:import></td>
            <c:if test="${not command.transcodingSupported}">
                <td class="warning"><fmt:message key="playersettings.notranscoder"/></td>
            </c:if>
        </tr>
    </table>

    <c:if test="${command.ldapEnabled and not command.admin}">
        <table>
            <tr>
                <td><form:checkbox path="ldapAuthenticated" id="ldapAuthenticated" cssClass="checkbox" onclick="javascript:enablePasswordChangeFields()"/></td>
                <td><label for="ldapAuthenticated"><fmt:message key="usersettings.ldap"/></label></td>
                <td><c:import url="helpToolTip.jsp"><c:param name="topic" value="ldap"/></c:import></td>
            </tr>
        </table>
    </c:if>

    <c:choose>
        <c:when test="${command.newUser}">

            <table class="indent">
                <tr>
                    <td style="width:120;"><fmt:message key="usersettings.username"/></td>
                    <td><form:input path="username" size="32"/></td>
                    <td class="warning"><form:errors path="username"/></td>
                </tr>
                <tr>
                    <td><fmt:message key="usersettings.email"/></td>
                    <td><form:input path="email" size="32"/></td>
                    <td class="warning"><form:errors path="email"/></td>
                </tr>
                <tr>
                    <td><fmt:message key="usersettings.password"/></td>
                    <td><form:password path="password" size="32"/></td>
                    <td class="warning"><form:errors path="password"/></td>
                </tr>
                <tr>
                    <td><fmt:message key="usersettings.confirmpassword"/></td>
                    <td><form:password path="confirmPassword" size="32"/></td>
                    <td/>
                </tr>
                <tr>
                    <td style="width:120;">comment</td>
                    <td><form:input path="comment" size="64"/></td>
                    <td class="warning"><form:errors path="comment"/></td>
                </tr>				
            </table>
        </c:when>

        <c:otherwise>

			<table>
                <tr>
                    <td style="width:120;"><fmt:message key="usersettings.email"/></td>
                    <td><form:input path="email" size="32"/></td>
                    <td class="warning"><form:errors path="email"/></td>
                </tr>
            </table>
			
			<table>
                <tr>
                    <td style="width:120;">comment</td>
                    <td><form:input path="comment" size="64"/></td>
                    <td class="warning"><form:errors path="comment"/></td>
                </tr>
            </table>
		
            <table id="passwordChangeCheckboxTable">
                <tr>
                    <td><form:checkbox path="passwordChange" id="passwordChange" onclick="enablePasswordChangeFields();" cssClass="checkbox"/></td>
                    <td><label for="passwordChange"><fmt:message key="usersettings.changepassword"/></label></td>
                </tr>
            </table>

            <table id="passwordChangeTable" style="display:none">
                <tr>
                    <td style="width:120;"><fmt:message key="usersettings.newpassword"/></td>
                    <td><form:password path="password" id="path" size="32"/></td>
                    <td class="warning"><form:errors path="password"/></td>
                </tr>
                <tr>
                    <td style="width:120;"><fmt:message key="usersettings.confirmpassword"/></td>
                    <td><form:password path="confirmPassword" id="confirmPassword" size="32"/></td>
                    <td/>
                </tr>
            </table>

        </c:otherwise>
    </c:choose>

	<c:if test="${not command.newUser and not command.admin}">
        <table class="indent">
            <tr>
                <td><form:checkbox path="delete" id="delete" cssClass="checkbox"/></td>
                <td><label for="delete"><fmt:message key="usersettings.delete"/></label></td>
            </tr>
        </table>
	</c:if>
	
    <input type="submit" value="<fmt:message key="common.save"/>" style="margin-top:1.5em;margin-right:0.3em">
    <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'" style="margin-top:1.5em">
</form:form>

    </c:if>

<c:if test="${command.reloadNeeded}">
    <script language="javascript" type="text/javascript">
        parent.frames.main.location.href="userSettings.view?";
    </script>
</c:if>    
    
</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

</body>

<c:if test="${customScrollbar}">
<script type="text/javascript">    

		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);

$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</html>
