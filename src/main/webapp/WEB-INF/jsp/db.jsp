<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
<%@ include file="head.jsp" %>
<script type="text/javascript">
function fillBox(what)
{
	document.forms[0].query.value = "SELECT * FROM " + what.firstChild.nodeValue;
	return false;
}
</script>
<style>
input[type="submit"],input[type="button"] {
    padding: 10px 50px;
    margin: 10px 0px -20px 0px;
}
p {
    padding: 0;
    border: 0;
    margin: 0px 0px 5px 0px;
}    
</style>
</head><body class="mainframe bgcolor1" onload="document.getElementById('query').focus()">
<h1>Database query</h1>

<div style="float:left; text-align:left; width: 100%; display: block;">
<fmt:message key="db.tables" />
</div>
<div style="float:left; text-align:left; width: 100%; display: block;">
<fmt:message key="db.query" />
</div>
<div style="float:left;">

<form method="post" action="db.view" name="action">
    <textarea rows="8" cols="140" id="query" name="query" style="margin-top:1em">${model.query}</textarea><br>

    <fieldset style="margin-top:8px;">
    
    <input type="radio" id="default" name="option" value="default" checked="checked" style="margin-top:8px;">
    <label for="export"> Output to Console</label> 
    
    <input type="radio" id="export" name="option" value="export">
    <label for="export"> Export as CSV</label> 
    
    <input type="radio" id="download" name="option" value="download">
    <label for="download"> Download as CSV</label><br>
    
    </fieldset>
    
    <input type="submit" value="query">
</form>

</div>
<div style="float:left;margin-top: 158px;margin-left: -480px;">
<c:if test="${model.export}">
    <p class="warning">
        ${model.exportPath}
    </p>
</c:if>
</div>

<c:if test="${model.download}">
    <p class="warning">
        download
    </p>
</c:if>

<c:if test="${not empty model.result}">
<div style="left;">
    <h1 style="margin-top:530px">Result</h1>

    <table class="indent ruleTableCellDB" style="border-collapse:collapse; white-space:nowrap; border-spacing:1px;">
        <c:forEach items="${model.result}" var="row" varStatus="loopStatus">

            <c:if test="${loopStatus.count == 1}">
                <tr>
                    <c:forEach items="${row}" var="entry">
                        <td class="ruleTableHeader">${entry.key}</td>
                    </c:forEach>
                </tr>
            </c:if>
            <tr>
                <c:forEach items="${row}" var="entry">
                    <td class="ruleTableCellDB">${entry.value}</td>
                </c:forEach>
            </tr>
        </c:forEach>

    </table>
</div>	
</c:if>

<c:if test="${not empty model.error}">
    <h1 style="margin-top:510px;">Error</h1>

    <p class="warning">
        ${model.error}
    </p>
</c:if>

</body></html>