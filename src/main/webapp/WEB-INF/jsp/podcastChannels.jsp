<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--
  ~ This file is part of Madsonic.
  ~
  ~  Madsonic is free software: you can redistribute it and/or modify
  ~  it under the terms of the GNU General Public License as published by
  ~  the Free Software Foundation, either version 3 of the License, or
  ~  (at your option) any later version.
  ~
  ~  Madsonic is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~  Copyright 2009-2016 (C) Sindre Mehus, Martin Karel
  --%>

<%--@elvariable id="model" type="java.util.Map"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
</head>


<body class="mainframe bgcolor1"> 			 <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> 		 <!-- CONTAINER -->

<div style="display:flex; align-items:center; padding-bottom:2em">

    <h1 style="flex-grow:1">
        <img src="<spring:theme code="podcastLargeImage"/>" width="32" alt="">&nbsp;&nbsp;<fmt:message key="podcastreceiver.title"/>
    </h1>
	
    <div style="position: absolute;top:0;right:10px;padding-top:20px;z-index:1000">
        <Table>
        <tr>
            <td style="padding-left:0.2em; padding-bottom:0em;">
				<%@ include file="viewSelectorPodcast.jsp" %>
            </td>
        </tr>
        </table>
    </div>
</div>

<c:if test="${empty model.channels}">
    <p><em><fmt:message key="podcastreceiver.empty"/></em></p>
</c:if>


<c:choose>
	<c:when test="${model.viewAs eq 'list'}">
        <table class="music" style="margin-bottom:2em">
            <c:forEach items="${model.channels}" var="channel">
                <tr>
                    <td class="fit"><i class="fa fa-play clickable icon custom-icon" onclick="parent.playQueue.onPlayPodcastChannel(${channel.key.id}, false)"></i></td>
                    <td class="fit"><i class="fa fa-plus clickable icon custom-icon" onclick="parent.playQueue.onAddPodcastChannel(${channel.key.id}, false)" ></i></td>
                    <td class="fit" style="padding-left:10px;"><a href="podcastChannel.view?id=${channel.key.id}">${fn:escapeXml(channel.key.title)}</a></td>
                    <td class="fit rightalign detail"><fmt:message key="podcastreceiver.episodes"><fmt:param value="${fn:length(channel.value)}"/></fmt:message></td>
                    <td class="truncate detail">${fn:escapeXml(channel.key.description)}</td>
                </tr>
            </c:forEach>
        </table>
    </c:when>
    <c:otherwise>
<c:forEach items="${model.channels}" var="channel" varStatus="loopStatus">

    <c:set var="caption2">
        <fmt:message key="podcastreceiver.episodes"><fmt:param value="${fn:length(channel.value)}"/></fmt:message>
    </c:set>
    <div class="albumThumb">
        <c:import url="playlistArt.jsp">
            <c:param name="podcastChannelId" value="${channel.key.id}"/>
            <c:param name="coverArtSize" value="${model.coverArtSize}"/>
            <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>            
            <c:param name="caption1" value="${fn:escapeXml(channel.key.title)}"/>
            <c:param name="caption2" value="${caption2}"/>
            <c:param name="captionCount" value="2"/>
            <c:param name="showLink" value="true"/>
            <c:param name="appearAfter" value="${loopStatus.count * 20}"/>
        </c:import>
    </div>

</c:forEach>
    </c:otherwise>
</c:choose>

<table style="padding-top:1em"><tr>
    <c:if test="${model.user.settingsRole}">
        <td style="padding-right:2em"><div class="forward"><a href="podcastReceiverRefresh.view?refresh"><fmt:message key="podcastreceiver.check"/></a></div></td>
    </c:if>
    <c:if test="${model.user.adminRole}">
        <td style="padding-right:2em"><div class="forward"><a href="podcastSettings.view?"><fmt:message key="podcastreceiver.settings"/></a></div></td>
    </c:if>
</tr></table>

<c:if test="${model.user.podcastRole}">
    <form method="post" action="podcastReceiverAdmin.view?">
        <table>
            <tr>
                <td><fmt:message key="podcastreceiver.subscribe"/></td>
                <td><input type="text" name="add" value="http://" style="width:30em" onclick="select()"/></td>
                <td><input type="submit" value="<fmt:message key="common.ok"/>"/></td>
            </tr>
        </table>
    </form>
</c:if>

<c:if test="${not empty model.newestEpisodes}">
    <h2 style="margin-top:1em"><fmt:message key="podcastreceiver.newestepisodes"/></h2>
    <table class="music">
        <c:forEach items="${model.newestEpisodes}" var="episode" varStatus="i">
            <tr>
                <!--
                <c:import url="playButtons.jsp">
                    <c:param name="id" value="${episode.mediaFileId}"/>
                    <c:param name="podcastEpisodeId" value="${episode.id}"/>
                    <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode}"/>
                    <c:param name="addEnabled" value="${model.user.streamRole and not model.partyMode}"/>
                    <c:param name="asTable" value="true"/>
                    <c:param name="onPlay" value="top.playQueue.onPlayNewestPodcastEpisode(${i.index})"/>
                </c:import>
                -->
					<c:import url="playAddDownload.jsp">
						<c:param name="id" value="${episode.mediaFileId}"/>
                        <c:param name="podcastEpisodeId" value="${episode.id}"/>                        
						<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode}"/>
						<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
						<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
						<c:param name="addEnabled" value="false"/>
						<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode)}"/>
						<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode)}"/>						
						<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
						<c:param name="starEnabled" value="false"/>
						<c:param name="loveEnabled" value="false"/>
						<c:param name="asTable" value="true"/>
                        <c:param name="onPlay" value="top.playQueue.onPlayNewestPodcastEpisode(${i.index})"/>                        
					</c:import>                
                
                <c:set var="channelTitle" value="${model.channelMap[episode.channelId].title}"/>
                <c:set var="channelDesc" value="${model.channelMap[episode.channelId].description}"/>				

                <td class="fit" style="padding-left:10px;padding-right:10px;">
                    <span title="${episode.title}" class="songTitle">${episode.title}</span>
                </td>

                <td class="fit rightalign detail" style="padding-right:15px;">
                    <span class="detail">${episode.duration}</span>
                </td>

                <td class="fit" style="padding-right:5px;">
                    <a href="podcastChannel.view?id=${episode.channelId}"><span class="detail" title="${channelTitle}">${channelTitle}</span></a>
                </td>
				
 				<td class="fit" style="padding-left:10px; padding-right:10px;">
                    <span class="detail"><fmt:formatDate value="${episode.publishDate}" dateStyle="long"/></span>
                </td>
				
                <td class="truncate" style="padding-right:5px;">
                    <span class="detail" title="${channelTitle}">${channelDesc}</span>
                </td>				
				
            </tr>
        </c:forEach>
    </table>
</c:if>

<c:set var="licenseInfo" value="${model.licenseInfo}"/>
<%@ include file="licenseNotice.jsp" %>

	</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">

    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:650, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:false,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>    
</body>
</html>
