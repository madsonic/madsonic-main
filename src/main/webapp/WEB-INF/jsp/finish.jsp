<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <%@ include file="head.jsp" %>
    
    <style>
    input {
        color: #222 !important;
        border: 1px solid #ddd;
        background-image: url("../icons/madsonic_black/bg-white.jpg");
    }
    </style>
</head>
<body class="mainframe bgcolor1 splash">

<center>
    <div class="bgcolor2 loginsplash" style="border:1px solid black; padding:20px 50px 20px 50px; margin-top:200px;max-width:50em;">

        <div style="margin-left: auto; margin-right: auto; max-width:50em">

			<c:if test="${!model.customlogo}">
				<img src="<spring:theme code="logoImage"/>" width="200" alt=""> 
			</c:if>				
			
			<c:if test="${model.customlogo}">
				<img id="logo-icon" src="coverArt.view?logo=1" width="200" alt=""> 
			</c:if>
		
            <h1><fmt:message key="finish.title"/></h1>
            <br><br>
            
            <p><b>Welcome ${model.username}</b></p>
            <p>${model.email}</p>
            <p class="status" style="padding-top: 1em; padding-bottom: 0.5em"><fmt:message key="finish.text"/></p>

            <c:if test="${not empty model.error}">
                <p style="padding-top: 1em" class="warning"><fmt:message key="${model.error}"/></p>
            </c:if>

            <div class="back" style="padding-left: 60px;margin-top: 1.5em; background-position: center center;"><a href="login.view"><fmt:message key="common.back"/></a></div>

        </div>
    </div>
	</center>
</body>
</html>
