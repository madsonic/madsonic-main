<c:choose><c:when test="${model.customScrollbar}">
<link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>
</c:when><c:otherwise>
<link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
</c:otherwise>
</c:choose>