<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%--@elvariable id="command" type="org.madsonic.command.LdapSettingsCommand"--%>

<html>
<head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

	<c:choose><c:when test="${customScrollbar}">
	<link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
	<script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>
	</c:when><c:otherwise>
	<link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
	</c:otherwise>
	</c:choose>	
	
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    <script type="text/javascript" language="javascript">	
        function init() {
			enableLdapFields();
            enableLdapGroupSyncTypeFields();
        }

		function enableLdapFields() {
			$("#ldap").is(":checked") ? $("#ldapTable").show() : $("#ldapTable").hide();
		}

        function enableLdapGroupSyncTypeFields() {
            var ldapAutoMapping = $("#ldapAutoMapping").is(":checked");
            $("#ldapGroupSyncTypeMERGE").prop("disabled", !ldapAutoMapping);
            $("#ldapGroupSyncTypeREPLACE").prop("disabled", !ldapAutoMapping);
            $("#ldapGroupAutoSync").prop("disabled", !ldapAutoMapping);
        }

    </script>
	
</head>

<body class="mainframe bgcolor1" onload="init()"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="ldap"/>
    <c:param name="toast" value="${command.toast}"/>
</c:import>

<form:form method="post" action="ldapSettings.view" commandName="command">
    <table class="indent"><tr><td>
        <br>
        <form:checkbox path="ldapEnabled" id="ldap" cssClass="checkbox" onclick="enableLdapFields()"/>
        <label for="ldap"><fmt:message key="advancedsettings.ldapenabled"/></label>
        <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldap"/></c:import>
    </td></tr>
    <tr><td><br>
        <p class="detail" style="width:60%;white-space:normal">
        <fmt:message key="ldapsettings.description"/>
        </p>
    </td></tr>
    </table>

    <table class="indent" id="ldapTable" style="padding-left:2em;">
        <tr>
            <td><fmt:message key="advancedsettings.ldapurl"/></td>
            <td colspan="3">
                <form:input path="ldapUrl" size="110"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapurl"/></c:import>
            </td>
        </tr>

        <tr>
            <td><fmt:message key="advancedsettings.ldapsearchfilter"/></td>
            <td colspan="3">
                <form:input path="ldapSearchFilter" size="110"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapsearchfilter"/></c:import>
            </td>
        </tr>
        <tr> 
        <td>&nbsp;</td>
        </tr>
        
        <tr style="padding-top:10px;">
            <td><fmt:message key="advancedsettings.ldapgroupsearchbase"/></td>
            <td colspan="3">
                <form:input path="ldapGroupSearchBase" size="110" />
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapgroupsearchbase"/></c:import>
            </td>
        </tr>        
        
        <tr style="padding-top:10px;">
            <td><fmt:message key="advancedsettings.ldapgroupfilter"/></td>
            <td colspan="3">
                <form:input path="ldapGroupFilter" size="110" />
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapgroupfilter"/></c:import>
            </td>
        </tr>
        
        <tr>
            <td><fmt:message key="advancedsettings.ldapgroupattrib"/></td>
            <td colspan="3">
                <form:input path="ldapGroupAttrib" size="110" />
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapgroupattrib"/></c:import>
            </td>
        </tr>
        <tr> 
        <td>&nbsp;</td>
        </tr>
        <tr>
            <td><fmt:message key="advancedsettings.ldapmanagerdn"/></td>
            <td>
                <form:input path="ldapManagerDn" size="70"/>
            </td>
            <td><fmt:message key="advancedsettings.ldapmanagerpassword"/></td>
            <td>
                <form:password path="ldapManagerPassword" size="20"/>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapmanagerdn"/></c:import>
            </td>
        </tr>
        <tr> 
        <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5">
                <form:checkbox path="ldapAutoShadowing" id="ldapAutoShadowing" cssClass="checkbox"/>
                <label for="ldapAutoShadowing"><fmt:message key="advancedsettings.ldapautoshadowing"><fmt:param value="${command.brand}"/></fmt:message></label>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapautoshadowing"/></c:import>
            </td>
        </tr>
        
        <tr>
            <td colspan="5">
                <form:checkbox path="ldapAutoMapping" id="ldapAutoMapping" cssClass="checkbox" onclick="enableLdapGroupSyncTypeFields()"/>
                <label for="ldapAutoMapping"><fmt:message key="advancedsettings.ldapautomapping"><fmt:param value="${command.brand}"/></fmt:message></label>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapautomapping"/></c:import>
            </td>
        </tr>        
  
        <tr>
            <td colspan="5">
				<form:radiobutton id="ldapGroupSyncTypeMERGE" path="ldapGroupSyncType" value="MERGE" onclick="enableLdapGroupSyncTypeFields()"/>
				<label for="ldapGroupSyncTypeMERGE"><fmt:message key="advancedsettings.synctypemerge"/></label>
				<br>
				<form:radiobutton id="ldapGroupSyncTypeREPLACE" path="ldapGroupSyncType" value="REPLACE" onclick="enableLdapGroupSyncTypeFields()"/>
				<label for="ldapGroupSyncTypeREPLACE"><fmt:message key="advancedsettings.synctypereplace"/></label>
            </td>
        </tr>  
		
        <tr>
            <td colspan="5">
                <form:checkbox path="ldapGroupAutoSync" id="ldapGroupAutoSync" cssClass="checkbox"/>
                <label for="ldapGroupAutoSync"><fmt:message key="advancedsettings.ldapgroupautosync"><fmt:param value="${command.brand}"/></fmt:message></label>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="ldapGroupAutoSync"/></c:import>
            </td>
        </tr>		
		
    </table>

    <c:set var="licenseInfo" value="${command.licenseInfo}"/>
    <%@ include file="licenseNotice.jsp" %>    
    <br>
	
    <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
    <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">

</form:form>

</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${customScrollbar}">
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:true, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:false,
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);
        

$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</body>
</html>