<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>    
    
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">    
    
    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>
    
    <c:if test="${model.listType eq 'random'}">
        <meta http-equiv="refresh" content="30000">
    </c:if>

    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-1.4.2.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.dropdownReplacement-0.5.3.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/style/jquery.dropdownReplacement.css"/>" type="text/css">
    
    <c:if test="${model.listType eq 'hot'}">
    <link href="<c:url value="/style/carousel.css"/>" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/script/jquery.event.drag-2.2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/cloud-carousel.1.0.7.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/cloud-carousel-home.js"/>"></script>
    </c:if>
    
    <script type="text/javascript">  

    // --------------------------------------------
    function onToastmessage(type, text) {
        $().toastmessage(type, text);
    }
    
    function changeMusicFolder(musicFolderId) {
        multiService.setSelectedMusicFolder(musicFolderId, refresh);
    }

    function changeGenre(genre) {
        multiService.setSelectedGenre(genre, refresh);
    }        
    
    function refresh() {
        top.main.location.href = top.main.location.href;
    } 
    
    </script>
    
</head>
<body class="mainframe bgcolor1"> <!-- BODY -->

  <div id="content_home" class="content_home"> <!-- CONTENT -->
	
	<div id="container" class="container"> <!-- CONTAINER -->

    <c:set var="captionLength" value="${model.player.coverArtScheme.captionLength}"/>
    <fmt:message key="home.${model.listType}.text" var="listTypeCat"/>
	

    <c:if test="${not empty model.welcomeTitle}">
        <h1>
            <img src="<spring:theme code="homeImage"/>" width="32" alt="">
            ${model.welcomeTitle}
        </h1>
    </c:if>

    <c:if test="${not empty model.welcomeSubtitle}">
        <h2>${model.welcomeSubtitle}</h2>
    </c:if>
    
    <h2>
    <!-- random newest hot allArtist starredArtist starred tip highest frequent recent decade genre alphabetical top new -->
        <c:forTokens items="${model.homeHeader}" delims=" " var="cat" varStatus="loopStatus">

        <c:choose>
            <c:when test="${loopStatus.count > 1 and  (loopStatus.count - 1) % 15 != 0}">&nbsp;<img src="<spring:theme code="sepImage"/>" alt="">&nbsp;</c:when>
            <c:otherwise></h2><h2></c:otherwise>
        </c:choose>
    
            <madsonic:url var="url" value="home.view">
                <madsonic:param name="listType" value="${cat}"/>
            </madsonic:url>
            
            <c:choose>
                <c:when test="${model.listType eq cat}">

                <span class="headerSelected">
                    <c:if test="${cat eq 'starred' || cat eq 'starredArtist'}">
						<i class="fa fa-star starred"></i>
                    </c:if>
                <fmt:message key="home.${cat}.title"/></span>
                </c:when>
                <c:otherwise>
                    <c:if test="${cat eq 'starred' || cat eq 'starredArtist'}">
						<i class="fa fa-star starred"></i>
                    </c:if>
                    <a href="${url}"><fmt:message key="home.${cat}.title"/></a>
                </c:otherwise>
            </c:choose>
        </c:forTokens>
    </h2>
    
    <div style="margin-top: 20px;">
    <c:if test="${model.isIndexBeingCreated}">
        <p class="warning" style="margin-top: 10px;"><i class="fa fa-info-circle fa-lg fa-fw icon" style="color: #FFC107"></i> <fmt:message key="home.scan"/></p>
    </c:if>
    
    <c:if test="${model.showHomePagerTop}">
        <%@ include file="homePager.jsp" %>
    </c:if>
    </div>
    <br>
    <c:if test="${not empty model.welcomeMessage}">
        <div style="width:15em;float:right;padding:0 1em 0 1em;border-left:1px solid #<spring:theme code="detailColor"/>">
            <madsonic:wiki text="${model.welcomeMessage}"/>
        </div>
    </c:if>
  
    <c:choose>
    <c:when test="${model.listType eq 'hot'}">

        <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            
        <img id="left-but" src="<spring:theme code="buttonLeftImage"/>" title=""/>
        <img id="right-but" src="<spring:theme code="buttonRightImage"/>" title=""/>
        
        <div id="preloader">
            <div id="preloaderText">
                <span id="currentProcess"></span> 
                <span id="persent"></span>
                <div id="stopLoading">cancel</div>
                <div id="startLoading">resume Downloads</div>
            </div>
        </div>
        <div id="resizable">
            <div id = "carousel1" > 
            <c:forEach items="${model.albums}" var="album" varStatus="loopStatus">
                <c:import url="carousel.jsp">
                    <c:param name="albumId" value="${album.id}"/>            
                    <c:param name="auth" value="${album.hash}"/>            
                    <c:param name="albumPath" value="${album.path}"/>
                    <c:param name="albumName" value="${album.albumSetName}"/>
                    <c:param name="albumArtist" value="${album.artist}"/>
                    <c:param name="albumYear" value="${album.albumYear}"/>                
                    <c:param name="coverArtSize" value="150"/>
                    <c:param name="coverArtSize" value="${model.coverArtSize}"/>
                    <c:param name="showLink" value="true"/>
                    <c:param name="showPlayAlbum" value="false"/>                    
                    <c:param name="showAddAlbum" value="false"/>
                </c:import>                
            </c:forEach>
            </div>
        </div>
        <!--
        <div id = "nextcontrols" > 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <c:choose>
                        <c:when test="${model.listType eq 'random'}">
                        </c:when>
                        <c:otherwise>
                            <madsonic:url value="home.view" var="previousUrl">
                                <madsonic:param name="listType" value="${model.listType}"/>
                                <madsonic:param name="listOffset" value="${model.listOffset - model.listSize}"/>
                                <madsonic:param name="genre" value="${model.genre}"/>
                                <madsonic:param name="decade" value="${model.decade}"/>
                            </madsonic:url>
                            <madsonic:url value="home.view" var="nextUrl">
                                <madsonic:param name="listType" value="${model.listType}"/>
                                <madsonic:param name="listOffset" value="${model.listOffset + model.listSize}"/>
                                <madsonic:param name="genre" value="${model.genre}"/>
                                <madsonic:param name="decade" value="${model.decade}"/>                                
                            </madsonic:url>
                                <td width="33%"></td>
                                <td width="80"><fmt:message key="home.albums"><fmt:param value="${model.listOffset + 1}"/><fmt:param value="${model.listOffset + model.listSize}"/></fmt:message></td>
                                <td width="33%"></td>
                            </c:otherwise> 
                        </c:choose>
                    </tr>
            </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="33%"><div class="back"><a href="${previousUrl}"><fmt:message key="common.previous"/></a></div></td>
                    <td width="100" style="padding-up:1.5em">
                        <select name="listSize" onchange="location='home.view?listType=${model.listType}&amp;listOffset=${model.listOffset}&amp;listSize=' + options[selectedIndex].value;">
                            <c:forTokens items="5 10 15 20 25 30 35" delims=" " var="size">
                                <option ${size eq model.listSize ? "selected" : ""} value="${size}"><fmt:message key="home.listsize"><fmt:param value="${size}"/></fmt:message></option>
                            </c:forTokens>
                        </select>
                    </td>
                    <td width="33%"><div class="forwardright"><a href="${nextUrl}"><fmt:message key="common.next"/></a></div></td>
                </tr>
            </table>
        </div> -->

    </c:when>
    <c:when test="${model.listType eq 'users'}">
        <table>
            <tr>
                <th><fmt:message key="home.chart.total"/></th>
                <th><fmt:message key="home.chart.stream"/></th>
            </tr>
            <tr>
                <td><img src="<c:url value="/userChart.view"><c:param name="type" value="total"/></c:url>" alt=""></td>
                <td><img src="<c:url value="/userChart.view"><c:param name="type" value="stream"/></c:url>" alt=""></td>
            </tr>
            <tr>
                <th><fmt:message key="home.chart.download"/></th>
                <th><fmt:message key="home.chart.upload"/></th>
            </tr>
            <tr>
                <td><img src="<c:url value="/userChart.view"><c:param name="type" value="download"/></c:url>" alt=""></td>
                <td><img src="<c:url value="/userChart.view"><c:param name="type" value="upload"/></c:url>" alt=""></td>
            </tr>
    </table>
    </c:when>
    <c:otherwise>

    <c:forEach items="${model.albums}" var="album" varStatus="loopStatus">

        <c:choose>
        <c:when test="${captionLength > 1}">
            <div class="albumThumb">
        </c:when>
        <c:otherwise>
            <div class="albumThumbs">
        </c:otherwise>
        </c:choose>
    
            <c:import url="coverArt.jsp">
                <c:param name="albumId" value="${album.id}"/>
                <c:param name="auth" value="${album.hash}"/>
                <c:param name="albumName" value="${album.albumSetName}"/>
                <c:param name="artistName" value="${album.artist}"/>
                <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>
                <c:param name="coverArtSize" value="${model.coverArtSize}"/>
                <c:param name="coverArtPath" value="${album.coverArtPath}"/>
                <c:param name="showLink" value="true"/>
                <c:param name="showZoom" value="false"/>
                <c:param name="showChange" value="false"/>
                <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                <c:param name="showPlayAlbum" value="${model.showPlayer}"/>                                    
                <c:param name="showAddAlbum" value="${model.showPlayer}"/>
                <c:param name="captionLength" value="${captionLength}"/>   
                <c:param name="showCaption" value="false"/>
                <c:param name="showTopTrack" value="${model.showTopTrack}"/>
                <c:param name="showSmoother" value="false"/>
            </c:import>
            
        <c:if test="${captionLength > 1}">

            <div class="detailmini">
            
            <c:if test="${not empty album.playCount}">
            <div class="detailcolordark">
                <fmt:message key="home.playcount"><fmt:param value="${album.playCount}"/></fmt:message>
            </div>
            </c:if>
            <c:if test="${not empty album.lastPlayed}">
            <div class="detailcolordark">
                <fmt:formatDate value="${album.lastPlayed}" dateStyle="short" var="lastPlayedDate"/>
                <fmt:message key="home.lastplayed"><fmt:param value="${lastPlayedDate}"/></fmt:message>
            </div>
            </c:if>
            <c:if test="${not empty album.created}">
            <div class="detailcolordark">
                <fmt:formatDate value="${album.created}" dateStyle="short" var="creationDate"/>
                <fmt:message key="home.created"><fmt:param value="${creationDate}"/></fmt:message>
            </div>
            <c:if test="${not empty album.year}">
                ${album.year}
            </c:if>
            </c:if>
            <c:if test="${not empty album.rating}">
                <c:import url="rating.jsp">
                    <c:param name="readonly" value="true"/>
                    <c:param name="rating" value="${album.rating}"/>
                </c:import>
            </c:if>
            </div>

            
        <c:choose>
            <c:when test="${empty album.artist and empty album.albumTitle}">
            <div class="detail"><fmt:message key="common.unknown"/></div>
            </c:when>
            <c:otherwise>

            <madsonic:url value="main.view" var="parent">
            <madsonic:param name="id" value="${album.parentId}"/>
            </madsonic:url>

                <div class="detailcolor"><a href="${parent}"><str:truncateNicely upper="${captionLength}">${album.artist}</str:truncateNicely></a></div>
                
                    <c:choose>
                        <c:when test="${fn:startsWith(album.albumTitle,'[')}">
                            <div class="detail"><str:truncateNicely upper="${captionLength}">${fn:split(album.albumTitle,']')[1]}</str:truncateNicely></div>
                        </c:when>
                        <c:otherwise>
                            <div class="detail"><str:truncateNicely upper="${captionLength}">${album.albumTitle}</str:truncateNicely></div>
                        </c:otherwise>
                    </c:choose>
                
            </c:otherwise>
        </c:choose>
        
        </c:if>            
        
        </div>
        
    </c:forEach>
      
    </c:otherwise>
    </c:choose>  

    <c:if test="${model.showHomePagerBottom}">
        <%@ include file="homePager.jsp" %>
    </c:if> 
    
        </div>

       
        
	</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_home").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:true, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>

<script type="text/javascript">

function refreshPage() {
    window.location.href = window.location.href;
}

<c:if test="${model.listType eq 'decade'}">
$("#decadeSelect").dropdownReplacement({selectCssWidth: 60, onSelect : function(value, text, selectIndex){
    location="home.view?listType=${model.listType}&decade=" + value;
}});
</c:if>        

<c:if test="${model.listType eq 'genre'}">
 $("#genre").dropdownReplacement({selectCssWidth: 120, optionsDisplayNum: 15, onSelect : function(value, text, selectIndex){
    location="home.view?listType=${model.listType}&genre=" + value;
}}); 
</c:if>        

<c:if test="${model.listType ne 'tip' and model.listType ne 'tip' and model.listType ne 'genre' and model.listType ne 'decade'}"> <!-- or model.listType ne 'genre' or model.listType ne 'tip' -->
$(document).ready(function() {
    $("#musicFolder").dropdownReplacement({selectCssWidth: 115, optionsDisplayNum: 10, onSelect : function(value, text, selectIndex){
        changeMusicFolder(value);
    }});
});
</c:if>
        
</script> 
</body>
</html>
