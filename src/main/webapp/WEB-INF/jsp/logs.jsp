<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>	
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
    <img src="<spring:theme code="logImage"/>" width="32" alt="">
    <fmt:message key="log.title"><fmt:param value="${model.brand}"/></fmt:message>
</h1>

<c:if test="${model.user.adminRole}">
    
<h2><fmt:message key="help.log"/></h2>

<c:if test="${model.logReverse}">
<div class="forward"><a href="log.view?"><fmt:message key="common.refresh"/></a></div>
</c:if>

<table class="detaillog" style="border-collapse:separate; white-space:nowrap; border-spacing:2px;margin-top:2px;margin-bottom:2px;width: 100%;" cellpadding="1">
    <c:forEach items="${model.logEntries}" var="entry">
        <tr> 
            <td style="width: 1px;">[<fmt:formatDate value="${entry.date}" dateStyle="short" timeStyle="short" type="both"/>]</td>
			
			<c:if test="${entry.level eq 'INFO'}">
            <td class="bgcolor2" style="width: 1px;color: #298A08; background-color: rgba(88, 134, 232, 0.25);">${entry.level}</td>
            <td style="width: 1px;background-color: rgba(88, 134, 232, 0.15);">${entry.category}</td>
            <td class="trancate" style="background-color: rgba(88, 134, 232, 0.05);">${entry.message}</td>
			</c:if>

			<c:if test="${entry.level eq 'DEBUG'}">
            <td class="bgcolor2" style="width: 1px;color: #0084C1; background-color: rgba(88, 134, 232, 0.25);">${entry.level}</td>
            <td style="width: 1px;background-color: rgba(88, 134, 232, 0.15);color: #0084C1;">${entry.category}</td>
            <td class="trancate" style="background-color: rgba(88, 134, 232, 0.05);color: #9C9C9C;">${entry.message}</td>
			</c:if>

			<c:if test="${entry.level eq 'WARN'}">
            <td class="bgcolor2" style="width: 1px;color: #DF7401; background-color: rgba(88, 134, 232, 0.25);">${entry.level}</td>
            <td style="width: 1px;background-color: rgba(88, 134, 232, 0.15);color: #DF7401;">${entry.category}</td>
            <td class="trancate" style="background-color: rgba(88, 134, 232, 0.05);color: #DF7401">${entry.message}</td>
			</c:if>

			<c:if test="${entry.level eq 'ERROR'}">
            <td class="bgcolor2" style="width: 1px;color: #FF0303;">${entry.level}</td>
            <td class="trancate" style="width: 1px;background-color: rgba(88, 134, 232, 0.15);color: #FF0303;">${entry.category}</td>
            <td>${entry.message}</td>
			</c:if>

        </tr>
    </c:forEach>
</table>

<p><fmt:message key="log.logfile"><fmt:param value="${model.logFile}"/></fmt:message> </p>

<c:if test="${not model.logReverse}">
<div class="forward"><a href="log.view?"><fmt:message key="common.refresh"/></a><br></div>
</c:if>
<br>
</c:if>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"xy",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/  
                    alwaysShowScrollbar:true,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:10 },       /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
                $("#content_main").mCustomScrollbar("update");
			});
		})(jQuery);

$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</body>

</html>