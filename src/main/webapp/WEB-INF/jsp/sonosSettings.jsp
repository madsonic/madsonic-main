<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%--
  ~ This file is part of Madsonic.
  ~
  ~  Madsonic is free software: you can redistribute it and/or modify
  ~  it under the terms of the GNU General Public License as published by
  ~  the Free Software Foundation, either version 3 of the License, or
  ~  (at your option) any later version.
  ~
  ~  Madsonic is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~  Copyright 2015 (C) Sindre Mehus
  --%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="sonos"/>
    <c:param name="toast" value="${model.toast}"/>
</c:import>

<form method="post" action="sonosSettings.view">
    <br>
    <div>
        <!-- disabled="disabled" -->
        <input type="checkbox" name="sonosEnabled" id="sonosEnabled" class="checkbox" 
               <c:if test="${model.sonosEnabled}">checked="checked"</c:if>/>
        <label for="sonosEnabled"><fmt:message key="sonossettings.enabled"/></label>
    </div>
    <br>
    <br>
    <p class="detail" style="width:60%;white-space:normal">
        <fmt:message key="sonossettings.description"/>
    </p>
    <div>
		<div style="min-width:140px;display:inline-table;"><fmt:message key="sonossettings.servicename"/></div>
        <input name="sonosServiceName" id="sonosServiceName" size="40" value="<c:out value="${model.sonosServiceName}" escapeXml="true"/>"/>
    </div>
	
	<div style="min-width:140px;margin-top:10px;display:inline-table;">Sonos Rating Type</div>
	<select name="sonosRatingType">
		<c:if test="${model.sonosRatingType eq 'STARRED'}"><option selected="selected">STARRED</option></c:if>
		<c:if test="${model.sonosRatingType ne 'STARRED'}"><option>STARRED</option></c:if>
		<c:if test="${model.sonosRatingType eq 'LOVED'}"><option selected="selected">LOVED</option></c:if>
		<c:if test="${model.sonosRatingType ne 'LOVED'}"><option>LOVED</option></c:if>
	</select>
	
    <p class="detail" style="width:60%;white-space:normal;padding-top:10px">
        <fmt:message key="sonossettings.servicename.description"/>
    </p>
    <c:set var="licenseInfo" value="${model.licenseInfo}"/>
    <%@ include file="licenseNotice.jsp" %>
    <br>
    <p>
        <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
        <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">
    </p>
</form>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:true,
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);
        
        $("#content_main").mCustomScrollbar("update");


$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</body>

</html>