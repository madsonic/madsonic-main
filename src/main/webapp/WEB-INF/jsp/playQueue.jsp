<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/nowPlayingService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/playQueueService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/playlistService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/cast_sender-v2.js"/>"></script>
    <%@ include file="playQueueCast.jsp" %>
    
    <style type="text/css">
        .ui-slider .ui-slider-handle {
            width: 12px; height: 12px; cursor: pointer;
            background: #0090BB;
            display:none;
            border:none;
        }
        .ui-slider {
            cursor: pointer;
            border:none;
        }
        .ui-slider-range-min {
            background: #0090BB;
        }
        
        .ui-slider-range-max {
            background: #444;
        }
        
        .ui-slider-handle:focus {
            outline:none;
        }
        #startButton, #stopButton, #bufferButton {
            cursor:pointer; font-size:30px; color:#0090BB;
        }
        
        @media all and (min-width: 1px) {
            #fastpreviousButton, #fastnextButton, 
            #repeat_off, #repeat_on, 
            #book1, #book2, #book3, 
            #saveQueue, #loadQueue, 
            #followMe_off, #followMe_on, 
            #settings1, #clear1, #save, #moreActions, 
            #shuffle, #undo, #node,
            #progress-and-duration,
            #current-and-count {
                display:none !important;
            } 
        }

        @media all and (min-width: 700px) {
            #lock1, #lock2, #lock3, 
            #repeat_off, #repeat_on,    
            #followMe_off, #followMe_on, 
            #shuffle, #node, #undo,
            #settings1 {
                display:inline !important;
            } 
        }          
        
        @media all and (min-width: 800px) {
            #progress-and-duration {
                display:inline !important;
            } 
        }        
        
        @media all and (min-width: 1000px) {
            #moreActions, #saveQueue, #loadQueue, {display:inline !important;} 
            #moreActions {width:24px;}
        }        
        
        @media all and (min-width: 1200px) {
            #fastpreviousButton, #fastnextButton, 
            #book1, #book2, #book3, 
            #repeat_off, #repeat_on, 
            #saveQueue, #loadQueue, 
            #settings1, #clear1, #save, #node, #undo,
            #progress-and-duration,
            #current-and-count,
            #moreActions {
                display:inline !important;
            } 
            #moreActions {
                width:24px;
            }            
        }
        
        @media all and (min-width: 1400px) {
            #moreActions {
                width:150px;
            }            
        }
        
        #fastpreviousButton, #fastnextButton{
            cursor:pointer; font-size:16px; padding:5px; margin-left:5px; margin-right:5px;
        }        
        #previousButton, #nextButton, #starButton, #bookmarkButton {
            cursor:pointer; font-size:18px; padding:5px; margin-left:5px; margin-right:5px;
        }
        
        #settings1, #clear1, #save, #node,
        #book1, #book2, #book3, 
        #repeat_off, #repeat_on, 
        #starred_off, #starred_on,        
        #loved_off, #loved_on,
        #shuffle, #undo,
        #followMe_on, #followMe_off, #lock0, #lock1, #lock2
        {
            cursor:pointer; font-size:18px; 
             padding-top:3px; 
             margin-left:6px; 
             margin-right:6px; 
        }
        
        .savePlayQueue, 
        .loadPlayQueue {
            cursor:pointer;  
            padding-left:6px;
            padding-right:6px;
        }
        #muteOn, #muteOff {
            cursor:pointer; font-size:20px; padding:8px
        }
        
        #starred_off, #starred_on {
            display:none;
        }
	
        #loved_off, #loved_on {
            display:none;
        }    
    
        #castOn, #castOff {
            margin-left:15px; margin-right:20px; cursor:pointer;display:none;
        }
        #startButton:hover, #stopButton:hover {
            opacity: 0.8;
        }
        #songName {
            cursor:pointer; font-weight:600; display:block;
        }
        #artistName {
            cursor:pointer; font-weight:300; display:block;
        }
        #coverArt {
            cursor:pointer; width:80px; height:80px;
            border-radius: 3px;
            border-color: #000;
        }
        #volume {
            width:100px; height:4px; margin-right:20px
        }
        #progress {
            height:4px; margin: 5px 20px 5px 15px;
        }
 </style>

<script type="text/javascript" language="javascript">

    var player = null;
    var songs = null;
    var currentAlbumUrl = null;
    var currentStreamUrl = null;
    var currentStreamIndex = null;
    var currentIndex = null;    
    var startPlayer = true;
    var PlayLast = false;
    var repeatEnabled = false;
    var followMeEnabled = false;
    var lockMode = 0;
    
    var castPlayer;
    var localPlayer = null;
    var jukeboxPlayer = false;
    var externalPlayer = false;
    var externalPlayerWithPlaylist = false;
    var ignore = false;
    var force = false;
    var currentPosition = 0;
    var current_pos = -1;
	var start_pos;
	var stop_pos;

    function init() {
        
        jukeboxPlayer = ${model.player.jukebox};
        externalPlayer = ${model.player.external};
        externalPlayerWithPlaylist = ${model.player.externalWithPlaylist};
        castPlayer = new CastPlayer(!jukeboxPlayer && !externalPlayer && !externalPlayerWithPlaylist);
        initMouseListener();
   
        dwr.engine.setErrorHandler(null);

        $("#dialog-select-playlist").dialog({resizable: true, width: 400, height: 220, autoOpen: false,
            buttons: {
                "<fmt:message key="common.cancel"/>": function() {
                    $(this).dialog("close");
                }
            }});

        $("#progress").slider({max: 0, range: "min"});
        $("#volume").slider({max: 100, value: 50, animate: "fast", range: "min"});
        $("#volume").on("slidestop", onVolumeChanged);
        $("#progress").on("slidestop", onProgressChanged);
        $(".ui-slider").css("background", $("#dummy").css("background-color"));

        if (!externalPlayerWithPlaylist) {
            $("#playlistBody").sortable({
				
            start: function(event, ui) {
                start_pos = ui.item.index();
                if (currentStreamIndex == currentStreamUrl + "&index=" + start_pos) {
                    force = true;
                }
                 for (var i = 0; i < songs.length; i++) {
                     var song  = songs[i];
                        if (song.streamUrl + "&index=" + i == currentStreamIndex ) {
                            current_pos = i;
                         } 
                 }                
            },                
            stop: function(event, ui) {
				
				stop_pos = ui.item.index();
                var indexes = [];
                $("#playlistBody").children().each(function() {
                    var id = $(this).attr("id").replace("pattern", "");
                    if (id.length > 0) {
                        indexes.push(parseInt(id) - 1);
                    }
                });
                onRearrange(indexes);

                if (force) {
                  currentStreamIndex = currentStreamUrl + "&index=" + ui.item.index();
                  force = false;
                } else {
					if (start_pos <= current_pos && stop_pos >= current_pos) {
					    currentStreamIndex = currentStreamUrl + "&index=" + (current_pos - 1);
					} 
					if (start_pos >= current_pos && stop_pos <= current_pos) {
						currentStreamIndex = currentStreamUrl + "&index=" + (current_pos + 1);
					}
				}
				
			},
                cursor: "move",
                axis: "y",
                containment: "parent",
                helper: function (e, tr) {
                    var originals = tr.children();
                    var trclone = tr.clone();
                    trclone.children().each(function (index) {
                    // Set cloned cell sizes to match the original sizes
                    $(this).width(originals.eq(index).width());
                    $(this).css("maxWidth", originals.eq(index).width());
                    $(this).css("border-top", "1px solid #888");
                    $(this).css("border-bottom", "1px solid #888");
                    $(this).css("background-color", "#333");
                    $(this).css("color", "#fff"); 
                    });
                    return trclone;
                }
            });
        }

        <c:if test="${model.player.web}">createPlayer();</c:if>

        if (jukeboxPlayer || externalPlayer || externalPlayerWithPlaylist) {
            startTimer();
            $("#progress").hide();
            $("#progress-and-duration").hide();
        }
        if (externalPlayer || externalPlayerWithPlaylist) {
            $("#volume").hide();
            $("#muteOn").hide();
            $("#muteOff").hide();
        }
        if (externalPlayerWithPlaylist) {
            $("#nextButton").hide();
            $("#previousButton").hide();
        }

        getPlayQueue();
    }

    function initMouseListener() {
        $("body").mouseleave(function (event) {
            if (event.clientY < 30) {
                if (lockMode == 0) {
                   setFrameHeight(95);
                }
                $(".ui-slider-handle").stop().fadeOut();
            }
        });

        // LOCKMODE: 0 UNLOCKED, 2 FIXED, 1 LOCKED
        $("body").mouseenter(function (event) {
            var height = $("body").height() + 25;
            if (event.clientY < 95) {
                height = Math.min(height, window.top.innerHeight * 0.8);
            } else {
                height = Math.min(height, window.top.innerHeight * 0.8) - 7;
            }
            if (lockMode == 0) { 
                setFrameHeight(height);
                $(".ui-slider-handle").stop().fadeIn();
            }
            else if (lockMode == 1){
                setFrameHeight("${model.playQueueSize}");
                $(".ui-slider-handle").stop().fadeIn();
            }
            else if (lockMode == 2){
                setFrameHeight(95);   
                $(".ui-slider-handle").stop().fadeIn();
            } 
        });
    }

    function setFrameHeight(height) {
        parent.setPlayQueueHeight(height);
    }

    function startTimer() {
        nowPlayingService.getNowPlayingForCurrentPlayer(nowPlayingCallback);
        setTimeout("startTimer()", 8000);
    }

    function nowPlayingCallback(nowPlayingInfo) {
        if (nowPlayingInfo != null && nowPlayingInfo.streamUrl != currentStreamUrl) {
            getPlayQueue();
            if (currentAlbumUrl != nowPlayingInfo.albumUrl && parent.main.updateNowPlaying) {
                parent.main.location.replace("nowPlaying.view?");
                currentAlbumUrl = nowPlayingInfo.albumUrl;
            }
            currentStreamUrl = nowPlayingInfo.streamUrl;
            updateCurrentImage();
            updateCoverArt(songs[getCurrentSongIndex()]);
        }
    }

    function createPlayer() {
        localPlayer = new Audio();
	    
        localPlayer.addEventListener("ended", function() {
            if (getCurrentSongIndex() == songs.length - 1) {
               	if (followMeEnabled) { onPlayFollowMeRadio(); } 
            }
            onNext(repeatEnabled)});
        localPlayer.addEventListener("canplay", function() {updateControls()});
        localPlayer.addEventListener("canplaythrough", function() {updateControls()});
        localPlayer.addEventListener("loadeddata", function() {updateControls()});
        localPlayer.addEventListener("loadedmetadata", function() {updateControls()});
        localPlayer.addEventListener("loadstart", function() {updateControls()});
        localPlayer.addEventListener("seeked", function() {updateControls()});
        localPlayer.addEventListener("seeking", function() {updateControls()});
        localPlayer.addEventListener("stalled", function() {updateControls()});
        localPlayer.addEventListener("suspend", function() {updateControls()});
        localPlayer.addEventListener("error", function() {updateControls()});
        localPlayer.addEventListener("waiting", function() {updateControls()});
        localPlayer.addEventListener("play", function() {updateControls()});
        localPlayer.addEventListener("playing", function() {updateControls()});
        localPlayer.addEventListener("pause", function() {updateControls()});
        localPlayer.addEventListener("volumechange", function() {updateControls()});
        localPlayer.addEventListener("timeupdate", function() {updateProgressBar()});

        localPlayer.volume = 0.66;
    }

    function updateControls() {
        var ready = localPlayer.readyState == localPlayer.HAVE_FUTURE_DATA ||
                    localPlayer.readyState == localPlayer.HAVE_ENOUGH_DATA ||
                    localPlayer.networkState == localPlayer.NETWORK_NO_SOURCE;

        var paused = localPlayer.paused;
        $("#startButton").toggle(ready && paused);
        $("#stopButton").toggle(ready && !paused);
        $("#bufferButton").toggle(!ready);
        toggleSpinner(ready && !paused);

        $("#volume").slider("option", "value", Math.round(localPlayer.volume * 100));

        var muted = localPlayer.muted;
        $("#muteOn").toggle(!muted);
        $("#muteOff").toggle(muted);
    }

    function toggleSpinner(spin) {
        $(".fa-circle-o-notch").toggleClass("fa-spin", spin);
    }

    function updateProgressBar() {
        var position = Math.round(localPlayer.currentTime);
        var duration = localPlayer.duration;
        duration = isNaN(duration) ? 0 : duration;

        if (position != currentPosition) {
            currentPosition = position;
            $("#progress").slider("option", "value", position * 1000);
        }

        $("#progress").slider("option", "max", Math.round(duration * 1000));
        $("#progress-text").html(formatDuration(Math.round(position)));
        $("#duration-text").html(formatDuration(Math.round(duration)));
    }

    function formatDuration(duration) {
        var hours = Math.floor(duration / 3600);
        duration = duration % 3600;
        var minutes = Math.floor(duration / 60);
        var seconds = Math.floor(duration % 60);

        var result = "";
        if (hours > 0) {
            result += hours + ":";
            if (minutes < 10) {
                result += "0";
            }
        }
        result += minutes + ":";
        if (seconds < 10) {
            result += "0";
        }
        result += seconds;
        return result;
    }

    function getPlayQueue() {
        playQueueService.getPlayQueue(playQueueCallback);
    }

    function onClear() {
        var ok = true;
    <c:if test="${model.partyMode}">
        ok = confirm("<fmt:message key="playlist.confirmclear"/>");
    </c:if>
        if (ok) {
            $("#current").html( "");        
            $("#current").hide();
            $("#currenticon").hide();  
            playQueueService.clear(playQueueCallback);
        }
    }

    function onStart() {
        if (castPlayer.castSession) {
            castPlayer.playCast();
        } else if (localPlayer) {
            if (localPlayer.ended && getCurrentSongIndex() == songs.length -1) {
                skip(0, 0, true);
            } else {
                localPlayer.play();
            }

        } else {
            playQueueService.start(playQueueCallback);
        }
    }

    function onStop() {
        if (castPlayer.castSession) {
            castPlayer.pauseCast();
        } else if (localPlayer) {
            localPlayer.pause();
        } else {
            playQueueService.stop(playQueueCallback);
        }
    }

    function onVolumeChanged() {
        var value = parseInt($("#volume").slider("option", "value"));
        if (castPlayer.castSession) {
            castPlayer.setCastVolume(value / 100, false);
        } else if (localPlayer) {
            localPlayer.volume = value / 100.0;
            localPlayer.muted = false;
        } else if (jukeboxPlayer) {
            playQueueService.setJukeboxGain(value / 100);
        }
    }

    function onProgressChanged() {
        var value = parseInt($("#progress").slider("option", "value") / 1000);
        if (localPlayer && !castPlayer.castSession && Math.round(localPlayer.currentTime) != value) {
            localPlayer.currentTime = value;
            localPlayer.play();
        }
    }

    function onMute(mute) {
        $("#muteOn").toggle(!mute);
        $("#muteOff").toggle(mute);

        if (castPlayer.castSession) {
            castPlayer.castMute(mute);
        } else if (localPlayer) {
            localPlayer.muted = mute;
        } else if (jukeboxPlayer) {
            playQueueService.setJukeboxMute(mute);
        }
    }

    function keyboardShortcut(action) {
        if (action == "togglePlayPause") {
            if ($("#startButton").is(":visible")) {
                $("#startButton").click();
            } else if ($("#stopButton").is(":visible")) {
                $("#stopButton").click();
            }
        } else if (action == "previous" && $("#previousButton").is(":visible")) {
            $("#previousButton").click();
        } else if (action == "next" && $("#nextButton").is(":visible")) {
            $("#nextButton").click();
        } else if (action == "volumeDown" && $("#volume").is(":visible")) {
            var volume = parseInt($("#volume").slider("option", "value"));
            $("#volume").slider("option", "value", Math.max(0, volume - 5));
            onVolumeChanged();
        } else if (action == "volumeUp" && $("#volume").is(":visible")) {
            var volume = parseInt($("#volume").slider("option", "value"));
            $("#volume").slider("option", "value", Math.min(100, volume + 5));
            onVolumeChanged();
        } else if (action == "seekForward") {
            var position = parseInt($("#progress").slider("option", "value"));
            var duration = parseInt($("#progress").slider("option", "max"));
            $("#progress").slider("option", "value", Math.min(duration, position + 1000));
            onProgressChanged();
        } else if (action == "seekBackward") {
            var position = parseInt($("#progress").slider("option", "value"));
            $("#progress").slider("option", "value", Math.max(0, position - 1000));
            onProgressChanged();
        }
    }

    function onNext(wrap) {
        var index = parseInt(getCurrentSongIndex()) + 1;
        if (wrap) {
            index = index % songs.length;
        }
        skip(index, 0, true);
    }
    function onPrevious() {
        if (localPlayer && !castPlayer.castSession && localPlayer.currentTime > 4.0) {
            skip(parseInt(getCurrentSongIndex()), 0, true);
        } else {
            skip(Math.max(0, parseInt(getCurrentSongIndex()) - 1), 0, true);
        }
    }
    function onPlay(id) {
        playQueueService.play(id, playQueueCallback);
    }

    function onPlayAll(ids) {
        playQueueService.playAll(ids, playQueueCallback);
    }
 
    function onAddAll(ids) {
        startPlayer = false;
        playQueueService.addAll(ids, playQueueCallback);
    }
 
    function onPlayMore(id) {
        playQueueService.playMore(id, playQueueCallback);
    }
    
    function onPlayAdd(id) {
        PlayLast = true;
        playQueueService.add(id, playQueueCallback);
    }

    function onAddPlaylist(id) {
        startPlayer = false;
        playQueueService.AddPlaylist(id, playQueueCallback);
    }
    
    function onPlayPlaylist(id, random) {
        playQueueService.playPlaylist(id, random, playQueueCallback);
    }

    function onPlayPodcastChannel(id) {
        playQueueService.playPodcastChannel(id, playQueueCallback);
    }
	
    function onAddPodcastChannel(id) {
        playQueueService.addPodcastChannel(id, playQueueCallback);
        toast("<fmt:message key="main.addlast.toast"/>");
    }
	
    function onPlayPodcastEpisode(id) {
        playQueueService.playPodcastEpisode(id, playQueueCallback);
    }
    function onPlayNewestPodcastEpisode(index) {
        playQueueService.playNewestPodcastEpisode(index, playQueueCallback);
    }

    function onPlayRandom(id, count) {
        playQueueService.playRandom(id, count, playQueueCallback);
    }
    
    function onPlayRandomAll(ids, count) {
        playQueueService.playRandomAll(ids, count, playQueueCallback);
    }
    
    function onPlaySimilar(id, count) {
        playQueueService.playSimilar(id, count, playQueueCallback);
    }    
    
    function onPlayTopTrack(artist) {
         playQueueService.playTopTrack(artist, playQueueCallback);
    }
    
    function onAddTopTrack(artist) {
        startPlayer = false;
        playQueueService.addTopTrack(artist, playQueueCallback);
    }

    function onUpdateTopTrack(artist, id) {
        playQueueService.updateTopTrack(artist);
        parent.frames.main.location.href="main.view?id=" + id;
    }

    function onPlayFollowMeRadio() {
        playQueueService.playFollowMeRadio(playQueueCallback);
    }
    
    function onAdd(id) {
        startPlayer = false;
        playQueueService.add(id, playQueueCallback);
        toast("<fmt:message key="main.addlast.toast"/>");
    }

    function onAddNext(id) {
        startPlayer = false;
        playQueueService.addAt(id, getCurrentSongIndex() + 1, playQueueCallback);
        toast("<fmt:message key="main.addnext.toast"/>");
    }
    
    function onAddSelectedNext(indexes) {
        startPlayer = false;
        playQueueService.addSelectedAt(indexes, getCurrentSongIndex() + 1, playQueueCallback);
    }

    function onAddSelectedLast(indexes) {
        startPlayer = false;
        playQueueService.addSelected(indexes, playQueueCallback);
    }
    
    function onPlayGenreRadio(genres, count, year) {
        playQueueService.playGenreRadio(genres, count, year, playQueueCallback);
    }    

    function onPlayMoodRadio(moods, count, year) {
        playQueueService.playMoodRadio(moods, count, year, playQueueCallback);
    }    
    
    function onPlayRandomRadio(count, genre, mood, year, musicFolderId) {
        playQueueService.playRandomRadio(count, genre, mood, year, musicFolderId, playQueueCallback);
    }    
    
    function onShuffle() {
        playQueueService.shuffle(playQueueCallback);
    }
    
    function onToogleBookmark(index) {
        playQueueService.deleteBookmark(index, playQueueCallback);
    }
    
    function onStar(index) {
        playQueueService.toggleStar(index, playQueueCallback);
    }

    function onLove(index) {
        playQueueService.toggleLove(index, playQueueCallback);
    }
    
    function onStarCurrentSong(star) {
        var i = getCurrentSongIndex();
        if (i != -1) {
            onStar(i);
            $("#starred_off").toggle(!star);
            $("#starred_on").toggle(star);
        }
    }    

    function onLoveCurrentSong(love) {
        var i = getCurrentSongIndex();
        if (i != -1) {
            onLove(i);
            $("#loved_off").toggle(!love);
            $("#loved_on").toggle(love);
        }
    }    

    function onRemove(index) {
        playQueueService.remove(index, playQueueCallback);
		var currentIndex = parseInt(currentStreamIndex.substr(currentStreamIndex.length - 1));
		if (index < currentIndex) {
			currentStreamIndex = currentStreamUrl + "&index=" + (currentIndex - 1);
		}
    }
    
    function onRemoveSelected() {
        var indexes = new Array();
        var counter = 0;
        for (var i = 0; i < songs.length; i++) {
            var index = i + 1;
            if ($("#songIndex" + index).is(":checked")) {
                indexes[counter++] = i;
            }
        }
        playQueueService.removeMany(indexes, playQueueCallback);
    }
    function onRearrange(indexes) {
        playQueueService.rearrange(indexes, playQueueCallback);
    }
    function onToggleRepeat() {
        playQueueService.toggleRepeat(playQueueCallback);
    }
    function onToggleFollowMe() {
        playQueueService.toggleFollowMe(playQueueCallback);
    }
    function onToggleLockMode() {
        playQueueService.toggleLockMode(playQueueCallback);
    }    
    function onUndo() {
        playQueueService.undo(playQueueCallback);
    }
    function onSortByRank() {
        playQueueService.sortByRank(playQueueCallback);
    }
    function onSortByTrack() {
        playQueueService.sortByTrack(playQueueCallback);
    }
    function onSortByAlbum() {
        playQueueService.sortByAlbum(playQueueCallback);
    }
    function onSortByArtist() {
        playQueueService.sortByArtist(playQueueCallback);
    }
    function onSortByGenre() {
        playQueueService.sortByGenre(playQueueCallback);
    }
    function onSortByMood() {
        playQueueService.sortByMood(playQueueCallback);
    }
    function onSortByBpm() {
        playQueueService.sortByBpm(playQueueCallback);
    }
    function onSortByComposer() {
        playQueueService.sortByComposer(playQueueCallback);
    }

    function onRewind() {
        var positionMillis = localPlayer ? Math.round(1000.0 * localPlayer.currentTime) : 0;
        positionMillis = positionMillis - 10000;
        if (positionMillis > 0) {
            localPlayer.currentTime = positionMillis / 1000;
        }
    }    

    function onForward() {
        var positionMillis = localPlayer ? Math.round(1000.0 * localPlayer.currentTime) : 0;
        positionMillis = positionMillis + 10000;
        if (positionMillis > 0) {
            localPlayer.currentTime = positionMillis / 1000;
        }
    }     

    function onBookmark() {
        var positionMillis = localPlayer ? Math.round(1000.0 * localPlayer.currentTime) : 0;
        playQueueService.createBookmark(getCurrentSongIndex(), positionMillis, playQueueCallback);
		var bookmarkPosition = formatDuration(positionMillis / 1000);
        $().toastmessage("showSuccessToast", "Bookmark at " + bookmarkPosition);
        $("#book1").attr('id', 'book3')
    }

    function onControlNode(url, command) {
        playQueueService.doControlNode(url, command);
        $().toastmessage("showSuccessToast", "NodeControl: " + command.toUpperCase());
    }

    function onConfigNode(url) {
        playQueueService.doConfigNode(url);
        $().toastmessage("showSuccessToast", "send Credential to Node");
    }
    
    function onSendToNode(url) {
        playQueueService.doSendToNode(url);
        $().toastmessage("showSuccessToast", "send PlayQueue to Node");
    }
    
    function onSavePlayQueue() {
        var positionMillis = localPlayer ? Math.round(1000.0 * localPlayer.currentTime) : 0;
        playQueueService.savePlayQueue(getCurrentSongIndex(), positionMillis);
        toast("<fmt:message key="playlist.toast.saveplayqueue"/>");
    }

    function onNode() {
        parent.main.location.href = "nodeSettings.view?";
    }
    
    function onLoadPlayQueue() {
        playQueueService.loadPlayQueue(playQueueCallback);
        toast("<fmt:message key="playlist.toast.loadplaylist"/>");
    }
    
    function onSavePlaylist() {
        playlistService.createPlaylistForPlayQueue(function (playlistId) {
            parent.main.location.href = "playlist.view?id=" + playlistId;
            toast("<fmt:message key="playlist.toast.saveasplaylist"/>");
        });
    }
    
    function onAppendPlaylist() {
        playlistService.getWritablePlaylists(playlistCallback);
    }
    function playlistCallback(playlists) {
        $("#dialog-select-playlist-list").empty();
        for (var i = 0; i < playlists.length; i++) {
            var playlist = playlists[i];
            $("<p class='dense'><b><a href='#' onclick='appendPlaylist(" + playlist.id + ")'>" + escapeHtml(playlist.name)
                    + "</a></b></p>").appendTo("#dialog-select-playlist-list");
        }
        $("#dialog-select-playlist").dialog("open");
    }
    function appendPlaylist(playlistId) {
        $("#dialog-select-playlist").dialog("close");

        var mediaFileIds = new Array();
        for (var i = 0; i < songs.length; i++) {
            if ($("#songIndex" + (i + 1)).is(":checked")) {
                mediaFileIds.push(songs[i].id);
            }
        }
        playlistService.appendToPlaylist(playlistId, mediaFileIds, function (){
            parent.main.location.href = "playlist.view?id=" + playlistId;
            toast("<fmt:message key="playlist.toast.appendtoplaylist"/>");
        });
    }

    function playQueueCallback(playQueue) {
        songs = playQueue.entries;
        lockMode = playQueue.lockMode;
        repeatEnabled = playQueue.repeatEnabled;
        followMeEnabled = playQueue.followMeEnabled;
        
        if ($("#start")) {
            $("#start").toggle(!playQueue.stopEnabled);
            $("#stop").toggle(playQueue.stopEnabled);
        }

        if (repeatEnabled) {
            $('.ToggleButton').attr('id', 'repeat_on');
        }
        if (followMeEnabled) {
            $('.FollowMeButton').attr('id', 'followMe_on');
        }
        if (lockMode > -1) {
            $('.LockButton').attr('id', 'lock' + lockMode);
        }
        
        if (songs.length == 0) {
            $("#songCountAndDuration").html("");
            $("#duration").html("");
            $("#empty").show();
            $("#counticon").hide();
            $("#count").hide();
            $("#durationicon").hide();            
        } else {
            $("#songCountAndDuration").html(songs.length + " <fmt:message key="playlist2.songs"/>&nbsp;&nbsp;&bull;&nbsp;&nbsp;" + playQueue.durationAsString);
            $("#empty").hide();
            $("#counticon").show();
            $("#count").html(songs.length);
            $("#count").show(); 
            $("#durationicon").show();
            $("#duration").html(playQueue.durationAsString);
            $("#duration").show();    	               
        }
        
        // Delete all the rows except for the "pattern" row
        dwr.util.removeAllRows("playlistBody", { filter:function(tr) {
            return (tr.id != "pattern");
        }});

        // Create a new set cloned from the pattern row
        for (var i = 0; i < songs.length; i++) {
            var song  = songs[i];
            var id = i + 1;
            dwr.util.cloneNode("pattern", { idSuffix:id });

            $("#currentIndex" + id).html(i);
            $("#currentIndex" + id).hide();				
			
            $("#bookmarkSong" + id).addClass(song.bookmarkPosition > 0 ? "fa-bookmark starred" : "fa-bookmark-o");

            $("#loveSong" + id).addClass(song.loved ? "fa-heart starred" : "fa-heart-o");
            
            $("#starSong" + id).addClass(song.starred ? "fa-star starred" : "fa-star-o");
            
            if ($("#discNumber" + id)) {
                $("#discNumber" + id).html(song.discNumber);
            }
            if ($("#trackNumber" + id)) {
                $("#trackNumber" + id).html(song.trackNumber);
            }
            
            if ($("#currentImage" + id) && song.streamUrl == currentStreamUrl) {
                $("#currentImage" + id).show();
            }
            if ($("#title" + id)) {
                $("#title" + id).html(song.title);
                $("#title" + id).attr("title", song.title);
            }
            if ($("#titleUrl" + id)) {
                $("#titleUrl" + id).html(song.title);
                $("#titleUrl" + id).attr("title", song.title);
                $("#titleUrl" + id).click(function () {skip(this.id.substring(8) - 1, 0, true)});
            }
            if ($("#album" + id)) {
                $("#album" + id).html(song.album);
                $("#album" + id).attr("title", song.album);
                $("#albumUrl" + id).attr("href", song.albumUrl);
            }
            if ($("#artist" + id)) {
                $("#artist" + id).html(song.artist);
                $("#artist" + id).attr("title", song.artist);
            }
            if ($("#composer" + id)) {
                $("#composer" + id).html(song.composer);
            }
            if ($("#genre" + id)) {
                $("#genre" + id).html(song.genre);
            }
            if ($("#mood" + id)) {
                $("#mood" + id).html(song.mood);
            }
            if ($("#year" + id)) {
                $("#year" + id).html(song.year);
            }
            if ($("#bpm" + id)) {
                $("#bpm" + id).html(song.bpm);
            }
            if ($("#bitRate" + id)) {
                $("#bitRate" + id).html(song.bitRate);
            }
            if ($("#duration" + id)) {
                $("#duration" + id).html(song.durationAsString);
            }
            if ($("#format" + id)) {
                $("#format" + id).html(song.sourceFormat);
            }
            if ($("#fileSize" + id)) {
                $("#fileSize" + id).html(song.fileSize);
            }
            if ($("#rank" + id) && song.rank > 0) {
                $("#rank" + id).html( (song.rank < 10) ? "0" + song.rank : song.rank);
                $("#rank" + id).addClass("rank");
            }
            $("#pattern" + id).addClass((i % 2 == 0) ? "bgcolor1" : "bgcolor2");

            // Note: show() method causes page to scroll to top.
            $("#pattern" + id).css("display", "table-row");
        }

        if (playQueue.sendM3U) {
            parent.frames.main.location.href="play.m3u?";
        }

        if (jukeboxPlayer) {
            $("#volume").slider("option", "value", Math.floor(playQueue.jukeboxGain * 100));
            $("#muteOn").toggle(!playQueue.jukeboxMute);
            $("#muteOff").toggle(playQueue.jukeboxMute);
        }

        if (localPlayer) {
            startPlayer = true;
            triggerLocalPlayer(playQueue.startPlayerAt, playQueue.startPlayerAtPosition);
        } else {
            $("#startButton").toggle(!playQueue.stopEnabled);
            $("#stopButton").toggle(playQueue.stopEnabled);
            toggleSpinner(playQueue.stopEnabled);
            if (playQueue.startPlayerAt != -1) {
                currentStreamUrl = songs[playQueue.startPlayerAt].streamUrl;
                updateCoverArt(songs[playQueue.startPlayerAt]);
            }
            updateCurrentImage();
        }
    }

    function triggerLocalPlayer(index, positionMillis) {

        // Load first song (but don't play) if this is the initial case.
        if (index == -1 &&
            (localPlayer.networkState == localPlayer.NETWORK_EMPTY || localPlayer.networkState == localPlayer.NETWORK_NO_SOURCE) &&
            localPlayer.readyState == localPlayer.HAVE_NOTHING) {
            skip(0, 0, false);
        } else {
            skip(index, 0, true);
        }

        if (positionMillis != 0) {
            localPlayer.currentTime = positionMillis / 1000;
        }
		
        updateCurrentImage();
        if (songs.length == 0) {
            localPlayer.pause();
            localPlayer.src = "";
            updateCoverArt(null);
            updateProgressBar();
        }

    }
     
    function updateCoverArt(song) {
        var showAlbum = function () {
            parent.frames.main.location.href = "main.view?id=" + song.id
        };
        $("#coverArt").attr("src", song ? "coverArt.view?id=" + song.id + "&auth=" + song.hash + "&size=160" : "<c:url value="/icons/default/default_cover.png"/>");

        var loved = song != null && song.loved;
        $("#loved_off").toggle(!loved);
        $("#loved_on").toggle(loved);
        
        var starred = song != null && song.starred;
        $("#starred_off").toggle(!starred);
        $("#starred_on").toggle(starred);

        $("#songName").text(song && song.title ? song.title : "");
        $("#artistName").text(song && song.artist ? song.artist : "");
        $("#songName").off("click");
        $("#artistName").off("click");
        $("#coverArt").off("click");
        if (song) {
            $("#songName").click(showAlbum);
            $("#artistName").click(showAlbum);
            $("#coverArt").click(showAlbum);
        }
    }     
        
    function skip(index, position, play) {
        if (index < 0 || index >= songs.length) {
            $("#current").html("");        
            $("#current").hide();
            $("#currenticon").hide();
            return;
        }

        var position = 0;
        var song = songs[index];
        currentStreamUrl = song.streamUrl;
        currentStreamIndex = song.streamUrl + "&index=" + index;

        $("#current").html( index + 1);        
        $("#current").show();
        $("#currenticon").show();
        
        updateCurrentImage();

        if (castPlayer.castSession) {
            castPlayer.loadCastMedia(song, position);
        } else if (localPlayer) {
            localPlayer.src = song.streamUrl;
            if (play) {
            localPlayer.play();
            console.log(song.streamUrl);
            
            if (song.bookmarkPosition > 0) {
                $("#book1").attr('id', 'book3');
                localPlayer.currentTime = song.bookmarkPosition / 1000;
				var bookmarkPosition = formatDuration(song.bookmarkPosition / 1000);
				$().toastmessage("showSuccessToast", "<fmt:message key="playlist.gotobookmark"/> " + bookmarkPosition);
                } else {
                $("#book3").attr('id', 'book1');
                }
	    }
        } else {
            playQueueService.skip(index, playQueueCallback);
        }

        updateWindowTitle(song);
        updateCoverArt(song);

        if (play && ${model.notify}) {
            showNotification(song);
        }
    }
        
    function updateWindowTitle(song) {
        var title = "";
        if (song.title) {
            title += song.title;
        }
        if (song.title && song.artist) {
            title += " - ";
        }
        if (song.artist) {
            title += song.artist;
        }
        top.document.title = title  + " - Madsonic";
    }
    

    function showNotification(song) {
        if (!("Notification" in window)) {
            return;
        }
        if (Notification.permission === "granted") {
            createNotification(song);
        }
        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                Notification.permission = permission;
                if (permission === "granted") {
                    createNotification(song);
                }
            });
        }
    }

    function createNotification(song) {
        var body = "";
        if (song.artist) {
            body += song.artist;
        }
        if (song.artist && song.album) {
            body += " - ";
        }
        if (song.album) {
            body += song.album;
        }

        var n = new Notification(song.title, {
            tag: "madsonic",
            body: body,
            icon: "coverArt.view?id=" + song.id + "&auth=" + song.hash + "&size=110"
        });
        n.onshow = function() {
            setTimeout(function() {n.close()}, 5000);
        }
    }

    function updateCurrentImage() {
        for (var i = 0; i < songs.length; i++) {
            var song  = songs[i];
            var id = i + 1;
            var image = $("#currentImage" + id);

            if (image) {
                if (song.streamUrl + "&index=" + i == currentStreamIndex ) {
                    image.show();
                } else {
                    image.hide();
                }
            }
        }
    }

    function getCurrentSongIndex() {
        for (var i = 0; i < songs.length; i++) {
             if (songs[i].streamUrl + "&index=" + i  == currentStreamIndex ) {
                return i;
            }
        }
        return -1;
    }

    function truncate(s) {
        if (s == null) {
            return s;
        }
        var cutoff = ${model.visibility.captionCutoff};

        if (s.length > cutoff) {
            return s.substring(0, cutoff) + "...";
        }
        return s;
    }

    function actionSelected(id) {
        var selectedIndexes = getSelectedIndexes();
        if (id == "top") {
            return;
        } else if (id == "savePlayQueue") {
            onSavePlayQueue();
        } else if (id == "loadPlayQueue") {
            onLoadPlayQueue();
        } else if (id == "savePlaylist") {
            onSavePlaylist();
        } else if (id == "downloadPlaylist") {
            location.href = "download.view?player=${model.player.id}";
        } else if (id == "sharePlaylist") {
            parent.frames.main.location.href = "createShare.view?player=${model.player.id}&" + getSelectedIndexes();
        } else if (id == "sortByRank") {
            onSortByRank();
        } else if (id == "sortByTrack") {
            onSortByTrack();
        } else if (id == "sortByArtist") {
            onSortByArtist();
        } else if (id == "sortByComposer") {
            onSortByComposer();
        } else if (id == "sortByAlbum") {
            onSortByAlbum();
        } else if (id == "sortByGenre") {
            onSortByGenre();
        } else if (id == "sortByMood") {
            onSortByMood();
        } else if (id == "sortByBpm") {
            onSortByBpm();
        } else if (id == "selectAll") {
            selectAll(true);
        } else if (id == "selectNone") {
            selectAll(false);
        } else if (id == "removeSelected") {
            onRemoveSelected();
        } else if (id == "download" && selectedIndexes != "") {
            location.href = "download.view?player=${model.player.id}&" + selectedIndexes;
        } else if (id == "appendPlaylist" && selectedIndexes != "") {
            onAppendPlaylist();
        }
        $("#moreActions").prop("selectedIndex", 0);
    }

    function getSelectedIndexes() {
        var result = "";
        for (var i = 0; i < songs.length; i++) {
            if ($("#songIndex" + (i + 1)).is(":checked")) {
                result += "i=" + i + "&";
            }
        }
        return result;
    }

    function selectAll(b) {
        for (var i = 0; i < songs.length; i++) {
            if (b) {
                $("#songIndex" + (i + 1)).attr("checked", "checked");
            } else {
                $("#songIndex" + (i + 1)).removeAttr("checked");
            }
        }
    }

    function toast(text) {
        $().toastmessage("showToast", {
            text     : text,
            sticky   : false,
            position : "middle-center",
            type     : "success"
        });
    }

    </script>
</head>

<body class="bgcolor2 playlistframe" onload="init()">

<span id="dummy" class="bgcolor1" style="display:none"></span>

<div class="bgcolor2 playlistframe" style="position:fixed; bottom:0; width:100%; z-index:2">

    <div style="display:flex; margin-top:8px; margin-bottom:8px">
    
        <img id="coverArt" class="dropshadow">
        
        <div class="ellipsis" style="flex-grow:1">
        
            <div id="progress"></div>

            <div class="ellipsis" style="display:flex; align-items:center; margin-left:10px">
            
                <div class="ellipsis" style="flex:5">
                    <div id="songName" class="ellipsis"></div>
                    <div id="artistName" class="ellipsis"></div>
                </div>

                <a id="book1" onclick="onBookmark()" title="<fmt:message key="playlist.setbookmark"/>"></a>

                <a id="loved_on"  style="display:none" onclick="onLoveCurrentSong(false)" title="<fmt:message key="playlist.removeloved"/>"></a>
                <a id="loved_off" style="display:block" onclick="onLoveCurrentSong(true)" title="<fmt:message key="playlist.setloved"/>"></a>
                
                <a id="starred_on"  style="display:none" onclick="onStarCurrentSong(false)" title="<fmt:message key="playlist.removestarred"/>"></a>
                <a id="starred_off" style="display:block" onclick="onStarCurrentSong(true)" title="<fmt:message key="playlist.setstarred"/>"></a>

                <span id="fastpreviousButton" class="fa-stack fa-lg" onclick="onRewind()" title="<fmt:message key="playlist.rewind"/>">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-fast-backward fa-stack-1x fa-inverse"></i>
                </span> 

                <span id="previousButton" class="fa-stack fa-lg" onclick="onPrevious()" title="<fmt:message key="playlist.previous"/>">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-step-backward fa-stack-1x fa-inverse"></i>
                </span> 

                <span id="startButton" class="fa-stack fa-lg" onclick="onStart()" title="<fmt:message key="playlist.start"/>">
                    <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                    <i class="fa fa-play-circle fa-stack-2x"></i>
                </span>
		
                <span id="stopButton" class="fa-stack fa-lg" onclick="onStop()" style="display:none" title="<fmt:message key="playlist.stop"/>">
                    <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                    <i class="fa fa-pause-circle fa-stack-2x"></i>
                </span>
		
                <span id="bufferButton" class="fa-stack fa-lg" style="display:none" title="<fmt:message key="playlist.buffer"/>">
                    <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                    <i class="fa fa-refresh fa-stack-1x fa-spin"></i>
                </span>
		
                <span id="nextButton" class="fa-stack fa-lg" onclick="onNext(repeatEnabled)" title="<fmt:message key="playlist.next"/>">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-step-forward fa-stack-1x fa-inverse"></i>
                </span>             

                <span id="fastnextButton" class="fa-stack fa-lg" onclick="onForward()" title="<fmt:message key="playlist.forward"/>">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-fast-forward fa-stack-1x fa-inverse"></i>
                </span>             
           
                <c:if test="${model.player.web or model.player.jukebox or model.player.external}">
                <a id="repeat_off" onclick="onToggleRepeat()" title="<fmt:message key="playlist.repeat"/>" class="ToggleButton"><span id="onToggleRepeat" class="handcursor"></span></a>
                </c:if>            
                <a id="shuffle" onclick="onShuffle()" title="<fmt:message key="playlist.shuffle"/>" title="Shuffle"><fmt:message key="playlist.shuffle"/></a>
                <a id="undo" onclick="onUndo()" title="<fmt:message key="playlist.undo"/>"><fmt:message key="playlist.undo"/></a>
                <a id="clear1" onclick="onClear()" title="<fmt:message key="playlist.clear"/>"><fmt:message key="playlist.clear"/></a>
                <a id="followMe_off" onclick="onToggleFollowMe()" title="<fmt:message key="playlist.followme"/>" class="FollowMeButton"><span id="onToggleFollowMe" class="handcursor"></span></a>
                <a id="lock2" onclick="onToggleLockMode()" title="<fmt:message key="playlist.lockmode"/>" class="LockButton"><span id="onToggleLock" class="handcursor"></span></a>
                <c:if test="${model.nodeEnabled}">
                <a id="node" onclick="onNode()" title="<fmt:message key="playlist.node"/>" class="node"><span id="onNode" class="handcursor"></span></a>
                </c:if>
                <c:if test="${model.user.settingsRole}">
                <a id="settings1" href="playerSettings.view?id=${model.player.id}" target="main" title="<fmt:message key="playlist.settings"/>"></a>
                </c:if>
                <a id="save" onclick="onSavePlaylist()" title="<fmt:message key="playlist.save"/>"></a><div style="margin-right:10px"> </div>
                <a id="saveQueue" onclick="onSavePlayQueue()" title="<fmt:message key="playlist.saveplayqueue"/>"" class="savePlayQueue"><span id="onSavePlayQueue" class="handcursor"></span></a>
            	<a id="loadQueue" onclick="onLoadPlayQueue()" title="<fmt:message key="playlist.loadplayqueue"/>"" class="loadPlayQueue"><span id="onLoadPlayQueue" class="handcursor"></span></a>
				
                <i id="castOn" class="material-icons" onclick="castPlayer.launchCastApp()">cast</i>
                <i id="castOff" class="material-icons" onclick="castPlayer.stopCastApp()">cast_connected</i>


            <!--
            <c:if test="${model.user.settingsRole and fn:length(model.players) gt 1}">
            <select name="player" onchange="location='playQueue.view?player=' + options[selectedIndex].value;">
                <c:forEach items="${model.players}" var="player">
                    <option ${player.id eq model.player.id ? "selected" : ""} title='${player.ipAddress}' value="${player.id}">${player.shortDescription}</option>
                </c:forEach>
            </select>
            </c:if>            
            -->
            
        <c:if test="${model.player.web or model.player.jukebox or model.player.external}">
                <div style="flex:1">
                <select id="moreActions" onchange="actionSelected(this.options[selectedIndex].id)">
                        <option id="top" selected="selected"><fmt:message key="playlist.more"/></option>
                        <optgroup label="<fmt:message key="playlist.more.playlist"/>">
                            <option id="savePlayQueue"><fmt:message key="playlist.saveplayqueue"/></option>
                            <option id="loadPlayQueue"><fmt:message key="playlist.loadplayqueue"/></option>
                            <option id="savePlaylist"><fmt:message key="playlist.save"/></option>
                            <c:if test="${model.user.downloadRole}">
                            <option id="downloadPlaylist"><fmt:message key="common.download"/></option>
                            </c:if>
                            <c:if test="${model.user.shareRole}">
                            <option id="sharePlaylist"><fmt:message key="main.more.share"/></option>
                            </c:if>
                            <option id="sortByRank"><fmt:message key="playlist.more.sortbyrank"/></option>
                            <option id="sortByTrack"><fmt:message key="playlist.more.sortbytrack"/></option>
                            <option id="sortByAlbum"><fmt:message key="playlist.more.sortbyalbum"/></option>
                            <option id="sortByArtist"><fmt:message key="playlist.more.sortbyartist"/></option>
                            <option id="sortByComposer"><fmt:message key="playlist.more.sortbycomposer"/></option>
                            <option id="sortByGenre"><fmt:message key="playlist.more.sortbygenre"/></option>
                            <option id="sortByMood"><fmt:message key="playlist.more.sortbymood"/></option>
                            <option id="sortByBpm"><fmt:message key="playlist.more.sortbybpm"/></option>
                        </optgroup>
                        <optgroup label="<fmt:message key="playlist.more.selection"/>">
                            <option id="selectAll"><fmt:message key="playlist.more.selectall"/></option>
                            <option id="selectNone"><fmt:message key="playlist.more.selectnone"/></option>
                            <option id="removeSelected"><fmt:message key="playlist.remove"/></option>
                            <c:if test="${model.user.downloadRole}">
                                <option id="download"><fmt:message key="common.download"/></option>
                            </c:if>
                            <option id="appendPlaylist"><fmt:message key="playlist.append"/></option>
                        </optgroup>
                </select>
                </div>                
            </c:if>
          
                <div style="flex:1">
                    <div id="spacer" class="detail" style="text-align:right">
                    </div>
                </div>
          
                <div style="flex:1 1 auto;margin-left:5px;min-width:90px;">
                    <div id="progress-and-duration" class="detail" style="text-align:center">
                        <span id="progress-text">0:00</span> /
                        <span id="duration-text">0:00</span>
                    </div>
                </div>

                <div style="flex:1;margin-left:10px;">
                    <div id="current-and-count" class="detail" style="text-align:right">
                        <span id="current"></span> /
                        <span id="count"></span>
                    </div>                
                </div>
                
                <i id="muteOn" class="fa fa-volume-up fa-fw" onclick="onMute(true)"></i>
                <i id="muteOff" class="fa fa-volume-off fa-fw" onclick="onMute(false)" style="display:none"></i>
                <div id="volume"></div>
            </div>
        </div>
    </div>
</div>

<h2 style="float:left"><fmt:message key="playlist.more.playlist"/></h2>
<h2 id="songCountAndDuration" style="float:right;padding-right:0.5em"></h2>
<div style="clear:both"></div>
                <p id="empty"><em><fmt:message key="playlist.empty"/></em></p>
                <table class="music indent" style="cursor:pointer">
                <tbody id="playlistBody">
                    <tr id="pattern" style="display:none;margin:0;padding:0;border:0">
                    
                        <td class="fit">
                            <c:if test="${model.buttonVisibility.rankVisible}">
                                <div class="icon-wrapper"><i class="fa custom-icon">
                                    <span class="custom-icon-rank starred" >  
                                    <c:if test="${child.rank < 10}">0</c:if>${child.rank}<span id="rank">&nbsp;</span></i>       
                                    </span>
                                </div>
                            </c:if>
                        </td>   
                        
                        <td class="fit">
                        <div class="icon-wrapper">
                        <i class="fa fa-bookmark clickable custom-icon" id="bookmarkSong" onclick="onToogleBookmark(this.id.substring(12) - 1)" title="<fmt:message key="common.bookmark"/>"><span class="fix-editor">&nbsp;</span></i></div>
                        </td>

                        <c:if test="${model.buttonVisibility.lovedVisible}">
                        <td class="fit">
                            <i id="loveSong" class="fa custom-icon clickable" onclick="onLove(this.id.substring(8) - 1)"></i>
                        </td>
                        </c:if>
                        
                        <c:if test="${model.buttonVisibility.starredVisible}">
                        <td class="fit">
                            <i id="starSong" class="fa custom-icon clickable" onclick="onStar(this.id.substring(8) - 1)"></i>
                        </td>
                        </c:if>

                        <td class="fit" style="padding-left:10px;"><input type="checkbox" class="checkbox" id="songIndex"></td>

                        <c:if test="${model.visibility.discNumberVisible}">
                           <td class="fit"><span class="detail" id="discNumber">0</span></td>
                        </c:if>
                        
                        <c:if test="${model.visibility.trackNumberVisible}">
                            <td class="fit rightalign"><span class="detail" id="trackNumber">1</span></td>
                        </c:if>

                            <td class="truncate">
                            <img id="currentImage" src="<spring:theme code="currentImage"/>" alt="" style="display:none">
                            <c:choose>
                                <c:when test="${model.player.externalWithPlaylist}">
                                    <span class="detail" id="title">Title</span>
                                </c:when>
                                <c:otherwise>
                                    <a id="titleUrl" class="detail" href="javascript:void(0)">Title</a>
                                </c:otherwise>
                            </c:choose>
                        </td>

                        <c:if test="${model.visibility.albumVisible}">
                        <td class="truncate"><a id="albumUrl" target="main"><span id="album" class="detail">Album</span></a></td>
                        </c:if>
                        <c:if test="${model.visibility.artistVisible}">
                            <td class="truncate"><span id="artist" class="detail">Artist</span></td>
                        </c:if>
                        <c:if test="${model.visibility.composerVisible}">
                            <td class="truncate"><span id="composer" class="detail">Composer</span></td>
                        </c:if>
                        <c:if test="${model.visibility.moodVisible}">
                        <td class="truncate"><span id="mood" class="detail">Mood</span></td>
                        </c:if>
                        <c:if test="${model.visibility.genreVisible}">
                        <td class="truncate"><span id="genre" class="detail">Genre</span></td>
                        </c:if>
                        <c:if test="${model.visibility.yearVisible}">
                        <td class="fit rightalign"><span id="year" class="detail">Year</span></td>
                        </c:if>
                        <c:if test="${model.visibility.bpmVisible}">
                        <td class="fit rightalign"><span id="bpm" class="detail">Bpm</span></td>
                        </c:if>
                        <c:if test="${model.visibility.formatVisible}">
                        <td class="fit rightalign"><span id="format" class="detail">Format</span></td>
                        </c:if>
                        <c:if test="${model.visibility.fileSizeVisible}">
                        <td class="fit rightalign"><span id="fileSize" class="detail">Format</span></td>
                        </c:if>
                        <c:if test="${model.visibility.durationVisible}">
                        <td class="fit rightalign"><span id="duration" class="detail">Duration</span></td>
                        </c:if>
                        <c:if test="${model.visibility.bitRateVisible}">
                        <td class="fit rightalign"><span id="bitRate" class="detail">Bit Rate</span></td>
                        </c:if>
                        <td class="fit"><span class="detail" id="currentIndex"> </span></td>
						
						
		            <c:if test="${not model.player.externalWithPlaylist}">
		                <td class="fit" style="padding-right:10px;">
		                    <i id="removeSong" class="fa fa-remove clickable icon custom-icon warning" onclick="onRemove(this.id.substring(10) - 1)" title="<fmt:message key="playlist.remove"/>"></i>
		                </td>
		            </c:if> 						
						
                    </tr>
                </tbody>
            </table>

<div style="height:94px"></div>

            <div id="dialog-select-playlist" title="<fmt:message key="main.addtoplaylist.title"/>" style="display: none;">
                <p><fmt:message key="main.addtoplaylist.text"/></p>
                <div id="dialog-select-playlist-list"></div>
            </div>
          
<script>

$('.ToggleButton').click(function(){
switch(this.id) {
    case "repeat_off": $(this).attr('id', 'repeat_on'); break;
    case "repeat_on": $(this).attr('id', 'repeat_off'); break;
    }
});

$('.FollowMeButton').click(function(){
    switch(this.id) {
    case "followMe_off": $(this).attr('id', 'followMe_on'); break;
    case "followMe_on": $(this).attr('id', 'followMe_off'); break;
    }
});

$('.LockButton').click(function(){
    switch(this.id) {
    case "lock0": $(this).attr('id', 'lock1'); break;
    case "lock1": $(this).attr('id', 'lock2'); break;
    case "lock2": $(this).attr('id', 'lock0'); break;
    }
});
</script>
</body></html>