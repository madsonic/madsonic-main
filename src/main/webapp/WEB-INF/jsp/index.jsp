<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<html>
<head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <link rel="alternate" type="application/rss+xml" title="Madsonic Podcast" href="podcast.view?suffix=.rss">
    
    <script type="application/javascript">
    
        function toggleLeftBar(show) {
            var width = show ? 270 : 50;
            $("#left").stop();
            $("#left").animate({"width": width});
        }
        
        function toggleLeftPanel(show) {
            var width = show ? ${model.leftframeSize} : 0;
            $("#leftPanel").stop();
            $("#leftPanel").animate({"width": width});
        }
        
        function setPlayQueueHeight(height) {
            $("#playQueue").stop();
            $("#playQueue").animate({"height": height}, {step: updateSize});
        }
        
        function setRightPanelWidth(width) {
            $("#right").stop();
            $("#right").animate({"width": width}, {step: updateSize});
        }

        function setSidePanelWidth(width) {
            $("#sidePanel").stop();
            $("#sidePanel").animate({"width": width}, {step: updateSize});
        }
        
        // Work-around for quirky iframe layout issue.
        function updateSize(playQueueHeight) {
            var w = $(window).width() - $("#left").width() - $("#leftPanel").width() - $("#right").width() - $("#sidePanel").width() - 1;
            var h = $(window).height() - playQueueHeight - 1;
            $("#main").width(w);
        }

        function init() {
            updateSize($("#playQueue").height());
            $(window).resize(function () {
                updateSize($("#playQueue").height());
            });
        }

    </script>
    
    <style type="text/css">
        #page {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            flex-direction: column; 
            width: 100%;
            height: 100%;
        }
        
        #content {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display:flex; 
            width: 100%; 
            height: 100%;
            -webkit-box-flex: 1;
            -moz-box-flex: 1;
            -ms-flex: 1;
            -webkit-flex: 1;
            flex-grow: 1;
        }
        
        #left {
            width: ${model.showLeftBar ? 270 : 50}px;
            height: 100%;
        }
        
        #leftPanel {
            width: 0px;
            height: 100%;
        }
        
        #main {
            -webkit-box-flex: 1;
            -moz-box-flex: 1;
            -ms-flex: 1;
            -webkit-flex: 1;
            display: flex;
            flex-grow: 1;
            min-height: 100%;
        }
        
        #right { 
            width: ${model.showRightPanel ? 300 : 0}px;
            height: 100%;
        }
        
        #sidePanel { 
            width:  ${model.showSidePanel ? 300 : 0}px;
            height: 100%;
        }
        
        #playQueue { 
            width:100%;
            height: 95px;
            z-index: 100;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }
    </style>
</head>
<body class="bgcolor2 nospace" onload="init()">
<div class="page bgcolor2 nospace" id="page" name="page">
    <div id="content" name="content">
        <iframe src="left.view" id="left" name="left" class="bgcolor2 nospace"></iframe>
        <iframe src="leftPanel.view" id="leftPanel" name="leftPanel" class="bgcolor2 nospace"></iframe>
        <iframe src="home.view?listType=${model.listType}" id="main" name="main" allowfullscreen class="bgcolor1 nospace"></iframe>
        <iframe src="right.view" id="right" name="right" class="bgcolor2 nospace"></iframe>
        <iframe src="nowListening.view" id="sidePanel" name="sidePanel" class="bgcolor2 nospace"></iframe>
    </div>
    <iframe src="playQueue.view" id="playQueue" name="playQueue" class="bgcolor2 nospace"></iframe>
</div>
</body>
</html>