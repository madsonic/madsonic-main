<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose> 


    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/lovedTrackService.js"/>"></script>	    
	<script type="text/javascript" src="<c:url value="/dwr/interface/playlistService.js"/>"></script>
	
    <script type="text/javascript" language="javascript">

	    function init() {

        $("#dialog-select-playlist").dialog({resizable: true, height: 350, modal: false, autoOpen: false,
            buttons: {
                "<fmt:message key="common.cancel"/>": function() {
                    $(this).dialog("close");
                }
            }});
    }
	
    function toggleStar(mediaFileId, element) {
        starService.star(mediaFileId, !$(element).hasClass("fa-star"));
        $(element).toggleClass("fa-star fa-star-o starred");
    }    
   
    function toggleLoved(mediaFileId, element) {
        lovedTrackService.love(mediaFileId, !$(element).hasClass("fa-heart"));
        $(element).toggleClass("fa-heart fa-heart-o loved");
    } 
	</script>
</head>

<body class="mainframe bgcolor1" onload="init()"> 

  <div id="content_main" class="content_main"> 
  
	<div id="container" class="container">

	<h1>
		<img src="<spring:theme code="discoverImage"/>" width="32" alt="">
		<fmt:message key="welcome.title"/> 
	</h1>

	<h2>
		<!-- temp. remove artists -->
		<c:forTokens items="artists albums songs topplayed toptracks lastplayed" delims=" " var="cat" varStatus="loopStatus">
			<c:if test="${loopStatus.count > 1}">&nbsp;<img src="<spring:theme code="sepImage"/>" alt="">&nbsp;</c:if>
			<madsonic:url var="url" value="discover.view">
				<madsonic:param name="listType" value="${cat}"/>
			</madsonic:url>
			<c:choose>
				<c:when test="${model.listType eq cat}">
					<span class="headerSelected"><fmt:message key="welcome.${cat}.title"/></span>
				</c:when>
				<c:otherwise>
					<a href="${url}"><fmt:message key="welcome.${cat}.title"/></a>
				</c:otherwise>
			</c:choose>
		</c:forTokens>
	</h2>

	<c:if test="${empty model.artists and empty model.albums and empty model.songs and empty model.topPlayedSongs and empty model.lastPlayedSongs and empty model.topTrackSongs}">
		<p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p>
	</c:if>
	
	<p></p>

	<c:choose>
		<c:when test="${model.listType eq 'artists'}">
		<!-- artists --> 
		<c:if test="${not empty model.artists}">
		<!--<h2><fmt:message key="search.hits.artists"/></h2> -->
			<h1 style="border-top: 1px dotted #555;"></h1>
			<div class="coverbox">
			<table style="border-collapse:collapse;width: 95%;">
				<c:forEach items="${model.artists}" var="artist" varStatus="loopStatus">
					<madsonic:url value="/main.view" var="mainUrl">
						<madsonic:param name="path" value="${artist.path}"/>
					</madsonic:url>
					<tr>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:3px;padding-right:0.5em;">
						<c:import url="coverArtThumb.jsp">
							<c:param name="albumId" value="${artist.id}"/>
							<c:param name="auth" value="${artist.hash}"/>
							<c:param name="artistName" value="${artist.name}"/>
							<c:param name="coverArtSize" value="90"/>
							<c:param name="coverArtHQ" value="true"/>
							<c:param name="showLink" value="true"/>
							<c:param name="showZoom" value="false"/>
							<c:param name="showChange" value="false"/>
							<c:param name="showArtist" value="false"/>
							<c:param name="typArtist" value="true"/>
							<c:param name="scale" value="1"/>
							<c:param name="appearAfter" value="5"/>
						</c:import>
						</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1.5em;padding-right:1.0em;">
						<c:import url="playAddDownload.jsp">
							<c:param name="id" value="${artist.id}"/>
							<c:param name="playEnabled" value="false"/>
							<c:param name="addEnabled" value="false"/>
							<c:param name="downloadEnabled" value="false"/>
							<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
							<c:param name="starred" value="${not empty artist.starredDate}"/>
							<c:param name="asTable" value="false"/>
						</c:import>
					</td>
						<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left: 15px;padding-right:1.5em;">
							<h1 style="padding-top: 0px;"><a href="${mainUrl}">${artist.name}</a></h1>
						</td>
						<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left: 15px;padding-right:1.5em;">
							<str:truncateNicely upper="450">${artist.comment}</str:truncateNicely>
						</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${model.listType eq 'albums'}">
		<!-- albums -->
		<c:if test="${not empty model.albums}">
		<!--<h2><fmt:message key="search.hits.albums"/></h2>-->
		
			<h1 style="border-top: 1px dotted #555;"></h1>
			<div class="coverbox">
			<table class="music" style="border-collapse:collapse">
				<c:forEach items="${model.albums}" var="album" varStatus="loopStatus">

					<madsonic:url value="/main.view" var="mainUrl">
						<madsonic:param name="path" value="${album.path}"/>
					</madsonic:url>

					<madsonic:url value="/main.view" var="artistUrl">
						<madsonic:param name="path" value="${album.parentPath}"/>
					</madsonic:url>				
					
					<tr style="vertical-align: middle;">
					
						<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:3px;padding-right:0.5em;vertical-align: middle;">
						<c:import url="coverArtThumb.jsp">
							<c:param name="albumId" value="${album.id}"/>
							<c:param name="auth" value="${album.hash}"/>                        
							<c:param name="artistName" value="${album.name}"/>
							<c:param name="coverArtSize" value="90"/>
							<c:param name="showLink" value="true"/>
							<c:param name="showZoom" value="false"/>
							<c:param name="showChange" value="false"/>
							<c:param name="showArtist" value="false"/>
							<c:param name="typArtist" value="true"/>
							<c:param name="appearAfter" value="5"/>
							<c:param name="scale" value="0.5"/>						
						</c:import>
						</td>
						
						<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0.5em;">
							<c:import url="playAddDownload.jsp">
								<c:param name="id" value="${album.id}"/>
								<c:param name="playEnabled" value="true"/>
								<c:param name="addEnabled" value="true"/>
								<c:param name="downloadEnabled" value="false"/>
								<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
								<c:param name="starred" value="${not empty album.starredDate}"/>
								<c:param name="asTable" value="false"/>
							</c:import>
						</td>
						
						<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1.25em;padding-right:3.25em;padding-bottom:0.5em;">
							<h2><a href="${mainUrl}">${album.albumSetName}</a></h2>
						</td>

						<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:0.25em;padding-right:1.25em;padding-bottom:0.5em;">
							<h2><a href="${artistUrl}">${album.artist}</a></h2>
						</td>
					</tr>

				</c:forEach>
			</table>
		</c:if>
		</div>
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${model.listType eq 'songs'}">
		<!-- songs --> 
		<c:if test="${not empty model.songs}">
			<!--<h2><fmt:message key="search.hits.songs"/></h2>-->
			<h1 style="border-top: 1px dotted #555;"></h1>
			<div class="coverbox">
			<table class="music" style="border-collapse:collapse;white-space:nowrap;">
				<c:forEach items="${model.songs}" var="song" varStatus="loopStatus">

					<madsonic:url value="/main.view" var="mainUrl">
						<madsonic:param name="path" value="${song.parentPath}"/>
					</madsonic:url>

					<madsonic:url value="/main.view" var="artistUrl">
						<c:if test="${not empty song.artistPath}">
							<madsonic:param name="path" value="${song.artistPath}"/>
						</c:if>
						<c:if test="${empty song.artistPath}">
							<madsonic:param name="path" value="${song.parentPath}"/>
						</c:if>
					</madsonic:url>	
					
					<tr>
						<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:3px;">
						<c:import url="coverArtThumb.jsp">
							<c:param name="albumId" value="${song.id}"/>
							<c:param name="auth" value="${song.hash}"/>                        
							<c:param name="artistName" value="${song.name}"/>
							<c:param name="coverArtSize" value="50"/>
							<c:param name="showLink" value="true"/>
							<c:param name="showZoom" value="false"/>
							<c:param name="showChange" value="false"/>
							<c:param name="showArtist" value="false"/>
							<c:param name="typArtist" value="true"/>
							<c:param name="appearAfter" value="5"/>
							<c:param name="scale" value="0.5"/>						
						</c:import>
						</td>
						
						<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0.5em;">
						<c:import url="playAddDownload.jsp">
							<c:param name="id" value="${song.id}"/>
							<c:param name="playEnabled" value="true"/>
							<c:param name="addEnabled" value="true"/>
							<c:param name="downloadEnabled" value="false"/>
							<c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
							<c:param name="loved" value="${not empty song.lovedDate}"/>
							<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
							<c:param name="starred" value="${not empty song.starredDate}"/>
							<c:param name="video" value="${song.video and model.player.web}"/>
							<c:param name="asTable" value="false"/>
						</c:import>
						</td>
						
						<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-left:1.25em;padding-right:1.55em;">
						<str:truncateNicely upper="40">${song.title}</str:truncateNicely>
						</td>

						<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-right:1.25em">
							<a href="${mainUrl}"><str:truncateNicely upper="40">${song.albumName}</str:truncateNicely></a>
						</td>

						<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-right:1.25em;">
							<a href="${artistUrl}">${song.artist}</a>
						</td>
						
						<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"}>
							<span id="songId${loopStatus.count - 1}" style="display: none">${song.id}</span></td>
						</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		</div>
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${model.listType eq 'topplayed'}">
			<c:if test="${not empty model.topPlayedSongs}">
			<h1 style="border-top: 1px dotted #555;"></h1>
				<div class="coverbox">
				<table class="music" style="border-collapse:collapse;white-space:nowrap;">

						<c:forEach items="${model.topPlayedSongs}" var="song" varStatus="loopStatus">

							<madsonic:url value="/main.view" var="mainUrl">
								<madsonic:param name="path" value="${song.parentPath}"/>
							</madsonic:url>
							
							<madsonic:url value="/main.view" var="artistUrl">
								<c:if test="${not empty song.artistPath}">
									<madsonic:param name="path" value="${song.artistPath}"/>
								</c:if>
								<c:if test="${empty song.artistPath}">
									<madsonic:param name="path" value="${song.parentPath}"/>
								</c:if>
							</madsonic:url>	

					<tr>
								<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:3px;">								
									<c:import url="coverArtThumb.jsp">
										<c:param name="albumId" value="${song.id}"/>
										<c:param name="auth" value="${song.hash}"/>                                      
										<c:param name="artistName" value="${song.name}"/>
										<c:param name="coverArtSize" value="50"/>
										<c:param name="scale" value="0.5"/>									
										<c:param name="showLink" value="true"/>
										<c:param name="showZoom" value="false"/>
										<c:param name="showChange" value="false"/>
										<c:param name="showArtist" value="false"/>
										<c:param name="typArtist" value="true"/>
										<c:param name="appearAfter" value="5"/>
									</c:import>
								</td>

								<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0.5em;">
								<c:import url="playAddDownload.jsp">
									<c:param name="id" value="${song.id}"/>
									<c:param name="playEnabled" value="true"/>
									<c:param name="addEnabled" value="true"/>
									<c:param name="downloadEnabled" value="false"/>
									<c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
									<c:param name="loved" value="${not empty song.lovedDate}"/>
									<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
									<c:param name="starred" value="${not empty song.starredDate}"/>
									<c:param name="starred" value="${not empty song.starredDate}"/>
									<c:param name="video" value="${song.video and model.player.web}"/>
									<c:param name="asTable" value="false"/>
								</c:import>
								</td>								
			
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class='fit'"} style="padding-left:1.25em;padding-right:1.25em">
									<str:truncateNicely upper="40">${song.title}</str:truncateNicely>
								</td>
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class='fit'"} style="padding-right:1.25em">
									<a href="${mainUrl}"><str:truncateNicely upper="40">${song.albumName}</str:truncateNicely></a>
								</td>
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class='fit'"} style="padding-right:1.50em">
									<a href="${artistUrl}"><str:truncateNicely upper="40">${song.artist}</str:truncateNicely></a>
								</td>
								<c:choose>
									<c:when test="${model.listType eq 'topplayed'}">
										<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class='fit'"} style="padding-right:1.50em">
											<span class="detailcolor">(${song.playCount}x)</span>
										</td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${model.listType eq 'overall'}">
										<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class='fit'"} style="padding-right:1.50em">
											<span class="detailcolor">(${song.playCount}x)</span>
										</td>
									</c:when>
								</c:choose>				
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:0.75em">
									<span class="detail">${fn:substring(song.lastPlayed, 0, 16)}</span>
								</td>
								</tr>
								
						</c:forEach>
					</table>
				</c:if>
				</div>
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${model.listType eq 'lastplayed'}">
			<c:if test="${not empty model.lastPlayedSongs}">
			<h1 style="border-top: 1px dotted #555;"></h1>
			<div class="coverbox">
			<table class="music"  style="border-collapse:collapse;white-space:nowrap;">

						<c:forEach items="${model.lastPlayedSongs}" var="song" varStatus="loopStatus">

							<madsonic:url value="/main.view" var="mainUrl">
								<madsonic:param name="path" value="${song.parentPath}"/>
							</madsonic:url>
							
							<madsonic:url value="/main.view" var="artistUrl">
								<c:if test="${not empty song.artistPath}">
									<madsonic:param name="path" value="${song.artistPath}"/>
								</c:if>
								<c:if test="${empty song.artistPath}">
									<madsonic:param name="path" value="${song.parentPath}"/>
								</c:if>
							</madsonic:url>	
							<tr>
								<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:3px;">								
									<c:import url="coverArtThumb.jsp">
										<c:param name="albumId" value="${song.id}"/>
										<c:param name="auth" value="${song.hash}"/>                                      
										<c:param name="artistName" value="${song.name}"/>
										<c:param name="coverArtSize" value="50"/>
										<c:param name="scale" value="0.5"/>
										<c:param name="showLink" value="true"/>
										<c:param name="showZoom" value="false"/>
										<c:param name="showChange" value="false"/>
										<c:param name="showArtist" value="false"/>
										<c:param name="typArtist" value="true"/>
										<c:param name="appearAfter" value="5"/>
									</c:import>
								</td>

								<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0.5em;">
								<c:import url="playAddDownload.jsp">
									<c:param name="id" value="${song.id}"/>
									<c:param name="playEnabled" value="true"/>
									<c:param name="addEnabled" value="true"/>
									<c:param name="downloadEnabled" value="false"/>
									<c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
									<c:param name="loved" value="${not empty song.lovedDate}"/>
									<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
									<c:param name="starred" value="${not empty song.starredDate}"/>
									<c:param name="video" value="${song.video and model.player.web}"/>
									<c:param name="asTable" value="false"/>
								</c:import>
								</td>								
			
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1.0em;padding-right:1.25em">
									<str:truncateNicely upper="40">${song.title}</str:truncateNicely>
								</td>

								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.0em">
									<a href="${mainUrl}"><span class="detail"><str:truncateNicely upper="40">${song.albumName}</str:truncateNicely></span></a>
								</td>

								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
									<a href="${artistUrl}"><span class="detail"><str:truncateNicely upper="40">${song.artist}</str:truncateNicely></span></a>
								</td>
								<c:choose>
									<c:when test="${model.listType eq 'topplayed'}">
										<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
											<span class="detailcolor">(${song.playCount}x)</span>
										</td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${model.listType eq 'overall'}">
										<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
											<span class="detailcolor">(${song.playCount}x)</span>
										</td>
									</c:when>
								</c:choose>				
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:0.75em">
									<span class="detail">${fn:substring(song.lastPlayed, 0, 16)}</span>
								</td>
								</tr>
								
						</c:forEach>
					</table>
				</c:if>
				</div>
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${model.listType eq 'toptracks'}">
			<c:if test="${not empty model.topTrackSongs}">
			<h1 style="border-top: 1px dotted #555;"></h1>
			<div class="coverbox">
				<table class="music" style="border-collapse:collapse;white-space:nowrap;">

						<c:forEach items="${model.topTrackSongs}" var="song" varStatus="loopStatus">

							<madsonic:url value="/main.view" var="mainUrl">
								<madsonic:param name="path" value="${song.parentPath}"/>
							</madsonic:url>
							
							<madsonic:url value="/main.view" var="artistUrl">
								<c:if test="${not empty song.artistPath}">
									<madsonic:param name="path" value="${song.artistPath}"/>
								</c:if>
								<c:if test="${empty song.artistPath}">
									<madsonic:param name="path" value="${song.parentPath}"/>
								</c:if>
							</madsonic:url>	

							<tr>
								<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:3px;">								
									<c:import url="coverArtThumb.jsp">
										<c:param name="albumId" value="${song.id}"/>
										<c:param name="auth" value="${song.hash}"/>                                      
										<c:param name="artistName" value="${song.name}"/>
										<c:param name="coverArtSize" value="50"/>
										<c:param name="showLink" value="true"/>
										<c:param name="showZoom" value="false"/>
										<c:param name="showChange" value="false"/>
										<c:param name="showArtist" value="false"/>
										<c:param name="typArtist" value="true"/>
										<c:param name="appearAfter" value="5"/>
										<c:param name="scale" value="0.5"/>
									</c:import>
								</td>

								<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0.5em;">
								<c:import url="playAddDownload.jsp">
									<c:param name="id" value="${song.id}"/>
									<c:param name="playEnabled" value="true"/>
									<c:param name="addEnabled" value="true"/>
									<c:param name="downloadEnabled" value="false"/>
									<c:param name="video" value="${song.video and model.player.web}"/>
									<c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
									<c:param name="loved" value="${not empty song.lovedDate}"/>
									<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
									<c:param name="starred" value="${not empty song.starredDate}"/>                                
									<c:param name="asTable" value="false"/>
								</c:import>
								</td>								
								
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1.25em;padding-right:1.25em">
									<str:truncateNicely upper="40">${song.title}</str:truncateNicely>
								</td>
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
									<a href="${mainUrl}"><str:truncateNicely upper="40">${song.albumName}</str:truncateNicely></a>
								</td>
								<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
									<a href="${artistUrl}"><str:truncateNicely upper="40">${song.artist}</str:truncateNicely></a>
								</td>
								<c:choose>
									<c:when test="${model.listType eq 'topplayed'}">
										<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
											<span class="detailcolor">(${song.playCount}x)</span>
										</td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${model.listType eq 'overall'}">
										<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.50em">
											<span class="detailcolor">(${song.playCount}x)</span>
										</td>
									</c:when>
								</c:choose>				
								</tr>
								
						</c:forEach>
					</table>
				</c:if>
				</div>
		</c:when>
	</c:choose>


	<!-- CONTENT -->
	</div>
	
<!-- CONTAINER -->	
</div>


<script type="text/javascript" language="javascript">

		function actionSelected(id) {
		
        if (id == "top") {
            return;
        } else if (id == "savePlaylist") {
            onSavePlaylist();
        } else if (id == "selectAll") {
            selectAll(true);
        } else if (id == "selectNone") {
            selectAll(false);
        } else if (id == "appendPlaylist") {
            onAppendPlaylist();
        } else if (id == "saveasPlaylist") {
            onSaveasPlaylist();
        }        
        $("#moreActions").prop("selectedIndex", 0);
        }

        function getSelectedIndexes() {
        var result = "";
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
				var checkbox = $("#songIndex" + i);
				if (checkbox != null  && checkbox.is(":checked")) {
					result += "i=" + i + "&";
				}
			}
			return result;
		}

		function selectAll(b) {
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
				var checkbox = $("#songIndex" + i);
				if (checkbox != null) {
					if (b) {
						checkbox.attr("checked", "checked");
					} else {
						checkbox.removeAttr("checked");
					}
				}
			}
		}
		
		function onAppendPlaylist() {
			playlistService.getWritablePlaylists(playlistCallback);
		}
		
		function playlistCallback(playlists) {
		
			$("#dialog-select-playlist-list").empty();
			for (var i = 0; i < playlists.length; i++) {
				var playlist = playlists[i];
				$("<p class='dense'><b><a href='#' onclick='appendPlaylist(" + playlist.id + ")'>" + playlist.name + "</a></b></p>").appendTo("#dialog-select-playlist-list");
			}
			$("#dialog-select-playlist").dialog("open");
		}
		
		function appendPlaylist(playlistId) {
			$("#dialog-select-playlist").dialog("close");

			var mediaFileIds = new Array();
			
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
			
				var checkbox = $("#songIndex" + i);
				if (checkbox && checkbox.is(":checked")) {
					mediaFileIds.push($("#songId" + i).html());
				}
			}
			playlistService.appendToPlaylist(playlistId, mediaFileIds, function (){parent.left.updatePlaylists();});
		}
		
		function onSavePlaylist() {
		
            selectAll(true);
			
			var mediaFileIds = new Array();
			
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
			
				var checkbox = $("#songIndex" + i);
				if (checkbox && checkbox.is(":checked")) {
					mediaFileIds.push($("#songId" + i).html());
				}
			}
			playlistService.savePlaylist(mediaFileIds, function (){
			parent.left.updatePlaylists();
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
			});
		}
	
		function onSaveasPlaylist() {
		
			var mediaFileIds = new Array();
			
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
			
				var checkbox = $("#songIndex" + i);
				if (checkbox && checkbox.is(":checked")) {
					mediaFileIds.push($("#songId" + i).html());
				}
			}
			playlistService.savePlaylist(mediaFileIds, function (){
			parent.left.updatePlaylists();
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
			});
		}	
		
</script>

<c:if test="${model.customScrollbar}">

    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:850, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if> 
</body>
	
</html>