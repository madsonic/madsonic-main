<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
    <%@ include file="head.jsp" %>
	<%@ include file="jquery.jsp" %>         
	<%@ include file="customScrollbar.jsp" %>
    
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">
        
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
    <img id="pageimage" src="<spring:theme code="moreImage"/>" width="32" alt="" />
    <fmt:message key="more.title"/><span class="desc"></span>
</h1>
<br>
<a href="http://www.madsonic.org" target="_blank"><img alt="Apps" src="icons/default/madsonic1.png" style="float: right;margin-left: 3em; margin-right: 3em"/></a>
<h2><img src="<spring:theme code="androidImage"/>" alt=""/>&nbsp;<fmt:message key="more.madsonic.title"/></h2>
<fmt:message key="more.madsonic.text"/>
<br>
<a href="http://beta.madsonic.org/pages/apps.jsp" target="_blank"><img alt="Apps" src="icons/default/apps.png" style="float: right;margin-left: 3em; margin-right: 3em"/></a>
<h2><img src="<spring:theme code="androidImage"/>" alt=""/>&nbsp;<fmt:message key="more.apps.title"/></h2>
<fmt:message key="more.apps.text"/>
<br>
<h2><img src="<spring:theme code="podcastImage"/>" width="32" alt=""/> &nbsp; <fmt:message key="more.podcast.title"/></h2>
<fmt:message key="more.podcast.text"/>
<br>

<a name="shortcuts"></a>
<h2 class="more-header"><i class="fa fa-keyboard-o fa-lg icon"></i>&nbsp;&nbsp;<fmt:message key="more.keyboard.title"/></h2>
<table class="indent music" style="width:100%">
    <tr>
        <th colspan="2" style="text-align:left;"><fmt:message key="more.keyboard.playback"/></th>
        <th colspan="2" style="text-align:left;"><fmt:message key="more.keyboard.navigation"/></th>
        <th colspan="2" style="text-align:left;"><fmt:message key="more.keyboard.general"/></th>
    </tr>
    <tr>
        <td class="more-shortcut">Space</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.playpause"/></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> h</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.home"/></td>
        <td class="more-shortcut">s</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.search"/></td>
    </tr>
    <tr>
        <td class="more-shortcut"><i class="fa fa-long-arrow-left"></i></td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.previous"/></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> i</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.index"/></td>
        <td class="more-shortcut">m</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.sidebar"/></td>
    </tr>
    <tr>
        <td class="more-shortcut"><i class="fa fa-long-arrow-right"></i></td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.next"/></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> t</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.starred"/></td>
        <td class="more-shortcut">l</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.leftbar"/></td>
    </tr>
    <tr>
        <td class="more-shortcut">Shift <i class="fa fa-long-arrow-left"></i></td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.seekbackward"/></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> o</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.podcasts"/></td>
        <td class="more-shortcut">?</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.shortcuts"/></td>
    </tr>
    <tr>
        <td class="more-shortcut">Shift <i class="fa fa-long-arrow-right"></i></td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.seekforward"/></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> s</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.settings"/></td>
        <td></td><td></td>
    </tr>
    <tr>
        <td class="more-shortcut">&ndash;</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.volumedown"/></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> p</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.playlists"/></td>
        <td></td><td></td>
    </tr>
    
    <tr>
        <td class="more-shortcut">+</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.volumeup"/></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> g</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.genre"/></td>
        <td></td><td></td>
    </tr>
     <tr>
        <td></td><td></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> r</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.radio"/></td>
        <td></td><td></td>
    </tr>
     <tr>
        <td></td><td></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> d</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.autodj"/></td>
        <td></td><td></td>
    </tr>   
    <tr>
        <td></td><td></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> r</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.more"/></td>
        <td></td><td></td>
    </tr>
    <tr>
        <td></td><td></td>
        <td class="more-shortcut">g <fmt:message key="more.keyboard.then"/> a</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.about"/></td>
        <td></td><td></td>
    </tr>
    <tr>
        <td></td><td></td>
        <td class="more-shortcut">i <fmt:message key="more.keyboard.then"/> a, b, c &hellip;</td><td class="more-shortcut-descr"><fmt:message key="more.keyboard.indexletter"/></td>
        <td></td><td></td>
    </tr>
</table>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:880, /*scrolling inertia: integer (milliseconds)*/
  		    scrollEasing:"easeOutCubic", /*scrolling easing: string*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$(".content_main").resize(function(e){
	$(".content_main").mCustomScrollbar("update");
});
</script>
</c:if>	


</body>
</html>