<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>	
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="approve"/>
    <c:param name="toast" value="${command.toast}"/>
</c:import>

<br>

<c:choose>
    <c:when test="${not empty model.users}">
        <form method="post" action="approveSettings.view">

        <table class="indent">
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Comment</th>
                <th style="padding-left:1em">approved</th>
                <th style="padding-left:1em">delete</th>        
            </tr>

            <c:forEach items="${model.users}" var="user">
                <tr>
                    <td><input type="text" name="name[${user.username}]" readonly="readonly" disabled="disabled" size="15" value="${user.username}"/></td>
                    <td><input type="text" name="email[${user.email}]" readonly="readonly" disabled="disabled" size="25" value="${user.email}"/></td>
                    <td><input type="text" name="comment[${user.comment}]" readonly="readonly" disabled="disabled" size="80" value="${user.comment}"/></td>
                    <td align="center" style="padding-left:1em"><input type="checkbox" ${user.approved ? "checked" : ""} name="approved[${user.username}]" class="checkbox"/></td>
                    <td align="center" style="padding-left:1em"><input type="checkbox" name="delete[${user.username}]" class="checkbox"/></td>
                </tr>
            </c:forEach>

            <tr>
                <td style="padding-top:1.5em" colspan="5">
                    <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
                    <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">
                </td>
            </tr>
        </table>
        </form>
    </c:when>
  <c:otherwise>
    <img src="<spring:theme code="likeImage"/>" width="32"> <em style="padding-left:0.8em"> <fmt:message key="approvesettings.nouser"/></em>
  </c:otherwise>
</c:choose>

</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:true,
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);
        
        $("#content_main").mCustomScrollbar("update");


$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</body>

</html>