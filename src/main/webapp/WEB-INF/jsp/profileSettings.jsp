<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%--@elvariable id="command" type="org.madsonic.command.PersonalSettingsCommand"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <c:choose>
        <c:when test="${customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>
 
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-1.4.2.js"/>"></script>  
    <script type='text/javascript' src="<c:url value="/script/jquery.dropdownReplacement-0.5.3.js"/>"></script>
    
    <link rel="stylesheet" href="<c:url value="/style/jquery.dropdownReplacement.css"/>" type="text/css">        

    <script type="text/javascript" language="javascript">
        function enableLastFmFields() {
            $("#lastFm").is(":checked") ? $("#lastFmTable").show() : $("#lastFmTable").hide();
        }
		
        function showShortcut() {
            $('#avatarSettings').show('blind');
            $('#showSettings').hide();
            $('#hideSettings').show();
            }

        function hideShortcut() {
            $('#avatarSettings').hide('blind');
            $('#hideSettings').hide();
            $('#showSettings').show();
            }
        function changePreview(theme) {
             $('#preview1').show();            
             $('#preview1').attr('src','icons/preview/' + theme + '1.png');
        }
        
    </script>
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="profile"/>
    <c:param name="restricted" value="false"/>
    <c:param name="toast" value="${command.reloadNeeded}"/>
</c:import>
<br>
<madsonic:url value="avatar.view" var="avatarUrl">
    <madsonic:param name="username" value="${command.user.username}"/>
</madsonic:url>

<h1><fmt:message key="personalsettings.title"><fmt:param>${command.user.username}</fmt:param></fmt:message> <img src="${avatarUrl}" alt="${command.user.username}" width="24" height="24"></h1>

<fmt:message key="common.default" var="defaultLabel"/>
        <fmt:message key="common.rows" var="rows"/>
        <fmt:message key="common.columns" var="columns"/>
        
<form:form method="post" action="profileSettings.view" commandName="command">

    <table style="white-space:nowrap" class="indent">

        <tr>
          <td></td>
        <div id="profileSettings" style="display:none;margin-top:5px;">
        <td style="text-align:left"><form:hidden path="profile"/></td>
        </div>
           <td></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.language"/></td>
            <td>
                <form:select path="localeIndex" cssStyle="display:none">
                    <form:option value="-1" label="${defaultLabel}"/>
                    <c:forEach items="${command.locales}" var="locale" varStatus="loopStatus">
                        <form:option value="${loopStatus.count - 1}" label="${locale}"/>
                    </c:forEach>
                </form:select>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="language"/></c:import>
            </td>
        </tr>

        <tr>
            <td><fmt:message key="personalsettings.theme"/></td>
            <td>
                <form:select path="themeIndex" cssStyle="display:none" onchange="changePreview(this.options[selectedIndex].label)">
                    <form:option value="-1" label="${defaultLabel}"/>
                    <c:forEach items="${command.themes}" var="theme" varStatus="loopStatus">
                        <form:option value="${loopStatus.count - 1}" label="${theme.name}"/>
                    </c:forEach>
                </form:select>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="theme"/></c:import>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="personalsettings.listtype"/></td>
                    <td>
                        <form:select path="listType" cssStyle="display:none">
                            <c:forTokens items="random newest hot allArtist starredArtist starred tip highest alphabetical frequent recent top new" delims=" " var="cat" varStatus="loopStatus">
                                <form:option value="${cat}"><fmt:message key="home.${cat}.text"/></form:option>
                            </c:forTokens>
                        </form:select>
                        <c:import url="helpToolTip.jsp"><c:param name="topic" value="listtype"/></c:import>
                    </td>
                </tr>
    </table>

    <table class="indent">    
        <tr> 
            <h2><fmt:message key="personalsettings.displaysettings"/>:</h2>
        
            <td><form:checkbox path="customScrollbarEnabled" id="customScrollbar" cssClass="checkbox"/></td>
            <td><label for="customScrollbar">CustomScrollbar Enabled</label></td>
        </tr>    
        <tr> 
            <td><form:checkbox path="customAccordionEnabled" id="customAccordion" cssClass="checkbox"/></td>
            <td><label for="customAccordion">CustomAccordion Enabled</label><br></td>
        </tr>    
        <tr> 
            <td><br></td>        
        </tr>         
        <tr>
            <td><form:checkbox path="showNowPlayingEnabled" id="nowPlaying" cssClass="checkbox"/></td>
            <td><label for="nowPlaying"><fmt:message key="personalsettings.shownowplaying"/></label></td>
        </tr>
	<tr>   
            <td><form:checkbox path="nowPlayingAllowed" id="nowPlayingAllowed" cssClass="checkbox"/></td>
            <td><label for="nowPlayingAllowed"><fmt:message key="personalsettings.nowplayingallowed"/></label></td>	    
	</tr>         
        <tr> 
            <td><br></td>        
        </tr>    
        <tr> 
            <td><form:checkbox path="showLeftPanelEnabled" id="leftPanel" cssClass="checkbox"/></td>
            <td><label for="leftPanel"><fmt:message key="personalsettings.showleftpanel"/></label></td>
            
            <td><form:checkbox path="showLeftBarShrinked" id="leftBarShrinked" cssClass="checkbox"/></td>
            <td><label for="leftBarShrinked"><fmt:message key="personalsettings.showleftbarshrinked"/></label></td>
        </tr>
        <tr> 
            <td><form:checkbox path="showRightPanelEnabled" id="rightPanel" cssClass="checkbox"/></td>
            <td><label for="rightPanel"><fmt:message key="personalsettings.showrightpanel"/></label></td>
            
            <td><form:checkbox path="showChatEnabled" id="chat" cssClass="checkbox"/></td>
            <td><label for="chat"><fmt:message key="personalsettings.showchat"/></label></td>
        </tr>
        <tr> 
            <td><form:checkbox path="showSidePanelEnabled" id="sidePanel" cssClass="checkbox"/></td>
            <td><label for="sidePanel"><fmt:message key="personalsettings.showsidepanel"/></label></td>
        </tr>
    </table>    

    <table class="indent">
        <tr>
            <h2><fmt:message key="personalsettings.notificationsettings"/>:</h2>
        </tr>
        <tr>
            <td><form:checkbox path="songNotificationEnabled" id="song" cssClass="checkbox"/></td>
            <td><label for="song"><fmt:message key="personalsettings.songnotification"/></label></td>
        </tr>
    </table>

    <table class="indent">
        <tr>
            <h2><fmt:message key="personalsettings.lastfmsettings"/>:</h2>
        </tr>
        <tr>
            <td><form:checkbox path="artistInfoEnabled" id="artistInfo" cssClass="checkbox"/></td>
            <td><label for="artistInfo"><fmt:message key="personalsettings.showartistinfo"/></label></td>
        </tr>
        <tr>
            <td><form:checkbox path="lastFmEnabled" id="lastFm" cssClass="checkbox" onclick="enableLastFmFields()"/></td>
            <td><label for="lastFm"><fmt:message key="personalsettings.lastfmenabled"/></label></td>
        </tr>
    </table>

    <table id="lastFmTable" style="padding-left:2em">
        <tr>
            <td><fmt:message key="personalsettings.lastfmusername"/></td>
            <td><form:input path="lastFmUsername" size="24"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.lastfmpassword"/></td>
            <td><form:password path="lastFmPassword" size="24"/></td>
        </tr>
    </table>

    <table class="indent">
        <tr>
            <h2><fmt:message key="personalsettings.othersettings"/>:</h2>
        <tr>
            <td><form:checkbox path="partyModeEnabled" id="partyModeEnabled" cssClass="checkbox"/></td>
            <td><label for="partyModeEnabled"><fmt:message key="personalsettings.partymode"/></label>
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="partymode"/></c:import>
            </td>
        </tr>
    </table>
    <table>
    <tr>
    <td style="min-width: 250px; vertical-align:top"><!-- inner -->

    <table class="indent" style="line-height: 200%;">
        <tr>
            <h2><fmt:message key="personalsettings.visibilitysettings"/>:</h2>
        
            <th style="padding:0 0.5em 0.5em 0;text-align:left;"><fmt:message key="personalsettings.display"/> </th>
            <th style="padding:0 0.5em 0.5em 0.5em;text-align:center;"><fmt:message key="personalsettings.browse"/></th>
            <th style="padding:0 0 0.5em 0.5em;text-align:center;"><fmt:message key="personalsettings.playlist"/></th>
            <th style="padding:0 0 0.5em 0.5em">
                <c:import url="helpToolTip.jsp"><c:param name="topic" value="visibility"/></c:import>
            </th>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.discnumber"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.discNumberVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.discNumberVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.tracknumber"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.trackNumberVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.trackNumberVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.artist"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.artistVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.artistVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.composer"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.composerVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.composerVisible" cssClass="checkbox"/></td>
        </tr>        
        <tr>
            <td><fmt:message key="personalsettings.album"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.albumVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.albumVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.mood"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.moodVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.moodVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.genre"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.genreVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.genreVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.year"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.yearVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.yearVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.bpm"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.bpmVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.bpmVisible" cssClass="checkbox"/></td>
        </tr>        
        <tr>
            <td><fmt:message key="personalsettings.bitrate"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.bitRateVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.bitRateVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.duration"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.durationVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.durationVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.format"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.formatVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.formatVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td><fmt:message key="personalsettings.filesize"/></td>
            <td style="text-align:center"><form:checkbox path="mainVisibility.fileSizeVisible" cssClass="checkbox"/></td>
            <td style="text-align:center"><form:checkbox path="playlistVisibility.fileSizeVisible" cssClass="checkbox"/></td>
        </tr>
    <tr>
       <td> 
       <br>
       </td>
    </tr>
        <tr>
            <td><fmt:message key="personalsettings.captioncutoff"/></td>
            <td style="text-align:center"><form:input path="mainVisibility.captionCutoff" size="3"/></td>
            <td style="text-align:center"><form:input path="playlistVisibility.captionCutoff" size="3"/></td>
        </tr>
    </table>    

    </td> 
    <td style="min-width: 250px; vertical-align:top">
    
    <table class="indent" style="line-height: 200%;">
        <tr>
            <h2><fmt:message key="personalsettings.playersettings"/>:</h2>
        
            <th style="padding:0 0.5em 0.5em 0;text-align:left;"><fmt:message key="personalsettings.display"/></th>
            <th style="padding:0 0.5em 0.5em 0.5em;text-align:center;">Icon</th>
            <th style="padding:0 0 0.5em 0.5em;text-align:center;">Option</th>
        </tr>
        <tr>
            <td>Rank</td>
            <td>
            <div class="icon-wrapper"><i class="fa custom-icon-nohover">
            <span class="custom-icon-rank starred">1
                <span class="fix-editor">&nbsp;</span></i>        
            </span>            
            </div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.rankVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Loved</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-heart custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.lovedVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Starred</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-star custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.starredVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Play</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-play custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.playVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Play + Add</td>
            <td>
            <div class="icon-wrapper">
            <span class="fa-stack custom-icon-nohover">
              <i class="fa fa-play fa-stack-1x" style="margin-left:-2px;margin-top:-3px"></i>
              <i class="fa fa-plus fa-stack-1x" style="margin-left:4px;margin-top:2px"></i>
            </span>
            </div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.playAddVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Play + Add more</td>
            <td>
            <div class="icon-wrapper">
            <span class="fa-stack custom-icon-nohover">
              <i class="fa fa-play fa-stack-1x" style="margin-left:-2px;margin-top:-3px"></i>
              <i class="fa fa-file-text-o fa-stack-1x" style="margin-left:4px;margin-top:2px"></i>
            </span>
            </div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.playMoreVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Add Context menu</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-plus custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.addContextVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Add next</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-plus-circle custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.addNextVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Add last</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-plus-circle custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.addLastVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Download</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-download custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.downloadVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Youtube</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-youtube custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.youtubeVisible" cssClass="checkbox"/></td>
        </tr>
        <tr>
            <td>Lyric</td>
            <td>
            <div class="icon-wrapper"><i class="fa fa-file-text-o custom-icon-nohover"></i></div>
            </td>
            <td style="text-align:center"><form:checkbox path="buttonVisibility.lyricVisible" cssClass="checkbox"/></td>
        </tr>        
    </table>
    </td>
    </tr>
    </table>
    
    <p style="padding-top:1em;padding-bottom:1em">
        <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em"/>
        <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'">
    </p>
            
    
    <span id="showSettings" style="display:inline;">
    <div class="forward" style="margin-top:10px;" ><a href="javascript:noop()"onclick="showShortcut()">Show Avatar Settings</a></div></span>

    <span id="hideSettings" style="display:none;">
    <div class="forward" style="margin-top:10px;" ><a href="javascript:noop()"onclick="hideShortcut()">Hide Avatar Settings</a></div></span>
    
    <div id="avatarSettings" style="display:none;margin-top:5px;">

    <h2><fmt:message key="personalsettings.avatar.title"/></h2>

    <p style="padding-top:1em; width:80%;">
        <c:forEach items="${command.avatars}" var="avatar">
            <c:url value="avatar.view" var="avatarUrl">
                <c:param name="id" value="${avatar.id}"/>
            </c:url>
            <span style="white-space:nowrap;">
                <form:radiobutton id="avatar-${avatar.id}" path="avatarId" value="${avatar.id}"/>
			    <label for="avatar-${avatar.id}"><img src="${avatarUrl}" alt="${avatar.name}" width="${avatar.width * 0.7}" height="${avatar.height * 0.7}" style="padding-right:2em;padding-bottom:1em"/></label>
            </span>
        </c:forEach>
    </p>
    <p>
        <form:radiobutton id="noAvatar" path="avatarId" value="-1"/>
        <label for="noAvatar"><fmt:message key="personalsettings.avatar.none"/></label>
    </p>
    <p>
        <form:radiobutton id="customAvatar" path="avatarId" value="-2"/>
        <label for="customAvatar"><fmt:message key="personalsettings.avatar.custom"/>
            <c:if test="${not empty command.customAvatar}">
                <madsonic:url value="avatar.view" var="avatarUrl">
                    <madsonic:param name="username" value="${command.user.username}"/>
                </madsonic:url>
                <img src="${avatarUrl}" alt="${command.customAvatar.name}" width="${command.customAvatar.width}" height="${command.customAvatar.height}" style="padding-right:2em"/>
            </c:if>
        </label>
    </p>
</form:form>

    <form method="post" enctype="multipart/form-data" action="avatarUpload.view">
        <table>
            <tr>
                <td style="padding-right:1em"><fmt:message key="personalsettings.avatar.changecustom"/></td>
                <td style="padding-right:1em"><input type="file" id="file" name="file" size="40"/></td>
                <td style="padding-right:1em"><input type="submit" value="<fmt:message key="personalsettings.avatar.upload"/>"/></td>
            </tr>
        </table>
    </form>
    
    </div>
<br>
<!--  
<p class="detail" style="text-align:right">
    <fmt:message key="personalsettings.avatar.courtesy"/>
</p>
-->

<c:if test="${command.reloadNeeded}">
    <script language="javascript" type="text/javascript">
            parent.frames.main.location.href="userSettings.view?";
    </script>
</c:if>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<script type="text/javascript">
$(document).ready(function() {
    $("#localeIndex").dropdownReplacement({selectCssWidth: 250, optionsDisplayNum: 10 });
    $("#themeIndex").dropdownReplacement({selectCssWidth: 250, optionsDisplayNum: 15, });
    $("#listType").dropdownReplacement({selectCssWidth: 250, optionsDisplayNum: 15, });
});    
</script>


<c:if test="${customScrollbar}">
<script type="text/javascript">    

        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    alwaysShowScrollbar:true,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>    
</body>
</html>

