<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>	
	
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
    <meta http-equiv="REFRESH" content="20;URL=status.view">
</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
    <img src="<spring:theme code="statusImage"/>" width="32" alt="">
    <fmt:message key="status.title"/>
</h1>

<table width="100%" class="ruleTable indent">
    <tr>
        <th class="ruleTableHeader"><fmt:message key="status.type"/></th>
        <th class="ruleTableHeader"><fmt:message key="status.player"/></th>
        <th class="ruleTableHeader"><fmt:message key="status.user"/></th>
        <th class="ruleTableHeader"><fmt:message key="status.current"/></th>
        <th class="ruleTableHeader"><fmt:message key="status.transmitted"/></th>
        <th class="ruleTableHeader"><fmt:message key="status.bitrate"/></th>
    </tr>

    <c:forEach items="${model.transferStatuses}" var="status">

        <c:choose>
            <c:when test="${empty status.playerType}">
                <fmt:message key="common.unknown" var="type"/>
            </c:when>
            <c:otherwise>
                <c:set var="type" value="(${status.playerType})"/>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${status.stream}">
                <fmt:message key="status.stream" var="transferType"/>
            </c:when>
            <c:when test="${status.download}">
                <fmt:message key="status.download" var="transferType"/>
            </c:when>
            <c:when test="${status.upload}">
                <fmt:message key="status.upload" var="transferType"/>
            </c:when>
        </c:choose>

        <c:choose>
            <c:when test="${empty status.username}">
                <fmt:message key="common.unknown" var="user"/>
            </c:when>
            <c:otherwise>
                <c:set var="user" value="${status.username}"/>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${empty status.path}">
                <fmt:message key="common.unknown" var="current"/>
            </c:when>
            <c:otherwise>
                <c:set var="current" value="${status.path}"/>
            </c:otherwise>
        </c:choose>

        <madsonic:url value="/statusChart.view" var="chartUrl">
            <c:if test="${status.stream}">
                <madsonic:param name="type" value="stream"/>
            </c:if>
            <c:if test="${status.download}">
                <madsonic:param name="type" value="download"/>
            </c:if>
            <c:if test="${status.upload}">
                <madsonic:param name="type" value="upload"/>
            </c:if>
            <madsonic:param name="index" value="${status.index}"/>
        </madsonic:url>

        <tr>
            <td class="ruleTableCell">${transferType}</td>
            <td class="ruleTableCell">${status.player}<br>${type}</td>
            <td class="ruleTableCell">${user}</td>
            <td class="ruleTableCell">${current}</td>
            <td class="ruleTableCell">${status.bytes}</td>
            <td class="ruleTableCell" width="${model.chartWidth}"><img width="${model.chartWidth}" height="${model.chartHeight}" src="${chartUrl}" alt=""></td>
        </tr>
    </c:forEach>
</table>

<div class="forward"><a href="status.view?"><fmt:message key="common.refresh"/></a></div>


</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">    

		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);

$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	

</body>
</html>