<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
    
    <script type="text/javascript" src="<c:url value='/script/scripts.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/dwr/util.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/dwr/engine.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/dwr/interface/playlistService.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/dwr/interface/starService.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/dwr/interface/lovedTrackService.js'/>"></script>	    
    <script type="text/javascript" language="javascript">

        var playlist;
        var songs;

        function init() {
            dwr.engine.setErrorHandler(null);

            $("#dialog-edit").dialog({resizable: true, width:450, autoOpen: false,
                buttons: {
                    "<fmt:message key="common.save"/>": function() {
                        $(this).dialog("close");
                        var name = $("#newName").val(); 
                        var comment = $("#newComment").val();
                        var isPublic = $("#newPublic").is(":checked");
                        $("#name").html(name);
                        $("#comment").html(comment);
                        playlistService.updatePlaylist(playlist.id, name, comment, isPublic, function (playlistInfo){playlistCallback(playlistInfo);}); // parent.leftPanel.updatePlaylists()
                    },
                    "<fmt:message key="common.cancel"/>": function() {
                        $(this).dialog("close");
                    }
                }});

            $("#dialog-select-playlist").dialog({resizable: true, width:450, height: 400, modal: false, autoOpen: false,
                buttons: {
                    "<fmt:message key="common.cancel"/>": function() {
                        $(this).dialog("close");
                    }
                }});                
            
            $("#dialog-delete").dialog({resizable: false, width:450, height: 170, autoOpen: false,
                buttons: {
                    "<fmt:message key="common.delete"/>": function() {
                        $(this).dialog("close");
                        playlistService.deletePlaylist(playlist.id, function (){location.href = "playlists.view";});
                    },
                    "<fmt:message key="common.cancel"/>": function() {
                        $(this).dialog("close");
                    }
                }});

            $("#playlistBody").sortable({
                stop: function(event, ui) {
                    var indexes = [];
                    $("#playlistBody").children().each(function() {
                        var id = $(this).attr("id").replace("pattern", "");
                        if (id.length > 0) {
                            indexes.push(parseInt(id) - 1);
                        }
                    });
                    onRearrange(indexes);
                },
                cursor: "move",
                axis: "y",
                containment: "parent",
                helper: function(e, tr) {
                    var originals = tr.children();
                    var trclone = tr.clone();
                    trclone.children().each(function(index) {
                        // Set cloned cell sizes to match the original sizes
                        $(this).width(originals.eq(index).width());
                        $(this).css("maxWidth", originals.eq(index).width());
                        $(this).css("border-top", "1px solid #888");
                        $(this).css("border-bottom", "1px solid #888");
                        $(this).css("background-color", "#333");
                        $(this).css("color", "#888");
                        $(this).css("a:link", "color: #fff");
                    });
                    return trclone;
                }
            });

            getPlaylist();
        }
        
        function getPlaylist() {
            playlistService.getPlaylist(${model.playlist.id}, playlistCallback);
        }

        function playlistCallback(playlistInfo) {
            this.playlist = playlistInfo.playlist;
            this.songs = playlistInfo.entries;

            if (songs.length == 0) {
                $("#empty").show();
            } else {
                $("#empty").hide();
            }


            $("#songCount").html(playlist.fileCount);
            $("#duration").html(playlist.durationAsString);

            if (playlist.public) {
                $("#shared").html("<fmt:message key="playlist2.shared"/>");
            } else {
                $("#shared").html("<fmt:message key="playlist2.notshared"/>");
            }

            // Delete all the rows except for the "pattern" row
            dwr.util.removeAllRows("playlistBody", { filter:function(tr) {
                return (tr.id != "pattern");
            }});

            // Create a new set cloned from the pattern row
            for (var i = 0; i < songs.length; i++) {
                var song  = songs[i];
                var id = i + 1;
                
                dwr.util.cloneNode("pattern", { idSuffix:id });

                $("#loveSong" + id).addClass(song.loved ? "fa-heart starred" : "fa-heart-o");
                
                $("#starSong" + id).addClass(song.starred ? "fa-star starred" : "fa-star-o");
		
                if ($("#rank" + id)) {
                    if (song.rank >= 10) {
                        $("#rank" + id).html(truncate(song.rank));
                        $("#rank" + id).attr("rank", song.rank);
                        $("#rank" + id).attr("class", "rank");
                    } else if (song.rank == 0) {
                        $("#rank" + id).html(truncate(""));
                        $("#rank" + id).attr("rank", "");
                        $("#rank" + id).attr("class", "");
                    } else {
                        $("#rank" + id).html(truncate("0" + song.rank));
                        $("#rank" + id).attr("rank", "0" + song.rank);
                        $("#rank" + id).attr("class", "rank");
                    }
                }
		
                if (!song.present) {
                    $("#missing" + id).show();
                }
		
                if ($("#songId" + id)) {
                    $("#songId" + id).html(truncate(song.id));
                    $("#songId" + id).attr("songid", song.id);
                }
                if ($("#title" + id)) {
                    $("#title" + id).html(truncate(song.title));
                    $("#title" + id).attr("title", song.title);
                }
                if ($("#album" + id)) {
                    $("#album" + id).html(truncate(song.album));
                    $("#album" + id).attr("title", song.album);
                    $("#albumUrl" + id).attr("href", "main.view?id=" + song.id);
                }
                if ($("#artist" + id)) {
                    $("#artist" + id).html(truncate(song.artist));
                    $("#artist" + id).attr("title", song.artist);
                }
                if ($("#songDuration" + id)) {
                    $("#songDuration" + id).html(song.durationAsString);
                }

                // Note: show() method causes page to scroll to top.
                $("#pattern" + id).css("display", "table-row");
            }
        }

        function truncate(s) {
            if (s == null) {
                return s;
            }
            var cutoff = 50;

            if (s.length > cutoff) {
                return s.substring(0, cutoff) + "...";
            }
            return s;
        }

        function onPlay(index) {
            parent.playQueue.onPlay(songs[index].id);
        }
        function onPlayAll() {
            parent.playQueue.onPlayPlaylist(playlist.id, false);
        }
        
        function onPlayAllRandom() {
            parent.playQueue.onPlayPlaylist(playlist.id, true);
        }        
        function onAdd(index) {
            parent.playQueue.onAdd(songs[index].id);
            $().toastmessage('showSuccessToast', '<fmt:message key="main.addlast.toast"/>')
        }
        function onAddNext(index) {
            parent.playQueue.onAddNext(songs[index].id);
            $().toastmessage('showSuccessToast', '<fmt:message key="main.addnext.toast"/>')
        }        
        function onAddAll() {
            parent.playQueue.onAddPlaylist(playlist.id);
        }
        
        function onStar(index, forced) {
            playlistService.toggleStar(playlist.id, index, forced, playlistCallback);
        }

        function onLove(index, forced) {
            playlistService.toggleLove(playlist.id, index, forced, playlistCallback);
        }
        
        function onStarAll(forced) {
            playlistService.toggleAllStar(playlist.id, forced, playlistCallback);
        }
        
        function onLoveAll(forced) {
            playlistService.toggleAllLove(playlist.id, forced, playlistCallback);
        }        
        
        function onStarSelected(forced) {
             var mediaFileIds = new Array();
            var count = 0;
            
            for (var i = 0; i < songs.length+1; i++) {
                var checkbox = $("#songIndex" + i);
                if (checkbox && checkbox.is(":checked")) {
                    onStar(i - 1, forced);
                    count++;
                }
            }
            if (count > 0){
                $().toastmessage("showSuccessToast", "Toogle Stars");
            }

            //parent.leftPanel.updatePlaylists();
            getPlaylist();
        }
        
        function onLoveSelected(forced) {
            var mediaFileIds = new Array();
           var count = 0;
           
           for (var i = 0; i < songs.length+1; i++) {
               var checkbox = $("#songIndex" + i);
               if (checkbox && checkbox.is(":checked")) {
                   onLove(i - 1, forced);
                   count++;
               }
           }
           if (count > 0){
               $().toastmessage("showSuccessToast", "Toogle Loved");
           }

           //parent.leftPanel.updatePlaylists();
           getPlaylist();
       }        
        
        function toggleStar(mediaFileId, element) {
            starService.star(mediaFileId, !$(element).hasClass("fa-star"));
            $(element).toggleClass("fa-star fa-star-o starred");
        }         
        
        function onRemove(index) {
            playlistService.remove(playlist.id, index, function (playlistInfo){playlistCallback(playlistInfo)});
        }
        function onRearrange(indexes) {
            playlistService.rearrange(playlist.id, indexes, playlistCallback);
        }
        function onEditPlaylist() {
            $("#dialog-edit").dialog("open");
        }
        function onDeletePlaylist() {
            $("#dialog-delete").dialog("open");
        }

        function onAppendPlaylist() {
        playlistService.getWritablePlaylists(playlistAppendCallback);
        }
        function playlistAppendCallback(playlists) {
            $("#dialog-select-playlist-list").empty();
            for (var i = 0; i < playlists.length; i++) {
                var playlist = playlists[i];
                $("<p class='dense'><b><a href='#' onclick='appendPlaylist(" + playlist.id + ")'>" + playlist.name + "</a></b></p>").appendTo("#dialog-select-playlist-list");
            }
            $("#dialog-select-playlist").dialog("open");
        }
        function appendPlaylist(playlistId) {
            $("#dialog-select-playlist").dialog("close");

            var mediaFileIds = new Array();
            for (var i = 0; i < songs.length; i++) {
                if ($("#songIndex" + (i + 1)).is(":checked")) {
                    mediaFileIds.push(songs[i].id);
                }
            }
            playlistService.appendToPlaylist(playlistId, mediaFileIds, function (){
                //parent.leftPanel.updatePlaylists();
                $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.appendtoplaylist"/>");
            });
        }
        
        function selectAll(b) {
            for (var i = 0; i < songs.length; i++) {
                if (b) {
                    $("#songIndex" + (i + 1)).attr("checked", "checked");
                } else {
                    $("#songIndex" + (i + 1)).removeAttr("checked");
                }
            }
        }

        // --------------------------------------------
        function onAddSelectedNext() {
            var mediaFileIds = new Array();
            var count = 0;
            for (var i = 0; i < songs.length; i++) {
                var checkbox = $("#songIndex" + i);
                if (checkbox && checkbox.is(":checked")) {
                    mediaFileIds.push($("#songId" + i).html());
                    count++;
                }
            }
            parent.playQueue.onAddSelectedNext(mediaFileIds);
            if (count > 0) {
                $().toastmessage("showSuccessToast", "added next to PlayQueue");
            }
        }    

        // --------------------------------------------
        function onAddSelectedLast() {
            var mediaFileIds = new Array();
            var count = 0;
            for (var i = 0; i < songs.length; i++) {
                var checkbox = $("#songIndex" + i);
                if (checkbox && checkbox.is(":checked")) {
                    mediaFileIds.push($("#songId" + i).html());
                    count++;
                }
            }
            parent.playQueue.onAddSelectedLast(mediaFileIds);
            if (count > 0) {
                $().toastmessage("showSuccessToast", "added last to PlayQueue");
            }
        }    
    
    </script>
    
    <style type="text/css">
        .playlist-missing {
            color: red;
            border: 1px solid red;
            display: none;
            font-size: 90%;
            padding-left: 5px;
            padding-right: 5px;
            margin-right: 5px;
        }
    </style>
    
</head>

<body class="mainframe bgcolor1" onload="init()"> 

<div id="content_main" class="content_main"> 
  
<div id="container" class="container">

<h1 id="name">${model.playlist.name}</h1>

<div style="float:right;margin-right:2.0em;margin-bottom:1.75em">
<c:import url="playlistArt.jsp">
    <c:param name="playlistId" value="${model.playlist.id}"/>
    <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>      
    <c:param name="coverArtSize" value="120"/>
</c:import>
</div>

<h2>
        <a href="javascript:void(0)" onclick="self.location.href='playlists.view?'">Playlists</a>

        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="javascript:void(0)" onclick="onPlayAll();"><fmt:message key="common.play"/></a>
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="javascript:void(0)" onclick="onPlayAllRandom();"><fmt:message key="common.playrandom"/></a>
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="javascript:void(0)" onclick="onAddAll();"><fmt:message key="common.add"/></a>    
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="javascript:void(0)" onclick="selectAll(true);"><fmt:message key="common.select"/></a>
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="javascript:void(0)" onclick="onAppendPlaylist();"><fmt:message key="common.append"/></a>

     <!--   <img src="<spring:theme code="sepImage"/>" alt="">&nbsp;<a href="javascript:void(0)" onclick="onStarAll();">Toogle Star</a> -->
        
    <c:if test="${model.user.downloadRole}">
        <c:url value="download.view" var="downloadUrl"><c:param name="playlist" value="${model.playlist.id}"/></c:url>
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="${downloadUrl}"><fmt:message key="common.download"/></a>
    </c:if>
    <c:if test="${model.editAllowed}">
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="editTags.view?id=${model.playlist.id}&type=playlist">Tags</a>    
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="javascript:void(0)" onclick="onEditPlaylist();"><fmt:message key="common.edit"/></a>
        <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="javascript:void(0)" onclick="onDeletePlaylist();"><fmt:message key="common.delete"/></a>
    </c:if>
    <c:url value="exportPlaylist.view" var="exportUrl"><c:param name="id" value="${model.playlist.id}"/></c:url>
    <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="${exportUrl}"><fmt:message key="playlist2.export"/></a>
    
    <c:if test="${model.user.shareRole}">    
    <c:url value="createPlaylistShare.view" var="shareUrl"><c:param name="id" value="${model.playlist.id}"/></c:url>
    <img src="<spring:theme code="sepImage"/>" style="margin: 5px 5px;" alt="">&nbsp;<a href="${shareUrl}">Share</a>
    </c:if>
</h2>

<div class="detail" style="padding-top:1em">
    <fmt:message key="playlist2.created">
        <fmt:param>${model.playlist.username}</fmt:param>
        <fmt:param><fmt:formatDate type="date" dateStyle="long" value="${model.playlist.created}"/></fmt:param>
    </fmt:message>.
    <span id="shared"></span>.
    <span id="songCount"></span> <fmt:message key="playlist2.songs"/> (<span id="duration"></span>)
</div>

<div id="comment" class="detail" style="padding-top:1em;padding-bottom:1.5em">${model.playlist.comment}</div>

    <select id="moreActions" onchange="actionSelected(this.options[selectedIndex].id);" style="margin-bottom:1.0em">
    <option id="top" selected="selected"><fmt:message key="main.more"/></option>
    <optgroup label="all songs"/>
    <option id="selectAll"><fmt:message key="playlist.more.selectall"/></option>
    <option id="selectNone"><fmt:message key="playlist.more.selectnone"/></option>
    <option id="toogleAllLoved">Toogle Loved</option>
    <option id="toogleAll">Toogle Starred</option>
    <option id="toogleAllLovedForced">Forced All Loved</option>
    <option id="toogleAllForced">Forced All Starred</option>

    <c:if test="${model.user.shareRole}">
        <option id="share"><fmt:message key="main.more.share"/></option>
    </c:if>
    <c:if test="${model.user.downloadRole}">
        <option id="download"><fmt:message key="common.download"/></option>
    </c:if>
    <optgroup label="<fmt:message key="main.more.selection"/>"/>
    <option id="appendPlaylist"><fmt:message key="playlist.append"/></option>
    <option id="toogleLovedSelected">Toogle Loved</option>
    <option id="toogleSelected">Toogle Starred</option>
    <option id="toogleLovedSelectedForced">Forced Selection Loved</option>
    <option id="toogleSelectedForced">Forced Selection Starred</option>
    <option id="addNext">Add Next to PlayQueue</option>
    <option id="addLast">Add Last to PlayQueue</option>
    </select>

<div style="clear:both"></div>

<p id="empty" style="display: none;"><em><fmt:message key="playlist2.empty"/></em></p>

<table class="music" style="cursor:pointer">
    <tbody id="playlistBody">
    <tr id="pattern" style="display:none;margin:0;padding:0;border:0">
    
        <td class="fit">
            <div class="icon-wrapper"><i class="fa custom-icon"> 
            <span class="custom-icon-rank starred" style="cursor: text;">  
            <span id="rank">&nbsp;</span></span></i>      
            </div> 
        </td> 

        <td class="fit">
            <i id="loveSong" class="fa clickable custom-icon" onclick="onLove(this.id.substring(8)-1, true);"></i>
        </td>
        
        <td class="fit">
            <i id="starSong" class="fa clickable custom-icon" onclick="onStar(this.id.substring(8)-1, true);"></i>
        </td>
	
        <td class="fit">
            <i id="play" class="fa fa-play clickable custom-icon" onclick="onPlay(this.id.substring(4)-1)" title="<fmt:message key="common.play"/>"></i>
        </td>	
	
        <td class="fit">
            <i id="addNext" class="fa fa-plus-circle clickable custom-icon" onclick="onAddNext(this.id.substring(7)-1)" title="<fmt:message key="main.addnext"/>"></i>
        </td>	
		 
        <td class="fit">
            <i id="add" class="fa fa-plus-circle clickable custom-icon" onclick="onAdd(this.id.substring(3)-1)" title="<fmt:message key="common.addlast"/>"></i>
        </td>
        
        <td class="fit" style="padding-left: 1.2em"><input type="checkbox" class="checkbox" id="songIndex"></td>
        <td class="fit" style=""><span id="missing" class="playlist-missing"><fmt:message key="playlist.missing"/></span></td>
        <td class="fit"><span id="songId" style="display: none">id</span></td>    
        <td class="truncate"><span id="title">Title</span></td>
        <td class="truncate"><a id="albumUrl" target="main"><span id="album" class="detail">Album</span></a></td>
        <td class="truncate"><span id="artist" class="detail">Artist</span></td>
        <td class="fit rightalign"><span id="songDuration" class="detail">Duration</span></td>
        <c:if test="${model.editAllowed}">
        <td class="fit" style="padding-right:5px;">
            <i id="removeSong" class="fa fa-remove clickable custom-icon warning" onclick="onRemove(this.id.substring(10)-1)" title="<fmt:message key="playlist.remove"/>"></i>
        </td>	
        </c:if>  
    </tr>
    </tbody>
</table>

<div id="dialog-select-playlist" title="<fmt:message key="main.addtoplaylist.title"/>" style="display: none;">
    <p><fmt:message key="main.addtoplaylist.text"/></p>
    <div id="dialog-select-playlist-list"></div>
</div>

<div id="dialog-delete" title="<fmt:message key="common.confirm"/>" style="display: none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
        <fmt:message key="playlist2.confirmdelete"/></p>
</div>

<div id="dialog-edit" title="<fmt:message key="common.edit"/>" style="display: none;">
    <form>
        <label for="newName" style="display:block;"><fmt:message key="playlist2.name"/></label>
        <input type="text" name="newName" id="newName" value="${fn:escapeXml(model.playlist.name)}" class="ui-widget-content"
               style="display:block;width:95%;"/>
        <label for="newComment" style="display:block;margin-top:1em"><fmt:message key="playlist2.comment"/></label>
        <input type="text" name="newComment" id="newComment" value="${fn:escapeXml(model.playlist.comment)}" class="ui-widget-content"
               style="display:block;width:95%;"/>
        <input type="checkbox" name="newPublic" id="newPublic" ${model.playlist['public'] ? "checked='checked'" : ""} style="margin-top:1.5em" class="ui-widget-content"/>
        <label for="newPublic"><fmt:message key="playlist2.isPublic"/></label>
    </form>
</div>
    <script type="text/javascript" language="javascript">

        <!-- actionSelected() is invoked when the users selects from the "More actions..." combo box. -->
        function actionSelected(id) {

            if (id == "top") {
                return;
            } else if (id == "selectAll") {
                selectAll(true);
            } else if (id == "selectNone") {
                selectAll(false);
            } else if (id == "share") {
                parent.frames.main.location.href = "${shareUrl}";
            } else if (id == "download") {
                location.href = "${downloadUrl}";
            } else if (id == "toogleAll") {
                onStarAll(false);
            } else if (id == "toogleAllForced") {
                onStarAll(true);    
            } else if (id == "toogleAllLoved") {
            	onLoveAll(false);
            } else if (id == "toogleAllLovedForced") {
                onLoveAll(true);    
            } else if (id == "toogleSelected") {
                onStarSelected(true);    
            } else if (id == "toogleSelectedForced") {
                onStarSelected(false);    
            } else if (id == "toogleLovedSelected") {
                onLoveSelected(true);    
            } else if (id == "toogleLovedSelectedForced") {
                onLoveSelected(false);                 
            } else if (id == "addNext") {
                onAddSelectedNext();
            } else if (id == "addLast") {
                onAddSelectedLast();
            } else if (id == "appendPlaylist") {
                onAppendPlaylist();
            } else if (id == "savePlaylist") {
                onSavePlaylist();
        } else if (id == "savePlaylistNamed") {
                onSavePlaylistNamed();
            }
            $("#moreActions").prop("selectedIndex", 0);

    }
    </script>
	
  </div>    
</div>
<!-- CONTENT -->

<c:if test="${model.customScrollbar}">
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:600, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:true, /*auto-hide scrollbar when idle*/     
                    alwaysShowScrollbar:false,
                                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);

$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>	    
    
</body>
</html>