<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %> 

    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet"> 
     <!--   <script type="text/javascript" src="<c:url value="/script/smooth-scroll.js"/>"></script> -->
        </c:otherwise>
    </c:choose>    
    
    <c:if test="${model.customAccordion}">
    <script>
        $(window).ready(function() {
            var icons = {
            header: "ui-icon-triangle-1-e", 
            headerSelected: "ui-icon-triangle-1-s"
        };
        $("#accordion").accordion( {active: true, autoHeight: false, navigation: false, collapsible: true, icons: icons} );    
        $('.ui-accordion').bind('accordionchange', function(event, ui) {
            if(!ui.newHeader.length) { return; }
                ui.newHeader // jQuery object, activated header
                ui.oldHeader // jQuery object, previous header
                ui.newContent // jQuery object, activated content
                ui.oldContent // jQuery object, previous content
                $('html, body, content_left').animate({scrollTop: $(ui.newHeader).offset().top -170}, 300);
                $('ui.newHeader').click();
                $('#ui.newHeader').click();
                $('newHeader').click();
         }) ;
      });
    </script>
    </c:if>

    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-2.1.2.js"/>"></script>
    
    <script type='text/javascript' src="<c:url value="/script/jquery.dropdownReplacement-0.5.3.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/style/jquery.dropdownReplacement.css"/>" type="text/css">
    
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script> 
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/playlistService.js"/>"></script>

    <script language="javascript" type="text/javascript">
     function refreshFrames(sUrl) {
       location = "leftPanel.view?musicFolderId=" + sUrl;
       parent.frames.main.location.href="home.view?listType=${model.listType}";
    }
    </script>    
    
    <script type="text/javascript">
    $(document).ready(function() {
        $("#musicFolder").dropdownReplacement({selectCssWidth: 200, optionsDisplayNum: 10, onSelect : function(value, text, selectIndex){ refreshFrames( value ); }});
        $("#genreAll").dropdownReplacement({selectCssWidth: 200, optionsDisplayNum: 20, onSelect : function(value, text, selectIndex){ location='leftPanel.view?genre=' + escape(value); }});
    });    
    </script>
    
    <script type="text/javascript" language="javascript">
        var playlists;

        function init() {
            dwr.engine.setErrorHandler(null);
            updatePlaylists();
        }

        function updatePlaylists() {
            playlistService.getReadablePlaylists(playlistCallback);
        }

        function createEmptyPlaylist() {
            playlistService.createEmptyPlaylist(playlistCallback);
            $("#playlists").show();
        }
        function showStatistic() {
            $('#statistics').show('blind');
            $('#hideStatistics').show();
             $('#showStatistics').hide();
        }

        function hideStatistic() {
            $('#statistics').hide('blind');
            $('#hideStatistics').hide();
            $('#showStatistics').show();
        }

        function showShortcut() {
            $('#shortcuts').show('blind');
            $('#showShortcuts').hide();
            $('#hideShortcuts').show();
        }

        function hideShortcut() {
            $('#shortcuts').hide('blind');
            $('#hideShortcuts').hide();
            $('#showShortcuts').show();
            }

        function hideAllPlaylists() {
            $('#playlistOverflow').hide('blind');
            $("#playlists").hide('blind');
            $('#hideAllPlaylists').hide('blind');
            $('#showAllPlaylists').hide('blind');
            $('#showsomePlaylists').show('blind');
        }
        
        function showAllPlaylists() {
            $("#playlists").show();
            $('#playlistOverflow').show('blind');
            $('#hideAllPlaylists').show('blind');            
            $('#showAllPlaylists').hide('blind');
            $('#showsomePlaylists').hide('blind');
        }

        function showsomePlaylists() {
            $('#playlistOverflow').hide('blind');
            $("#playlists").show('blind');
            $('#hideAllPlaylists').show('blind');            
            $('#showAllPlaylists').show('blind');
            $('#showsomePlaylists').hide('blind');
        }
        
        function playlistCallback(playlists) {
            this.playlists = playlists;
            
            $("#playlists").empty();
            $("#playlistOverflow").empty();
            
            for (var i = 0; i < playlists.length; i++) {
                var playlist = playlists[i];
                var overflow = i > 9;
                $("<p class='dense'><a target='main' href='playlist.view?id=" +
                        playlist.id + "'>" + escapeHtml(playlist.name) + "&nbsp;(" + playlist.fileCount + ")</a></p>").appendTo(overflow ? "#playlistOverflow" : "#playlists");
            }

            if (playlists.length > 9 && !$('#playlistOverflow').is(":visible")) {
                $("#playlists").hide();
                $("#playlistOverflow").hide();            
                $('#showAllPlaylists').show();
                $('#showsomePlaylists').show();
            }
            
            if (playlists.length < 9 ) {
                $("#playlists").hide();
                $("#playlistOverflow").hide();
                $('#showAllPlaylists').hide();
                $('#showsomePlaylists').show('blind');
            }            
        }
       
    
        var previousQuery = "";
        var instantSearchTimeout;

        function triggerInstantSearch(event) {
            if (event.keyCode == 27) { // Escape key
                $("#query").blur();
                return;
            }
            if (instantSearchTimeout) {
                window.clearTimeout(instantSearchTimeout);
            }
            instantSearchTimeout = window.setTimeout(executeInstantSearch, 300);
        }
        
        function executeInstantSearch() {
            var query = $("#query").val().trim();
            if (query.length > 1 && query != previousQuery) {
                previousQuery = query;
                document.searchForm.submit();
            }
        }
    </script>    

</head>
<body class="bgcolor2 leftpanel" onload="init()">

<fmt:message key="common.search" var="search"/>


<c:if test="${not model.customScrollbar}">
<a name="top"></a>
</c:if>    

<c:if test="${fn:length(model.musicFolders) >= 0}">
<div id="content_leftpanel" class="content_leftpanel">
</c:if>    

    <!-- CONTENT -->
    <div class="bgcolor2" style="opacity: 1.0; clear: both; position: fixed; top: 0; right: 0; left: 0;
    padding: 0.25em 0.15em 0.15em 0.15em; max-width: 100%; border-bottom: 1px dotted #A4A4A4;z-index:1000">

    <c:if test="${fn:length(model.musicFolders) >= 0}">
        <div style="padding-top:10px; width:200px; margin-left:6px;">
            <select name="musicFolderId" id="musicFolder" style="display:none;">
                <option value="-1"><fmt:message key="left.allfolders"/></option>
                
                <c:if test="${model.MusicFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -2 ? "selected" : ""} value="-2"><fmt:message key="left.allmusic"/></option>            
                </c:if>
                <c:if test="${model.VideoFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -3 ? "selected" : ""} value="-3"><fmt:message key="left.allvideos"/></option>
                </c:if>
                <c:if test="${model.MoviesFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -4 ? "selected" : ""} value="-4"><fmt:message key="left.allmovies"/></option>
                </c:if>
                <c:if test="${model.SeriesFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -5 ? "selected" : ""} value="-5"><fmt:message key="left.allseries"/></option>
                </c:if>
                <c:if test="${model.ImagesFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -6 ? "selected" : ""} value="-6"><fmt:message key="left.allimages"/></option>
                </c:if>
                <c:if test="${model.TVFolderEnabled eq true}">
                <option ${model.selectedMusicFolderId == -7 ? "selected" : ""} value="-7"><fmt:message key="left.alltv"/></option>
                </c:if>
                <c:if test="${fn:length(model.musicFolders) > 0}">
                <option value="-1">------------</option>
                </c:if>
                <c:forEach items="${model.musicFolders}" var="musicFolder">
                    <option ${model.selectedMusicFolder.id == musicFolder.id ? "selected" : ""} value="${musicFolder.id}">${musicFolder.name}</option>
                </c:forEach>
            </select>
        </div>
    </c:if>

    <div style="padding-top:0,5em; width:200px;margin-left:6px;margin-top:5px;">
        <select name="genreAll" id="genreAll" style="display:none;">
            <option value=""><fmt:message key="left.allgenres"/></option>
            <c:forEach items="${model.allGenres}" var="genre">
                <option ${genre eq model.selectedGenre ? "selected" : ""} value="${genre}">${genre}</option>
            </c:forEach>
        </select>    
    </div>
        
    <c:choose>
        <c:when test="${model.scanning}">
        <div style="padding-bottom:0.35em; padding-left:0.7em;margin-top:10px;">
            <div class="forward"><a href="leftPanel.view"><fmt:message key="common.refresh"/></a></div>
        </div>
        </c:when>
        <c:otherwise>
            <div style="padding-bottom:0.35em; padding-left:0.7em;margin-top:10px;">
                <div class="forward"><a href="leftPanel.view?refresh=true"><fmt:message key="common.refresh"/></a></div>
            </div>
        </c:otherwise>
    </c:choose>
    </div>
    
    <c:if test="${model.customScrollbar}">
    <a id="top"></a>
    </c:if>        

    <div style="padding-left:4px">
    </div>
    
    <c:if test="${model.statistics.songCount gt 0}">
    
    <div id="statistics" style="display:none;margin-top:10px;">
    
    <h2 class="bgcolor1"><fmt:message key="left.statistic"/></h2>    
    
        <div class="detail" style="margin-left:5px;margin-top:10px;">
            <fmt:message key="left.statistics">
                <fmt:param value="${model.statistics.artistCount}"/>
                <fmt:param value="${model.statistics.albumArtistCount}"/>
                <fmt:param value="${model.statistics.albumCount}"/>
                <fmt:param value="${model.statistics.genreCount}"/>
                <fmt:param value="${model.statistics.songCount}"/>
                <fmt:param value="${model.statistics.videoCount}"/>
                <fmt:param value="${model.statistics.podcastCount}"/>
                <fmt:param value="${model.statistics.audiobookCount}"/>
                <fmt:param value="${model.bytes}"/>
                <fmt:param value="${model.hours}"/>
            </fmt:message>
        </div>
    </div>
    </c:if>


    <!--
    <c:if test="${fn:length(model.musicFolders) > 1}">
        <div style="padding-top:1em">
            <select name="musicFolderId" style="width:100%" onchange="location='left.view?musicFolderId=' + options[selectedIndex].value;" >
                <option value="-1"><fmt:message key="left.allfolders"/></option>
                <c:forEach items="${model.musicFolders}" var="musicFolder">
                    <option ${model.selectedMusicFolder.id == musicFolder.id ? "selected" : ""} value="${musicFolder.id}">${musicFolder.name}</option>
                </c:forEach>
            </select>
        </div>
    </c:if>
    -->
    <c:if test="${not model.ShowShortcuts}">    
        <c:if test="${not empty model.shortcuts}">    
        <span id="showShortcuts" style="display:inline;">
        <div class="forward" style="margin-top:10px;" ><a href="javascript:noop()"onclick="showShortcut()"><fmt:message key="left.showshortcuts"/></a></div></span>

        <span id="hideShortcuts" style="display:none;">
        <div class="forward" style="margin-top:10px;" ><a href="javascript:noop()"onclick="hideShortcut()"><fmt:message key="left.hideshortcuts"/></a></div></span>
        </c:if>
        <div id="shortcuts" style="display:none;">
    </c:if>
    
    <c:if test="${model.ShowShortcuts}">    
    <div id="shortcuts">
    </c:if>
    
    <c:if test="${not empty model.shortcuts}">
        <h2 class="bgcolor1"><fmt:message key="left.shortcut"/></h2>
        <c:forEach items="${model.shortcuts}" var="shortcut">
            <p class="dense" style="padding-left:0.5em">
                <madsonic:url value="main.view" var="mainUrl">
                    <madsonic:param name="id" value="${shortcut.id}"/>
                </madsonic:url>
                <a target="main" href="${mainUrl}">${shortcut.name}</a>
            </p>
        </c:forEach>
    </c:if>
    </div>

    <c:if test="${model.playlistEnabled}">    
        <h2 class="bgcolor1"><fmt:message key="left.playlists"/></h2> 
        <div id="playlistWrapper" style='padding-left:0.5em'>
            <div id="playlists"></div>
            <div id="playlistOverflow" style="display:none"></div>
            <div style="padding-top: 0.3em"/>
            <div id="showsomePlaylists" style="display: none"><a href="javascript:noop()" onclick="showsomePlaylists()"><fmt:message key="left.showsomeplaylists"/></a></div>
            <div id="showAllPlaylists" style="display: none"><a href="javascript:noop()" onclick="showAllPlaylists()"><fmt:message key="left.showallplaylists"/></a></div>
            <div id="hideAllPlaylists" style="display: none"><a href="javascript:noop()" onclick="hideAllPlaylists()"><fmt:message key="left.hideallplaylists"/></a></div>
            <div><a href="javascript:noop()" onclick="createEmptyPlaylist()"><fmt:message key="left.createplaylist"/></a></div>
            <div><a href="importPlaylist.view" target="main"><fmt:message key="left.importplaylist"/></a></div>
        </div>
    </c:if>

	<br>
	
<c:if test="${model.customAccordion}">
 <div id="accordion"> 
</c:if>

<c:forEach items="${model.indexedArtists}" var="entry">

<c:if test="${model.customAccordion}">
        <h3>
        <table width="100%" style="margin: 0 0 0 0;">
</c:if>
<c:if test="${not model.customAccordion}">
        <table class="bgcolor1" style="width:100%;padding:0;margin:1em 0 0 0;border:0">
</c:if>

            <tr style="padding:0;margin:0;border:0">
                <c:if test="${model.customScrollbar}">
                    <th style="text-align:left;padding:0;margin:0;border:0">
                    <c:choose>
                        <c:when test="${entry.key.index eq '#'}">
                            <a id="0"></a>
                        </c:when>
                        <c:when test="${entry.key.index eq '!'}">
                            <a id="1"></a>
                        </c:when>
                        <c:otherwise>
                            <a id="${entry.key.index}"></a> 
                        </c:otherwise>
                    </c:choose> 
                    <h2 style="padding:0;margin:0;border:0">${entry.key.index}</h2>
                    </th>
                </c:if>
        
                <c:if test="${not model.customScrollbar}">
                    <th style="text-align:left;padding:0;margin:0;border:0"><a id="${fn:escapeXml(entry.key.index)}" name="${fn:escapeXml(entry.key.index)}"></a>
                    <h2 style="padding:0;margin:0;border:0">${fn:escapeXml(entry.key.index)}</h2>
                    </th>
                </c:if>

                <th style="text-align:right;">
                    <c:if test="${not model.customScrollbar}">
                        <a href="#top"><img src="<spring:theme code="upImage"/>" alt=""></a>
                    </c:if>    
                    <c:if test="${model.customScrollbar}">
                        <a href="#" class="back_to_top"><img src="<spring:theme code="upImage"/>" alt=""></a>
                    </c:if>    
                </th>    
                </tr>
        </table>
        </h3>
        <div> 
            <c:forEach items="${entry.value}" var="artistEntry">
                <p class="dense" style="padding-left:0.5em">
                    <span title="${artistEntry.name}">
                        <madsonic:url value="main.view" var="mainUrl">
                            <c:choose>
                                <c:when test="${model.organizeByFolderStructure}">
                                    <c:forEach items="${artistEntry.mediaFiles}" var="mediaFile">
                                    <madsonic:param name="id" value="${mediaFile.id}"/>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="art" value="${artistEntry.artist}"/>
                                    <madsonic:param name="id" value="${art.name}"/>
                                    </c:otherwise>
                            </c:choose>
                        </madsonic:url>
                        <a target="main" href="${mainUrl}"><str:truncateNicely upper="${model.captionCutoff}">${artistEntry.name}</str:truncateNicely></a>
                    </span>
                </p>
            </c:forEach>
        </div>
    </c:forEach>

<c:if test="${model.customAccordion}">
</div>
</c:if>

    <div style="padding-top:1em"></div>

    <c:forEach items="${model.singleSongs}" var="song">
        <p class="dense" style="padding-left:0.5em">
            <span title="${song.title}">
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${song.id}"/>
                    <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode}"/>
                    <c:param name="addEnabled" value="${model.user.streamRole}"/>
                    <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode}"/>
                    <c:param name="video" value="${song.video and model.player.web}"/>
                </c:import>
                <str:truncateNicely upper="${model.captionCutoff}">${song.title}</str:truncateNicely>
            </span>
        </p>
    </c:forEach>

    <div style="height:5em"></div>

    <div class="bgcolor2" style="opacity: 1.0; clear: both; position: fixed; bottom: 0; right: 0; left: 0;
        padding: 0.25em 0.25em 0.25em 0.25em; border-top:1px dotted #DDDDDD; max-width: 640px;">

        <c:if test="${not model.customScrollbar}">
        <a href="#top">TOP</a>
        </c:if>    

        <c:if test="${model.customScrollbar}">
        <a class="back_to_top" href="#">TOP</a>

        <div id="anchor_list" style="display: inline">
        <c:forEach items="${model.indexes}" var="index">
                <c:choose>
                    <c:when test="${index.index eq '#'}">
                        <a href="#" lnk="0">${index.index}</a>
                    </c:when>
                    <c:when test="${index.index eq '!'}">
                        <a href="#" lnk="1">${index.index}</a>
                    </c:when>
                    <c:otherwise>
                        <a href="#" lnk="${index.index}">${index.index}</a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>    
        </c:if>    
            
        <c:if test="${not model.customScrollbar}">
            <div id="anchor_list" style="display: inline">
            <c:forEach items="${model.indexes}" var="index">
                <a href="#" lnk="${index.index}">${index.index}</a>
            </c:forEach>
            </div>    
        </c:if>    
            

    </div>
    <!-- CONTENT -->
</div>
 
<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
                $(window).load(function(){
            
                $("#content_leftpanel").mCustomScrollbar({
                axis:"y",
                scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                mouseWheel:true, /*mousewheel support: boolean*/
                mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                autoHideScrollbar:false, /*auto-hide scrollbar when idle*/       
                alwaysShowScrollbar:false,
                advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                scrollSpeed:"auto",      /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                scrollAmount:10 },       /*scroll buttons pixels scroll amount: integer (pixels)*/
                                theme:"${model.customScrollbarTheme}",
                                scrollbarPosition:"inside"
                });
                
                $("#content_leftpanel").mCustomScrollbar("update");
            });
        })(jQuery);

    $('#anchor_list>a').click(function(){
        var thisPos = $("#"+ $(this).attr("lnk")).position(); /* get the position of the corresponding paragraph */
        $("#content_leftpanel").mCustomScrollbar("scrollTo", thisPos.top -170 );  /* scroll to */
    });

    </script>    
</c:if>    

<c:if test="${not model.customScrollbar}">
	<script>
	$(".back_to_top").click(function() {
        $.scrollTo('0px', 800);
    });	
        
    $('#anchor_list>a').click(function(){
        var thisPos = $("#"+ $(this).attr("lnk")).position(); 
        $.scrollTo(thisPos.top -170 + 'px', 800);
    });        
	</script>
</c:if>	

<c:if test="${model.customScrollbar}">
    <script>
	$(".back_to_top").click(function() {
		$(".content_left").mCustomScrollbar("scrollTo","top"); 
	});
    </script>
</c:if>    

</body>
</html>