<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose> 


    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>	
    <script type="text/javascript" src="<c:url value="/dwr/interface/lovedTrackService.js"/>"></script>	
    <script type="text/javascript" src="<c:url value="/dwr/interface/playlistService.js"/>"></script>
	
    <script type="text/javascript" language="javascript">

    function init() {
    dwr.engine.setErrorHandler(null);
    
    $("#dialog-select-playlist").dialog({resizable: true, width:450, height: 350, modal: false, autoOpen: false,
        buttons: {
            "<fmt:message key="common.cancel"/>": function() {
                $(this).dialog("close");
            }
    }});
    }
    
    function toggleStar(mediaFileId, element) {
        starService.star(mediaFileId, !$(element).hasClass("fa-star"));
        $(element).toggleClass("fa-star fa-star-o starred");
    }    
   
    function toggleLoved(mediaFileId, element) {
        lovedTrackService.love(mediaFileId, !$(element).hasClass("fa-heart"));
        $(element).toggleClass("fa-heart fa-heart-o loved");
    }        

    function actionSelected(id) {
    
    if (id == "top") {
        return;
    } else if (id == "savePlaylist") {
        onSaveLovedPlaylist();			
    } else if (id == "selectAll") {
        selectAll(true);
    } else if (id == "selectNone") {
        selectAll(false);
    } else if (id == "appendPlaylist") {
        onAppendPlaylist();				
    } else if (id == "saveasPlaylist") {
        onSaveasPlaylist();				
    }        
    $("#moreActions").prop("selectedIndex", 0);
    }

    function getSelectedIndexes() {
        var result = "";
        for (var i = 0; i < ${fn:length(model.loved)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox != null  && checkbox.is(":checked")) {
                result += "i=" + i + "&";
            }
        }
        return result;
    }

    function selectAll(b) {
        for (var i = 0; i < ${fn:length(model.loved)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox != null) {
                if (b) {
                    checkbox.attr("checked", "checked");
                } else {
                    checkbox.removeAttr("checked");
                }
            }
        }
    }
    
    function onAppendPlaylist() {
        playlistService.getWritablePlaylists(playlistCallback);
    }
    
    function playlistCallback(playlists) {
    
        $("#dialog-select-playlist-list").empty();
        for (var i = 0; i < playlists.length; i++) {
            var playlist = playlists[i];
            $("<p class='dense'><b><a href='#' onclick='appendPlaylist(" + playlist.id + ")'>" + playlist.name + "</a></b></p>").appendTo("#dialog-select-playlist-list");
        }
        $("#dialog-select-playlist").dialog("open");
    }
    
    function appendPlaylist(playlistId) {
        $("#dialog-select-playlist").dialog("close");

        var mediaFileIds = new Array();
        
        for (var i = 0; i < ${fn:length(model.loved)}; i++) {
        
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        playlistService.appendToPlaylist(playlistId, mediaFileIds, function (){
        //parent.left.updatePlaylists();				
        $().toastmessage("showSuccessToast", "append to Playlist");				
        }); 
    }
    
    function onSavePlaylist() {
    
        selectAll(true);
        
        var mediaFileIds = new Array();
        
        for (var i = 0; i < ${fn:length(model.loved)}; i++) {
        
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        playlistService.savePlaylist(mediaFileIds, function (){
        //parent.left.updatePlaylists();
        $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
        });
    }


    function onSaveLovedPlaylist() {
    
        selectAll(true);
        var mediaFileIds = new Array();
        for (var i = 0; i < ${fn:length(model.loved)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        
    //    playlistService.saveLovedPlaylist(mediaFileIds, function (){
        // parent.left.updatePlaylists();
    //    $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
    //    });
        
        playlistService.createPlaylistForMain(mediaFileIds, null, function (playlistId) {
            // parent.left.updatePlaylists();
            parent.main.location.href = "playlist.view?id=" + playlistId;
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
        });        
        
    }

    function onSaveasPlaylist() {
    
        var mediaFileIds = new Array();
        for (var i = 0; i < ${fn:length(model.loved)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        playlistService.savePlaylist(mediaFileIds, function (){
        //parent.left.updatePlaylists();
        $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
        });
    }	
</script>    
</head>

<body class="mainframe bgcolor1" onload="init()"> 
 <div id="content_main" class="content_main"> 
 <div id="container" class="container">

<h1>
	<img src="<spring:theme code="starOnImage"/>" width="32" alt="">
	<fmt:message key="loved.title"/>
</h1>

<br> 
	<c:if test="${empty model.loved}">
	<p><em><fmt:message key="loved.empty"/></em></p>
	</c:if> 

        <h2>	
        <select id="moreActions" onchange="actionSelected(this.options[selectedIndex].id);" style="margin-bottom:1.0em">
            <option id="top" selected="selected"><fmt:message key="main.more"/></option>
            <optgroup label="all Loved Songs"/>
            <option id="savePlaylist"><fmt:message key="playlist.save"/></option>
            <optgroup label="<fmt:message key="main.more.selection"/>"/>
            <option id="selectAll"><fmt:message key="playlist.more.selectall"/></option>
            <option id="selectNone"><fmt:message key="playlist.more.selectnone"/></option>
            <option id="appendPlaylist"><fmt:message key="playlist.append"/></option>
            <option id="saveasPlaylist"><fmt:message key="playlist.save"/></option>
            </select>
        </h2> 
        <p>
        <i class="fa fa-chevron-right icon control"></i>&nbsp;<a href="loved.view?"><fmt:message key="common.refresh"/></a>
        </p>
        
        <h1 style="border-top: 1px dotted #666;"></h1>
        
		<table class="music" style="border-collapse:collapse;margin-top:5px;">
			<c:forEach items="${model.loved}" var="song" varStatus="loopStatus">

				<madsonic:url value="/main.view" var="mainUrl">
					<madsonic:param name="path" value="${song.parentPath}"/>
				</madsonic:url>
				<tr>
                
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:2px;">
					<c:import url="coverArtThumb.jsp">
						<c:param name="albumId" value="${song.id}"/>
						<c:param name="auth" value="${song.hash}"/>                        
						<c:param name="artistName" value="${song.name}"/>
						<c:param name="coverArtSize" value="50"/>
						<c:param name="scale" value="0.5"/>
						<c:param name="showLink" value="true"/>
						<c:param name="showZoom" value="false"/>
						<c:param name="showChange" value="false"/>
						<c:param name="showArtist" value="false"/>
						<c:param name="typArtist" value="true"/>
						<c:param name="appearAfter" value="5"/>
					</c:import>
				</td>
                
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0em">    
				
					<c:import url="playAddDownload.jsp">
						<c:param name="id" value="${song.id}"/>
						<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
						<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
						<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
						<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addContextVisible}"/>
						<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addNextVisible}"/>
						<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addLastVisible}"/>						
						<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
						<c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
                        <c:param name="loved" value="${not empty song.lovedDate}"/>
						<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
						<c:param name="starred" value="${not empty song.starredDate}"/>
						<c:param name="video" value="${song.video and model.player.web}"/>
						<c:param name="asTable" value="false"/>
					</c:import>
				</td>
               
                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:1em;padding-right:1.0em;max-width:15px;">
                    <input type="checkbox" class="checkbox" id="songIndex${loopStatus.count - 1}">
                    <span id="songId${loopStatus.count - 1}" style="display:none">${song.id}</span>
                </td>
                
                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-left:1em;padding-right:1em">
                        ${song.title}
                </td>
                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-right:1em">
                    <a href="${mainUrl}"><span class="detail">${song.albumName}</span></a>
                </td>
                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1em">
                    <span class="detail">${song.artist}</span>
                </td>
            </tr>
        </c:forEach>
    </table>
    
<div id="dialog-select-playlist" title="<fmt:message key="main.addtoplaylist.title"/>" style="display: none;">
    <p><fmt:message key="main.addtoplaylist.text"/></p>
    <div id="dialog-select-playlist-list"></div>
</div>    
    
<!-- CONTENT -->
  </div>
</div>
  
<c:if test="${model.customScrollbar}">

    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:600, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>  
</body>
</html>