<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%--@elvariable id="command" type="org.madsonic.command.MusicFolderTasksSettingsCommand"--%>
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <meta http-equiv="REFRESH" content="30;URL=musicFolderTasksSettings.view?">
 
    <c:choose>
        <c:when test="${customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>


    <link rel="stylesheet" media="all" type="text/css" href="<c:url value="/script/timepicker/jquery-ui-timepicker-addon.css"/>"/>
    <script type="text/javascript" src="<c:url value="/script/timepicker/jquery-ui-timepicker-addon.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/timepicker/jquery-ui-sliderAccess.js"/>"></script>
    <script type="text/javascript">
        function init() {
            <c:if test="${command.reload}">
            parent.frames.leftPanel.location.href="leftPanel.view?";
            parent.frames.right.location.href="right.view?";
            </c:if>
        }
    </script>
</head>

<body class="mainframe bgcolor1" onload="init()"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="musicFolderTasks"/>
    <c:param name="toast" value="${command.reload}"/>
</c:import>
<br>
<form:form commandName="command" action="musicFolderTasksSettings.view" method="post">
<table class="indent">
    <tr>
        <th><fmt:message key="musicfoldersettings.name"/></th>
        <th><fmt:message key="musicfoldersettings.path"/></th>
        <th style="padding-left:1em">Scan</th>        
        <th style="padding-left:1em">Interval</th>        
        <th style="padding-left:1em">Objects</th>        
        <th style="padding-left:1em">lastrun</th>        
        <th style="padding-left:1em">Status</th>        
        <th style="padding-left:1em">Result</th>        
        <th style="padding-left:1em">Comment</th>        
        <th></th>
    </tr>
    <c:forEach items="${command.musicFolders}" var="folder" varStatus="loopStatus">
        <tr>
            <td><form:input path="musicFolders[${loopStatus.count-1}].name" readonly="true" disabled="true" size="20"/></td>
            
            <td><form:input path="musicFolders[${loopStatus.count-1}].path" readonly="true" disabled="true" size="35"/></td>
            <td>                
            <fmt:formatDate value="${folder.timer}" type="both" pattern="yyy/MM/dd HH:mm" var="theFormattedDate" />
            <form:input path="musicFolders[${loopStatus.count-1}].timer" id="mf${loopStatus.count-1}" readonly="true" disabled="true"  value="${theFormattedDate}" size="16"/>
            </td> 
            <td>                
            <form:select path="musicFolders[${loopStatus.count-1}].interval" readonly="true" disabled="true">
                <fmt:message key="musicfoldersettings.interval.never" var="never"/>
                <fmt:message key="musicfoldersettings.interval.one" var="one"/>
                <form:option value="-1" label="${never}"/>
                <form:option value="300" label="5 Min."/>
                <form:option value="600" label="10 Min."/>
                <form:option value="1800" label="30 Min."/>
                <form:option value="3600" label="1 Hour"/>
                <form:option value="10800" label="3 Hour"/>                
                <form:option value="21600" label="6 Hour"/>
                <form:option value="43200" label="12 Hour"/>
                <form:option value="86400" label="1 Day"/>
                <form:option value="604800" label="1 Week"/>
                <form:option value="2419200" label="1 Month"/>
                <form:option value="7257600" label="3 Months"/>
                <form:option value="14515200" label="6 Months"/>
                <form:option value="29030400" label="1 Year"/>
            </form:select>
            </td> 
            <td> 
              <form:input path="musicFolders[${loopStatus.count-1}].found" id="mffo${loopStatus.count-1}" readonly="true" disabled="true"  cssClass="accent" size="8"/>
            </td> 
            <td> 
            <fmt:formatDate value="${folder.lastrun}" type="both" pattern="yyyy/MM/dd HH:mm" var="theFormattedDate" />
            <form:input path="musicFolders[${loopStatus.count-1}].lastrun" id="mflr${loopStatus.count-1}" readonly="true" disabled="true"  value="${theFormattedDate}" cssClass="accent" size="16"/>
            </td> 
            <td> 
              <form:input path="musicFolders[${loopStatus.count-1}].status" id="mfst${loopStatus.count-1}" readonly="true" disabled="true" cssClass="accent" size="15"/>
            </td> 
            <td> 
              <form:input path="musicFolders[${loopStatus.count-1}].result" id="mfrt${loopStatus.count-1}" readonly="true" disabled="true" cssClass="accent" size="20"/>
            </td> 
            <td> 
              <form:input path="musicFolders[${loopStatus.count-1}].comment" id="mfco${loopStatus.count-1}" readonly="true" disabled="true"  cssClass="accent" size="15"/>
            </td> 
        </tr>
    </c:forEach>
</table>
<br>
    <p>
    
    <div class="forward"><a href="musicFolderTasksSettings.view?"><fmt:message key="common.refresh"/></a></div>
    </p>    
</form:form>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${customScrollbar}">
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/     
                    alwaysShowScrollbar:true,                    
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);

</script>
</c:if>	

    <script type="text/javascript">
    $('#newMusicFolderTimer').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: "hh:mm", numberOfMonths: 2, stepMinute: 5, addSliderAccess: true, sliderAccessArgs: { touchonly: false } });
    <c:forEach items="${command.musicFolders}" var="folder" varStatus="loopStatus">
    $('#mf${loopStatus.count-1}').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: "hh:mm", numberOfMonths: 2,  stepMinute: 5, addSliderAccess: true, sliderAccessArgs: { touchonly: false } });
    </c:forEach>
    </script>    
</body>
</html>