<%--
  ~ This file is part of Madsonic.
  ~
  ~  Madsonic is free software: you can redistribute it and/or modify
  ~  it under the terms of the GNU General Public License as published by
  ~  the Free Software Foundation, either version 3 of the License, or
  ~  (at your option) any later version.
  ~
  ~  Madsonic is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~  Copyright 2015 (C) Sindre Mehus
  --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div style="float:right;padding-right:5px">
    <c:url value="podcastChannels.view" var="changeMixedViewUrl">
        <c:param name="viewAs" value="mixed"/>
    </c:url>
    <c:url value="podcastChannels.view" var="changeGridViewUrl">
        <c:param name="viewAs" value="grid"/>
    </c:url>
    <c:url value="podcastChannels.view" var="changeListViewUrl">
        <c:param name="viewAs" value="list"/>
    </c:url>    
    <c:choose>
        <c:when test="${model.viewAs eq 'list'}">
            <a href="${changeListViewUrl}"><img src="<spring:theme code="viewAsListImage"/>" class="viewSelected" alt="" style="margin-right:5px"/></a>
            <a href="${changeGridViewUrl}"><img src="<spring:theme code="viewAsGridImage"/>" alt="" style="margin-right:5px"/></a>
            <a href="${changeMixedViewUrl}"><img src="<spring:theme code="viewAsMixedImage"/>" alt=""/></a>
        </c:when>
        <c:when test="${model.viewAs eq 'grid'}">
            <a href="${changeListViewUrl}"><img src="<spring:theme code="viewAsListImage"/>" alt="" style="margin-right:5px"/></a>
            <a href="${changeGridViewUrl}"><img src="<spring:theme code="viewAsGridImage"/>" class="viewSelected" alt="" style="margin-right:5px"/></a>
            <a href="${changeMixedViewUrl}"><img src="<spring:theme code="viewAsMixedImage"/>" alt=""/></a>
        </c:when>
        <c:when test="${model.viewAs eq 'mixed'}">
            <a href="${changeListViewUrl}"><img src="<spring:theme code="viewAsListImage"/>" alt="" style="margin-right:5px"/></a>
            <a href="${changeGridViewUrl}"><img src="<spring:theme code="viewAsGridImage"/>" alt="" style="margin-right:5px"/></a>
            <a href="${changeMixedViewUrl}"><img src="<spring:theme code="viewAsMixedImage"/>" class="viewSelected" alt=""/></a>
        </c:when>
    </c:choose>
  
</div>
