<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<html>
<head>
    <%@ include file="head.jsp" %>
    <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">    
</head>
<body class="mainframe bgcolor1">

<div id="content_main" class="content_main">

    <h1>
        <img src="<spring:theme code="logoffImage"/>" alt=""/>
        <fmt:message key="accessDenied.title"/>
    </h1>

    <p style="padding:20px;">
        <fmt:message key="accessDenied.text"/>
    </p>

    <div class="back"><a href="javascript:history.go(-1)"><fmt:message key="common.back"/></a></div>

</div>
</body>
</html>