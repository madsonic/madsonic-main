<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--@elvariable id="model" type="java.util.Map"--%>

<html><head>
<%@ include file="head.jsp" %>
<%@ include file="jquery.jsp" %>
<%@ include file="customScrollbar.jsp" %>  

<c:if test="${not model.updateNowPlaying}">
    <meta http-equiv="refresh" content="180;URL=nowPlaying.view?">
</c:if>
	<script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/lovedTrackService.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/playlistService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>  
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-1.4.2.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.dropdownReplacement-0.5.3.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/fancyzoom/FancyZoom.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/fancyzoom/FancyZoomHTML.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/lightview/lightview.js"/>"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/style/lightview/lightview.css"/>"></script>
    
	<style type="text/css">
	    .coverart { float: left; padding-left:0px; padding-right:5px; padding-bottom:10px }
		
		.maincontrol .fa {
			padding: 2px;
		}
		
	</style>
    <link rel="stylesheet" href="<c:url value="/style/jquery.dropdownReplacement.css"/>" type="text/css">
</head>

<body class="mainframe bgcolor1" onload="init();"> 	<!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<madsonic:url value="createShare.view" var="shareUrl">
    <madsonic:param name="id" value="${model.dir.id}"/>
</madsonic:url>
<madsonic:url value="download.view" var="downloadUrl">
    <madsonic:param name="id" value="${model.dir.id}"/>
</madsonic:url>
<madsonic:url value="appendPlaylist.view" var="appendPlaylistUrl">
    <madsonic:param name="id" value="${model.dir.id}"/>
</madsonic:url>


<c:if test="${model.dir.album}">
	<script type="text/javascript">
    $(document).ready(function() { 
        $("#moreActions").dropdownReplacement({selectCssWidth: 190, optionsDisplayNum: 10, onSelect : function(value, text, selectIndex){ actionSelected(value); }});
    });
    </script>
</c:if>                                

<script type="text/javascript" language="javascript">
    function init() {

        setupZoom('<c:url value="/"/>');

        $("#dialog-select-playlist").dialog({resizable: true, width:450, height:400, modal: false, autoOpen: false,
            buttons: {
                "<fmt:message key="common.cancel"/>": function() {
                    $(this).dialog("close");
                }
            }});
            
        RefreshMediaType();

        <c:if test="${model.dir.mediaType ne 'ALBUM'}">        
            <c:if test="${model.showArtistInfo}">
            loadArtistInfo();
            </c:if>
        </c:if>
        
        <c:if test="${model.dir.mediaType eq 'ALBUM'}">
            <c:if test="${model.showArtistInfo}">
                loadAlbumInfo();
            </c:if>
        </c:if>
        
        loadArtistTopTracks();
        
    }

      function loadArtistInfo() {
            multiService.getMainArtistInfo('${model.dir.id}', 8, function (artistInfo) {
           
                if (artistInfo.similarArtists.length > 0) {
                    var html = "";
                    for (var i = 0; i < artistInfo.similarArtists.length; i++) {
                        html += "<a href='main.view?id=" + artistInfo.similarArtists[i].mediaFileId + "' target='main'>" +
                                escapeHtml(artistInfo.similarArtists[i].artistName) + "</a>";
                        if (i < artistInfo.similarArtists.length - 1) {
                            html += " <span class='similar-artist-divider'><img src='icons/default/sep.png'></span> ";
                        }
                    }
                    $("#similarArtists").append(html);
                    $("#similarArtists").show();
                    $("#similarArtistsTitle").show();
                    $("#similarArtistsRadio").show();
                    $("#placeholder").show();
                }

                if (artistInfo.artistBio && artistInfo.artistBio.biography) {
                $("#artistBio").append("<img src='<c:url value='/icons/default/artist.png'/>' style='width:16px;padding-right:4px'>");
                $("#artistBio").append(artistInfo.artistBio.biography);
                $("#artistBio").show();
                    
                    if (artistInfo.artistBio.mediumImageUrl) {
                        $("#artistImage").attr("src", artistInfo.artistBio.largeImageUrl);
                        $("#artistImageZoom").attr("href", artistInfo.artistBio.largeImageUrl);                        
                        $("#artistImage").attr("src", artistInfo.artistBio.smallImageUrl);
                        $("#artistImage").show();
                    }
                }
            });
        }   

    function loadAlbumInfo() {
    	
        multiService.getAlbumInfo('${model.dir.id}', 8, function (albumInfo) {
        	
            var artistInfo = albumInfo.artistInfo;
            
                if (artistInfo.similarArtists.length > 0) {
                    var html = "";
                    for (var i = 0; i < artistInfo.similarArtists.length; i++) {
                        html += "<a href='main.view?id=" + artistInfo.similarArtists[i].mediaFileId + "' target='main'>" +
                                escapeHtml(artistInfo.similarArtists[i].artistName) + "</a>";
                        if (i < artistInfo.similarArtists.length - 1) {
                            html += " <span class='similar-artist-divider'><img src='icons/default/sep.png'></span> ";
                        }
                    }
                    $("#similarArtists").append(html);
                    $("#similarArtists").show();
                    $("#similarArtistsTitle").show();
                    $("#similarArtistsRadio").show();
                    $("#placeholder").show();
                }            
            
            if (artistInfo.artistBio && artistInfo.artistBio.biography) {
            $("#artistBio").append("<img src='<c:url value='/icons/default/artist.png'/>' style='width:16px;padding-right:4px'>");
            $("#artistBio").append(artistInfo.artistBio.biography);
            $("#artistBio").show();
                
                if (artistInfo.artistBio.mediumImageUrl) {
                    $("#artistImage").attr("src", artistInfo.artistBio.largeImageUrl);
                    $("#artistImageZoom").attr("href", artistInfo.artistBio.largeImageUrl);                        
                    $("#artistImage").attr("src", artistInfo.artistBio.smallImageUrl);
                    $("#artistImage").show();
                }
            }            
            
            if (albumInfo.notes) {
                $("#albumBio").append("<img src='<c:url value='/icons/default/home.png'/>' style='width:16px;padding-right:4px'>");
                $("#albumBio").append(albumInfo.notes);
                $("#albumBio").show();
            }
        });
    }

    function loadArtistTopTracks() {
        multiService.getTopTrackInfo('${model.dir.id}', function (topTrackInfo) {
                if (topTrackInfo != null) {
                    $("#topTrackResult").append("(" + topTrackInfo.topPlayedfound + "|" + topTrackInfo.toplastFMfound + ")");
                    $("#playTopTrack").stop(1,1).fadeIn(500);
                    $("#addTopTrack").stop(1,1).fadeIn(500);
                }
            });
        }    
    
    function actionSelected(id) {
        var selectedIndexes = getSelectedIndexes();

        if (id == "top") {
            return;
        } else if (id == "selectAll") {
            selectAll(true);
        } else if (id == "selectNone") {
            selectAll(false);
        } else if (id == "share" && selectedIndexes != "") {
            parent.frames.main.location.href = "${shareUrl}&" + selectedIndexes;
        } else if (id == "download" && selectedIndexes != "") {
            location.href = "${downloadUrl}&" + getSelectedIndexes();
        } else if (id == "addNext" && selectedIndexes != "") {
            onAddSelectedNext();
        } else if (id == "addLast" && selectedIndexes != "") {
            onAddSelectedLast();
        } else if (id == "appendPlaylist" && selectedIndexes != "") {
            onAppendPlaylist();
        } else if (id == "savePlaylist") {
            onSavePlaylist();
    } else if (id == "savePlaylistNamed") {
            onSavePlaylistNamed();
        }
        $("#moreActions").prop("selectedIndex", 0);
    }

    function getSelectedIndexes() {
        var result = "";
        for (var i = 0; i < ${fn:length(model.children)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox != null  && checkbox.is(":checked")) {
                result += "i=" + i + "&";
            }
        }
        return result;
    }

    function selectAll(b) {
        for (var i = 0; i < ${fn:length(model.children)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox != null) {
                if (b) {
                    checkbox.attr("checked", "checked");
                } else {
                    checkbox.removeAttr("checked");
                }
            }
        }
    }

    function toggleStar(mediaFileId, element) {
        starService.star(mediaFileId, !$(element).hasClass("fa-star"));
        $(element).toggleClass("fa-star fa-star-o starred");
    }    
   
    function toggleLoved(mediaFileId, element) {
        lovedTrackService.love(mediaFileId, !$(element).hasClass("fa-heart"));
        $(element).toggleClass("fa-heart fa-heart-o loved");
    }
   
    function onAppendPlaylist() {
        playlistService.getWritablePlaylists(playlistCallback);
    }
    
    function playlistCallback(playlists) {
        $("#dialog-select-playlist-list").empty();
        for (var i = 0; i < playlists.length; i++) {
            var playlist = playlists[i];
            $("<p class='dense'><b><a href='#' onclick='appendPlaylist(" + playlist.id + ")'>" + playlist.name + "</a></b></p>").appendTo("#dialog-select-playlist-list");
        }
        $("#dialog-select-playlist").dialog("open");
    }
    
    function appendPlaylist(playlistId) {
        $("#dialog-select-playlist").dialog("close");

        var mediaFileIds = new Array();
        for (var i = 0; i < ${fn:length(model.children)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        playlistService.appendToPlaylist(playlistId, mediaFileIds, function (){});
        parent.main.location.href = "playlist.view?id=" + playlistId;
    }

    // --------------------------------------------
    function onSavePlaylist() {
        selectAll(true);
        var mediaFileIds = new Array();
        for (var i = 0; i < "${fn:length(model.children)}"; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        
        playlistService.createPlaylistForMain(mediaFileIds, null, function (playlistId) {
            // parent.left.updatePlaylists();
            parent.main.location.href = "playlist.view?id=" + playlistId;
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
        });  
    }

    // --------------------------------------------
    function onSavePlaylistNamed() {
    
        selectAll(true);
        var mediaFileIds = new Array();
        var PlaylistName = "${model.artist} - ${model.album}";
        for (var i = 0; i < ${fn:length(model.children)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        
        playlistService.createPlaylistForMain(mediaFileIds, PlaylistName, function (playlistId) {
            // parent.left.updatePlaylists();
            parent.main.location.href = "playlist.view?id=" + playlistId;
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
        });        
    }    

    // --------------------------------------------
    function onAddSelectedNext() {
        var mediaFileIds = new Array();
        for (var i = 0; i < ${fn:length(model.children)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        parent.playQueue.onAddSelectedNext(mediaFileIds);
        $().toastmessage("showSuccessToast", "added next to PlayQueue");
    }    

    // --------------------------------------------
    function onAddSelectedLast() {
        var mediaFileIds = new Array();
        for (var i = 0; i < ${fn:length(model.children)}; i++) {
            var checkbox = $("#songIndex" + i);
            if (checkbox && checkbox.is(":checked")) {
                mediaFileIds.push($("#songId" + i).html());
            }
        }
        parent.playQueue.onAddSelectedLast(mediaFileIds);
        $().toastmessage("showSuccessToast", "added last to PlayQueue");
    }        
    
    // --------------------------------------------
    function onPlayAll() {
    
        var mediaFileIds = new Array();
        ${model.ids}.forEach(function(entry) {
        mediaFileIds.push(entry);
        });
        parent.playQueue.onPlayAll(mediaFileIds);
        $().toastmessage("showSuccessToast", "Play all entries");
    }    

    // --------------------------------------------
    function onAddAll() {
    
        var mediaFileIds = new Array();
        ${model.ids}.forEach(function(entry) {
        mediaFileIds.push(entry);
        });
        parent.playQueue.onAddAll(mediaFileIds);
        $().toastmessage("showSuccessToast", "Add all entries");
    }  
    

    // --------------------------------------------
    function onPlayRandom() {
    
        var mediaFileIds = new Array();
        ${model.ids}.forEach(function(entry) {
        mediaFileIds.push(entry);
        });
        
     // parent.playQueue.onAddAll(mediaFileIds);
        parent.playQueue.onPlayRandomAll(mediaFileIds, 30);
        $().toastmessage("showSuccessToast", "Play Random");
    }  

    // --------------------------------------------
    function PlayTopTrack() {
    
        var mediaFileIds = new Array();
        ${model.ids}.forEach(function(entry) {
        mediaFileIds.push(entry);
        });
        parent.playQueue.onPlayTopTrack("${model.artist}");
        $().toastmessage("showSuccessToast", "Play ${model.topPlayedfound} Top-Tracks");
    }  
    
    // --------------------------------------------
    function AddTopTrack() {
    
        var mediaFileIds = new Array();
        ${model.ids}.forEach(function(entry) {
        mediaFileIds.push(entry);
        });
        parent.playQueue.onAddTopTrack("${model.artist}");
        $().toastmessage("showSuccessToast", "Add ${model.topPlayedfound} Top-Tracks");
    }      

    
    function playSimilar() {
        parent.playQueue.onPlaySimilar(${model.dir.id}, 50);
        $().toastmessage("showSuccessToast", "Play Similar Artist");
    }    
    

    // --------------------------------------------
    function onToastmessage(type, text) {
        $().toastmessage(type, text);
    }    


    // --------------------------------------------
    function showAllAlbums() {
        $("#showAllButton").hide();
        $(".coverart").show();
    }
    
function RefreshMediaType() {

//    console.log("--RefreshGUI--");
    var mediatype = document.getElementsByName("mediatype");
    for (var i=0; i < mediatype.length; i++) {
        if (mediatype[i].checked == true) {
    //        console.log("The " + (i + 1) + ". radio button is checked");
        }
    }    
    if ("${model.dir.mediaType}" == "MULTIARTIST") { mediatype[0].checked=true; }
    if ("${model.dir.mediaType}" == "ARTIST")        { mediatype[1].checked=true; }
    if ("${model.dir.mediaType}" == "DIRECTORY")   { mediatype[2].checked=true; }
    if ("${model.dir.mediaType}" == "VIDEOSET")    { mediatype[3].checked=true; }
    if ("${model.dir.mediaType}" == "ALBUMSET")    { mediatype[4].checked=true; }
    if ("${model.dir.mediaType}" == "ALBUM")       { mediatype[5].checked=true; }
    if ("${model.dir.mediaTypeOverride}"=="false") { mediatype[6].checked=true; }
    }
</script>

<c:if test="${model.updateNowPlaying}">
    <script type="text/javascript" language="javascript">
        // Variable used by javascript in playlist.jsp
        var updateNowPlaying = true;
    </script>
</c:if>

    <h1>

    <a id="artistImageZoom" rel="zoom" href="javascript:void(0)">
        <img id="artistImage" class="circle" alt="" src="<c:url value="/icons/default/default_artist_alpha.png"/>" style="max-width:48px; max-height:48px">
    </a>   
    

    <span class="icon-wrapper">
    <i class="fa ${not empty model.dir.starredDate ? 'fa-star starred' : 'fa-star-o'} clickable custom-icon-header" onclick="toggleStar(${param.id}, this)"title="<fmt:message key="common.star"/>"><span class="fix-editor">&nbsp;</span></i></span>
     
    <c:forEach items="${model.ancestors}" var="ancestor">
        <madsonic:url value="main.view" var="ancestorUrl">
            <madsonic:param name="id" value="${ancestor.id}"/>
        </madsonic:url>
        
    <c:choose>
        <c:when test="${fn:startsWith(ancestor.name,'[')}">
            <a href="${ancestorUrl}">${fn:split(ancestor.name,']')[1]}</a> &raquo;
        </c:when>
        <c:otherwise>
            <a href="${ancestorUrl}">${ancestor.name}</a> &raquo;
        </c:otherwise>
    </c:choose>        
    </c:forEach>

    <c:choose>
        <c:when test="${not empty model.dir.albumSetName}">
            ${model.dir.albumSetName}
        </c:when>
        <c:otherwise>
            ${model.dir.name}
        </c:otherwise>
    </c:choose>        

    <c:if test="${model.dir.album and model.averageRating gt 0}">
    <div style="display:inline;">
    <span display="inline" class="detail" style="padding-left:1.5em;"}>
        <c:import url="rating.jsp">
            <c:param name="id" value="${model.dir.id}"/>
            <c:param name="path" value="${model.dir.path}"/>
            <c:param name="readonly" value="true"/>
            <c:param name="rating" value="${model.averageRating}"/>
        </c:import>
        </span>        
        </div>
    </c:if>
    </h1>

<c:if test="${not model.partyMode}">
<h2>
<div class="maincontrol">
    <c:if test="${model.navigateUpAllowed}">
        <madsonic:url value="main.view" var="upUrl">
            <madsonic:param name="id" value="${model.parent.id}"/>
        </madsonic:url>
        <a href="${upUrl}"> <i class="fa fa-arrow-up accent"></i> <fmt:message key="main.up"/></a>
        <c:set var="needSep" value="true"/>
    </c:if>
    
    <c:if test="${model.user.streamRole}">
        <c:if test="${needSep}"> <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;"></c:if> 
        
        <a href="#" onclick="onPlayAll();"> <i class="fa fa-play accent"></i> <fmt:message key="main.playall"/></a> 
        
        <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
        <a href="#" onclick="onPlayRandom()"> <i class="fa fa-random accent"></i> <fmt:message key="main.playrandom"/> </a>

        <c:if test="${model.lastFMTopTrackSearch}">
            <c:if test="${model.user.lastFMRole}">
                <c:if test="${not model.dir.album}">
                    <c:if test="${model.dir.mediaType ne 'VIDEOSET'}">
                    <span id="playTopTrack" style="display:none;">
                    <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
                    <a href="#" onclick="PlayTopTrack()"> <i class="fa fa-play accent"></i> <fmt:message key="main.playtoptrack"/> 
                    <span style="font-size:80% !important;" id="topTrackResult"></span> 
                    </a> <a href="#" onclick="parent.playQueue.onUpdateTopTrack(&quot;${model.artist}&quot;,${model.dir.id});"><i class="fa fa-refresh accent"></i></a>
                    </span>
                    </c:if>
                </c:if>
            </c:if>
        </c:if>
    <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">

    <a "similarArtistsRadio" href="javascript:playSimilar()"> <i class="fa fa-play accent"></i> <fmt:message key="main.playradio"/> <span style="font-size:75% !important;"></span></a>
    <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
    <a href="#" onclick="onAddAll()"> <i class="fa fa-plus-circle accent"></i> <fmt:message key="main.addall"/></a>
    
        <c:if test="${model.lastFMTopTrackSearch}">
            <c:if test="${model.user.lastFMRole}">
                <c:if test="${not model.dir.album}">
                    <c:if test="${model.dir.mediaType ne 'VIDEOSET'}">
                    <span id="addTopTrack" style="display:none;">
                    <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
                    <a href="#" onclick="AddTopTrack()"> <i class="fa fa-plus-circle accent"></i> <fmt:message key="main.addtoptrack"/></a>
                    </span>
                    </c:if>
                </c:if>
            </c:if>
        </c:if>
        <c:set var="needSep" value="true"/>        
    </c:if>

    <c:if test="${model.dir.album}">

        <c:if test="${model.user.downloadRole}">
            <madsonic:url value="download.view" var="downloadUrl">
                <madsonic:param name="id" value="${model.dir.id}"/>
            </madsonic:url>
            <c:if test="${needSep}"> <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;"></c:if>
            <a href="${downloadUrl}"> <i class="fa fa-download accent"></i> <fmt:message key="common.download"/> </a>
            <c:set var="needSep" value="true"/>
        </c:if>

        <c:if test="${model.user.coverArtRole}">
            <madsonic:url value="editTags.view" var="editTagsUrl">
                <madsonic:param name="id" value="${model.dir.id}"/>
            </madsonic:url>
            <c:if test="${needSep}"><img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;"></c:if>
            <a href="${editTagsUrl}"> <i class="fa fa-code accent"></i> <fmt:message key="main.tags"/> </a>
            <c:set var="needSep" value="true"/>
        </c:if>

    </c:if>
	
    <c:if test="${model.user.commentRole}">
        <c:if test="${needSep}"><img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;"></c:if>
        <a href="javascript:toggleComment()"> <i class="fa fa-pencil-square-o accent"></i> <fmt:message key="main.comment"/></a>
    <c:set var="needSep" value="true"/>
    </c:if>

    <c:if test="${model.showCreateLink}">    
        <c:if test="${model.user.adminRole}">
            <c:if test="${needSep}"><img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;"></c:if>
            <a href="javascript:toggleCreateURL()"> <i class="fa fa-link accent"></i>  <fmt:message key="main.createlink"/></a>
        </c:if>
    </c:if>
    
    <c:if test="${model.showMediaType}">       
        <c:if test="${model.user.coverArtRole}">
            <c:if test="${needSep}"><img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;"></c:if>
            <a href="javascript:toggleMediaType()"> <i class="fa fa-bars accent"></i>  <fmt:message key="main.mediatype"/></a>
        </c:if>
    </c:if>

    <c:if test="${model.user.shareRole and model.dir.mediaType ne 'ALBUMSET' and model.dir.mediaType ne 'ARTIST'}">
        <c:if test="${needSep}"><img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;"></c:if>
			<a href="${shareUrl}"> <i class="fa fa-share accent"></i> <fmt:message key="main.sharealbum"/></a>
		<c:set var="needSep" value="true"/>
		
    </c:if>	
</div>
</h2>

</c:if>
<c:if test="${not model.dir.album}">
<c:if test="${model.user.searchRole}">

        <div class="detail">
        <div class="maincontrol">
            <madsonic:url value="http://www.google.com/search" var="googleUrl" encoding="UTF-8">
                <madsonic:param name="q" value="\"${model.dir.name}\""/>
            </madsonic:url>
            <madsonic:url value="http://en.wikipedia.org/wiki/Special:Search" var="wikipediaUrl" encoding="UTF-8">
                <madsonic:param name="search" value="\"${model.dir.name}\""/>
                <madsonic:param name="go" value="Go"/>
            </madsonic:url>
            <madsonic:url value="http://www.discogs.com/search" var="discogsUrl" encoding="UTF-8">
                <madsonic:param name="q" value="${model.dir.name}"/>
                <madsonic:param name="type" value="artist"/>                
            </madsonic:url>
            <madsonic:url value="http://www.allmusic.com/search/artists/${model.dir.name}" var="allmusicUrl">
            </madsonic:url>
            <madsonic:url value="http://www.musik-sammler.de/search/${model.dir.name}/" var="MusiksammlerUrl" encoding="UTF-8">
                <madsonic:param name="type" value="artist"/>  
            </madsonic:url>
            <madsonic:url value="http://www.laut.de/Suche" var="lautUrl" encoding="UTF-8">
                <madsonic:param name="q" value="${model.dir.name}"/>
            </madsonic:url>
            <madsonic:url value="http://www.last.fm/search" var="lastFmUrl" encoding="UTF-8">
                <madsonic:param name="q" value="\"${model.dir.name}\""/>
                <madsonic:param name="type" value="artist"/>
            </madsonic:url>
            <madsonic:url value="http://musicbrainz.org/search" var="musicbrainzUrl" encoding="UTF-8">
                <madsonic:param name="query" value="${model.dir.name}"/>
                <madsonic:param name="type" value="artist"/>
            </madsonic:url>            
            <madsonic:url value="http://www.youtube.com/results" var="YoutubeUrl" encoding="UTF-8">
                <madsonic:param name="search_query" value="${model.dir.name}"/>
            </madsonic:url>
            
            <span display="inline" class="detailcolor" style="margin-left: 10px;"><fmt:message key="common.search"/></span> 

            <a target="_blank" href="${googleUrl}"> <i class="fa fa-google accent" title="Search with Google"></i> Google</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${wikipediaUrl}"> <i class="fa fa-wikipedia-w accent" title="Search with Wikipedia"></i> Wikipedia </a>
            <img src="<spring:theme code="sepImage"/>" alt="">
    
            <a target="_blank" href="${discogsUrl}"> <i class="fa fa-dot-circle-o accent" title="Search with Discogs"></i> Discogs</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${musicbrainzUrl}"> <i class="fa fa-music accent" title="Search with MusicBrainz"></i> MusicBrainz</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${lastFmUrl}"> <i class="fa fa-lastfm accent" title="Search with Last.FM"></i> Last.FM</a>
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">

            <a target="_blank" href="${YoutubeUrl}"> <i class="fa fa-youtube-play accent" title="Search with Youtube"></i> Youtube</a>            
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${allmusicUrl}"><img src="<spring:theme code="allmusicImage"/>" alt="" title="Search with Allmusic"> Allmusic</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${MusiksammlerUrl}"> <i class="fa fa-search accent" title="Search with MusikSammler"></i> MusikSammler</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${lautUrl}"><img src="<spring:theme code="lautImage"/>" alt="" title="Search with Laut"> Laut</a>  
    </div>
</div>
</c:if>
</c:if>

<c:if test="${model.dir.album}">

    <c:if test="${model.user.searchRole}">

    <div class="detail">
        <div class="maincontrol" style="display:inline;padding-left:2px">
        
        <c:if test="${not empty model.artist or not empty model.album}">
            <madsonic:url value="http://www.google.com/search" var="googleUrl" encoding="UTF-8">
                <madsonic:param name="q" value="\"${model.artist}\" \"${model.album}\""/>
            </madsonic:url>
            <madsonic:url value="http://en.wikipedia.org/wiki/Special:Search" var="wikipediaUrl" encoding="UTF-8">
                <madsonic:param name="profile" value="default"/>
                <madsonic:param name="search" value="album +${model.artist} +${model.album}"/>
                <madsonic:param name="fulltext" value="Search"/>
                <madsonic:param name="go" value="Go"/>
            </madsonic:url>
            <madsonic:url value="http://www.discogs.com/search" var="discogsUrl" encoding="UTF-8">
                <madsonic:param name="q" value="${model.album}"/>
                <madsonic:param name="type" value="release"/>                
            </madsonic:url>
            <madsonic:url value="http://www.allmusic.com/search/albums/${model.album}" var="allmusicUrl">
            </madsonic:url>            
            <madsonic:url value="http://www.musik-sammler.de/search/${model.album}/" var="MusiksammlerUrl" encoding="UTF-8">
                <madsonic:param name="type" value="album"/>
            </madsonic:url>
            <madsonic:url value="http://www.laut.de/Suche" var="lautUrl" encoding="UTF-8">
                <madsonic:param name="q" value="${model.artist} ${model.album}"/>
            </madsonic:url>
            <madsonic:url value="http://www.last.fm/search" var="lastFmUrl" encoding="UTF-8">
                <madsonic:param name="q" value="\"${model.artist}\" \"${model.album}\""/>
                <madsonic:param name="type" value="album"/>
            </madsonic:url>
            <madsonic:url value="http://musicbrainz.org/search" var="musicbrainzUrl" encoding="UTF-8">
                <madsonic:param name="query" value="${model.album}"/>
                <madsonic:param name="type" value="release"/>
            </madsonic:url>            
            <madsonic:url value="http://www.youtube.com/results" var="YoutubeUrl" encoding="UTF-8">
                <madsonic:param name="search_query" value="${model.artist} ${model.album}"/>
            </madsonic:url>
            <span display="inline" class="detailcolor"><fmt:message key="common.search"/></span> 
            <a target="_blank" href="${googleUrl}"> <i class="fa fa-google accent" title="Search with Google"></i> Google</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
			
            <a target="_blank" href="${wikipediaUrl}"> <i class="fa fa-wikipedia-w accent" title="Search with Wikipedia"></i> Wikipedia </a>
            <img src="<spring:theme code="sepImage"/>" alt="">
    
            <a target="_blank" href="${discogsUrl}"> <i class="fa fa-dot-circle-o accent" title="Search with Discogs"></i> Discogs</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${musicbrainzUrl}"> <i class="fa fa-music accent" title="Search with MusicBrainz"></i> MusicBrainz</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${lastFmUrl}"> <i class="fa fa-lastfm accent" title="Search with Last.FM"></i> Last.FM</a>
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">

            <a target="_blank" href="${YoutubeUrl}"> <i class="fa fa-youtube-play accent" title="Search with Youtube"></i> Youtube</a>            
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${allmusicUrl}"><img src="<spring:theme code="allmusicImage"/>" alt="" title="Search with Allmusic"> Allmusic</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${MusiksammlerUrl}"> <i class="fa fa-search accent" title="Search with MusikSammler"></i> MusikSammler</a> 
            <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;">
            
            <a target="_blank" href="${lautUrl}"><img src="<spring:theme code="lautImage"/>" alt="" title="Search with Laut"> Laut</a> 			
            
        </c:if>
    </div>
	<!--
    <c:if test="${model.user.shareRole and model.dir.mediaType ne 'ALBUMSET'}">
        <img src="<spring:theme code="sepImage"/>" style="margin-left: 5px;margin-right: 5px;">
        <a href="${shareUrl}"><span class="detailcolor" style="margin-right: 5px;"><fmt:message key="main.sharealbum"/></span></a> 
        <a href="${shareUrl}"><img src="<spring:theme code="shareTwitterImage"/>" alt=""></a>
        <a href="${shareUrl}"><img src="<spring:theme code="shareFacebookImage"/>" alt=""></a>
        <a href="${shareUrl}"><img src="<spring:theme code="shareGooglePlusImage"/>" alt=""></a>
    </c:if>
	-->
    </div>
    </c:if>
    
<div style="padding-top:0.8em;padding-bottom:1.0em;">
    
    <select id="moreActions" name="moreActions" style="display:none">
        <option value="top" selected="selected"><fmt:message key="main.more"/></option>
        <option value="savePlaylist">Save as Playlist (Date)</option>
        <option value="savePlaylistNamed">Save as Playlist (Named)</option>
        <option value="selectAll"><fmt:message key="playlist.more.selectall"/></option>
        <option value="selectNone"><fmt:message key="playlist.more.selectnone"/></option>
        <c:if test="${model.user.shareRole}">
        <option value="share"><fmt:message key="main.more.share"/></option>
        </c:if>
        <c:if test="${model.user.downloadRole}">
        <option value="download"><fmt:message key="common.download"/></option>
        </c:if>
        <option value="appendPlaylist"><fmt:message key="playlist.append"/></option>
        <option value="addNext">Add Next to PlayQueue</option>
        <option value="addLast">Add Last to PlayQueue</option>
    </select>

    <c:if test="${model.user.commentRole}">
    <span display="inline" class="detail" style="padding-left:1.5em;vertical-align: -webkit-baseline-middle;">
        <c:import url="rating.jsp">
            <c:param name="id" value="${model.dir.id}"/>
            <c:param name="path" value="${model.dir.path}"/>
            <c:param name="readonly" value="false"/>
            <c:param name="rating" value="${model.userRating}"/>
        </c:import>
        </span>
    </c:if>
    
    <c:if test="${model.user.commentRole}">
    <span display="inline" class="detail" style="padding-left:1.5em;vertical-align: -webkit-baseline-middle;">    
        <c:import url="hot.jsp">
            <c:param name="id" value="${model.dir.id}"/>
            <c:param name="flag" value="${model.hotRating}"/>
        </c:import>
        </span>
    </c:if>        
</div>

<span display="inline" class="detailcolor" style="padding-left:2px;margin-top:20px">

<fmt:message key="main.playcount"><fmt:param value="${model.dir.playCount}"/></fmt:message>
<c:if test="${not empty model.dir.lastPlayed}">
    <fmt:message key="main.lastplayed">
        <fmt:param><fmt:formatDate type="date" dateStyle="long" value="${model.dir.lastPlayed}"/></fmt:param>
    </fmt:message>
</c:if>
</span>
    
</c:if>

<div id="createURLForm" display="inline" class="bgcolor2 detailcolor" style="display:none;margin-top:20px">
    <form method="post" action="createURL.view">
        <input type="hidden" name="action" value="create">
        <input type="hidden" name="id" value="${model.dir.id}">    
        https://www.youtube.com/watch?v=
        <input name="URL" size="15" value="mcixldqDIEQ">
        <input name="title" size="35" value="Example YouTube Video">
        <input type="submit" value="Create Link">
    </form>
</div>

<div id="MediaTypeSwitch" class="bgcolor2" style="display:none;margin-top: 20px;">
    <br>
    <p class="info">Here you can override your current MediaFile-Type.</p>
    <br>
        <form method="post" action="setMediaFile.view">
        <input type="hidden" name="id" value="${model.dir.id}">
        <input type="hidden" name="action" value="setmediatype">
        <input type="radio" name="mediatype" value="MULTIARTIST"> MULTIARTIST (Sorting on Title)<br>
        <input type="radio" name="mediatype" value="ARTIST"> ARTIST<br>
        <br>
        <input type="radio" name="mediatype" value="DIRECTORY"> DIRECTORY<br>
        <input type="radio" name="mediatype" value="VIDEOSET"> VIDEOSET <br>        
        <input type="radio" name="mediatype" value="ALBUMSET"> ALBUMSET <br>
        <input type="radio" name="mediatype" value="ALBUM"> ALBUM <br>
        <br>
        <input type="radio" name="mediatype" value="AUTO"> AUTO<br>
        <br>
        <input type="submit" value="Update">
    <br><br>
    <span class="detailcolor">-- DEBUGINFO --</span><br><br>
    <span class="detailcolordark">Media Id </span>
    <span class="detailcolor">${model.dir.id} </span><br>
    <span class="detailcolordark">Override </span>
    <span class="detailcolor">${model.dir.mediaTypeOverride} </span><br>
    <span class="detailcolordark">MediaType </span>
    <span class="detailcolor">${model.dir.mediaType}</span>    <br>
    <br>
    </form>    
</div>

<div id="comment" class="albumComment"><madsonic:wiki text="${model.dir.comment}"/></div>

<div id="commentForm" class="bgcolor2" style="display:none;margin-top:20px;padding-bottom:10px;">
    <form method="post" action="setMusicFileInfo.view">
        <input type="hidden" name="action" value="comment">
        <input type="hidden" name="id" value="${model.dir.id}">    
        <textarea name="comment" rows="8" cols="100">${model.dir.comment}</textarea>
        <input type="submit" value="<fmt:message key="common.save"/>">
    </form>
    <fmt:message key="main.wiki"/>
</div>

<script type='text/javascript'>
    function toggleComment() {
        $("#commentForm").toggle();
        $("#comment").toggle();
    }
    
    function toggleMediaType() {
        $("#MediaTypeSwitch").toggle();
    }    
    
    function toggleCreateURL() {
        $("#createURLForm").toggle();
    }    
    
</script>

<c:if test="${model.user.adminRole}">
    <c:if test="${model.showQuickEdit}">
        <c:if test="${model.dir.mediaType eq 'ARTIST'} ">
            <form method="post" action="setMediaFile.view">
            <input type="hidden" name="id" value="${model.dir.id}">
            <input type="hidden" name="action" value="setmediatype">
            <input type="hidden" name="mediatype" checked value="ALBUMSET">
            <input type="submit" value="Update to ALBUMSET">
        </c:if>        
        <c:if test="${model.dir.mediaType eq 'ARTIST'}">
            <form method="post" action="setMediaFile.view">
            <input type="hidden" name="id" value="${model.dir.id}">
            <input type="hidden" name="action" value="setmediatype">
            <input type="hidden" name="mediatype" checked value="MULTIARTIST">
            <input type="submit" value="Update to MULTIARTIST">
            </c:if>    
        <c:if test="${model.dir.mediaType eq 'VIDEOSET'}">
            <form method="post" action="setMediaFile.view">
            <input type="hidden" name="id" value="${model.dir.id}">
            <input type="hidden" name="action" value="setmediatype">
            <input type="hidden" name="mediatype" checked value="ALBUMSET">
            <input type="submit" value="Update to ALBUMSET">
        </c:if>    
        <c:if test="${model.dir.mediaType ne 'VIDEOSET'}">
            <form method="post" action="setMediaFile.view">
            <input type="hidden" name="id" value="${model.dir.id}">
            <input type="hidden" name="action" value="setmediatype">
            <input type="hidden" name="mediatype" checked value="VIDEOSET">
            <input type="submit" value="Update to VIDEOSET">
        </c:if>            
        <c:if test="${model.dir.mediaType eq 'ALBUMSET' || model.dir.mediaType eq 'ALBUM' }">
            <form method="post" action="setMediaFile.view">
            <input type="hidden" name="id" value="${model.dir.id}">
            <input type="hidden" name="action" value="setmediatype">
            <input type="hidden" name="mediatype" checked value="ARTIST">
            <input type="submit" value="Update to ARTIST">
        </c:if>        
        <c:if test="${model.dir.mediaType ne 'ALBUM'}">
            <form method="post" action="setMediaFile.view">
            <input type="hidden" name="id" value="${model.dir.id}">
            <input type="hidden" name="action" value="setmediatype">
            <input type="hidden" name="mediatype" checked value="ALBUM">
            <input type="submit" value="Update to ALBUM">
        </c:if>            
        <c:if test="${model.dir.mediaType eq 'ARTIST' || model.dir.mediaType eq 'MULTIARTIST' }">
            <form method="post" action="setMediaFile.view">
            <input type="hidden" name="id" value="${model.dir.id}">
            <input type="hidden" name="action" value="setmediatype">
            <input type="hidden" name="mediatype" checked value="DIRETORY">
            <input type="submit" value="Update to DIRETORY">
        </c:if>    
        <span class="detailcolor">now: ${model.dir.mediaType}</span>    
    </c:if>    
</c:if>    

    <div style="position: absolute;top:0;right:0px;padding-top:20px;z-index:1000">
        <Table>
        <tr>
            <td style="padding-left:0.2em; padding-bottom:0em;">
            <%@ include file="viewSelectorMain.jsp" %>
            </td>
        </tr>
        </table>
    </div>

<table cellpadding="10" style="width:100%">
<tr style="vertical-align:top;">

    <c:if test="${model.viewAs eq 'list' or model.viewAs eq 'mixed' or model.viewAs eq 'grid' and model.dir.mediaType eq 'ALBUM'}">

    <td style="vertical-align:top;min-width:360px;">
        <table class="content" width="100%" style="border-collapse:collapse;white-space:nowrap">
            <c:set var="cutoff" value="${model.visibility.captionCutoff}"/>
            <c:forEach items="${model.children}" var="child" varStatus="loopStatus">
                <%--@elvariable id="child" type="org.madsonic.domain.MediaFile"--%>
                <c:choose>
                    <c:when test="${loopStatus.count % 2 == 1}">
                        <c:set var="htmlClass" value="class='bgcolor2'"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="htmlClass" value=""/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                <c:when test="${model.dir.mediaType eq 'MULTIARTIST' or model.dir.mediaType eq 'VIDEOSET'}">
                    <tr style="margin:0;padding:0;border:0;vertical-align: middle;">
                    <td class="fit" >
                    <c:import url="playAddDownload.jsp">
                        <c:param name="id" value="${child.id}"/>
                        <c:param name="video" value="true"/>
                        <c:param name="playEnabled" value="${ (model.dir.mediaType eq 'VIDEOSET') and not child.directory}"/>
                        <c:param name="playAddEnabled" value="false"/>
                        <c:param name="addEnabled" value="false"/>
                        <c:param name="downloadEnabled" value="false"/>
                        <c:param name="artist" value="${fn:escapeXml(child.artist)}"/>
                        <c:param name="title" value="${child.title}"/>
                        <c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
                        <c:param name="starred" value="${not empty child.starredDate}"/>
                        <c:param name="asTable" value="false"/>
                        <c:param name="YoutubeEnabled" value="false"/>
                    </c:import>
                    </c:when>
                    <c:otherwise>
                    </td>
                    <tr style="margin:0;padding:0;border:0">

                    <td class="fit">
                    <c:if test="${child.bookmarkPosition > 0}">
                        <div class="icon-wrapper">
                        <i class="fa fa-bookmark custom-icon starred" id="bookmarkSong" onclick="onToogleBookmark(this.id.substring(12) - 1)" title="<fmt:message key="common.bookmark"/>"><span class="fix-editor">&nbsp;</span></i></div>
                    </c:if>
                    </td>                    
                    
                    <c:if test="${child.rank > 0}">
                        <td class="fit">
                            <c:if test="${model.buttonVisibility.rankVisible}">
                            <div class="icon-wrapper"><i class="fa custom-icon">
                            
                            <span class="custom-icon-rank starred" >  
                            <c:if test="${child.rank < 10}">0</c:if>${child.rank}<span class="fix-editor">&nbsp;</span></i>       
                            </span>
                            </div> 
                            </c:if>
                        </td>                            
                    </c:if>
                
                
                    <c:if test="${child.rank == 0}">
                        <td class="fit" style="padding-right:0.25em;"></td>                            
                    </c:if>
                
                    <td class="fit">
                    <c:import url="playAddDownload.jsp">
                        <c:param name="id" value="${child.id}"/>
                        <c:param name="video" value="${child.video and model.player.web}"/>
                        <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
                        <c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
                        <c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
                        <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addContextVisible}"/>
                        <c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addNextVisible}"/>
                        <c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addLastVisible}"/>                        
                        <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
                        <c:param name="artist" value="${fn:escapeXml(child.artist)}"/>
                        <c:param name="title" value="${child.title}"/>
                        <c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
                        <c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible and model.dir.mediaType ne 'ARTIST' and model.dir.mediaType ne 'ALBUMSET'}"/>
                        <c:param name="starred" value="${not empty child.starredDate}"/>
                        <c:param name="loved" value="${not empty child.lovedDate}"/>                        
                        <c:param name="asTable" value="false"/>
                        <c:param name="YoutubeEnabled" value="${model.buttonVisibility.youtubeVisible}"/>
                        <c:param name="LyricEnabled" value="${model.buttonVisibility.lyricVisible}"/>
                        </c:import>
                        </td>

                    </c:otherwise>
                    
                </c:choose>

                    <c:choose>
                        <c:when test="${child.directory}">
                            <madsonic:url value="main.view" var="childUrl">
                                <madsonic:param name="id" value="${child.id}"/>
                            </madsonic:url>
 
                            <td class="fit" colspan="1">
                                <c:if test="${child.mediaType eq 'ALBUMSET' and model.dir.mediaType ne 'VIDEOSET'}">
                                     <div class="icon-wrapper" style="margin-top:0px;">
                                     <i class="fa fa-dot-circle-o custom-icon" title="<fmt:message key="common.albumset"/>"></i>   
                                     </div>
                                </c:if>                                    
                            </td>
                            
                            <c:choose>
                                <c:when test="${model.showAlbumYear}">
                                    <td class="fit" style="padding-left:1em;padding-right:1em">
                                    <span class="detailyear"><c:if test="${not empty child.year}">[${child.year}]</c:if></span></td>
                                </c:when>
                                <c:otherwise>
                                </c:otherwise>
                            </c:choose>

                            <c:if test="${not empty child.year}">
                                <c:if test="${not empty child.albumSetName}">
                                    <td class="fit" style="padding-left:0.5em;padding-right:1.5em" colspan="5">
                                        <a href="${childUrl}" title="${child.albumSetName}"><span class="album" style="white-space:nowrap;vertical-align:bottom;"><str:truncateNicely upper="${cutoff}">${child.albumSetName}</str:truncateNicely></span></a>

                                    <c:if test="${child.newAdded}">
                                        <img id="newaddedImage" style="margin-left:5px;" src="<spring:theme code="newaddedImage"/>" width="14" height="14" title="new added">
                                    </c:if>
                                    </td>
                                    
                                </c:if>
                                <c:if test="${not empty child.albumName and empty child.albumSetName}">
                                    <td class="fit" style="padding-left:0.5em;padding-right:1.5em" colspan="5">
                                        <a href="${childUrl}" title="${child.albumName}"><span class="album" style="white-space:nowrap;vertical-align: bottom;"><str:truncateNicely upper="${cutoff}">${child.albumSetName}</str:truncateNicely></span></a>
                                        <img id="cdImage" src="<spring:theme code="CDImage"/>" alt="Albumset">
                                    </td>
                                </c:if>
                            </c:if>

                            <c:if test="${empty child.year}">
                                <c:if test="${not empty child.name}">
                                    <td class="fit" style="padding-left:0.5em;padding-right:0.5em" colspan="5">
                                        <a href="${childUrl}" title="${child.albumSetName}"><span class="album" style="white-space:nowrap;"><str:truncateNicely upper="${cutoff}">${child.name}</str:truncateNicely></span></a>
                                    </td>
                                </c:if>
                            </c:if>
                        </c:when>

                        <c:otherwise>
                        
                            <c:if test="${model.dir.mediaType ne 'VIDEOSET'}">
                            <td class="fit" style="padding-left:0.5em;padding-right:0.5em;"><input type="checkbox" class="checkbox" id="songIndex${loopStatus.count - 1}">
                                <span id="songId${loopStatus.count - 1}" style="display: none">${child.id}</span></td>
                            </c:if>    

                            <c:if test="${model.visibility.discNumberVisible}">
                                <td class="fit"  style="padding-right:1.5em;text-align:right">
                                    <span class="detail">${child.discNumber}</span>
                                </td>
                            </c:if>                                
                                
                            <c:if test="${model.visibility.trackNumberVisible}">
                                <td class="fit"  style="padding-right:0.5em;text-align:right">
                                    <span class="detail">${child.trackNumber}</span>
                                </td>
                            </c:if>

                            <td class="fit" style="padding-left:1.0em;padding-right:1.5em;white-space:nowrap">
                                    <span class="songTitle" title="${child.title}"><str:truncateNicely upper="${cutoff}">${fn:escapeXml(child.title)}</str:truncateNicely></span>
                            </td>

                            <c:if test="${model.visibility.albumVisible}">
                                <td class="fit"  style="padding-right:1.5em;white-space:nowrap">
                                    <span class="detail" title="${child.albumName}"><str:truncateNicely upper="${cutoff}">${fn:escapeXml(child.albumName)}</str:truncateNicely></span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.artistVisible and model.multipleArtists}">
                                <td class="fit"  style="padding-right:1.5em;white-space:nowrap">
                                    <span class="detail" title="${child.artist}"><str:truncateNicely upper="${cutoff}">${fn:escapeXml(child.artist)}</str:truncateNicely></span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.composerVisible}">
                                <td class="fit"  style="padding-right:1.5em;white-space:nowrap">
                                    <span class="detail" title="${child.composer}"><str:truncateNicely upper="${cutoff}">${fn:escapeXml(child.composer)}</str:truncateNicely></span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.moodVisible}">
                                <td class="fit"  style="padding-right:1.5em;white-space:nowrap">
                                    <span class="detail">${child.mood}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.genreVisible}">
                                <td class="fit" style="padding-right:1.5em;white-space:nowrap">
                                    <span class="detail">${child.genre}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.yearVisible}">
                                <td class="fit"  style="padding-right:1.5em">
                                    <span class="detail">${child.year}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.bpmVisible}">
                                <td class="fit"  style="padding-right:1.5em;white-space:nowrap">
                                    <span class="detail">${child.bpm}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.formatVisible}">
                                <td class="fit"  style="padding-right:1.5em">
                                    <span class="detail">${fn:toLowerCase(child.format)}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.fileSizeVisible}">
                                <td class="fit"  style="padding-right:1.5em;text-align:right">
                                    <span class="detail"><madsonic:formatBytes bytes="${child.fileSize}"/></span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.durationVisible}">
                                <td class="fit"  style="padding-right:1.5em;text-align:right">
                                    <span class="detail">${child.durationString}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.bitRateVisible}">
                                <td class="fit"  style="padding-right:0em;text-align:right">
                                    <span class="detail">
                                        <c:if test="${not empty child.bitRate}">
                                            ${child.bitRate} Kbps ${child.variableBitRate ? "vbr" : ""}
                                        </c:if>
                                        <c:if test="${child.video and not empty child.width and not empty child.height}">
                                            (${child.width}x${child.height})
                                        </c:if>
                                    </span>
                                </td>
                            </c:if>
                               <td class="rightalign">
                                    <span class="detail">
                                    </span>
                                </td>

                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </table>
    </td>
    
    </c:if>

    <c:if test="${model.viewAs eq 'grid' or model.viewAs eq 'mixed'}">
    
    <td style="vertical-align:top;width:100%" rowspan="2">

    <c:set var="coverArtSize" value="${model.player.coverArtScheme.size}"/>
    <c:set var="captionLength" value="${model.player.coverArtScheme.captionLength}"/>
    
    <c:if test="${model.showGenericArtistArt} and ${model.artist} eq null">
    </c:if>

    <c:if test="${model.showGenericArtistArt}">
        <div class="artistbanner" style="padding-bottom:6px;float:left; "> 
        <c:import url="coverArt.jsp">
            <c:param name="albumId" value="${model.dir.id}"/>
            <c:param name="auth" value="${model.dir.hash}"/>
            <c:param name="artistName" value="${model.dir.name}"/>
            <c:param name="coverArtSize" value="${model.coverArtSize}"/>
            <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>            
            <c:param name="showChange" value="true"/> 
            <c:param name="showLink" value="true"/>
            <c:param name="showZoom" value="false"/>
            <c:param name="showArtist" value="true"/>
		    <c:param name="typArtist" value="true"/>            
            <c:param name="showTopTrack" value="false"/>            
            <c:param name="showPlayAlbum" value="false"/>    
            <c:param name="showAddAlbum" value="false"/>                       
            <c:param name="appearAfter" value="15"/>
            <c:param name="showSmoother" value="true"/>            
        </c:import>
    </div>
    
           <c:if test="${coverArt eq model.dir or coverArt eq model.album or coverArt eq model.artist}"> 
                <div id="placeholder" style="padding-right:0.5em; margin-left:${model.coverArtSize + 20}px;display:none" > 
                    <span id="similarArtistsTitle" style="padding-right:0.5em; display:none;"><fmt:message key="main.similarartists"/></span>
                    <span id="similarArtists"></span>
                </div>
           
                <div id="artistBio" style="max-width: 95%;padding-bottom:1em; margin-left:${model.coverArtSize + 20}px;display:none;"></div>
                <div id="albumBio" style="max-width: 95%;padding-bottom:1em; margin-left:${model.coverArtSize + 20}px;display:none;"></div>
                
                <div style="clear:both;"></div>                
            </c:if> 
    
    </c:if>
    
        <c:forEach items="${model.coverArts}" var="coverArt" varStatus="loopStatus">
        
            <div style="float:left; padding-bottom:6px; padding-right:12px;"> 
                <c:import url="coverArt.jsp">
                    <c:param name="albumId" value="${coverArt.id}"/>
                    <c:param name="auth" value="${coverArt.hash}"/>                    
                    <c:param name="albumName" value="${coverArt.albumSetName}"/>
                    <c:param name="artistName" value="${coverArt.artist}"/>       
                    <c:param name="typVideo" value="${model.dir.mediaType eq 'VIDEOSET'}"/>
                    <c:param name="typArtist" value="${coverArt.mediaType eq 'ARTIST'}"/>                    
                    <c:param name="coverArtSize" value="${model.coverArtSize}"/>
                    <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>                    
                    <c:param name="showLink" value="${coverArt ne model.dir}"/>
                    <c:param name="showZoom" value="${coverArt eq model.dir}"/>
                    <c:param name="showArtist" value="${(coverArt eq model.dir) and model.user.lastFMRole}"/> 
                    <c:param name="showChange" value="${(coverArt eq model.dir) and (not model.isArtist)}"/> 
                    <c:param name="showCaption" value="true"/>
                    <c:param name="showTopTrack" value="${coverArt eq model.dir and model.user.streamRole and model.dir.mediaType ne 'VIDEOSET'}"/>    
                    <c:param name="showPlayAlbum" value="${model.user.streamRole and not model.partyMode}"/>    
                    <c:param name="showAddAlbum" value="${model.user.streamRole and not model.partyMode}"/>                       
                    <c:param name="captionLength" value="${captionLength}"/>
                    <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                    <c:param name="showSmoother" value="true"/>                    
                </c:import>
                
            </div>  
           <c:if test="${coverArt eq model.dir or coverArt eq model.album or coverArt eq model.isArtist}"> 
                <div id="placeholder" style="padding-right:0.5em; margin-left:${model.coverArtSize + 20}px;display:none;" > 
                    <img  id="similarArtistsIcon" src="icons/default/artists.png" style="width:16px;padding-right:4px">
                    <span id="similarArtistsTitle" style="padding-right:0.5em; display:none;"><fmt:message key="main.similarartists"/></span>
                    <span id="similarArtists"></span>
                </div>

                <div id="artistBio" style="max-width: 95%;padding-bottom:1em; margin-left:${model.coverArtSize + 20}px;display:none;"></div>
                <div id="albumBio" style="max-width: 95%;padding-bottom:1em; margin-left:${model.coverArtSize + 20}px;display:none;"></div>
                
                <div style="clear:both;"></div>                
            </c:if>
        </c:forEach>
       
    <c:if test="${model.multipleArtists}">
    
    </c:if>

    <c:if test="${model.showGenericCoverArt}">
    <div class="coverart" style="float:left;">
        <c:import url="coverArt.jsp">
            <c:param name="albumId" value="${model.dir.id}"/>
            <c:param name="auth" value="${model.dir.hash}"/>              
            <c:param name="coverArtSize" value="${model.coverArtSize}"/>
            <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>            
            <c:param name="showLink" value="false"/>
            <c:param name="showZoom" value="false"/>
            <c:param name="showChange" value="${model.user.coverArtRole}"/>
            <c:param name="appearAfter" value="5"/>
            <c:param name="showSmoother" value="true"/>            
        </c:import>
    </div>
    
           <c:if test="${coverArt eq model.dir or coverArt eq model.album or coverArt eq model.artist}"> 
                <div id="placeholder" style="padding-right:0.5em; margin-left:${model.coverArtSize + 20}px;" > 
                    <span id="similarArtistsTitle" style="padding-right:0.5em; display:none;"><fmt:message key="main.similarartists"/></span>
                    <span id="similarArtists"></span>
                </div>

                <div id="artistBio" style="max-width: 95%;padding-bottom:1em; margin-left:${model.coverArtSize + 20}px;display:none;"></div>
                <div id="albumBio" style="max-width: 95%;padding-bottom:1em; margin-left:${model.coverArtSize + 20}px;display:none;"></div>
                
                <div style="clear:both;"></div>                
            </c:if>
    
    </c:if>
    </td>

    <td style="vertical-align:top;">
    
       <c:forEach items="${model.sieblingAlbums}" var="sieblingAlbum" varStatus="loopStatus">
       <div class="coverart" style="display:${loopStatus.count < 5 ? 'inline-block' : 'none'}">
            <c:import url="coverArt.jsp">
                <c:param name="albumId" value="${sieblingAlbum.id}"/>
                <c:param name="auth" value="${sieblingAlbum.hash}"/>                  
                <c:param name="albumName" value="${sieblingAlbum.name}"/>
                <c:param name="coverArtSize" value="${model.sieblingCoverArtScheme.size}"/>
                <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>
                <c:param name="showLink" value="true"/>
                <c:param name="showZoom" value="false"/>
                <c:param name="showChange" value="false"/>
                <c:param name="showCaption" value="true"/>
                <c:param name="captionLength" value="${model.sieblingCoverArtScheme.captionLength}"/>
                <c:param name="showPlayAlbum" value="${model.user.streamRole and not model.partyMode}"/>    
                <c:param name="showAddAlbum" value="${model.user.streamRole and not model.partyMode}"/>                       
                <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                <c:param name="showSmoother" value="true"/>                
            </c:import>
        </div> 
        </c:forEach>
        <c:if test="${fn:length(model.sieblingAlbums) >= 5}">
            <input id="showAllButton" class="albumOverflowButton" type="button" value="<fmt:message key="main.showall"/>" onclick="showAllAlbums()">
        </c:if>
    </td>
    
    </c:if>
    
    <c:if test="${model.showAd}">
        <td class="ad" style="vertical-align:top;min-width:160px" rowspan="2">
            <h2 style="padding-bottom: 1em">Madsonic Premium</h2>
            <p style="font-size: 90%">
                Upgrade to Madsonic Premium and get:
            </p>
            <div style="font-size: 80%;padding-bottom: 0.5em">
            
                <p>Your personal<br> 
                   server address: <em>you</em>.madsonic.org</p>
                <p>Advanced LDAP support.</p>
                <p>Advanced user management.</p>
                <p>Advanced folder management.</p>
                <p>Sonos & Chromecast support.</p>
                <p>DLNA/UPnP device support.</p>
                <p>Audio conversion support.</p>                
                <p>Video conversion support.</p>
                <p>Share with Social media.</p>
                <p>Logon with Signup service.</p>
                <p>Node management.</p>                
                <p>No ads.</p>
            </div>
            <p class="forward" style="white-space: nowrap"><a href="http://beta.madsonic.org/pages/premium.jsp" target="_blank">Get Premium</a></p>
        </td>
        <td style="vertical-align:top;width:10px" rowspan="2">
        </td>
  </c:if>
      
    </tr>
    
    
</table>

<c:choose>
    <c:when test="${model.viewAs eq 'list'}">
        <table class="music indent" style="margin-left:14px; width: 98.3%;">
            <c:forEach items="${model.sieblingAlbums}" var="subDir">
                <tr>
                    <td class="fit">
                    <c:import url="playAddDownload.jsp">
                        <c:param name="id" value="${subDir.id}"/>
                        <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
                        <c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
                        <c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
                        <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not subDir.directory) and model.buttonVisibility.addContextVisible}"/>
                        <c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not subDir.directory) and model.buttonVisibility.addNextVisible}"/>
                        <c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not subDir.directory) and model.buttonVisibility.addLastVisible}"/>                        
                        <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
                        <c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>                        
                        <c:param name="starred" value="${not empty subDir.starredDate}"/>
                        <c:param name="asTable" value="false"/>
                    </c:import>
                    </td>
                    <td class="fit rightalign detailyear" style="padding-left:1em;padding-right:0.5em">[${subDir.year}]</td>
                    <td class="truncate"><a href="main.view?id=${subDir.id}" class="album" title="${fn:escapeXml(subDir.albumName)}">${fn:escapeXml(subDir.albumName)}</a></td>
                    
                </tr>
            </c:forEach>
        </table>
    </c:when>
</c:choose>

<div id="dialog-select-playlist" title="<fmt:message key="main.addtoplaylist.title"/>" style="display: none;">
    <p><fmt:message key="main.addtoplaylist.text"/></p>
    <div id="dialog-select-playlist-list"></div>
</div>

	</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>

</body>
</html>
