<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- ---------------------------------------------------------------------------------------- --> <!--    HTML    -->
<head><!-- ---------------------------------------------------------------------------------------- --> <!--    HEAD    -->
<%@ include file="head.jsp" %> <!-- --------------------------------------------------------------- --> <!--   HEADER   -->
<%@ include file="jquery.jsp" %> <!-- ------------------------------------------------------------- --> <!--   JQUERY   -->
<%@ include file="customScrollbar.jsp" %> <!-- ---------------------------------------------------- --> <!-- /SCROLLBAR -->
<script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script> <!-- ----------- --> <!--   SCRIPT   -->
</head><!-- --------------------------------------------------------------------------------------- --> <!--   /HEAD    -->
<body class="mainframe bgcolor1"> <!-- ------------------------------------------------------------ --> <!--    BODY    -->
  <div id="content_main" class="content_main"> <!-- ----------------------------------------------  --> <!--   CONTENT  -->
	<div id="container" class="container"> <!-- --------------------------------------------------- --> <!--  CONTAINER -->
		<h1><img src="<spring:theme code="genresImage"/>" width="32" alt=""> Template</h1> <p>...</p>   <!--   CONTEXT  -->
	</div> <!-- ----------------------------------------------------------------------------------- --> <!-- /CONTAINER -->
</div> <!-- --------------------------------------------------------------------------------------- --> <!--  /CONTENT  -->
<!-- ---------------------------------------------------------------------------------------------- --> <!--  SCROLLBAR -->
<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
	(function($){
		$(window).load(function(){
			$("#content_main").mCustomScrollbar({
				axis:"y",
				scrollInertia:600, 		 /*scrolling inertia: integer (milliseconds)*/
				mouseWheel:true, 		 /*mousewheel support: boolean*/
				mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
				autoDraggerLength:true,  /*auto-adjust scrollbar dragger length: boolean*/
				autoHideScrollbar:true,  /*auto-hide scrollbar when idle*/        
				alwaysShowScrollbar:false,
				advanced:{      updateOnBrowserResize:true,            /*for layouts based on percentages: boolean*/
								updateOnContentResize:true, 		   /*for dynamic content: boolean*/
								autoExpandHorizontalScroll:false },    /*for horizontal scrolling: boolean*/
				scrollButtons:{ enable:true, 						   /*scroll buttons support: boolean*/
								scrollType:"continuous", 			   /*scroll buttons scrolling type: "continuous", "pixels"*/
								scrollSpeed:"auto", 				   /*scroll buttons continuous scrolling speed: integer, "auto"*/
								scrollAmount:40 },  				   /*scroll buttons pixels scroll amount: integer (pixels)*/
								theme:"${model.customScrollbarTheme}", /*scroll buttons theme*/
								scrollbarPosition:"inside"
			});
		});
	})(jQuery);
		
    $("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
    });		
    </script>
</c:if>
<!-- ---------------------------------------------------------------------------------------------- --> <!-- /SCROLLBAR  -->
</body> <!-- -------------------------------------------------------------------------------------- --> <!--    /BODY    -->
</html> <!-- -------------------------------------------------------------------------------------- --> <!--    /HTML    -->