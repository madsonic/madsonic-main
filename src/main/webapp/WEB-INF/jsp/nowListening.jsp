<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<html>
<head>
    <%@ include file="head.jsp" %>
	<%@ include file="jquery.jsp" %> 	
	<%@ include file="customScrollbar.jsp" %>
	
	<link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">
	<script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/nowPlayingService.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>        
	<script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>        
</head>

<body class="playingframe bgcolor1" onload="init()"> 
  <div id="content_main" class="content_main"> 
	<div id="container" class="container">

	<script type="text/javascript">
		function init() {
			dwr.engine.setErrorHandler(null);
		}
	</script>
	
    <!-- This script uses AJAX to periodically retrieve what all users are playing. -->
    <script type="text/javascript" language="javascript">        
        
        startgetNowListeningTimer();
		
        function startgetNowListeningTimer() {
            nowPlayingService.getNowListening(getNowListeningCallback);
            setTimeout("startgetNowListeningTimer()", 10000);
        }
    
        function getNowListeningCallback(nowPlaying) {
    
            <c:if test="${model.showSidePanel}">   
                        
            if (nowPlaying.length == 0) {
               parent.setSidePanelWidth(0);                
            } else {
               parent.setSidePanelWidth(300);

            if (escapeHtml(jQuery('#artistname').html()) != nowPlaying[0].artist) {
                jQuery("#similarArtists").hide();
                jQuery("#similarArtistsTitle").hide();
                jQuery("#similarArtistsRadio").hide();
                jQuery("#artistBioTable").hide();                    
                jQuery("#artistBio").hide();
                jQuery("#topAlbums").hide();
                jQuery("#topAlbumsTitle").hide();
                jQuery("#topTracks").hide();
                jQuery("#topTracksTitle").hide();
            }
            
            var html = ""; 
            
            if (nowPlaying.length == 0) {
                // ignore
            } else {
                html += "<h1 style='padding-top: 11px;padding-bottom: 5px;'>";
                
                if (nowPlaying[0].artist != null) {
                    html += nowPlaying[0].artist + "</h1>" + "<table style='width:100%'>";
                } else {
                    html += "</h1>" + "<table style='width:100%'>";
                }          
            }

            html += "<tr><td colspan='2' class='detail' style=';white-space:nowrap'>";
            html += "<a title='" + nowPlaying[0].tooltip + "' target='main' href='" + nowPlaying[0].albumUrl + "'>";
            html += "<span id='songTitle' class='songTitle'>" + nowPlaying[0].title + "</span></a><br/>";
            html += "</td></tr><tr><td rowspan='2'>";
             if (nowPlaying[0].year != null && nowPlaying[0].year != 'null') {
                html += "<span id='songYear' class='songYear'>" + nowPlaying[0].year + "</span></a> ";
            }           
            html += "<span id='songDuration' class='songDuration'>";
             if (nowPlaying[0].year != null && nowPlaying[0].year != 'null') {
                html += " / ";
             }
            html += nowPlaying[0].duration + "</span></a>";
            html += "<span id='mediaId' style='display:none'>" + nowPlaying[0].mediaId + "</span></a><br>";
            html += "</td></tr><tr><td rowspan='2'>";
            html += "<a title='" + nowPlaying[0].tooltip + "' target='main' href='" + nowPlaying[0].albumUrl + "'>" +
            "<img src='" + nowPlaying[0].coverArtZoomUrl + "' class='dropshadow' height='60' width='60' style='float: right;margin-right: 10px;'></a>";            
            html += "</td></tr><tr><td><div align='left'>";
            
            if (nowPlaying[0].bookmarked == true) {
                         html += "<i style='margin-right:1px;' class='fa fa-bookmark custom-icon-nohover starred'></i>";
                } else { html += "<i style='margin-right:1px;' class='fa fa-bookmark-o custom-icon-nohover'></i>";
            }
            if (nowPlaying[0].loved == true) {
                         html += "<i style='margin-right:1px;' class='fa fa-heart custom-icon-nohover starred'></i>";
                } else { html += "<i style='margin-right:1px;' class='fa fa-heart-o custom-icon-nohover'></i>";
            }                 
            if (nowPlaying[0].starred == true) {
                         html += "<i style='margin-right:1px;' class='fa fa-star custom-icon-nohover starred'></i>";
                } else { html += "<i style='margin-right:1px;' class='fa fa-star-o custom-icon-nohover'></i>";
            }
            html += "</div></td></tr>";
            html += "</table>";
            
            jQuery("#nowPlaying").html(html);
            jQuery("#nowPlaying").show();
            
            if (escapeHtml(jQuery('#artistname').html()) != nowPlaying[0].artist) {
                jQuery("#artistname").html(nowPlaying[0].artist);
               loadArtistInfo(nowPlaying[0].mediaId);               
            }
            
            } 
            </c:if> 
            
        }
        
        function loadArtistInfo(mediaId) {
        
            multiService.getFullArtistInfo(mediaId, 10, function (artistInfo) {
                
                
                if (artistInfo.similarArtists.length != null && artistInfo.similarArtists.length > 0 ) {
                    var html = "";
                    for (var i = 0; i < artistInfo.similarArtists.length; i++) {
                        html += "<i class='fa fa-file accent' style='margin:3'> </i> <a href='main.view?id=" + artistInfo.similarArtists[i].mediaFileId + "' style='white-space:nowrap' target='main'>" + escapeHtml(artistInfo.similarArtists[i].artistName) + "</a><br>";
                    }
                    jQuery("#similarArtists").html(html);
                    jQuery("#similarArtists").show();
                    jQuery("#similarArtistsTitle").show();
                    jQuery("#similarArtistsRadio").show();
                    jQuery("#artistBioTable").show();                    
                    jQuery("#artistBio").show();
                } else {
                    jQuery("#similarArtists").hide();
                    jQuery("#similarArtistsTitle").hide();
                    jQuery("#similarArtistsRadio").hide();
                }
                
                if (artistInfo.topAlbums.length != null && artistInfo.topAlbums.length > 0 ) {
                    var html = "";
                    for (var i = 0; i < artistInfo.topAlbums.length; i++) {
                        html += "<i class='fa fa-file accent' style='margin:3'> </i> <a href='main.view?id=" + artistInfo.topAlbums[i].mediaFileId + "' style='white-space:nowrap' target='main'>" + escapeHtml(artistInfo.topAlbums[i].albumName) + "</a><br>";
                    }
                    jQuery("#topAlbums").html(html);
                    jQuery("#topAlbums").show();
                    jQuery("#topAlbumsDiv").show();
                    jQuery("#topAlbumsTitle").show();

                } else {
                    jQuery("#topAlbums").hide();
                    jQuery("#topAlbumsDiv").hide();
                    jQuery("#topAlbumsTitle").hide();
                }                

                
                if (artistInfo.topTracks.length != null && artistInfo.topTracks.length > 0) {
                    var html = "";
                    for (var i = 0; i < artistInfo.topTracks.length; i++) {
                        html += "<i class='fa fa-music accent' style='margin:3'> </i> <a href='main.view?id=" + artistInfo.topTracks[i].mediaFileId + "' style='white-space:nowrap' target='main'>" + escapeHtml(artistInfo.topTracks[i].trackName) + "</a><br>";
                    }
                    jQuery("#topTracks").html(html);
                    jQuery("#topTracks").show();
                    jQuery("#topTracksDiv").show();                    
                    jQuery("#topTracksTitle").show();
                } else {
                    jQuery("#topTracks").hide();
                    jQuery("#topTracksDiv").hide();
                    jQuery("#topTracksTitle").hide();
                }                
                
                if (artistInfo.artistBio && artistInfo.artistBio.shortBio) {
                
                    jQuery("#artistBio").html( artistInfo.artistBio.shortBio);
                    
                    if (artistInfo.artistBio.mediumImageUrl) {
                        jQuery("#artistImage").attr("src", artistInfo.artistBio.smallImageUrl);  
                        jQuery("#artistImage").attr("height", "60");
                        jQuery("#artistImage").attr("width", "60");                        
                        jQuery("#artistImage").show();
                    }
                jQuery("#artistBio").show();
                jQuery("#artistBioTable").show();                    
                } else {
                jQuery("#artistBio").html("");
                jQuery("#similarArtists").html("");                
                jQuery("#artistBioTable").hide();                    
                jQuery("#artistImage").hide();
                jQuery("#similarArtists").hide();
                jQuery("#similarArtistsTitle").show();
                }
            });
        }          

        function playSimilar() {
            parent.playQueue.onPlaySimilar(jQuery("#mediaId").html(), 30);
            parent.main.onToastmessage("showSuccessToast", "Play similar artist radio");
        }         
    </script>
                        
    <div id="nowPlaying" class="nowPlaying"></div>
    <div id="artistname" style="display:none"></div>
    
    <table id="artistBioTable" style="width:100%;padding-top:1em;padding-bottom: 1em;clear:both;border-bottom: 1px dotted #ccc;display: none">
    <tr>
        <td id="artistBio"></td>
        <td style="vertical-align:top">
            <img id="artistImage" class="dropshadow" alt="" style="float: right;margin-left: 5px;margin-right: 10px; display:none">
        </td>        
    </tr>
    <tr>
    <!-- <td colspan=2><div style="border-bottom: 1px dotted #333;"></div></td> -->
    </tr>
    <tr><td colspan=2>
        <h2><span id="similarArtistsTitle" style="display:none"><fmt:message key="main.similarartists"/></span></h2>
        <span id="similarArtists"></span>
    </td></tr>
    <tr>
    <td>
        <div id="similarArtistsRadio" class="forward" style="display: none">
            <a href="javascript:playSimilar()"><fmt:message key="main.startradio"/></a>
        </div>
    </td></tr>
  
    <tr>
    <td colspan=2><div id="topAlbumsDiv" style="padding-bottom: 8px;border-bottom: 1px dotted #ccc;"></div>
    <h2><span id="topAlbumsTitle" style="display:none">TOP Albums</span></h2>    
    <span id="topAlbums" style="display:none"></span>
    </td></tr>

    <tr>
    <td colspan=2><div id="topTracksDiv" style="padding-bottom: 8px;border-bottom: 1px dotted #ccc;"></div>
    <h2><span id="topTracksTitle" style="display:none">TOP Tracks</span></h2>    
    <span id="topTracks" style="display:none"></span>
    </td></tr>
    
    </table>
	
</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
	(function($){
		$(window).load(function(){
			$("#content_main").mCustomScrollbar({
				axis:"y",
				scrollInertia:600, 		 /*scrolling inertia: integer (milliseconds)*/
				mouseWheel:true, 		 /*mousewheel support: boolean*/
				mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
				autoDraggerLength:true,  /*auto-adjust scrollbar dragger length: boolean*/
				autoHideScrollbar:false,  /*auto-hide scrollbar when idle*/        
				alwaysShowScrollbar:false,
				advanced:{      updateOnBrowserResize:true,           /*for layouts based on percentages: boolean*/
								updateOnContentResize:true, 		   /*for dynamic content: boolean*/
								autoExpandHorizontalScroll:false },    /*for horizontal scrolling: boolean*/
				scrollButtons:{ enable:true, 						   /*scroll buttons support: boolean*/
								scrollType:"continuous", 			   /*scroll buttons scrolling type: "continuous", "pixels"*/
								scrollSpeed:"auto", 				   /*scroll buttons continuous scrolling speed: integer, "auto"*/
								scrollAmount:40 },  				   /*scroll buttons pixels scroll amount: integer (pixels)*/
								theme:"${model.customScrollbarTheme}", /*scroll buttons theme*/
								scrollbarPosition:"inside"
			});
		});
	})(jQuery);
		
    $("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
    });		
    </script>
</c:if>
	
</body>
</html>