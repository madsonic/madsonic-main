<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <link href="<c:url value="/style/artist/style-artist.css"/>" rel="stylesheet">
    
    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/smooth-scroll.js"/>"></script>
        </c:otherwise>
    </c:choose>
 

    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script> 
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

    <script type="text/javascript">
        function playTagRadio() {
        var genres = new Array();
        var e = document.getElementsByTagName("span");
        for (var i = 0; i < e.length; i++) {
            if (e[i].className == "on2") {
                genres.push(e[i].firstChild.data);
            }
        }
        // var num = document.getElementsByName("GenreRadioPlayCount")[0].selectedIndex;
        // var playcount = document.getElementsByName("GenreRadioPlayCount")[0].options[num].text;

        parent.playQueue.onPlayGenreRadio(genres, 10);
    }
    </script>
    
    
    <script type="text/javascript">
        function albumlink(ob) {
            window.location.href = $(ob).attr("alt");
        }    
    </script>
    
    <script type="text/javascript" language="javascript">
        function init() {
            dwr.engine.setErrorHandler(null);
        }
        
        function changeClass(elem, className1,className2) {
        elem.className = (elem.className == className1)?className2:className1;
        }

	function showSearch(albumId) {
		var SearchStack = "#showSearch_" + albumId;
		var ShowSearchToogle = "#showSearchToogle_" + albumId;
		var HideSearchToogle = "#hideSearchToogle_" + albumId;
		
		$( SearchStack ).show('blind');
		$( ShowSearchToogle ).hide();
		$( HideSearchToogle ).show();
	}

	function hideSearch(albumId) {
		var SearchStack = "#showSearch_" + albumId;
		var ShowSearchToogle = "#showSearchToogle_" + albumId;
		var HideSearchToogle = "#hideSearchToogle_" + albumId;
		
		$( SearchStack ).hide('blind');
		$( ShowSearchToogle ).show();
		$( HideSearchToogle ).hide();
	}
	
	function showAlbums(albumId) {
		var AlbumStack = "#album_stack_" + albumId;
		var ShowAlbumButton = "#showAlbums_" + albumId;
		var HideAlbumButton = "#hideAlbums_" + albumId;

		$( AlbumStack ).show('blind');
		$( ShowAlbumButton ).hide();
		$( HideAlbumButton ).show();
	}

	function hideAlbums(albumId) {
		var AlbumStack = "#album_stack_" + albumId;
		var ShowAlbumButton = "#showAlbums_" + albumId;
		var HideAlbumButton = "#hideAlbums_" + albumId;

		$( AlbumStack ).hide('blind');
		$( HideAlbumButton ).hide();
		$( ShowAlbumButton ).show();
        }        
    </script>

    <script type="text/javascript">

        function albumlink(ob) {
            window.location.href = $(ob).attr("alt");
        }    
    
    $(document).ready(function() { 
    $(".image_stack").delegate('img', 'mouseenter', function() {//when user hover mouse on image with div id=stackphotos 
            if ($(this).hasClass('stackphotos')) {//
            // the class stackphotos is not really defined in css , it is only assigned to each images in the photo stack to trigger the mouseover effect on  these photos only 
                
                var $parent = $(this).parent();
                $parent.find('img#photo1').addClass('rotate4');
                $parent.find('img#photo2').addClass('rotate3');//add class rotate1,rotate2,rotate3 to each image so that 
                $parent.find('img#photo3').addClass('rotate1');//it rotates to the correct degree in the correct direction ( 15 degrees one to the left , one to the right ! )
                $parent.find('img#photo2').css("left","-30px"); // reposition the first and last image 
                $parent.find('img#photo3').css("left","65px");
            }
        })
        .delegate('img', 'mouseleave', function() {// when user removes cursor from the image stack
                $('img#photo1').removeClass('rotate4');// remove the css class that was previously added to make it to its original position
                $('img#photo2').removeClass('rotate3');
                $('img#photo3').removeClass('rotate1');
                $('img#photo2').css("left","");// remove the css property 'left' value from the dom
                $('img#photo3').css("left","");
        });;
    });
    
	$(document).ready(function() { 
    $(".image_stack2").delegate('img', 'mouseenter', function() {//when user hover mouse on image with div id=stackphotos 
            if ($(this).hasClass('stackphotos')) {//
            // the class stackphotos is not really defined in css , it is only assigned to each images in the photo stack to trigger the mouseover effect on  these photos only 
                
                var $parent = $(this).parent();
                $parent.find('img#photo1').addClass('rotate4');
                $parent.find('img#photo2').addClass('rotate3');//add class rotate1,rotate2,rotate3 to each image so that 
                $parent.find('img#photo3').addClass('rotate1');//it rotates to the correct degree in the correct direction ( 15 degrees one to the left , one to the right ! )
                $parent.find('img#photo2').css("left","-30px"); // reposition the first and last image 
                $parent.find('img#photo3').css("left","65px");
            }
        })
        .delegate('img', 'mouseleave', function() {// when user removes cursor from the image stack
                $('img#photo1').removeClass('rotate4');// remove the css class that was previously added to make it to its original position
                $('img#photo2').removeClass('rotate3');
                $('img#photo3').removeClass('rotate1');
                $('img#photo2').css("left","");// remove the css property 'left' value from the dom
                $('img#photo3').css("left","");
        });;
    });
    </script>

    <link href="<c:url value="/style/carousel2.css"/>" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script> 
    <script type="text/javascript" src="<c:url value="/script/jquery.event.drag-2.2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/cloud-carousel.1.0.7.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/cloud-carousel-artist.js"/>"></script>
    
</head>

<body class="bgcolor1 mainframe" onload="init()">

<c:choose>
  <c:when test="${model.showAlbum}">
    <%@ include file="artistDetail.jsp" %>
      </c:when>
  <c:otherwise>
    <%@ include file="artistMain.jsp" %>
  </c:otherwise>
</c:choose>

<c:if test="${model.lastFMArtistTopTracks.size() > 0}">
<h2>Artist TopTrack</h2>
</c:if>

<!--
<table cellpadding="10" style="width:100%">
<tr style="vertical-align:top;">
<td style="vertical-align:top;min-width:340px;">
-->

        <table class="content" style="border-collapse:collapse;white-space:nowrap">
            <c:set var="cutoff" value="${model.visibility.captionCutoff}"/>
            <c:forEach items="${model.lastFMArtistTopTracks}" var="child" varStatus="loopStatus">
            
            <tr style="margin:0;padding:0;border:0">
            
                <c:if test="${child.rank > 0}">
                    <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor1'" : "class='fit'"}>
                        <div class="icon-wrapper"><i class="fa custom-icon"> 
                        <span class="custom-icon-rank starred" style="cursor:text;">
                        <c:if test="${child.rank < 10}">0</c:if>${child.rank}                        
                        <span id="rank">&nbsp;</span></span></i>      
                        </div> 
                    </td>                     
                </c:if>

                    <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor1'" : "class='fit'"} style="padding-left:0.25em; padding-right:0.75em">
                    <c:import url="playAddDownload.jsp">
                        <c:param name="id" value="${child.id}"/>
                        <c:param name="video" value="${child.video and model.player.web}"/>
                        <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
                        <c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
                        <c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
                        <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addContextVisible}"/>
                        <c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addNextVisible}"/>
                        <c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addLastVisible}"/>                        
                        <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
                        <c:param name="artist" value="${fn:escapeXml(child.artist)}"/>
                        <c:param name="title" value="${child.title}"/>
                        <c:param name="starEnabled" value="true"/>
                        <c:param name="starred" value="${not empty child.starredDate}"/>
                        <c:param name="asTable" value="false"/>
                        <c:param name="YoutubeEnabled" value="${model.buttonVisibility.youtubeVisible}"/>
                        </c:import>
                    </td>
                    
                    <c:choose>
                        <c:when test="${child.directory}">
                            <madsonic:url value="main.view" var="childUrl">
                                <madsonic:param name="id" value="${child.id}"/>
                            </madsonic:url>
 
                            <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-left:0.5em" colspan="1">
                                <c:if test="${child.mediaType eq 'ALBUMSET' and model.dir.mediaType ne 'VIDEOSET'}">
                                    <img id="cdImage" src="<spring:theme code="CDImage"/>" alt="Albumset">
                                </c:if>                                    
                            </td>
                            
                            <c:choose>
                                <c:when test="${model.showAlbumYear}">
                                    <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-left:0.50em">
                                    <span class="detail"><c:if test="${not empty child.year}">[${child.year}]</c:if></span></td>
                                </c:when>
                                <c:otherwise>
                                </c:otherwise>
                            </c:choose>

                            <c:if test="${not empty child.year}">
                                <c:if test="${not empty child.albumSetName}">
                                    <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-left:0.5em" colspan="5">
                                        <a href="${childUrl}" title="${child.albumSetName}"><span class="album" style="white-space:nowrap;vertical-align:bottom;"><str:truncateNicely upper="${cutoff}">${child.albumSetName}</str:truncateNicely></span></a>

                                    <c:if test="${child.newAdded}">
                                        <img id="newaddedImage" style="margin-left:5px;" src="<spring:theme code="newaddedImage"/>" width="14" height="14" title="new added">
                                    </c:if>
                                    </td>
                                    
                                </c:if>
                                <c:if test="${not empty child.albumName and empty child.albumSetName}">
                                    <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-left:0.5em" colspan="5">
                                        <a href="${childUrl}" title="${child.albumName}"><span style="white-space:nowrap;vertical-align: bottom;"><str:truncateNicely upper="${cutoff}">${child.albumSetName}</str:truncateNicely></span></a>
                                        <img id="cdImage" src="<spring:theme code="CDImage"/>" alt="Albumset">
                                    </td>
                                </c:if>
                            </c:if>

                            <c:if test="${empty child.year}">
                                <c:if test="${not empty child.name}">
                                    <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-left:0.5em" colspan="5">
                                        <a href="${childUrl}" title="${child.albumSetName}"><span style="white-space:nowrap;"><str:truncateNicely upper="${cutoff}">${child.name}</str:truncateNicely></span></a>
                                    </td>
                                </c:if>
                            </c:if>
                        </c:when>

                        <c:otherwise>
<!--                        
                            <c:if test="${model.dir.mediaType ne 'VIDEOSET'}">
                            <td ${htmlClass} style="padding-left:0.50em;padding-right:0.5em;"><input type="checkbox" class="checkbox" id="songIndex${loopStatus.count - 1}">
                                <span id="songId${loopStatus.count - 1}" style="display: none">${child.id}</span></td>
                            </c:if>    

                            <c:if test="${model.visibility.discNumberVisible}">
                                <td ${htmlClass} style="padding-right:0.75em;text-align:right">
                                    <span class="detail">${child.discNumber}</span>
                                </td>
                            </c:if>                                
-->                                
                            <c:if test="${model.visibility.trackNumberVisible}">
                                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:1.0em;text-align:right">
                                    <span class="detail">${child.trackNumber}</span>
                                </td>
                            </c:if>

                            <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:2.0em;white-space:nowrap">
                                    <span class="songTitle" title="${child.title}"><str:truncateNicely upper="${cutoff}">${fn:escapeXml(child.title)}</str:truncateNicely></span>
                            </td>

                            <c:if test="${model.visibility.albumVisible}">
                                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:1.75em;white-space:nowrap">
                                    <span class="detail" title="${child.albumName}"><str:truncateNicely upper="${cutoff}">${fn:escapeXml(child.albumName)}</str:truncateNicely></span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.artistVisible and model.multipleArtists}">
                                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:1.75em;white-space:nowrap">
                                    <span class="detail" title="${child.artist}"><str:truncateNicely upper="${cutoff}">${fn:escapeXml(child.artist)}</str:truncateNicely></span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.moodVisible}">
                                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:2.25em;white-space:nowrap">
                                    <span class="detail">${child.mood}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.genreVisible}">
                                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:2.0em;white-space:nowrap">
                                    <span class="detail">${child.genre}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.yearVisible}">
                                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:1.50em">
                                    <span class="detail">${child.year}</span>
                                </td>
                            </c:if>
<!--
                            <c:if test="${model.visibility.formatVisible}">
                                <td ${htmlClass} style="padding-right:1.25em">
                                    <span class="detail">${fn:toLowerCase(child.format)}</span>
                                </td>
                            </c:if>

                            <c:if test="${model.visibility.fileSizeVisible}">
                                <td ${htmlClass} style="padding-right:1.25em;text-align:right">
                                    <span class="detail"><madsonic:formatBytes bytes="${child.fileSize}"/></span>
                                </td>
                            </c:if>
-->
                            <c:if test="${model.visibility.durationVisible}">
                                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor1'" : "class=''"} style="padding-right:1.25em;text-align:right">
                                    <span class="detail">${child.durationString}</span>
                                </td>
                            </c:if>

<!--                            <c:if test="${model.visibility.bitRateVisible}">
                                <td ${htmlClass} style="padding-right:0.25em">
                                    <span class="detail">
                                        <c:if test="${not empty child.bitRate}">
                                            ${child.bitRate} Kbps ${child.variableBitRate ? "vbr" : ""}
                                        </c:if>
                                        <c:if test="${child.video and not empty child.width and not empty child.height}">
                                            (${child.width}x${child.height})
                                        </c:if>
                                    </span>
                                </td>
                            </c:if>
-->                
                
                    </c:otherwise>
                    </c:choose>
                
                </tr>
            </c:forEach>
            
        </table>
        
        <!--
        </td>        
    </tr>
</table>
 -->
 </div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->


<c:if test="${model.customScrollbar}">
<script>
	(function($){
		$(window).load(function(){
			
			$("#content_artist").mCustomScrollbar({
				axis:"y",
				scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
				mouseWheel:true, /*mousewheel support: boolean*/
				mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
				autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
				autoHideScrollbar:false, /*auto-hide scrollbar when idle*/     
				alwaysShowScrollbar:true,
				scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
								scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
								scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
								scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
								theme:"${model.customScrollbarTheme}",
								scrollbarPosition:"inside"
			});
		});
	})(jQuery);
</script>
</c:if>
</body>

</html>