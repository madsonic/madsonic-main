<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%@ include file="include.jsp" %>

<c:if test="${param.bug}">
    <script type="text/javascript">
        $(document).ready(function () {
			// reconfiguring the toasts as sticky
			$().toastmessage({sticky:true});
            $().toastmessage("showErrorToast", "${param.bugInfo}"); // <fmt:message key="common.bug"/> 
        });
    </script>
</c:if>

<c:if test="${param.warn}"> 
    <script type="text/javascript">
        $(document).ready(function () {
			// reconfiguring the toasts as sticky
			$().toastmessage({sticky:true});		
            $().toastmessage("showWarningToast", "${param.warnInfo}" ); 
        });
    </script>
</c:if>

<c:if test="${param.toast}">
    <script type="text/javascript">
        $(document).ready(function () {
			$().toastmessage({sticky:false});		
            $().toastmessage("showNoticeToast", "<fmt:message key="common.settingssaved"/>");
        });
    </script>
</c:if>

<c:if test="${param.done}">
    <script type="text/javascript">
        $(document).ready(function () {
			$().toastmessage({sticky:false});		
            $().toastmessage("showSuccessToast", "<fmt:message key="common.done"/>");
        });
    </script>
</c:if>

<c:choose>
    <c:when test="${param.restricted eq true}">
        <c:set var="categories" value="${param.demo ? 'personal avatar share' : 'personal avatar password player share'}"/>
    </c:when>
    <c:otherwise>
        <c:set var="categories" value="${'admin musicFolder musicFolderTasks folder general advanced personal avatar default user group access icon lastfm player followMe internetRadio podcast rss transcoding cleanup playlist audioAd share network node ldap dlna sonos signup approve api audioConversion videoConversion premium'}"/>
    </c:otherwise>
</c:choose>

    <h1>
    <img src="<spring:theme code="settingsImage"/>" width="32" alt=""/>
    <fmt:message key="settingsheader.title"/>
</h1>

<h2 style="max-width:800px">
<c:forTokens items="${categories}" delims=" " var="cat" varStatus="loopStatus">
    <c:choose>
        <c:when test="${loopStatus.count > 1 and  (loopStatus.count - 1) % 50 != 0}">&nbsp;<img src="<spring:theme code="sepImage"/>" alt="">&nbsp;</c:when>
        <c:otherwise></h2><h2></c:otherwise>
    </c:choose>

    <c:url var="url" value="${cat}Settings.view?"/>

    <c:choose>
        <c:when test="${param.cat eq cat}">
            <span class="headerSelected"><fmt:message key="settingsheader.${cat}"/></span>
        </c:when>
        <c:otherwise>
            <a href="${url}"><fmt:message key="settingsheader.${cat}"/></a>
        </c:otherwise>
    </c:choose>

</c:forTokens>
</h2>

<p></p>
