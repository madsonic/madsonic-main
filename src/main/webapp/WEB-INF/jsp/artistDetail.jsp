<c:if test="${not model.customScrollbar}">
<a name="top"></a>
</c:if>    

<div id="content_artist" class="content_artist1" style="opacity:1.0;z-index: 100;"> <!-- CONTENT -->
  <div id="container" class="container"> <!-- CONTAINER -->
  
    <div class="bgcolor2 artistindex" style="opacity: 1.0; clear: both; position: fixed; top: 0; right: 0; left: 0px;
    padding: 0.15em 0.15em 0.15em 0.15em; height: 55px; z-index: 1000">
    
        <c:if test="${model.customScrollbar}">
        <a id="top"></a>
        </c:if>    
        <div style="padding-bottom:1.0em; padding-left: 10px; font-size: 14pt;">
        <c:if test="${model.customScrollbar}">
            <div id="anchor_list"><h2>
            <c:forEach items="${model.indexes}" var="index">
                <a href="?name=${index.index}" lnk="${index.index}">${index.index}</a>
            </c:forEach>
            </h2>
            </div>
        </c:if>    
        <c:if test="${not model.customScrollbar}">
            <div id="anchor_list"><h2>        
            <c:forEach items="${model.indexes}" var="index">
                <a href="?name=${index.index}" accesskey="${index.index}">${index.index}</a>
            </c:forEach>
            </h2>
            </div>            
        </c:if>    
        </div>
    </div>
    
    <div style="margin-top: 60px; padding: 0.15em 0.15em 0.15em 0.15em; z-index: 100">
    
    <fmt:message key="common.search" var="search"/>
    
    <form method="get" action="artist.view" target="main" name="artistForm">
        <table><tr>
            <td><input type="text" name="name" id="query" size="30" placeholder="${search} ..." style="padding-left:8px;margin-right:4px;" onclick="select();"></td>
            <td><a href="javascript:document.artistForm.submit()"><img src="<spring:theme code="searchImage"/>" width="32" alt="${search}" title="${search}"></a></td>
        </tr></table>
    </form>
 
    <c:if test="${model.indexedArtistHub == null}">
     <i>Select Artist Index.</i>
    </c:if>    

    <c:if test="${model.lastFMArtist == null}">
     <h2>
        <img src="<spring:theme code="warningImage"/>" alt=""/>
        No Last.FM-Data found! 
    </h2>
    <br>
    No Data exist or the Server should be re-synchronized with Last.FM Update Function.
    </c:if>    

    <c:if test="${model.lastFMArtist ne null}">
    
    <c:forEach items="${model.indexedArtistHub}" var="entry">

    <table width="740px" border="0" cellspacing="0" cellpadding="1" style="border-collapse:collapse;margin-top:15px;">
        <c:forEach items="${entry.value}" var="artist" varStatus="status">
        
        <tr><td colspan="2">
        <!-- 01 -->
        <madsonic:url value="main.view" var="mainUrl">
            <c:forEach items="${artist.mediaFiles}" var="mediaFile">
            <madsonic:param name="id" value="${mediaFile.id}"/>
            </c:forEach>
        </madsonic:url>
        <h1 style="margin: 0 0 5px;">
        <a target="main" href="${mainUrl}"><str:truncateNicely upper="${model.captionCutoff}">${artist.name}</str:truncateNicely></a> 
        </h1>
        </td></tr>

        <tr><td colspan="2">
        <h2>Artist TopTags</h2>
        </td></tr>            
        
        <tr><td colspan="2">
        <c:forEach items="${model.lastFMArtistTopTags}" var="TopTags">
        <span class="off2" onclick='changeClass(this,"on2","off2");'>${TopTags}</span>
        </c:forEach>
        <form><input type="button" value="Play Tag Radio!" onClick="playTagRadio();"></form>
        </td></tr>

        <tr><td colspan="2">
        <span class="image_stack2" style="margin-left:550px;margin-top:-60px;"> 
                <c:forEach items="${artist.albums}" var="album" varStatus="loopStatus">
                <c:choose>
                  <c:when test="${loopStatus.count <= 3}">
                    <c:import url="artistAlbums.jsp">
                        <c:param name="albumId" value="${album.mediaFileId}"/>
                        <c:param name="auth" value="${album.getHash()}"/>
                        <c:param name="albumName" value="${album.name}"/>
                        <c:param name="coverArtSize" value="50"/>
                        <c:param name="showLink" value="true"/>
                        <c:param name="showZoom" value="false"/>
                        <c:param name="showChange" value="false"/>
                        <c:param name="showCaption" value="false"/>
                        <c:param name="appearAfter" value="0"/>
                        <c:param name="count" value="${loopStatus.count}"/>
                    </c:import>
                  </c:when>
                  <c:otherwise>
                  </c:otherwise>
                </c:choose>
                </c:forEach>
        </span>        
        </td></tr>            
        
        
        <tr><td>
        
        <c:if test="${model.lastFMArtist.coverart1 ne null and model.lastFMArtist.coverart2 ne null}">
        <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.js"/>"></script>
            
        <div id="preloader">
            <div id="preloaderText">
                <span id="currentProcess"></span> 
                <span id="persent"></span>
                <div id="stopLoading">cancel</div>
                <div id="startLoading">resume</div>
            </div>
        </div>
        <div id="resizable">
            <div id = "carousel1" > 
    
                <img class = "cloudcarousel" src="${model.lastFMArtist.coverart1}" width="110">
                <img class = "cloudcarousel" src="${model.lastFMArtist.coverart2}" width="110">
                <img class = "cloudcarousel" src="${model.lastFMArtist.coverart3}" width="110">
                <img class = "cloudcarousel" src="${model.lastFMArtist.coverart4}" width="110">
                <img class = "cloudcarousel" src="${model.lastFMArtist.coverart5}" width="110">
            </div>
        </div>        
        </c:if>    

        </td>
        <td class="detailFont" style="padding:15px 0 15px;">
        ${model.lastFMArtist.summary}
        </td></tr>        
        <tr><td colspan="2">
        <h2>Artist Similar</h2>
        </td></tr>    
        <tr><td colspan="2">        
        <c:forEach items="${model.lastFMArtistSimilar}" var="similar">
        <span class="off2" onclick='changeClass(this,"on2","off2");'><a href="search.jsp?&q=${similar}" target="main" lnk="${similar}">${similar}</a></span>
        </c:forEach>
        <!--
        <c:forEach items="${model.lastFMArtistSimilar}" var="similar">
        <a href="search.jsp?&q=${similar}" target="main" lnk="${similar}">${similar}</a><br>
        </c:forEach>
        -->
        </td></tr>    
        </table>
        
        <table>        

    <tr>            
        <td colspan="2">
        <h2>Artist TopAlbum</h2>
        </td>
    </tr>    
    <!--
        <tr>    
        <td colspan="2">
        <c:forEach items="${model.lastFMArtistTopAlbums}" var="TopAlbum">
        <span class="off2" onclick='changeClass(this,"on2","off2");'>${TopAlbum}</span>
        </c:forEach>        
        </td>
        </tr>    
    -->    
        <tr><td colspan="2">
        <c:forEach items="${model.lastFMArtistTopAlbumX}" var="TopAlbums">
        <span class="off2" onclick='changeClass(this,"on2","off2");'>
        
        <c:choose>
          <c:when test="${TopAlbums.mediaFileId eq 0}">
            ${TopAlbums.albumName}
          </c:when>
          <c:otherwise>
            <a href="main.view?&id=${TopAlbums.mediaFileId}" target="main" lnk="${TopAlbums.mediaFileId}">${TopAlbums.albumName}
            <img src="icons/default/Folder.png" width="12" style="margin-top:-2px;margin-right:-2px" alt="" title="<fmt:message key="artist.open"/>">
            </a>

          </c:otherwise>
        </c:choose>
        </span>

        </c:forEach>
        </td></tr>  
        
        <tr><td width="500px">
        <h2>Artist Album</h2>
        </td>
        </tr>            
        
        <tr>
        <td style="vertical-align: text-top;">
			
        <c:forEach items="${artist.albums}" var="album" varStatus="loopStatus">
                <div style="margin-left: 8px;display: inline-flex;"></div>
                <c:import url="playAddDownload.jsp">
                <c:param name="id" value="${album.mediaFileId}"/>
                <c:param name="video" value="false"/>
                <c:param name="playEnabled" value="true"/>
                <c:param name="playAddEnabled" value="false"/>
                <c:param name="addEnabled" value="true"/>
                <c:param name="downloadEnabled" value="false"/>
                <c:param name="artist" value="${album.name}"/>
                <c:param name="title" value="${album.name}"/>
                <c:param name="starEnabled" value="false"/>
                <c:param name="starred" value="${not empty child.starredDate}"/>
                <c:param name="asTable" value="false"/>
                <c:param name="YoutubeEnabled" value="false"/>
                </c:import>

                <!-- <span class="detailmini"><c:if test="${album.playCount < 100}">0</c:if><c:if test="${album.playCount < 10}">0</c:if>${album.playCount}</span> |  -->
                <!--<div class="detailmini" style="display:inline;font-family: verdana, arial, sans-serif;"><c:if test="${album.songCount < 100}">0</c:if><c:if test="${album.songCount < 10}">0</c:if>${album.songCount}</div>
                <img src="icons/default/note.png" width="10" height="10" title="Tracks" style="margin-right: 5px;"/> -->

                <div style="display: inline-flex;display:-webkit-inline-box;padding-left:8px;padding-right:8px;">
                
                <c:choose>
                  <c:when test="${album.playCount < 100}">
                    <div style="width:${(album.playCount)/2}px;height:8px;background-color:#4CAF50;" title="Played ${album.playCount} times"></div>
                  </c:when>
                  <c:otherwise>
                    <div style="width:50px; height:8px;background-color:#4CAF50;" title="Played ${album.playCount} times"></div>
                    <img src="icons/default/plus.png" width="8" height="8" style="position:relative; left:-10px;"/>
                  </c:otherwise>
                </c:choose>
                
                <c:choose>
                  <c:when test="${album.playCount < 100}">
                        <div style="width:${(100-album.playCount)/2}px;height:8px;background-color:#888;" title="Played ${album.playCount} times"></div>
                  </c:when>
                </c:choose>
                </div>

            </div>

            <c:choose>
                  <c:when test="${album.playCount > 100}">
                    <c:if test="${album.year == 0}"><span class="detail" style="padding-right: 10px;margin-left: -10px;">[0000]</span></c:if>
                    <c:if test="${album.year > 1}"><span class="detailcolor"  style="padding-right: 10px;margin-left: -10px;">[${album.year}]</span></c:if>
                  </c:when>
                  <c:otherwise>
                    <c:if test="${album.year == 0}"><span class="detail" style="padding-right: 10px;">[0000]</span></c:if>
                    <c:if test="${album.year > 1}"><span class="detailcolor"  style="padding-right: 10px;">[${album.year}]</span></c:if>
                  </c:otherwise>
            </c:choose>
                
            <madsonic:url value="main.view" var="mediaFileIdUrl">
                <madsonic:param name="id" value="${album.mediaFileId}"/>
            </madsonic:url>
            <a target="main" href="${mediaFileIdUrl}"><str:truncateNicely upper="${model.captionCutoff}">${album.name}</str:truncateNicely></a> 
            
            <c:if test="${album.songCount ne null}"> - 
            <span class="detailcolor">${album.songCount}</span>
            </c:if>
            
            <c:if test="${album.genre ne null}"> - 
            <span class="detailcolor">(${album.genre})</span>
            </c:if>
            <br>
            </c:forEach>
        </td>
    </tr>        
        </c:forEach>
</c:forEach>
  </tr>
  
</table>
</c:if>      

