<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose> 


    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/lovedTrackService.js"/>"></script>	    
	<script type="text/javascript" src="<c:url value="/dwr/interface/playlistService.js"/>"></script>
	
    <script type="text/javascript" language="javascript">

	function init() {

    $("#dialog-select-playlist").dialog({resizable: true, width:450, height: 350, modal: false, autoOpen: false,
        buttons: {
            "<fmt:message key="common.cancel"/>": function() {
                $(this).dialog("close");
            }
        }});
    }
	
    function toggleStar(mediaFileId, element) {
        starService.star(mediaFileId, !$(element).hasClass("fa-star"));
        $(element).toggleClass("fa-star fa-star-o starred");
    }    
   
    function toggleLoved(mediaFileId, element) {
        lovedTrackService.love(mediaFileId, !$(element).hasClass("fa-heart"));
        $(element).toggleClass("fa-heart fa-heart-o loved");
    }
	</script>
</head>

<body class="mainframe bgcolor1" onload="init()"> 

<div id="content_main" class="content_main"> 
  
<div id="container" class="container">

<h1>
	<img src="<spring:theme code="starOnImage"/>" width="32" alt="">
    <fmt:message key="starred.title"/>
</h1>
<br>
<h2>
    <c:forTokens items="last artists albums songs links videos sets all" delims=" " var="cat" varStatus="loopStatus">
        <c:if test="${loopStatus.count > 1}">&nbsp;<img src="<spring:theme code="sepImage"/>" alt="">&nbsp;</c:if>
        <madsonic:url var="url" value="starred.view">
            <madsonic:param name="listType" value="${cat}"/>
        </madsonic:url>
        <c:choose>
            <c:when test="${model.listType eq cat}">
                <span class="headerSelected"><fmt:message key="starred.${cat}.title"/></span>
            </c:when>
            <c:otherwise>
                <a href="${url}"><fmt:message key="starred.${cat}.title"/></a>
            </c:otherwise>
        </c:choose>
    </c:forTokens>
</h2>

	<c:if test="${model.listType eq 'last' or model.listType eq 'all'}">

<!--<c:if test="${empty model.last}">
	<p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p>
	</c:if>-->
    
		<div id="dialog-select-playlist" title="<fmt:message key="main.addtoplaylist.title"/>" style="display: none;">
            <h2>
			<p><fmt:message key="main.addtoplaylist.text"/></p>
			<div id="dialog-select-playlist-list"></div>
            </h2>
		</div>
        
		<c:if test="${model.listType ne 'all'}">
        <br><h1 style="border-top: 1px dotted #666;"></h1>
        </c:if>
        
		<table class="music" style="border-collapse:collapse;margin-top:5px;">
			<c:forEach items="${model.last}" var="song" varStatus="loopStatus">

				<madsonic:url value="/main.view" var="mainUrl">
					<madsonic:param name="path" value="${song.parentPath}"/>
				</madsonic:url>

				<tr>
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:2px">
				<c:import url="coverArtThumb.jsp">
							<c:param name="albumId" value="${song.id}"/>
							<c:param name="auth" value="${song.hash}"/>
							<c:param name="artistName" value="${song.name}"/>
							<c:param name="coverArtSize" value="50"/>
							<c:param name="scale" value="0.5"/>
							<c:param name="showLink" value="true"/>
							<c:param name="showZoom" value="false"/>
							<c:param name="showChange" value="false"/>
							<c:param name="showArtist" value="false"/>
							<c:param name="typArtist" value="true"/>
							<c:param name="appearAfter" value="${loopStatus.count * 15}"/>
				</c:import>
				</td>                
                    <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0em">
                        <c:import url="playAddDownload.jsp">
                            <c:param name="id" value="${song.id}"/>
                            <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
                            <c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
                            <c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
                            <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addContextVisible}"/>
                            <c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addNextVisible}"/>
                            <c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addLastVisible}"/>						
                            <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
                            <c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
                            <c:param name="starred" value="${not empty song.starredDate}"/>
                            <c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
                            <c:param name="album" value="${song.mediaType ne 'MUSIC'}"/>
                            <c:param name="loved" value="${not empty song.lovedDate}"/>                        
                            <c:param name="video" value="${song.video and model.player.web}"/>
                            <c:param name="asTable" value="false"/>
                        </c:import> 
                    </td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1em;padding-right:1em">
							${song.title}
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1em">
						<a href="${mainUrl}"><span class="detail">${song.albumName}</span></a>
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1em">
						<span class="detail">${song.artist}</span>
					</td>
				</tr>
			</c:forEach>
    </table>
	</c:if>

	<c:if test="${model.listType eq 'artists' and empty model.artists}">
	<p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p>
	</c:if>
	<c:if test="${model.listType eq 'artists' or model.listType eq 'all'}">
    <c:if test="${model.listType eq 'all' and not empty model.artists}">
    <h2><fmt:message key="search.hits.artists"/><br></h2>
   	</c:if>
  	<c:if test="${not empty model.artists}">
    <br><h1 style="border-top: 1px dotted #666;"></h1>
	</c:if>	
    
    <table class="music artists" style="border-collapse:collapse;margin-top:5px;">
        <c:forEach items="${model.artists}" var="artist" varStatus="loopStatus">
            <madsonic:url value="/main.view" var="mainUrl">
                <madsonic:param name="path" value="${artist.path}"/>
            </madsonic:url>
            <tr>
            <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:2px;padding-right:5px">
            <c:import url="coverArtThumb.jsp">
                        <c:param name="albumId" value="${artist.id}"/>
                        <c:param name="auth" value="${artist.hash}"/>                            
                        <c:param name="artistName" value="${artist.name}"/>
                        <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>                            
                        <c:param name="coverArtSize" value="50"/>
                        <c:param name="scale" value="1.0"/>
                        <c:param name="showLink" value="true"/>
                        <c:param name="showZoom" value="false"/>
                        <c:param name="showChange" value="false"/>
                        <c:param name="showArtist" value="false"/>
                        <c:param name="typArtist" value="true"/>
                        <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
            </c:import>
            </td>            
			<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0em">
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${artist.id}"/>
					<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
					<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
					<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
					<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not artist.directory) and model.buttonVisibility.addContextVisible}"/>
					<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not artist.directory) and model.buttonVisibility.addNextVisible}"/>
					<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not artist.directory) and model.buttonVisibility.addLastVisible}"/>						
					<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
					<c:param name="starEnabled" value="true"/>
                    <c:param name="starred" value="${not empty artist.starredDate}"/>
                    <c:param name="asTable" value="false"/>
                </c:import>
				</td>
                <td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1em;padding-right:1em">
                    <a href="${mainUrl}">${artist.name}</a>
                </td>
            </tr>
        </c:forEach>
    </table>
	</c:if>

    
	<c:if test="${model.listType eq 'albums' and empty model.albums}">
	<p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p>
	</c:if>
	<c:if test="${model.listType eq 'albums' or model.listType eq 'all'}">
    <c:if test="${model.listType eq 'all' and not empty model.albums}">
    <br><h2><fmt:message key="search.hits.albums"/><br></h2>
    </c:if>
  	<c:if test="${not empty model.albums}">
    <br><h1 style="border-top: 1px dotted #666;"></h1>
	</c:if>	

    <table class="music albums" style="border-collapse:collapse;margin-top:5px;">
        <c:forEach items="${model.albums}" var="album" varStatus="loopStatus">

				<tr>
                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em">
                <c:import url="coverArtThumb.jsp">
                            <c:param name="albumId" value="${album.id}"/>
                            <c:param name="auth" value="${album.hash}"/>                                
                            <c:param name="artistName" value="${album.name}"/>
                            <c:param name="coverArtSize" value="50"/>
                            <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>                            
                            <c:param name="scale" value="1.0"/>
                            <c:param name="showLink" value="true"/>
                            <c:param name="showZoom" value="false"/>
                            <c:param name="showChange" value="false"/>
                            <c:param name="showArtist" value="false"/>
                            <c:param name="typArtist" value="true"/>
                            <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                </c:import>
                </td>                
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0em">
					<c:import url="playAddDownload.jsp">
						<c:param name="id" value="${album.id}"/>
                        <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
                        <c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
                        <c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
                        <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not album.directory) and model.buttonVisibility.addContextVisible}"/>
                        <c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not album.directory) and model.buttonVisibility.addNextVisible}"/>
                        <c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not album.directory) and model.buttonVisibility.addLastVisible}"/>						
                        <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
						<c:param name="starEnabled" value="true"/>
						<c:param name="starred" value="${not empty album.starredDate}"/>
						<c:param name="asTable" value="false"/>
					</c:import>
					</td>					
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:1em;padding-right:1em">
						<a href="${mainUrl}">${album.albumSetName}</a>
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1em">
						<span class="detail">${album.artist}</span>
					</td>
				</tr>

			</c:forEach>
		</table>

	</c:if>

	<c:if test="${model.listType eq 'songs' and empty model.songs}">
    <!--
	<p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p>
    -->
	</c:if>
	<c:if test="${model.listType eq 'songs' or model.listType eq 'all'}">
    <c:if test="${model.listType eq 'all' or not empty model.songs}">
    <br><h2><fmt:message key="search.hits.songs"/><br></h2>    
    <h2>	
	<select id="moreActions" onchange="actionSelected(this.options[selectedIndex].id);" style="margin-bottom:0.2em">
		<option id="top" selected="selected"><fmt:message key="main.more"/></option>
		<optgroup label="<fmt:message key="playlist.more.starred"/>"/>
		<option id="savePlaylist"><fmt:message key="playlist.save"/></option>
		<optgroup label="<fmt:message key="main.more.selection"/>"/>
		<option id="selectAll"><fmt:message key="playlist.more.selectall"/></option>
		<option id="selectNone"><fmt:message key="playlist.more.selectnone"/></option>
		<option id="appendPlaylist"><fmt:message key="playlist.append"/></option>
		<option id="saveasPlaylist"><fmt:message key="playlist.save"/></option>
		</select>
    </h2>        
	</c:if>

		<div id="dialog-select-playlist" title="<fmt:message key="main.addtoplaylist.title"/>" style="display: none;">
			<p><fmt:message key="main.addtoplaylist.text"/></p>
			<div id="dialog-select-playlist-list"></div>
		</div>
        <br><h1 style="border-top: 1px dotted #666;"></h1>
        
		<table class="music songs" style="border-collapse:collapse;margin-top:5px;">
			<c:forEach items="${model.songs}" var="song" varStatus="loopStatus">

				<madsonic:url value="/main.view" var="mainUrl">
					<madsonic:param name="path" value="${song.parentPath}"/>
				</madsonic:url>

				<tr>
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em">
				<c:import url="coverArtThumb.jsp">
							<c:param name="albumId" value="${song.id}"/>
							<c:param name="auth" value="${song.hash}"/>                            
							<c:param name="artistName" value="${song.name}"/>
							<c:param name="coverArtSize" value="50"/>
							<c:param name="scale" value="0.5"/>
							<c:param name="showLink" value="true"/>
							<c:param name="showZoom" value="false"/>
							<c:param name="showChange" value="false"/>
							<c:param name="showArtist" value="false"/>
							<c:param name="typArtist" value="true"/>
							<c:param name="appearAfter" value="${loopStatus.count * 15}"/>
				</c:import>
				</td>                
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0em">
					<c:import url="playAddDownload.jsp">
						<c:param name="id" value="${song.id}"/>
						<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
						<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
						<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
						<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addContextVisible}"/>
						<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addNextVisible}"/>
						<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addLastVisible}"/>						
						<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
						<c:param name="starEnabled" value="true"/>
						<c:param name="starred" value="${not empty song.starredDate}"/>
						<c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
						<c:param name="loved" value="${not empty song.lovedDate}"/>                        
						<c:param name="video" value="${song.video and model.player.web}"/>
						<c:param name="asTable" value="false"/>
					</c:import>
				</td>

					<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "fit"} style="padding-left:1em;padding-right:1em;max-width:15px;">
                        <input type="checkbox" class="checkbox" id="songIndex${loopStatus.count - 1}">
                        <span id="songId${loopStatus.count - 1}" style="display: none">${song.id}</span>
                    </td>				
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:0.5em;padding-right:0.5em">
						<span class="detail">${song.title}</span>
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1em">
						<a href="${mainUrl}"><span class="detail">${song.albumName}</span></a>
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1em">
						<span class="detail">${song.artist}</span>
					</td>
				</tr>
			</c:forEach>
    </table>
   	</c:if>    

	<c:if test="${model.listType eq 'videos' and empty model.videos}">	
    <!-- <p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p>  -->
	</c:if>
	<c:if test="${model.listType eq 'videos' or model.listType eq 'all'}">
    <c:if test="${model.listType eq 'all' and not empty model.videos}">
    <br><h2><fmt:message key="search.hits.videos"/><br></h2> 
   	</c:if>
  	<c:if test="${not empty model.videos}">
    <br><h1 style="border-top: 1px dotted #666;"></h1>
	</c:if>	
    
    <table class="music" style="border-collapse:collapse;margin-top:5px;">
        <c:forEach items="${model.videos}" var="song" varStatus="loopStatus">

				<madsonic:url value="/main.view" var="mainUrl">
					<madsonic:param name="id" value="${song.id}"/>
				</madsonic:url>

				<tr>
                    <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em">
                    <c:import url="coverArtThumb.jsp">
                                <c:param name="albumId" value="${song.id}"/>
                                <c:param name="auth" value="${song.hash}"/>
                                <c:param name="artistName" value="${song.name}"/>
                                <c:param name="coverArtSize" value="90"/>
                                <c:param name="scale" value="1.0"/>
                                <c:param name="showLink" value="true"/>
                                <c:param name="showZoom" value="false"/>
                                <c:param name="showChange" value="false"/>
                                <c:param name="showArtist" value="false"/>
                                <c:param name="typArtist" value="true"/>
                                <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                    </c:import>
                    </td>
                    <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.75em;padding-right:0.25em">
					<c:import url="playAddDownload.jsp">
						<c:param name="id" value="${song.id}"/>
						<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
						<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
						<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
						<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addContextVisible}"/>
						<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addNextVisible}"/>
						<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addLastVisible}"/>						
						<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
						<c:param name="starEnabled" value="true"/>
						<c:param name="starred" value="${not empty song.starredDate}"/>
						<c:param name="video" value="${song.video and model.player.web}"/>
						<c:param name="asTable" value="false"/>
					</c:import>
                    </td>
                    
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-left:0.75em;padding-right:1.25em">
							${song.title}
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
						<a href="${mainUrl}"><span class="detail">${song.albumName}</span></a>
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:0.25em">
						<span class="detail">${song.artist}</span>
					</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>

	<c:if test="${model.listType eq 'links' and empty model.links}">
    <!-- <p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p> -->
	</c:if>	
	<c:if test="${model.listType eq 'links' or model.listType eq 'all'}">
    <c:if test="${model.listType eq 'all' and not empty model.links}">
     <br><h2><fmt:message key="search.hits.links"/><br></h2> 
   	</c:if>
  	<c:if test="${not empty model.links}">
    <br><h1 style="border-top: 1px dotted #666;"></h1>
	</c:if>	
    
		<table class="music" style="border-collapse:collapse;margin-top:5px;">
			<c:forEach items="${model.links}" var="song" varStatus="loopStatus">

				<madsonic:url value="/main.view" var="mainUrl">
					<madsonic:param name="path" value="${song.parentPath}"/>
				</madsonic:url>

				<tr>
                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em">
                <c:import url="coverArtThumb.jsp">
                            <c:param name="albumId" value="${song.id}"/>
                            <c:param name="auth" value="${song.hash}"/>
                            <c:param name="artistName" value="${song.name}"/>
                            <c:param name="coverArtSize" value="50"/>
                            <c:param name="scale" value="1.0"/>
                            <c:param name="showLink" value="true"/>
                            <c:param name="showZoom" value="false"/>
                            <c:param name="showChange" value="false"/>
                            <c:param name="showArtist" value="false"/>
                            <c:param name="typArtist" value="true"/>
                            <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                </c:import>
                </td> 
                    
                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"}>
					<c:import url="playAddDownload.jsp">
						<c:param name="id" value="${song.id}"/>
						<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
						<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
						<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
						<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addContextVisible}"/>
						<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addNextVisible}"/>
						<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addLastVisible}"/>						
						<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
						<c:param name="starEnabled" value="true"/>
						<c:param name="starred" value="${not empty song.starredDate}"/>
						<c:param name="video" value="${song.video and model.player.web}"/>
						<c:param name="asTable" value="false"/>
					</c:import>
                    </td>

					<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.75em;padding-right:1.25em">
							${song.title}
					</td>

					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
						<a href="${mainUrl}"><span class="detail">${song.albumName}</span></a>
					</td>

					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:0.25em">
						<span class="detail">${song.artist}</span>
					</td>
				</tr>

			</c:forEach>
		</table>
	</c:if>

	<c:if test="${model.listType eq 'sets' and empty model.sets}">	
        <!-- <p style="padding-top: 1em"><em><fmt:message key="starred.empty"/></em></p> -->
	</c:if>	
    
	<c:if test="${model.listType eq 'sets' or model.listType eq 'all'}">
	<c:if test="${model.listType eq 'all' and not empty model.sets}">
        <h2><br><fmt:message key="search.hits.sets"/><br></h2> 
	</c:if>	
  	<c:if test="${not empty model.sets}">
    <br><h1 style="border-top: 1px dotted #666;"></h1>
	</c:if>	
    
		<table class="music" style="border-collapse:collapse">
			<c:forEach items="${model.sets}" var="song" varStatus="loopStatus">

				<madsonic:url value="/main.view" var="mainUrl">
					<madsonic:param name="id" value="${song.id}"/>
				</madsonic:url>

				<tr>
                
                    <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em">
                    <c:import url="coverArtThumb.jsp">
                                <c:param name="albumId" value="${song.id}"/>
                                <c:param name="auth" value="${song.hash}"/>
                                <c:param name="artistName" value="${song.name}"/>
                                <c:param name="coverArtSize" value="50"/>
                                <c:param name="coverArtHQ" value="${model.coverArtHQ}"/>                            
                                <c:param name="scale" value="1.0"/>
                                <c:param name="showLink" value="true"/>
                                <c:param name="showZoom" value="false"/>
                                <c:param name="showChange" value="false"/>
                                <c:param name="showArtist" value="false"/>
                                <c:param name="typArtist" value="true"/>
                                <c:param name="appearAfter" value="${loopStatus.count * 15}"/>
                    </c:import>
                    </td>
                    
                    <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.75em;padding-right:0em">
                        <c:import url="playAddDownload.jsp">
                            <c:param name="id" value="${song.id}"/>
                            <c:param name="playEnabled" value="false"/>
                            <c:param name="addEnabled" value="false"/>
                            <c:param name="downloadEnabled" value="false"/>
                            <c:param name="starEnabled" value="true"/>
                            <c:param name="starred" value="${not empty song.starredDate}"/>
                            <c:param name="video" value="${song.video and model.player.web}"/>
                            <c:param name="asTable" value="false"/>
                        </c:import>
                    </td>

					<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0em;padding-right:1.25em">
							${song.title}
					</td>

					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-right:1.25em">
						<a href="${mainUrl}"><span class="detail">${song.albumName}</span></a>
					</td>

					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1.25em">
						<span class="detail">${song.artist}</span>
					</td>
				</tr>

			</c:forEach>
		</table>
	</c:if>	

	<!-- CONTENT -->
	</div>
	
<!-- CONTAINER -->
</div>

<script type="text/javascript" language="javascript">

		function actionSelected(id) {
		
        if (id == "top") {
            return;
        } else if (id == "savePlaylist") {
            onSaveStarredPlaylist();			
        } else if (id == "selectAll") {
            selectAll(true);
        } else if (id == "selectNone") {
            selectAll(false);
        } else if (id == "appendPlaylist") {
            onAppendPlaylist();				
        } else if (id == "saveasPlaylist") {
            onSaveasPlaylist();				
		}        
		$("#moreActions").prop("selectedIndex", 0);
		}

		function getSelectedIndexes() {
			var result = "";
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
				var checkbox = $("#songIndex" + i);
				if (checkbox != null  && checkbox.is(":checked")) {
					result += "i=" + i + "&";
				}
			}
			return result;
		}

		function selectAll(b) {
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
				var checkbox = $("#songIndex" + i);
				if (checkbox != null) {
					if (b) {
						checkbox.attr("checked", "checked");
					} else {
						checkbox.removeAttr("checked");
					}
				}
			}
		}
		
		function onAppendPlaylist() {
			playlistService.getWritablePlaylists(playlistCallback);
		}
		
		function playlistCallback(playlists) {
		
			$("#dialog-select-playlist-list").empty();
			for (var i = 0; i < playlists.length; i++) {
				var playlist = playlists[i];
				$("<p class='dense'><b><a href='#' onclick='appendPlaylist(" + playlist.id + ")'>" + playlist.name + "</a></b></p>").appendTo("#dialog-select-playlist-list");
			}
			$("#dialog-select-playlist").dialog("open");
		}
		
		function appendPlaylist(playlistId) {
			$("#dialog-select-playlist").dialog("close");

			var mediaFileIds = new Array();
			
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
			
				var checkbox = $("#songIndex" + i);
				if (checkbox && checkbox.is(":checked")) {
					mediaFileIds.push($("#songId" + i).html());
				}
			}
			playlistService.appendToPlaylist(playlistId, mediaFileIds, function (){
			//parent.left.updatePlaylists();				
			$().toastmessage("showSuccessToast", "append to Playlist");				
			}); 
		}
		
		function onSavePlaylist() {
		
            selectAll(true);
			
			var mediaFileIds = new Array();
			
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
			
				var checkbox = $("#songIndex" + i);
				if (checkbox && checkbox.is(":checked")) {
					mediaFileIds.push($("#songId" + i).html());
				}
			}
			playlistService.savePlaylist(mediaFileIds, function (){
			//parent.left.updatePlaylists();
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
			});
		}
	
	
		function onSaveStarredPlaylist() {
		
            selectAll(true);
			
			var mediaFileIds = new Array();
			
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
			
				var checkbox = $("#songIndex" + i);
				if (checkbox && checkbox.is(":checked")) {
					mediaFileIds.push($("#songId" + i).html());
				}
			}
			//playlistService.saveStarredPlaylist(mediaFileIds, function (){
			//// parent.left.updatePlaylists();
            //$().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
			//});
            
            playlistService.createPlaylistForMain(mediaFileIds, null, function (playlistId) {
            // parent.left.updatePlaylists();
            parent.main.location.href = "playlist.view?id=" + playlistId;
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
        });             
            
		}
	
	
		function onSaveasPlaylist() {
		
			var mediaFileIds = new Array();
			
			for (var i = 0; i < ${fn:length(model.songs)}; i++) {
			
				var checkbox = $("#songIndex" + i);
				if (checkbox && checkbox.is(":checked")) {
					mediaFileIds.push($("#songId" + i).html());
				}
			}
			playlistService.savePlaylist(mediaFileIds, function (){
			//parent.left.updatePlaylists();
            $().toastmessage("showSuccessToast", "<fmt:message key="playlist.toast.saveasplaylist"/>");
			});
		}	
		
</script>

<c:if test="${model.customScrollbar}">

    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:850, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>  
</body>
</html>