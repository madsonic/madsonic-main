<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
	
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/lovedTrackService.js"/>"></script>		
    <script type="text/javascript" src="<c:url value="/dwr/interface/playQueueService.js"/>"></script>
    <script type="text/javascript" language="javascript">
	    function init() {
			dwr.engine.setErrorHandler(null);			
		}
	
        function toggleStar(mediaFileId, element) {
            starService.star(mediaFileId, !$(element).hasClass("fa-star"));
            $(element).toggleClass("fa-star fa-star-o starred");
        }    
       
        function toggleLoved(mediaFileId, element) {
            lovedTrackService.love(mediaFileId, !$(element).hasClass("fa-heart"));
            $(element).toggleClass("fa-heart fa-heart-o loved");
        } 
		
		function onToogleBookmark(index) {
			playQueueService.deleteBookmarkid(index);
			parent.frames.playQueue.toast("Bookmark deleted");
			parent.frames.main.location.href="bookmark.view?";
		};
	</script>
</head>

<body class="mainframe bgcolor1" onload="init();">

<body class="mainframe bgcolor1" onload="init()"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<h1>
	<img src="<spring:theme code="bookmarkImage"/>" width="32" alt="">
	<fmt:message key="bookmark.title"/>
</h1>

<br>
	<c:if test="${empty model.bookmark}">
	<p><em><fmt:message key="bookmark.empty"/></em></p>
	</c:if> 

		<div style="padding-bottom:0.35em; padding-left:0.7em;margin-top:10px;">
			<i class="fa fa-chevron-right icon control"></i>&nbsp;<a href="bookmark.view?"><fmt:message key="common.refresh"/></a>
        </div>

        <br>
        <h1 style="border-top: 1px dotted #666;"></h1>        
        
		<table class="music" style="border-collapse:collapse;margin-top:5px;">
			<c:forEach items="${model.bookmark}" var="song" varStatus="loopStatus">

				<madsonic:url value="/main.view" var="mainUrl">
					<madsonic:param name="path" value="${song.parentPath}"/>
				</madsonic:url>

				
				<tr>
                
                
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:2px">
					<c:import url="coverArtThumb.jsp">
						<c:param name="albumId" value="${song.id}"/>
						<c:param name="auth" value="${song.hash}"/>                        
						<c:param name="artistName" value="${song.name}"/>
						<c:param name="coverArtSize" value="50"/>
						<c:param name="scale" value="0.5"/>
						<c:param name="showLink" value="true"/>
						<c:param name="showZoom" value="false"/>
						<c:param name="showChange" value="false"/>
						<c:param name="showArtist" value="false"/>
						<c:param name="typArtist" value="true"/>
						<c:param name="appearAfter" value="5"/>
					</c:import>
				</td>
                
                <td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"} style="padding-left:0.5em;padding-right:0em" >
                <div class="icon-wrapper">
                <i class="fa fa-bookmark clickable custom-icon starred" style="margin-top:2px" id="bookmarkSong" onclick="onToogleBookmark(${song.id})" title="<fmt:message key="common.bookmark"/>"><span class="fix-editor">&nbsp;</span></i></div>
                </td>
               
                
				<td ${loopStatus.count % 2 == 1 ? "class='fit bgcolor2'" : "class='fit'"}>    
					<c:import url="playAddDownload.jsp">
						<c:param name="id" value="${song.id}"/>
						<c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
						<c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
						<c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
						<c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addContextVisible}"/>
						<c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addNextVisible}"/>
						<c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory) and model.buttonVisibility.addLastVisible}"/>						
						<c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode and model.buttonVisibility.downloadVisible}"/>
						<c:param name="loveEnabled" value="${model.buttonVisibility.lovedVisible}"/>
                        <c:param name="loved" value="${not empty song.lovedDate}"/>
						<c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
						<c:param name="starred" value="${not empty song.starredDate}"/>
						<c:param name="video" value="${song.video and model.player.web}"/>
						<c:param name="asTable" value="false"/>
					</c:import>
				</td>

					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-left:1em;padding-right:1em">
							${song.title}
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : "class=''"} style="padding-right:1em">
						<a href="${mainUrl}"><span class="detail">${song.albumName}</span></a>
					</td>
					<td ${loopStatus.count % 2 == 1 ? "class='bgcolor2'" : ""} style="padding-right:1em">
						<span class="detail">${song.artist}</span>
					</td>
				</tr>
			</c:forEach>
    </table>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">

    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:850, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>  
</body>
</html>