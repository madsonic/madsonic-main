<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<%--@elvariable id="model" type="Map"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	
    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>

</head>

<body class="mainframe bgcolor1"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="share"/>
    <c:param name="toast" value="${model.toast}"/>
    <c:param name="restricted" value="${not model.user.adminRole}"/>
    <c:param name="demo" value="${fn:startsWith(model.user.username, 'demo')}"/>    

</c:import>

<br>
<form method="post" action="shareSettings.view">

    <table class="music" style="border-collapse:collapse;white-space:nowrap">
        <tr>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.name"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.owner"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.description"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.lastvisited"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.visits"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.files"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.link"/></th>            
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.expires"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="sharesettings.expirein"/></th>
            <th style="padding-left:1em;text-align:left;"><fmt:message key="common.delete"/></th>
        </tr>

        <c:forEach items="${model.shareInfos}" var="shareInfo" varStatus="loopStatus">
            <c:set var="share" value="${shareInfo.share}"/>
            <c:choose>
                <c:when test="${loopStatus.count % 2 == 1}">
                    <c:set var="htmlClass" value="class='fit bgcolor2'"/>
                </c:when>
                <c:otherwise>
                    <c:set var="htmlClass" value="fit"/>
                </c:otherwise>
            </c:choose>

            <madsonic:url value="main.view" var="albumUrl">
                <madsonic:param name="id" value="${shareInfo.dir.id}"/>
            </madsonic:url>

            <tr>
                <td ${htmlClass} style="padding-left:1em"><a href="${model.shareBaseUrl}${share.name}" target="_blank">${share.name}</a></td>
                <td ${htmlClass} style="padding-left:1em">${fn:escapeXml(share.username)}</td>
                <td ${htmlClass} style="padding-left:1em"><input type="text" name="description[${share.id}]" size="50" value="${share.description}"/></td>
                <td ${htmlClass} style="padding-left:1em"><fmt:formatDate value="${share.lastVisited}" type="date" dateStyle="medium"/></td>
                <td ${htmlClass} style="padding-left:1em; text-align:center">${share.visitCount}</td>
                <td ${htmlClass} style="padding-left:1em; text-align:center">${share.fileCount}</td>
                <td ${htmlClass} style="padding-left:1em;width: 180px;"><a href="${albumUrl}" title="${shareInfo.dir.name}"><str:truncateNicely upper="30">${fn:escapeXml(shareInfo.dir.name)}</str:truncateNicely></a></td>
                <td ${htmlClass} style="padding-left:1em"><fmt:formatDate value="${share.expires}" type="date" dateStyle="medium"/></td>
                <td ${htmlClass} style="padding-left:1em">
                    <label><input type="radio" name="expireIn[${share.id}]" value="3"><fmt:message key="sharesettings.expirein.day"/></label>
                    <label><input type="radio" name="expireIn[${share.id}]" value="7"><fmt:message key="sharesettings.expirein.week"/></label>
                    <label><input type="radio" name="expireIn[${share.id}]" value="30"><fmt:message key="sharesettings.expirein.month"/></label>
                    <label><input type="radio" name="expireIn[${share.id}]" value="365"><fmt:message key="sharesettings.expirein.year"/></label>
                    <label><input type="radio" name="expireIn[${share.id}]" value="0"><fmt:message key="sharesettings.expirein.never"/></label>
                </td>
                <td ${htmlClass} style="padding-left:1em" align="center" style="padding-left:1em"><input type="checkbox" name="delete[${share.id}]" class="checkbox"/></td>
            </tr>
        </c:forEach>

        <tr style="border-bottom: 0px !important;">
            <td colspan="10" style="padding-top:1em;padding-bottom:1em; padding-left:1em;">
                <input type="submit" value="<fmt:message key="common.save"/>" style="margin-right:0.3em">
                <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'" style="margin-right:2.0em">
	        <input type="checkbox" id="deleteExpired" name="deleteExpired" class="checkbox"/>
        	<label for="deleteExpired"><fmt:message key="sharesettings.deleteexpired"/></label>
            </td>
        </tr>

    </table>
</form>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
	<script>
		(function($){
			$(window).load(function(){
				
				$("#content_main").mCustomScrollbar({
					axis:"y",
                    scrollInertia:450, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:true,
					scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
				});
			});
		})(jQuery);
        
        $("#content_main").mCustomScrollbar("update");


$("#content_main").resize(function(e){
	$("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>    
</body>
</html>
