<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="head.jsp" %>
	<%@ include file="jquery.jsp" %> 
	
	<script type="text/javascript" src="<c:url value="/script/prototype.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/fancyzoom/FancyZoom.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/fancyzoom/FancyZoomHTML.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/chatService.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/nowPlayingService.js"/>"></script>

	<!--[if lt IE 9]>
	  <script type="text/javascript" src="<c:url value="/script/excanvas/excanvas.js"/>"></script>
	<![endif]-->
	<script type="text/javascript" src="<c:url value="/script/spinners/spinners.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/script/lightview/lightview.js"/>"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/style/lightview/lightview.css"/>"></script>

    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>    
    
	<script type='text/javascript'>
		Lightview.setDefaultSkin('mac');
	</script> 
	
		
	<style type="text/css">
	/* workaround for logo link */
	.lv_skin canvas {
		left:2500px !important; 
		top:2500px !important;
	}
	.lv_shadow canvas {
	}
	.lv_bubble canvas {
	}
	</style>
</head>	

<body class="mainframe bgcolor1" onload="init()"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript">
    function init() {
        setupZoom('<c:url value="/"/>');
        dwr.engine.setErrorHandler(null);
		dwr.util.setEscapeHtml(false);		
        chatService.addMessage(" ");
    }
</script>

    <!-- This script uses AJAX to periodically retrieve what all users are playing. -->
    <script type="text/javascript" language="javascript">

        startGetNowPlayingTimer();

        function startGetNowPlayingTimer() {
            nowPlayingService.getNowPlaying(getNowPlayingCallback);
            setTimeout("startGetNowPlayingTimer()", 10000);
        }

        function getNowPlayingCallback(nowPlaying) {
            var html = nowPlaying.length == 0 ? "" : "<h2><fmt:message key="main.nowplaying"/></h2><table width=300>";
            for (var i = 0; i < nowPlaying.length; i++) {
                html += "<tr><td colspan='2' class='detail' style='padding-top:1em;white-space:nowrap'>";

                if (nowPlaying[i].avatarUrl != null) {
                    html += "<img src='" + nowPlaying[i].avatarUrl + "' style='padding-right:5pt;width:30px;height:30px'>";
                }
                html += "<b>" + nowPlaying[i].username + "</b></td></tr>";

                html += "<tr><td class='detail' style='padding-right:0.2em'>" +
                        "<a title='" + nowPlaying[i].tooltip + "' target='main' href='" + nowPlaying[i].albumUrl + "'>";

                if (nowPlaying[i].artist != null) {
                    html += nowPlaying[i].artist + "<br/>";
                }

                html += "<span class='songTitle'>" + nowPlaying[i].title + "</span></a><br/>";
                if (nowPlaying[i].lyricsUrl != null) {
                    html += "<span class='forward' style='padding-bottom:1px;'><a href='"+ nowPlaying[i].lyricsUrl + 
                        "' class='lightview' data-lightview-type='iframe' data-lightview-options='width: 600, height: 480'>" +
                        "<fmt:message key="main.lyrics"/>" + "</a></span>";
                }
                html += "</td><td>" +
                        "<a title='" + nowPlaying[i].tooltip + "' target='main' href='" + nowPlaying[i].albumUrl + "'>" +
                        "<img src='" + nowPlaying[i].coverArtZoomUrl + "' class='dropshadow' height='60' width='60'></a>" +
                        "</td></tr>";

                var minutesAgo = nowPlaying[i].minutesAgo;
                if (minutesAgo > 4) {
                    html += "<tr><td class='detail' colspan='2'>" + minutesAgo + " <fmt:message key="main.minutesago"/></td></tr>";
                }
            }
            html += "</table>";
            $('nowPlaying').innerHTML = html;
            prepZooms();
        }
    </script>

<table width=100% border=0  style="border:0px solid white; border-width: 0px;">

<!-- <table width=100% style="border:1px solid white; empty-cells:show; border-top-style:none;  border-left-style:soild; border-middle-style:none; border-bottom-style:none; border-right-style:solid; ">  -->

<Tr>
<Td valign='top' width='*'>
    <div id="nowPlaying"></div>
</td>

    <script type="text/javascript">

        var revision = 0;
        startGetMessagesTimer();

        function startGetMessagesTimer() {
            chatService.getMessages(revision, getMessagesCallback);
            setTimeout("startGetMessagesTimer()", 5000);
        }

        function addMessage() {
            chatService.addMessage($("message").value, getMessagesCallback);
            dwr.util.setValue("message", null, { escapeHtml:false });
        }
        function clearMessages() {
            chatService.clearMessages(getMessagesCallback);
        }
        function getMessagesCallback(messages) {

            if (messages == null) {
                return;
            }
            revision = messages.revision;

            // Delete all the rows except for the "pattern" row
            dwr.util.removeAllRows("chatlog", { filter:function(div) {
                return (div.id != "pattern");
            }});

            // Create a new set cloned from the pattern row
            for (var i = 0; i < messages.messages.length; i++) {
                var message = messages.messages[i];
                var id = i + 1;
                dwr.util.cloneNode("pattern", { idSuffix:id });
                dwr.util.setValue("user" + id, message.username);
                dwr.util.setValue("date" + id, " [" + formatDate(message.date) + "]");
                dwr.util.setValue("content" + id, message.content);
                $("pattern" + id).show();
            }

            var clearDiv = $("clearDiv");
            if (clearDiv) {
                if (messages.messages.length == 0) {
                    clearDiv.hide();
                } else {
                    clearDiv.show();
                }
            }
        }
        function formatDate(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var result = hours < 10 ? "0" : "";
            result += hours;
            result += ":";
            if (minutes < 10) {
                result += "0";
            }
            result += minutes;
            return result;
        }
    </script>
<td valign='top' width='*'>
    <div id="nowPlaying2"></div>
</td>
<td valign='top' width='100%'>
    <h2><fmt:message key="main.chat"/></h2>
    <div style="padding-top:0.3em;padding-bottom:0.3em">
        <input id="message" value=" <fmt:message key="main.message"/>" style="width:50%" onclick="dwr.util.setValue('message', null, { escapeHtml:false });" onkeypress="dwr.util.onReturn(event, addMessage)"/>
    </div>

    <table>
        <tbody id="chatlog">
        <tr id="pattern" style="display:none;margin:0;padding:0 0 0.15em 0;border:0"><td>
            <span id="user" class="detail" style="font-weight:bold"></span>&nbsp;<span id="date" class="detail"></span> <span id="content" class="detail"></span></td>
        </tr>
        </tbody>
    </table>
	<br>
    <c:if test="${model.user.adminRole}">
	<Table>
	<Tr>
	<Td width='150'>
		<div id="clearDiv" style="display:none;" class="forward"><a href="#" onclick="clearMessages(); return false;"> <fmt:message key="main.clearchat"/></a></div>
    </c:if>
        <div id="clearDiv" class="forward"><a href="#" onclick="dwr.util.onReturn(event, addMessage)"> Refresh Chat </a>
	</td>
	</Tr>
	</div>
	
</td>
</td>
</Table>

</div> <!-- /CONTENT -->
</div> <!-- /CONTAINER -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:true, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:false,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>

</body>
</html>