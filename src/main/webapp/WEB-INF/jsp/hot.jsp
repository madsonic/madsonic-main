<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%@ include file="include.jsp" %>

<%--
Creates HTML for displaying the rating stars.
PARAMETERS
  id: MediaFile id
  flag: HotFlag int.
--%>

<madsonic:url value="setHot.view" var="hotUrl">
	<madsonic:param name="id" value="${param.id}"/>
	<madsonic:param name="flag" value="1"/>
</madsonic:url>

<c:choose>
	<c:when test="${param.flag eq '1'}">
		<spring:theme code="hotOnImage" var="imageUrl"/>
	</c:when>
	<c:otherwise>
		<spring:theme code="hotOffImage" var="imageUrl"/>
	</c:otherwise>
</c:choose>

        <c:if test="${not param.readonly}">
            <a href="${hotUrl}">                   
        </c:if>
            <c:choose>
                <c:when test="${param.flag eq '1'}">
                    <i class="fa fa-fire clickable icon hot custom-icon-main" title="<fmt:message key="hot.rating"/>"></i>
                </c:when>
                <c:otherwise>
                    <i class="fa fa-fire clickable icon control custom-icon-main" title="<fmt:message key="hot.rating"/>"></i>
                </c:otherwise>
            </c:choose>
        <c:if test="${not param.readonly}">
            </a>
        </c:if>

<madsonic:url value="setHot.view" var="clearHotUrl">
    <madsonic:param name="id" value="${param.id}"/>
    <madsonic:param name="flag" value="0"/>
</madsonic:url>

<c:if test="${not param.readonly}">
   <!-- &nbsp;|&nbsp; -->
    <a href="${clearHotUrl}">        
    <div class="icon-wrapper"><i class="fa fa-close clickable custom-icon-main warning" title="<fmt:message key="hot.clearrating"/>"><span class="fix-editor">&nbsp;</span></i></div>
    </a>   
</c:if>