<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>

    <script type="text/javascript" language="javascript">
        function enableNodesFields() {
            $("#nodesServiceEnabled").is(":checked") ? $("#nodesTable").show() : $("#nodesTable").hide();
            $("#nodesServiceEnabled").is(":checked") ? $("#refreshButton").show() : $("#refreshButton").hide();
        }
        function send2Nodes(url) {
            parent.playQueue.onSendToNode(url);
        }
        function configNode(url) {
            parent.playQueue.onConfigNode(url);
        }
        function controlNode(url, command) {
            parent.playQueue.onControlNode(url, command);
        }
    </script>    
    
</head>
<style>
.offline {
    background-color:red; 
    padding-left:14px;
    padding-right:14px;
    border-radius:5px;    
}
.online {
    background-color:green; 
    padding-left:14px;
    padding-right:14px;
    border-radius:5px;    
}

input[type=button],input[type=submit] {
    min-width:60px !important;
}
</style>


<body class="mainframe bgcolor1" onload="enableNodesFields()"> <!-- BODY -->
<div id="content_main" class="content_main"> <!-- CONTENT -->
<div id="container" class="container"> <!-- CONTAINER -->

<script type="text/javascript" src="<c:url value="/script/wz_tooltip.js"/>"></script>
<script type="text/javascript" src="<c:url value="/script/tip_balloon.js"/>"></script>

<c:import url="settingsHeader.jsp">
    <c:param name="cat" value="node"/>
    <c:param name="toast" value="${model.reload}"/>
</c:import>
<br>
<form method="post" action="nodeSettings.view">
    <div>
        <input type="checkbox" name="nodesServiceEnabled" id="nodesServiceEnabled" onclick="enableNodesFields()" class="checkbox" 
        <c:if test="${model.nodesServiceEnabled}">checked="checked"</c:if>/>
        <label for="nodesServiceEnabled"><fmt:message key="nodes.enabled"/></label>
        <c:import url="helpToolTip.jsp"><c:param name="topic" value="nodeservice"/></c:import>
    </div>
    <br>
    <p class="detail" style="width:60%;white-space:normal">
        <fmt:message key="nodes.description"/>
    </p>
    
    <table class="indent" id="nodesTable" >
        <tr>
            <th colspan="${empty model.nodes ? 0 : 8}" style="padding-left:1em"></th>
            <th><fmt:message key="nodesettings.url"/></th>
            <th><fmt:message key="nodesettings.name"/></th>
            <th style="padding-left:15px"><fmt:message key="nodesettings.online"/></th>
            <th style="padding-left:5px"><fmt:message key="nodesettings.enabled"/></th>
            <th style="padding-left:5px"><fmt:message key="common.delete"/></th>
        </tr>
        <c:forEach items="${model.nodes}" var="node">
            <tr>
                <td><input type="submit" name="open[${node.id}]" value="open" onclick="window.open('${node.url}','_blank')"/></td>
                <td><input type="button" name="start[${node.id}]" value="start" onclick="controlNode('${node.url}','start')"/></td>
                <td><input type="button" name="pause[${node.id}]" value="pause" onclick="controlNode('${node.url}', 'pause')"/></td>
                <td><input type="button" name="resume[${node.id}]" value="resume" onclick="controlNode('${node.url}', 'resume')"/></td>
                <td><input type="button" name="skip[${node.id}]" value="skip" onclick="controlNode('${node.url}', 'skip')"/></td>
                <td><input type="button" name="stop[${node.id}]" value="stop" onclick="controlNode('${node.url}', 'stop')"/></td>
                <td><input type="button" name="transfer[${node.id}]" value="transfer" onclick="send2Nodes('${node.url}')"/></td>
                <td><input type="button" name="config[${node.id}]" value="config" onclick="configNode('${node.url}')"/></td>
                <td><input type="text" name="url[${node.id}]" size="25" value="${node.url}"/></td>
                <td style="padding-right:10px"><input type="text" name="name[${node.id}]" size="20" value="${node.name}"/></td>
                <td align="center" class=${node.online ? "online" : "offline"}><input disabled type="checkbox" ${node.online ? "checked" : ""} name="online[${node.id}]" class="checkbox"/></td>
                <td align="center" class=${node.enabled ? "online" : "offline"}><input type="checkbox" ${node.enabled ? "checked" : ""} name="enabled[${node.id}]" class="checkbox"/></td>
                <td align="center" style="padding-left:1em"><input type="checkbox" name="delete[${node.id}]" class="checkbox"/></td>
            </tr>
        </c:forEach>
        
        <c:if test="${not empty model.nodes}">
        <tr>
            <th colspan="${empty model.nodes ? 0 : 8}" style="padding-left:1em"></th>
            <th align="left" style="padding-top:1em"><fmt:message key="nodesettings.add"/></th>
        </tr>
        </c:if>
        <tr>
            <td colspan="${empty model.nodes ? 0 : 8}"> </td>
            <td ><input type="text" name="url" size="25" placeholder="<fmt:message key="nodesettings.url"/>"/></td>
            <td><input type="text" name="name" size="20" placeholder="<fmt:message key="nodesettings.name"/>"/></td>
            <td align="center" class="online"><input name="online" checked type="checkbox" class="checkbox"/></td>
            <td align="center" class="online"><input name="enabled" checked type="checkbox" class="checkbox"/></td>
            <td/>
        </tr>
        <tr>
     
            <td style="padding-top:1.5em" colspan="${empty model.nodes ? 0 : 8}"></td>
        </tr>
    </table>

    <c:set var="licenseInfo" value="${model.licenseInfo}"/>
    <%@ include file="licenseNotice.jsp" %>
    
    <br>
    <input type="submit" value="<fmt:message key="common.save"/>" style="margin-left:3px" />
    <input type="button" value="<fmt:message key="common.cancel"/>" onclick="location.href='nowPlaying.view'" />
    <input type="button" value="refresh" name="refreshButton" id="refreshButton" onclick="location.href='nodeSettings.view?'" />

</form>

<c:if test="${not empty model.error}">
    <p class="warning"><fmt:message key="${model.error}"/></p>
</c:if>

</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">
<script type="text/javascript">    

        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/                    
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);

$("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>

</body></html>