<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--@elvariable id="model" type="java.util.Map"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/script/fancyzoom/FancyZoom.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/fancyzoom/FancyZoomHTML.js"/>"></script>
    
    
    <script type="text/javascript">
    
        function toggleStar(mediaFileId, imageId) {
        if ($(imageId).attr("src").indexOf("<spring:theme code="ratingOnImage"/>") != -1) {
            $(imageId).attr("src", "<spring:theme code="ratingOffImage"/>");
            starService.unstar(mediaFileId);
        }        
        else if ($(imageId).attr("src").indexOf("<spring:theme code="ratingOffImage"/>") != -1) {
            $(imageId).attr("src", "<spring:theme code="ratingOnImage"/>");
            starService.star(mediaFileId);
        }
    }
        
    </script>

    <style type="text/css">

        .videoContainer {
            width: 282px;
            float: left;
            padding-right: 14px;
            padding-bottom: 10px;
        }      
    
        .imageContainer {
            width: 160px;
            float: left;
            padding-right: 14px;
            padding-bottom: 10px;
        }    
    
        .duration {
            font-size: 9px;
            position: absolute;
            bottom: 3px;
            right: 3px;
            color: #d3d3d3;
            background-color: black;
            opacity: 0.8;
            padding-right:4px;
            padding-left:3px;
        }
        .format {
            font-size:9;
            position: absolute;
            bottom: 3px;
            left: 4px;
            color: #d3d3d3;
            background-color: black;
            opacity: 0.8;
            padding-right:4px;
            padding-left:3px;
        }        
        
        .title {
            width:213px;
            overflow: hidden;
            text-overflow: ellipsis;
            padding-top: 3px;
        }
        .directory {
            width: 213px;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>

</head>

<body class="mainframe bgcolor1" onload="init();">
<script type="text/javascript" language="javascript">
    function init() {
        setupZoom('<c:url value="/"/>');
    };
  </script>      

<h1 style="padding-bottom: 3em">
    <span style="vertical-align: middle;">
        <c:forEach items="${model.ancestors}" var="ancestor">
            <madsonic:url value="main.view" var="ancestorUrl">
                <madsonic:param name="id" value="${ancestor.id}"/>
            </madsonic:url>
            <a href="${ancestorUrl}">${ancestor.name}</a> &raquo;
        </c:forEach>
        ${model.dir.name}
    </span>
</h1>

<c:forEach items="${model.children}" var="child">

	<c:choose>
		<c:when test="${child.video}">
        
        <madsonic:url value="/videoPlayer.view" var="videoUrl">
            <madsonic:param name="id" value="${child.id}"/>
        </madsonic:url>
        
        <madsonic:url value="/coverArt.view" var="coverArtUrl">
            <madsonic:param name="id" value="${child.id}"/>
            <madsonic:param name="size" value="160"/>
        </madsonic:url>

        <div class="videoContainer">
            <div style="position:relative">
                <div>
                    <a href="${videoUrl}"><img src="${coverArtUrl}" alt="" class="dropshadow"></a>
                </div>
                <div class="detail format">${child.format}</div>                 
                <div class="detail duration">${child.durationString}</div>
            </div>
            
            <div class="detail title" title="${child.name}" style="padding-top:10px">
            
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${child.id}"/>
                    <c:param name="video" value="${child.video and model.player.web}"/>
                    <c:param name="playEnabled" value="false"/>
                    <c:param name="playAddEnabled" value="false"/>
                    <c:param name="playMoreEnabled" value="false"/>
                    <c:param name="addEnabled" value="false"/>
                    <c:param name="addNextEnabled" value="false"/>
                    <c:param name="addLastEnabled" value="false"/>						
                    <c:param name="downloadEnabled" value="false"/>
                    <c:param name="artist" value="${fn:escapeXml(child.artist)}"/>
                    <c:param name="title" value="${child.title}"/>
                    <c:param name="starEnabled" value="true"/>
                    <c:param name="starred" value="${not empty child.starredDate}"/>
                    <c:param name="asTable" value="false"/>
                    <c:param name="YoutubeEnabled" value="false"/>
                </c:import>
            
            <b>${child.name}</b></div>
        </div>        
		</c:when>
        <c:when test="${child.audio}">        

                    <div class="imageContainer">
                        <div style="position:relative">
                            <div class="coverart" style="float:left;padding-bottom:8px;">
                                <img width="160" src="icons/default/default_cover.png">
                            </div>
                                        
                    <c:import url="playAddDownload.jsp">
                        <c:param name="id" value="${child.id}"/>
                        <c:param name="video" value="${child.video and model.player.web}"/>
                        <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playVisible}"/>
                        <c:param name="playAddEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playAddVisible}"/>
                        <c:param name="playMoreEnabled" value="${model.user.streamRole and not model.partyMode and model.buttonVisibility.playMoreVisible}"/>
                        <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addContextVisible}"/>
                        <c:param name="addNextEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addNextVisible}"/>
                        <c:param name="addLastEnabled" value="${model.user.streamRole and (not model.partyMode or not child.directory) and model.buttonVisibility.addLastVisible}"/>                        
                        <c:param name="downloadEnabled" value="false"/>
                        <c:param name="artist" value="${fn:escapeXml(child.artist)}"/>
                        <c:param name="title" value="${child.title}"/>
                        <c:param name="starEnabled" value="${model.buttonVisibility.starredVisible}"/>
                        <c:param name="starred" value="${not empty child.starredDate}"/>
                        <c:param name="asTable" value="false"/>
                        <c:param name="YoutubeEnabled" value="${model.buttonVisibility.youtubeVisible}"/>
                        <c:param name="LyricEnabled" value="${model.buttonVisibility.lyricVisible}"/>
                        </c:import>
                        
                        </div>                        
                    </div>                        
 
		</c:when>
		<c:otherwise>
        <div class="imageContainer">
            <div style="position:relative">
                <div class="coverart" style="float:left;">
                <c:import url="coverArt.jsp">
                    <c:param name="albumId" value="${child.id}"/>
                    <c:param name="albumName" value="${child.name}"/>
                    <c:param name="coverArtSize" value="160"/>
                    <c:param name="showLink" value="false"/>
                    <c:param name="showZoom" value="true"/>
                    <c:param name="showChange" value="false"/>
                    <c:param name="showCaption" value="false"/>
                    <c:param name="captionLength" value="20"/>
                    <c:param name="appearAfter" value="1"/>
                    <c:param name="showSmoother" value="true"/>                
                </c:import>
                </div>
                <b>${child.name}</b>
            </div>
        </div>
		</c:otherwise>
	</c:choose>		


</c:forEach>
</body>
</html>