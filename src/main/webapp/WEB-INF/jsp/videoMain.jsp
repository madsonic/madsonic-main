<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%--@elvariable id="model" type="java.util.Map"--%>

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <script type="text/javascript">
        var image;
        var id;
        var hash;
        var duration;
        var timer;
        var offset;
        var step;
        var size = 120;

        function startPreview(img, id, hash, duration) {
            stopPreview();
            image = $(img);
            step = Math.max(5, Math.round(duration / 50));
            offset = step;
            this.id = id;
            this.hash = hash;
            this.duration = duration;
            updatePreview();
            timer = window.setInterval(updatePreview, 1000);
        }

        function updatePreview() {
            image.attr("src", "coverArt.view?id=" + id + "&auth=" + hash + "&size=" + size + "&offset=" + offset);
            offset += step;
            if (offset > duration) {
                stopPreview();
            }
        }

        function stopPreview() {
            if (timer != null) {
                window.clearInterval(timer);
                timer = null;
            }
            if (image != null) {
                image.attr("src", "coverArt.view?id=" + id + "&auth=" + hash + "&size=" + size);
            }
        }
    </script>

    <style type="text/css">

        .tvContainer {
            width: 183px;
            float: left;
            padding-right: 14px;
            padding-bottom: 10px;
        }    
    
        .videoContainer {
            width: 213px;
            float: left;
            padding-right: 14px;
            padding-bottom: 10px;
        }
        .duration {
            font-size: 9px;
            position: absolute;
            bottom: 3px;
            right: 3px;
            color: #d3d3d3;
            background-color: rgba(0, 0, 0, 0.50);
            opacity: 0.8;
            padding-right:3px;
            padding-left:3px;
        }
        .format {
            font-size:9;
            position: absolute;
            bottom: 3px;
            left: 4px;
            color: #d3d3d3;
            background-color: rgba(0, 0, 0, 0.50);
            opacity: 0.8;
            padding-right:4px;
            padding-left:3px;
        }        
        
        .title {
            width:213px;
            overflow: hidden;
            text-overflow: ellipsis;
            padding-top: 3px;
        }
        .directory {
            width: 213px;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>

</head><body class="mainframe bgcolor1" style="margin:1.5em;">


<div style="display:flex; align-items:center; padding-top:1.8em; padding-bottom:2em">

    <div style="flex-grow:1" class="ellipsis">
        <div class="ellipsis" style="margin-bottom:0.5em">
            <c:set var="musicFolder" value="${model.musicFolder}"/>
            <c:set var="ancestors" value="${model.ancestors}"/>
            <%@ include file="indexLink.jsp" %>
        </div>
        <h1 class="ellipsis" style="flex-grow:1">${fn:escapeXml(model.dir.name)}</h1>
    </div>
</div>

<!--
<h1 style="padding-bottom: 3em">
    <span style="vertical-align: middle;">
        <c:forEach items="${model.ancestors}" var="ancestor">
            <madsonic:url value="main.view" var="ancestorUrl">
                <madsonic:param name="id" value="${ancestor.id}"/>
            </madsonic:url>
            <a href="${ancestorUrl}">${fn:escapeXml(ancestor.name)}</a> &nbsp;&bull;&nbsp;
        </c:forEach>
        ${fn:escapeXml(model.dir.name)}
    </span>
</h1>
-->

<div style="position: absolute;top:0;right:10px;padding-top:30px;z-index:1000">
    <Table>
    <tr>
        <td style="padding-left:0.2em; padding-bottom:0em;">
        <%@ include file="viewSelectorVideo.jsp" %>
        </td>
    </tr>
    </table>
</div>
    
    
<c:forEach items="${model.files}" var="child">

    <c:if test="${child.video and child.format ne 'tv'}">

        <madsonic:url value="/videoPlayer.view" var="videoUrl">
            <madsonic:param name="id" value="${child.id}"/>
        </madsonic:url>
        
        <madsonic:url value="/coverArt.view" var="coverArtUrl">
            <madsonic:param name="id" value="${child.id}"/>
            <madsonic:param name="auth" value="${child.hash}"/>            
            <madsonic:param name="size" value="120"/>
        </madsonic:url>

        <div class="videoContainer">
            <div style="position:relative">
                <div>
                    <a href="${videoUrl}"><img src="${coverArtUrl}" alt="" class="dropshadow" 
                                               onmouseover="startPreview(this, ${child.id}, ${child.hash}, ${child.durationSeconds})"
                                               onmouseout="stopPreview()"></a>
                </div>
                <div class="detail format">${child.format}</div>                 
                <div class="detail duration">${child.durationString}</div>
            </div>
            
            <div class="detail title" title="${child.name}" style="padding-top:10px">
            
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${child.id}"/>
                    <c:param name="video" value="${child.video and model.player.web}"/>
                    <c:param name="playEnabled" value="false"/>
                    <c:param name="playAddEnabled" value="false"/>
                    <c:param name="playMoreEnabled" value="false"/>
                    <c:param name="addEnabled" value="false"/>
                    <c:param name="addNextEnabled" value="false"/>
                    <c:param name="addLastEnabled" value="false"/>						
                    <c:param name="downloadEnabled" value="false"/>
                    <c:param name="artist" value="${fn:escapeXml(child.artist)}"/>
                    <c:param name="title" value="${child.title}"/>
                    <c:param name="starEnabled" value="false"/>
                    <c:param name="starred" value="${not empty child.starredDate}"/>
                    <c:param name="asTable" value="false"/>
                    <c:param name="YoutubeEnabled" value="false"/>
                </c:import>
            
            <b>${child.name}</b></div>
        </div>
    </c:if>

    <c:if test="${child.format eq 'tv'}">

        <madsonic:url value="/videoPlayer.view" var="videoUrl">
            <madsonic:param name="id" value="${child.id}"/>
        </madsonic:url>
        
        <madsonic:url value="/coverArt.view" var="coverArtUrl">
            <madsonic:param name="id" value="${child.id}"/>
            <madsonic:param name="auth" value="${child.hash}"/>
            <madsonic:param name="size" value="120"/>
        </madsonic:url>

        <div class="tvContainer">
            <div style="position:relative">
                <div>
                    <a href="${videoUrl}"><img src="${coverArtUrl}" alt="" class="dropshadow tv"></a>
                </div>
                <div class="detail format">${child.format}</div>                 
                <div class="detail duration">${child.durationString}</div>
            </div>
            
            <div class="detail title" title="${child.name}" style="padding-top:10px">
            
                <c:import url="playAddDownload.jsp">
                    <c:param name="id" value="${child.id}"/>
                    <c:param name="video" value="${child.tv and model.player.web}"/>
                    <c:param name="playEnabled" value="false"/>
                    <c:param name="playAddEnabled" value="false"/>
                    <c:param name="playMoreEnabled" value="false"/>
                    <c:param name="addEnabled" value="false"/>
                    <c:param name="addNextEnabled" value="false"/>
                    <c:param name="addLastEnabled" value="false"/>						
                    <c:param name="downloadEnabled" value="false"/>
                    <c:param name="artist" value="${fn:escapeXml(child.artist)}"/>
                    <c:param name="title" value="${child.title}"/>
                    <c:param name="starEnabled" value="false"/>
                    <c:param name="starred" value="${not empty child.starredDate}"/>
                    <c:param name="asTable" value="false"/>
                    <c:param name="YoutubeEnabled" value="false"/>
                </c:import>
            
            <b>${child.name}</b></div>
        </div>
    </c:if>
    
    
</c:forEach>

<div style="clear:both;padding-top: 1em">
    <c:set var="cssClass" value="directory"/>
    <c:forEach items="${model.children}" var="child" varStatus="loopStatus">
        <c:if test="${child.directory}">
            <c:choose>
                <c:when test="${cssClass eq 'directory'}">
                    <c:set var="cssClass" value="bgcolor2 directory"/>
                </c:when>
                <c:otherwise>
                    <c:set var="cssClass" value="directory"/>
                </c:otherwise>
            </c:choose>
            <madsonic:url value="main.view" var="childUrl">
                <madsonic:param name="id" value="${child.id}"/>
            </madsonic:url>

            <div class="${cssClass}">
                <a href="${childUrl}" title="${child.name}"><span style="white-space:nowrap;">${child.name}</span></a>
            </div>
        </c:if>
    </c:forEach>
</div>

</body>
</html>
