<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">    
    
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    
    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-2.1.2.js"/>"></script>    

    <style type="text/css">
    
        .artistindex {
            opacity: 1.0;
            clear: both;
            position: fixed;
            top: 0;
            right: 0;
            margin: 0px 0px 0px 0px;
            padding: 5px 5px 5px 5px;
            height: 100%;
            z-index: 1000;
            text-align: center;
        }
    
        .browse-selected-music-folder {
            padding: 0.1em 0.5em;
            border:1px solid #<spring:theme code="detailColor"/>;
            margin-right: 2em;
        }
        .browse-index-shortcut {
            padding-bottom: 8px;
            font-size: 105%;
            line-height: 100%;
        }
        .browse-index-title {
            clear: both;
            font-size: 4em;
            line-height: 100%;
            padding-top: 20px;
            padding-bottom: 5px;
        }
        .browse-artist {
            float: left;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            width: 12em;
            padding: 0.2em 1.5em 0.2em 1.5em;
            font-weight: 300;
        }
    </style>

    <script type="text/javascript" language="javascript">

        function toggleStar(mediaFileId, element) {
            starService.star(mediaFileId, !$(element).hasClass("fa-star"));
            $(element).toggleClass("fa-star fa-star-o starred");
        }

        function filterArtists(element) {
            var filter = $(element).val().toLowerCase();
            $(".browse-artist").each(function(i) {
                var artist = $(this).text().toLowerCase();
                $(this).toggle(artist.indexOf(filter) != -1);
            });
        }
    </script>
</head>



<body class="bgcolor1 mainframe" style="margin-left:20px; margin-right:40px;">

<!-- content block -->

<div id="content_main" class="content_main">
<!-- CONTENT -->

<a name="top"></a>
<h1 style="padding-bottom:1em"><i class="fa fa-microphone fa-lg icon"></i>&nbsp;&nbsp;<fmt:message key="common.artists"/></h1>

    <div class="bgcolor2 artistindex" style="">

        <h2>
        <div id="anchor_list">
        <div class="browse-index-shortcut"><i class="fa fa-arrow-up fa-fw icon clickable" onclick="location.href='#top'"></i></div>
        <c:forEach items="${model.indexes}" var="index">
            <div class="browse-index-shortcut"><a href="#${index.index}" lnk="${index.index}">${index.index}</a></div>
        </c:forEach>
        </div>
        </h2>
    </div>
    
<div style="padding-bottom:1.5em">
    <c:if test="${not empty model.selectedMusicFolder}">
        <span class="browse-selected-music-folder bgcolor2"><i class="fa fa-folder-open-o fa-fw icon"></i>&nbsp;${fn:escapeXml(model.selectedMusicFolder.name)}</span>
    </c:if>
    <span>
        <c:choose>
            <c:when test="${model.scanning}">
                <i class="fa fa-refresh fa-fw icon"></i>&nbsp;<a href="artists.view"><fmt:message key="common.refresh"/></a>
            </c:when>
            <c:otherwise>
                <i class="fa fa-refresh fa-fw icon"></i>&nbsp;<a href="artists.view?refresh=true"><fmt:message key="common.refresh"/></a>
            </c:otherwise>
        </c:choose>
    </span>
    <div style="float:right; padding-right:3em">
        <input type="text" size="28" placeholder="<fmt:message key="common.filter"/>" onclick="select();" onkeyup="filterArtists(this)">
    </div>
</div>

<div style="padding-bottom:0.4em">
    <c:forEach items="${model.shortcuts}" var="shortcut">
        <madsonic:url value="main.view" var="mainUrl">
            <madsonic:param name="id" value="${shortcut.id}"/>
        </madsonic:url>
        <input type="button" style="margin-right:0.6em; margin-bottom:0.6em" value="${fn:escapeXml(shortcut.name)}" onclick="location.href='${mainUrl}'">
    </c:forEach>
</div>

<c:forEach items="${model.indexedArtists}" var="entry">
    <a name="${fn:escapeXml(entry.key.index)}"></a>
    <div class="browse-index-title">${fn:escapeXml(entry.key.index)}</div>

    <c:forEach items="${entry.value}" var="artist">
        <madsonic:url value="main.view" var="mainUrl">
            <c:forEach items="${artist.mediaFiles}" var="mediaFile">
                <madsonic:param name="id" value="${mediaFile.id}"/>
            </c:forEach>
        </madsonic:url>
        
        <div class="browse-artist">
        
            <!--
            <c:import url="coverArt.jsp">
            <c:param name="albumId" value="${artist.mediaFiles[0].id}"/>
            <c:param name="albumName" value="${mediaFile.name}"/>
            <c:param name="coverArtSize" value="160"/>
            <c:param name="typArtist" value="true"/>
            <c:param name="showLink" value="false"/>
            <c:param name="showZoom" value="false"/>
            <c:param name="showChange" value="false"/>
            <c:param name="showCaption" value="false"/>
            <c:param name="appearAfter" value="5"/>
            </c:import>        
            -->
            
            <a target="main" href="${mainUrl}" title="${fn:escapeXml(artist.name)}">${fn:escapeXml(artist.name)}</a>
        </div>
    </c:forEach>
</c:forEach>

<div style="clear:both; padding-top:2em"></div>

<table class="music">
    <c:forEach items="${model.singleSongs}" var="song" varStatus="loopStatus">
        <%--@elvariable id="song" type="ord.madsonic.domain.MediaFile"--%>
        <tr style="margin:0;padding:0;border:0">
            <c:import url="playButtons.jsp">
                <c:param name="id" value="${song.id}"/>
                <c:param name="video" value="${song.video and model.player.web}"/>
                <c:param name="playEnabled" value="${model.user.streamRole and not model.partyMode}"/>
                <c:param name="addEnabled" value="${model.user.streamRole and (not model.partyMode or not song.directory)}"/>
                <c:param name="downloadEnabled" value="${model.user.downloadRole and not model.partyMode}"/>
                <c:param name="starEnabled" value="true"/>
                <c:param name="starred" value="${not empty song.starredDate}"/>
                <c:param name="asTable" value="true"/>
            </c:import>

            <c:if test="${model.visibility.trackNumberVisible}">
                <td class="fit rightalign">
                    <span class="detail">${song.trackNumber}</span>
                </td>
            </c:if>

            <td class="truncate">
                <span class="songTitle" title="${fn:escapeXml(song.title)}">${fn:escapeXml(song.title)}</span>
            </td>

            <c:if test="${model.visibility.albumVisible}">
                <td class="truncate">
                    <span class="detail" title="${fn:escapeXml(song.albumName)}">${fn:escapeXml(song.albumName)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.artistVisible}">
                <td class="truncate">
                    <span class="detail" title="${fn:escapeXml(song.artist)}">${fn:escapeXml(song.artist)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.genreVisible}">
                <td class="fit rightalign">
                    <span class="detail">${fn:escapeXml(song.genre)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.yearVisible}">
                <td class="fit rightalign">
                    <span class="detail">${song.year}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.formatVisible}">
                <td class="fit rightalign">
                    <span class="detail">${fn:toLowerCase(song.format)}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.fileSizeVisible}">
                <td class="fit rightalign">
                    <span class="detail"><sub:formatBytes bytes="${song.fileSize}"/></span>
                </td>
            </c:if>

            <c:if test="${model.visibility.durationVisible}">
                <td class="fit rightalign">
                    <span class="detail">${song.durationString}</span>
                </td>
            </c:if>

            <c:if test="${model.visibility.bitRateVisible}">
                <td class="fit rightalign">
                    <span class="detail">
                        <c:if test="${not empty song.bitRate}">
                            ${song.bitRate} Kbps ${song.variableBitRate ? "vbr" : ""}
                        </c:if>
                        <c:if test="${song.video and not empty song.width and not empty song.height}">
                            (${song.width}x${song.height})
                        </c:if>
                    </span>
                </td>
            </c:if>
        </tr>
    </c:forEach>
</table>

<div style="padding-top:2em"></div>

	<script>
    $('#anchor_list>a').click(function(){
        var thisPos = $("#"+ $(this).attr("lnk")).position(); 
        $.scrollTo(thisPos.top -170 + 'px', 800);
    });        
	</script>

<!-- CONTENT -->
</div>    
    
</body></html>