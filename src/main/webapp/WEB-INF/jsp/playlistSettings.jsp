<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
	<%@ include file="customScrollbar.jsp" %>
    <script type="text/javascript" src="<c:url value="/script/scripts.js"/>"></script>
</head>
<body class="mainframe bgcolor1"> <!-- BODY -->
  <div id="content_main" class="content_main"> <!-- CONTENT -->
	<div id="container" class="container"> <!-- CONTAINER -->
	<c:import url="settingsHeader.jsp">
		<c:param name="cat" value="playlist"/>
		<c:param name="toast" value="${model.toast}"/>
		<c:param name="done" value="${model.done}"/>
		<c:param name="warn" value="${model.warn}"/>
		<c:param name="warnInfo" value="${model.warnInfo}"/>
		<c:param name="bug" value="${model.bug}"/>    
		<c:param name="bugInfo" value="${model.bugInfo}"/>        
	</c:import>
	<br>
	<p class="forward"><a href="playlists.view?">View Playlists</a></p>
	<!--<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;">View all Playlists.</p>-->
	<br>
	<p class="forward"><a href="generalSettings.view?">View Settings</a></p>
	<!--<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;">View Playlist Settings.</p>-->
	<br>
	<p class="forward"><a href="playlistEditor.view?">Use Playlist Editor</a></p>
	<!--<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;">Create a new Playlists.</p>-->
	<br>
	<p class="forward"><a href="importPlaylist.view?">Import Playlist</a></p>
	<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;">Import Playlists into Madsonic Database.</p>
	<br>
	<p class="forward"><a href="playlistSettings.view?exportPlaylists"><fmt:message key="cleanupsettings.exportplaylist.title"/></a></p>
	<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="cleanupsettings.exportplaylist"><fmt:param value="${model.exportType}"/></fmt:message>: <b>${model.exportfolder}</b><br></p>
	<br>
	<p class="forward"><a href="playlistSettings.view?backupPlaylists">Backup Playlists</a></p>
	<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="cleanupsettings.backupplaylist"><fmt:param value="${model.exportType}"/></fmt:message>: <b>${model.backupfolder}</b><br></p>
	<br>
	<p class="forward"><a href="playlistSettings.view?cleanupPlaylists">Cleanup Backup Playlists</a></p>
	<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><i class="fa fa-exclamation-triangle fa-lg fa-fw icon" style="color: #FFC107"></i> Delete all Playlists from the specified Folder: <b>${model.backupfolder}</b></p>
	<br>
	<p class="forward"><a href="playlistSettings.view?deletePlaylists">Delete all Playlists</a></p>
	<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><i class="fa fa-exclamation-triangle fa-lg fa-fw icon" style="color: #FFC107"></i> Delete all Playlists from Database only.</p>
	<br>
	<p class="forward"><a href="playlistSettings.view?recoveryPlaylists">Recover Playlists</a></p>
	<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;">Restore all Playlists from the specified Folder: <b>${model.backupfolder}</b></p>
	<br>
	<!--
	<p class="forward"><a href="playlistSettings.view?resetPlaylists"><fmt:message key="cleanupsettings.resetplaylist.title"/></a></p>
	<p class="detail" style="padding-left: 20px;width:80%;white-space:normal;margin-top:-5px;"><fmt:message key="cleanupsettings.resetplaylist"/></p>
	<br>-->

	<c:if test="${not empty model.error}">
		<p class="warning"><fmt:message key="${model.error}"/></p>
	</c:if>

	<c:if test="${model.reload}">
		<script language="javascript" type="text/javascript">parent.frames.left.location.href="left.view?"</script>
	</c:if>

	</div> <!-- CONTAINER -->
</div> <!-- CONTENT -->

<c:if test="${model.customScrollbar}">
    <script>
        (function($){
            $(window).load(function(){
                
                $("#content_main").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:false, /*auto-hide scrollbar when idle*/   
                    alwaysShowScrollbar:true,
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/},
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
        
        $("#content_main").mCustomScrollbar("update");


$("#content_main").resize(function(e){
    $("#content_main").mCustomScrollbar("update");
});
</script>
</c:if>    

</body>

</html>