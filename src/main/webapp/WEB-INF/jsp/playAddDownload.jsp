<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<%@ include file="include.jsp" %>

<%--
PARAMETERS
  id: ID of file.
  album: Whether the file is a album, albumset.
  video: Whether the file is a video (default false).
  playEnabled: Whether the current user is allowed to play songs (default true).
  playMoreEnabled: 
  addEnabled: Whether the current user is allowed to add songs to the playlist (default true).
  addNextEnabled: Whether the current user is allowed to add songs to the playlist (default flase).
  addLastEnabled: Whether the current user is allowed to add songs to the playlist (default false).
  downloadEnabled: Whether the current user is allowed to download songs (default false).
  starEnabled: Whether to show star/unstar controls (default false).
  loveEnabled: Whether to show love/unlove controls (default false).
  starred: Whether the file is currently starred.
  loved: Whether the file is currently loved.
  asTable: Whether to put the images in td tags.
  YoutubeEnabled: Whether to show Youtube Control.
  LyricEnabled: Whether to show LyricURL.  
  onPlay: Overrides the javascript used for the play action.
--%>

<madsonic:url value="/download.view" var="downloadUrl">
    <madsonic:param name="id" value="${param.id}"/>
</madsonic:url>

<c:if test="${param.loveEnabled}">
<c:if test="${param.asTable}"><td class="fit"></c:if>

<c:choose>
<c:when test="${param.album}">
<div class="icon-wrapper">
<i class="fa fa-ban custom-icon"></i></div>
</c:when>
<c:otherwise>
<div class="icon-wrapper">
<i class="fa ${param.loved ? 'fa-heart loved' : 'fa-heart-o'} clickable custom-icon" onclick="toggleLoved(${param.id}, this)"title="<fmt:message key="common.love"/>"><span class="fix-editor">&nbsp;</span></i></div>
</c:otherwise>
</c:choose>
<c:if test="${param.asTable}"></td></c:if>
</c:if>


<c:if test="${param.starEnabled}">
<c:if test="${param.asTable}"><td class="fit"></c:if>
<div class="icon-wrapper">
<i class="fa ${param.starred ? 'fa-star starred' : 'fa-star-o'} clickable custom-icon" onclick="toggleStar(${param.id}, this)"title="<fmt:message key="common.star"/>"><span class="fix-editor">&nbsp;</span></i></div>
<c:if test="${param.asTable}"></td></c:if>
</c:if>


<c:if test="${empty param.playEnabled or param.playEnabled}">
    <c:choose>
        <c:when test="${param.video}">
            <madsonic:url value="/videoPlayer.view" var="videoUrl">
                <madsonic:param name="id" value="${param.id}"/>
            </madsonic:url>
            
            <%-- Open video in new window if Chromecast is already initialized in play queue. --%>
			<c:if test="${param.asTable}"><td class="fit"></c:if>
                <div class="icon-wrapper"><i class="fa fa-play clickable custom-icon" onclick="location.href='${videoUrl}'"title="<fmt:message key="common.play"/>"><span class="fix-editor">&nbsp;</span></i></div>
            <c:if test="${param.asTable}"></td></c:if>				
        </c:when>
        
        <c:when test="${not empty param.onPlay}">
		<c:if test="${param.asTable}"><td class="fit"></c:if>				
            <i class="fa fa-play clickable icon custom-icon" onclick="${param.onPlay}" title="<fmt:message key="common.play"/>"></i>
		<c:if test="${param.asTable}"></td></c:if>				
        </c:when>        
        
        <c:otherwise>
		<c:if test="${param.asTable}"><td class="fit"></c:if>
        <div class="icon-wrapper"><i class="fa fa-play clickable custom-icon" onclick="parent.playQueue.onPlay(${param.id})"title="<fmt:message key="common.play"/>"><span class="fix-editor">&nbsp;</span></i></div>
		<c:if test="${param.asTable}"></td></c:if>		
		
		<c:if test="${param.playAddEnabled}">
		<c:if test="${param.asTable}"><td class="fit"></c:if>
            <div class="icon-wrapper">
            <span class="fa-stack custom-icon" onclick="parent.playQueue.onPlayAdd(${param.id})" title="<fmt:message key="common.playadd"/>">
              <i class="fa fa-play fa-stack-1x" style="margin-left:-2px;margin-top:-3px"></i>
              <i class="fa fa-plus fa-stack-1x" style="margin-left:3px;margin-top:2px"></i>
            </span>
            </div>      

            
		<c:if test="${param.asTable}"></td></c:if>
		</c:if>
		
		<c:if test="${param.playMoreEnabled}">
			<c:if test="${param.asTable}"><td class="fit"></c:if>
            <div class="icon-wrapper">
            <span class="fa-stack custom-icon" onclick="parent.playQueue.onPlayMore(${param.id})" title="<fmt:message key="common.playmore"/>">
              <i class="fa fa-play fa-stack-1x" style="margin-left:-2px;margin-top:-3px"></i>
              <i class="fa fa-file-text-o fa-stack-1x" style="margin-left:3px;margin-top:2px"></i>
            </span>
            </div>            
			<c:if test="${param.asTable}"></td></c:if>
		</c:if>
			
        </c:otherwise>
    </c:choose>
</c:if>
		
<c:if test="${(empty param.addEnabled or param.addEnabled) and not param.video}">
<c:if test="${param.asTable}"><td class="fit"></c:if>
    <div class="icon-wrapper"><i class="fa fa-plus custom-icon" id="add${param.id}" title="<fmt:message key="common.add"/>"><span class="fix-editor">&nbsp;</span></i></div>
<c:if test="${param.asTable}"></td></c:if>
</c:if>

<c:if test="${(param.addNextEnabled) and not param.video}">
<c:if test="${param.asTable}"><td class="fit"></c:if>
    <i class="fa fa-plus-circle clickable icon custom-icon" onclick="parent.playQueue.onAddNext(${param.id});" title="<fmt:message key="common.addnext"/>"></i>        
 <c:if test="${param.asTable}"></td></c:if>
</c:if>

<c:if test="${(param.addLastEnabled) and not param.video}">
<c:if test="${param.asTable}"><td class="fit"></c:if>
    <i class="fa fa-plus-circle clickable icon custom-icon" onclick="parent.playQueue.onAdd(${param.id});" title="<fmt:message key="common.addlast"/>"></i>        
<c:if test="${param.asTable}"></td></c:if>
</c:if>

<c:if test="${param.video}">
<c:if test="${param.playAddEnabled and param.asTable}"><td></td></c:if>
<c:if test="${param.playMoreEnabled and param.asTable}"><td></td></c:if>
<c:if test="${param.addEnabled and param.asTable}"><td></td></c:if>
<c:if test="${param.addNextEnabled and param.asTable}"><td></td></c:if>
<c:if test="${param.addLastEnabled and param.asTable}"><td></td></c:if>
</c:if>

<c:if test="${param.downloadEnabled}">
<c:if test="${param.asTable}"><td class="fit"></c:if>
    <div class="icon-wrapper"><i class="fa fa-download clickable custom-icon" onclick="location.href='${downloadUrl}'" title="<fmt:message key="common.download"/>"><span class="fix-editor">&nbsp;</span></i></div>
<c:if test="${param.asTable}"></td></c:if>			 
</c:if>

<c:if test="${model.dir.album}">
	<c:if test="${param.asTable}"><td class="fit"></c:if>
		<c:if test="${param.YoutubeEnabled}">

		<madsonic:url value="http://www.youtube.com/results" var="YoutubeUrl" encoding="UTF-8">
			<madsonic:param name="search_query" value="${param.artist} ${param.title}"/>
		</madsonic:url>
        <div class="icon-wrapper"><i class="fa fa-youtube clickable custom-icon" onclick="window.open('${YoutubeUrl}', '_blank')" title="<fmt:message key="common.youtube"/>""><span class="fix-editor">&nbsp;</span></i></div>
		<c:if test="${param.asTable}"></td></c:if>
		</c:if>
</c:if>

<c:if test="${model.dir.album}">
	<c:if test="${param.asTable}"><td></c:if>
		<c:if test="${param.LyricEnabled}">    
        <madsonic:url value="/lyrics.view" var="lyricUrl">
            <madsonic:param name="id" value="${param.id}"/>
        </madsonic:url>
		<a href="${lyricUrl}" class="lightview clickable" data-lightview-type="iframe" data-lightview-options="width: 600, height: 480">
        <div class="icon-wrapper"><i class="fa fa-file-text-o custom-icon control" title="<fmt:message key="common.lyric"/>""><span class="fix-editor">&nbsp;</span></i></div>
        </a>
		<c:if test="${param.asTable}"></td></c:if>
		</c:if>
</c:if>

<script type="text/javascript">
    jQuery(function () {
        jQuery.contextMenu({
            selector:'#add${param.id}',
            trigger:'left',
            callback:function (key, options) {
                if (key == "addnext") {
                    parent.playQueue.onAddNext(${param.id});
                } else {
                    parent.playQueue.onAdd(${param.id});
                }
            },
            items:{
                "addnext":{name:"<fmt:message key="main.addnext"/>"},
                "addlast":{name:"<fmt:message key="main.addlast"/>"}
            }
        });
    });
</script>
