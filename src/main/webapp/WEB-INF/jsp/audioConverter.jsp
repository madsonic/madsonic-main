<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1" %>

<html>
<head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>

	<link rel="stylesheet" href='<c:url value="/script/fontawesome/css/font-awesome.min.css"/>' type="text/css">	
	
    <style type="text/css">
        .ui-progressbar-value {
            background: #0090BB;
        }
    </style>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>

    <script type="text/javascript" language="javascript">

        var authorized = ${model.user.audioConversionRole};
        var updateInterval = 2000;

        function init() {
            $("#conversion-progressbar").progressbar({
                max: ${model.audio.durationSeconds}
            });
            dwr.engine.setErrorHandler(null);
            updateConversionStatus();
        }
        function updateConversionStatus() {
            multiService.getAudioConversionStatus(${model.audio.id}, conversionStatusCallback);
            setTimeout(updateConversionStatus, updateInterval);
        }
        function conversionStatusCallback(conversionStatus) {
            $("#conversion-progressbar").progressbar("option", "value", conversionStatus == null ? 0 : conversionStatus.progressSeconds);
            $("#conversion-status-new").toggle(conversionStatus != null && conversionStatus.statusNew);
            $("#conversion-status-in-progress").toggle(conversionStatus != null && conversionStatus.statusInProgress);
            $("#conversion-status-completed").toggle(conversionStatus != null && conversionStatus.statusCompleted);
            $("#conversion-status-error").toggle(conversionStatus != null && conversionStatus.statusError);
            $("#conversion-download-converted").toggle(conversionStatus != null && conversionStatus.statusCompleted);
            $("#conversion-start-mp3-192").toggle(authorized && (conversionStatus == null || conversionStatus.statusError));
            $("#conversion-start-mp3-320").toggle(authorized && (conversionStatus == null || conversionStatus.statusError));
            $("#conversion-start-ogg-320").toggle(authorized && (conversionStatus == null || conversionStatus.statusError));
            $("#conversion-cancel").toggle(authorized && (conversionStatus != null && (conversionStatus.statusNew || conversionStatus.statusInProgress)));

            $("#conversion-target-file").text(conversionStatus == null ? "" : conversionStatus.targetFile);
            $("#conversion-log-file").text(conversionStatus == null ? "" : conversionStatus.logFile);

            if (conversionStatus != null && conversionStatus.statusInProgress) {
                $("#conversion-status-progress").text(conversionStatus.progressString);
                updateInterval = 600;
            } else {
                updateInterval = 2000;
            }
        }
        function startConversion(bitrate, format) {
            multiService.startAudioConversion(${model.audio.id}, bitrate, format, conversionStatusCallback);
        }
        function cancelConversion() {
            multiService.cancelAudioConversion(${model.audio.id}, conversionStatusCallback);
        }
        
        function startDownload(raw) {
            if (raw) {
                location.href = "download.view?id=${model.audio.id}&format=raw";
            } else {
                location.href = "download.view?id=${model.audio.id}";
            }
        }

        function back() {
            if (document.referrer) {
                location.href = document.referrer;
            } else {
                history.back();
            }
        }
    </script>
</head>

<body class="mainframe bgcolor1" style="padding-bottom:0.5em" onload="init()">

<h1><i class="fa fa-film fa-fw icon"></i>&nbsp;&nbsp;<fmt:message key="audioConverter.title"/></h1>

<p style="margin-top:1.5em">
    <fmt:message key="audioConverter.intro"/>
</p>

<div style="margin: 2em 3em;">
    <div style="float:left; width:213px; margin-right: 3em">
        <div id="conversion-progressbar" style="width:100%; height:10px; margin-top:5px; margin-bottom:5px;"></div>
        <c:if test="${model.licenseInfo.licenseOrTrialValid}">
            <input id="conversion-start-mp3-192" style="display:none; width:100%; margin-top:1em;cursor:pointer" type="button" value="<fmt:message key="audioConverter.start"><fmt:param value="mp3 192kbps"/></fmt:message>" onclick="startConversion(192,'mp3')">
            <input id="conversion-start-mp3-320" style="display:none; width:100%; margin-top:1em;cursor:pointer" type="button" value="<fmt:message key="audioConverter.start"><fmt:param value="mp3 320kbps"/></fmt:message>" onclick="startConversion(320,'mp3')">
            <input id="conversion-start-ogg-320" style="display:none; width:100%; margin-top:1em;cursor:pointer" type="button" value="<fmt:message key="audioConverter.start"><fmt:param value="ogg 320kbps"/></fmt:message>" onclick="startConversion(320,'ogg')">
            <input id="conversion-cancel" style="display:none; width:100%; margin-top:1em;cursor:pointer" type="button" value="<fmt:message key="audioConverter.cancel"/>" onclick="cancelConversion()">
            <input id="conversion-download-converted" style="display:none; width:100%; margin-top:1em;cursor:pointer" type="submit" value="<fmt:message key="audioConverter.download"><fmt:param value="converted"/></fmt:message>" onclick="startDownload(false)"></c:if>
            <input id="conversion-download-original" type="submit" style="display:inline; width:100%; margin-top:1em;cursor:pointer" type="button" value="<fmt:message key="audioConverter.download"><fmt:param value="${model.audio.format}"/></fmt:message>" onclick="startDownload(true)">
    </div>
    
    <table class="detail" style="float:left">
        <tr><td style="padding-right:1em"><b><fmt:message key="audioConverter.details.title"/></b></td><td><b>${model.audio.title}</b></td></tr>
        <tr><td style="padding-right:1em"><b><fmt:message key="personalsettings.format"/></b></td><td>${model.audio.format}</td></tr>
        <tr><td style="padding-right:1em"><b><fmt:message key="personalsettings.duration"/></b></td><td>${model.audio.durationString}</td></tr>
        <tr><td style="padding-right:1em"><b><fmt:message key="personalsettings.bitrate"/></b></td><td>${model.audio.bitRate} Kbps</td></tr>
        <tr><td style="padding-right:1em"><b><fmt:message key="personalsettings.filesize"/></b></td><td><madsonic:formatBytes bytes="${model.audio.fileSize}"/></td></tr>
        <tr><td style="padding-right:1em"><b><fmt:message key="audioConverter.details.status"/></b></td><td>
            <span style="display:none" id="conversion-status-new"><fmt:message key="audioConverter.status.new"/></span>
            <span style="display:none" id="conversion-status-in-progress"><i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;<fmt:message key="audioConverter.status.in_progress"/>
                (<span id="conversion-status-progress"></span>)</span>
            <span style="display:none" id="conversion-status-completed"><fmt:message key="audioConverter.status.completed"/></span>
            <span style="display:none" id="conversion-status-error"><fmt:message key="audioConverter.status.error"/></span>
        </td></tr>
        <tr><td style="padding-right:1em"><b><fmt:message key="audioConverter.details.targetfile"/></b></td><td><span id="conversion-target-file"></span></td></tr>
        <tr><td style="padding-right:1em"><b><fmt:message key="audioConverter.details.logfile"/></b></td><td><span id="conversion-log-file"></span></td></tr>
    </table>
</div>

<div style="clear:both"></div>

<c:set var="licenseInfo" value="${model.licenseInfo}"/>
<%@ include file="licenseNotice.jsp" %>

<p style="margin-left:2em;margin-top:2em">
    <c:choose>
        <c:when test="${model.user.audioConversionRole}">
            <fmt:message key="audioConverter.info"/>
        </c:when>
        <c:otherwise>
            <span class="warning"><fmt:message key="audioConverter.notallowed"/></span>
        </c:otherwise>
    </c:choose>
</p>

<p>
    <i class="fa fa-chevron-left icon"></i>&nbsp;<a href="javascript:back()"><fmt:message key="common.back"/></a>

    <c:if test="${model.user.adminRole}">
        <span style="margin-left:3em">
            <i class="fa fa-cog fa-lg icon"></i>&nbsp;<a href="audioConversionSettings.view"><fmt:message key="common.settings"/></a>
        </span>
    </c:if>
</p>
</body>
</html>
