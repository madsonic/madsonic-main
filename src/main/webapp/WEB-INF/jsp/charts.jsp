<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="iso-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <%@ include file="head.jsp" %>
    <%@ include file="jquery.jsp" %>
    
    <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value="/script/fontawesome/css/font-awesome.min.css"/>" type="text/css">    
    
    <c:choose>
        <c:when test="${model.customScrollbar}">
            <link href="<c:url value="/style/customScrollbar.css"/>" rel="stylesheet">
            <script type="text/javascript" src="<c:url value="/script/jquery.mousewheel.min.js"/>"></script>
            <script type="text/javascript" src="<c:url value="/script/jquery.mCustomScrollbar.concat.min.js"/>"></script>                
        </c:when>
        <c:otherwise>
            <link href="<c:url value="/style/customScrollbarAuto.css"/>" rel="stylesheet">
        </c:otherwise>
    </c:choose>     
    
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/starService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/multiService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/script/jquery-migrate-1.2.1.min.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/script/jquery.scrollTo-2.1.2.js"/>"></script>    
    <script type="text/javascript" language="javascript">
       function refresh() {
            top.main.location.href = "artists.view";
        }        
    </script>
    
<style type="text/css">
span.off {
    font-size:20px !important;
    font-weight:700;
    cursor: pointer;
    float:left;
    padding: 22px 26px;
    margin: 5px;
    /* background: #FFF; */
    color: #fff;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    border-radius: 7px;
    border: solid 2px #888 !important;
    -webkit-transition-duration: 0.1s;
    -moz-transition-duration: 0.1s;
    transition-duration: 0.1s;
    -webkit-user-select:none;
    -moz-user-select:none;
    -ms-user-select:none;
    user-select:none;
    white-space: nowrap;
}

span.on {
    font-size:20px !important;
    font-weight:700;    
    cursor: pointer;
    float:left;
    padding: 22px 26px;
    margin: 5px;
    /* background: #D2F5FF; */
    color: #fff;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    border-radius: 7px;
    border: solid 2px #fff !important;
    -webkit-transition-duration: 0.1s;
    -moz-transition-duration: 0.1s;
    transition-duration: 0.1s;
    -webkit-user-select:none;
    -moz-user-select:none;
    -ms-user-select:none;
    user-select:none;
    white-space: nowrap;
}

span.off:hover {
    background: #D2F5FF;opacity:0.7;
    border: solid 2px #FFF !important;
    text-decoration: none;
    }
    
input {
    margin-right:5px;
}    
    
</style>    

<script type="text/javascript">

function changeClass(elem, className1,className2) {
    elem.className = (elem.className == className1)?className2:className1;
}
</script>

</head>

<body class="bgcolor1 mainframe" style="margin-left:20px; margin-right:40px;">

<!-- content block -->

<div id="content_index" class="content_index">
<!-- CONTENT -->

<a name="top"></a>
<h1 style="padding-bottom:2.8em"><i class="fa fa-line-chart fa-lg icon"></i> artist charts</h1>

<h1>${model.artist}</h1><br>
<br>
<span class="off" style="background: #33CCCC !important;" onclick='changeClass(this,"on","off");'>1</span>
<span class="off" style="background: #99CC00 !important;" onclick='changeClass(this,"on","off");'>2</span>
<span class="off" style="background: #FFCC00 !important;" onclick='changeClass(this,"on","off");'>3</span>
<span class="off" style="background: #FF9900 !important;" onclick='changeClass(this,"on","off");'>4</span>
<span class="off" style="background: #FF6600 !important;" onclick='changeClass(this,"on","off");'>5</span>


<!-- CONTENT -->
</div>

<c:if test="${model.customScrollbar}">
<script type="text/javascript">        
        (function($){
            $(window).load(function(){
                
                $("#content_index").mCustomScrollbar({
                    axis:"y",
                    scrollInertia:800, /*scrolling inertia: integer (milliseconds)*/
                    mouseWheel:true, /*mousewheel support: boolean*/
                    mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
                    autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
                    autoHideScrollbar:true, /*auto-hide scrollbar when idle*/        
                    alwaysShowScrollbar:true,
                    advanced:{      updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                                    autoExpandHorizontalScroll:false }, /*auto expand width for horizontal scrolling: boolean*/
                    scrollButtons:{ enable:true, /*scroll buttons support: boolean*/
                                    scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
                                    scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
                                    scrollAmount:40 },  /*scroll buttons pixels scroll amount: integer (pixels)*/
                                    theme:"${model.customScrollbarTheme}",
                                    scrollbarPosition:"inside"
                });
            });
        })(jQuery);
    </script>
</c:if>

</body> 
   
</html>