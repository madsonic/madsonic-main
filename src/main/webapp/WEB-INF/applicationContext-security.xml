<?xml version="1.0" encoding="UTF-8"?>
<beans:beans xmlns="http://www.springframework.org/schema/security"
       xmlns:beans="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
        http://www.springframework.org/schema/security
        http://www.springframework.org/schema/security/spring-security-3.2.xsd">

    <http pattern="/rest/getApi.view" security="none"/>
    <http pattern="/rest2/getApi.view" security="none"/>
    <http pattern="/rest/videoPlayer*" security="none"/>
    <http pattern="/rest2/videoPlayer*" security="none"/>

    <http pattern="/rest/**" 
    	  use-expressions="true"
    	  create-session="stateless" 
    	  entry-point-ref="restRequestEntryPoint">
        <intercept-url pattern="/**" access="permitAll" />
        <intercept-url pattern="/rest/**" access="permitAll" />
        <custom-filter ref="restSubsonicRequestParameterProcessingFilter" before="PRE_AUTH_FILTER" />
    </http>
     
    <http pattern="/rest2/**" 
          use-expressions="true"
          create-session="stateless" 
          entry-point-ref="restRequestEntryPoint">
		<intercept-url pattern="/**" access="permitAll" />
        <intercept-url pattern="/rest2/**" access="permitAll" />
        <custom-filter ref="restMadsonicRequestParameterProcessingFilter" before="PRE_AUTH_FILTER" />
    </http>

    <http auto-config="false" 
    	  use-expressions="true" 
    	  access-denied-page="/accessDenied.view" 
    	  entry-point-ref="loginUrlAuthenticationEntryPoint">

        <!-- ====================================================================================== -->
        <!-- ANONYMOUS-FILTER -->
        <!-- ====================================================================================== -->
        
        <intercept-url pattern="/crossdomain.xml" access="permitAll" />
        <intercept-url pattern="/login.view*" access="permitAll" />
        <intercept-url pattern="/recover.view" access="permitAll" />
        <intercept-url pattern="/signup.view" access="permitAll" />
        <intercept-url pattern="/finish.view" access="permitAll" />
        <intercept-url pattern="/accessDenied.view" access="permitAll" />
        <intercept-url pattern="/videoPlayer.view" access="permitAll" />
        <intercept-url pattern="/coverArt.view" access="permitAll" />
        <intercept-url pattern="/sonosArt.view" access="permitAll" />
        <intercept-url pattern="/stream/**" access="permitAll" />
        <intercept-url pattern="/dash/**" access="permitAll" />
        <intercept-url pattern="/hls/**" access="permitAll" />
        <intercept-url pattern="/share/**" access="permitAll" />
        <intercept-url pattern="/style/**" access="permitAll" />
        <intercept-url pattern="/icons/**" access="permitAll" />
        <intercept-url pattern="/flash/**" access="permitAll" />
        <intercept-url pattern="/fonts/**" access="permitAll" />
        <intercept-url pattern="/script/**" access="permitAll" />
        <intercept-url pattern="/sonos/**" access="permitAll" />
        <intercept-url pattern="/rss/**" access="permitAll" />		
        <intercept-url pattern="/ws/**" access="permitAll" />
        
        <!-- ====================================================================================== -->
        <!-- ROLE: SETTINGS -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/personalSettings.view" access="hasRole('ROLE_SETTINGS')" />
        <intercept-url pattern="/passwordSettings.view" access="hasRole('ROLE_SETTINGS')" />
        <intercept-url pattern="/playerSettings.view" access="hasRole('ROLE_SETTINGS')" />
        <intercept-url pattern="/shareSettings.view" access="hasRole('ROLE_SETTINGS')" />

        <!-- ====================================================================================== -->
        <!-- ROLE: ADMIN -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/adminSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/generalSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/advancedSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/userSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/lastfmSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/musicFolderSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/musicFolderTasksSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/folderSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/audioConversionSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/videoConversionSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/ldapSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/sonosSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/signupSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/approveSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/apiSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/networkSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/nodeSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/transcodingSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/internetRadioSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/searchSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/podcastSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/radioSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/dlnaSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/followMeSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/groupSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/accessSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/cleanupSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/iconSettings.view" access="hasRole('ROLE_ADMIN')" />
        <intercept-url pattern="/playlistSettings.view" access="hasRole('ROLE_ADMIN')" />		
        <intercept-url pattern="/db.view" access="hasRole('ROLE_ADMIN')" />
        
        <!-- ====================================================================================== -->
        <!-- ROLE: PLAYLIST -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/deletePlaylist.view" access="hasRole('ROLE_PLAYLIST')" />
        <intercept-url pattern="/savePlaylist.view" access="hasRole('ROLE_PLAYLIST')" />
        
        <!-- ====================================================================================== -->
        <!-- ROLE: DOWNLOAD -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/download.view" access="hasRole('ROLE_DOWNLOAD')" />
        
        <!-- ====================================================================================== -->
        <!-- ROLE: UPLOAD -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/upload.view" access="hasRole('ROLE_UPLOAD')" />

        <!-- ====================================================================================== -->
        <!-- ROLE: SHARE -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/createShare.view" access="hasRole('ROLE_SHARE')" />
        
        <!-- ====================================================================================== -->
        <!-- ROLE: COVERART -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/changeCoverArt.view" access="hasRole('ROLE_COVERART')" />
        <intercept-url pattern="/artistGenres.view" access="hasRole('ROLE_COVERART')"/>
        <intercept-url pattern="/editTags.view" access="hasRole('ROLE_COVERART')" />
        <intercept-url pattern="/editArtist.view" access="hasRole('ROLE_COVERART')" />
        
        <!-- ====================================================================================== -->
        <!-- ROLE: COMMENT -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/setMusicFileInfo.view" access="hasRole('ROLE_COMMENT')" />
        
        <!-- ====================================================================================== -->
        <!-- ROLE: PODCAST -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/podcastReceiverAdmin.view" access="hasRole('ROLE_PODCAST')" />
        <intercept-url pattern="/podcastReceiverRefresh.view" access="hasRole('ROLE_SETTINGS')" />
		
        <!-- ====================================================================================== -->
        <!-- ROLE: PODCAST -->
        <!-- ====================================================================================== -->
        <!-- <intercept-url pattern="/podcastChannel.view" access="hasRole('ROLE_STREAM')" /> -->
        <!-- <intercept-url pattern="/podcastChannels.view" access="hasRole('ROLE_STREAM')" /> -->
        
        <!-- ====================================================================================== -->
        <!-- ROLE: VIDEO -->
        <!-- ====================================================================================== -->
        <!--  <intercept-url pattern="/videoPlayer.view" access="hasRole('ROLE_VIDEO')" />  -->

        <!-- ====================================================================================== -->
        <!-- ROLE: IMAGE -->
        <!-- ====================================================================================== -->
        <!--  <intercept-url pattern="/image.view" access="hasRole('ROLE_IMAGE')" />  -->

        <!-- ====================================================================================== -->
        <!-- BROWSE -->
        <!-- ====================================================================================== -->
        <intercept-url pattern="/**" access="isAuthenticated()" />

        <!-- ====================================================================================== -->
        <!-- CUSTOM -->
        <!-- ====================================================================================== -->
		<custom-filter position="FORM_LOGIN_FILTER" ref="madsonicUsernamePasswordAuthenticationFilter" />
        <custom-filter position="REMEMBER_ME_FILTER" ref="madsonicRememberMeFilter" />    
        
        <logout logout-success-url="/login.view" />
    </http>
    
    <!-- FORM-LOGIN -->    
    
    <beans:bean id="loginUrlAuthenticationEntryPoint" class="org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint">
        <beans:property name="loginFormUrl" value="/login.view"/>
    </beans:bean>
    
    <beans:bean id="madsonicUsernamePasswordAuthenticationFilter" class="org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter">
         <beans:property name="authenticationManager" ref="authenticationManager" /> 
         <beans:property name="postOnly" value="false" />
         <beans:property name="rememberMeServices" ref="rememberMeServices" />
         <beans:property name="authenticationFailureHandler" ref="madsonicAuthenticationFailureHandler"/>
         <beans:property name="authenticationSuccessHandler" ref="madsonicAuthenticationSuccessHandler"/>
    </beans:bean>

    <beans:bean id="madsonicAuthenticationSuccessHandler" class="org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler">
        <beans:property name="defaultTargetUrl" value="/index.view" />
    </beans:bean>

    <beans:bean id="madsonicAuthenticationFailureHandler" class="org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler">
        <beans:property name="defaultFailureUrl" value="/login.view?error=true"/> 
        <beans:property name="useForward" value="true"/>
    </beans:bean>
    
    <!-- REMEMBER-ME -->    
    
    <beans:bean id="madsonicRememberMeFilter" class="org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter">
        <beans:property name="rememberMeServices" ref="rememberMeServices"/>
        <beans:property name="authenticationManager" ref="authenticationManager" />
    </beans:bean>

    <beans:bean id="rememberMeServices" class="org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices">
        <beans:property name="userDetailsService" ref="securityService"/>
        <beans:property name="cookieName" value="madsonic_remember_me_cookie" />
        <beans:property name="tokenValiditySeconds" value="31536000"/>
        <beans:property name="key" value="madsonic"/>
    </beans:bean>

    <beans:bean id="rememberMeAuthenticationProvider" class="org.springframework.security.authentication.RememberMeAuthenticationProvider">
        <beans:property name="key" value="madsonic" />
    </beans:bean>
    

    <!-- SECURITY-FILTER -->
    
    <beans:bean id="loginFailureLogger" class="org.madsonic.security.LoginFailureLogger"/>

    <beans:bean class="org.madsonic.security.MadsonicApplicationEventListener">
        <beans:property name="loginFailureLogger" ref="loginFailureLogger"/>
    </beans:bean>
    
    <!-- REST-FILTER -->
    
    <beans:bean id="restMadsonicRequestParameterProcessingFilter" class="org.madsonic.security.MadsonicRESTRequestParameterProcessingFilter">
        <beans:property name="authenticationManager" ref="authenticationManager"/>
        <beans:property name="settingsService" ref="settingsService"/>        
        <beans:property name="securityService" ref="securityService"/>     
        <beans:property name="loginFailureLogger" ref="loginFailureLogger"/>        
    </beans:bean>
    
        <beans:bean id="restSubsonicRequestParameterProcessingFilter" class="org.madsonic.security.SubsonicRESTRequestParameterProcessingFilter">
        <beans:property name="authenticationManager" ref="authenticationManager"/>
        <beans:property name="settingsService" ref="settingsService"/>        
        <beans:property name="securityService" ref="securityService"/>     
        <beans:property name="loginFailureLogger" ref="loginFailureLogger"/>        
    </beans:bean>
    
    <beans:bean id="restRequestEntryPoint" class="org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint">
        <beans:property name="realmName" value="madsonic" />
        <beans:property name="key" value="spring" />        
    </beans:bean>

    <!-- LDAP-FILTER -->

    <beans:bean id="ldapAuthenticationProvider" class="org.springframework.security.ldap.authentication.LdapAuthenticationProvider">
        <beans:constructor-arg ref="bindAuthenticator"/>
        <beans:constructor-arg ref="userDetailsServiceBasedAuthoritiesPopulator"/>
    </beans:bean>

    <beans:bean id="bindAuthenticator" class="org.madsonic.ldap.MadsonicLdapBindAuthenticator">
        <beans:property name="securityService" ref="securityService"/>
        <beans:property name="settingsService" ref="settingsService"/>
    </beans:bean>

    <beans:bean id="userDetailsServiceBasedAuthoritiesPopulator" class="org.madsonic.ldap.UserDetailsServiceBasedAuthoritiesPopulator">
        <beans:property name="userDetailsService" ref="securityService"/>
        <beans:property name="settingsService" ref="settingsService"/>        
        <beans:property name="securityService" ref="securityService"/>
	</beans:bean>

    <!-- ANONYMOUS-FILTER -->

    <beans:bean id="anonymousAuthenticationProvider" class="org.springframework.security.authentication.AnonymousAuthenticationProvider">
        <beans:property name="key" value="madsonic" />
    </beans:bean>
    
    <!-- DECISIONVOTER -->
    
    <beans:bean id="accessDecisionManager" class="org.springframework.security.access.vote.AffirmativeBased">
        <beans:property name="allowIfAllAbstainDecisions" value="false"/>
        <beans:property name="decisionVoters">
            <beans:list>
                <beans:bean class="org.springframework.security.access.vote.RoleVoter"/>
                <beans:bean class="org.springframework.security.access.vote.AuthenticatedVoter"/>
            </beans:list>
        </beans:property>
    </beans:bean>

    <!-- PROVIDER -->
    
    <authentication-manager alias="authenticationManager">
        <authentication-provider ref="ldapAuthenticationProvider"></authentication-provider>
        <authentication-provider ref="rememberMeAuthenticationProvider"></authentication-provider> 
        <authentication-provider ref="anonymousAuthenticationProvider"></authentication-provider> 
        <authentication-provider user-service-ref="securityService">
            <password-encoder ref="passwordEncoder">
                 <salt-source ref="saltSource" />
            </password-encoder>
        </authentication-provider>
    </authentication-manager>

</beans:beans>