Mousetrap.bind('space', function() { getPlayerWindow().keyboardShortcut("togglePlayPause"); return false });
Mousetrap.bind('left', function() { getPlayerWindow().keyboardShortcut("previous") });
Mousetrap.bind('right', function() { getPlayerWindow().keyboardShortcut("next") });
Mousetrap.bind('shift+left', function() { getPlayerWindow().keyboardShortcut("seekBackward") });
Mousetrap.bind('shift+right', function() { getPlayerWindow().keyboardShortcut("seekForward") });
Mousetrap.bind('-', function() { getPlayerWindow().keyboardShortcut("volumeDown") });
Mousetrap.bind('plus', function() { getPlayerWindow().keyboardShortcut("volumeUp") });

Mousetrap.bind('m', function() { parent.frames.left.keyboardShortcut("toggleLeftBar") });
Mousetrap.bind('l', function() { parent.frames.left.keyboardShortcut("toggleLeftPanel") });

Mousetrap.bind('g h', function() { parent.frames.left.keyboardShortcut("showHome") });
Mousetrap.bind('g i', function() { parent.frames.left.keyboardShortcut("showIndex") });
Mousetrap.bind('g t', function() { parent.frames.left.keyboardShortcut("showStarred") });
Mousetrap.bind('g g', function() { parent.frames.left.keyboardShortcut("showGenre") });
Mousetrap.bind('g r', function() { parent.frames.left.keyboardShortcut("showRadio") });
Mousetrap.bind('g d', function() { parent.frames.left.keyboardShortcut("showDJ") });
Mousetrap.bind('g p', function() { parent.frames.left.keyboardShortcut("showPlaylists") });
Mousetrap.bind('g o', function() { parent.frames.left.keyboardShortcut("showPodcasts") });
Mousetrap.bind('g s', function() { parent.frames.left.keyboardShortcut("showSettings") });
Mousetrap.bind('g m', function() { parent.frames.left.keyboardShortcut("showMore") });
Mousetrap.bind('g a', function() { parent.frames.left.keyboardShortcut("showAbout") });
Mousetrap.bind('s', function() { parent.frames.left.keyboardShortcut("search"); return false });
Mousetrap.bind('?', function() { parent.frames.left.keyboardShortcut("showMore", "shortcuts")});

var indices = "abcdefghijklmnopqrstuvwxyz";
for (var i = 0, len = indices.length; i < len; i++) {
    var target = indices.substring(i, i + 1);
    createKeyboardShortcutForIndex(target);
}
function getPlayerWindow() {
    if (parent.frames.main == null) {
        return window;
    }
    if (parent.frames.main.location.href.indexOf("videoPlayer.view") == -1) {
        return parent.frames.playQueue;
    }
    return parent.frames.main;
}
function createKeyboardShortcutForIndex(index) {
    Mousetrap.bind("i " + index, function() {
        parent.frames.left.keyboardShortcut("showIndex", index.toUpperCase());
    });
}
